package com.srp.agronome.android;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Geolocalisation;
import com.srp.agronome.android.db.GeolocalisationDao;
import com.srp.agronome.android.db.ILOT;
import com.srp.agronome.android.db.ILOTDao;
import com.srp.agronome.android.db.PARCELLE;
import com.srp.agronome.android.db.PARCELLEDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;

import org.greenrobot.greendao.query.QueryBuilder;
import java.util.Calendar;
import java.util.List;

import static com.srp.agronome.android.CustomparcelleAdapter.Nom_Parcelle;
import static com.srp.agronome.android.CustomparcelleAdapter.Nom_ilot;
import static com.srp.agronome.android.CustomparcelleAdapter.Numero_Parcelle;
import static com.srp.agronome.android.CustomparcelleAdapter.Numero_ilot;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuParcelle.Id_Parcelle_selected;
import static com.srp.agronome.android.MenuParcelle.Id_ilot_selected;

/**
 * Created by jitendra on 27-08-2017.
 */

public class ModificationParcelle  extends AppCompatActivity {
    public static int  archived = 1;
    private TextView Text_nom_Ilot = null;
    private TextView Text_numero_Ilot = null;
    private TextView Text_Nom_Parcelle = null;
    private TextView Text_Numero_Parcelle = null;


    private ImageButton Edit = null;
    private ImageButton Delete = null;
    private ImageButton imgBtnHome = null;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_modificationparcelle);
        Edit = (ImageButton) findViewById(R.id.btn_edit);
        Edit.setOnClickListener(ButtonEditHandler);

        Delete = (ImageButton) findViewById(R.id.btn_delete);
        Delete.setOnClickListener(ButtonDeleteHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_ModificationParcelle);
        imgBtnHome.setOnClickListener(ButtonbackHandler);
        FindWidgetViewbyId();

        //Example Vincent
        Text_nom_Ilot.setText( Nom_ilot);
        Text_numero_Ilot.setText( Numero_ilot);
        Text_Nom_Parcelle.setText( Nom_Parcelle);
        Text_Numero_Parcelle.setText( Numero_Parcelle);

        parcelleobjecttoid(Id_ilot_selected,Id_Parcelle_selected);
}

    private void FindWidgetViewbyId() {
        Text_nom_Ilot = (TextView) findViewById(R.id.Ilot_nom);
        Text_numero_Ilot = (TextView) findViewById(R.id.Ilot_numero);
        Text_Nom_Parcelle= (TextView) findViewById(R.id.Nom_Parcelle);
        Text_Numero_Parcelle = (TextView) findViewById(R.id.NUMERO_PARCELLE);
    }

    private void parcelleobjecttoid( Long Id_ilot_selected, Long Id_Parcelle_selected){


    }


    View.OnClickListener ButtonEditHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style
            PopupMenu popup = new PopupMenu(wrapper, Edit); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_edit_parcelle, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    //Toast.makeText(getApplicationContext(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                    if (item.getTitle().equals(getString(R.string.TextEditParcelle))){
                        Intent intent = new Intent(ModificationParcelle.this, EditParcelle.class);
                        finish();
                        startActivity(intent);
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    //Bouton back Toolbar
    View.OnClickListener ButtonbackHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };

    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(ModificationParcelle.this, ListParcelle.class);
                finish();
                startActivity(intent);
             //   Delete_cache();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
               // clearData();
                Intent intent = new Intent(ModificationParcelle.this, ListParcelle.class);
                finish();
                startActivity(intent);
              //  Delete_Cache();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }
    View.OnClickListener ButtonDeleteHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style

            PopupMenu popup = new PopupMenu(wrapper, Delete); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_delete_parcelle, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {

                    if (item.getTitle().equals(getString(R.string.TextDeleteParcelle))){
                        Show_Dialog__Delete_Parcelle(getString(R.string.TitleDeleteparcelle),getString(R.string.TextDeleteparcelle));
                    }
                    if (item.getTitle().equals(getString(R.string.TextDeleteILOT))){
                        Show_Dialog__Delete_Ilot(getString(R.string.TitleDeleteilot),getString(R.string.TextDeleteilot));
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };
    //Display Dialog Delete_Parcelle
    private void Show_Dialog__Delete_Parcelle(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ParcelleDeletestrategy();
                Intent intent = new Intent(ModificationParcelle.this, ListParcelle.class);
                finish();
                startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }
    private void ParcelleDeletestrategy(){
        SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        List <ILOT> list_ilot = getIlotList(sp);
        ILOT ilot_selected = getIlot(list_ilot,Id_ilot_selected);
        List <PARCELLE> list_parcelle = getParcelleList(ilot_selected);
        PARCELLE parcelle_delet = getParcelle(list_parcelle,Id_Parcelle_selected);
        DeleteParcelle(parcelle_delet);


    }
    //Delete a parcelle
    private void DeleteParcelle(PARCELLE parcelle_delete){
        if (CheckRelationsParcelle(parcelle_delete.getId())){
            if (parcelle_delete.getArchive() == 2){
                Show_Dialog_Delete_Parcelle(getString(R.string.TitleConfirmDelete) , getString(R.string.TextConfirmDelete), parcelle_delete);
            }

            else {
                ArchiveParcelle(parcelle_delete);
               }
        }
        else{
            Show_Dialog(getString(R.string.TitleDeleteFail),getString(R.string.TextDeleteFail));
        }
    }

    private boolean CheckRelationsParcelle (long ID_Parcelle){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ILOTDao ilot_data = daoSession.getILOTDao();
        QueryBuilder<ILOT> queryBuilder = ilot_data.queryBuilder();
        queryBuilder.join(PARCELLE.class, PARCELLEDao.Properties.ID_ILOT)
                .where(PARCELLEDao.Properties.Id.eq(ID_Parcelle));
        List<ILOT> list_ilot = queryBuilder.list();
        //  Log.i("ILot Relation","number  of ilot  Relation : " + list_ilot.size() );
        return (list_ilot.size() < 2 );
    }

    //Display Dialog Delete_Parcelle
    private void Show_Dialog_Delete_Parcelle(String title, String message , final PARCELLE parcelle_delete) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArchiveParcelle(parcelle_delete);
                //     Log.i("check value","ArchiveParcelle : " + parcelle_delete );
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void ArchiveParcelle(PARCELLE parcelle_delete){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PARCELLEDao parcelle_data = daoSession.getPARCELLEDao();
        parcelle_delete.setArchive(archived); // 1= hidden
        parcelle_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        parcelle_delete.setDate_modification(Today_Date());
        parcelle_data.update(parcelle_delete);
    }
    //Display Dialog Delete_Entreprise
    private void Show_Dialog__Delete_Ilot(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ILOTDeletestrategy();
                Intent intent = new Intent(ModificationParcelle.this, ListParcelle.class);
                finish();
                startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }
    private void ILOTDeletestrategy(){
        SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        List <ILOT> list_ilot = getIlotList(sp);
        ILOT ilot_delete = getIlot(list_ilot,Id_ilot_selected);
        DeleteIlot(ilot_delete);
    }

    //Delete a ilot and its list of parcelle

    private void DeleteIlot(ILOT ilot_delete){
        List<PARCELLE> list_parcelle = ilot_delete.getPARCELLEList();
        boolean archive = true;
        if (CheckRelationsIlot(ilot_delete.getId())) {archive = false ;}
        for (PARCELLE P : list_parcelle){
            if (CheckRelationsParcelle(P.getId())) { archive = false ;}
        }
        if (archive){
            Show_Dialog_Delete_Ilot(getString(R.string.TitleConfirmDelete) , getString(R.string.TextConfirmDelete), ilot_delete);
        }
        else{
            Show_Dialog(getString(R.string.TitleDeleteFail),getString(R.string.TextDeleteFail));
        }
    }
    //Display Dialog Delete_Ilot
    private void Show_Dialog_Delete_Ilot(String title, String message , final ILOT ilot_delete) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArchiveIlot(ilot_delete);
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }
    private boolean CheckRelationsIlot(long ID_Ilot){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
        QueryBuilder<SUIVI_PRODUCTEUR> queryBuilder = suivi_producteur_data.queryBuilder();
        queryBuilder.join(ILOT.class, ILOTDao.Properties.ID_SUIVI_PRODUCTEUR)
                .where(ILOTDao.Properties.Id.eq(ID_Ilot));
        List<SUIVI_PRODUCTEUR> list_SP = queryBuilder.list();
        //Log.i("EntrepriseRelation","Nbr Entreprise Relation : " + list_entreprise.size() );
        return (list_SP.size() < 2 );
    }


    private void ArchiveIlot (ILOT ilot_delete){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ILOTDao ilot_data = daoSession.getILOTDao();
        PARCELLEDao parcelle_data = daoSession.getPARCELLEDao();
        List <PARCELLE> list_parcelle = ilot_delete.getPARCELLEList();
        if (list_parcelle != null){
            for (PARCELLE P : list_parcelle){
                P.setArchive(archived);
                P.setDate_modification(Today_Date());
                P.setID_Agronome_Modification((int)ID_Compte_Selected);
                parcelle_data.update(P);
            }
        }
        ilot_delete.setArchive(archived);
        ilot_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        ilot_delete.setDate_modification(Today_Date());
        ilot_data.update(ilot_delete);
    }
    public static java.util.Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }
    private SUIVI_PRODUCTEUR getSuiviProducteurOfOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        SUIVI_PRODUCTEUR suivi_producteur_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                            .where(CompteDao.Properties.Id.eq(Id_Compte))
                            .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        suivi_producteur_result = e.getSUIVI_PRODUCTEUR();
                                }
                            }
                        }
                    }
                    return suivi_producteur_result;
                }

    private void DeleteGeolocalisationParcelle(PARCELLE geolocalisation_parcelle_delete){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PARCELLEDao parcelle_data = daoSession.getPARCELLEDao();
        GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();
        geolocalisation_data.delete(geolocalisation_parcelle_delete.getGeolocalisation());
        parcelle_data.update(geolocalisation_parcelle_delete);
    }


    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }
    //Return a list of Parcelle
    private List<PARCELLE> getParcelleList(ILOT ilot) {
        return ilot.getPARCELLEList();
    }

    //Return a parcelle
    private PARCELLE getParcelle(List<PARCELLE> list_parcelle, long ID_Parcelle) {
        PARCELLE result = null;
        for (PARCELLE P : list_parcelle) {
            if (P.getId() == ID_Parcelle) {
                result = P;
            }
        }
        return result;
    }
                //Return a list of Ilots
                private List<ILOT> getIlotList(SUIVI_PRODUCTEUR Suivi_producteur){
                    return Suivi_producteur.getILOTList();
                }

    //Return an ilot
    private ILOT getIlot(List<ILOT> list_ilot, long ID_Ilot) {
        ILOT result = null;
        for (ILOT I : list_ilot) {
            if (I.getId() == ID_Ilot) {
                result = I;
            }
        }
        return result;
    }
    }






