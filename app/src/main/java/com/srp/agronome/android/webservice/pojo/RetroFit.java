package com.srp.agronome.android.webservice.pojo;


import com.srp.agronome.android.webservice.LoggingInterceptor;
import com.srp.agronome.android.webservice.pojo.MultipleResource;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.srp.agronome.android.webservice.Service;

/**
 * Created by soytouchrp on 04/12/2017.
 */

public class RetroFit {

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

//        LoggingInterceptor interceptor = new LoggingInterceptor();
     //   interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        OkHttpClient client = new OkHttpClient.Builder().build();


        retrofit = new Retrofit.Builder()
                .baseUrl(Service.REST2_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();



        return retrofit;
    }

    public static Retrofit getClient(Interceptor interceptorToAdd) {

        LoggingInterceptor interceptor = new LoggingInterceptor();
    //    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
             //   .addInterceptor(interceptor)
                .addInterceptor(interceptorToAdd)
                .build();



        retrofit = new Retrofit.Builder()
                .baseUrl(Service.REST2_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();



        return retrofit;
    }
}