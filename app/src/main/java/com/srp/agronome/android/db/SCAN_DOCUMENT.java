package com.srp.agronome.android.db;

import org.greenrobot.greendao.annotation.*;

import com.srp.agronome.android.db.DaoSession;
import org.greenrobot.greendao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "SCAN__DOCUMENT".
 */
@Entity(active = true)
public class SCAN_DOCUMENT {

    @Id
    private Long id;
    private String nom;
    private long ID_DOCUMENT ;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient SCAN_DOCUMENTDao myDao;

    @ToOne(joinProperty = "ID_DOCUMENT ")
    private DOCUMENT dOCUMENT;

    @Generated
    private transient Long dOCUMENT__resolvedKey;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public SCAN_DOCUMENT() {
    }

    public SCAN_DOCUMENT(Long id) {
        this.id = id;
    }

    @Generated
    public SCAN_DOCUMENT(Long id, String nom, long ID_DOCUMENT ) {
        this.id = id;
        this.nom = nom;
        this.ID_DOCUMENT  = ID_DOCUMENT ;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getSCAN_DOCUMENTDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getID_DOCUMENT () {
        return ID_DOCUMENT ;
    }

    public void setID_DOCUMENT (long ID_DOCUMENT ) {
        this.ID_DOCUMENT  = ID_DOCUMENT ;
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public DOCUMENT getDOCUMENT() {
        long __key = this.ID_DOCUMENT ;
        if (dOCUMENT__resolvedKey == null || !dOCUMENT__resolvedKey.equals(__key)) {
            __throwIfDetached();
            DOCUMENTDao targetDao = daoSession.getDOCUMENTDao();
            DOCUMENT dOCUMENTNew = targetDao.load(__key);
            synchronized (this) {
                dOCUMENT = dOCUMENTNew;
            	dOCUMENT__resolvedKey = __key;
            }
        }
        return dOCUMENT;
    }

    @Generated
    public void setDOCUMENT(DOCUMENT dOCUMENT) {
        if (dOCUMENT == null) {
            throw new DaoException("To-one property 'ID_DOCUMENT ' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.dOCUMENT = dOCUMENT;
            ID_DOCUMENT  = dOCUMENT.getId();
            dOCUMENT__resolvedKey = ID_DOCUMENT ;
        }
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
