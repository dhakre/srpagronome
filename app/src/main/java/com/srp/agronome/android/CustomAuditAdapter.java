package com.srp.agronome.android;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.AUDIT_DOCUMENT;
import com.srp.agronome.android.db.AUDIT_DOCUMENTDao;
import com.srp.agronome.android.db.AUDIT_PARCELLE;
import com.srp.agronome.android.db.AUDIT_PARCELLEDao;
import com.srp.agronome.android.db.AUDIT_STOCKAGE;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Visite;
import com.srp.agronome.android.db.VisiteDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuAudit.Audit_Id_Selected;
import static com.srp.agronome.android.Audit.AUDIT_TYPE;
import static com.srp.agronome.android.ListAudit.activity;

import static com.srp.agronome.android.MenuVisite.Id_Visite_Selected;
import static com.srp.agronome.android.R.id.list_visite;

/**
 * Created by sumitra on 10/23/2017.
 */



public class CustomAuditAdapter extends ArrayAdapter<AuditClass> {
    Context context;
   // Activity activity1=activity;
    int layoutResourceId;

    ArrayList<AuditClass> auditData=new ArrayList<>();

    public CustomAuditAdapter(Context context, int layoutResourceId, ArrayList<AuditClass> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.auditData = data;
        getFilter();
    }

    public void onItemselected(int position) {

    }

    public class Holder {
        TextView remarque;
        TextView audit_date;
       // TextView audit_type;
        TextView Adocument;
        TextView Aparcelle;
        TextView Astockage;
        ImageButton edit;
        ImageButton delete;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder = new Holder();
        String remarque,docType;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            Log.i("CheckValue", "inside holder : ");

            final AuditClass auditClass = auditData.get(position);
            //get ui
            holder.remarque = (TextView) row.findViewById(R.id.auditRemarque);
            holder.audit_date = (TextView) row.findViewById(R.id.auditYear);
            holder.delete = (ImageButton) row.findViewById(R.id.btn_delete_visite);
            holder.edit= (ImageButton) row.findViewById(R.id.btn_edit);
            //see list of audit sub-modules doc,parcelle,stockage
            holder.Adocument= (TextView) row.findViewById(R.id.auditDocument);
            holder.Aparcelle= (TextView) row.findViewById(R.id.auditParcelle);
            holder.Astockage= (TextView) row.findViewById(R.id.auditStockage);
            //holder.audit_type= (TextView) row.findViewById(R.id.auditType);


            Typeface Regular = Typeface.createFromAsset(getContext().getAssets(), "Exo/Exo-Regular.ttf");


            Log.i("data", "getView: "+auditClass.getRemarque());
            remarque=auditClass.getRemarque();
            //remarque=GlobalValues.getInstance().setName(remarque);
            Log.i("name data", "getView: "+remarque);
            holder.remarque.setText(remarque);
           //holder.audit_type.setText(AUDIT_TYPE);  //set the audit type
           /* if(AUDIT_TYPE!=null)
            {
             docType=AUDIT_TYPE;
            }
            else
            {
             docType="Not defined";
            }*/
           //audit type
           // AUDIT_TYPE=getAuditType(Audit_Id_Selected);

           // holder.audit_type.setText(AUDIT_TYPE);
            holder.remarque.setTypeface(Regular);

            holder.audit_date.setText(auditClass.getYear());
            holder.audit_date.setTypeface(Regular);
            Log.i("year", "getView: "+auditClass.date);

            //doc list
            holder.Adocument.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Audit_Id_Selected=auditClass.getAudit_ID();  //get selected id
                   // Intent intent=new Intent(context,listDocument.class);
                  //  context.startActivity(intent);
                }
            });

            //parcelle list
            holder.Aparcelle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Audit_Id_Selected=auditClass.getAudit_ID();  //get selected id
                    // Intent intent=new Intent(context,listParcelle.class);
                    //  context.startActivity(intent);

                }
            });

            //stockage list
            holder.Astockage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Audit_Id_Selected=auditClass.getAudit_ID();  //get selected id
                    // Intent intent=new Intent(context,listStockage.class);
                    //  context.startActivity(intent);

                }
            });

            //edit
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Audit_Id_Selected=auditClass.getAudit_ID();  //get selected id
                    Intent intent=new Intent(context,EditAudit.class);
                    context.startActivity(intent);

                }
            });

            //delete
           holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent=new Intent(context,.class);
                    //context.startActivity(intent);
                    //Creating the instance of PopupMenu
                    Context wrapper = new ContextThemeWrapper(context, R.style.MyPopupMenu); //Affecter le style

                    PopupMenu popup = new PopupMenu(wrapper, holder.delete); //Creation du popup
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.menu_delete_audit, popup.getMenu());
                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {

                            if (item.getTitle().equals(context.getString(R.string.deleteAudit))){
                                Audit_Id_Selected=auditClass.getAudit_ID();
                                Show_Dialog__Delete_audit(context.getString(R.string.titleDeleteAudit),context.getString(R.string.textDeleteAuditMessage));
                            }
                            return true;
                        }
                    });
                    popup.show();//showing popup menu
                }
            });



            //return row;
        }

        return row;
    }

    private void Show_Dialog__Delete_audit(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(context.getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AuditDelete();
                Intent intent = new Intent(context, ListAudit.class);
                //finish();
                context.startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(context.getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //get audit type
    private String getAuditType(long audit_id_selected)
    {
        String auditType="Not defined";
        DaoSession daoSession = ((AppController) activity).getDaoSession();
        AUDITDao auditDao=daoSession.getAUDITDao();
        //get audit
        AUDIT audit_selected=getAuditfromID(audit_id_selected);

        //check for audit doc
        AUDIT_DOCUMENTDao audit_documentDao=daoSession.getAUDIT_DOCUMENTDao();
        List<AUDIT_DOCUMENT> audit_documentList=audit_documentDao.queryBuilder().where(AUDIT_DOCUMENTDao.Properties.Id.eq(audit_selected.getID_AUDIT_DOCUMENT())).list();
        if(audit_documentList!=null)
        {
            auditType="Document";
        }
        else
        {
            auditType="Not defined";
        }

        //check for parcelle
        AUDIT_PARCELLEDao audit_parcelleDao=daoSession.getAUDIT_PARCELLEDao();
        List<AUDIT_PARCELLE> audit_parcelleList=audit_selected.getAUDIT_PARCELLEList();
        if(audit_parcelleList!=null)
        {
            auditType=auditType+":"+"Parcelle";
        }
        else
        {
            auditType=auditType+":"+"Not defined";
        }

        //check audit stockage
        List<AUDIT_STOCKAGE> audit_stockageList=audit_selected.getAUDIT_STOCKAGEList();
        if(audit_stockageList!=null)
        {
            auditType=auditType+":"+"Stockage";
        }
        else
            {
                auditType=auditType+":"+"Not defined";
        }

         return auditType;
    }

    //get audit
    private AUDIT getAuditfromID(long ID_audit){
        DaoSession daoSession = ((AppController) activity).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        AUDIT result = null;
        List<AUDIT> audit_load = audit_data.queryBuilder()
                .where(AUDITDao.Properties.Id.eq(ID_audit))
                .list();
        if (audit_load != null) {
            for (AUDIT A: audit_load) {

                result = A;
            }
        }
        return result;
    }

    private void AuditDelete() {
       // DaoSession daoSession=((AppController) activity.getApplication()).getDaoSession();
        DaoSession daoSession = ((AppController) activity).getDaoSession();
        AUDITDao auditDao=daoSession.getAUDITDao();
        List <AUDIT> list_audit = auditDao.loadAll();
        AUDIT delete_audit =getAudit(Audit_Id_Selected,list_audit);
        //Log.i("Deletevisit","delete_visit  "+delete_visit);
        //ArchiveVisite(delete_visit);
        // Show_Dialog_Delete_Visite(getString(R.string.TitleConfirmDelete) , getString(R.string.TextConfirmDelete), delete_visit);
        DeleteAudit(delete_audit);
    }

    private AUDIT getAudit(long ID_Audit, List<AUDIT> list_audit) {
        AUDIT result = null;
        if (list_audit != null){
            for (AUDIT a : list_audit){
                if (a.getId() == ID_Audit){
                    result = a;
                }
            }
        }
        return result;
    }

    private void DeleteAudit(AUDIT audit_delete) {
        if (CheckRelationsAudit(audit_delete.getId())){
            Log.i("Deleteaudit","audit_delete.getId()  "+audit_delete.getId());
            Log.i("Deleteaudit","audit_delete.getArchive()  "+audit_delete.getArchive());
            Log.i("Deleteaudit","audit_delete  "+audit_delete);
            if (audit_delete.getArchive() == GlobalValues.getInstance().getGlobalArchiveAvecUtilisateur()){    //archieve=2
                Log.i("Deleteaudit","audit_delete.getArchive()inside  "+audit_delete.getArchive());
                Show_Dialog_Delete_audit(context.getString(R.string.TitleConfirmDelete) , context.getString(R.string.TextConfirmDelete), audit_delete);
            }

            else{

                ArchiveAudit(audit_delete);

            }
        }
        else{
            Show_Dialog(context.getString(R.string.TitleDeleteFail),context.getString(R.string.TextDeleteFail));
        }
    }



    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

    private boolean CheckRelationsAudit(Long id_audit) {
        Log.i("Deleteaudit","ID_audit  "+id_audit);
        DaoSession daoSession = ((AppController) activity).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        QueryBuilder<AUDIT> queryBuilder = audit_data.queryBuilder();
        queryBuilder.join(AUDIT.class, AUDITDao.Properties.Id)
                .where(AUDITDao.Properties.Id.eq(id_audit));
        List<AUDIT> list_A = queryBuilder.list();
        Log.i("Deleteaudit","list_A.size()  "+list_A.size());
        return (list_A.size() < 2 );
    }

    private void Show_Dialog_Delete_audit(String string, String string1, AUDIT audit_delete) {
    }


    private void ArchiveAudit(AUDIT audit_delete) {
        Log.i("DeleteAudit","ArchiveAudit1  "+audit_delete);
        DaoSession daoSession = ((AppController) activity).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        audit_delete.setArchive(GlobalValues.getInstance().getGlobalArchiveAvecUtilisateur());  //put global value here archive=1
        audit_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        audit_delete.setDate_modification(GlobalValues.Today_Date());
        audit_data.update(audit_delete);
        Log.i("Deleteaudit","audit_data  "+audit_data);
    }

}
