package com.srp.agronome.android;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.AUDIT_PARCELLEDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Visite;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.Audit.AUDIT_TYPE;

public class ListAudit extends AppCompatActivity {
    private ImageButton btnRetour =null;
    private ListView listview_audit;
    private ArrayList<AuditClass> arrayListaudit = new ArrayList<AuditClass>();
    public static Application activity;
    //Custom Adapter
    CustomAuditAdapter auditAdapter;

    private TextView toolbarName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_list);
        activity=getApplication();

        LoadAuditList();

        toolbarName= (TextView) findViewById(R.id.ListVisite);
        listview_audit= (ListView) findViewById(R.id.listview_visite);
        btnRetour = (ImageButton) findViewById(R.id.arrow_back);
        //set name to audit
        toolbarName.setText("AUDIT");//code optimization

        //custom layout
        auditAdapter=new CustomAuditAdapter(this,R.layout.audit_custom_layout,arrayListaudit);

        listview_audit.setAdapter(auditAdapter);

        btnRetour.setOnClickListener(ButtonHomeHandler);


    }

    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ListAudit.this, MenuAudit.class);
            finish();
            startActivity(intent);
        }
    };


    //load audit
    public void LoadAuditList()
    {
        DaoSession daoSession=((AppController) getApplication()).getDaoSession();
        Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        //enterprise for audit
       // String entrepriseselected =entreprise_selected.getRaison_sociale();

        AUDITDao auditDao=daoSession.getAUDITDao();
      //  List<AUDIT> list_audit= auditDao.queryBuilder().where(AUDIT_PARCELLEDao.Properties.REMARQUES_AUDIT_PARCELLE.eq("good"))
       //         .orderAsc(AUDIT_PARCELLEDao.Properties.Date_creation).list();
        List<AUDIT> list_audit=auditDao.loadAll();

        Log.i("CheckValue", "list_Audit  : " + list_audit ) ;
        for (AUDIT audit : list_audit) {

            try {
                if (audit.getArchive() == GlobalValues.getInstance().getGlobalArchive())        //chnaging the value 1 to the global value
                    arrayListaudit.add(new AuditClass(
                            audit.getId(),
                            "Remarque:" + audit.getREMARQUES_AUDITS(),
                            "Audit Date : " + datetostring(audit.getDATE())

                    ));
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

    }

    //Return an Organization
    public Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }

    //chnage date
    public String datetostring(Date date)
    {
        SimpleDateFormat sdf=new SimpleDateFormat("d/MM/yyyy");
        return  sdf.format(date);
    }
}
