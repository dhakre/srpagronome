package com.srp.agronome.android;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.LoginFilter;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.PopupMenu;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Visite;
import com.srp.agronome.android.db.VisiteDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.R.id.message;
import static com.srp.agronome.android.ListViste.DAOSESSION;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuVisite.Id_Visite_Selected;
import static com.srp.agronome.android.R.attr.title;
import static com.srp.agronome.android.db.Sujet_Visite_SecondaireDao.Properties.ID_Visite;

/**
 * Created by jitendra on 07-09-2017.
 */


public class CustomVisitAdapter extends ArrayAdapter<VisiteClass> {

    public static String RaisonSociale;
    public static String objetVisite;
    public static  String sub_principal;
    public static String sub_secondary;
    Context context;
    Activity activity;
    int layoutResourceId;
    ArrayList<VisiteClass> Visitedata = new ArrayList<VisiteClass>();
    private ArrayList<VisiteClass> filteredList;

    public CustomVisitAdapter(Context context, int layoutResourceId, ArrayList<VisiteClass> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.Visitedata = data;
        this.filteredList = data;
        getFilter();
    }

    public void onItemselected(int position) {

    }

    public class Holder {
        MyTextView Raison_sociale;
        MyTextView objet_visite;
        MyTextView sub_principal;
        MyTextView comment;
        ImageButton btn;
        ImageButton delete;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder = new Holder();
        String nom;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            Log.i("CheckValue", "inside holder : ");

            final VisiteClass VisiteClass = Visitedata.get(position);
            holder.Raison_sociale = (MyTextView) row.findViewById(R.id.Raison__sociale);
            holder.objet_visite = (MyTextView) row.findViewById(R.id.objet__visite);
            holder.btn = (ImageButton) row.findViewById(R.id.btn_voir_visite);  //photo button
            holder.delete = (ImageButton) row.findViewById(R.id.btn_delete_visite);
            holder.sub_principal= (MyTextView) row.findViewById(R.id.sub_principal);
            holder.comment= (MyTextView) row.findViewById(R.id.comment);

            Typeface Regular = Typeface.createFromAsset(getContext().getAssets(), "Exo/Exo-Regular.ttf");

            //holder.Raison_sociale.setText(VisiteClass.getRaisonsociale());
            Log.i("data", "getView: "+VisiteClass.getRaisonsociale());
            nom=VisiteClass.getRaisonsociale();
            nom=setNameVisite(nom);   //Raison sociale : MIRANI ANDRE
            Log.i("name data", "getView: "+nom);
            holder.Raison_sociale.setText("Raison sociale"+nom);
            holder.Raison_sociale.setTypeface(Regular);

            holder.objet_visite.setText(VisiteClass.getObjetvisite());
            holder.objet_visite.setTypeface(Regular);

            holder.sub_principal.setText(VisiteClass.getSubject_principal());
            holder.sub_principal.setTypeface(Regular);

            holder.comment.setText(VisiteClass.getComment());

            holder.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    RaisonSociale = VisiteClass.getRaisonsociale();
                    objetVisite = VisiteClass.getObjetvisite();
                    sub_principal=VisiteClass.getSubject_principal();
                    //sub_secondary=VisiteClass.getSubject_secoundary();
                    //Log.i("CheckValue", "objetVisite : " + objetVisite ) ;
                    Id_Visite_Selected = VisiteClass.getID_Visite();
                    Intent next = new Intent(context, ModificationVisite.class);
                    ((Activity) context).finish();
                    context.startActivity(next);
                }
            });

            //delete
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent=new Intent(context,.class);
                    //Intent intent=new Intent(context,.class);
                    //context.startActivity(intent);
                    //Creating the instance of PopupMenu
                    Context wrapper = new ContextThemeWrapper(context, R.style.MyPopupMenu); //Affecter le style

                    PopupMenu popup = new PopupMenu(wrapper, holder.delete); //Creation du popup
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.menu_delete_visite, popup.getMenu());
                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {

                            if (item.getTitle().equals(context.getString(R.string.TextDeleteVisite))){
                                Id_Visite_Selected=VisiteClass.getID_Visite();
                                Show_Dialog__Delete_visite(context.getString(R.string.TitleDeleteVisite),context.getString(R.string.TextDeletevisitemessage));
                            }
                            return true;
                        }
                    });
                    popup.show();//showing popup menu
                }
            });



            //return row;
        }

     return row;
    }

    private void Show_Dialog__Delete_visite(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(context.getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                VisiteDeletestrategy();
                Intent intent = new Intent(context, ListViste.class);
                //finish();
                context.startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(context.getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void VisiteDeletestrategy() {
        Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        List <Visite> list_visite = getListVisite(entreprise_selected);
        Visite delete_visit =getVisite(Id_Visite_Selected,list_visite);
        //Log.i("Deletevisit","delete_visit  "+delete_visit);
        //ArchiveVisite(delete_visit);
        // Show_Dialog_Delete_Visite(getString(R.string.TitleConfirmDelete) , getString(R.string.TextConfirmDelete), delete_visit);
        DeleteVisite(delete_visit);
    }

    private void DeleteVisite(Visite visite_delete) {
        if (CheckRelationsVisite(visite_delete.getId())){
            Log.i("Deletevisit","visite_delete.getId()  "+visite_delete.getId());
            Log.i("Deletevisit","visite_delete.getArchive()  "+visite_delete.getArchive());
            Log.i("Deletevisit","visite_delete  "+visite_delete);
            if (visite_delete.getArchive() == 2){    //archieve=2
                Log.i("Deletevisit","visite_delete.getArchive()inside  "+visite_delete.getArchive());
                Show_Dialog_Delete_Visite(context.getString(R.string.TitleConfirmDelete) , context.getString(R.string.TextConfirmDelete), visite_delete);
            }

            else{

                ArchiveVisite(visite_delete);

            }
        }
        else{
            Show_Dialog(context.getString(R.string.TitleDeleteFail),context.getString(R.string.TextDeleteFail));
        }
    }

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

    private void ArchiveVisite(Visite visite_delete) {
        Log.i("Deletevisit","ArchiveVisite1  "+visite_delete);
        DaoSession daoSession = DAOSESSION;
        VisiteDao visite_data = daoSession.getVisiteDao();
        visite_delete.setArchive(1);  //put global value here archive=1
        visite_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        visite_delete.setDate_modification(GlobalValues.Today_Date());
        visite_data.update(visite_delete);
        Log.i("Deletevisit","visite_data  "+visite_data);
    }

    private Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    private void Show_Dialog_Delete_Visite(String string, String string1, Visite visite_delete) {
    }



    private boolean CheckRelationsVisite(Long ID_Visite) {
        Log.i("Deletevisit","ID_Visite  "+ID_Visite);
        DaoSession daoSession = ((AppController) ((Activity)context).getApplication()).getDaoSession();
        //DaoSession daoSession=daoSession_ip;
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        QueryBuilder<Entreprise> queryBuilder = entreprise_data.queryBuilder();
        queryBuilder.join(Visite.class, VisiteDao.Properties.ID_Entreprise)
                .where(VisiteDao.Properties.Id.eq(ID_Visite));
        List<Entreprise> list_E = queryBuilder.list();
        Log.i("Deletevisit","list_E.size()  "+list_E.size());
        return (list_E.size() < 2 );
    }

    private Visite getVisite(long ID_Visite, List<Visite> list_visite) {
        Visite result = null;
        if (list_visite != null){
            for (Visite V : list_visite){
                if (V.getId() == ID_Visite){
                    result = V;
                }
            }
        }
        return result;
    }

    private List<Visite> getListVisite(Entreprise entreprise) {
        return entreprise.getVisiteList();
    }

    private Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
       DaoSession daoSession = ((AppController) ((Activity)context).getApplication()).getDaoSession();
       // DaoSession daoSession =DAOSESSION;

        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }

    //set name function
    public String setNameVisite(String nom)
    {
        String name=nom.substring(15,nom.length());
        if(name.length()>15)
        {
            name=name.substring(0,14);
        }
        return name;
    }
}
