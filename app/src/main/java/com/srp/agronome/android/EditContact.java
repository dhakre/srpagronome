package com.srp.agronome.android;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;

import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.srp.agronome.android.db.*;
import com.srp.agronome.android.db.Contact;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Contact_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;

import static com.srp.agronome.android.MainLogin.Nom_Contact;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;
import static com.srp.agronome.android.MainLogin.Prenom_Contact;

/**
 * Created by Vincent on 31/07/2017
 */

public class EditContact extends AppCompatActivity {

    public static long EDIT_CONTACT_ID;

    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    private Spinner Spinner_Entreprise = null;
    private Spinner Spinner_Metier = null;
    private Spinner Spinner_Categorie = null;
    private Spinner Spinner_Sociabilite = null;
    private ImageView Contact_Photo = null;

    private EditText Last_Name = null;
    private EditText First_Name = null;

    static TextView TextDate = null;
    static Date Birth_Date = null;

    private RadioGroup Sex = null;
    private EditText StreetAddress1 = null;
    private EditText StreetAddress2 = null;
    private EditText City = null;
    private EditText State = null;
    private EditText PostalCode = null;
    private EditText Country = null;
    private ImageView Geolocalisation_Adresse_Contact = null;
    private EditText PhoneNumber1 = null;
    private EditText PhoneNumber2 = null;
    private EditText Mail1 = null;
    private EditText Remarques = null;

    /**
     * Image bouton pour valider la saisie
     */
    private ImageButton imgBtnValidate = null;
    private Button BtnValidate = null;

    /**
     * Image bouton pour annuler
     */
    private ImageButton imgBtnHome = null;

    private static final int CAMERA_PIC_REQUEST = 2;
    private static final int GALLERY_PICK = 1;
    private static final int PICK_CROP = 3;

    private double latitude_adresse_personnnelle = 0;
    private double longitude_adresse_personnnelle = 0;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);

        FindWidgetViewbyId(); //Associate Widgets
        AddListenerEvent(); //Add Widgets Event

        CreateListIntoDatabase();
        LoadSpinnerCategorie();
        LoadSpinnerEntreprise();
        LoadSpinnerSociabilite();
        LoadSpinnerMetier();

        LoadContactIntoDatabase(ID_Entreprise_Selected, ID_Contact_Selected);


        BtnValidate = (Button) findViewById(R.id.btnvalidate);
        BtnValidate.setOnClickListener(ButtonValidateHandler);

        imgBtnValidate = (ImageButton) findViewById(R.id.btn_ajoutContact);
        imgBtnValidate.setOnClickListener(ButtonValidateHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menu_annuaire);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);
    }


    View.OnClickListener TextViewHandler = new View.OnClickListener() {
        public void onClick(View v) {
            showDatePickerDialog(v);
        }
    };

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new EditContact.DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Date affichée à l'ouverture de la boite de dialogue
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR) - 30;  //
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Crée une nouvelle instance et la retourne
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (month <= 8) {
                TextDate.setText(day + "/0" + (month + 1) + "/" + year);
                Birth_Date = Create_date(year, month, day);

            } else {
                TextDate.setText(day + "/" + (month + 1) + "/" + year);
                Birth_Date = Create_date(year, month, day);
            }
        }
    }

    /**
     * Retourne une date.
     *
     * @param year  Année
     * @param month Mois
     * @param day   Jour
     * @return Retourne la date sous le format "Date"
     */
    public static Date Create_date(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Retourne la date et l'heure actuelle
     *
     * @return Retourne la date et l'heure actuelle sous le format Date
     */

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    private void FindWidgetViewbyId() {
        Contact_Photo = (ImageView) findViewById(R.id.contact_picture);

        Last_Name = (EditText) findViewById(R.id.Contact_LastName);
        First_Name = (EditText) findViewById(R.id.Contact_FirstName);
        Sex = (RadioGroup) findViewById(R.id.CMaleOrFemale);
        TextDate = (TextView) findViewById(R.id.CBirthday);
        StreetAddress1 = (EditText) findViewById(R.id.CAddress1);
        StreetAddress2 = (EditText) findViewById(R.id.CAddress2);
        City = (EditText) findViewById(R.id.CCity);
        State = (EditText) findViewById(R.id.CState);
        PostalCode = (EditText) findViewById(R.id.CPostalCode);
        Country = (EditText) findViewById(R.id.CCountry);
        Geolocalisation_Adresse_Contact = (ImageView) findViewById(R.id.BtnGeolocalisation_AdresseContact);
        PhoneNumber1 = (EditText) findViewById(R.id.CPhoneNumber1);
        PhoneNumber2 = (EditText) findViewById(R.id.CPhoneNumber2);
        Mail1 = (EditText) findViewById(R.id.CMail1);
        Remarques = (EditText) findViewById(R.id.remarques_id);

        Spinner_Entreprise = (Spinner) findViewById(R.id.Spinner_Organization);
        Spinner_Metier = (Spinner) findViewById(R.id.Spinner_Metier);
        Spinner_Categorie = (Spinner) findViewById(R.id.Spinner_Categorie);
        Spinner_Sociabilite = (Spinner) findViewById(R.id.Spinner_Sociabilite);
    }


    private void AddListenerEvent() {
        Contact_Photo.setOnClickListener(PhotoHandler);
        TextDate.setOnClickListener(TextViewHandler);
        Geolocalisation_Adresse_Contact.setOnClickListener(Handler_Geolocalisation_Adresse_Contact);
    }

    View.OnClickListener Handler_Geolocalisation_Adresse_Contact = new View.OnClickListener() {
        public void onClick(View v) {
            // create class object
            GPSTracker gps = new GPSTracker(EditContact.this);
            // check if GPS enabled
            if (gps.canGetLocation()) {
                latitude_adresse_personnnelle = gps.getLatitude();
                longitude_adresse_personnnelle = gps.getLongitude();
                //Display Values
                Toast.makeText(getApplicationContext(), getString(R.string.TitleLocationFound) + "\n" +
                                getString(R.string.TextLatitude) + " " + latitude_adresse_personnnelle + "\n" +
                                getString(R.string.TextLongitude) + " " + longitude_adresse_personnnelle
                        , Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    };


    //Event Clic on Photo :
    View.OnClickListener PhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Selection_Photo_Choice();
        }
    };

    //Afficher une boîte de dialogue contenant une liste de choix pour affecter une photo
    public void Selection_Photo_Choice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditContact.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionPhoto, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Prendre une photo
                        TakePicturefromCamera();
                        break;
                    case 1: //Choisir une photo depuis la galerie
                        pickImagefromGallery();
                        break;
                    case 2: //Rogner la photo affichée
                        //Verifie si il y a une photo
                        if (Contact_Photo.getDrawable().getConstantState() != EditContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            CropImage();
                        }
                        break;
                    case 3: //Supprimer la photo -> mettre l'image de base à la place
                        Contact_Photo.setImageResource(R.mipmap.ic_add_photo);
                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }


    //Pick a photo from Gallery
    public void pickImagefromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICK);
    }


    //Take a photo from Camera
    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
        File file = new File(myDir, FileName);
        return file;
    }

    //Permet de retourner les 3 premiers caracteres d'un texte
    private String getSubString(String chaine) {
        String result = "";
        if (chaine.length() >= 3) {
            result = chaine.substring(0, 3);
        }
        if (chaine.length() == 2) {
            result = chaine.substring(0, 2);
        }
        if (chaine.length() == 1) {
            result = chaine.substring(0, 1);
        }
        return result;
    }

    private String getPhotoContactNameHQ(String compte, String entreprise, String nom, String prenom) {
        return getSubString(compte) + "_" + entreprise + "_C_" + getSubString(nom) + "_" + getSubString(prenom) + "_HQ.jpg";
    }

    private String getPhotoContactNameLQ(String compte, String entreprise, String nom, String prenom) {
        return getSubString(compte) + "_" + entreprise + "_C_" + getSubString(nom) + "_" + getSubString(prenom) + "_LQ.jpg";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == GALLERY_PICK) { //Depuis la galerie
            Uri uri = data.getData();
            Bitmap Picture_Load;
            try {
                Picture_Load = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                Picture_Load = rotateImageIfRequired(Picture_Load, uri, true); //Rotation si necessaire
                SaveImage(Picture_Load, getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
                Picture_Load = getResizedBitmap(Picture_Load, 256, 256);
                Contact_Photo.setImageBitmap(Picture_Load);
                SaveImage(Picture_Load, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_PIC_REQUEST) { //Depuis la caméra : pas de bitmap en sortie
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
            File file = new File(myDir, getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
            Uri uri = Uri.fromFile(file);
            Bitmap newPicture;
            try {
                newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
                newPicture = rotateImageIfRequired(newPicture, uri, false); //Rotation si necessaire
                SaveImage(newPicture, getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
                newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
                Contact_Photo.setImageBitmap(newPicture);
                SaveImage(newPicture, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_CROP) {
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
            File file = new File(myDir, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
            Uri URI = Uri.fromFile(file);
            Bitmap Picture_Crop;
            try {
                Picture_Crop = MediaStore.Images.Media.getBitmap(getContentResolver(), URI);
                Contact_Photo.setImageBitmap(Picture_Crop);
                SaveImage(Picture_Crop, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Permet de rogner ou recadrer une image
    private void CropImage() {
        try {
            //Acceder à la photo à rogner
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
            File photoFile = new File(myDir, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
            Uri photoURI = FileProvider.getUriForFile(this,
                    "com.srp.android.fileprovider",
                    photoFile);
            getApplicationContext().grantUriPermission("com.android.camera", photoURI,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //Paramètres de l'Intent Crop
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //Android N need set permission to uri otherwise system camera don't has permission to access file wait crop
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // indicate image type and Uri
            cropIntent.setDataAndType(photoURI, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PICK_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
        }
    }


    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
        File file = new File(myDir, FileName);
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Get the path of the directory : Intern or SD path
    private File getDirectoryPath() {
        SharedPreferences SP = getSharedPreferences("PrefValues", 0); // 0 = Private Mode
        String location = SP.getString("LocationStorage", null);
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (location.equals("Intern")) {
            return Dirs[0];
        } else {
            return Dirs[1];
        }
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage, boolean PickingGallery) throws IOException {

        InputStream input = getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else if (PickingGallery) {
            ei = new ExifInterface(getPath(selectedImage));
        } else {
            ei = new ExifInterface(selectedImage.getPath());
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    //Get Path = Pick up a picture in Gallery = getRealPath
    private String getPath(Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    //Rotate an image with angle in degree
    private Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        Log.i("Info Rotation", "Rotation de l image de : " + degree);
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }


    //Méthode pour charger une image depuis le téléphone dans l'application.
    private void loadImageFromStorage(String FileName) {
        try {
            File f = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts/" + FileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            Contact_Photo.setImageBitmap(b);
            Log.i("Load Bitmap", "L'image définie.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("Load Bitmap", "L'image est introuvable");
        }
    }

    public static boolean checkImageResource(Context ctx, ImageView imageView, int imageResource) {
        boolean result = false;
        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }
            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }
        return result;
    }

    //Copie un fichier source vers un fichier de destination
    public void CopyAndDeleteFileCache(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = new FileInputStream(sourceFile).getChannel();
        FileChannel destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size()); //Copie le fichier
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
        sourceFile.delete(); //Supprime le fichier une fois qu'il a ete copié
    }


    //Methode qui permet de créer les listes de choix dans la base de données.
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Categorie_ContactDao categorie_contact_data = daoSession.getCategorie_ContactDao();
        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

        List<Categorie_Contact> List_Categorie_Contact = categorie_contact_data.loadAll();
        if (List_Categorie_Contact.size() == 0) {

            Categorie_Contact Categorie_Contact_input_1 = new Categorie_Contact();
            Categorie_Contact_input_1.setNom("Prospect");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_1);

            Categorie_Contact Categorie_Contact_input_2 = new Categorie_Contact();
            Categorie_Contact_input_2.setNom("Premium");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_2);

            Categorie_Contact Categorie_Contact_input_3 = new Categorie_Contact();
            Categorie_Contact_input_3.setNom("Volatile");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_3);

            Categorie_Contact Categorie_Contact_input_4 = new Categorie_Contact();
            Categorie_Contact_input_4.setNom("Expert");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_4);

            Categorie_Contact Categorie_Contact_input_5 = new Categorie_Contact();
            Categorie_Contact_input_5.setNom("Simple");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_5);

            Categorie_Contact Categorie_Contact_input_6 = new Categorie_Contact();
            Categorie_Contact_input_6.setNom("Non intéressé");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_6);
        }

        List<Sociabilite> List_Sociabilite = sociabilite_data.loadAll();
        if (List_Sociabilite.size() == 0) {

            Sociabilite Sociabilite_input_1 = new Sociabilite();
            Sociabilite_input_1.setNom("Accueillant");
            sociabilite_data.insertOrReplace(Sociabilite_input_1);

            Sociabilite Sociabilite_input_2 = new Sociabilite();
            Sociabilite_input_2.setNom("Méfiant");
            sociabilite_data.insertOrReplace(Sociabilite_input_2);

            Sociabilite Sociabilite_input_3 = new Sociabilite();
            Sociabilite_input_3.setNom("Réfractaire");
            sociabilite_data.insertOrReplace(Sociabilite_input_3);

            Sociabilite Sociabilite_input_4 = new Sociabilite();
            Sociabilite_input_4.setNom("Antipathique");
            sociabilite_data.insertOrReplace(Sociabilite_input_4);

            Sociabilite Sociabilite_input_5 = new Sociabilite();
            Sociabilite_input_5.setNom("Sympathique");
            sociabilite_data.insertOrReplace(Sociabilite_input_5);

        }

    }


    //Charger le spinner Metier
    private void LoadSpinnerMetier() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        MetierDao metier_data = daoSession.getMetierDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorHint));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter.add(getString(R.string.TextAnotherMetier)); //Text Add a new structure sociale
        //Charger les éléments de la liste
        List<Metier> List_Metier = metier_data.loadAll();
        if (List_Metier != null) {
            for (Metier M : List_Metier) {
                adapter.add(M.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(getString(R.string.TextMetier)); //This is the text that will be displayed as hint.
        Spinner_Metier.setAdapter(adapter);
        Spinner_Metier.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Metier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {
                if (position == 0) { //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(getString(R.string.TextValidateMetier), getString(R.string.TextInputMetier), 3);
                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    //Charger le spinner Categorie Contact
    private void LoadSpinnerCategorie() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Categorie_ContactDao Categorie_Contact_data = daoSession.getCategorie_ContactDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorHint));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherCategorie)); //Text Add a new structure sociale
        //Charger les éléments de la liste
        List<Categorie_Contact> List_Categorie = Categorie_Contact_data.loadAll();
        if (List_Categorie != null) {
            for (Categorie_Contact CC : List_Categorie) {
                adapter.add(CC.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextCategorie)); //This is the text that will be displayed as hint.
        Spinner_Categorie.setAdapter(adapter);
        Spinner_Categorie.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Categorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {
                if (position == 0) { //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(getString(R.string.TextValidateCategorie), getString(R.string.TextInputCategorie), 1);
                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    //Charger le spinner Sociabilité
    private void LoadSpinnerSociabilite() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SociabiliteDao Sociabilite_data = daoSession.getSociabiliteDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorHint));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherSociabilite)); //Text Add a new structure sociale
        //Charger les éléments de la liste
        List<Sociabilite> List_Sociabilite = Sociabilite_data.loadAll();
        if (List_Sociabilite != null) {
            for (Sociabilite S : List_Sociabilite) {
                adapter.add(S.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextSociabilite)); //This is the text that will be displayed as hint.
        Spinner_Sociabilite.setAdapter(adapter);
        Spinner_Sociabilite.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Sociabilite.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {
                if (position == 0) { //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(getString(R.string.TextValidateSociabilite), getString(R.string.TextInputSociabilite), 2);
                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    //Charger le spinner Entreprise
    private void LoadSpinnerEntreprise() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorHint));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    adapter.add(e.getRaison_sociale());
                }
            }
        }

        adapter.add(getString(R.string.TextSpinner)); //This is the text that will be displayed as hint.
        Spinner_Entreprise.setAdapter(adapter);
        Spinner_Entreprise.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Entreprise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {

            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditContact.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1: //Code Categorie Contact
                        AddCategorieContact(NewValueList);
                        break;
                    case 2: //Code Sociabilité
                        AddSociabilite(NewValueList);
                        break;
                    case 3: //Code Metier
                        AddMetier(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Incrémenter la table Categorie Contact
    private void AddCategorieContact(String Nom) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
        Categorie_Contact categorie_input = new Categorie_Contact();
        categorie_input.setNom(Nom);
        categorie_data.insertOrReplace(categorie_input);
        LoadSpinnerCategorie();
    }

    private void AddMetier(String Nom) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        MetierDao metier_data = daoSession.getMetierDao();
        Metier metier_input = new Metier();
        metier_input.setNom(Nom);
        metier_data.insertOrReplace(metier_input);
        LoadSpinnerMetier();
    }

    //Incrémenter la table Sociabilité
    private void AddSociabilite(String Nom) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();
        Sociabilite sociabilite_input = new Sociabilite();
        sociabilite_input.setNom(Nom);
        sociabilite_data.insertOrReplace(sociabilite_input);
        LoadSpinnerSociabilite();
    }


    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {

                String email1 = Mail1.getText().toString().trim();
                String number1 = PhoneNumber1.getText().toString().trim();
                String number2 = PhoneNumber2.getText().toString().trim();


                String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                String MobilePattern = "";
                String CP_Pattern = "";

                //Pattern selon Pays de selection :
                sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
                String Format_Pays = sharedpreferences.getString("FormatCode", "FR");
                switch (Format_Pays) {
                    case "FR": {
                        MobilePattern = "\"(0|\\\\+33|0033)[1-9][0-9]{8}\"";
                        CP_Pattern = "^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$";
                        break;
                    }
                    case "US": {
                        MobilePattern = "^[0-9]{3,3}[-]{1,1}[0-9]{3,3}[-]{1,1}[0-9]{4,4}$";
                        CP_Pattern = "^[A-Z]{1,2}([0-9]{1,2}|[0-9]{1,1}[A-Z]{1,1})( |)[0-9]{1,1}[A-Z]{2,2}$";
                        break;
                    }
                }

                MobilePattern = "[0-9]{10}"; //Ancien a supprimer si les tests sont bons


                if (email1.matches(emailPattern)) {
                    if (number1.matches(MobilePattern) & (TextUtils.isEmpty(PhoneNumber2.getText().toString())) || (number1.matches(MobilePattern) & (number2.matches(MobilePattern)))) {

                        // Validation du Sexe
                        String Text_Sex;
                        if (Sex.getCheckedRadioButtonId() == R.id.Cradiobtn_male) {
                            Text_Sex = getString(R.string.Radiobtn1);
                        } else {
                            Text_Sex = getString(R.string.Radiobtn2);
                        }

                        Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm), getString(R.string.textConfirm) + "\n" +
                                "\n" + getString(R.string.raison) + " : " + Spinner_Entreprise.getSelectedItem().toString() + "\n" +
                                "\n" + getString(R.string.TextLastName) + " : " + Last_Name.getText().toString() +
                                "\n" + getString(R.string.TextFirstName) + " : " + First_Name.getText().toString() + "\n" +
                                "\n" + getString(R.string.TextValidateMetier) + " : " + Spinner_Metier.getSelectedItem().toString() +
                                "\n" + getString(R.string.TextValidateCategorie) + " : " + Spinner_Categorie.getSelectedItem().toString() +
                                "\n" + getString(R.string.TextValidateSociabilite) + " : " + Spinner_Sociabilite.getSelectedItem().toString() + "\n" +
                                "\n" + getString(R.string.datenaissance) + " : " + TextDate.getText().toString() +
                                "\n" + getString(R.string.TextSex) + " : " + Text_Sex + "\n" +
                                "\n" + getString(R.string.TextAddress) + " : " + StreetAddress1.getText().toString() +
                                "\n" + getString(R.string.TextAddress2) + " : " + StreetAddress2.getText().toString() +
                                "\n" + getString(R.string.TextCity) + " : " + City.getText().toString() +
                                "\n" + getString(R.string.TextState) + " : " + State.getText().toString() +
                                "\n" + getString(R.string.TextPostalCode) + " : " + PostalCode.getText().toString() +
                                "\n" + getString(R.string.TextCountry) + " : " + Country.getText().toString() + "\n" +
                                "\n" + getString(R.string.TextPhoneNumber1) + " : " + PhoneNumber1.getText().toString() +
                                "\n" + getString(R.string.TextPhoneNumber2) + " : " + PhoneNumber2.getText().toString() + "\n" +
                                "\n" + getString(R.string.TextSection4) + " : " + Mail1.getText().toString() + "\n" +
                                "\n" + getString(R.string.rmk) + " : " + Remarques.getText().toString());


                    } else {
                        Show_Dialog(getString(R.string.warntitle), getString(R.string.warnphone));
                    }

                } else {
                    Show_Dialog(getString(R.string.warntitle), getString(R.string.warnemail));
                }


            } else {
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();
            }

        }
    };

    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField() {
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewContact);
        int position = 0;

        if (TextUtils.isEmpty(Mail1.getText().toString())) {
            position = Mail1.getTop();
        }
        if (TextUtils.isEmpty(PhoneNumber1.getText().toString())) {
            position = PhoneNumber1.getTop();
        }
        if (TextUtils.isEmpty(Country.getText().toString())) {
            position = Country.getTop();
        }
        if (TextUtils.isEmpty(PostalCode.getText().toString())) {
            position = PostalCode.getTop();
        }
        if (TextUtils.isEmpty(City.getText().toString())) {
            position = City.getTop();
        }
        if (TextUtils.isEmpty(StreetAddress1.getText().toString())) {
            position = StreetAddress1.getTop();
        }
        if (Spinner_Sociabilite.getSelectedItem().toString().equals(getString(R.string.TextSociabilite))) {
            position = Spinner_Sociabilite.getTop();
        }
        if (Spinner_Categorie.getSelectedItem().toString().equals(getString(R.string.TextCategorie))) {
            position = Spinner_Categorie.getTop();
        }
        if (Spinner_Metier.getSelectedItem().toString().equals(getString(R.string.TextMetier))) {
            position = Spinner_Metier.getTop();
        }
        if (TextUtils.isEmpty(First_Name.getText().toString())) {
            position = First_Name.getTop();
        }
        if (TextUtils.isEmpty(Last_Name.getText().toString())) {
            position = Last_Name.getTop();
        }
        if (Spinner_Entreprise.getSelectedItem().toString().equals(getString(R.string.TextSpinner))) {
            position = Spinner_Entreprise.getTop();
        }

        final int Position_Y = position;
        final int Position_X = 0;
        sc.post(new Runnable() {
            public void run() {
                sc.scrollTo(Position_X, Position_Y); // these are your x and y coordinates
            }
        });
    }

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditContact.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }


    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(EditContact.this, MenuAgriculteur.class);
            finish();
            startActivity(intent);
        }
    };

    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditContact.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                EditContactIntoDatabase(ID_Entreprise_Selected, ID_Contact_Selected);
                Intent intent = new Intent(EditContact.this, MenuAgriculteur.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(EditContact.this, MenuAgriculteur.class);
            finish();
            startActivity(intent);

        }
        return true;
    }


    /**
     * Verifie si les champs sont renseignées
     *
     * @return Retourne un booleen
     */
    private boolean CheckInput() {
        boolean b = true;

        if (TextUtils.isEmpty(Last_Name.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(First_Name.getText().toString())) {
            b = false;
        }
        if (TextDate.getText().toString().equals(getString(R.string.DefaultBirthday))) {
            b = false;
        }
        if (TextUtils.isEmpty(StreetAddress1.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(City.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(PostalCode.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Country.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(PhoneNumber1.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Mail1.getText().toString())) {
            b = false;
        }
        if (Spinner_Entreprise.getSelectedItem().toString().equals(getString(R.string.TextSpinner))) {
            b = false;
        }
        if (Spinner_Categorie.getSelectedItem().toString().equals(getString(R.string.TextCategorie))) {
            b = false;
        }
        if (Spinner_Metier.getSelectedItem().toString().equals(getString(R.string.TextMetier))) {
            b = false;
        }
        if (Spinner_Sociabilite.getSelectedItem().toString().equals(getString(R.string.TextSociabilite))) {
            b = false;
        }
        return b;
    }


    //Charge les données de la base de données et les places sont dans les editTexts
    private void LoadContactIntoDatabase(long Id_Entreprise, long Id_Contact) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<com.srp.agronome.android.db.Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        //Charger les donnees entreprise

                        //Spinner Entreprise
                        SpinnerAdapter TextAdapterEntreprise = Spinner_Entreprise.getAdapter(); //Charge la liste des valeurs du spinner
                        for (int i = 0; i < TextAdapterEntreprise.getCount(); i++) {
                            if (TextAdapterEntreprise.getItem(i).toString().equals(e.getRaison_sociale())) {
                                Spinner_Entreprise.setSelection(i);
                            }
                        }

                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                            if (ct.getId() == Id_Contact) {
                                //Charger les donnes du contact
                                if (ct.getIdentite().getNom() != null) {
                                    Last_Name.setText(ct.getIdentite().getNom());
                                }
                                if (ct.getIdentite().getPrenom() != null) {
                                    First_Name.setText(ct.getIdentite().getPrenom());
                                }

                                //Spinner Categorie
                                if (ct.getCategorie_Contact() != null) {
                                    SpinnerAdapter TextAdapterCategorie = Spinner_Categorie.getAdapter();
                                    for (int i = 0; i < TextAdapterCategorie.getCount(); i++) {
                                        if (TextAdapterCategorie.getItem(i).toString().equals(ct.getCategorie_Contact().getNom())) {
                                            Spinner_Categorie.setSelection(i);
                                        }
                                    }
                                }

                                //Spinner Metier
                                if (ct.getMetier() != null) {
                                    SpinnerAdapter TextAdapterMetier = Spinner_Metier.getAdapter();
                                    for (int i = 0; i < TextAdapterMetier.getCount(); i++) {
                                        if (TextAdapterMetier.getItem(i).toString().equals(ct.getMetier().getNom())) {
                                            Spinner_Metier.setSelection(i);
                                        }
                                    }
                                }

                                //Spinner Sociabilite
                                if (ct.getSociabilite() != null) {
                                    SpinnerAdapter TextAdapterSociabilite = Spinner_Sociabilite.getAdapter();
                                    for (int i = 0; i < TextAdapterSociabilite.getCount(); i++) {
                                        if (TextAdapterSociabilite.getItem(i).toString().equals(ct.getSociabilite().getNom())) {
                                            Spinner_Sociabilite.setSelection(i);
                                        }
                                    }
                                }

                                //Sexe du contact :
                                if (ct.getIdentite().getSexe() != null) {
                                    if (ct.getIdentite().getSexe().getNom().equals(getString(R.string.Radiobtn1))) {
                                        Sex.check(R.id.Cradiobtn_male);
                                    } else {
                                        Sex.check(R.id.Cradiobtn_female);
                                    }
                                }

                                //Charger la date de naissance -> EditText formaté en date jour/mois/année
                                if (ct.getIdentite().getDate_naissance() != null) {
                                    Birth_Date = ct.getIdentite().getDate_naissance();
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(Birth_Date);
                                    if (cal.get(Calendar.MONTH) <= 8) {
                                        TextDate.setText(cal.get(Calendar.DAY_OF_MONTH) + "/0" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR));
                                    } else {
                                        TextDate.setText(cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR));
                                    }
                                }

                                if (ct.getIdentite().getAdresse().getAdresse_numero_rue() != null) {
                                    StreetAddress1.setText(ct.getIdentite().getAdresse().getAdresse_numero_rue());
                                }
                                if (ct.getIdentite().getAdresse().getComplement_adresse() != null) {
                                    StreetAddress2.setText(ct.getIdentite().getAdresse().getComplement_adresse());
                                }

                                if (ct.getIdentite().getAdresse().getCode_Postal_Ville() != null) {
                                    if (ct.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal() != null) {
                                        PostalCode.setText(ct.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal());
                                    }
                                    if (ct.getIdentite().getAdresse().getCode_Postal_Ville().getVille() != null) {
                                        City.setText(ct.getIdentite().getAdresse().getCode_Postal_Ville().getVille());
                                    }
                                }

                                if (ct.getIdentite().getAdresse().getRegion() != null) {
                                    State.setText(ct.getIdentite().getAdresse().getRegion().getNom());
                                }

                                if (ct.getIdentite().getAdresse().getPays() != null) {
                                    Country.setText(ct.getIdentite().getAdresse().getPays().getNom());
                                }

                                if (ct.getIdentite().getTelephone_principal() != null) {
                                    PhoneNumber1.setText(ct.getIdentite().getTelephone_principal());
                                }

                                if (ct.getIdentite().getTelephone_secondaire() != null) {
                                    PhoneNumber2.setText(ct.getIdentite().getTelephone_secondaire());
                                }

                                if (ct.getIdentite().getAdresse_email() != null) {
                                    Mail1.setText(ct.getIdentite().getAdresse_email());
                                }

                                if (ct.getRemarque_contact() != null) {
                                    Remarques.setText(ct.getRemarque_contact());
                                }

                                if (ct.getIdentite().getPhoto().equals("")) {
                                    Contact_Photo.setImageResource(R.mipmap.ic_add_photo);
                                } else {
                                    loadImageFromStorage(ct.getIdentite().getPhoto());
                                }

                            }
                        }
                    }
                }
            }
        }
    }

    private long getIdEntreprise(String Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        long result = 0;
        List<Compte> list_account = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_account != null) {
            for (Compte c : list_account) {
                List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                for (Entreprise e : List_Company) {
                    if (e.getRaison_sociale().equals(Entreprise)) {
                        result = e.getId();
                    }
                }
            }
        }
        return result;
    }


    private void EditContactIntoDatabase(long Id_Entreprise, long Id_Contact) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        ContactDao contact_data = daoSession.getContactDao();
        Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
        MetierDao metier_data = daoSession.getMetierDao();
        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        IdentiteDao identite_data = daoSession.getIdentiteDao();
        SexeDao sexe_data = daoSession.getSexeDao();
        AdresseDao adresse_data = daoSession.getAdresseDao();
        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
        RegionDao region_data = daoSession.getRegionDao();
        PaysDao pays_data = daoSession.getPaysDao();
        GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();

        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<com.srp.agronome.android.db.Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        //Editer les donnees de l'entreprises

                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                            if (ct.getId() == Id_Contact) {
                                //Editer les donnes du contact

                                //edit id
                                EDIT_CONTACT_ID=Id_Contact;


                                ct.getIdentite().setNom(Last_Name.getText().toString().trim());
                                ct.getIdentite().setPrenom(First_Name.getText().toString().trim());
                                ct.setID_Agronome_Modification((int) ID_Compte_Selected);
                                ct.setDate_modification(Today_Date());


                                //Edit Spinner Categorie
                                List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                        .where(Categorie_ContactDao.Properties.Nom.eq(Spinner_Categorie.getSelectedItem().toString()))
                                        .list();
                                if (List_Categorie != null) {
                                    for (Categorie_Contact CC : List_Categorie) {
                                        ct.setCategorie_Contact(CC);
                                        ct.setID_Categorie_Contact(CC.getId());
                                    }
                                }

                                //Edit Spinner Metier
                                List<Metier> List_Metier = metier_data.queryBuilder()
                                        .where(MetierDao.Properties.Nom.eq(Spinner_Metier.getSelectedItem().toString()))
                                        .list();
                                if (List_Metier != null) {
                                    for (Metier M : List_Metier) {
                                        ct.setMetier(M);
                                        ct.setID_Metier(M.getId());
                                    }
                                }

                                //Edit Spinner Sociabilite
                                List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                        .where(SociabiliteDao.Properties.Nom.eq(Spinner_Sociabilite.getSelectedItem().toString()))
                                        .list();
                                if (List_Sociabilite != null) {
                                    for (Sociabilite S : List_Sociabilite) {
                                        ct.setSociabilite(S);
                                        ct.setID_Sociabilite(S.getId());
                                    }
                                }

                                //Edit or Create Sexe
                                if (ct.getIdentite().getSexe() != null) {
                                    if (Sex.getCheckedRadioButtonId() == R.id.Cradiobtn_male) {
                                        ct.getIdentite().getSexe().setNom(getString(R.string.Radiobtn1));
                                    } else {
                                        ct.getIdentite().getSexe().setNom(getString(R.string.Radiobtn2));
                                    }
                                } else {
                                    Sexe sexe_input = new Sexe();
                                    if (Sex.getCheckedRadioButtonId() == R.id.Cradiobtn_male) {
                                        sexe_input.setNom(getString(R.string.Radiobtn1));
                                    } else {
                                        sexe_input.setNom(getString(R.string.Radiobtn2));
                                    }
                                    sexe_data.insertOrReplace(sexe_input);
                                    ct.getIdentite().setSexe(sexe_input);
                                    ct.getIdentite().setID_Sexe(sexe_input.getId());
                                }

                                //Edit date de naissance
                                ct.getIdentite().setDate_naissance(Birth_Date);

                                ct.getIdentite().getAdresse().setAdresse_numero_rue(StreetAddress1.getText().toString().trim());
                                ct.getIdentite().getAdresse().setComplement_adresse(StreetAddress2.getText().toString().trim());
                                ct.getIdentite().getAdresse().setID_Agronome_Modification((int) ID_Compte_Selected);
                                ct.getIdentite().getAdresse().setDate_modification(Today_Date());
                                ct.getIdentite().getAdresse().getCode_Postal_Ville().setCode_postal(PostalCode.getText().toString().trim());
                                ct.getIdentite().getAdresse().getCode_Postal_Ville().setVille(City.getText().toString().trim());

                                //Edit or create Region
                                if (ct.getIdentite().getAdresse().getRegion() != null) {
                                    ct.getIdentite().getAdresse().getRegion().setNom(State.getText().toString().trim());
                                } else {
                                    Region region_personnelle_input = new Region();
                                    region_personnelle_input.setNom(State.getText().toString().trim());
                                    region_data.insertOrReplace(region_personnelle_input);
                                    ct.getIdentite().getAdresse().setRegion(region_personnelle_input);
                                    ct.getIdentite().getAdresse().setID_Region(region_personnelle_input.getId());
                                }

                                //Edit or create Geolocalisation
                                if ((latitude_adresse_personnnelle != 0) & (longitude_adresse_personnnelle != 0)) {
                                    if (ct.getIdentite().getAdresse().getGeolocalisation() != null) {
                                        //Code edit Geolocalisation
                                        ct.getIdentite().getAdresse().getGeolocalisation().setLatitude(latitude_adresse_personnnelle);
                                        ct.getIdentite().getAdresse().getGeolocalisation().setLongitude(longitude_adresse_personnnelle);
                                        geolocalisation_data.update(ct.getIdentite().getAdresse().getGeolocalisation());
                                    } else {
                                        //Code addNew Geolocalisation
                                        Geolocalisation localisation_adresse_input = new Geolocalisation();
                                        localisation_adresse_input.setLatitude(latitude_adresse_personnnelle);
                                        localisation_adresse_input.setLongitude(longitude_adresse_personnnelle);
                                        geolocalisation_data.insertOrReplace(localisation_adresse_input);
                                        ct.getIdentite().getAdresse().setGeolocalisation(localisation_adresse_input);
                                        ct.getIdentite().getAdresse().setID_Geolocalisation(localisation_adresse_input.getId());
                                    }
                                }


                                //Edit Pays
                                ct.getIdentite().getAdresse().getPays().setNom(Country.getText().toString().trim());

                                //Edit Telephone - Mail - Remarques
                                ct.getIdentite().setTelephone_principal(PhoneNumber1.getText().toString().trim());
                                ct.getIdentite().setTelephone_secondaire(PhoneNumber2.getText().toString().trim());
                                ct.getIdentite().setAdresse_email(Mail1.getText().toString().trim());
                                ct.setRemarque_contact(Remarques.getText().toString().trim());

                                //Photo Edit
                                if (checkImageResource(this, Contact_Photo, R.mipmap.ic_add_photo)) {
                                    ct.getIdentite().setPhoto("");
                                } else {
                                    ct.getIdentite().setPhoto(getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
                                }

                                //Changement d'entreprise du contact
                                if (!(e.getRaison_sociale().equals(Spinner_Entreprise.getSelectedItem().toString()))) {
                                    List<Compte> update_compte = compte_dao.queryBuilder()
                                            .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                                            .list();
                                    if (update_compte != null) {
                                        for (Compte compte : update_compte) {
                                            List<Entreprise> List_Company = compte.getEntrepriseList(); //get All Company
                                            for (Entreprise e2 : List_Company) {
                                                if (e2.getRaison_sociale().equals(Spinner_Entreprise.getSelectedItem().toString())) {
                                                    ct.setID_Entreprise(e2.getId());
                                                    e.resetContactList();
                                                    e2.resetContactList();
                                                    entreprise_data.update(e2);
                                                    compte_dao.update(compte);
                                                }
                                            }
                                        }
                                    }
                                }

                                //Changement du répertoire de la photo si Changement Entreprise
                                if (!(e.getRaison_sociale().equals(Spinner_Entreprise.getSelectedItem().toString()))) {
                                    //Photo basse qualité
                                    File myDir_Source1 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
                                    File photoFile_Source1 = new File(myDir_Source1, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));

                                    File myDir_Dest1 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                                    File photoFile_Dest1 = new File(myDir_Dest1, getPhotoContactNameLQ(Nom_Compte, Spinner_Entreprise.getSelectedItem().toString(), Nom_Contact, Prenom_Contact));
                                    try {
                                        CopyAndDeleteFileCache(photoFile_Source1, photoFile_Dest1);
                                    } catch (IOException exception) {
                                        exception.printStackTrace();
                                    }
                                    //Photo haute qualité
                                    File myDir_Source2 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
                                    File photoFile_Source2 = new File(myDir_Source2, getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));

                                    File myDir_Dest2 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                                    File photoFile_Dest2 = new File(myDir_Dest2, getPhotoContactNameHQ(Nom_Compte, Spinner_Entreprise.getSelectedItem().toString(), Nom_Contact, Prenom_Contact));
                                    try {
                                        CopyAndDeleteFileCache(photoFile_Source2, photoFile_Dest2);
                                    } catch (IOException exception) {
                                        exception.printStackTrace();
                                    }
                                    ct.getIdentite().setPhoto(getPhotoContactNameLQ(Nom_Compte, Spinner_Entreprise.getSelectedItem().toString(), Nom_Contact, Prenom_Contact));
                                }


                                //Update des tables : DAO
                                code_postal_ville_data.update(ct.getIdentite().getAdresse().getCode_Postal_Ville());
                                region_data.update(ct.getIdentite().getAdresse().getRegion());
                                pays_data.update(ct.getIdentite().getAdresse().getPays());
                                adresse_data.update(ct.getIdentite().getAdresse());
                                sexe_data.update(ct.getIdentite().getSexe());
                                identite_data.update(ct.getIdentite());
                                contact_data.update(ct);
                                entreprise_data.update(e);
                                compte_dao.update(c);

                                Nom_Contact = ct.getIdentite().getNom();
                                Prenom_Contact = ct.getIdentite().getPrenom();
                                Nom_Entreprise = Spinner_Entreprise.getSelectedItem().toString();

                                ID_Entreprise_Selected = getIdEntreprise(Spinner_Entreprise.getSelectedItem().toString());
                                ID_Contact_Selected = ct.getId();
                            }
                        }
                    }
                }
            }
        }
    }
}

