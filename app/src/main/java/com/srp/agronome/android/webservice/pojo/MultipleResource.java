package com.srp.agronome.android.webservice.pojo;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by soytouchrp on 04/12/2017.
 */

public class MultipleResource {


    @SerializedName("page")
    public Integer page;
    @SerializedName("per_page")
    public Integer perPage;
    @SerializedName("total")
    public Integer total;
    @SerializedName("total_pages")
    public Integer totalPages;
    @SerializedName("data")
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("ID_FOURNISSEUR")
        public Integer id;
        @SerializedName("message")
        public String name;
        @SerializedName("status_code")
        public Integer idAdresse;

    }
}