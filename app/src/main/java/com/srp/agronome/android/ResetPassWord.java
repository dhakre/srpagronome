package com.srp.agronome.android;

import android.content.Intent;
import android.os.Bundle;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Created by doucoure on 09/06/2017.
 */

public class ResetPassWord extends AppCompatActivity {

    private EditText Username;
    private Button ValidateButton;
    private Boolean checked = true;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_psw);
        Username = (EditText) findViewById(R.id.resetemail);
        ValidateButton = (Button) findViewById(R.id.send);
        ValidateButton.setOnClickListener(ValidateButtonHandler);


    }

    View.OnClickListener ValidateButtonHandler = new View.OnClickListener() {
        public void onClick(View v) {
            String id = Username.getText().toString();
            if (TextUtils.isEmpty(id)) {
                Toast.makeText(getApplicationContext(), getString(R.string.Champsvide), Toast.LENGTH_SHORT);

            } else

                // = db.checkUsername(Username.getText().toString());
                if (checked) {

                    Show_Dialog(getString(R.string.Confirmation), getString(R.string.confirmationenvoiemail));
                }
        }

    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(ResetPassWord.this, MainLogin.class);
            finish();
            startActivity(intent);

        }
        return true;
    }

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ResetPassWord.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

}
