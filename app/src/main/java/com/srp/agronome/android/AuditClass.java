package com.srp.agronome.android;

import java.util.Date;

/**
 * Created by sumitra on 10/19/2017.
 */

public class AuditClass {

    long Audit_ID;
    String date;
    String remarque;
   // Date date;

    public AuditClass(long id,String remarque,String year)
    {
        super();
        this.Audit_ID=id;
        this.date=year;  //audit_date
        this.remarque=remarque;

    }

    public void setAudit_ID(Long audit_ID) {
        Audit_ID = audit_ID;
    }

    public void setYear(String year) {
        this.date = year;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getYear() {

        return date;
    }

    public String getRemarque() {
        return remarque;
    }

    public Long getAudit_ID() {

        return Audit_ID;
    }
}
