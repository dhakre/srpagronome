package com.srp.agronome.android;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.srp.agronome.android.db.PARCELLE;

/**
 * Created by jitendra on 03-08-2017.
 */

public class MenuParcelle  extends AppCompatActivity {
    public static long Id_ilot_selected;
    public static long Id_Parcelle_selected;
    public static long  Id_suivi_producteur;
    public static long  Id_stockage_selected;


    private Button list_of_parcelle = null;
    private Button list_of_stockage = null;
    private Button Ajout_parcelle = null;
    private Button Ajout_stockage = null;

    private ImageButton imgBtnHome = null;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_parcelle);

        list_of_parcelle = (Button) findViewById(R.id.btn_list_parcelle);
        list_of_parcelle.setOnClickListener(Button_listparcelle_Handler);

        Ajout_parcelle = (Button) findViewById(R.id.btn_ajout_parcelle);
        Ajout_parcelle.setOnClickListener(Button_Ajoutparcelle_Handler);

        list_of_stockage = (Button) findViewById(R.id.btn_list_stockage);
        list_of_stockage.setOnClickListener(Button_liststockage_Handler);


        Ajout_stockage = (Button) findViewById(R.id.btn_ajout_stockage);
        Ajout_stockage.setOnClickListener(Button_AjoutStockage_Handler);


        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menu_parcelle);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

    }

    View.OnClickListener Button_listparcelle_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuParcelle.this, ListParcelle.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener Button_liststockage_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuParcelle.this, ListStockage.class);
            startActivity(intent);
            finish();
        }
    };

    //Back pressed button
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(MenuParcelle.this, MenuAgriculteur.class);
            finish();
            startActivity(intent);
        }
        return true;
    }

    View.OnClickListener Button_Ajoutparcelle_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuParcelle.this, Parcelle.class);
            startActivity(intent);
            finish();
        }
    };
    View.OnClickListener Button_AjoutStockage_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuParcelle.this, Stockage.class);
            startActivity(intent);
            finish();
        }
    };
    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuParcelle.this, MenuAgriculteur.class);
            finish();
            startActivity(intent);
        }
    };

}
