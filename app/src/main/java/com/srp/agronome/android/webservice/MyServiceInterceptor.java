package com.srp.agronome.android.webservice;

/**
 * Created by soytouchrp on 05/12/2017.
 */

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

import com.srp.agronome.android.webservice.pojo.DataAdresse;
import com.srp.agronome.android.webservice.pojo.DataUser;
import com.srp.agronome.android.webservice.pojo.FournisseurInterface;
import com.srp.agronome.android.webservice.pojo.FournisseurInterfaceComplexe;
import com.srp.agronome.android.webservice.pojo.Login;
import com.srp.agronome.android.webservice.pojo.RetroFit;
import com.srp.agronome.android.webservice.pojo.Token;
import com.srp.agronome.android.webservice.pojo.TokenInterface;
import com.srp.agronome.android.webservice.pojo.User;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class MyServiceInterceptor implements Interceptor {
    private String sessionToken;

    private static MyServiceInterceptor tokenInterceptor;


    private String tokenToUse = "";

    public static MyServiceInterceptor getInstance() {
        if (tokenInterceptor == null) {
            tokenInterceptor = new MyServiceInterceptor();
        }
        return tokenInterceptor;
    }

    @Inject
    private MyServiceInterceptor() {

//        System.out.println("TVS : interceptor : 1  ");
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        Request requestBuilder = request.newBuilder().header("Authorization", Service.KEY_AUTHENTIFICATION + " " + getTokenToUse())
                .method(request.method(), request.body())
                .build();
        System.out.println("TVS : interceptor : 2 : requestBuilder : "+requestBuilder.headers());

//        if (request.header(Service.KEY_AUTHENTIFICATION) == null) {
//            // needs credentials
//            if (sessionToken == null) {
//                throw new RuntimeException("Session token should be defined for auth apis");
//            } else {
////                requestBuilder.addHeader("Cookie", sessionToken);
//            }
//        }

        Response response = chain.proceed(requestBuilder);
//        System.out.println("TVS : interceptor : response : "+response.toString());

//        Log.d("MyApp", "Code : "+response.code());
        if (response.code() == 401) {
            // si token non valide, on demande un nouveau token
            // launch update of the token

//            OkHttpClient client = new OkHttpClient.Builder()
//                    .addInterceptor(MyLoginInterceptor.getInstance())
//                    .build();

            Retrofit retrofit = new Retrofit.Builder()
//                    .client(client)
                    .baseUrl(Service.REST2_ENDPOINT)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build();

            // Create an instance of our GitHub API interface.
            TokenInterface apiInterface = retrofit.create(TokenInterface.class);
            Login login = new Login();
            login.setEmail("test43@free.fr"); // email de l’authentification, ici en clair, mais en production il faudra le récupérer dynamiquement le login/password de l’utilisateur depuis l’endroit où on les a stocker

            login.setPassword("mlkjhhg"); // password de l’authentification
            Call<Token> call = apiInterface.getNewToken(login);
            retrofit2.Response<Token> responseToken = call.execute();
            System.out.println("TVS : interceptor : response login : " + responseToken.toString());
            Token newToken = responseToken.body();
            String token = newToken.getToken();
            String status = newToken.getStatus();
            System.out.println("TVS : interceptor : 2eme tentative pour token : " + token + " / status : " + status);
            if (status.equalsIgnoreCase("ok")) {
                setTokenToUse(token);

                // launch new attempt to retrieve the token
                Request requestBuilder2 = request.newBuilder().header("Authorization", Service.KEY_AUTHENTIFICATION + " " + getTokenToUse())
                        .method(request.method(), request.body())
                        .build();
                Response response2 = chain.proceed(requestBuilder2);
                System.out.println("TVS : interceptor : 2eme tentative pour token : " + response2.toString());
                return response2;

//        Log.d("MyApp", "Code : "+response.code());
            }


            return response;
        }
        return response;
    }

    public String getTokenToUse() {
        return tokenToUse;
    }

    public void setTokenToUse(String tokenToUse) {
        this.tokenToUse = tokenToUse;
    }

    public static void main(String... args) {
        boolean testGet = false;
        boolean testUpdate = false;
        boolean testInsert = false;
        boolean testDelete = false;

        boolean testGetComplexe = true;
        boolean testUpdateComplexe = true;
        boolean testInsertComplexe = false;
        boolean testDeleteComplexe = false;


        try {
            if (testGet == true) {
                // get fournisseur 392
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(MyServiceInterceptor.getInstance())
                        .build();

                Retrofit retrofit = new Retrofit.Builder()
                        .client(client)
                        .baseUrl(Service.REST2_ENDPOINT)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                FournisseurInterface apiInterface = retrofit.create(FournisseurInterface.class);
                Call<DataUser> call = apiInterface.doGetUserById(392);
                retrofit2.Response<DataUser> responseUser = call.execute();
                DataUser newUser = responseUser.body();
                System.out.println("TVS : get fournisseur : " + responseUser.toString());
                if (newUser != null) {
                    DataUser dataUser = newUser;
                    if (dataUser != null) {
//                    DataAdresse dataAdresse = newUser.getData().getAdresseSiege();
                        System.out.println("TVS : get fournisseur dataUser : " + dataUser.getNomFournisseur()); //+" / pays : "+dataAdresse.getPays());
                    } else {
                        System.out.println("TVS : get fournisseur dataUser : NULL ");
                    }
                    if (testUpdate == true) {

                        // update fournisseur
                        DataUser userToUpdate = newUser;
                        DataUser dataUserToUpdate = userToUpdate;
                        dataUserToUpdate.setNomFournisseur("test 67");
                        // userToUpdate.setData(dataUserToUpdate);
                        Call<DataUser> callUpdate = apiInterface.doUpdateUser(392, dataUserToUpdate);
                        retrofit2.Response<DataUser> responseUserToUpdate = callUpdate.execute();
//                User newUserUpdated = responseUserToUpdate.body();
//                System.out.println("TVS : get fournisseur updated : "+responseUserToUpdate.toString());
//                if (newUserUpdated != null && newUserUpdated.getData() != null){
//                    System.out.println("TVS : get name of fournisseur updated : "+newUserUpdated.getData().getNomFournisseur());
//                } else {
//                    System.out.println("TVS : get name of fournisseur updated : NULL "+responseUserToUpdate.raw());
//                }

                        // 2eme call pour vérifier
                        Call<DataUser> callAfterUpdate = apiInterface.doGetUserById(392);
                        retrofit2.Response<DataUser> responseUserAfterUpdate = callAfterUpdate.execute();
                        DataUser newUserAfterUpdate = responseUserAfterUpdate.body();
                        System.out.println("TVS : get fournisseur dataUserAfterUpdate : " + newUserAfterUpdate.getNomFournisseur());
                    }

                    if (testInsert == true) {
                        // create a user
                        DataUser userToinsert = new DataUser();
                        userToinsert.setNomFournisseur("test201");
                        userToinsert.setMailFournisseur("test201@gmail.com");
                        userToinsert.setTelFournisseur("0123456788");
                        Call<DataUser> callInsert = apiInterface.createUser(userToinsert);
                        retrofit2.Response<DataUser> responseUserInsert = callInsert.execute();
                        DataUser newUserAfterInsert = responseUserInsert.body();
                        System.out.println("TVS : get fournisseur dataUserAfter insert : " + responseUserInsert.raw());
                        if (newUserAfterInsert != null) {
                            System.out.println("TVS : get fournisseur dataUserAfter insert : " + newUserAfterInsert.getNomFournisseur() + " / id : " + newUserAfterInsert.getIdFournisseur());
                        }
                    }

                    if (testDelete == true) {
                        // create a user
                        Call<DataUser> callDelete = apiInterface.doDeleteUser(400);
                        retrofit2.Response<DataUser> responseUserDelete = callDelete.execute();
                        DataUser newUserAfterDelete = responseUserDelete.body();
                        System.out.println("TVS : get fournisseur dataUserAfter insert : " + responseUserDelete.raw());
                        if (newUserAfterDelete != null) {
                            System.out.println("TVS : get fournisseur dataUserAfter insert : " + newUserAfterDelete.getNomFournisseur() + " / id : " + newUserAfterDelete.getIdFournisseur());
                        }
                    }
                } else {
                    System.out.println("TVS : get fournisseur : NULL ");
                }
            }

            if (testGetComplexe == true) {
                // create a user
                Retrofit retrofitComplexe = RetroFit.getClient(MyServiceInterceptor.getInstance());
                FournisseurInterfaceComplexe apiInterfaceComplexe = retrofitComplexe.create(FournisseurInterfaceComplexe.class);
                Call<User> callComplexe = apiInterfaceComplexe.doGetUserById(400);
                retrofit2.Response<User> responseUser = callComplexe.execute();
                User newUser = responseUser.body();
                System.out.println("TVS : 2 : get fournisseur : " + responseUser.toString());
                System.out.println("TVS : 2 : get newUser : " + newUser);

                if (testUpdateComplexe == true) {

                    // update fournisseur
                    User userToUpdate = newUser;
                    DataAdresse dataAdresseToUpdate = userToUpdate.getData().getAdresseSiege();
                    if (dataAdresseToUpdate == null){ // pas d'adresse affectée
                        dataAdresseToUpdate = new DataAdresse();
                    }
                    dataAdresseToUpdate.setPays("FRANCHE 23");
                    dataAdresseToUpdate.setCREATIONDATE(dataAdresseToUpdate.getCREATIONDATE()+".000001");
                    dataAdresseToUpdate.setMODIFICATIONDATE(dataAdresseToUpdate.getMODIFICATIONDATE()+".000001");
                    userToUpdate.getData().setCreationDate( userToUpdate.getData().getCreationDate()+".000001");
                    userToUpdate.getData().setModificationDate(userToUpdate.getData().getModificationDate()+".000001");

                    System.out.println("TVS : 2 : UPDATE 0 aujourdhui : "+dataAdresseToUpdate.getCREATIONDATE());
                    if (dataAdresseToUpdate.getCREATIONDATE() == null || dataAdresseToUpdate.getCREATIONDATE().trim().isEmpty()){
                        SimpleDateFormat formater = null;
                        Date aujourdhui = new Date();
                        formater = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss.SSSSSS");
                        System.out.println("TVS : 2 : UPDATE 1 aujourdhui : "+formater.format(aujourdhui));

                        dataAdresseToUpdate.setCREATIONDATE(formater.format(aujourdhui));
                        dataAdresseToUpdate.setMODIFICATIONDATE(formater.format(aujourdhui));
                    };
                    userToUpdate.getData().setAdresseSiege(dataAdresseToUpdate);
                    userToUpdate.getData().setNomFournisseur("test 700");
                    // userToUpdate.setData(dataAdresseToUpdate);
                    Call<User> callUpdate = apiInterfaceComplexe.doUpdateUser(400, userToUpdate);
                    retrofit2.Response<User> responseUserToUpdate = callUpdate.execute();
//                User newUserUpdated = responseUserToUpdate.body();
//                System.out.println("TVS : get fournisseur updated : "+responseUserToUpdate.toString());
//                if (newUserUpdated != null && newUserUpdated.getData() != null){
//                    System.out.println("TVS : get name of fournisseur updated : "+newUserUpdated.getData().getNomFournisseur());
//                } else {
//                    System.out.println("TVS : get name of fournisseur updated : NULL "+responseUserToUpdate.raw());
//                }

                    // 2eme call pour vérifier
                    Call<User> callAfterUpdate = apiInterfaceComplexe.doGetUserById(400);
                    retrofit2.Response<User> responseUserAfterUpdate = callAfterUpdate.execute();
                    User newUserAfterUpdate = responseUserAfterUpdate.body();
                    System.out.println("TVS : get fournisseur dataUserAfterUpdate : " + newUserAfterUpdate.getData().getNomFournisseur());
                    System.out.println("TVS : get fournisseur dataUserAfterUpdate getAdresseSiege : " + newUserAfterUpdate.getData().getAdresseSiege());
                }


            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}


