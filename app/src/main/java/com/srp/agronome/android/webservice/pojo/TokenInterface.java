package com.srp.agronome.android.webservice.pojo;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by soytouchrp on 04/12/2017.
 */

public interface TokenInterface {


    @POST("auth/login")
    Call<Token> getNewToken(@Body Login login);
}