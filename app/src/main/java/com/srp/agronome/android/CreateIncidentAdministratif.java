package com.srp.agronome.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Etat_Incident;
import com.srp.agronome.android.db.Etat_IncidentDao;
import com.srp.agronome.android.db.Incident;
import com.srp.agronome.android.db.IncidentDao;
import com.srp.agronome.android.db.Incident_Administratif;
import com.srp.agronome.android.db.Incident_AdministratifDao;
import com.srp.agronome.android.db.Incident_Contrat;
import com.srp.agronome.android.db.Incident_ContratDao;
import com.srp.agronome.android.db.Sujet_Message;
import com.srp.agronome.android.db.Sujet_MessageDao;
import com.srp.agronome.android.db.Type_Incident;
import com.srp.agronome.android.db.Type_IncidentDao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;


public class CreateIncidentAdministratif extends AppCompatActivity {

    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    private Spinner Spinner_Type_Incident = null;
    private Spinner Spinner_Incident_Administratif = null;
    private Spinner Spinner_Incident_Contrat= null ;
    private RadioGroup SMSorEMAIL = null;
    private EditText Message = null;
    private final static String KeyMessage = "KeyMessageIncident";

    private ImageButton BtnRetour = null;
    private ImageButton BtnConfirmToolbar = null;
    private Button BtnConfirlView = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_incident_administratif);

        CreateListIntoDatabase();

        Spinner_Type_Incident = (Spinner) findViewById(R.id.SpinnerTypeIncident);
        Spinner_Incident_Administratif = (Spinner) findViewById(R.id.Spinner_Incident_Administratif);
        Spinner_Incident_Contrat= (Spinner) findViewById(R.id.Spinner_Incident_Contrat);
        SMSorEMAIL = (RadioGroup) findViewById(R.id.EmailOrSMS);
        Message = (EditText) findViewById(R.id.TextDescriptionIncident);
        Message.addTextChangedListener(generalTextWatcher);

        GetDataSharedPreference();
        LoadSpinner_TypeIncident();
        LoadSpinner_IncidentAdministratif();
        LoadSpinner_IncidentContrat();

        BtnRetour = (ImageButton) findViewById(R.id.arrow_back_create_incident);
        BtnRetour.setOnClickListener(ButtonRetourHandler);
        BtnConfirlView = (Button) findViewById(R.id.btn_valide_create_incident);
        BtnConfirlView.setOnClickListener(ButtonValidateHandler);
        BtnConfirmToolbar = (ImageButton) findViewById(R.id.btn_confirm_create_incident);
        BtnConfirmToolbar.setOnClickListener(ButtonValidateHandler);
    }

    //Bouton Retour
    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };

    //Event BackPressed
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
        return true;
    }

    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm), InputToText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
            }
        }
    };

    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateIncidentAdministratif.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearDataSharedPreference();
                //Create Incident in DB
                InsertIncidentIntoDatabase();
                DisplayListIncidentInLog();
                Intent intent = new Intent(CreateIncidentAdministratif.this, IncidentAdministratif.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateIncidentAdministratif.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private String InputToText(){
        String result = getString(R.string.textConfirm) + "\n";
        result += getString(R.string.TextValidateTypeIncident) + " : " + Spinner_Type_Incident.getSelectedItem().toString() + "\n";
        if (!Spinner_Incident_Administratif.getSelectedItem().equals(getString(R.string.TextSpinnerIncidentAdministratif)))
        {
            result += getString(R.string.TextValidateIncidentAdministratif) + " : " + Spinner_Incident_Administratif.getSelectedItem().toString() + "\n";
        }
        if (!Spinner_Incident_Contrat.getSelectedItem().equals(getString(R.string.TextSpinnerIncidentContrat)))
        {
            result += getString(R.string.TextValidateIncidentContrat) + " : " + Spinner_Incident_Contrat.getSelectedItem().toString() + "\n";
        }
        result += getString(R.string.TextDescriptionIncident) + " :\n";
        result += Message.getText().toString().trim() + "\n";
        result += getString(R.string.TextSendBy);
        if (SMSorEMAIL.getCheckedRadioButtonId() == R.id.btn_email){
            result += getString(R.string.TextSendByEmail);
        }
        else{
            result += getString(R.string.TextSendBySMS);
        }
        return result;
    }

    //Event Multiple EditText = save data
    private TextWatcher generalTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            if (Message.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyMessage, Message.getText().toString());
            }
        }
    };

    private void SaveStringDataInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }

    // Lire les données enregistrées et les mettre dans les definir dans les edit text
    private void GetDataSharedPreference() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
        if (sharedpreferences.contains(KeyMessage)) {
            Message.setText(sharedpreferences.getString(KeyMessage, ""), TextView.BufferType.EDITABLE);
        }
    }

    //Clear data in sharepreference
    private void clearDataSharedPreference() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(KeyMessage);
        editor.apply();
    }

    private boolean CheckInput() {
        boolean b = true;
        if (TextUtils.isEmpty(Message.getText().toString())) {
            b = false;
        }
        if (Spinner_Type_Incident.getSelectedItem().toString().equals(getString(R.string.TextSpinnerTypeIncident))) {
            b = false;
        }
        if ((Spinner_Type_Incident.getSelectedItemPosition() == 0)&(Spinner_Incident_Administratif.getSelectedItem().toString().equals(getString(R.string.TextSpinnerIncidentAdministratif)))) {
            b = false;
        }
        if ((Spinner_Type_Incident.getSelectedItemPosition() == 1)&(Spinner_Incident_Contrat.getSelectedItem().toString().equals(getString(R.string.TextSpinnerIncidentContrat)))) {
            b = false;
        }
        return b;
    }

    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateIncidentAdministratif.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(CreateIncidentAdministratif.this, IncidentAdministratif.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearDataSharedPreference();
                Intent intent = new Intent(CreateIncidentAdministratif.this, IncidentAdministratif.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Charger le spinner TypeIncident
    private void LoadSpinner_TypeIncident() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Type_IncidentDao type_incident_data = daoSession.getType_IncidentDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Charger les éléments de la liste
        List<Type_Incident> List_TI = type_incident_data.loadAll();
        if (List_TI != null) {
            for (Type_Incident TI : List_TI) {
                adapter.add(TI.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(getString(R.string.TextSpinnerTypeIncident)); //This is the text that will be displayed as hint.
        Spinner_Type_Incident.setAdapter(adapter);
        Spinner_Type_Incident.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Type_Incident.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Type Incident Administratif
                Spinner_Incident_Administratif.setVisibility(View.VISIBLE);
                Spinner_Incident_Contrat.setVisibility(View.GONE);
                LoadSpinner_IncidentContrat(); // Reset the value of Spinner
            }
            if (position == 1 ){ //Type Incident Contrat
                Spinner_Incident_Administratif.setVisibility(View.GONE);
                Spinner_Incident_Contrat.setVisibility(View.VISIBLE);
                LoadSpinner_IncidentAdministratif(); // Reset the value of Spinner
            }
        }
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }


    //Charger le spinner IncidentAdministratif
    private void LoadSpinner_IncidentAdministratif() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Incident_AdministratifDao incident_administratif_data = daoSession.getIncident_AdministratifDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter.add(getString(R.string.TextAnotherIncidentAdministratif)); //Another

        //Charger les éléments de la liste
        List<Incident_Administratif> List_IA = incident_administratif_data.loadAll();
        if (List_IA != null) {
            for (Incident_Administratif IA : List_IA) {
                adapter.add(IA.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(getString(R.string.TextSpinnerIncidentAdministratif)); //This is the text that will be displayed as hint.
        Spinner_Incident_Administratif.setAdapter(adapter);
        Spinner_Incident_Administratif.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Incident_Administratif.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Type Incident Administratif
                Show_Dialog_EditText(getString(R.string.TextAnotherIncidentAdministratif),getString(R.string.TextValidateIncidentAdministratif),1);
            }
        }
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Charger le spinner IncidentAdministratif
    private void LoadSpinner_IncidentContrat() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Incident_ContratDao incident_contrat_data = daoSession.getIncident_ContratDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter.add(getString(R.string.TextAnotherIncidentContrat)); //Another

        //Charger les éléments de la liste
        List<Incident_Contrat> List_IC = incident_contrat_data.loadAll();
        if (List_IC != null) {
            for (Incident_Contrat IC : List_IC) {
                adapter.add(IC.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(getString(R.string.TextSpinnerIncidentContrat)); //This is the text that will be displayed as hint.
        Spinner_Incident_Contrat.setAdapter(adapter);
        Spinner_Incident_Contrat.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Incident_Contrat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Type Incident Administratif
                Show_Dialog_EditText(getString(R.string.TextAnotherIncidentContrat),getString(R.string.TextValidateIncidentContrat),2);
            }
        }
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }


    private void AddIncidentAdministratif(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Incident_AdministratifDao incident_administratif_data = daoSession.getIncident_AdministratifDao();
        Incident_Administratif incident_administratif_input = new Incident_Administratif();
        incident_administratif_input.setNom(Nom);
        incident_administratif_data.insertOrReplace(incident_administratif_input);
        LoadSpinner_IncidentAdministratif();
    }

    private void AddIncidentContrat(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Incident_ContratDao incident_contrat_data = daoSession.getIncident_ContratDao();
        Incident_Contrat incident_contrat_input = new Incident_Contrat();
        incident_contrat_input.setNom(Nom);
        incident_contrat_data.insertOrReplace(incident_contrat_input);
        LoadSpinner_IncidentContrat();
    }


    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateIncidentAdministratif.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code Incident Administratif
                        AddIncidentAdministratif(NewValueList);
                        break;
                    case 2 : //Code Incident Contrat
                        AddIncidentContrat(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }



    //Fonction Base de données
    //Creation des listes de choix dans la base de données
    private void CreateListIntoDatabase(){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Type_IncidentDao type_incident_data = daoSession.getType_IncidentDao();
        Etat_IncidentDao etat_incident_data = daoSession.getEtat_IncidentDao();
        Incident_AdministratifDao incident_administratif_data = daoSession.getIncident_AdministratifDao();
        Incident_ContratDao incident_contrat_data = daoSession.getIncident_ContratDao();

        List<Type_Incident> ListTypeIncident = type_incident_data.loadAll();
        if (ListTypeIncident.size() == 0){
            Type_Incident type_incident_input_1 = new Type_Incident();
            type_incident_input_1.setNom("Administratif");
            type_incident_data.insertOrReplace(type_incident_input_1);

            Type_Incident type_incident_input_2 = new Type_Incident();
            type_incident_input_2.setNom("Contrat");
            type_incident_data.insertOrReplace(type_incident_input_2);
        }

        List <Etat_Incident> ListEtatIncident = etat_incident_data.loadAll();
        if (ListEtatIncident.size() == 0){
            Etat_Incident etat_incident_input_1 = new Etat_Incident();
            etat_incident_input_1.setNom("Résolu");
            etat_incident_data.insertOrReplace(etat_incident_input_1);

            Etat_Incident etat_incident_input_2 = new Etat_Incident();
            etat_incident_input_2.setNom("Non résolu");
            etat_incident_data.insertOrReplace(etat_incident_input_2);
        }

        List<Incident_Administratif>  ListIncidentAdministratif = incident_administratif_data.loadAll();
        if (ListIncidentAdministratif.size() == 0){

            Incident_Administratif incident_administratif_input_1 = new Incident_Administratif();
            incident_administratif_input_1.setNom("Problème de facturation");
            incident_administratif_data.insertOrReplace(incident_administratif_input_1);

            Incident_Administratif incident_administratif_input_2 = new Incident_Administratif();
            incident_administratif_input_2.setNom("Problème de livraison");
            incident_administratif_data.insertOrReplace(incident_administratif_input_2);
        }

        List <Incident_Contrat> ListIncidentContrat = incident_contrat_data.loadAll();
        if (ListIncidentContrat.size() == 0){
            Incident_Contrat incident_contrat_input_1 = new Incident_Contrat();
            incident_contrat_input_1.setNom("Produits non conformes");
            incident_contrat_data.insertOrReplace(incident_contrat_input_1);

            Incident_Contrat incident_contrat_input_2 = new Incident_Contrat();
            incident_contrat_input_2.setNom("Contrat non signé");
            incident_contrat_data.insertOrReplace(incident_contrat_input_2);
        }
    }

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    //Insert an Incident in DB
    private void InsertIncidentIntoDatabase(){
        Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);

        String Description_Incident = "";
        if (Spinner_Type_Incident.getSelectedItemPosition() == 0) { //Cas Administratif Selected
            Description_Incident = Spinner_Incident_Administratif.getSelectedItem().toString();
        }
        if (Spinner_Type_Incident.getSelectedItemPosition() == 1) { //Cas Contrat Selected
            Description_Incident = Spinner_Incident_Contrat.getSelectedItem().toString();
        }

        String Type_Incident = Spinner_Type_Incident.getSelectedItem().toString();
        String TextMessage = Message.getText().toString();
        String Etat_Incident = "Non résolu";
        String Message_Incident = CreateMessageIncident(Nom_Entreprise,Type_Incident,TextMessage,Nom_Compte);
        Sujet_Message sujet_message_input = CreateSujetMessageIntoDatabase(Type_Incident,Description_Incident);

        Incident incident_administratif_input = CreateIncidentIntoDatabase(Message_Incident,Etat_Incident);
        addSujetMessageToIncident(incident_administratif_input,sujet_message_input);
        addIncidentToOrganization(incident_administratif_input,entreprise_selected);
    }

    //Construction du message à envoyer
    private String CreateMessageIncident(String Producteur,String Type_Probleme, String TextMessage, String Agronome){
        String result ="";
        result += getString(R.string.CorpsMessageIncident_1) + " " + Producteur + " " ;
        result += getString(R.string.CorpsMessageIncident_2) + " " + Type_Probleme + " ";
        result += getString(R.string.CorpsMessageIncident_3) + " " + TextMessage + "\n";
        result += getString(R.string.CorpsMessageIncident_4) + "\n";
        result += getString(R.string.CorpsMessageIncident_5) + "\n\n" + Agronome;
        return result;
    }

    //Return an Organization
    private Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }

    private Incident CreateIncidentIntoDatabase(String Message , String Etat_Incident){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        IncidentDao incident_data = daoSession.getIncidentDao();
        Etat_IncidentDao etat_incident_data = daoSession.getEtat_IncidentDao();

        Incident incident_input = new Incident();
        incident_input.setArchive(1);
        incident_input.setDate_creation(Today_Date());
        incident_input.setDate_modification(Today_Date());
        incident_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        incident_input.setID_Agronome_Modification((int)ID_Compte_Selected);

        incident_input.setMessage_Incident(Message);

        //Ajout Etat Incident
        List<Etat_Incident> List_Etat_Incident = etat_incident_data.queryBuilder()
                .where(Etat_IncidentDao.Properties.Nom.eq(Etat_Incident))
                .list();
        if (List_Etat_Incident != null) {
            for (Etat_Incident EI : List_Etat_Incident) {
                incident_input.setEtat_Incident(EI);
                incident_input.setID_Etat_Incident(EI.getId());
            }
        }
        incident_data.insertOrReplace(incident_input);
        return incident_input;
    }

    private Sujet_Message CreateSujetMessageIntoDatabase(String Type_Incident , String Description){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Sujet_MessageDao sujet_message_data = daoSession.getSujet_MessageDao();
        Type_IncidentDao type_incident_data = daoSession.getType_IncidentDao();
        Incident_ContratDao incident_contrat_data = daoSession.getIncident_ContratDao();
        Incident_AdministratifDao incident_administratif_data = daoSession.getIncident_AdministratifDao();

        Sujet_Message sujet_message_input = new Sujet_Message();
        //Ajout Type Incident
        List<Type_Incident> List_Type_Incident = type_incident_data.queryBuilder()
                .where(Type_IncidentDao.Properties.Nom.eq(Type_Incident))
                .list();
        if (List_Type_Incident != null) {
            for (Type_Incident TI : List_Type_Incident) {
                sujet_message_input.setType_Incident(TI);
                sujet_message_input.setID_Type_Incident(TI.getId());
            }
        }
        if (Spinner_Type_Incident.getSelectedItemPosition() == 0){ //Cas Administratif Selected
            List<Incident_Administratif> List_Incident_Administratif = incident_administratif_data.queryBuilder()
                    .where(Incident_AdministratifDao.Properties.Nom.eq(Description))
                    .list();
            if (List_Incident_Administratif != null) {
                for (Incident_Administratif IA : List_Incident_Administratif) {
                    sujet_message_input.setIncident_Administratif(IA);
                    sujet_message_input.setID_Nom(IA.getId());
                }
            }
        }
        if (Spinner_Type_Incident.getSelectedItemPosition() == 1){ //Cas Contrat Selected
            List<Incident_Contrat> List_Incident_Contrat= incident_contrat_data.queryBuilder()
                    .where(Incident_ContratDao.Properties.Nom.eq(Description))
                    .list();
            if (List_Incident_Contrat != null) {
                for (Incident_Contrat IC : List_Incident_Contrat) {
                    sujet_message_input.setIncident_Contrat(IC);
                    sujet_message_input.setID_Nom(IC.getId());
                }
            }
        }

        sujet_message_data.insertOrReplace(sujet_message_input);
        return sujet_message_input;
    }


    private void addSujetMessageToIncident(Incident incident_input, Sujet_Message sujet_message_input){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        IncidentDao incident_data = daoSession.getIncidentDao();
        Sujet_MessageDao sujet_message_data = daoSession.getSujet_MessageDao();
        incident_input.setSujet_Message(sujet_message_input);
        incident_input.setID_Sujet_Message(sujet_message_input.getId());
        sujet_message_data.update(sujet_message_input);
        incident_data.update(incident_input);
    }

    private void addIncidentToOrganization(Incident incident_input , Entreprise entreprise_input){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        IncidentDao incident_data = daoSession.getIncidentDao();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        incident_input.setID_Entreprise(entreprise_input.getId());
        incident_data.update(incident_input);
        entreprise_input.resetIncidentList();
        entreprise_data.update(entreprise_input);
    }

    private void DisplayListIncidentInLog(){
        Entreprise entreprise_load = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        List<Incident> list_incident = entreprise_load.getIncidentList();
        if (list_incident != null){
            for (Incident I : list_incident){
                Log.i("DescriptionIncident", I.getSujet_Message().getType_Incident().getNom());
                Log.i("EtatIncident", I.getEtat_Incident().getNom());
                Log.i("MessageIncident", I.getMessage_Incident());
            }
        }
    }

}


