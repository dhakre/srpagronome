package com.srp.agronome.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;

import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;

import com.srp.agronome.android.db.Entreprise;

import com.srp.agronome.android.db.Geolocalisation;
import com.srp.agronome.android.db.GeolocalisationDao;
import com.srp.agronome.android.db.ILOT;
import com.srp.agronome.android.db.ILOTDao;
import com.srp.agronome.android.db.IRRIGATION;
import com.srp.agronome.android.db.IRRIGATIONDao;

import com.srp.agronome.android.db.PARCELLE;
import com.srp.agronome.android.db.PARCELLEDao;
import com.srp.agronome.android.db.STATUT_PARCELLE;
import com.srp.agronome.android.db.STATUT_PARCELLEDao;

import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;
import com.srp.agronome.android.db.TYPE_CULTURE;
import com.srp.agronome.android.db.TYPE_CULTUREDao;
import com.srp.agronome.android.db.TYPE_IRRIGATION;
import com.srp.agronome.android.db.TYPE_IRRIGATIONDao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;

import static com.srp.agronome.android.MenuParcelle.Id_ilot_selected;
import static com.srp.agronome.android.MenuParcelle.Id_Parcelle_selected;
import static com.srp.agronome.android.MenuParcelle.Id_suivi_producteur;
/**
 * Created by Jitendra on 19/07/2017.
 */

public class EditParcelle extends AppCompatActivity {


    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    private EditText NOM_PARCELLE   = null;
    private final static String KeyNOM_PARCELLE  = "NOM_PARCELLE ";


    private EditText NUMERO_PARCELLE   = null;
    private final static String KeyNUMERO_PARCELLE  = "NUMERO_PARCELLE ";

    private EditText SURFACE    = null;
    private final static String KeySURFACE   = "surface";


    private Spinner SpinnerSTATUT_PARCELLE = null;
    private Spinner  SpinnerTYPE_CULTURE = null;
    private Spinner SpinnerIRRIGATION = null;
    private Spinner SpinnerTYPE_IRRIGATION = null;
    private Spinner SpinnerIlot = null;
    private ImageButton imgBtnHome = null;
    private Button BtnValidate = null;
    private Button PrendrePhoto = null;

    private ImageView Geolocalisation_parcelle = null;

    private double GPS_Latitude = 0;
    private double GPS_Longitude = 0 ;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_parcelle);
        FindWidgetViewbyId(); //Associate Widgets

      AddListenerEvent(); //Add Widgets Event
      //GetStringData();

        //Liste de choix dans la base de données
        CreateListIntoDatabase();
        LoadSpinnerSTATUT_PARCELLE();
        LoadSpinnerTYPE_CULTURE();
        LoadSpinnerIRRIGATION();
        LoadSpinnerTYPE_IRRIGATION();
        LoadSpinnerIlot();

        PARCELLE parcelle_loaded = getParcelleV2(Id_Parcelle_selected);
        setDataParcelleToWidget(parcelle_loaded);

        ILOT ilot_selected = getIlotV2(Id_ilot_selected);
        setDataILOTToWidget(ilot_selected);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_parcelle);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);
        BtnValidate = (Button) findViewById(R.id.btnvalidate_CQ);
        BtnValidate.setOnClickListener(ButtonValidateHandler);
        PrendrePhoto = (Button) findViewById(R.id.parcelle_Photo);
        PrendrePhoto.setOnClickListener(ButtonPrendrePhotoHandler);





    }

    private void FindWidgetViewbyId() {
        NOM_PARCELLE  = (EditText) findViewById(R.id.NOM_edit_PARCELE);
        NUMERO_PARCELLE  = (EditText) findViewById(R.id.NUMERO_edit_PARCELLE);
        SURFACE  = (EditText) findViewById(R.id.Edit_SURFACE);
        Geolocalisation_parcelle = (ImageView) findViewById(R.id.ibtnGeolocalisation_parcelle);

        SpinnerSTATUT_PARCELLE = (Spinner) findViewById(R.id.STATUT_PARCELLE);
        SpinnerTYPE_CULTURE = (Spinner) findViewById(R.id.TYPE_CULTURE);
        SpinnerIRRIGATION = (Spinner) findViewById(R.id.IRRIGATION);
        SpinnerTYPE_IRRIGATION = (Spinner) findViewById(R.id.TYPE_IRRIGATION);
        SpinnerIlot = (Spinner) findViewById(R.id.Spinner_Ilot);
    }



    //addTextChangedListener : Adds a TextWatcher to the list of those whose methods are called whenever this TextView's text changes.
   private void AddListenerEvent() {
       Geolocalisation_parcelle.setOnClickListener(Handler_Geolocalisation_parcelle);
    }

    View.OnClickListener ButtonPrendrePhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            TakePicturefromCamera();
        }
    };


    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile("_Parcelle" + "_HD.jpg");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivity(intent);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/parcelle/ILOT/2017/Soytouch");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        return file;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        //Depuis la caméra : pas de bitmap en sortie
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit/2017/Soytouch");
        File file = new File(myDir, "_Parcelle/ILOT_picture_HD.jpg");
        Uri uri = Uri.fromFile(file);
        Bitmap newPicture;
        try {
            newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
            SaveImage(newPicture, "_Parcelle/ILOT_picture_HD.jpg");
            newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
            SaveImage(newPicture, "_Parcelle/ILOT_picture.jpg"); //Sauvegarde l'image dans le téléphone
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit_Document");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        //Log.i("File Location",file.toString());
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //Bouton Home Toolbar
    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };
    View.OnClickListener Handler_Geolocalisation_parcelle = new View.OnClickListener() {
        public void onClick(View v) {
            // create class object
            GPSTracker gps = new GPSTracker(EditParcelle.this);
            // check if GPS enabled
            if (gps.canGetLocation()) {
                GPS_Latitude = gps.getLatitude();
                GPS_Longitude = gps.getLongitude();
                //Display Values
                Toast.makeText(getApplicationContext(), getString(R.string.TitleLocationFound) + "\n" +
                                getString(R.string.TextLatitude) + " " + GPS_Latitude + "\n" +
                                getString(R.string.TextLongitude) + " " + GPS_Longitude
                        , Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }

    };
    //Bouton Valider Toolbar
    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();}
        } };

    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewQC);
        int position = 0 ;



        if (TextUtils.isEmpty(NOM_PARCELLE.getText().toString())) {
            position = NOM_PARCELLE.getTop();
        }

        if (TextUtils.isEmpty(NUMERO_PARCELLE.getText().toString())) {
            position = NUMERO_PARCELLE.getTop();
        }
        if (TextUtils.isEmpty(SURFACE.getText().toString())) {
            position = SURFACE.getTop();
        }


        final int Position_Y = position;
        final int Position_X = 0;
        sc.post(new Runnable() {
            public void run() {
                sc.scrollTo(Position_X, Position_Y); // these are your x and y coordinates
            }
        });
    }

    //A method that creates a string listing the data entered.
    private String InputToText(){
        String text = getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.Textilot) + " : " + SpinnerIlot.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.NOM_PARCELLE) + " : " + NOM_PARCELLE.getText().toString() + "\n";
       text += getString(R.string.NUMERO_PARCELLE) + " : " +NUMERO_PARCELLE.getText().toString() + "\n";
       text += getString(R.string.SURFACE) + " : " + SURFACE.getText().toString() + "\n\n";

        text += getString(R.string.TextStatutParcelle) + " : " + SpinnerSTATUT_PARCELLE.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextTYPE_CULTURE) + " : " + SpinnerTYPE_CULTURE.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextIRRIGATION) + " : " + SpinnerIRRIGATION.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextTYPE_IRRIGATION) + " : " + SpinnerTYPE_IRRIGATION.getSelectedItem().toString() + "\n\n";

        return text;
    }


    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(EditParcelle.this, MenuParcelle.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(EditParcelle.this, MenuParcelle.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }
    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditParcelle.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }


    // Method for creating the selection lists in the database
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STATUT_PARCELLEDao STATUT_PARCELLE_data = daoSession.getSTATUT_PARCELLEDao();
        TYPE_CULTUREDao TYPE_CULTURE_data = daoSession.getTYPE_CULTUREDao();
        IRRIGATIONDao IRRIGATION_data = daoSession.getIRRIGATIONDao();
        TYPE_IRRIGATIONDao TYPE_IRRIGATION_data = daoSession.getTYPE_IRRIGATIONDao();
        ILOTDao Ilot_data = daoSession.getILOTDao();



        List<STATUT_PARCELLE> List_STATUT_PARCELLE = STATUT_PARCELLE_data.loadAll();
        if (List_STATUT_PARCELLE.size() == 0) {

            STATUT_PARCELLE  input_1 = new STATUT_PARCELLE();
            input_1.setNom(" Autorisé de livraison");
            STATUT_PARCELLE_data.insertOrReplace(input_1);


            STATUT_PARCELLE  input_2 = new STATUT_PARCELLE();
            input_2.setNom(" Sujet à contre-visite");
            STATUT_PARCELLE_data.insertOrReplace(input_2);

            STATUT_PARCELLE  input_3 = new STATUT_PARCELLE();
            input_3.setNom(" Interdit de livraison");
            STATUT_PARCELLE_data.insertOrReplace(input_3);
        }
        List<TYPE_CULTURE> List_TYPE_CULTURE = TYPE_CULTURE_data.loadAll();
        if (List_TYPE_CULTURE.size() == 0) {

            TYPE_CULTURE  input_TYPE_CULTURE_1 = new TYPE_CULTURE();
            input_TYPE_CULTURE_1.setNom("Céréale");
            TYPE_CULTURE_data.insertOrReplace(input_TYPE_CULTURE_1);

            TYPE_CULTURE  input_TYPE_CULTURE_2 = new TYPE_CULTURE();
            input_TYPE_CULTURE_2.setNom("Arboriculture");
            TYPE_CULTURE_data.insertOrReplace(input_TYPE_CULTURE_2);

            TYPE_CULTURE  input_TYPE_CULTURE_3 = new TYPE_CULTURE();
            input_TYPE_CULTURE_3.setNom("Maréchaige");
            TYPE_CULTURE_data.insertOrReplace(input_TYPE_CULTURE_3);

            TYPE_CULTURE  input_TYPE_CULTURE_4 = new TYPE_CULTURE();
            input_TYPE_CULTURE_4.setNom("Viticulture");
            TYPE_CULTURE_data.insertOrReplace(input_TYPE_CULTURE_4);

        }
        List<IRRIGATION> List_IRRIGATION = IRRIGATION_data.loadAll();
        if (List_IRRIGATION.size() == 0) {

            IRRIGATION  input_IRRIGATION_1 = new IRRIGATION();
            input_IRRIGATION_1.setNom("Oui");
            IRRIGATION_data.insertOrReplace(input_IRRIGATION_1);

            IRRIGATION  input_IRRIGATION_2 = new IRRIGATION();
            input_IRRIGATION_2.setNom("Non");
            IRRIGATION_data.insertOrReplace(input_IRRIGATION_2);

            IRRIGATION  input_IRRIGATION_3 = new IRRIGATION();
            input_IRRIGATION_3.setNom("Les deux");
            IRRIGATION_data.insertOrReplace(input_IRRIGATION_3);
        }
        List<TYPE_IRRIGATION> List_TYPE_IRRIGATION = TYPE_IRRIGATION_data.loadAll();
        if (List_TYPE_IRRIGATION.size() == 0) {

            TYPE_IRRIGATION  input_TYPE_IRRIGATION_1 = new TYPE_IRRIGATION();
            input_TYPE_IRRIGATION_1.setNom("Pivot");
            TYPE_IRRIGATION_data.insertOrReplace(input_TYPE_IRRIGATION_1);

            TYPE_IRRIGATION  input_TYPE_IRRIGATION_2 = new TYPE_IRRIGATION();
            input_TYPE_IRRIGATION_2.setNom("Enrouleur");
            TYPE_IRRIGATION_data.insertOrReplace(input_TYPE_IRRIGATION_2);

            TYPE_IRRIGATION  input_TYPE_IRRIGATION_3 = new TYPE_IRRIGATION();
            input_TYPE_IRRIGATION_3.setNom("Intégrale");
            TYPE_IRRIGATION_data.insertOrReplace(input_TYPE_IRRIGATION_3);

            TYPE_IRRIGATION  input_TYPE_IRRIGATION_4 = new TYPE_IRRIGATION();
            input_TYPE_IRRIGATION_4.setNom("Mix");
            TYPE_IRRIGATION_data.insertOrReplace(input_TYPE_IRRIGATION_4);
        }
    }




    // Load spinner STATUT_PARCELLE
    private void LoadSpinnerSTATUT_PARCELLE() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STATUT_PARCELLEDao STATUT_PARCELLE_data = daoSession.getSTATUT_PARCELLEDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));

                }return v;}
            @Override
            public int getCount() {
                return super.getCount()-1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.Textanotherstatutparcelle));
        //Load items from the list
        List<STATUT_PARCELLE> List_STATUT_PARCELLE =STATUT_PARCELLE_data.loadAll();
        if (List_STATUT_PARCELLE != null) {
            for (STATUT_PARCELLE SP : List_STATUT_PARCELLE) {
                adapter.add(SP.getNom()); //Ajouter les éléments

            }
        }

        adapter.add(getString(R.string.TextStatutParcelle)); //This is the text that will be displayed as hint.
        SpinnerSTATUT_PARCELLE.setAdapter(adapter);  //pay attention
        SpinnerSTATUT_PARCELLE.setSelection(adapter.getCount());
        SpinnerSTATUT_PARCELLE.setSelection(1);
        //set the hint the default selection so it appears on launch.
        SpinnerSTATUT_PARCELLE.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidateSTATUT_PARCELLE),getString(R.string.TextInputSTATUT_PARCELLE),1);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});

    }



    // Load Spinner TYPE_CULTURE
    private void LoadSpinnerTYPE_CULTURE() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_CULTUREDao TYPE_CULTURE_data = daoSession.getTYPE_CULTUREDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextanotherTYPE_CULTURE));
        //Load items from the list
        List<TYPE_CULTURE> List_TYPE_CULTURE =TYPE_CULTURE_data.loadAll();
        if (List_TYPE_CULTURE != null) {
            for (TYPE_CULTURE TP : List_TYPE_CULTURE) {
                adapter.add(TP.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(getString(R.string.TextTYPE_CULTURE)); //This is the text that will be displayed as hint.
        SpinnerTYPE_CULTURE.setAdapter(adapter);  //pay attention
        SpinnerTYPE_CULTURE.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
// Add new Type Culture
        SpinnerTYPE_CULTURE.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(getString(R.string.TextValidateTYPE_CULTURE),getString(R.string.TextInputTYPE_CULTURE),2);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }


    // Load Spinner IRRIGATION
    private void LoadSpinnerIRRIGATION() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        IRRIGATIONDao IRRIGATION_data = daoSession.getIRRIGATIONDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextanotherIRRIGATION));
        //Load items from the list
        List< IRRIGATION> List_IRRIGATION = IRRIGATION_data.loadAll();
        if (List_IRRIGATION != null) {
            for (IRRIGATION IR : List_IRRIGATION) {
                adapter.add(IR.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(getString(R.string.TextIRRIGATION)); //This is the text that will be displayed as hint.
        SpinnerIRRIGATION.setAdapter(adapter);  //pay attention
        SpinnerIRRIGATION.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
// Add new irrigation
        SpinnerIRRIGATION.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(getString(R.string.TextValidateIRRIGATION),getString(R.string.TextInputIRRIGATION),3);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    // Load Spinner TYPE_IRRIGATION
    private void LoadSpinnerTYPE_IRRIGATION() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_IRRIGATIONDao TYPE_IRRIGATION_data = daoSession.getTYPE_IRRIGATIONDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextanotherTYPE_IRRIGATION));
        //Load items from the list
        List< TYPE_IRRIGATION> List_TYPE_IRRIGATION=TYPE_IRRIGATION_data.loadAll();
        if (List_TYPE_IRRIGATION != null) {
            for (TYPE_IRRIGATION TIR : List_TYPE_IRRIGATION) {
                adapter.add(TIR.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(getString(R.string.TextTYPE_IRRIGATION)); //This is the text that will be displayed as hint.
        SpinnerTYPE_IRRIGATION.setAdapter(adapter);  //pay attention
        SpinnerTYPE_IRRIGATION.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

        SpinnerTYPE_IRRIGATION.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(getString(R.string.TextValidateTYPE_IRRIGATION),getString(R.string.TextInputTYPE_IRRIGATION),4);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }


    //Load spinner Ilot
    private void LoadSpinnerIlot() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ILOTDao   Ilotdata = daoSession.getILOTDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.Textanotherilot)); //Text Add a new  ilot
        //Charger les éléments de la liste
        List<ILOT> List_ilot = Ilotdata.loadAll();
        if (List_ilot != null) {
            for (ILOT ilot : List_ilot) {
                adapter.add(ilot.getNOM_ILOT() + " " + ilot.getNUMERO_ILOT()); //NOM ET NUMERO SUR LE MEME ELEMENT
            }
        }
        adapter.add(getString(R.string.Textilot)); //This is the text that will be displayed as hint.
        SpinnerIlot.setAdapter(adapter);
        SpinnerIlot.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        SpinnerIlot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText2(getString(R.string.TextValidateNom_Ilot),getString(R.string.TextInputNom_Ilot),1);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }



    //Create a dialog with an editText and return a string
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditParcelle.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim();//Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code STATUT_PARCELLE
                        AddSTATUT_PARCELLEDB(NewValueList);
                        break;
                    case 2 : //Code TYPE_CULTURE
                        AddTYPE_CULTUREDB(NewValueList);
                        break;
                    case 3 : //Code IRRIGATION
                        AddIRRIGATIONDB(NewValueList);
                        break;
                    case 4 : //Code Type_Irrigation
                        AddTYPE_IRRIGATIONDB(NewValueList);
                        break;

                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }





    private void AddSTATUT_PARCELLEDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STATUT_PARCELLEDao STATUT_PARCELLE_data = daoSession.getSTATUT_PARCELLEDao();
        STATUT_PARCELLE STATUT_PARCELLE_input = new STATUT_PARCELLE();
        STATUT_PARCELLE_input.setNom(Nom);
        STATUT_PARCELLE_data.insertOrReplace(STATUT_PARCELLE_input);
        LoadSpinnerSTATUT_PARCELLE();
    }
    private void AddTYPE_CULTUREDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_CULTUREDao TYPE_CULTURE_data = daoSession.getTYPE_CULTUREDao();
        TYPE_CULTURE TYPE_CULTURE_input = new TYPE_CULTURE();
        TYPE_CULTURE_input.setNom(Nom);
        TYPE_CULTURE_data.insertOrReplace(TYPE_CULTURE_input);
        LoadSpinnerTYPE_CULTURE();
    }


    private void AddIRRIGATIONDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        IRRIGATIONDao IRRIGATION_data = daoSession.getIRRIGATIONDao();
        IRRIGATION IRRIGATION_input = new IRRIGATION();
        IRRIGATION_input.setNom(Nom);
        IRRIGATION_data.insertOrReplace(IRRIGATION_input);
        LoadSpinnerIRRIGATION();
    }

    private void AddTYPE_IRRIGATIONDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_IRRIGATIONDao TYPE_IRRIGATION_data = daoSession.getTYPE_IRRIGATIONDao();
        TYPE_IRRIGATION TYPE_IRRIGATION_input = new TYPE_IRRIGATION();
        TYPE_IRRIGATION_input.setNom(Nom);
        TYPE_IRRIGATION_data.insertOrReplace(TYPE_IRRIGATION_input);
        LoadSpinnerTYPE_IRRIGATION();
    }

    // Method to check if the user has entered the data

    private Boolean CheckInput(){
        boolean b = true;

        //Vérification des texte editables

        /*   if (TextUtils.isEmpty(NOM_PARCELLE .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(NUMERO_PARCELLE  .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(SURFACE  .getText().toString())) {
            b = false;
        } */
        //Vérification des listes de choix :
        if (SpinnerSTATUT_PARCELLE.getSelectedItem().toString().equals(getString(R.string.TextStatutParcelle))) {
            b = false;
        }
        if (SpinnerTYPE_CULTURE.getSelectedItem().toString().equals(getString(R.string.TextTYPE_CULTURE))) {
            b = false;
        }
        if (SpinnerIRRIGATION.getSelectedItem().toString().equals(getString(R.string.TextIRRIGATION))) {
            b = false;
        }
        if (SpinnerTYPE_IRRIGATION.getSelectedItem().toString().equals(getString(R.string.TextTYPE_IRRIGATION))) {
            b = false;
        }


        return b;
    }
    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
              //   clearData();
                // Edit Parcelle into db
                EditparcelleIntoDatabase(Id_ilot_selected , Id_Parcelle_selected);
                Intent intent = new Intent(EditParcelle.this, ListParcelle.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Create a dialog with an editText and return a string
    private void Show_Dialog_EditText2(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edit_numero_text, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditParcelle.this);
        final   EditText Nom_ilot = (EditText) dialogview.findViewById(R.id.InputDialogText);
        final   EditText Numero_ilot = (EditText) dialogview.findViewById(R.id.InputDialog_ilotnumero_Text);
        builder.setTitle(title);
        Nom_ilot.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValuenom_ilot = Nom_ilot.getText().toString().trim(); //Globale Variable String
                NewValuenom_ilot = NewValuenom_ilot.replaceAll("\\s+",""); //Erase the space
                String NewValuenumero_ilot = Numero_ilot.getText().toString().trim();
                NewValuenumero_ilot = NewValuenumero_ilot.replaceAll("\\s+",""); //Erase the space

                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 :
                        ILOT nouveau_ilot = CreateIlotIntoDatabase(NewValuenom_ilot, NewValuenumero_ilot);
                        SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected, ID_Entreprise_Selected);
                        AddIlotToSuiviProducteur(sp, nouveau_ilot); //Relier le nouvel ilot crée au suivi_producteu
                        LoadSpinnerIlot();
                }


            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    //Create an Ilot into Database
    private ILOT CreateIlotIntoDatabase(String Nom_Ilot, String Numero_Ilot){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ILOTDao ilot_data = daoSession.getILOTDao();

        ILOT ilot_input = new ILOT();
        ilot_input.setNOM_ILOT(Nom_Ilot);
        ilot_input.setNUMERO_ILOT(Numero_Ilot);
        ilot_input.setArchive(1);
        ilot_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        ilot_input.setID_Agronome_Modification((int)ID_Compte_Selected);
        ilot_input.setDate_creation(Today_Date());
        ilot_input.setDate_modification(Today_Date());
        ilot_data.insertOrReplace(ilot_input); // Insert in DB
        return ilot_input;

    }

    private void EditparcelleIntoDatabase(long Id_Ilot_Selected, long Id_Parcelle_selected) {

        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SUIVI_PRODUCTEURDao suivi_producteurDao = daoSession.getSUIVI_PRODUCTEURDao();
        ILOTDao ilotDao = daoSession.getILOTDao();

        List<SUIVI_PRODUCTEUR> list_suiviproducteur = suivi_producteurDao.queryBuilder()
                .where(SUIVI_PRODUCTEURDao.Properties.Id.eq(Id_suivi_producteur))
                .list();
        if (list_suiviproducteur != null) {
            for (SUIVI_PRODUCTEUR sp : list_suiviproducteur) {

                List<ILOT> List_ilot = sp.getILOTList(); //Load All ilot
                for (ILOT il : List_ilot) {
                    if (il.getId() == Id_Ilot_Selected) {
                        ILOT editilot = null;
                        //Edit  Ilot Data
                        //Edit Spinner Ilot
                        List<ILOT> list_ilot = ilotDao.queryBuilder()
                                .where(ILOTDao.Properties.NOM_ILOT.eq(SpinnerIlot.getSelectedItem().toString()))
                                .list();
                        if (list_ilot != null) {
                            for (ILOT ilt : list_ilot) {
                                UpdateIlotIntoDatabase(editilot,ilt.getNOM_ILOT(),ilt.getNUMERO_ILOT());
                            }

                            }



 List<PARCELLE> List_parcelle = il.getPARCELLEList();
                        for (PARCELLE pl : List_parcelle) {
                            if (pl.getId() == Id_Parcelle_selected) {
                                // Edit  parcelle data
                              PARCELLE editparcelle =null;
                                UpdateParcelleIntoDatabase(editparcelle,NOM_PARCELLE.getText().toString().trim(),
                                        NUMERO_PARCELLE.getText().toString().trim(),
                                        Float.valueOf(SURFACE.getText().toString().trim()),
                                                SpinnerSTATUT_PARCELLE.getSelectedItem().toString(),
                                                SpinnerTYPE_CULTURE.getSelectedItem().toString(),
                                                SpinnerIRRIGATION.getSelectedItem().toString(),
                                                SpinnerTYPE_IRRIGATION.getSelectedItem().toString()
                                        );} } }}}}}


    //Fonctions Base de Donnnes Parcelles/Ilot pour Jitendra

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }







    //Add 1 Ilot to 1 SuiviProducteur
    private void AddIlotToSuiviProducteur(SUIVI_PRODUCTEUR suivi_producteur_input , ILOT ilot_input){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ILOTDao ilot_data = daoSession.getILOTDao();
        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
        ilot_input.setID_SUIVI_PRODUCTEUR(suivi_producteur_input.getId());
        ilot_data.update(ilot_input);
        suivi_producteur_data.update(suivi_producteur_input);
    }

    private SUIVI_PRODUCTEUR getSuiviProducteurOfOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        SUIVI_PRODUCTEUR suivi_producteur_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        suivi_producteur_result = e.getSUIVI_PRODUCTEUR();
                    }
                }
            }
        }
        return suivi_producteur_result;
    }


    //Return a list of Ilots
    private List<ILOT> getIlotList(SUIVI_PRODUCTEUR Suivi_producteur){
        return Suivi_producteur.getILOTList();
    }

    //Return an ilot
    private ILOT getIlot(List <ILOT> list_ilot ,long ID_Ilot){
        ILOT result = null;
        for (ILOT I : list_ilot){
            if (I.getId() == ID_Ilot){
                result = I;
            }
        }
        return result;
    }

    //Return a list of Parcelle
    private List<PARCELLE> getParcelleList(ILOT ilot){
        return ilot.getPARCELLEList();
    }



    //Update an Ilot in Database
    private void UpdateIlotIntoDatabase(ILOT ilot_edit,String Nom_Ilot, String Numero_Ilot){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ILOTDao ilot_data = daoSession.getILOTDao();
        ilot_edit.setNOM_ILOT(Nom_Ilot);
        ilot_edit.setNUMERO_ILOT(Numero_Ilot);
        ilot_edit.setID_Agronome_Modification((int)ID_Compte_Selected);
        ilot_edit.setDate_modification(Today_Date());
        ilot_data.update(ilot_edit); //Update in DB
    }

    //Update a Parcelle in Database
    private void UpdateParcelleIntoDatabase(PARCELLE parcelle_edit,String Nom_Parcelle,String Numero_Parcelle , float Surface , String Statut_Parcelle , String Type_Culture, String Irrigation , String Type_Irrigation){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PARCELLEDao parcelle_data = daoSession.getPARCELLEDao();
        STATUT_PARCELLEDao statut_parcelle_data = daoSession.getSTATUT_PARCELLEDao();
        TYPE_CULTUREDao type_culture_data = daoSession.getTYPE_CULTUREDao();
        IRRIGATIONDao irrigation_data = daoSession.getIRRIGATIONDao();
        TYPE_IRRIGATIONDao type_irrigation_data = daoSession.getTYPE_IRRIGATIONDao();

        parcelle_edit.setNOM_PARCELLE(Nom_Parcelle);
        parcelle_edit.setNUMERO_PARCELLE(Numero_Parcelle);
        parcelle_edit.setSURFACE(Surface);

        //Ajout Statut Parcelle
        List<STATUT_PARCELLE> List_Statut_Parcelle = statut_parcelle_data.queryBuilder()
                .where(STATUT_PARCELLEDao.Properties.Nom.eq(Statut_Parcelle))
                .list();
        if (List_Statut_Parcelle != null) {
            for (STATUT_PARCELLE SP : List_Statut_Parcelle) {
                parcelle_edit.setSTATUT_PARCELLE(SP);
                parcelle_edit.setID_STATUT_PARCELLE(SP.getId());
            }
        }

        //Ajout Type Culture
        List<TYPE_CULTURE> List_Type_Culture = type_culture_data.queryBuilder()
                .where(TYPE_CULTUREDao.Properties.Nom.eq(Type_Culture))
                .list();
        if (List_Type_Culture != null) {
            for (TYPE_CULTURE TC : List_Type_Culture) {
                parcelle_edit.setTYPE_CULTURE(TC);
                parcelle_edit.setID_TYPE_CULTURE(TC.getId());
            }
        }
        //Ajout Irrigation
        List<IRRIGATION> List_Irrigation = irrigation_data.queryBuilder()
                .where(IRRIGATIONDao.Properties.Nom.eq(Irrigation))
                .list();
        if (List_Irrigation != null) {
            for (IRRIGATION I : List_Irrigation) {
                parcelle_edit.setIRRIGATION(I);
                parcelle_edit.setID_IRRIGATION(I.getId());
            }
        }
        //Ajout Type Irrigation
        List<TYPE_IRRIGATION> List_Type_Irrigation = type_irrigation_data.queryBuilder()
                .where(TYPE_IRRIGATIONDao.Properties.Nom.eq(Type_Irrigation))
                .list();
        if (List_Type_Irrigation  != null) {
            for (TYPE_IRRIGATION TI : List_Type_Irrigation ) {
                parcelle_edit.setTYPE_IRRIGATION(TI);
                parcelle_edit.setID_TYPE_IRRIGATION(TI.getId());
            }
        }
        parcelle_edit.setID_Agronome_Modification((int)ID_Compte_Selected);
        parcelle_edit.setDate_modification(Today_Date());
        parcelle_data.update(parcelle_edit); //Update in DB
    }
    //Return a parcelle
    private PARCELLE getParcelle(List<PARCELLE> list_parcelle, long ID_Parcelle) {
        PARCELLE result = null;
        for (PARCELLE P : list_parcelle) {
            if (P.getId() == ID_Parcelle) {
                result = P;
            }
        }
        return result;
    }
    //Delete a parcelle
    private void DeleteParcelle(PARCELLE parcelle_delete){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PARCELLEDao parcelle_data = daoSession.getPARCELLEDao();
        parcelle_delete.setArchive(0); // 0 = hidden
        parcelle_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        parcelle_delete.setDate_modification(Today_Date());
        parcelle_data.update(parcelle_delete);
    }

    //Delete a ilot and its list of parcelle
    private void DeleteIlot (ILOT ilot_delete){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ILOTDao ilot_data = daoSession.getILOTDao();
        PARCELLEDao parcelle_data = daoSession.getPARCELLEDao();
        List <PARCELLE> list_parcelle = ilot_delete.getPARCELLEList();
        if (list_parcelle != null){
            for (PARCELLE P : list_parcelle){
                P.setArchive(0);
                P.setDate_modification(Today_Date());
                P.setID_Agronome_Modification((int)ID_Compte_Selected);
                parcelle_data.update(P);
            }
        }
        ilot_delete.setArchive(0);
        ilot_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        ilot_delete.setDate_modification(Today_Date());
        ilot_data.update(ilot_delete);
    }

    //Add Latitude and Longitude position to a Parcelle
    private void addGeolocalisationToParcelle (PARCELLE parcelle, double latitude, double longitude){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PARCELLEDao parcelle_data = daoSession.getPARCELLEDao();
        GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();
        Geolocalisation geolocalisation_input = new Geolocalisation();
        geolocalisation_input.setLatitude(latitude);
        geolocalisation_input.setLongitude(longitude);
        geolocalisation_data.insertOrReplace(geolocalisation_input);
        parcelle.setGeolocalisation(geolocalisation_input);
        parcelle.setID_GEOLOCALISATION_PARCELLE(geolocalisation_input.getId());
        parcelle_data.update(parcelle);
    }

    //Get Latitude and Longitude position of a Parcelle
    private Geolocalisation getGeolocalisationOfParcelle (PARCELLE parcelle){
        return parcelle.getGeolocalisation();
    }


    // Autor : Vincent Lacour

    //Load a parcelle only with the ID
    //Input : ID of parcelle
    //Output : PARCELLE Object
    private PARCELLE getParcelleV2(long ID_Parcelle){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PARCELLEDao parcelle_data = daoSession.getPARCELLEDao();
        PARCELLE result = null;
        List<PARCELLE> parcelle_load = parcelle_data.queryBuilder()
                .where(PARCELLEDao.Properties.Id.eq(ID_Parcelle))
                .list();
        if (parcelle_load != null) {
            for (PARCELLE P : parcelle_load) {
                result = P;
            }
        }
        return result;
    }

    //Load an Ilot only with the ID
    //Input : ID of ilot
    //Output : ILOT Object
    private ILOT getIlotV2(long ID_Ilot){
        Log.i("CheckValue", "ID_Ilot_in getilotv2: " + ID_Ilot ) ;
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ILOTDao ilot_data = daoSession.getILOTDao();
        ILOT result = null;
        List<ILOT> ilot_load = ilot_data.queryBuilder()
                .where(ILOTDao.Properties.Id.eq(ID_Ilot))
                .list();
        if (ilot_load != null) {
            for (ILOT I : ilot_load) {
                result = I;
                Log.i("CheckValue", "ILot_result_in_getilotv2: " + result ) ;
            }
        }
        return result;
    }

    // //This function is an example how to set the data on Widgets (EditText and Spinner)
    //input : ILOT objects
    private void setDataILOTToWidget(ILOT Ilot_selected){
        //Load Ilot Data
        //Spinner Ilot
        SpinnerAdapter TextadapterIlot = SpinnerIlot.getAdapter(); //Loads the list of spinner values
        for (int i = 0; i < TextadapterIlot.getCount(); i++) {
            if (TextadapterIlot.getItem(i).toString().equals(Ilot_selected.getNOM_ILOT()+Ilot_selected.getNUMERO_ILOT())) {
                SpinnerIlot.setSelection(i);
            }
        }

    }
    // Input : PARCELLE objects
    private void setDataParcelleToWidget(PARCELLE parcelle_selected){
        //Example EditText
       NOM_PARCELLE.setText(parcelle_selected.getNOM_PARCELLE().toString().trim());
       NUMERO_PARCELLE.setText(parcelle_selected.getNUMERO_PARCELLE().toString().trim());
       SURFACE.setText(String.valueOf(parcelle_selected.getSURFACE().toString().trim())); //Float into String

       //Example Spinner
        SpinnerAdapter TextAdapterStatutParcelle = SpinnerSTATUT_PARCELLE.getAdapter();
        for (int i = 0; i < TextAdapterStatutParcelle.getCount(); i++) {
            if (TextAdapterStatutParcelle.getItem(i).toString().equals(parcelle_selected.getSTATUT_PARCELLE().getNom())) {
                SpinnerSTATUT_PARCELLE.setSelection(i);
            }
        }

        //Spinner Type culture
        SpinnerAdapter TextAdapterTypeculture = SpinnerTYPE_CULTURE.getAdapter();
        for (int i = 0; i < TextAdapterTypeculture.getCount(); i++) {
            if (TextAdapterTypeculture.getItem(i).toString().equals(parcelle_selected.getTYPE_CULTURE().getNom())) {
               SpinnerTYPE_CULTURE.setSelection(i);
            }
       }

        //Spinner IRRIGATION
       SpinnerAdapter TextAdapterSpinnerIRRIGATION = SpinnerIRRIGATION.getAdapter();
        for (int i = 0; i < TextAdapterSpinnerIRRIGATION.getCount(); i++) {
            if (TextAdapterSpinnerIRRIGATION.getItem(i).toString().equals(parcelle_selected.getIRRIGATION().getNom())) {
                SpinnerIRRIGATION.setSelection(i);
            }
        }
        //Spinner Type IRRIGATION
       SpinnerAdapter TextAdapterSpinnerTYPE_IRRIGATION = SpinnerTYPE_IRRIGATION.getAdapter();
        for (int i = 0; i < TextAdapterSpinnerTYPE_IRRIGATION.getCount(); i++) {
            if (TextAdapterSpinnerTYPE_IRRIGATION.getItem(i).toString().equals(parcelle_selected.getTYPE_IRRIGATION().getNom())) {
                SpinnerTYPE_IRRIGATION.setSelection(i);
            }
        }



    }


}
