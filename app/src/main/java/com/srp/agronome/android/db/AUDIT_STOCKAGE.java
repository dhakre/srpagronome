package com.srp.agronome.android.db;

import org.greenrobot.greendao.annotation.*;

import java.util.List;
import com.srp.agronome.android.db.DaoSession;
import org.greenrobot.greendao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "AUDIT__STOCKAGE".
 */
@Entity(active = true)
public class AUDIT_STOCKAGE {

    @Id
    private Long id;
    private String REMARQUES_AUDIT_STOCKAGE ;
    private int archive;
    private int ID_Agronome_Creation;

    @NotNull
    private java.util.Date date_creation;
    private int ID_Agronome_Modification;

    @NotNull
    private java.util.Date date_modification;
    private Long ID_STOCKAGE  ;
    private Long ID_PROPRETE_AUDIT_STOCKAGE ;
    private Long ID_CONTAMINATION_CROISE   ;
    private Long ID_VENTILATION ;
    private Long ID_TEMPERATURE ;
    private Long ID_TRIAGE_GRAIN  ;
    private long ID_AUDIT ;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient AUDIT_STOCKAGEDao myDao;

    @ToOne(joinProperty = "ID_STOCKAGE  ")
    private STOCKAGE sTOCKAGE;

    @Generated
    private transient Long sTOCKAGE__resolvedKey;

    @ToOne(joinProperty = "ID_PROPRETE_AUDIT_STOCKAGE ")
    private PROPRETE pROPRETE;

    @Generated
    private transient Long pROPRETE__resolvedKey;

    @ToOne(joinProperty = "ID_CONTAMINATION_CROISE   ")
    private CONTAMINATION_CROISE cONTAMINATION_CROISE;

    @Generated
    private transient Long cONTAMINATION_CROISE__resolvedKey;

    @ToOne(joinProperty = "ID_VENTILATION ")
    private VENTILATION vENTILATION;

    @Generated
    private transient Long vENTILATION__resolvedKey;

    @ToOne(joinProperty = "ID_TEMPERATURE ")
    private TEMPERATURE tEMPERATURE;

    @Generated
    private transient Long tEMPERATURE__resolvedKey;

    @ToOne(joinProperty = "ID_TRIAGE_GRAIN  ")
    private TRIAGE_GRAIN tRIAGE_GRAIN;

    @Generated
    private transient Long tRIAGE_GRAIN__resolvedKey;

    @ToMany(joinProperties = {
        @JoinProperty(name = "id", referencedName = "ID_AUDIT_STOCKAGE ")
    })
    private List<NUISIBLE> nUISIBLEList;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public AUDIT_STOCKAGE() {
    }

    public AUDIT_STOCKAGE(Long id) {
        this.id = id;
    }

    @Generated
    public AUDIT_STOCKAGE(Long id, String REMARQUES_AUDIT_STOCKAGE , int archive, int ID_Agronome_Creation, java.util.Date date_creation, int ID_Agronome_Modification, java.util.Date date_modification, Long ID_STOCKAGE  , Long ID_PROPRETE_AUDIT_STOCKAGE , Long ID_CONTAMINATION_CROISE   , Long ID_VENTILATION , Long ID_TEMPERATURE , Long ID_TRIAGE_GRAIN  , long ID_AUDIT ) {
        this.id = id;
        this.REMARQUES_AUDIT_STOCKAGE  = REMARQUES_AUDIT_STOCKAGE ;
        this.archive = archive;
        this.ID_Agronome_Creation = ID_Agronome_Creation;
        this.date_creation = date_creation;
        this.ID_Agronome_Modification = ID_Agronome_Modification;
        this.date_modification = date_modification;
        this.ID_STOCKAGE   = ID_STOCKAGE  ;
        this.ID_PROPRETE_AUDIT_STOCKAGE  = ID_PROPRETE_AUDIT_STOCKAGE ;
        this.ID_CONTAMINATION_CROISE    = ID_CONTAMINATION_CROISE   ;
        this.ID_VENTILATION  = ID_VENTILATION ;
        this.ID_TEMPERATURE  = ID_TEMPERATURE ;
        this.ID_TRIAGE_GRAIN   = ID_TRIAGE_GRAIN  ;
        this.ID_AUDIT  = ID_AUDIT ;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getAUDIT_STOCKAGEDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getREMARQUES_AUDIT_STOCKAGE () {
        return REMARQUES_AUDIT_STOCKAGE ;
    }

    public void setREMARQUES_AUDIT_STOCKAGE (String REMARQUES_AUDIT_STOCKAGE ) {
        this.REMARQUES_AUDIT_STOCKAGE  = REMARQUES_AUDIT_STOCKAGE ;
    }

    public int getArchive() {
        return archive;
    }

    public void setArchive(int archive) {
        this.archive = archive;
    }

    public int getID_Agronome_Creation() {
        return ID_Agronome_Creation;
    }

    public void setID_Agronome_Creation(int ID_Agronome_Creation) {
        this.ID_Agronome_Creation = ID_Agronome_Creation;
    }

    @NotNull
    public java.util.Date getDate_creation() {
        return date_creation;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setDate_creation(@NotNull java.util.Date date_creation) {
        this.date_creation = date_creation;
    }

    public int getID_Agronome_Modification() {
        return ID_Agronome_Modification;
    }

    public void setID_Agronome_Modification(int ID_Agronome_Modification) {
        this.ID_Agronome_Modification = ID_Agronome_Modification;
    }

    @NotNull
    public java.util.Date getDate_modification() {
        return date_modification;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setDate_modification(@NotNull java.util.Date date_modification) {
        this.date_modification = date_modification;
    }

    public Long getID_STOCKAGE  () {
        return ID_STOCKAGE  ;
    }

    public void setID_STOCKAGE  (Long ID_STOCKAGE  ) {
        this.ID_STOCKAGE   = ID_STOCKAGE  ;
    }

    public Long getID_PROPRETE_AUDIT_STOCKAGE () {
        return ID_PROPRETE_AUDIT_STOCKAGE ;
    }

    public void setID_PROPRETE_AUDIT_STOCKAGE (Long ID_PROPRETE_AUDIT_STOCKAGE ) {
        this.ID_PROPRETE_AUDIT_STOCKAGE  = ID_PROPRETE_AUDIT_STOCKAGE ;
    }

    public Long getID_CONTAMINATION_CROISE   () {
        return ID_CONTAMINATION_CROISE   ;
    }

    public void setID_CONTAMINATION_CROISE   (Long ID_CONTAMINATION_CROISE   ) {
        this.ID_CONTAMINATION_CROISE    = ID_CONTAMINATION_CROISE   ;
    }

    public Long getID_VENTILATION () {
        return ID_VENTILATION ;
    }

    public void setID_VENTILATION (Long ID_VENTILATION ) {
        this.ID_VENTILATION  = ID_VENTILATION ;
    }

    public Long getID_TEMPERATURE () {
        return ID_TEMPERATURE ;
    }

    public void setID_TEMPERATURE (Long ID_TEMPERATURE ) {
        this.ID_TEMPERATURE  = ID_TEMPERATURE ;
    }

    public Long getID_TRIAGE_GRAIN  () {
        return ID_TRIAGE_GRAIN  ;
    }

    public void setID_TRIAGE_GRAIN  (Long ID_TRIAGE_GRAIN  ) {
        this.ID_TRIAGE_GRAIN   = ID_TRIAGE_GRAIN  ;
    }

    public long getID_AUDIT () {
        return ID_AUDIT ;
    }

    public void setID_AUDIT (long ID_AUDIT ) {
        this.ID_AUDIT  = ID_AUDIT ;
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public STOCKAGE getSTOCKAGE() {
        Long __key = this.ID_STOCKAGE  ;
        if (sTOCKAGE__resolvedKey == null || !sTOCKAGE__resolvedKey.equals(__key)) {
            __throwIfDetached();
            STOCKAGEDao targetDao = daoSession.getSTOCKAGEDao();
            STOCKAGE sTOCKAGENew = targetDao.load(__key);
            synchronized (this) {
                sTOCKAGE = sTOCKAGENew;
            	sTOCKAGE__resolvedKey = __key;
            }
        }
        return sTOCKAGE;
    }

    @Generated
    public void setSTOCKAGE(STOCKAGE sTOCKAGE) {
        synchronized (this) {
            this.sTOCKAGE = sTOCKAGE;
            ID_STOCKAGE   = sTOCKAGE == null ? null : sTOCKAGE.getId();
            sTOCKAGE__resolvedKey = ID_STOCKAGE  ;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public PROPRETE getPROPRETE() {
        Long __key = this.ID_PROPRETE_AUDIT_STOCKAGE ;
        if (pROPRETE__resolvedKey == null || !pROPRETE__resolvedKey.equals(__key)) {
            __throwIfDetached();
            PROPRETEDao targetDao = daoSession.getPROPRETEDao();
            PROPRETE pROPRETENew = targetDao.load(__key);
            synchronized (this) {
                pROPRETE = pROPRETENew;
            	pROPRETE__resolvedKey = __key;
            }
        }
        return pROPRETE;
    }

    @Generated
    public void setPROPRETE(PROPRETE pROPRETE) {
        synchronized (this) {
            this.pROPRETE = pROPRETE;
            ID_PROPRETE_AUDIT_STOCKAGE  = pROPRETE == null ? null : pROPRETE.getId();
            pROPRETE__resolvedKey = ID_PROPRETE_AUDIT_STOCKAGE ;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public CONTAMINATION_CROISE getCONTAMINATION_CROISE() {
        Long __key = this.ID_CONTAMINATION_CROISE   ;
        if (cONTAMINATION_CROISE__resolvedKey == null || !cONTAMINATION_CROISE__resolvedKey.equals(__key)) {
            __throwIfDetached();
            CONTAMINATION_CROISEDao targetDao = daoSession.getCONTAMINATION_CROISEDao();
            CONTAMINATION_CROISE cONTAMINATION_CROISENew = targetDao.load(__key);
            synchronized (this) {
                cONTAMINATION_CROISE = cONTAMINATION_CROISENew;
            	cONTAMINATION_CROISE__resolvedKey = __key;
            }
        }
        return cONTAMINATION_CROISE;
    }

    @Generated
    public void setCONTAMINATION_CROISE(CONTAMINATION_CROISE cONTAMINATION_CROISE) {
        synchronized (this) {
            this.cONTAMINATION_CROISE = cONTAMINATION_CROISE;
            ID_CONTAMINATION_CROISE    = cONTAMINATION_CROISE == null ? null : cONTAMINATION_CROISE.getId();
            cONTAMINATION_CROISE__resolvedKey = ID_CONTAMINATION_CROISE   ;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public VENTILATION getVENTILATION() {
        Long __key = this.ID_VENTILATION ;
        if (vENTILATION__resolvedKey == null || !vENTILATION__resolvedKey.equals(__key)) {
            __throwIfDetached();
            VENTILATIONDao targetDao = daoSession.getVENTILATIONDao();
            VENTILATION vENTILATIONNew = targetDao.load(__key);
            synchronized (this) {
                vENTILATION = vENTILATIONNew;
            	vENTILATION__resolvedKey = __key;
            }
        }
        return vENTILATION;
    }

    @Generated
    public void setVENTILATION(VENTILATION vENTILATION) {
        synchronized (this) {
            this.vENTILATION = vENTILATION;
            ID_VENTILATION  = vENTILATION == null ? null : vENTILATION.getId();
            vENTILATION__resolvedKey = ID_VENTILATION ;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public TEMPERATURE getTEMPERATURE() {
        Long __key = this.ID_TEMPERATURE ;
        if (tEMPERATURE__resolvedKey == null || !tEMPERATURE__resolvedKey.equals(__key)) {
            __throwIfDetached();
            TEMPERATUREDao targetDao = daoSession.getTEMPERATUREDao();
            TEMPERATURE tEMPERATURENew = targetDao.load(__key);
            synchronized (this) {
                tEMPERATURE = tEMPERATURENew;
            	tEMPERATURE__resolvedKey = __key;
            }
        }
        return tEMPERATURE;
    }

    @Generated
    public void setTEMPERATURE(TEMPERATURE tEMPERATURE) {
        synchronized (this) {
            this.tEMPERATURE = tEMPERATURE;
            ID_TEMPERATURE  = tEMPERATURE == null ? null : tEMPERATURE.getId();
            tEMPERATURE__resolvedKey = ID_TEMPERATURE ;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public TRIAGE_GRAIN getTRIAGE_GRAIN() {
        Long __key = this.ID_TRIAGE_GRAIN  ;
        if (tRIAGE_GRAIN__resolvedKey == null || !tRIAGE_GRAIN__resolvedKey.equals(__key)) {
            __throwIfDetached();
            TRIAGE_GRAINDao targetDao = daoSession.getTRIAGE_GRAINDao();
            TRIAGE_GRAIN tRIAGE_GRAINNew = targetDao.load(__key);
            synchronized (this) {
                tRIAGE_GRAIN = tRIAGE_GRAINNew;
            	tRIAGE_GRAIN__resolvedKey = __key;
            }
        }
        return tRIAGE_GRAIN;
    }

    @Generated
    public void setTRIAGE_GRAIN(TRIAGE_GRAIN tRIAGE_GRAIN) {
        synchronized (this) {
            this.tRIAGE_GRAIN = tRIAGE_GRAIN;
            ID_TRIAGE_GRAIN   = tRIAGE_GRAIN == null ? null : tRIAGE_GRAIN.getId();
            tRIAGE_GRAIN__resolvedKey = ID_TRIAGE_GRAIN  ;
        }
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    @Generated
    public List<NUISIBLE> getNUISIBLEList() {
        if (nUISIBLEList == null) {
            __throwIfDetached();
            NUISIBLEDao targetDao = daoSession.getNUISIBLEDao();
            List<NUISIBLE> nUISIBLEListNew = targetDao._queryAUDIT_STOCKAGE_NUISIBLEList(id);
            synchronized (this) {
                if(nUISIBLEList == null) {
                    nUISIBLEList = nUISIBLEListNew;
                }
            }
        }
        return nUISIBLEList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated
    public synchronized void resetNUISIBLEList() {
        nUISIBLEList = null;
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
