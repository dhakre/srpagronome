package com.srp.agronome.android.webservice.pojo;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by soytouchrp on 04/12/2017.
 */

public interface FournisseurInterfaceComplexe {


    @GET("fournisseurs2")
    Call<User> doGetListResources();

    @POST("fournisseurs2")
    Call<User> createUser(@Body User user);

    @GET("fournisseurs2/{id}")
//    Call<User> doGetUserById(@Path("id") int id);
    Call<User> doGetUserById(@Path("id") int id);

    @PUT("fournisseurs2/{id}")
//    Call<User> doUpdateUser(@Path("id") int id, @Body User user);
    Call<User> doUpdateUser(@Path("id") int id, @Body User dataUser);

    @DELETE("fournisseurs2/{id}")
//    Call<User> doUpdateUser(@Path("id") int id, @Body User user);
    Call<User> doDeleteUser(@Path("id") int id);

//    @Headers("Cache-Control: max-age=640000")
//    @GET("/users/{user}/repos")
//    List<Repo> listRepos(@Path("user") String user);
}