package com.srp.agronome.android;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class CustomIncidentAdapter extends BaseAdapter {
    private static ArrayList<Element_Incident> searchArrayList;
    private LayoutInflater mInflater;

    public CustomIncidentAdapter(Context context, ArrayList<Element_Incident> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
    }
    //counts the total number of elements from the arrayList.
    public int getCount() {
        return searchArrayList.size();
    }

    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.custom_row_incident, parent, false);
            holder = new ViewHolder();

            //Find ID Methods
            holder.txtObjetIncident = (TextView) convertView.findViewById(R.id.ObjetIncident);
            holder.txtTexteIncident = (TextView) convertView.findViewById(R.id.TextIncident);
            holder.txtDateIncident = (TextView) convertView.findViewById(R.id.DateIncident);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

            //Set Text Methods
            holder.txtObjetIncident.setText(searchArrayList.get(position).getObjetIncident());
            holder.txtTexteIncident.setText(searchArrayList.get(position).getTexteIncident());
            holder.txtDateIncident.setText(searchArrayList.get(position).getDateIncident());

        return convertView;
    }

    static class ViewHolder {
        TextView txtObjetIncident;
        TextView txtTexteIncident;
        TextView txtDateIncident;
    }
}