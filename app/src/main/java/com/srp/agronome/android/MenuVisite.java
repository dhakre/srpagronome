package com.srp.agronome.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Sujet_Visite_PrincipalDao;
import com.srp.agronome.android.db.Visite;
import com.srp.agronome.android.db.VisiteDao;
import com.srp.agronome.android.webservice.Service;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;

/**
 * Created by jitendra on 07-09-2017.
 */

public class MenuVisite extends AppCompatActivity {
    public static long Id_Visite_Selected;
    private Button list_of_visite = null;
    private Button Ajout_visite = null;
    private ImageButton imgBtnHome = null;
    //sync button to syncronize the db Local and DB server
    private ImageButton syncButton=null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_visite);

        list_of_visite = (Button) findViewById(R.id.btn_list_visite);
        list_of_visite.setOnClickListener(Button_listvisite_Handler);

        Ajout_visite = (Button) findViewById(R.id.btn_ajout_visite);
        Ajout_visite.setOnClickListener(Button_Ajoutvisite_Handler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menuvisite);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

        //sync button functionality
       syncButton= (ImageButton) findViewById(R.id.sync);
        syncButton.setOnClickListener(ButtonsyncHandler);

        //get visite data
        getVisit getData=new getVisit();
        getData.execute();

        //put visite data
        putVisite putData=new putVisite();
        //putData.execute();
    }

    View.OnClickListener Button_Ajoutvisite_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuVisite.this, CreateVisite.class);
            startActivity(intent);
            finish();
        }
    };
    View.OnClickListener Button_listvisite_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuVisite.this, ListViste.class);
            startActivity(intent);
            finish();
        }
    };
    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuVisite.this, MenuSuiviProducteur.class);
            finish();
            startActivity(intent);
        }
    };

    View.OnClickListener ButtonsyncHandler= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           // Toast.makeText(getApplicationContext(), "synchornized button action", Toast.LENGTH_SHORT).show();
            //when sync button clicked syncronize the local and server DB
            //get visite data
       //     getVisit getData=new getVisit();
        //    getData.execute();
        }
    };
    /*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Show_Dialog_Home(getString(R.string.TitleDisconnect), getString(R.string.TextDisconnect));
        }
        return true;
    }

   //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuVisite.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(MenuVisite.this, MainLogin.class);
                finish();
                startActivity(intent);

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }*/


    //get data from db for the sync
    public class getVisit extends AsyncTask<String,String,String>
    {
        HttpURLConnection urlConnection;

        @Override
        protected String doInBackground(String... params) {
            Log.i("server", "getting data from server ");
            StringBuilder visitData = new StringBuilder();
            try{
                URL url = new URL(Service.VISITEENDPOINT);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    visitData.append(line);
                    Log.i("App", line + "---------------------------------");

                }
            }catch(Exception e)
            {
                e.printStackTrace();
            }finally {
                urlConnection.disconnect();
            }

            Log.i("App", "finish background");
            //reading data from server
            try{
                JSONObject jsonObj = new JSONObject(visitData.toString());
                JSONArray jsonArray = jsonObj.getJSONArray("testData");

                for(int i=0;i<jsonArray.length();i++)
                {
                    JSONObject json = jsonArray.getJSONObject(i);
                    String archives=json.getString("archives");
                    String id_agronome_creation=json.getString("id_agronome_creation");
                    String id_attitude_entreprise=json.getString("id_attitude_entreprise");
                    String date_creation=json.getString("date_creation");
                    String date_modification=json.getString("date_modification");
                    String id_agronome_modification=json.getString("id_agronome_modification");
                    String id_entreprise=json.getString("id_entreprise");
                    String id_selection_sujet_visite_pricipal=json.getString("id_selection_sujet_visite_pricipal");
                    String id_interet=json.getString("id_interet");
                   // String remarque_contre_visite=json.getString("remarque_contre_visite");

                    //get data
                    DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
                    CompteDao compte_dao = daoSession.getCompteDao();

                    DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    Date datecreation = sourceFormat.parse(date_creation);
                    Date datemodification = sourceFormat.parse(date_modification);

                    //insert data into visit locale DB
                    VisiteDao visiteData=daoSession.getVisiteDao();
                    //table visit
                    Visite visite_inputdata=new Visite();
                    visite_inputdata.setArchive(Integer.parseInt(archives));
                    visite_inputdata.setID_Agronome_Modification(Integer.parseInt(id_agronome_modification));
                    visite_inputdata.setDate_creation(datecreation);
                    visite_inputdata.setDate_modification(datemodification);
                    visite_inputdata.setID_Agronome_Creation(Integer.parseInt(id_agronome_creation));
                    visite_inputdata.setID_Entreprise((long) Integer.parseInt(id_entreprise));
                    visite_inputdata.setID_Attitude_Entreprise((long) Integer.parseInt(id_attitude_entreprise));
                    visite_inputdata.setID_Sujet_Visite_Principal((long) Integer.parseInt(id_selection_sujet_visite_pricipal));
                    visite_inputdata.setID_Interet_Visite((long) Integer.parseInt(id_interet));
                    //Add data to table local
                    visiteData.insertOrReplace(visite_inputdata);

                    //table select_sujet_visite_principal
                    Sujet_Visite_PrincipalDao sujetData=daoSession.getSujet_Visite_PrincipalDao();



                }
            }catch(Exception e)
            {
                e.printStackTrace();
            }
            return visitData.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i("App", "debut post");

            //    dialog.dismiss();
//
        }
    }

    //post data request visite
    public class putVisite extends AsyncTask<String,String,String>
    {

        HttpURLConnection urlConnection;


        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {


            Log.i("App", "starting post request");

            StringBuilder result = new StringBuilder();


            try {
                URL url = new URL(Service.VISITEENDPOINT);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                    Log.i("App", line + "---------------------------------");

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            Log.i("App", "finished background server connection");

            try{
                //send data to server
                JSONObject jsonObj = new JSONObject(result.toString());
                JSONArray jsonArray = jsonObj.getJSONArray("testData");
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject explrObject = jsonArray.getJSONObject(i);
                    String date_creation = explrObject.getString("date_creation");
                    String date_modif = explrObject.getString("date_modification");
                    String visite_id=explrObject.getString("id");
                    int v_id=Integer.parseInt(visite_id);

                    DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    Date datemodification = sourceFormat.parse(date_modif);
                    Date datecreation= sourceFormat.parse(date_creation);

                    DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
                    CompteDao compte_dao = daoSession.getCompteDao();
                    List<Compte> update_compte = compte_dao.queryBuilder()
                            .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                            .list();
                    if (update_compte != null)
                    {
                        for (Compte c : update_compte)
                        {
                            List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                            for (Entreprise e : List_Entreprise)
                            {
                                List<com.srp.agronome.android.db.Visite> List_Visite = e.getVisiteList(); //Load All visite
                                for (com.srp.agronome.android.db.Visite visite : List_Visite)
                                {
                                    Log.i("visite", visite.getId().toString()); //print visite ID
                                    JSONObject datapost = new JSONObject();
                                    datapost.put("id_entreprise",visite.getID_Entreprise());
                                    datapost.put("id_selection_sujet_visite_pricipal",visite.getID_Sujet_Visite_Principal());
                                    datapost.put("id_attitude_entreprise",visite.getAttitude_Entreprise_Visite());
                                    datapost.put("id_interet",visite.getID_Interet_Visite());
                                    datapost.put("archives",visite.getArchive());
                                    datapost.put("id_agronome_creation",visite.getID_Agronome_Creation());
                                    datapost.put("date_creation",visite.getDate_creation());
                                    datapost.put("date_modification",visite.getDate_modification());
                                    datapost.put("id_agronome_modification",visite.getID_Agronome_Modification());

                                    //send json to server

                                    if(visite_id==null)
                                    {
                                        URL url = new URL(Service.VISITEENDPOINT);

                                        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                                        httpCon.setDoOutput(true);
                                        httpCon.setDoInput(true);
                                        httpCon.setUseCaches(false);
                                        httpCon.setRequestProperty("Content-Type", "application/json");
                                        httpCon.setRequestProperty("Accept", "application/json");
                                        httpCon.setRequestMethod("POST");
                                        OutputStream os = httpCon.getOutputStream();
                                        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                                        osw.write(datapost.toString());
                                        Log.i("post", datapost.toString());
                                        osw.flush();
                                        osw.close();
                                    }
                                    else if(comparedate(datemodification,visite.getDate_modification()))
                                    {
                                        URL url = new URL(Service.ENDPOINT + visite_id);

                                        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                                        httpCon.setDoOutput(true);
                                        httpCon.setDoInput(true);
                                        httpCon.setUseCaches(false);
                                        httpCon.setRequestProperty("Content-Type", "application/json");
                                        httpCon.setRequestProperty("Accept", "application/json");
                                        httpCon.setRequestMethod("PUT");
                                        OutputStream os = httpCon.getOutputStream();
                                        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                                        osw.write(datapost.toString());
                                        Log.i("PUT", datapost.toString());
                                        osw.flush();
                                        osw.close();
                                    }



                                }
                            }
                        }
                    }

                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }







            return result.toString();
        }
    }

    public boolean comparedate(Date date1, Date date2) {
        boolean b = true;
        if (date1.compareTo(date2) == 0) {
            b = false;
        }
        return b;
    }







}









