package com.srp.agronome.android;

/**
 * Created by agronome on 18/07/17.
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.srp.agronome.android.db.Activite;
import com.srp.agronome.android.db.ActiviteDao;
import com.srp.agronome.android.db.Adresse;
import com.srp.agronome.android.db.AdresseDao;
import com.srp.agronome.android.db.BiologiqueDao;
import com.srp.agronome.android.db.Code_Postal_Ville;
import com.srp.agronome.android.db.Code_Postal_VilleDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Pays;
import com.srp.agronome.android.db.PaysDao;
import com.srp.agronome.android.db.Region;
import com.srp.agronome.android.db.RegionDao;
import com.srp.agronome.android.db.SituationDao;
import com.srp.agronome.android.db.Statut_SoytouchDao;
import com.srp.agronome.android.db.Statut_Soytouch;
import com.srp.agronome.android.db.Structure_SocialeDao;
import com.srp.agronome.android.db.Structure_Sociale;
import com.srp.agronome.android.db.Situation;
import com.srp.agronome.android.db.Type_BioDao;
import com.srp.agronome.android.db.Type_Bio;
import com.srp.agronome.android.db.Biologique;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.Nom_Compte;


public class CreateOrganization extends AppCompatActivity {

    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    private EditText RaisonSociale = null;
    private final static String KeyRaisonSociale = "RaisonSociale";

    private Spinner Structure_Sociale = null;
    private Spinner Situation = null;
    private Spinner Statut_Soytouch = null;

    private EditText SIRET = null;
    private final static String KeySIRET = "SIRET";

    private EditText Nb_Employee = null;
    private final static String KeyNb_Employee = "Nb_Employee";

    private EditText Projet = null;
    private final static String KeyProjet = "Projet";

    private Spinner Activite_Principale = null;
    private Spinner Activite_Secondaire = null;

    private Switch Biologique = null;
    private MultiSpinner Spinner_Type_Bio = null;

    //Adresse du siege social
    private EditText Adresse_siege_1 = null;
    private static final String KeyAdresse_siege_1 = "Adresse_siege_1";
    private EditText Adresse_siege_2 = null;
    private static final String KeyAdresse_siege_2 = "Adresse_siege_2";
    private EditText CodePostal_siege = null;
    private static final String KeyCodePostal_siege = "CodePostal_siege";
    private EditText Ville_siege = null;
    private static final String KeyVille_siege = "Ville_siege";
    private EditText Region_siege = null;
    private static final String KeyRegion_siege = "Region_siege";
    private EditText Pays_siege = null;
    private static final String KeyPays_siege = "Pays_siege";

    //Adresse de facturation
    private EditText Adresse_facturation_1 = null;
    private static final String KeyAdresse_facturation_1 = "Adresse_facturation_1";
    private EditText Adresse_facturation_2 = null;
    private static final String KeyAdresse_facturation_2 = "Adresse_facturation_2";
    private EditText CodePostal_facturation = null;
    private static final String KeyCodePostal_facturation = "CodePostal_facturation";
    private EditText Ville_facturation = null;
    private static final String KeyVille_facturation = "Ville_facturation";
    private EditText Region_facturation = null;
    private static final String KeyRegion_facturation = "Region_facturation";
    private EditText Pays_facturation = null;
    private static final String KeyPays_facturation = "Pays_facturation";

    //Adresse de livraison
    private EditText Adresse_livraison_1 = null;
    private static final String KeyAdresse_livraison_1 = "Adresse_livraison_1";
    private EditText Adresse_livraison_2 = null;
    private static final String KeyAdresse_livraison_2 = "Adresse_livraison_2";
    private EditText CodePostal_livraison = null;
    private static final String KeyCodePostal_livraison = "CodePostal_livraison";
    private EditText Ville_livraison = null;
    private static final String KeyVille_livraison = "Ville_livraison";
    private EditText Region_livraison = null;
    private static final String KeyRegion_livraison = "Region_livraison";
    private EditText Pays_livraison = null;
    private static final String KeyPays_livraison = "Pays_livraison";


    private Switch CessationActivite = null;
    private EditText CauseArret = null;
    private static final String KeyCauseArret = "CauseArret";
    static TextView TextDate = null;
    static Date Date_Arret = null;

    private EditText Telephone = null;
    private static final String KeyTelephone = "Telephone";
    private EditText FAX = null;
    private static final String KeyFAX = "FAX";

    private EditText Email_1 = null;
    private static final String KeyEmail_1 = "Email_1";
    private EditText Email_2 = null;
    private static final String KeyEmail_2 = "Email_2";

    private Switch Facturation = null;
    private Switch Livraison = null;

    private ImageButton imgBtnValidate = null;
    private ImageButton imgBtnHome = null;
    private Button BtnValidate = null;




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_organization);
        FindWidgetViewbyId(); //Associate Widgets
        AddListenerEvent(); //Add Widgets Event
        GetStringData();

        //Liste de choix dans la base de données
        CreateListIntoDatabase();
        LoadSpinnerStructureSociale();
        LoadSpinnerSituation();
        LoadSpinnerStatut_Soytouch();
        LoadSpinnerActivitePrincipale();
        LoadSpinnerActiviteSecondaire();
        LoadSpinnerTypeBio();

        imgBtnValidate = (ImageButton) findViewById(R.id.btn_ajoutContact);
        imgBtnValidate.setOnClickListener(ButtonValidateHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menu);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

        BtnValidate = (Button) findViewById(R.id.btnvalidate_CQ);
        BtnValidate.setOnClickListener(ButtonValidateHandler);

        //Switch Biologique : cacher ou non les champs
        Biologique.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean checked) {
                if (checked) {
                    Spinner_Type_Bio.setVisibility(View.VISIBLE);
                } else {
                    Spinner_Type_Bio.setVisibility(View.GONE);
                }
            }
        });

        //Switch Cessation d'activité : cacher ou non les champs
        CessationActivite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean checked) {
                if (checked) {
                    CauseArret.setVisibility(View.VISIBLE);
                    TextDate.setVisibility(View.VISIBLE);
                } else {
                    CauseArret.setVisibility(View.GONE);
                    TextDate.setVisibility(View.GONE);
                    CauseArret.setText("", TextView.BufferType.EDITABLE);
                }
            }
        });

        //Switch Adresse facturation : cacher ou non les champs
        Facturation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean checked) {
                if (checked) {
                    Adresse_facturation_1.setVisibility(View.VISIBLE);
                    Adresse_facturation_2.setVisibility(View.VISIBLE);
                    CodePostal_facturation.setVisibility(View.VISIBLE);
                    Ville_facturation.setVisibility(View.VISIBLE);
                    Region_facturation.setVisibility(View.VISIBLE);
                    Pays_facturation.setVisibility(View.VISIBLE);
                } else {
                    Adresse_facturation_1.setVisibility(View.GONE);
                    Adresse_facturation_2.setVisibility(View.GONE);
                    CodePostal_facturation.setVisibility(View.GONE);
                    Ville_facturation.setVisibility(View.GONE);
                    Region_facturation.setVisibility(View.GONE);
                    Pays_facturation.setVisibility(View.GONE);
                }
            }
        });

        //Switch Adresse livraison : cacher ou non les champs
        Livraison.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean checked) {
                if (checked) {
                    Adresse_livraison_1.setVisibility(View.VISIBLE);
                    Adresse_livraison_2.setVisibility(View.VISIBLE);
                    CodePostal_livraison.setVisibility(View.VISIBLE);
                    Ville_livraison.setVisibility(View.VISIBLE);
                    Region_livraison.setVisibility(View.VISIBLE);
                    Pays_livraison.setVisibility(View.VISIBLE);
                } else {
                    Adresse_livraison_1.setVisibility(View.GONE);
                    Adresse_livraison_2.setVisibility(View.GONE);
                    CodePostal_livraison.setVisibility(View.GONE);
                    Ville_livraison.setVisibility(View.GONE);
                    Region_livraison.setVisibility(View.GONE);
                    Pays_livraison.setVisibility(View.GONE);
                }
            }
        });



    }


    private void FindWidgetViewbyId() {
        RaisonSociale = (EditText) findViewById(R.id.Organization_Name);
        Structure_Sociale = (Spinner) findViewById(R.id.SpinnerStructure);
        Situation = (Spinner) findViewById(R.id.SpinnerSituation);
        Statut_Soytouch = (Spinner) findViewById(R.id.SpinnerStatutSoytouch);

        SIRET = (EditText) findViewById(R.id.SIRET);
        Nb_Employee = (EditText) findViewById(R.id.Number_employee);
        Projet = (EditText) findViewById(R.id.Project);

        Activite_Principale = (Spinner) findViewById(R.id.SpinnerActivity1);
        Activite_Secondaire = (Spinner) findViewById(R.id.SpinnerActivity2);

        Biologique = (Switch) findViewById(R.id.SwitchBiologique);
        Spinner_Type_Bio = (MultiSpinner) findViewById(R.id.SpinnerTypeBiologique); //MultiSpinner class

        Adresse_siege_1 = (EditText) findViewById(R.id.Address11);
        Adresse_siege_2 = (EditText) findViewById(R.id.Address12);
        CodePostal_siege = (EditText) findViewById(R.id.PostalCode1);
        Ville_siege = (EditText) findViewById(R.id.City1);
        Region_siege = (EditText) findViewById(R.id.Region1);
        Pays_siege = (EditText) findViewById(R.id.Country1);

        Adresse_facturation_1 = (EditText) findViewById(R.id.Address21);
        Adresse_facturation_2 = (EditText) findViewById(R.id.Address22);
        CodePostal_facturation = (EditText) findViewById(R.id.PostalCode2);
        Ville_facturation = (EditText) findViewById(R.id.City2);
        Region_facturation = (EditText) findViewById(R.id.Region2);
        Pays_facturation = (EditText) findViewById(R.id.Country2);

        Adresse_livraison_1 = (EditText) findViewById(R.id.Address31);
        Adresse_livraison_2 = (EditText) findViewById(R.id.Address32);
        CodePostal_livraison = (EditText) findViewById(R.id.PostalCode3);
        Ville_livraison = (EditText) findViewById(R.id.City3);
        Region_livraison = (EditText) findViewById(R.id.Region3);
        Pays_livraison = (EditText) findViewById(R.id.Country3);

        CessationActivite = (Switch) findViewById(R.id.SwitchArretActivite);
        CauseArret = (EditText) findViewById(R.id.TextCauseArret);
        TextDate = (TextView) findViewById(R.id.DateArret);

        Telephone = (EditText) findViewById(R.id.CompanyPhoneNumber);
        FAX = (EditText) findViewById(R.id.CompanyFaxNumber);

        Email_1 = (EditText) findViewById(R.id.Mail1);
        Email_2 = (EditText) findViewById(R.id.Mail2);
        Facturation = (Switch) findViewById(R.id.SwitchAdresse_2);
        Livraison = (Switch) findViewById(R.id.SwitchAdresse_3);
    }

    private void AddListenerEvent() {

        RaisonSociale.addTextChangedListener(generalTextWatcher);

        SIRET.addTextChangedListener(generalTextWatcher);
        Nb_Employee.addTextChangedListener(generalTextWatcher);
        Projet.addTextChangedListener(generalTextWatcher);

        Adresse_siege_1.addTextChangedListener(generalTextWatcher);
        Adresse_siege_2.addTextChangedListener(generalTextWatcher);
        CodePostal_siege.addTextChangedListener(generalTextWatcher);
        Ville_siege.addTextChangedListener(generalTextWatcher);
        Region_siege.addTextChangedListener(generalTextWatcher);
        Pays_siege.addTextChangedListener(generalTextWatcher);

        Adresse_facturation_1.addTextChangedListener(generalTextWatcher);
        Adresse_facturation_2.addTextChangedListener(generalTextWatcher);
        CodePostal_facturation.addTextChangedListener(generalTextWatcher);
        Ville_facturation.addTextChangedListener(generalTextWatcher);
        Region_facturation.addTextChangedListener(generalTextWatcher);
        Pays_facturation.addTextChangedListener(generalTextWatcher);

        Adresse_livraison_1.addTextChangedListener(generalTextWatcher);
        Adresse_livraison_2.addTextChangedListener(generalTextWatcher);
        CodePostal_livraison.addTextChangedListener(generalTextWatcher);
        Ville_livraison.addTextChangedListener(generalTextWatcher);
        Region_livraison.addTextChangedListener(generalTextWatcher);
        Pays_livraison.addTextChangedListener(generalTextWatcher);

        CauseArret.addTextChangedListener(generalTextWatcher);
        TextDate.setOnClickListener(TextViewHandler);

        Telephone.addTextChangedListener(generalTextWatcher);
        FAX.addTextChangedListener(generalTextWatcher);

        Email_1.addTextChangedListener(generalTextWatcher);
        Email_2.addTextChangedListener(generalTextWatcher);

    }


    //Event Multiple EditText = save data
    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (RaisonSociale.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyRaisonSociale, RaisonSociale.getText().toString());
            } else if (SIRET.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeySIRET, SIRET.getText().toString());
            } else if (Nb_Employee.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyNb_Employee, Nb_Employee.getText().toString());
            } else if (Projet.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyProjet, Projet.getText().toString());

            } else if (Adresse_siege_1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAdresse_siege_1, Adresse_siege_1.getText().toString());
            } else if (Adresse_siege_2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAdresse_siege_2, Adresse_siege_2.getText().toString());
            } else if (CodePostal_siege.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCodePostal_siege, CodePostal_siege.getText().toString());
            } else if (Ville_siege.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyVille_siege, Ville_siege.getText().toString());
            } else if (Region_siege.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyRegion_siege, Region_siege.getText().toString());
            } else if (Pays_siege.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPays_siege, Pays_siege.getText().toString());

            } else if (Adresse_facturation_1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAdresse_facturation_1, Adresse_facturation_1.getText().toString());
            } else if (Adresse_facturation_2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAdresse_facturation_2, Adresse_facturation_2.getText().toString());
            } else if (CodePostal_facturation.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCodePostal_facturation, CodePostal_facturation.getText().toString());
            } else if (Ville_facturation.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyVille_facturation, Ville_facturation.getText().toString());
            } else if (Region_facturation.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyRegion_facturation, Region_facturation.getText().toString());
            } else if (Pays_facturation.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPays_facturation, Pays_facturation.getText().toString());

            } else if (Adresse_livraison_1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAdresse_livraison_1, Adresse_livraison_1.getText().toString());
            } else if (Adresse_livraison_2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAdresse_livraison_2, Adresse_livraison_2.getText().toString());
            } else if (CodePostal_livraison.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCodePostal_livraison, CodePostal_livraison.getText().toString());
            } else if (Ville_livraison.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyVille_livraison, Ville_livraison.getText().toString());
            } else if (Region_livraison.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyRegion_livraison, Region_livraison.getText().toString());
            } else if (Pays_livraison.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPays_livraison, Pays_livraison.getText().toString());

            } else if (CauseArret.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCauseArret, CauseArret.getText().toString());

            } else if (Telephone.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyTelephone, Telephone.getText().toString());
            } else if (FAX.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyFAX, FAX.getText().toString());

            } else if (Email_1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyEmail_1, Email_1.getText().toString());
            } else if (Email_2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyEmail_2, Email_2.getText().toString());
            }
        }

    };

    private void SaveStringDataInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }


    // Lire les données enregistrées et les definir dans les edit text correspondants
    private void GetStringData() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
        if (sharedpreferences.contains(KeyRaisonSociale)) {
            RaisonSociale.setText(sharedpreferences.getString(KeyRaisonSociale, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeySIRET)) {
            SIRET.setText(sharedpreferences.getString(KeySIRET, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyNb_Employee)) {
            Nb_Employee.setText(sharedpreferences.getString(KeyNb_Employee, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyProjet)) {
            Projet.setText(sharedpreferences.getString(KeyProjet, ""), TextView.BufferType.EDITABLE);
        }

        if (sharedpreferences.contains(KeyAdresse_siege_1)) {
            Adresse_siege_1.setText(sharedpreferences.getString(KeyAdresse_siege_1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAdresse_siege_2)) {
            Adresse_siege_2.setText(sharedpreferences.getString(KeyAdresse_siege_2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCodePostal_siege)) {
            CodePostal_siege.setText(sharedpreferences.getString(KeyCodePostal_siege, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyVille_siege)) {
            Ville_siege.setText(sharedpreferences.getString(KeyVille_siege, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyRegion_siege)) {
            Region_siege.setText(sharedpreferences.getString(KeyRegion_siege, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPays_siege)) {
            Pays_siege.setText(sharedpreferences.getString(KeyPays_siege, ""), TextView.BufferType.EDITABLE);
        }

        if (sharedpreferences.contains(KeyAdresse_facturation_1)) {
            Adresse_facturation_1.setText(sharedpreferences.getString(KeyAdresse_facturation_1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAdresse_facturation_2)) {
            Adresse_facturation_2.setText(sharedpreferences.getString(KeyAdresse_facturation_2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCodePostal_facturation)) {
            CodePostal_facturation.setText(sharedpreferences.getString(KeyCodePostal_facturation, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyVille_facturation)) {
            Ville_facturation.setText(sharedpreferences.getString(KeyVille_facturation, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyRegion_facturation)) {
            Region_facturation.setText(sharedpreferences.getString(KeyRegion_facturation, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPays_facturation)) {
            Pays_facturation.setText(sharedpreferences.getString(KeyPays_facturation, ""), TextView.BufferType.EDITABLE);
        }

        if (sharedpreferences.contains(KeyAdresse_livraison_1)) {
            Adresse_livraison_1.setText(sharedpreferences.getString(KeyAdresse_livraison_1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAdresse_livraison_2)) {
            Adresse_livraison_2.setText(sharedpreferences.getString(KeyAdresse_livraison_2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCodePostal_livraison)) {
            CodePostal_livraison.setText(sharedpreferences.getString(KeyCodePostal_livraison, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyVille_livraison)) {
            Ville_livraison.setText(sharedpreferences.getString(KeyVille_livraison, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyRegion_livraison)) {
            Region_livraison.setText(sharedpreferences.getString(KeyRegion_livraison, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPays_livraison)) {
            Pays_livraison.setText(sharedpreferences.getString(KeyPays_livraison, ""), TextView.BufferType.EDITABLE);
        }

        if (sharedpreferences.contains(KeyCauseArret)) {
            CauseArret.setText(sharedpreferences.getString(KeyCauseArret, ""), TextView.BufferType.EDITABLE);
        }

        if (sharedpreferences.contains(KeyTelephone)) {
            Telephone.setText(sharedpreferences.getString(KeyTelephone, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyFAX)) {
            FAX.setText(sharedpreferences.getString(KeyFAX, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyEmail_1)) {
            Email_1.setText(sharedpreferences.getString(KeyEmail_1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyEmail_2)) {
            Email_2.setText(sharedpreferences.getString(KeyEmail_2, ""), TextView.BufferType.EDITABLE);
        }
    }


    //Clear data in sharepreference
    private void clearData() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(KeyRaisonSociale);
        editor.remove(KeySIRET);
        editor.remove(KeyNb_Employee);
        editor.remove(KeyProjet);

        editor.remove(KeyAdresse_siege_1);
        editor.remove(KeyAdresse_siege_2);
        editor.remove(KeyCodePostal_siege);
        editor.remove(KeyVille_siege);
        editor.remove(KeyRegion_siege);
        editor.remove(KeyPays_siege);

        editor.remove(KeyAdresse_facturation_1);
        editor.remove(KeyAdresse_facturation_2);
        editor.remove(KeyCodePostal_facturation);
        editor.remove(KeyVille_facturation);
        editor.remove(KeyRegion_facturation);
        editor.remove(KeyPays_facturation);

        editor.remove(KeyAdresse_livraison_1);
        editor.remove(KeyAdresse_livraison_2);
        editor.remove(KeyCodePostal_livraison);
        editor.remove(KeyVille_livraison);
        editor.remove(KeyRegion_livraison);
        editor.remove(KeyPays_livraison);

        editor.remove(KeyCauseArret);

        editor.remove(KeyTelephone);
        editor.remove(KeyFAX);

        editor.remove(KeyEmail_1);
        editor.remove(KeyEmail_2);
        editor.apply();
    }


    //Bouton Home Toolbar
    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };

    //Bouton Valider Toolbar
    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {

                String number = Telephone.getText().toString().trim();
                String email = Email_1.getText().toString().trim();

                String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

                String MobilePattern="";
                String CP_Pattern = "";
                //Pattern selon Pays de selection :
                sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
                String Format_Pays = sharedpreferences.getString("FormatCode","FR");
                switch (Format_Pays){
                    case "FR" : {
                        MobilePattern = "\"(0|\\\\+33|0033)[1-9][0-9]{8}\"";
                        CP_Pattern ="^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$";
                        break;}
                    case "US" :{
                        MobilePattern = "^[0-9]{3,3}[-]{1,1}[0-9]{3,3}[-]{1,1}[0-9]{4,4}$";
                        CP_Pattern = "^[A-Z]{1,2}([0-9]{1,2}|[0-9]{1,1}[A-Z]{1,1})( |)[0-9]{1,1}[A-Z]{2,2}$";
                        break;}
                }

                MobilePattern = "[0-9]{10}"; //Ancien a supprimer si les tests sont bons

                if (email.matches(emailPattern)) {
                    if (number.matches(MobilePattern)) {
                        Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
                    } else {
                        Show_Dialog(getString(R.string.warntitle), getString(R.string.warnphone));
                    }

                } else {
                    Show_Dialog(getString(R.string.warntitle), getString(R.string.warnemail));
                }


            } else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();}
        }
    };

    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewAddOrganization);
        int position = 0 ;

        if (TextUtils.isEmpty(Email_1.getText().toString())) {
            position = Email_1.getTop();
        }
        if (TextUtils.isEmpty(Telephone.getText().toString())) {
            position = Telephone.getTop();
        }
        if ((CessationActivite.isChecked())&&(TextUtils.isEmpty(CauseArret.getText().toString()))){
            position = CauseArret.getTop();
        }
        if (TextUtils.isEmpty(Pays_siege.getText().toString())) {
            position = Pays_siege.getTop();
        }
        if (TextUtils.isEmpty(Ville_siege.getText().toString())) {
            position = Ville_siege.getTop();
        }
        if (TextUtils.isEmpty(CodePostal_siege.getText().toString())) {
            position = CodePostal_siege.getTop();
        }
        if (TextUtils.isEmpty(Adresse_siege_1.getText().toString())) {
            position = Adresse_siege_1.getTop();
        }
        if ((Biologique.isChecked())&&(Spinner_Type_Bio.getText().toString().equals(getString(R.string.TextTypeBio)))){
            position = Spinner_Type_Bio.getTop();
        }
        if (Activite_Principale.getSelectedItem().toString().equals(getString(R.string.TextActivitePrincipale))) {
            position = Activite_Principale.getTop();
        }
        if (Statut_Soytouch.getSelectedItem().toString().equals(getString(R.string.TextStatut_Soytouch))) {
            position = Statut_Soytouch.getTop();
        }
        if (Situation.getSelectedItem().toString().equals(getString(R.string.TextSituation))) {
            position = Situation.getTop();
        }
        if (Structure_Sociale.getSelectedItem().toString().equals(getString(R.string.TextStructureSociale))) {
            position = Structure_Sociale.getTop();
        }
        if (TextUtils.isEmpty(RaisonSociale.getText().toString())) {
            position = RaisonSociale.getTop();
        }

        final int Position_Y = position;
        final int Position_X = 0;
        sc.post(new Runnable() {
            public void run() {
                sc.scrollTo(Position_X, Position_Y); // these are your x and y coordinates
            }
        });
    }



    //Methode qui crée une chaine de caractère listant les données saisies.
    private String InputToText(){
        String text = getString(R.string.textConfirm) + "\n\n";

        text += getString(R.string.TextOrganizationName) + " : " + RaisonSociale.getText() + "\n";
        text += getString(R.string.TextValidateStructureSociale) + " : " + Structure_Sociale.getSelectedItem().toString() + "\n";
        text += getString(R.string.TextValidateSituation) + " : " + Situation.getSelectedItem().toString() + "\n";
        text += getString(R.string.TextValidateStatutSoytouch) + " : " + Statut_Soytouch.getSelectedItem().toString() + "\n\n";

        text += getString(R.string.TextSIRET) + " : " + SIRET.getText() + "\n" ;
        text += getString(R.string.TextNumberEmployee) + " : " + Nb_Employee.getText() + "\n";
        text += getString(R.string.TextProject) + " : " + Projet.getText() + "\n\n";

        text += getString(R.string.TextValidateActivite1) + " : " + Activite_Principale.getSelectedItem().toString() + "\n";
        if (!(Activite_Secondaire.getSelectedItem().toString().equals(getString(R.string.TextActiviteSecondaire)))) {
            text += getString(R.string.TextValidateActivite2) + " : " + Activite_Secondaire.getSelectedItem().toString() + "\n";}

        if (Biologique.isChecked()){
            text += getString(R.string.TextValidateBiologique) + " : " + getString(R.string.TextYes) + "\n";
            text += getString(R.string.TextValidateTypeBiologique) + " : " + Spinner_Type_Bio.getText().toString() + "\n\n";
        }
        else{
            text += getString(R.string.TextValidateBiologique) + " : " + getString(R.string.TextNo) + "\n\n";}

        text += getString(R.string.SectionAdressSiegeSociale) + " : " + "\n";
        text += getString(R.string.TextAddress) + " : " + Adresse_siege_1.getText().toString() + "\n";
        text += getString(R.string.TextAddress2) + " : " + Adresse_siege_2.getText().toString() + "\n";
        text += getString(R.string.TextPostalCode) + " : " + CodePostal_siege.getText().toString()+  "\n" ;
        text += getString(R.string.TextCity) + " : " + Ville_siege.getText().toString() + "\n";
        text += getString(R.string.TextState) + " : " + Region_siege.getText().toString() + "\n" ;
        text += getString(R.string.TextCountry) + " : " + Pays_siege.getText().toString() + "\n\n" ;

        if (Facturation.isChecked()){
            text += getString(R.string.SectionAdressFacturation) + " : " + "\n";
            text += getString(R.string.TextAddress) + " : " + Adresse_facturation_1.getText().toString() + "\n";
            text += getString(R.string.TextAddress2) + " : " + Adresse_facturation_2.getText().toString() + "\n";
            text += getString(R.string.TextPostalCode) + " : " + CodePostal_facturation.getText().toString()+  "\n" ;
            text += getString(R.string.TextCity) + " : " + Ville_facturation.getText().toString() + "\n";
            text += getString(R.string.TextState) + " : " + Region_facturation.getText().toString() + "\n" ;
            text += getString(R.string.TextCountry) + " : " + Pays_facturation.getText().toString() + "\n\n" ;
        }

        if (Livraison.isChecked()){
            text += getString(R.string.SectionAdressLivraison) + " : " + "\n";
            text += getString(R.string.TextAddress) + " : " + Adresse_livraison_1.getText().toString() + "\n";
            text += getString(R.string.TextAddress2) + " : " + Adresse_livraison_2.getText().toString() + "\n";
            text += getString(R.string.TextPostalCode) + " : " + CodePostal_livraison.getText().toString()+  "\n" ;
            text += getString(R.string.TextCity) + " : " + Ville_livraison.getText().toString() + "\n";
            text += getString(R.string.TextState) + " : " + Region_livraison.getText().toString() + "\n" ;
            text += getString(R.string.TextCountry) + " : " + Pays_livraison.getText().toString() + "\n\n" ;
        }

        if (CessationActivite.isChecked()){
            text += getString(R.string.TextValidateCessation) + " : " + getString(R.string.TextYes) + "\n" ;
            text += getString(R.string.TextCauseArret) + " : " + CauseArret.getText() + "\n" ;
            text += getString(R.string.TextDateArret) + " : " + TextDate.getText() + " \n\n" ;}
        else {
            text += getString(R.string.TextValidateCessation) + " : " + getString(R.string.TextNo) + "\n\n" ;}

        text += getString(R.string.TextPhoneNumberQC) + " : " + Telephone.getText() + "\n";
        text += getString(R.string.TextFax) + " : " + FAX.getText() + "\n\n";

        text += getString(R.string.TextMailPrincipale) + " : " + Email_1.getText() + "\n";
        text += getString(R.string.TextMailSecondaire) + " : " + Email_2.getText() + "\n";
        return text ;
    }
    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrganization.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                CreateEntrepriseIntoDatabase();
                Intent intent = new Intent(CreateOrganization.this, ContactMenu.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
        return true;
    }

    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrganization.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(CreateOrganization.this, ContactMenu.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                Intent intent = new Intent(CreateOrganization.this, ContactMenu.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrganization.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

    //Methode qui permet de créer les listes de choix dans la base de données.
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Structure_SocialeDao Structure_Sociale_data = daoSession.getStructure_SocialeDao();
        SituationDao Situation_data = daoSession.getSituationDao();
        Statut_SoytouchDao Statut_Soytouch_data = daoSession.getStatut_SoytouchDao();
        ActiviteDao Activite_data = daoSession.getActiviteDao();
        Type_BioDao Type_Bio_data = daoSession.getType_BioDao();
        BiologiqueDao Biologique_data = daoSession.getBiologiqueDao();

        List<Structure_Sociale> List_Structure = Structure_Sociale_data.loadAll();
        if (List_Structure.size() == 0) {

            Structure_Sociale Structure_Sociale_input_1 = new Structure_Sociale();
            Structure_Sociale_input_1.setNom("EARL");
            Structure_Sociale_data.insertOrReplace(Structure_Sociale_input_1);

            Structure_Sociale Structure_Sociale_input_2 = new Structure_Sociale();
            Structure_Sociale_input_2.setNom("Gaec");
            Structure_Sociale_data.insertOrReplace(Structure_Sociale_input_2);

            Structure_Sociale Structure_Sociale_input_3 = new Structure_Sociale();
            Structure_Sociale_input_3.setNom("SCEA");
            Structure_Sociale_data.insertOrReplace(Structure_Sociale_input_3);

            Structure_Sociale Structure_Sociale_input_4 = new Structure_Sociale();
            Structure_Sociale_input_4.setNom("SA");
            Structure_Sociale_data.insertOrReplace(Structure_Sociale_input_4);

            Structure_Sociale Structure_Sociale_input_5 = new Structure_Sociale();
            Structure_Sociale_input_5.setNom("SARL");
            Structure_Sociale_data.insertOrReplace(Structure_Sociale_input_5);

        }

        List <Situation> List_Situation = Situation_data.loadAll();
        if (List_Situation.size() == 0){

            Situation Situation_input_1 = new Situation();
            Situation_input_1.setNom("Un exploitant");
            Situation_data.insertOrReplace(Situation_input_1);

            Situation Situation_input_2 = new Situation();
            Situation_input_2.setNom("Plusieurs exploitations");
            Situation_data.insertOrReplace(Situation_input_2);

            Situation Situation_input_3 = new Situation();
            Situation_input_3.setNom("Familial");
            Situation_data.insertOrReplace(Situation_input_3);

            Situation Situation_input_4 = new Situation();
            Situation_input_4.setNom("Non familial");
            Situation_data.insertOrReplace(Situation_input_4);
        }

        List<Statut_Soytouch> List_Statut_Soytouch = Statut_Soytouch_data.loadAll();
        if (List_Statut_Soytouch.size() == 0){

            Statut_Soytouch Statut_Soytouch_input_1 = new Statut_Soytouch();
            Statut_Soytouch_input_1.setNom("Producteur");
            Statut_Soytouch_data.insertOrReplace(Statut_Soytouch_input_1);

            Statut_Soytouch Statut_Soytouch_input_2 = new Statut_Soytouch();
            Statut_Soytouch_input_2.setNom("Prospect");
            Statut_Soytouch_data.insertOrReplace(Statut_Soytouch_input_2);

            Statut_Soytouch Statut_Soytouch_input_3 = new Statut_Soytouch();
            Statut_Soytouch_input_3.setNom("Pas soja");
            Statut_Soytouch_data.insertOrReplace(Statut_Soytouch_input_3);

        }

        List <Activite> List_Activite = Activite_data.loadAll();
        if (List_Activite.size() == 0){

            Activite Activite_input_1 = new Activite();
            Activite_input_1.setNom("Céréalier");
            Activite_data.insertOrReplace(Activite_input_1);

            Activite Activite_input_2 = new Activite();
            Activite_input_2.setNom("Arboriculteur");
            Activite_data.insertOrReplace(Activite_input_2);

            Activite Activite_input_3 = new Activite();
            Activite_input_3.setNom("Viticulteur");
            Activite_data.insertOrReplace(Activite_input_3);

            Activite Activite_input_4 = new Activite();
            Activite_input_4.setNom("Elevage");
            Activite_data.insertOrReplace(Activite_input_4);

            Activite Activite_input_5 = new Activite();
            Activite_input_5.setNom("Entrepreneur agricole");
            Activite_data.insertOrReplace(Activite_input_5);

        }

        List <Type_Bio> List_Type_Bio = Type_Bio_data.loadAll();
        if (List_Type_Bio.size() == 0){

            Type_Bio Type_Bio_input_1 = new Type_Bio();
            Type_Bio_input_1.setNom("CE");
            Type_Bio_data.insertOrReplace(Type_Bio_input_1);

            Type_Bio Type_Bio_input_2 = new Type_Bio();
            Type_Bio_input_2.setNom("NOP");
            Type_Bio_data.insertOrReplace(Type_Bio_input_2);

            Type_Bio Type_Bio_input_3 = new Type_Bio();
            Type_Bio_input_3.setNom("BIOSUISSE");
            Type_Bio_data.insertOrReplace(Type_Bio_input_3);

            Type_Bio Type_Bio_input_4 = new Type_Bio();
            Type_Bio_input_4.setNom("JAS");
            Type_Bio_data.insertOrReplace(Type_Bio_input_4);

        }

        List<com.srp.agronome.android.db.Biologique> List_Biologique = Biologique_data.loadAll();
        if (List_Biologique.size() == 0){

            com.srp.agronome.android.db.Biologique Biologique_input_1 = new Biologique();
            Biologique_input_1.setNom("Oui");
            Biologique_data.insertOrReplace(Biologique_input_1);

            com.srp.agronome.android.db.Biologique Biologique_input_2 = new Biologique();
            Biologique_input_2.setNom("Non");
            Biologique_data.insertOrReplace(Biologique_input_2);
        }
    }

    //Charger le spinner Structure Sociale
    private void LoadSpinnerStructureSociale() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Structure_SocialeDao Structure_Sociale_data = daoSession.getStructure_SocialeDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherStructureSociale)); //Text Add a new structure sociale
        //Charger les éléments de la liste
        List<Structure_Sociale> List_Structure = Structure_Sociale_data.loadAll();
        if (List_Structure != null) {
            for (Structure_Sociale SS : List_Structure) {
                adapter.add(SS.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextStructureSociale)); //This is the text that will be displayed as hint.
        Structure_Sociale.setAdapter(adapter);
        Structure_Sociale.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Structure_Sociale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidateStructureSociale),getString(R.string.TextInputStructureSociale),1);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Charger le spinner Situation
    private void LoadSpinnerSituation() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SituationDao Situation_data = daoSession.getSituationDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherSituation));
        //Charger les éléments de la liste
        List<Situation> List_Situation = Situation_data.loadAll();
        if (List_Situation != null) {
            for (Situation S : List_Situation) {
                adapter.add(S.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextSituation)); //This is the text that will be displayed as hint.
        Situation.setAdapter(adapter);
        Situation.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Situation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une entreprise
                Show_Dialog_EditText(getString(R.string.TextValidateSituation),getString(R.string.TextInputSituation),2);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Charger le spinner Statut_Soytouch
    private void LoadSpinnerStatut_Soytouch() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Statut_SoytouchDao Statut_Soytouch_data = daoSession.getStatut_SoytouchDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter.add(getString(R.string.TextAnotherStatut));
        //Charger les éléments de la liste
        List<Statut_Soytouch> List_Statut_Soytouch = Statut_Soytouch_data.loadAll();
        if (List_Statut_Soytouch != null) {
            for (Statut_Soytouch Statut : List_Statut_Soytouch) {
                adapter.add(Statut.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextStatut_Soytouch)); //This is the text that will be displayed as hint.
        Statut_Soytouch.setAdapter(adapter);
        Statut_Soytouch.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Statut_Soytouch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une entreprise
                Show_Dialog_EditText(getString(R.string.TextValidateStatutSoytouch),getString(R.string.TextInputStatut),5);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Charger le spinner Activite principale
    private void LoadSpinnerActivitePrincipale() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ActiviteDao Activite_data = daoSession.getActiviteDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter.add(getString(R.string.TextAnotherActivite));

        //Charger les éléments de la liste
        List<Activite> List_Activite = Activite_data.loadAll();
        if (List_Activite != null) {
            for (Activite A : List_Activite) {
                adapter.add(A.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextActivitePrincipale)); //This is the text that will be displayed as hint.
        Activite_Principale.setAdapter(adapter);
        Activite_Principale.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Activite_Principale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une activite
                Show_Dialog_EditText(getString(R.string.TextValidateActivite1),getString(R.string.TextInputActivite),3);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Charger le spinner Activite secondaire
    private void LoadSpinnerActiviteSecondaire() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ActiviteDao Activite_data = daoSession.getActiviteDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherActivite));
        //Charger les éléments de la liste
        List<Activite> List_Activite = Activite_data.loadAll();
        if (List_Activite != null) {
            for (Activite A : List_Activite) {
                adapter.add(A.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextActiviteSecondaire)); //This is the text that will be displayed as hint.
        Activite_Secondaire.setAdapter(adapter);
        Activite_Secondaire.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Activite_Secondaire.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une entreprise
                Show_Dialog_EditText(getString(R.string.TextValidateActivite1),getString(R.string.TextInputActivite),3);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    // A changer : Charger le spinner Type Bio ==> Choix Multiple
    private void LoadSpinnerTypeBio() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Type_BioDao Type_Bio_data = daoSession.getType_BioDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste
        List<Type_Bio> List_Type_Bio = Type_Bio_data.loadAll();
        if (List_Type_Bio != null) {
            for (Type_Bio TB : List_Type_Bio) {
                adapter.add(TB.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextAnotherTypeBio));
        adapter.add(getString(R.string.TextTypeBio));
        Spinner_Type_Bio.setAdapter(adapter, false, onSelectedListener); //All selected = False
        Spinner_Type_Bio.setDefaultText(getString(R.string.TextTypeBio));
    }

    //Event MultiSpinnerTypeBio
    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            if (selected[selected.length-1]){
                Show_Dialog_EditText(getString(R.string.TextValidateTypeBiologique),getString(R.string.TextInputTypeBio),4);
            }

        }
    };



    //Methode qui permet de vérifier si l'utilisateurs a saisie les données
    private Boolean CheckInput(){
        boolean b = true;

        //Vérification des texte editables
        if (TextUtils.isEmpty(RaisonSociale.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Adresse_siege_1.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(CodePostal_siege.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Ville_siege.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Pays_siege.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Telephone.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Email_1.getText().toString())) {
            b = false;
        }

        //Vérification des listes de choix :
        if (Structure_Sociale.getSelectedItem().toString().equals(getString(R.string.TextStructureSociale))) {
            b = false;
        }
        if (Situation.getSelectedItem().toString().equals(getString(R.string.TextSituation))) {
            b = false;
        }
        if (Statut_Soytouch.getSelectedItem().toString().equals(getString(R.string.TextStatut_Soytouch))) {
            b = false;
        }
        if (Activite_Principale.getSelectedItem().toString().equals(getString(R.string.TextActivitePrincipale))) {
            b = false;
        }
        //Verification avec les switch
        if ((Biologique.isChecked())&&(Spinner_Type_Bio.getText().toString().equals(getString(R.string.TextTypeBio)))){
            b = false;
        }
        if ((CessationActivite.isChecked())&&(TextUtils.isEmpty(CauseArret.getText().toString()))){
            b = false;
        }
        return b;
    }

    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrganization.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code Structure Sociale
                        AddStructureSocialeDB(NewValueList);
                        break;
                    case 2 : //Code Situation
                        AddSituationDB(NewValueList);
                        break;
                    case 3 : //Code Activite
                        AddActiviteDB(NewValueList);
                        break;
                    case 4 : //Code TypeBio
                        AddTypeBioDB(NewValueList);
                        break;
                    case 5 : //Code Statut SoyTouch
                        AddStatutDB(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void AddStructureSocialeDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Structure_SocialeDao Structure_Sociale_data = daoSession.getStructure_SocialeDao();
        Structure_Sociale Structure_Sociale_input = new Structure_Sociale();
        Structure_Sociale_input.setNom(Nom);
        Structure_Sociale_data.insertOrReplace(Structure_Sociale_input);
        LoadSpinnerStructureSociale();
    }

    private void AddSituationDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SituationDao Situation_data = daoSession.getSituationDao();
        Situation Situation_input = new Situation();
        Situation_input.setNom(Nom);
        Situation_data.insertOrReplace(Situation_input);
        LoadSpinnerSituation();
    }

    private void AddActiviteDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ActiviteDao Activite_data = daoSession.getActiviteDao();
        Activite Activite_input = new Activite();
        Activite_input.setNom(Nom);
        Activite_data.insertOrReplace(Activite_input);
        LoadSpinnerActivitePrincipale();
        LoadSpinnerActiviteSecondaire();
    }

    private void AddTypeBioDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Type_BioDao Type_Bio_data = daoSession.getType_BioDao();
        Type_Bio Type_Bio_input = new Type_Bio();
        Type_Bio_input.setNom(Nom);
        Type_Bio_data.insertOrReplace(Type_Bio_input);
        LoadSpinnerTypeBio();
    }

    private void AddStatutDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Statut_SoytouchDao Statut_Soytouch_data = daoSession.getStatut_SoytouchDao();
        Statut_Soytouch Statut_Soytouch_input = new Statut_Soytouch();
        Statut_Soytouch_input.setNom(Nom);
        Statut_Soytouch_data.insertOrReplace(Statut_Soytouch_input);
        LoadSpinnerStatut_Soytouch();
    }

    View.OnClickListener TextViewHandler = new View.OnClickListener() {
        public void onClick(View v) {
            showDatePickerDialog(v);
        }
    };

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new CreateOrganization.DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Date affichée à l'ouverture de la boite de dialogue
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Crée une nouvelle instance et la retourne
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (month <= 8) {
                TextDate.setText(day + "/0" + (month + 1) + "/" + year);
                Date_Arret = Create_date(year, month, day);

            } else {
                TextDate.setText(day + "/" + (month + 1) + "/" + year);
                Date_Arret  = Create_date(year, month, day);
            }
        }
    }

    /**
     * Retourne une date.
     *
     * @param year  Année
     * @param month Mois
     * @param day   Jour
     * @return Retourne la date sous le format "Date"
     */
    public static Date Create_date(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Retourne la date et l'heure actuelle
     *
     * @return Retourne la date et l'heure actuelle sous le format Date
     */

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    private void CreateDirectoryOrganization(String OrganizationName){
        File OrganizationDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + OrganizationName + "/Contacts" );
        if (!OrganizationDir.exists()) {
            if (!OrganizationDir.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
        File OrganizationDir2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + OrganizationName + "/Audits" );
        if (!OrganizationDir2.exists()) {
            if (!OrganizationDir2.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
    }


    private void CreateEntrepriseIntoDatabase(){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
        SituationDao situation_data = daoSession.getSituationDao();
        Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
        ActiviteDao activite_data = daoSession.getActiviteDao();
        BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
        Type_BioDao type_bio_data = daoSession.getType_BioDao();

        AdresseDao adresse_data = daoSession.getAdresseDao();
        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
        PaysDao pays_data = daoSession.getPaysDao();
        RegionDao region_data = daoSession.getRegionDao();

        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

        //Table Entreprise
        Entreprise entreprise_input = new Entreprise();
        entreprise_input.setRaison_sociale(RaisonSociale.getText().toString().trim());
        if (!(TextUtils.isEmpty(Nb_Employee.getText().toString()))) {
            entreprise_input.setNombre_salarie(Integer.parseInt(Nb_Employee.getText().toString().trim()));}
        entreprise_input.setSIRET(SIRET.getText().toString().trim());
        entreprise_input.setProjet_connu(Projet.getText().toString().trim());
        entreprise_input.setTelephone_entreprise(Telephone.getText().toString().trim());
        entreprise_input.setFax_entreprise(FAX.getText().toString().trim());
        entreprise_input.setAdresse_email_principale(Email_1.getText().toString().trim());
        entreprise_input.setAdresse_email_secondaire(Email_2.getText().toString().trim());

        entreprise_input.setArchive(0);
        entreprise_input.setDate_creation(Today_Date());
        entreprise_input.setDate_modification(Today_Date());
        entreprise_input.setID_Agronome_Creation(1);
        entreprise_input.setID_Agronome_Modification(1);

        //Ajout de la table Situation
        List<Situation> List_Situation = situation_data.queryBuilder()
                .where(SituationDao.Properties.Nom.eq(Situation.getSelectedItem().toString()))
                .list();
        if (List_Situation != null) {
            for (Situation S : List_Situation) {
                entreprise_input.setSituation(S);
                entreprise_input.setID_Situation(S.getId());}}

        //Ajout de la table Structure Sociale
        List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                .where(Structure_SocialeDao.Properties.Nom.eq(Structure_Sociale.getSelectedItem().toString()))
                .list();
        if (List_Structure_Sociale != null) {
            for (Structure_Sociale SS : List_Structure_Sociale) {
                entreprise_input.setStructure_Sociale(SS);
                entreprise_input.setID_Structure_Sociale(SS.getId());}}

        //Ajout de la table Statut SoyTouch
        List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                .where(Statut_SoytouchDao.Properties.Nom.eq(Statut_Soytouch.getSelectedItem().toString()))
                .list();
        if (List_Statut != null) {
            for (Statut_Soytouch Sst : List_Statut) {
                entreprise_input.setStatut_Soytouch(Sst);
                entreprise_input.setID_Statut_Soytouch(Sst.getId());}}

        //Ajout de la table Activite Principale
        List<Activite> List_Activite_P = activite_data.queryBuilder()
                .where(ActiviteDao.Properties.Nom.eq(Activite_Principale.getSelectedItem().toString()))
                .list();
        if (List_Activite_P  != null) {
            for (Activite AP : List_Activite_P ) {
                entreprise_input.setActivite_Principale(AP);
                entreprise_input.setID_Activite_Principale(AP.getId());}}

        //Ajout de la table Activite Secondaire si renseignée
        if (!(Activite_Principale.getSelectedItem().toString().equals(getString(R.string.TextActivitePrincipale)))) {
            List<Activite> List_Activite_S = activite_data.queryBuilder()
                    .where(ActiviteDao.Properties.Nom.eq(Activite_Secondaire.getSelectedItem().toString()))
                    .list();
            if (List_Activite_S  != null) {
                for (Activite AS : List_Activite_S ) {
                    entreprise_input.setActivite_Secondaire(AS);
                    entreprise_input.setID_Activite_Secondaire(AS.getId());}}
        }



        //Ajout de la table Biologique
        if(Biologique.isChecked()){
            List<Biologique> List_Bio = biologique_data.queryBuilder()
                    .where(BiologiqueDao.Properties.Nom.eq("Oui"))
                    .list();
            if (List_Bio  != null) {
                for (Biologique B : List_Bio ) {
                    entreprise_input.setBiologique(B);
                    entreprise_input.setID_Biologique(B.getId());}}
        }
        else {
            List<Biologique> List_Bio = biologique_data.queryBuilder()
                    .where(BiologiqueDao.Properties.Nom.eq("Non"))
                    .list();
            if (List_Bio  != null) {
                for (Biologique B : List_Bio ) {
                    entreprise_input.setBiologique(B);
                    entreprise_input.setID_Biologique(B.getId());}}
        }


        //Ajout de la table TypeBio si entreprise Bio : Selection Multiple

//        if (typebioselected[0]) Log.i("EventChange" , TextAdapter.getItem(0).toString());
//        if (typebioselected[1]) Log.i("EventChange" , TextAdapter.getItem(1).toString());
//        if (typebioselected[2]) Log.i("EventChange" , TextAdapter.getItem(2).toString());
//        if (typebioselected[3]) Log.i("EventChange" , TextAdapter.getItem(3).toString());


        //Ajout des champs cessation d'activité
        if(CessationActivite.isChecked()){
            entreprise_input.setArret_activite(true);
            entreprise_input.setCause_arret_activite(CauseArret.getText().toString().trim());
            entreprise_input.setDate_arret_activite(Date_Arret);
        }
        else{
            entreprise_input.setArret_activite(false);
        }


        //Adresse_siege_social = Adresse de l'entreprise
        Adresse adresse_siege_social_input = new Adresse();
        adresse_siege_social_input.setAdresse_numero_rue(Adresse_siege_1.getText().toString().trim()); //Adresse
        adresse_siege_social_input.setComplement_adresse(Adresse_siege_2.getText().toString().trim()); //Complement Adresse
        adresse_siege_social_input.setArchive(0);
        adresse_siege_social_input.setDate_creation(Today_Date());
        adresse_siege_social_input.setDate_modification(Today_Date());
        adresse_siege_social_input.setID_Agronome_Creation(1);
        adresse_siege_social_input.setID_Agronome_Modification(1);

        //Code postal adresse siège sociale
        Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

        code_postal_ville_siege_input.setCode_postal(CodePostal_siege.getText().toString().trim());

        code_postal_ville_siege_input.setVille(Ville_siege.getText().toString().trim());
        code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

        //Pays adresse siège
        Pays pays_siege_input = new Pays();
        pays_siege_input.setNom(Pays_siege.getText().toString().trim());
        pays_data.insertOrReplace(pays_siege_input);

        //Région adresse siège
        Region region_siege_input = new Region();
        region_siege_input.setNom(Region_siege.getText().toString().trim());
        region_data.insertOrReplace(region_siege_input);

        adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
        adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
        adresse_siege_social_input.setPays(pays_siege_input);
        adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
        adresse_siege_social_input.setRegion(region_siege_input);
        adresse_siege_social_input.setID_Region(region_siege_input.getId());
        adresse_data.insertOrReplace(adresse_siege_social_input);

        entreprise_input.setAdresse_Siege(adresse_siege_social_input);
        entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

        if (Facturation.isChecked()){
            //Adresse_facturation
            Adresse adresse_facturation_input = new Adresse();
            adresse_facturation_input.setAdresse_numero_rue(Adresse_facturation_1.getText().toString().trim()); //Adresse
            adresse_facturation_input.setComplement_adresse(Adresse_facturation_2.getText().toString().trim()); //Complement Adresse
            adresse_facturation_input.setArchive(0);
            adresse_facturation_input.setDate_creation(Today_Date());
            adresse_facturation_input.setDate_modification(Today_Date());
            adresse_facturation_input.setID_Agronome_Creation(1);
            adresse_facturation_input.setID_Agronome_Modification(1);

            //Code postal adresse facturation
            Code_Postal_Ville code_postal_ville_facturation_input = new Code_Postal_Ville();
            code_postal_ville_facturation_input.setCode_postal(CodePostal_facturation.getText().toString().trim());
            code_postal_ville_facturation_input.setVille(Ville_facturation.getText().toString().trim());
            code_postal_ville_data.insertOrReplace(code_postal_ville_facturation_input);

            //Pays adresse facturation
            Pays pays_facturation_input = new Pays();
            pays_facturation_input.setNom(Pays_facturation.getText().toString().trim());
            pays_data.insertOrReplace(pays_facturation_input);

            //Région adresse facturation
            Region region_facturation_input = new Region();
            region_facturation_input.setNom(Region_facturation.getText().toString().trim());
            region_data.insertOrReplace(region_facturation_input);

            adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_facturation_input);
            adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_facturation_input.getId());
            adresse_facturation_input.setPays(pays_facturation_input);
            adresse_facturation_input.setID_Pays(pays_facturation_input.getId());
            adresse_facturation_input.setRegion(region_facturation_input);
            adresse_facturation_input.setID_Region(region_facturation_input.getId());
            adresse_data.insertOrReplace(adresse_facturation_input);

            entreprise_input.setAdresse_Facturation(adresse_facturation_input);
            entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
        }
        else{
            entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
            entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
        }

        if (Livraison.isChecked()){
            //Adresse livraison
            Adresse adresse_livraison_input = new Adresse();
            adresse_livraison_input.setAdresse_numero_rue(Adresse_livraison_1.getText().toString().trim()); //Adresse
            adresse_livraison_input.setComplement_adresse(Adresse_livraison_2.getText().toString().trim()); //Complement Adresse
            adresse_livraison_input.setArchive(0);
            adresse_livraison_input.setDate_creation(Today_Date());
            adresse_livraison_input.setDate_modification(Today_Date());
            adresse_livraison_input.setID_Agronome_Creation(1);
            adresse_livraison_input.setID_Agronome_Modification(1);

            //Code postal adresse livraison
            Code_Postal_Ville code_postal_ville_livraison_input = new Code_Postal_Ville();
            code_postal_ville_livraison_input.setCode_postal(CodePostal_livraison.getText().toString().trim());
            code_postal_ville_livraison_input.setVille(Ville_livraison.getText().toString().trim());
            code_postal_ville_data.insertOrReplace(code_postal_ville_livraison_input);

            //Pays adresse livraison
            Pays pays_livraison_input = new Pays();
            pays_livraison_input.setNom(Pays_livraison.getText().toString().trim());
            pays_data.insertOrReplace(pays_livraison_input);

            //Région adresse livraison
            Region region_livraison_input = new Region();
            region_livraison_input.setNom(Region_livraison.getText().toString().trim());
            region_data.insertOrReplace(region_livraison_input);

            adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_livraison_input);
            adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_livraison_input.getId());
            adresse_livraison_input.setPays(pays_livraison_input);
            adresse_livraison_input.setID_Pays(pays_livraison_input.getId());
            adresse_livraison_input.setRegion(region_livraison_input);
            adresse_livraison_input.setID_Region(region_livraison_input.getId());
            adresse_data.insertOrReplace(adresse_livraison_input);

            entreprise_input.setAdresse_Livraison(adresse_livraison_input);
            entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
        }
        else{
            entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
            entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
        }

        entreprise_data.insertOrReplace(entreprise_input);


        if ((Biologique.isChecked())&&(!(Spinner_Type_Bio.getText().toString().equals(getString(R.string.TextTypeBio))))){
            boolean[] typebioselected = Spinner_Type_Bio.getSelected(); //Charge le tableau des booleans
            SpinnerAdapter TextAdapter = Spinner_Type_Bio.getAdapter(); //Charge la liste des valeurs

            List <Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                    .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                    .list();
            if (List_Entreprise_cible != null){
                for (Entreprise e : List_Entreprise_cible) {
                    for(int i = 0; i <= (typebioselected.length-2) ; i++){ //Parcours de tous les champs
                        if (typebioselected[i]){ //Si element est coché
                            List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                    .where(Type_BioDao.Properties.Nom.eq(TextAdapter.getItem(i).toString()))
                                    .list();
                            if (List_Type_Bio  != null) {
                                for (Type_Bio TB : List_Type_Bio ) {
                                    TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                    //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                    type_bio_data.insertOrReplace(TB);
                                    e.update();
                                }}
                        }
                    }
                }}
        }


        //Associer la nouvelle entreprise au compte
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                entreprise_input.setID_Compte(c.getId());
                entreprise_data.insertOrReplace(entreprise_input);
                c.update();
            }
        }

        CreateDirectoryOrganization(RaisonSociale.getText().toString().trim());

//        //Afficher la liste des entreprise associé à un compte dans les logs
//        List <Compte> List_Compte = compte_dao.queryBuilder()
//                .where(CompteDao.Properties.Login.eq(Nom_Compte))
//                .list();
//        if (List_Compte != null){
//            for (Compte c : List_Compte) {
//                List<Entreprise> List_Entreprise = c.getEntrepriseList();
//               for (Entreprise e : List_Entreprise){
//                   Log.i("Liste Entreprise","Raison : " + e.getRaison_sociale());
//                   List<Type_Bio> ListTB = e.getType_BioList();
//                        for (Type_Bio TB : ListTB){
//                            Log.i("ListTypeBio","TypeBio : " + TB.getNom());
//                         }
//               }
//            }
//        }

    }

}