package com.srp.agronome.android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

//common activity for thr list view

public abstract class BasicListActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_list);
        mapWidget();

    }

    public void mapWidget()
    {
        listView= (ListView) findViewById(R.id.listview);
    }

}
