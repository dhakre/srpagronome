package com.srp.agronome.android.db;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "ETAT__NUISIBLE".
 */
@Entity
public class ETAT_NUISIBLE {

    @Id
    private Long id;
    private String nom;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public ETAT_NUISIBLE() {
    }

    public ETAT_NUISIBLE(Long id) {
        this.id = id;
    }

    @Generated
    public ETAT_NUISIBLE(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
