package com.srp.agronome.android.webservice;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.util.Log;

import com.srp.agronome.android.AppController;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.Contact;
import com.srp.agronome.android.db.ContactDao;
import com.srp.agronome.android.db.DaoMaster;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;

/**
 * Created by doucoure on 23/08/2017.
 */

public class ConnectivityReceiver extends BroadcastReceiver {
    Call<Contact> call;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            Log.d("ConnectivityReceiver", "ConnectivityReceiver invoked...");
//            Service api = Communication.getApiService();
//
//
//            DaoMaster daoMaster = new DaoMaster();
//            DaoSession daoSession = daoMaster.newSession();
//
//
//            ContactDao contactDao = daoSession.getContactDao();
//            List<Contact> list_contact = contactDao.queryBuilder()
//                    .list();
//            if (list_contact != null) {
//
//                call = api.postMyContact(list_contact.get(1));
//            }


            //
            //Creating an object of our api interface


            /**
             * Calling JSON
             */


            /**
             * Enqueue Callback will be call when get response...
             */
            call.enqueue(new Callback<Contact>() {
                @Override
                public void onResponse(Call<Contact> call, Response<Contact> response) {

                    /*if (response.isSuccessful()) {
                        /**
                         * Got Successfully



                    }*/

                    if (response.code() == 200 && response.isSuccessful()) {
                        // all is good
                    } else {
                        // something went wrong;
                    }

                }

                @Override
                public void onFailure(Call<Contact> call, Throwable t) {

                }
            });


        }
    }

    /**
     * Enables ConnectivityReceiver
     *
     * @param context
     */
    public static void enableReceiver(Context context) {
        ComponentName component = new ComponentName(context, ConnectivityReceiver.class);

        context.getPackageManager().setComponentEnabledSetting(component,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
    }

    /**
     * Disables ConnectivityReceiver
     *
     * @param context
     */
    public static void disableReceiver(Context context) {
        ComponentName component = new ComponentName(context, ConnectivityReceiver.class);

        context.getPackageManager().setComponentEnabledSetting(component,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
    }

}
