package com.srp.agronome.android;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.DaoSession;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.internal.TableStatements;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumitra on 10/16/2017.
 */

public class Table<T> extends AbstractDao {
    public T table;
    public ArrayList<T> entity;

    public Table(DaoConfig config, AbstractDaoSession daoSession) {
        super(config, daoSession);
    }

    public T get()
    {
        return this.table;
    }

    public void set(T tableName)
    {
        this.table=tableName;
    }

    //load all table data
    public List<T> loadAllgeneric(DaoSession daoSession)
    {
        Database db=daoSession.getDatabase();
        //TableStatements tableStatements=
        Cursor cursor = db.rawQuery(statements.getSelectAll(), null);
        return loadAllAndCloseCursor(cursor);
    }

    @Override
    protected Object readEntity(Cursor cursor, int offset) {
        return null;
    }

    @Override
    protected Object readKey(Cursor cursor, int offset) {
        return null;
    }

    @Override
    protected void readEntity(Cursor cursor, Object entity, int offset) {

    }

    @Override
    protected void bindValues(DatabaseStatement stmt, Object entity) {

    }

    @Override
    protected void bindValues(SQLiteStatement stmt, Object entity) {

    }

    @Override
    protected Object updateKeyAfterInsert(Object entity, long rowId) {
        return null;
    }

    @Override
    protected Object getKey(Object entity) {
        return null;
    }

    @Override
    protected boolean hasKey(Object entity) {
        return false;
    }

    @Override
    protected boolean isEntityUpdateable() {
        return false;
    }


    //function call when sync the DB
   //public Boolean sync();

    //function to implement get

    //public void get_request();

    //function to implement post

    //public void post_request();


    //get number and type of columns


}
