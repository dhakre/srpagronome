package com.srp.agronome.android.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "GEOLOCALISATION".
*/
public class GeolocalisationDao extends AbstractDao<Geolocalisation, Long> {

    public static final String TABLENAME = "GEOLOCALISATION";

    /**
     * Properties of entity Geolocalisation.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Longitude = new Property(1, Double.class, "longitude", false, "LONGITUDE");
        public final static Property Latitude = new Property(2, Double.class, "latitude", false, "LATITUDE");
    }


    public GeolocalisationDao(DaoConfig config) {
        super(config);
    }
    
    public GeolocalisationDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"GEOLOCALISATION\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"LONGITUDE\" REAL," + // 1: longitude
                "\"LATITUDE\" REAL);"); // 2: latitude
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"GEOLOCALISATION\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Geolocalisation entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Double longitude = entity.getLongitude();
        if (longitude != null) {
            stmt.bindDouble(2, longitude);
        }
 
        Double latitude = entity.getLatitude();
        if (latitude != null) {
            stmt.bindDouble(3, latitude);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Geolocalisation entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Double longitude = entity.getLongitude();
        if (longitude != null) {
            stmt.bindDouble(2, longitude);
        }
 
        Double latitude = entity.getLatitude();
        if (latitude != null) {
            stmt.bindDouble(3, latitude);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Geolocalisation readEntity(Cursor cursor, int offset) {
        Geolocalisation entity = new Geolocalisation( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getDouble(offset + 1), // longitude
            cursor.isNull(offset + 2) ? null : cursor.getDouble(offset + 2) // latitude
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Geolocalisation entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setLongitude(cursor.isNull(offset + 1) ? null : cursor.getDouble(offset + 1));
        entity.setLatitude(cursor.isNull(offset + 2) ? null : cursor.getDouble(offset + 2));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Geolocalisation entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Geolocalisation entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Geolocalisation entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
