package com.srp.agronome.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.OS;
import com.srp.agronome.android.db.OSDao;
import com.srp.agronome.android.db.SOURCE_INTERNET;
import com.srp.agronome.android.db.SOURCE_INTERNETDao;
import com.srp.agronome.android.db.SOURCE_JOURNAUX;
import com.srp.agronome.android.db.SOURCE_JOURNAUXDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;

/**
 * Created by doucoure on 12/07/2017.
 */

public class SuiviProducteur extends AppCompatActivity {

    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    private static final float preferenceValuesf = 4;
    private EditText SurfaceAgricole = null;
    private final static String KeySurfaceAgricole  = "SurfaceAgricole ";

    private EditText SurfaceAgricoleIrrigable = null;
    private final static String KeySurfaceAgricoleIrrigable  = "SurfaceAgricoleIrrigable ";

    private EditText SurfaceAgricoleBiologique = null;
    private final static String KeySurfaceAgricoleBiologique = "SurfaceAgricoleBiologique ";

    private EditText SurfaceAgricoleIrrigableBiologique = null;
    private final static String KeySurfaceAgricoleIrrigableBiologique  = "SurfaceAgricoleIrrigableBiologique ";

    private Spinner spinneractivite1 = null;
    private Spinner  spinneractivite2 = null;
    private Spinner spinersourceinternet = null;
    private Spinner spinersourcejournal = null;


    /**
     * Image bouton pour valider la saisie
     */
    private Button BtnValidate = null;
    /**
     * Image bouton pour annuler
     */
    private ImageButton imgBtnHome = null;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suivi_producteur);
        FindWidgetViewbyId(); //Associate Widgets
     //   AdapterSpinner();
        AddListenerEvent(); //Add Widgets Event
        GetStringData();
        CreateListIntoDatabase();
        LoadSpinnerOS_Principal(spinneractivite1, getString(R.string.TextActivitePrincipale),
                getString(R.string.TextanotherActivitePrincipale),
                getString(R.string.TextValidatenewActivityPrinciple),
                getString(R.string.TextInputNewActivityPrinciple));

        LoadSpinnerOS_Secondaire(spinneractivite2, getString(R.string.TextActiviteSecondaire),
                getString(R.string.TextanotherActiviteSecondaire),
                getString(R.string.TextValidatenewActiviteSecondaire),
                getString(R.string.TextInputNewActiviteSecondaire));

        LoadSpinnerSource_Internet(spinersourceinternet, getString(R.string.TextActiviteSourceInternet),
                getString(R.string.TextanotherActiviteSourceInternet),
                getString(R.string.TextValidatenewActiviteSourceInternet),
                getString(R.string.TextInputNewActiviteSourceInternet));

        LoadSpinnerSource_Journaux(spinersourcejournal, getString(R.string.TextActiviteSourceJournaux),
                getString(R.string.TextanotherActiviteSourceJournaux),
                getString(R.string.TextValidatenewActiviteSourceJournaux),
                getString(R.string.TextInputNewActiviteSourceJournaux));



        BtnValidate = (Button) findViewById(R.id.btn_confirm_suivi);
        BtnValidate.setOnClickListener(ButtonValidateHandler);


        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_mainsuivi);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);
    }


    private void FindWidgetViewbyId() {

        SurfaceAgricole = (EditText) findViewById(R.id.sau);
        SurfaceAgricoleIrrigable = (EditText) findViewById(R.id.saui);
        SurfaceAgricoleBiologique = (EditText) findViewById(R.id.saub);
        SurfaceAgricoleIrrigableBiologique = (EditText) findViewById(R.id.sauib);
        spinneractivite1 = (Spinner) findViewById(R.id._spinner_act1);
        spinneractivite2 = (Spinner) findViewById(R.id._spinner_act2);
        spinersourceinternet = (Spinner) findViewById(R.id._spinner_si);
        spinersourcejournal = (Spinner) findViewById(R.id._spinner_sj);

    }
    //addTextChangedListener : Adds a TextWatcher to the list of those whose methods are called whenever this TextView's text changes.
    private void AddListenerEvent() {


        SurfaceAgricole.addTextChangedListener(generalTextWatcher);
        SurfaceAgricoleIrrigable.addTextChangedListener(generalTextWatcher);
        SurfaceAgricoleBiologique.addTextChangedListener(generalTextWatcher);
        SurfaceAgricoleIrrigableBiologique.addTextChangedListener(generalTextWatcher);
    }

    //Event Multiple EditText = save data
    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {


            if (SurfaceAgricole .getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeySurfaceAgricole , SurfaceAgricole .getText().toString());
            }else if (SurfaceAgricoleIrrigable  .getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeySurfaceAgricoleIrrigable  , SurfaceAgricoleIrrigable  .getText().toString());
            }else if (SurfaceAgricoleBiologique  .getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeySurfaceAgricoleBiologique  , SurfaceAgricoleBiologique  .getText().toString());
            }else if (SurfaceAgricoleIrrigableBiologique  .getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeySurfaceAgricoleIrrigableBiologique  , SurfaceAgricoleIrrigableBiologique  .getText().toString());
            }

        }};

    private void SaveStringDataInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }


    // Read the recorded data and define it in the corresponding edit text
    private void GetStringData() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode

        if (sharedpreferences.contains(KeySurfaceAgricole )) {
            SurfaceAgricole .setText(sharedpreferences.getString(KeySurfaceAgricole  , ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeySurfaceAgricoleIrrigable )) {
            SurfaceAgricoleIrrigable  .setText(sharedpreferences.getString(KeySurfaceAgricoleIrrigable  , ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeySurfaceAgricoleBiologique )) {
            SurfaceAgricoleBiologique  .setText(sharedpreferences.getString(KeySurfaceAgricoleBiologique , ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeySurfaceAgricoleIrrigableBiologique )) {
            SurfaceAgricoleIrrigableBiologique  .setText(sharedpreferences.getString(KeySurfaceAgricoleIrrigableBiologique , ""), TextView.BufferType.EDITABLE);
        }
    }

  /**  private void AdapterSpinner() {

        Spinner spinneractivite1 = (Spinner) findViewById(R.id._spinner_act1);
        Spinner spinneractivite2 = (Spinner) findViewById(R.id._spinner_act2);
        Spinner spinersourceinternet = (Spinner) findViewById(R.id._spinner_si);
        Spinner spinersourcejournal = (Spinner) findViewById(R.id._spinner_sj);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter adapteract1 = ArrayAdapter.createFromResource(this, R.array.activite_principale, android.R.layout.simple_selectable_list_item);
        ArrayAdapter adapteract2 = ArrayAdapter.createFromResource(this, R.array.activite_secondaire, android.R.layout.simple_selectable_list_item);
        ArrayAdapter adaptersi = ArrayAdapter.createFromResource(this, R.array.source_internet, android.R.layout.simple_selectable_list_item);
        ArrayAdapter adaptersj = ArrayAdapter.createFromResource(this, R.array.source_journal, android.R.layout.simple_selectable_list_item);

        // Specify the layout to use when the list of choices appears
        adapteract1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapteract2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adaptersi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adaptersj.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        // Apply the adapter to the spinner
        spinneractivite1.setAdapter(adapteract1);
        spinneractivite2.setAdapter(adapteract2);
        spinersourceinternet.setAdapter(adaptersi);
        spinersourcejournal.setAdapter(adaptersj);


    }  */

    //Bouton Valider Toolbar
    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();}
        } };

    //A method that creates a string listing the data entered.
    private String InputToText(){
        String text = getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.SAU) + " : " + SurfaceAgricole.getText().toString() + "\n";
        text += getString(R.string.SAUI) + " : " +SurfaceAgricoleIrrigable.getText().toString() + "\n";
        text += getString(R.string.SAUB) + " : " + SurfaceAgricoleBiologique.getText().toString() + "\n";
        text += getString(R.string.SAUIB) + " : " + SurfaceAgricoleIrrigableBiologique.getText().toString() + "\n\n";

        text += getString(R.string.TextActivitePrincipale) + " : " + spinneractivite1.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextActiviteSecondaire) + " : " + spinneractivite2.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextActiviteSourceInternet) + " : " + spinersourceinternet.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextActiviteSourceJournaux) + " : " + spinersourcejournal.getSelectedItem().toString() + "\n\n";

        return text;
    }

    // Method to check if the user has entered the data

    private Boolean CheckInput(){
        boolean b = true;

        //Vérification des texte editables

        if (TextUtils.isEmpty(SurfaceAgricole .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(SurfaceAgricoleIrrigable  .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(SurfaceAgricoleBiologique  .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(SurfaceAgricoleIrrigableBiologique  .getText().toString())) {
            b = false;
        }

        if (spinneractivite1.getSelectedItem().toString().equals(getString(R.string.TextStatutParcelle))) {
        b = false;
    }
        if (spinneractivite2.getSelectedItem().toString().equals(getString(R.string.TextTYPE_CULTURE))) {
        b = false;
    }
        if (spinersourceinternet.getSelectedItem().toString().equals(getString(R.string.TextIRRIGATION))) {
        b = false;
    }
        if (spinersourcejournal.getSelectedItem().toString().equals(getString(R.string.TextTYPE_IRRIGATION))) {
        b = false;
    }


        return b;
}

    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SuiviProducteur.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                // create Parcelle into db
                updatesuiviproducteurdb();

                Intent intent = new Intent(SuiviProducteur.this, MenuSuiviProducteur.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Clear data in sharedpreference
    private void clearData() {
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.remove(KeySurfaceAgricole);
        editor.remove(KeySurfaceAgricoleIrrigable);
        editor.remove(KeySurfaceAgricoleBiologique);
        editor.remove(KeySurfaceAgricoleIrrigableBiologique);
        editor.apply();

    }
    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewQC);
        int position = 0 ;



        if (TextUtils.isEmpty(SurfaceAgricole.getText().toString())) {
            position = SurfaceAgricole.getTop();
        }

        if (TextUtils.isEmpty(SurfaceAgricoleIrrigable.getText().toString())) {
            position = SurfaceAgricoleIrrigable.getTop();
        }
        if (TextUtils.isEmpty(SurfaceAgricoleBiologique.getText().toString())) {
            position = SurfaceAgricoleBiologique.getTop();
        }
        if (TextUtils.isEmpty(SurfaceAgricoleIrrigableBiologique.getText().toString())) {
            position = SurfaceAgricoleIrrigableBiologique.getTop();
        }

        final int Position_Y = position;
        final int Position_X = 0;
        sc.post(new Runnable() {
            public void run() {
                sc.scrollTo(Position_X, Position_Y); // these are your x and y coordinates
            }
        });
    }

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SuiviProducteur.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
        return true;
    }


    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SuiviProducteur.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(SuiviProducteur.this, MenuSuiviProducteur.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                Intent intent = new Intent(SuiviProducteur.this, MenuSuiviProducteur.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        OSDao OS_data = daoSession.getOSDao();
        SOURCE_INTERNETDao SOURCE_INTERNET_data = daoSession.getSOURCE_INTERNETDao();
        SOURCE_JOURNAUXDao SOURCE_JOURNAUX_data = daoSession.getSOURCE_JOURNAUXDao();

        List<String> list_OS = new ArrayList<>();list_OS.add("Agi-bio union");list_OS.add("Agri-agen");
        list_OS.add("Agricert");list_OS.add("Arterris");list_OS.add("CAPA");list_OS.add("CAPLA");
        list_OS.add("Casaus");list_OS.add("CRL");list_OS.add("Doumerg");list_OS.add("Euralis");
        list_OS.add("Goudy");list_OS.add("Laborderie");list_OS.add("Louit");list_OS.add("Lur berri");
        list_OS.add("Maisadour");list_OS.add("Mournet");list_OS.add("Qualisol");list_OS.add("Sansan");
        list_OS.add("Terre du sud");list_OS.add("Val de gascogne");list_OS.add("Vivadour");

        List<String> list_Source_Internet = new ArrayList<>();
        list_Source_Internet.add("France Agricole");
        list_Source_Internet.add("Terrenet");
        list_Source_Internet.add("AgriAffaire");

        List<String> list_Source_Journaux = new ArrayList<>();
        list_Source_Journaux.add("France Agricole");
        list_Source_Journaux.add("Petit Menier");

        List<OS> list_OS_DB = OS_data.loadAll();
        if (list_OS_DB.size() == 0) {
            for (String nom : list_OS) {
                OS OS_input = new OS();
                OS_input.setNom(nom);
                OS_data.insertOrReplace(OS_input);
            }
        }

        List<SOURCE_INTERNET> list_Source_Internet_BD = SOURCE_INTERNET_data.loadAll();
        if (list_Source_Internet_BD.size() == 0) {
            for (String nom : list_Source_Internet) {
                SOURCE_INTERNET SOURCE_INTERNET_input = new SOURCE_INTERNET();
                SOURCE_INTERNET_input.setNom(nom);
                SOURCE_INTERNET_data.insertOrReplace(SOURCE_INTERNET_input);
            }
        }

        List<SOURCE_JOURNAUX> list_Source_Journaux_BD = SOURCE_JOURNAUX_data.loadAll();
        if (list_Source_Journaux_BD.size() == 0) {
            for (String nom : list_Source_Journaux) {
                SOURCE_JOURNAUX SOURCE_JOURNAUX_input = new SOURCE_JOURNAUX();
                SOURCE_JOURNAUX_input.setNom(nom);
                SOURCE_JOURNAUX_data.insertOrReplace(SOURCE_JOURNAUX_input);
            }
        }
    }

    private void LoadSpinnerOS_Principal(Spinner spinner, String TextHint, String TextAnother, final String TitleAddNew, final String TextAddNew) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        OSDao OS_data = daoSession.getOSDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorHint));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<OS> List_OS = OS_data.loadAll();
        if (List_OS != null) {
            for (OS Organisme_stockeur : List_OS) {
                adapter.add(Organisme_stockeur.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {
                if (position == 0) { //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew, TextAddNew, 1);
                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }


    private void LoadSpinnerOS_Secondaire(Spinner spinner, String TextHint, String TextAnother, final String TitleAddNew, final String TextAddNew) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        OSDao OS_data = daoSession.getOSDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorHint));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<OS> List_OS = OS_data.loadAll();
        if (List_OS != null) {
            for (OS Organisme_stockeur : List_OS) {
                adapter.add(Organisme_stockeur.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {
                if (position == 0) { //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew, TextAddNew, 1);
                }
            }
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }


    private void LoadSpinnerSource_Internet(Spinner spinner, String TextHint, String TextAnother, final String TitleAddNew, final String TextAddNew) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SOURCE_INTERNETDao SOURCE_INTERNET_data = daoSession.getSOURCE_INTERNETDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorHint));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<SOURCE_INTERNET> List_SOURCE_INTERNET = SOURCE_INTERNET_data.loadAll();
        if (List_SOURCE_INTERNET != null) {
            for (SOURCE_INTERNET SI : List_SOURCE_INTERNET) {
                adapter.add(SI.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {
                if (position == 0) { //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew, TextAddNew, 2);
                }
            }
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }


    private void LoadSpinnerSource_Journaux(Spinner spinner, String TextHint, String TextAnother, final String TitleAddNew, final String TextAddNew) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SOURCE_JOURNAUXDao SOURCE_JOURNAUX_data = daoSession.getSOURCE_JOURNAUXDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorHint));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<SOURCE_JOURNAUX> List_SOURCE_JOURNAUX = SOURCE_JOURNAUX_data.loadAll();
        if (List_SOURCE_JOURNAUX  != null) {
            for (SOURCE_JOURNAUX SJ : List_SOURCE_JOURNAUX ) {
                adapter.add(SJ.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {
                if (position == 0) { //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew, TextAddNew, 3);
                }
            }
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(SuiviProducteur.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code OS Principal
                        addOS(NewValueList);
                        break;
                    case 2 : //Code Source Internet
                        addSOURCE_INTERNET(NewValueList);
                        break;
                    case 3 : //Code Source Journaux
                        addSOURCE_JOURNAUX(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    private void addOS(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        OSDao OS_data = daoSession.getOSDao();
        OS OS_input = new OS();
        OS_input.setNom(Nom);
        OS_data.insertOrReplace(OS_input);


        LoadSpinnerOS_Principal(spinneractivite1, getString(R.string.TextActivitePrincipale),
                getString(R.string.TextanotherActivitePrincipale),
                getString(R.string.TextValidatenewActivityPrinciple),
                getString(R.string.TextInputNewActivityPrinciple));

        LoadSpinnerOS_Secondaire(spinneractivite2, getString(R.string.TextActiviteSecondaire),
                getString(R.string.TextanotherActiviteSecondaire),
                getString(R.string.TextValidatenewActiviteSecondaire),
                getString(R.string.TextInputNewActiviteSecondaire));
    }

    private void addSOURCE_INTERNET(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SOURCE_INTERNETDao SOURCE_INTERNET_data = daoSession.getSOURCE_INTERNETDao();
        SOURCE_INTERNET SOURCE_INTERNET_input = new SOURCE_INTERNET();
        SOURCE_INTERNET_input.setNom(Nom);
        SOURCE_INTERNET_data.insertOrReplace(SOURCE_INTERNET_input);
        LoadSpinnerSource_Internet(spinersourceinternet, getString(R.string.TextActiviteSourceInternet),
                getString(R.string.TextanotherActiviteSourceInternet),
                getString(R.string.TextValidatenewActiviteSourceInternet),
                getString(R.string.TextInputNewActiviteSourceInternet));
    }

    private void addSOURCE_JOURNAUX(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SOURCE_JOURNAUXDao SOURCE_JOURNAUX_data = daoSession.getSOURCE_JOURNAUXDao();
        SOURCE_JOURNAUX SOURCE_JOURNAUX_input = new SOURCE_JOURNAUX();
        SOURCE_JOURNAUX_input.setNom(Nom);
        SOURCE_JOURNAUX_data.insertOrReplace(SOURCE_JOURNAUX_input);
        LoadSpinnerSource_Journaux(spinersourcejournal, getString(R.string.TextActiviteSourceJournaux),
                getString(R.string.TextanotherActiviteSourceJournaux),
                getString(R.string.TextValidatenewActiviteSourceJournaux),
                getString(R.string.TextInputNewActiviteSourceJournaux));
    }

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }
    private  void updatesuiviproducteurdb(){

        SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        if (sp != null) {
                if (sp.getSAU() != null) {
                int sau=   (sp.getSAU());

                    if (sp.getSAU_IRRIGABLE() != null) {
                    int sau_irrigable=   (sp.getSAU_IRRIGABLE());

                    if (sp.getSAU_BIO() != null) {
                        int sau_bio=   (sp.getSAU_BIO());

                        if (sp.getSAU_BIO_IRRIGABLE() != null) {
                            int sauBioIrrigable=   (sp.getSAU_BIO_IRRIGABLE());

                            UpdateSuiviProducteurIntoDatabase (sp, sau,
                sau_irrigable,sau_bio,sauBioIrrigable,spinneractivite1.getSelectedItem().toString(),
                spinneractivite2.getSelectedItem().toString(),spinersourceinternet.getSelectedItem().toString(),
                spinersourcejournal.getSelectedItem().toString());
    }  } } } } }
    //Methodes Base de donnnes pour Jitendra

    private SUIVI_PRODUCTEUR getSuiviProducteurOfOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        SUIVI_PRODUCTEUR suivi_producteur_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        suivi_producteur_result = e.getSUIVI_PRODUCTEUR();
                    }
                }
            }
        }
        return suivi_producteur_result;
    }

    private void UpdateSuiviProducteurIntoDatabase (SUIVI_PRODUCTEUR suivi_producteur_edit, int SAU, int SAU_i,int SAU_bio, int SAU_bio_i, String OS1 , String OS2 ,String Source_Internet, String Source_Journaux){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
        OSDao OS_data = daoSession.getOSDao();
        SOURCE_INTERNETDao source_internet_data = daoSession.getSOURCE_INTERNETDao();
        SOURCE_JOURNAUXDao source_journaux_data = daoSession.getSOURCE_JOURNAUXDao();

        suivi_producteur_edit.setSAU(SAU);
        suivi_producteur_edit.setSAU_IRRIGABLE(SAU_i);
        suivi_producteur_edit.setSAU_BIO(SAU_bio);
        suivi_producteur_edit.setSAU_BIO_IRRIGABLE(SAU_bio_i);
        suivi_producteur_edit.setDate_modification(Today_Date());
        suivi_producteur_edit.setID_Agronome_Modification((int)ID_Compte_Selected);

        //Edit Organisme Stockeur Principal
        List<OS> List_OS_1 = OS_data.queryBuilder()
                .where(OSDao.Properties.Nom.eq(OS1))
                .list();
        if (List_OS_1 != null) {
            for (OS OS_1 : List_OS_1) {
                suivi_producteur_edit.setOS_PRINCIPAL(OS_1);
                suivi_producteur_edit.setID_OS_PRINCIPAL(OS_1.getId());
            }
        }

        //Edit Organisme Stockeur Secondaire
        List<OS> List_OS_2 = OS_data.queryBuilder()
                .where(OSDao.Properties.Nom.eq(OS2))
                .list();
        if (List_OS_2 != null) {
            for (OS OS_2 : List_OS_2) {
                suivi_producteur_edit.setOS_SECONDAIRE(OS_2);
                suivi_producteur_edit.setID_OS_SECONDAIRE(OS_2.getId());
            }
        }

        //Edit Source Internet
        List<SOURCE_INTERNET> List_Source_Internet = source_internet_data.queryBuilder()
                .where(SOURCE_INTERNETDao.Properties.Nom.eq(Source_Internet))
                .list();
        if (List_Source_Internet  != null) {
            for (SOURCE_INTERNET SI : List_Source_Internet ) {
                suivi_producteur_edit.setSOURCE_INTERNET(SI);
                suivi_producteur_edit.setID_SOURCE_INTERNET(SI.getId());
            }
        }

        //Edit Source Journaux
        List<SOURCE_JOURNAUX> List_Source_Journaux = source_journaux_data.queryBuilder()
                .where(SOURCE_JOURNAUXDao.Properties.Nom.eq(Source_Journaux))
                .list();
        if (List_Source_Journaux  != null) {
            for (SOURCE_JOURNAUX SJ : List_Source_Journaux ) {
                suivi_producteur_edit.setSOURCE_JOURNAUX(SJ);
                suivi_producteur_edit.setID_SOURCE_JOURNAUX(SJ.getId());
            }
        }

        suivi_producteur_data.update(suivi_producteur_edit); // Update
    }

//    private void DeleteSuiviProducteur(SUIVI_PRODUCTEUR suivi_producteur_delete){
//        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
//        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
//        suivi_producteur_delete.setArchive(0);
//        suivi_producteur_delete.setDate_modification(Today_Date());
//        suivi_producteur_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
//        suivi_producteur_data.update(suivi_producteur_delete);
//    }

}






