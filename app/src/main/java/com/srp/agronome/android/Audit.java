package com.srp.agronome.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.AUDIT_DOCUMENT;
import com.srp.agronome.android.db.AUDIT_DOCUMENTDao;
import com.srp.agronome.android.db.AUDIT_PARCELLE;
import com.srp.agronome.android.db.AUDIT_PARCELLEDao;
import com.srp.agronome.android.db.AUDIT_STOCKAGE;
import com.srp.agronome.android.db.AUDIT_STOCKAGEDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;
import static com.srp.agronome.android.MenuAudit.Audit_Id_Selected;
import com.srp.agronome.android.db.VisiteDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;

/**
 * Created by doucoure on 12/07/2017.
 */


public class Audit extends AppCompatActivity{

    //    private Button AuditDocument = null;
    private Button AuditParcelle = null;
//    private Button AuditStockage = null;
    private Button ObjetAudit = null;
    Date auditdate;
    public static String AUDIT_TYPE=null;
    long  audit_ID_Selected=Audit_Id_Selected;
   // BasicListViewActivity basicListViewActivity;

    /**
     * Annee de l'audit
     */

    private EditText audit_date = null;

    /**
     * Remarque concernant l'audit document
     */
    private EditText Remarque = null;

    private ImageButton imgBtnHome = null;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // basicListViewActivity.setContentView();
        setContentView(R.layout.activity_choix_audit);

        audit_date = (EditText) findViewById(R.id.annee_id);
        Remarque = (EditText) findViewById(R.id.remarque_doc_id);


        Log.i("Remarque ui value", "onCreate: "+Remarque.getText().toString());



//        AuditDocument = (Button) findViewById(R.id.btn_auditdocs);
//        AuditDocument.setOnClickListener(ButtonAuditDocseHandler);
//
//           AuditParcelle = (Button) findViewById(R.id.btn_auditparcelle);
//         AuditParcelle.setOnClickListener(ButtonAuditParcelleéHandler);
//
//        AuditStockage = (Button) findViewById(R.id.btn_auditclassique);
//        AuditStockage.setOnClickListener(ButtonAuditStockHandler);

        ObjetAudit = (Button) findViewById(R.id.objet_id);
        ObjetAudit.setOnClickListener(ButtonObjetHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_contact);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

    }


    public void Selection_objetaudit_Choice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Audit.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionObjetAudit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Audit document
                        AUDIT_TYPE="Document";
                        Log.i("Audit", "onClick: Audit class: AUDIT_TYPE= "+AUDIT_TYPE);
                        Intent intent = new Intent(Audit.this, AuditDocument.class);
                        finish();
                        startActivity(intent);


                        break;
                    case 1: // Audit parcelle
                      //  Show_Dialog("Information", "En developpement");
                        AUDIT_TYPE="parcelle";
                        Intent intent_parcelle = new Intent(Audit.this, AuditParcelle.class);
                       finish();
                        startActivity(intent_parcelle);

                      break;
                    case 2: //Audit stockahe
                        AUDIT_TYPE="stockage";
                       // Show_Dialog("Information", "En developpement");

                        //Verifie si il y a une photo
                       Intent intentstock = new Intent(Audit.this, AuditStockage.class);
                        finish();
                        startActivity(intentstock);
                        break;

                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(Audit.this, ContactMenu.class);
            finish();
            startActivity(intent);

        }
        return true;
    }


//    View.OnClickListener ButtonAuditDocseHandler = new View.OnClickListener() {
//        public void onClick(View v) {
//            Intent intent = new Intent(Audit.this, AuditDocument.class);
//            finish();
//            startActivity(intent);
//
//        }
//    };
//
  View.OnClickListener ButtonAuditParcelleéHandler = new View.OnClickListener() {
   public void onClick(View v) {
//      Intent intent = new Intent(Audit.this, AuditParcelle.class);
//         finish();
//           startActivity(intent);

        }
      };

//    View.OnClickListener ButtonAuditStockHandler = new View.OnClickListener() {
//        public void onClick(View v) {
//            Intent intent = new Intent(Audit.this, AuditStockage.class);
//            finish();
//            startActivity(intent);
//
//        }
//    };


    View.OnClickListener ButtonObjetHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Selection_objetaudit_Choice();
            //Toast.makeText(getApplicationContext(),Remarque.getText().toString(),Toast.LENGTH_LONG).show();
            //string to date
            auditdate=stringtoDate(audit_date.getText().toString());
            insertAuditIntoDB(Remarque.getText().toString(),auditdate);   //create audit in DB


        }
    };


    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(Audit.this, MenuAudit.class);
            finish();
            startActivity(intent);
        }
    };

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Audit.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

    public AUDIT createAuditIntoDB(String remarque, Date date)
    {
        DaoSession daoSession=((AppController) getApplication()).getDaoSession();
        AUDIT_PARCELLEDao parcelle_attribute=daoSession.getAUDIT_PARCELLEDao();
        AUDIT_DOCUMENTDao document_attribute=daoSession.getAUDIT_DOCUMENTDao();
        AUDIT_STOCKAGEDao stockage_attribute=daoSession.getAUDIT_STOCKAGEDao();

        AUDITDao audit_data= daoSession.getAUDITDao();

        AUDIT audit_input=new AUDIT();
        audit_input.setArchive(GlobalValues.getInstance().getGlobalArchive());  //archive=1
        audit_input.setDate_creation(GlobalValues.Today_Date());
        audit_input.setDate_modification(GlobalValues.Today_Date());
        audit_input.setID_Agronome_Creation((int) ID_Compte_Selected);
        audit_input.setID_Agronome_Modification((int) ID_Compte_Selected);
        audit_input.setREMARQUES_AUDITS(remarque);
        audit_input.setDATE(date);


        //Add audit parcelle
       List<AUDIT_PARCELLE> audit_parcelleList=parcelle_attribute.queryBuilder().
                where(AUDIT_PARCELLEDao.Properties.Archive.eq(GlobalValues.getInstance().getGlobalArchivable()))  //archive=0
                        .list();
       // List<AUDIT_PARCELLE> audit_parcelleList = audit_input.getAUDIT_PARCELLEList();
        if (audit_parcelleList !=null)
        {
            for(AUDIT_PARCELLE ap: audit_parcelleList)
            {
               // audit_input.resetAUDIT_PARCELLEList();
            }
        }

       //add document to audit
        List<AUDIT_DOCUMENT> audit_documentList=document_attribute.queryBuilder().where(AUDIT_DOCUMENTDao.Properties.Archive.eq(GlobalValues.getInstance().getGlobalArchivable()))
                .list();
        if(audit_documentList!=null)
        {
            for(AUDIT_DOCUMENT ad: audit_documentList) {
                audit_input.setAUDIT_DOCUMENT(ad);   //add document
            }
        }

        //Add stockage to audit

        List<AUDIT_STOCKAGE> audit_stockageList=stockage_attribute.queryBuilder().where(AUDIT_STOCKAGEDao.Properties.Archive.eq(GlobalValues.getInstance().getGlobalArchivable()))
                .list();
        if(audit_stockageList!=null)
        {
            for(AUDIT_STOCKAGE as:audit_stockageList)
            {
                //audit_input.resetAUDIT_STOCKAGEList();
            }
        }



           audit_data.insertOrReplace(audit_input);
        return audit_input;
    }

    //get audit parcelle list
    public List<AUDIT_PARCELLE> getListAuditParcelle(AUDIT audit_input){
        return audit_input.getAUDIT_PARCELLEList();
    }

    //get audit stock list
    public List<AUDIT_STOCKAGE> getAuditStockageList(AUDIT audit_input)
    {
        return audit_input.getAUDIT_STOCKAGEList();
    }

    //Get a audit in a list of audit
    private AUDIT getAudit(long ID_audit, List <AUDIT> list_audit){
        AUDIT result = null;
        if (list_audit != null){
            for (AUDIT audit : list_audit){
                if (audit.getId() == ID_audit){
                    result = audit;
                }
            }
        }
        return result;
    }

    //
    public  void insertAuditIntoDB(String  remarque, Date date)
    {
        Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        //create audit
        AUDIT audit_insert=createAuditIntoDB(remarque,date);

        Audit_Id_Selected=audit_insert.getId();   //to be shared with sub-Modules

        //link audit to organisation
        AddAuditToOrganization(audit_insert,entreprise_selected);

        //Add audit to document


    }

    //Add a audit to Organization
    private void AddAuditToOrganization(AUDIT audit_input,Entreprise entreprise_input){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        //add audit to suvi_producer
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
       // SUIVI_PRODUCTEURDao suivi_producteur_data=daoSession.getSUIVI_PRODUCTEURDao();


       // SUIVI_PRODUCTEUR suivi_producteur_input=entreprise_input.getSUIVI_PRODUCTEUR();   //get suivi_producer from enterprise

       // suivi_producteur_input
       // audit_input.setSUIVI_PRODUCTEUR(entreprise_input.getSUIVI_PRODUCTEUR());  //get suivi producer id from enterprise table
        audit_data.update(audit_input);
       // entreprise_input.reset;
       // suivi_producteur_data.update(suivi_producteur_input);
        entreprise_data.update(entreprise_input);
    }

    //add to document

    //get a organisation

    public Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }

    //string to date

    public  Date stringtoDate(String date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss ");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return  convertedDate;
    }

    private AUDIT getAuditV2(long ID_audit){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        AUDIT result = null;
        List<AUDIT> audit_load = audit_data.queryBuilder()
                .where(AUDITDao.Properties.Id.eq(ID_audit))
                .list();
        if (audit_load != null) {
            for (AUDIT A: audit_load) {

                result = A;
            }
        }
        return result;
    }



}
