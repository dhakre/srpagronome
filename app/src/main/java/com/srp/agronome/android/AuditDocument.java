package com.srp.agronome.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.AUDIT_DOCUMENT;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuAudit.Audit_Id_Selected;
import static com.srp.agronome.android.Audit.AUDIT_TYPE;

import com.srp.agronome.android.db.AUDIT_DOCUMENTDao;
import com.srp.agronome.android.db.ActiviteDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DOCUMENT;
import com.srp.agronome.android.db.DOCUMENTDao;
import com.srp.agronome.android.db.DOCUMENT_FOURNI;
import com.srp.agronome.android.db.DOCUMENT_FOURNIDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.TYPE_DOCUMENT;
import com.srp.agronome.android.db.TYPE_DOCUMENTDao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.Nom_Compte;

/**
 * Created by doucoure on 18/07/2017.
 */

public class AuditDocument extends AppCompatActivity {

    private static int page = 0;
    public static long ID_Audit_Document_Selected;


    /**
     * bouton retour
     */

    private ImageButton Retour = null;


    /**
     * bouton prise de photo
     */

    private Button PrendrePhoto = null;


    private Button Validate = null;


    private Spinner spinnerDoucument = null;

    private RadioGroup GroupDocFourni = null;

    private AUDIT audit_selected=null;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_docs);
        //set audit type
       // AUDIT_TYPE="Document";
        audit_selected=getAuditFromID(Audit_Id_Selected);
        AUDIT_TYPE= getAuditType(audit_selected);

        PrendrePhoto = (Button) findViewById(R.id.btnprendre_photo);
        PrendrePhoto.setOnClickListener(ButtonPrendrePhotoHandler);

        Retour = (ImageButton) findViewById(R.id.arrow_back_audit);
        Retour.setOnClickListener(ButtonRetourHandler);

        Validate = (Button) findViewById(R.id.btnvalidate_audit_doc);
        Validate.setOnClickListener(ButtonValidateHandler);

        spinnerDoucument = (Spinner) findViewById(R.id.SpinnerDoccument);


        GroupDocFourni = (RadioGroup) findViewById(R.id.OuiouNon);


        CreateListIntoDatabase();
        LoadSpinnerTypeDocument();

        File AppDirAudit = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit/2017/Soytouch");
        Log.i("Path Directory location", AppDirAudit.toString());
        if (!AppDirAudit.exists()) {
            if (!AppDirAudit.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }


    }


//    private void AdapterSpinner() {
//        Spinner spinnerDoucument = (Spinner) findViewById(R.id.SpinnerDoccument);
//        // Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter adapterdoc = ArrayAdapter.createFromResource(this, R.array.Document, android.R.layout.simple_selectable_list_item);
//        adapterdoc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//
//        // Apply the adapter to the spinner
//        spinnerDoucument.setAdapter(adapterdoc);
//
//    }

    //get audit type
    String getAuditType(AUDIT audit_id_selected)
    {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDITDao audit_data=daoSession.getAUDITDao();
        AUDIT_DOCUMENTDao audit_document_data=daoSession.getAUDIT_DOCUMENTDao();


        List<AUDIT_DOCUMENT> audit_documentList=audit_document_data.queryBuilder().where(AUDIT_DOCUMENTDao.Properties.Id.eq(audit_id_selected.getID_AUDIT_DOCUMENT())).list();
        if(audit_documentList!=null)
        {    //get value
            AUDIT_TYPE="Document";
        }
        else
        {
            AUDIT_TYPE="No Document Defined";
        }
        return AUDIT_TYPE;

    }

    View.OnClickListener ButtonPrendrePhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            TakePicturefromCamera();
        }
    };

    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {

            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
            }

            insertAuditDocIntoDB(spinnerDoucument.getSelectedItem().toString(),getSelectedTextFourni(GroupDocFourni),"email");
            displayAuditDocInLog();
          startActivity(new Intent(AuditDocument.this,MenuAudit.class));
            //Show_Dialog("Information", "En developpement");
        }
    };


    private Boolean CheckInput(){
        boolean b = true;

        //Vérification des listes de choix :
        if (spinnerDoucument.getSelectedItem().toString().equals(getString(R.string.TextTypeDocument))) {
            b = false;
        }
        if (GroupDocFourni.getCheckedRadioButtonId()==0) {
            b = false;
        }

        return b;
    }

    private String InputToText(){
        String text = getString(R.string.TextTypeDocument) + " : " + spinnerDoucument.getSelectedItem().toString() + "\n\n";
        text +=  getString(R.string.fourni) + ":"+ GroupDocFourni.getCheckedRadioButtonId();
        return text;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(AuditDocument.this, Audit.class);
            finish();
            startActivity(intent);

        }
        return true;
    }

    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(AuditDocument.this, Audit.class);
            finish();
            startActivity(intent);
        }
    };


    public void TakePicturefromCamera() {
        page++;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile("_document_page" + page + "_HD.jpg");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivity(intent);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit/2017/Soytouch");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        return file;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        //Depuis la caméra : pas de bitmap en sortie
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit/2017/Soytouch");
        File file = new File(myDir, "_auditDoc_picture_HD.jpg");
        Uri uri = Uri.fromFile(file);
        Bitmap newPicture;
        try {
            newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
            SaveImage(newPicture, "_auditDoc_picture_HD.jpg");
            newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
            SaveImage(newPicture, "_auditDoc_picture.jpg"); //Sauvegarde l'image dans le téléphone
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit_Document");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        //Log.i("File Location",file.toString());
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_DOCUMENTDao Type_document_data = daoSession.getTYPE_DOCUMENTDao();

        DOCUMENT_FOURNIDao Documentfourni_data = daoSession.getDOCUMENT_FOURNIDao();

        DOCUMENTDao documentDao=daoSession.getDOCUMENTDao();

        List<TYPE_DOCUMENT> List_typedocument = Type_document_data.loadAll();
        if (List_typedocument.size() == 0) {

            TYPE_DOCUMENT Type_document_input_1 = new TYPE_DOCUMENT();
            Type_document_input_1.setNom("PAC");
            Type_document_data.insertOrReplace(Type_document_input_1);

            TYPE_DOCUMENT Type_document_input_2 = new TYPE_DOCUMENT();
            Type_document_input_2.setNom("CERTIPHYTO");
            Type_document_data.insertOrReplace(Type_document_input_2);

            TYPE_DOCUMENT Type_document_input_3 = new TYPE_DOCUMENT();
            Type_document_input_3.setNom("ATTESTATION BIO");
            Type_document_data.insertOrReplace(Type_document_input_3);

            TYPE_DOCUMENT Type_document_input_4 = new TYPE_DOCUMENT();
            Type_document_input_4.setNom("FACTURE");
            Type_document_data.insertOrReplace(Type_document_input_4);

            TYPE_DOCUMENT Type_document_input_5 = new TYPE_DOCUMENT();
            Type_document_input_5.setNom("CAHIER CULTURE");
            Type_document_data.insertOrReplace(Type_document_input_5);

            TYPE_DOCUMENT Type_document_input_6 = new TYPE_DOCUMENT();
            Type_document_input_6.setNom("CHARTE PRODUCTION");
            Type_document_data.insertOrReplace(Type_document_input_6);

            TYPE_DOCUMENT Type_document_input_7 = new TYPE_DOCUMENT();
            Type_document_input_7.setNom("AUTRE");
            Type_document_data.insertOrReplace(Type_document_input_7);


        }


        List<DOCUMENT_FOURNI> List_document_fourni = Documentfourni_data.loadAll();
        if (List_document_fourni.size() == 0) {

            DOCUMENT_FOURNI Documentfourni_input_1 = new DOCUMENT_FOURNI();
            Documentfourni_input_1.setNom("Oui");
            Documentfourni_data.insertOrReplace(Documentfourni_input_1);

            DOCUMENT_FOURNI Documentfourni_input_2 = new DOCUMENT_FOURNI();
            Documentfourni_input_2.setNom("Non");
            Documentfourni_data.insertOrReplace(Documentfourni_input_2);


        }
    }

    //Charger le spinner type de document
    private void LoadSpinnerTypeDocument() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_DOCUMENTDao Type_document_data = daoSession.getTYPE_DOCUMENTDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste

        List<TYPE_DOCUMENT> type_documentList = Type_document_data.queryBuilder().list();
        if (type_documentList != null) {
            for (TYPE_DOCUMENT TD : type_documentList) {
                adapter.add(TD.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextTypeDocument)); //This is the text that will be displayed as hint.
        spinnerDoucument.setAdapter(adapter);
        spinnerDoucument.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

    }

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }


    private void CreateDataIntoDataBase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        DOCUMENTDao documentDaodata = daoSession.getDOCUMENTDao();
        TYPE_DOCUMENTDao typeDocumentDao_data = daoSession.getTYPE_DOCUMENTDao();
        DOCUMENT_FOURNIDao document_fourniDao_data = daoSession.getDOCUMENT_FOURNIDao();

        //GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();

        AUDIT_DOCUMENT auditDocument_input = new AUDIT_DOCUMENT();
        auditDocument_input.setArchive(1);
        auditDocument_input.setDate_creation(Today_Date());
        auditDocument_input.setDate_modification(Today_Date());
        auditDocument_input.setID_Agronome_Creation(1);
        auditDocument_input.setID_Agronome_Modification(1);

        //Table  document
        DOCUMENT document_input = new DOCUMENT();


        //Ajout de la table type document
        List<TYPE_DOCUMENT> List_type_document = typeDocumentDao_data.queryBuilder()
                .where(TYPE_DOCUMENTDao.Properties.Nom.eq(spinnerDoucument.getSelectedItem().toString()))
                .list();
        if (List_type_document != null) {
            for (TYPE_DOCUMENT S : List_type_document) {
                document_input.setTYPE_DOCUMENT(S);
                document_input.setID_TYPE_DOCUMENT(S.getId());


            }
        }


        if (GroupDocFourni.getCheckedRadioButtonId() == R.id.radiobtn_oui) {
            List<DOCUMENT_FOURNI> List_document_fourni = document_fourniDao_data.queryBuilder()
                    .where(DOCUMENT_FOURNIDao.Properties.Nom.eq("Oui"))
                    .list();
            if (List_document_fourni != null) {
                for (DOCUMENT_FOURNI S : List_document_fourni) {
                    document_input.setDOCUMENT_FOURNI(S);
                    document_input.setID_TYPE_DOCUMENT(S.getId());


                }
            }

        } else {
            List<DOCUMENT_FOURNI> List_document_fourni = document_fourniDao_data.queryBuilder()
                    .where(DOCUMENT_FOURNIDao.Properties.Nom.eq("Non"))
                    .list();
            if (List_document_fourni != null) {
                for (DOCUMENT_FOURNI S : List_document_fourni) {
                    document_input.setDOCUMENT_FOURNI(S);
                    document_input.setID_TYPE_DOCUMENT(S.getId());


                }
            }

        }
        documentDaodata.insertOrReplace(document_input);

        //add audit document to document
      //  List<DOCUMENT> audit_documents=documentDaodata.queryBuilder().where(DOCUMENTDao.Properties.ID_AUDIT_DOCUMENT_DOCUMENT.eq(auditDocument_input.getId())).list();

    }
    //insert in DB

    public  void insertAuditDocIntoDB(String type_doc,String doc_Fourni, String email)
    {
        Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        AUDIT audit_selected=getAuditFromID(Audit_Id_Selected);
        //create audit doc
        AUDIT_DOCUMENT audit_doc_insert=createAuditDocIntoDB(type_doc,doc_Fourni,email);
        ID_Audit_Document_Selected=audit_doc_insert.getId();   //to be shared with sub-Modules

        //link audit document to audit
        AddAuditDocumentToAudit(audit_doc_insert,audit_selected);

    }

    //get selected text
    private  String getSelectedTextFourni(RadioGroup groupDocFourni)
    {
        String selectedText;
        if (GroupDocFourni.getCheckedRadioButtonId() == R.id.radiobtn_oui)
        {
            selectedText="Oui";
        }
        else {
            selectedText="Non";
        }
        return selectedText;

    }



    private void AddAuditDocumentToAudit( AUDIT_DOCUMENT audit_document_input,AUDIT audit_selected)
    {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDIT_DOCUMENTDao audit__doc_data = daoSession.getAUDIT_DOCUMENTDao();
        AUDITDao auditDao_data=daoSession.getAUDITDao();

        audit__doc_data.update(audit_document_input);

        audit_selected.setAUDIT_DOCUMENT(audit_document_input);

        auditDao_data.update(audit_selected);


    }

    private AUDIT_DOCUMENT createAuditDocIntoDB(String docType,String docfounri, String email)
    {
        DaoSession daoSession=((AppController) getApplication()).getDaoSession();

        AUDITDao audit_data=daoSession.getAUDITDao();
        DOCUMENTDao documentDaodata = daoSession.getDOCUMENTDao();
        TYPE_DOCUMENTDao typeDocumentDao_data = daoSession.getTYPE_DOCUMENTDao();
        DOCUMENT_FOURNIDao document_fourniDao_data = daoSession.getDOCUMENT_FOURNIDao();

        AUDIT_DOCUMENTDao audit_doc_data= daoSession.getAUDIT_DOCUMENTDao();

        //selected audit
        AUDIT audit_input=getAuditFromID(Audit_Id_Selected);

        AUDIT_DOCUMENT audit_doc_input=new AUDIT_DOCUMENT();
        audit_doc_input.setArchive(GlobalValues.getInstance().getGlobalArchive());  //archive=1
        audit_doc_input.setDate_creation(GlobalValues.Today_Date());
        audit_doc_input.setDate_modification(GlobalValues.Today_Date());
        audit_doc_input.setID_Agronome_Creation((int) ID_Compte_Selected);
        audit_doc_input.setID_Agronome_Modification((int) ID_Compte_Selected);


        //add audit_doc to audit
      //  audit_input.setAUDIT_DOCUMENT(audit_doc_input);
      //  audit_input.setID_AUDIT_DOCUMENT(audit_doc_input.getId());

        //add data to doc_type
        TYPE_DOCUMENTDao type_document_data=daoSession.getTYPE_DOCUMENTDao();

        TYPE_DOCUMENT type_document_input=new TYPE_DOCUMENT();
        type_document_input.setNom(docType);  //set input data

        //add type_doc list to document
        DOCUMENT document_input=new DOCUMENT();
        List<TYPE_DOCUMENT> type_documentList=typeDocumentDao_data.queryBuilder().where(TYPE_DOCUMENTDao.Properties.Nom.eq(docType)).list();

        for(TYPE_DOCUMENT td:type_documentList)
        {
            //add to doc
            document_input.setID_TYPE_DOCUMENT(td.getId());
        }



        //add audit_document list to document
        List<AUDIT_DOCUMENT> audit_documentList=audit_doc_data.loadAll();
        for(AUDIT_DOCUMENT ad: audit_documentList)
        {
            document_input.setID_AUDIT_DOCUMENT_DOCUMENT(ad.getId());
        }


        //add data to doc founri
        DOCUMENT_FOURNIDao document_fourni_data=daoSession.getDOCUMENT_FOURNIDao();

        DOCUMENT_FOURNI document_fourni_input=new DOCUMENT_FOURNI();
        document_fourni_input.setNom(docfounri);  //set input data

        //add to doc
        List<DOCUMENT_FOURNI> document_fourniList=document_fourniDao_data.queryBuilder().where(DOCUMENT_FOURNIDao.Properties.Nom.eq(docfounri)).list();
        for(DOCUMENT_FOURNI df: document_fourniList)
        {
            document_input.setID_DOCUMENT_FOURNI(df.getId());
        }
        //insert into db
        audit_doc_data.insertOrReplace(audit_doc_input);
        document_fourni_data.insertOrReplace(document_fourni_input);
        documentDaodata.insertOrReplace(document_input);
        type_document_data.insertOrReplace(type_document_input);

        return audit_doc_input;

    }

    //get audit with the id
    private AUDIT getAuditFromID(long ID_audit){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        AUDIT result = null;
        List<AUDIT> audit_load = audit_data.queryBuilder()
                .where(AUDITDao.Properties.Id.eq(ID_audit))
                .list();
        if (audit_load != null) {
            for (AUDIT A: audit_load) {

                result = A;
            }
        }
        return result;
    }



    public Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }









    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditDocument.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditDocument.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

                //CreateContactIntoDataBase();
                //displayContactInLog();
                Intent intent = new Intent(AuditDocument.this, MenuAgriculteur.class);  //check where is the main aduit button ?
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditDocument.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(AuditDocument.this, Audit.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                //DO nothing
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    private void displayAuditDocInLog() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        DOCUMENTDao documentDao = daoSession.getDOCUMENTDao();
        List<DOCUMENT> update_compte = documentDao.queryBuilder()
                .list();
        if (update_compte != null) {
            for (DOCUMENT c : update_compte) {
                Log.i("Information Document", "Document fourni :" + c.getDOCUMENT_FOURNI());
                Log.i("Information Document", "Type document :" + c.getTYPE_DOCUMENT());

            }
        }
    }

    //get audit
    private AUDIT getAuditfromID(long ID_audit){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        AUDIT result = null;
        List<AUDIT> audit_load = audit_data.queryBuilder()
                .where(AUDITDao.Properties.Id.eq(ID_audit))
                .list();
        if (audit_load != null) {
            for (AUDIT A: audit_load) {

                result = A;
            }
        }
        return result;
    }








}
