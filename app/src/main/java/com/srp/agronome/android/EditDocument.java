package com.srp.agronome.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.AUDIT_DOCUMENT;
import com.srp.agronome.android.db.AUDIT_DOCUMENTDao;
import com.srp.agronome.android.db.DOCUMENT;
import com.srp.agronome.android.db.DOCUMENTDao;
import com.srp.agronome.android.db.DOCUMENT_FOURNI;
import com.srp.agronome.android.db.DOCUMENT_FOURNIDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.TYPE_DOCUMENT;
import com.srp.agronome.android.db.TYPE_DOCUMENTDao;

import org.xml.sax.helpers.LocatorImpl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.srp.agronome.android.MenuAudit.Audit_Id_Selected;
import static com.srp.agronome.android.AuditDocument.ID_Audit_Document_Selected;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.R.string.LoginMissing;
import static com.srp.agronome.android.R.string.year;

//Edit Audit document class
public class EditDocument extends AppCompatActivity {

    Toolbar toolbar;
    private static int page = 0;

    private ImageButton Retour = null;

    private Button PrendrePhoto = null;

    private Button Validate = null;

    private EditText doc_type_text=null;

    private RadioGroup radioYesNo = null;

    private String  radioSelectedText=null;
    private String TAG="Edit Document Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_document);
        Log.i(TAG, "onCreate: Inside audit document");

        toolbar= (Toolbar) findViewById(R.id.ToolbarEditDocument);
        toolbar.setTitle("Edit Audit Document");


        PrendrePhoto = (Button) findViewById(R.id.btntake_photo);
       // PrendrePhoto.setOnClickListener(ButtonPrendrePhotoHandler);

        Retour = (ImageButton) findViewById(R.id.arrow_back_audit);
        Retour.setOnClickListener(ButtonRetourHandler);

        doc_type_text= (EditText) findViewById(R.id.doc_type);

        Validate = (Button) findViewById(R.id.btnvalidate_audit_doc);
        Validate.setOnClickListener(ButtonValidateHandler);

        radioYesNo = (RadioGroup) findViewById(R.id.OuiouNon);
        //get radio selection
        radioSelectedText=getRadioSelectedText(radioYesNo);

        AUDIT_DOCUMENT audit_doc_loaded=getAuditDocumentV2(Audit_Id_Selected);

       setDataAuditToWidget(audit_doc_loaded);


    }

    //get selected radio text
    public String getRadioSelectedText(RadioGroup radioGroupButton)
    {
        String text;
        radioGroupButton.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup group, int checkedId)
                                                  {
                                                      RadioButton radioButton = (RadioButton) findViewById(checkedId);
                                                      Toast.makeText(getBaseContext(), radioButton.getText(), Toast.LENGTH_SHORT).show();
                                                      radioSelectedText=radioButton.getText().toString();
                                                  }
                                              }
        );

        return radioSelectedText;
    }

    //set the previous data
    private void setDataAuditToWidget(AUDIT_DOCUMENT doc_id) {
        Log.i(TAG, "setDataAuditToWidget: inside function widget");
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        DOCUMENTDao documentDaodata = daoSession.getDOCUMENTDao();
        TYPE_DOCUMENTDao typeDocumentDao_data = daoSession.getTYPE_DOCUMENTDao();
        DOCUMENT_FOURNIDao document_fourniDao_data = daoSession.getDOCUMENT_FOURNIDao();
        AUDITDao auditDao = daoSession.getAUDITDao();
        Long type_doc = null;
        String doc_type = null;

        List<DOCUMENT> documentData = doc_id.getDOCUMENTList();

        if (documentData != null)
        {
            for (DOCUMENT d : documentData) {
                if (d.getID_AUDIT_DOCUMENT_DOCUMENT() == doc_id.getId()) {
                    type_doc = d.getID_TYPE_DOCUMENT();   //get to the type doc table
                }
            }

        //get doc type
        List<TYPE_DOCUMENT> type_document_data = typeDocumentDao_data.queryBuilder().where(TYPE_DOCUMENTDao.Properties.Id.eq(type_doc))
                .list();
        for (TYPE_DOCUMENT t : type_document_data) {
            if (t.getId() == type_doc)
                doc_type = t.getNom();  //get the doc type name
        }
        //show

        doc_type_text.setText(doc_type);
        String yesNo;
        //get oui non
        List<DOCUMENT_FOURNI> List_document_fourni = document_fourniDao_data.queryBuilder()
                .where(DOCUMENT_FOURNIDao.Properties.Nom.eq("Oui"))
                .list();
        if (List_document_fourni != null) {
            for (DOCUMENT_FOURNI S : List_document_fourni) {
                yesNo = S.getNom();
                if (yesNo.equals("Oui")) {
                    radioYesNo.check(R.id.radiobtndoc_oui);
                } else {
                    radioYesNo.check(R.id.radiobtndoc_non);
                }
            }
        }
    }
    else {
            Log.i(TAG, "setDataAuditToWidget: Document list null"+documentData.size());
        }
            //set email button

    }
    //valide
    View.OnClickListener ButtonValidateHandler= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //finish();
            //startActivity(new Intent(EditDocument.this,ListAudit.class));
            Log.i(TAG, "onClick: Button valide handler");

            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
                AUDIT audit_loaded=getAuditV2(Audit_Id_Selected);
                ID_Audit_Document_Selected= audit_loaded.getID_AUDIT_DOCUMENT();
                //get doc selected
                AUDIT_DOCUMENT audit_doc_loaded=getAuditDocfromID(ID_Audit_Document_Selected);
                updateAuditDocument(doc_type_text.getText().toString(), radioSelectedText,audit_doc_loaded);
                //startActivity(new Intent(EditAudit.this,MenuAudit.class));

            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));

            }

        }
    };


    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditDocument.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }


    //get audit_document from id
    private AUDIT_DOCUMENT getAuditDocfromID(long ID_audit_doc){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDIT_DOCUMENTDao audit_doc_data = daoSession.getAUDIT_DOCUMENTDao();
        AUDIT_DOCUMENT result = null;
        List<AUDIT_DOCUMENT> audit_load = audit_doc_data.queryBuilder()
                .where(AUDIT_DOCUMENTDao.Properties.Id.eq(ID_audit_doc))
                .list();
        if (audit_load != null) {
            for (AUDIT_DOCUMENT AD: audit_load) {

                result = AD;
            }
        }
        return result;
    }



    //Get the audit only with the ID common function
    private AUDIT getAuditV2(long ID_audit){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        AUDIT result = null;
        List<AUDIT> audit_load = audit_data.queryBuilder()
                .where(AUDITDao.Properties.Id.eq(ID_audit))
                .list();
        if (audit_load != null) {
            for (AUDIT A: audit_load) {

                result = A;
            }
        }
        return result;
    }



    private String InputToText() {
        String text = getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.TextTypeDocument) + " : " + doc_type_text.getText().toString() + "\n";
        text += getString(R.string.fourni) + " : " +radioSelectedText + "\n";

        return text;
    }


    private Boolean CheckInput(){
        boolean b = false;

        if (doc_type_text!=null) {
            b = true;
        }
        if (radioSelectedText!=null) {
            b = true;
        }

        return b;
    }


    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditDocument.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // dialog.cancel();
                startActivity(new Intent(EditDocument.this,MenuAudit.class));



            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }





    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
            //Selection_objetaudit_Choice();
            AUDIT_DOCUMENT audit_doc_loaded=getAuditDocumentV2(Audit_Id_Selected);
         //uy2   updateAuditDocument(doc_type_text.getText().toString(),);
            Intent intent = new Intent(EditDocument.this, EditAudit.class);
            finish();
            startActivity(intent);
        }
    };

    //update audit
    private  void updateAuditDocument(String docType,String provideDoc,AUDIT_DOCUMENT audit_document_selected)
    {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        DOCUMENTDao documentDaodata = daoSession.getDOCUMENTDao();
        TYPE_DOCUMENTDao typeDocumentDao_data = daoSession.getTYPE_DOCUMENTDao();
        DOCUMENT_FOURNIDao document_fourniDao_data = daoSession.getDOCUMENT_FOURNIDao();
        AUDIT_DOCUMENTDao audit_doc_data=daoSession.getAUDIT_DOCUMENTDao();

        //other id to update
        TYPE_DOCUMENT typeDoc_edit=new TYPE_DOCUMENT();
        DOCUMENT_FOURNI fourni_edit=new DOCUMENT_FOURNI();

        //get Document table id
       /*DOCUMENT doc_id_selected=new DOCUMENT();
        Long doc_selected_id;
        List<DOCUMENT> documentList=documentDaodata.loadAll();
        for(DOCUMENT ad: documentList)
        {
            //check for the selected audit
            if(ad.getID_AUDIT_DOCUMENT_DOCUMENT()==audit_document_selected.getId())
            {
               doc_id_selected=ad;
                doc_selected_id=ad.getId();
            }
        }*/

        //load document for selected id

        List<DOCUMENT> docList=documentDaodata.queryBuilder().
                where(DOCUMENTDao.Properties.ID_AUDIT_DOCUMENT_DOCUMENT.eq(audit_document_selected.getId())).list();
        for(DOCUMENT d:docList)
        {
            //set type document
            List<TYPE_DOCUMENT> typedocList=typeDocumentDao_data.queryBuilder().
                                            where(TYPE_DOCUMENTDao.Properties.Id.eq(d.getID_TYPE_DOCUMENT())).list();
            for(TYPE_DOCUMENT t: typedocList)
            {
                t.setNom(docType);
            }

            //set founri doc

            List<DOCUMENT_FOURNI> fourniList=document_fourniDao_data.queryBuilder().
                                             where(DOCUMENT_FOURNIDao.Properties.Id.eq(d.getID_DOCUMENT_FOURNI())).list();
            for(DOCUMENT_FOURNI f:fourniList)
            {
                f.setNom(provideDoc);
            }

        }

    }

    //Get the audit only with the ID
    private AUDIT_DOCUMENT getAuditDocumentV2(long ID_audit_doc){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDIT_DOCUMENTDao audit_doc_data = daoSession.getAUDIT_DOCUMENTDao();
        AUDIT_DOCUMENT result = null;
        List<AUDIT_DOCUMENT> audit_doc_load = audit_doc_data.queryBuilder()
                .where(AUDIT_DOCUMENTDao.Properties.Id.eq(ID_audit_doc))
                .list();
        if (audit_doc_load != null) {
            for (AUDIT_DOCUMENT ad: audit_doc_load) {

                result = ad;
            }
        }
        return result;
    }


}
