package com.srp.agronome.android.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "LISTE__EXPEDITEUR__ADMINISTRATION".
*/
public class Liste_Expediteur_AdministrationDao extends AbstractDao<Liste_Expediteur_Administration, Long> {

    public static final String TABLENAME = "LISTE__EXPEDITEUR__ADMINISTRATION";

    /**
     * Properties of entity Liste_Expediteur_Administration.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Nom = new Property(1, String.class, "nom", false, "NOM");
    }


    public Liste_Expediteur_AdministrationDao(DaoConfig config) {
        super(config);
    }
    
    public Liste_Expediteur_AdministrationDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"LISTE__EXPEDITEUR__ADMINISTRATION\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"NOM\" TEXT);"); // 1: nom
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"LISTE__EXPEDITEUR__ADMINISTRATION\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Liste_Expediteur_Administration entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String nom = entity.getNom();
        if (nom != null) {
            stmt.bindString(2, nom);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Liste_Expediteur_Administration entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String nom = entity.getNom();
        if (nom != null) {
            stmt.bindString(2, nom);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Liste_Expediteur_Administration readEntity(Cursor cursor, int offset) {
        Liste_Expediteur_Administration entity = new Liste_Expediteur_Administration( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1) // nom
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Liste_Expediteur_Administration entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setNom(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Liste_Expediteur_Administration entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Liste_Expediteur_Administration entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Liste_Expediteur_Administration entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
