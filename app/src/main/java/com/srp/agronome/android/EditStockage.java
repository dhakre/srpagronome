package com.srp.agronome.android;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.ETAT_STOCKEUR;
import com.srp.agronome.android.db.ETAT_STOCKEURDao;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.STATUT_STOCKEUR;
import com.srp.agronome.android.db.STATUT_STOCKEURDao;
import com.srp.agronome.android.db.STOCKAGE;
import com.srp.agronome.android.db.STOCKAGEDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.TYPE_STOCKEUR;
import com.srp.agronome.android.db.TYPE_STOCKEURDao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;

import static com.srp.agronome.android.MenuParcelle.Id_suivi_producteur;
import static com.srp.agronome.android.MenuParcelle.Id_stockage_selected;
/**
 * Created by jitendra on 21-08-2017.
 */

public class EditStockage extends AppCompatActivity {


    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    private static final float preferenceValuesf = 4;



    private EditText NOM_STOCKEUR   = null;
    private final static String KeyNOM_STOCKEUR  = "NOM_STOCKEUR ";



    private EditText IDENTIFIANT_STOCKEUR   = null;
    private final static String KeyIDENTIFIANT_STOCKEUR = "IDENTIFIANT_STOCKEUR ";

    private EditText CAPACITE    = null;
    private final static String KeyCAPACITE  = "CAPACITE";


    private Spinner SpinnerTYPE_STOCKEUR = null;
    private Spinner  SpinnerETAT_STOCKEUR = null;
    private Spinner SpinnerSTATUT_STOCKEUR = null;
    private ImageButton imgBtnHome = null;
    private Button BtnValidate = null;
    private Button PrendrePhoto = null;
    static TextView TextDate = null;
    static Date Debut_Projet = null;
    private final static String KeyDate = "KeyDate";
    private final static String datepicker = "datepicker";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_stockage); //Associer le layout
        FindWidgetViewbyId(); //Associate Widgets
        CreateListIntoDatabase();
        //Load spinner Type stockeur
        LoadSpinnerTYPE_STOCKEUR(SpinnerTYPE_STOCKEUR,getString(R.string.TextTYPE_STOCKEUR),
                getString(R.string.TextanotherTYPE_STOCKEUR),getString(R.string.TextValidateTYPE_STOCKEUR),
                getString(R.string.TextInputTYPE_STOCKEUR));

        LoadSpinnerETAT_STOCKEUR(SpinnerETAT_STOCKEUR,getString(R.string.TextETAT_STOCKEUR),
                getString(R.string.TextanotherETAT_STOCKEUR),getString(R.string.TextValidateETAT_STOCKEUR),
                getString(R.string.TextInputETAT_STOCKEUR));

        LoadSpinnerSTATUT_STOCKEUR(SpinnerSTATUT_STOCKEUR,getString(R.string.TextSTATUT_STOCKEUR),
                getString(R.string.TextanotherSTATUT_STOCKEUR),getString(R.string.TextValidateSTATUT_STOCKEUR),
                getString(R.string.TextInputSTATUT_STOCKEUR));



        STOCKAGE stockage_loaded = getStockageV2(Id_stockage_selected);
        setDatastockageToWidget(stockage_loaded);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_stockage);
        imgBtnHome.setOnClickListener(ButtonbackHandler);

        BtnValidate = (Button) findViewById(R.id.btnvalidate_CQ);
        BtnValidate.setOnClickListener(ButtonValidateHandler);

        PrendrePhoto = (Button) findViewById(R.id.stockage_Photo);
        PrendrePhoto.setOnClickListener(ButtonPrendrePhotoHandler);
        TextDate.setOnClickListener(TextViewHandler);
    }

    View.OnClickListener TextViewHandler = new View.OnClickListener() {
        public void onClick(View v) {
            showDatePickerDialog(v);
        }
    };
    public void showDatePickerDialog(View v) {

        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(),datepicker);
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Date displayed at the opening of the dialog box
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);  //
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Creates a new instance and returns it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (month <= 8) {
                EditStockage.TextDate.setText(day + "/0" + (month + 1) + "/" + year);
             // EditStockage.SaveDateInput(KeyDate, TextDate.getText().toString());
                Debut_Projet = Create_date(year, month, day);

            } else {
                TextDate.setText(day + "/" + (month + 1) + "/" + year);
              //  EditStockage.SaveDateInput(KeyDate, TextDate.getText().toString());
                Debut_Projet = Create_date(year, month, day);
            }
        }

    }
    public static Date Create_date(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
       // cal.set(Calendar.HOUR_OF_DAY, 0);
      //  cal.set(Calendar.MINUTE, 0);
       // cal.set(Calendar.SECOND, 0);
     //   cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }




    //Bouton back Toolbar
    View.OnClickListener ButtonbackHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };
    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(EditStockage.this, ListStockage.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(EditStockage.this, ListStockage.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void FindWidgetViewbyId() {
        NOM_STOCKEUR   = (EditText) findViewById(R.id.NOM_STOCKEUR);
        IDENTIFIANT_STOCKEUR   = (EditText) findViewById(R.id.IDENTIFIANT_STOCKEUR);
        CAPACITE   = (EditText) findViewById(R.id.CAPACITE);
        TextDate = (TextView) findViewById(R.id.Debut_projet);
        SpinnerTYPE_STOCKEUR = (Spinner) findViewById(R.id.SpinnerTYPE_STOCKEUR);
        SpinnerETAT_STOCKEUR = (Spinner) findViewById(R.id.SpinnerETAT_STOCKEUR);
        SpinnerSTATUT_STOCKEUR = (Spinner) findViewById(R.id.SpinnerSTATUT_STOCKEUR);
    }


    //Bouton Valider Toolbar
    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();}
        } };

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

    // Method to check if the user has entered the data

    private Boolean CheckInput(){
        boolean b = true;

        //Vérification des texte editables

        if (TextUtils.isEmpty(NOM_STOCKEUR .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(IDENTIFIANT_STOCKEUR  .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(CAPACITE  .getText().toString())) {
            b = false;
        }

        //Vérification des listes de choix :
        if (SpinnerTYPE_STOCKEUR.getSelectedItem().toString().equals(getString(R.string.TextTYPE_STOCKEUR))) {
            b = false;
        }
        if (SpinnerETAT_STOCKEUR.getSelectedItem().toString().equals(getString(R.string.TextETAT_STOCKEUR))) {
            b = false;
        }
        if (SpinnerSTATUT_STOCKEUR.getSelectedItem().toString().equals(getString(R.string.TextSTATUT_STOCKEUR))) {
            b = false;
        }

        return b;
    }
    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
             //   clearData();

                //Load stockage
                SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
                List <STOCKAGE> list_stockage = getStockageList(sp);
                STOCKAGE stockage_edit =  getStockage(list_stockage,Id_stockage_selected);

                // Edit stockage into db
                UpdateStockageIntoDatabase(stockage_edit,NOM_STOCKEUR.getText().toString().trim(), IDENTIFIANT_STOCKEUR.getText().toString().trim(),
                        Integer.valueOf(CAPACITE.getText().toString().trim()),Debut_Projet, SpinnerTYPE_STOCKEUR.getSelectedItem().toString(),
                        SpinnerETAT_STOCKEUR.getSelectedItem().toString(),
                        SpinnerSTATUT_STOCKEUR.getSelectedItem().toString());
                Intent intent = new Intent(EditStockage.this, MenuAgriculteur.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }



    private SUIVI_PRODUCTEUR getSuiviProducteurOfOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        SUIVI_PRODUCTEUR suivi_producteur_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        suivi_producteur_result = e.getSUIVI_PRODUCTEUR();
                    }
                }
            }
        }
        return suivi_producteur_result;
    }
    //Return a list of Stockage
    private List<STOCKAGE> getStockageList(SUIVI_PRODUCTEUR Suivi_producteur){
        return Suivi_producteur.getSTOCKAGEList();
    }

    //Return a stockage
    private STOCKAGE getStockage(List <STOCKAGE> list_stockage ,long ID_Stockage){
        STOCKAGE result = null;
        for (STOCKAGE S : list_stockage){
            if (S.getId() == ID_Stockage){
                result = S;
            }
        }
        return result;
    }


    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewQC);
        int position = 0 ;



        if (TextUtils.isEmpty(NOM_STOCKEUR.getText().toString())) {
            position = IDENTIFIANT_STOCKEUR.getTop();
        }

        if (TextUtils.isEmpty(IDENTIFIANT_STOCKEUR.getText().toString())) {
            position = IDENTIFIANT_STOCKEUR.getTop();
        }
        if (TextUtils.isEmpty(CAPACITE.getText().toString())) {
            position = CAPACITE.getTop();
        } }
    //A method that creates a string listing the data entered.
    private String InputToText(){
        String text = getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.NOM_STOCKEUR) + " : " + NOM_STOCKEUR.getText().toString() + "\n";
        text += getString(R.string.IDENTIFIANT_STOCKEUR) + " : " +IDENTIFIANT_STOCKEUR.getText().toString() + "\n";
        text += getString(R.string.CAPACITE) + " : " + CAPACITE.getText().toString() + "\n\n";
        text += getString(R.string.datedebutprojet) + " : " + TextDate.getText().toString() + "\n\n";
        text += getString(R.string.TextTYPE_STOCKEUR) + " : " + SpinnerTYPE_STOCKEUR.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextETAT_STOCKEUR) + " : " + SpinnerETAT_STOCKEUR.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextSTATUT_STOCKEUR) + " : " + SpinnerSTATUT_STOCKEUR.getSelectedItem().toString() + "\n\n";

        return text;
    }


    View.OnClickListener ButtonPrendrePhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            TakePicturefromCamera();
        }
    };
    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile("_Stockage" + "_HD.jpg");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivity(intent);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/stockage/2017/Soytouch");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        return file;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        //Depuis la caméra : pas de bitmap en sortie
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/stockage/2017/Soytouch");
        File file = new File(myDir, "_stockage_HD.jpg");
        Uri uri = Uri.fromFile(file);
        Bitmap newPicture;
        try {
            newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
            SaveImage(newPicture, "_stockage_HD.jpg");
            newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
            SaveImage(newPicture, "_stockage.jpg"); //Sauvegarde l'image dans le téléphone
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/stockage");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        //Log.i("File Location",file.toString());
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // Method for creating the selection lists in the database
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_STOCKEURDao TYPE_STOCKEUR_data = daoSession.getTYPE_STOCKEURDao();
        ETAT_STOCKEURDao ETAT_STOCKEUR_data = daoSession.getETAT_STOCKEURDao();
        STATUT_STOCKEURDao STATUT_STOCKEUR_data = daoSession.getSTATUT_STOCKEURDao();

        List<TYPE_STOCKEUR> List_TYPE_STOCKEUR = TYPE_STOCKEUR_data.loadAll();
        if (List_TYPE_STOCKEUR.size() == 0) {

            TYPE_STOCKEUR Type_Stockeur_input_1 = new TYPE_STOCKEUR();
            Type_Stockeur_input_1.setNom("Cellule");
            TYPE_STOCKEUR_data.insertOrReplace(Type_Stockeur_input_1);

            TYPE_STOCKEUR Type_Stockeur_input_2 = new TYPE_STOCKEUR();
            Type_Stockeur_input_2.setNom("Plat");
            TYPE_STOCKEUR_data.insertOrReplace(Type_Stockeur_input_2);
        }

        List <ETAT_STOCKEUR> List_ETAT_STOCKEUR = ETAT_STOCKEUR_data.loadAll();
        if (List_ETAT_STOCKEUR.size() == 0){

            ETAT_STOCKEUR Etat_Stockeur_input_1 = new ETAT_STOCKEUR();
            Etat_Stockeur_input_1.setNom("Existant");
            ETAT_STOCKEUR_data.insertOrReplace(Etat_Stockeur_input_1);

            ETAT_STOCKEUR Etat_Stockeur_input_2 = new ETAT_STOCKEUR();
            Etat_Stockeur_input_2.setNom("Projet");
            ETAT_STOCKEUR_data.insertOrReplace(Etat_Stockeur_input_2);
        }

        List <STATUT_STOCKEUR> List_STATUT_STOCKEUR = STATUT_STOCKEUR_data.loadAll();
        if (List_STATUT_STOCKEUR.size() == 0){

            STATUT_STOCKEUR Statut_Stockeur_input_1 = new STATUT_STOCKEUR();
            Statut_Stockeur_input_1.setNom("Autorisé de livraison");
            STATUT_STOCKEUR_data.insertOrReplace(Statut_Stockeur_input_1);

            STATUT_STOCKEUR Statut_Stockeur_input_2 = new STATUT_STOCKEUR();
            Statut_Stockeur_input_2.setNom("Sujet à contre-visite");
            STATUT_STOCKEUR_data.insertOrReplace(Statut_Stockeur_input_2);

            STATUT_STOCKEUR Statut_Stockeur_input_3 = new STATUT_STOCKEUR();
            Statut_Stockeur_input_3.setNom("Interdit de livraison");
            STATUT_STOCKEUR_data.insertOrReplace(Statut_Stockeur_input_3);
        }
    }

    private void LoadSpinnerTYPE_STOCKEUR (Spinner spinner , String TextHint , String TextAnother, final String TitleAddNew, final String TextAddNew){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_STOCKEURDao TYPE_STOCKEUR_data = daoSession.getTYPE_STOCKEURDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<TYPE_STOCKEUR> List_TYPE_STOCKEUR = TYPE_STOCKEUR_data.loadAll();
        if (List_TYPE_STOCKEUR != null) {
            for (TYPE_STOCKEUR TS : List_TYPE_STOCKEUR) {
                adapter.add(TS.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew,TextAddNew,1);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    private void LoadSpinnerETAT_STOCKEUR (Spinner spinner , String TextHint , String TextAnother, final String TitleAddNew, final String TextAddNew){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_STOCKEURDao ETAT_STOCKEUR_data = daoSession.getETAT_STOCKEURDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<ETAT_STOCKEUR> List_ETAT_STOCKEUR = ETAT_STOCKEUR_data.loadAll();
        if (List_ETAT_STOCKEUR != null) {
            for (ETAT_STOCKEUR ES : List_ETAT_STOCKEUR) {
                adapter.add(ES.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew,TextAddNew,2);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    private void LoadSpinnerSTATUT_STOCKEUR (Spinner spinner , String TextHint , String TextAnother, final String TitleAddNew, final String TextAddNew){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STATUT_STOCKEURDao STATUT_STOCKEUR_data = daoSession.getSTATUT_STOCKEURDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<STATUT_STOCKEUR> List_STATUT_STOCKEUR = STATUT_STOCKEUR_data.loadAll();
        if (List_STATUT_STOCKEUR != null) {
            for (STATUT_STOCKEUR SS : List_STATUT_STOCKEUR) {
                adapter.add(SS.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew,TextAddNew,3);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditStockage.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code TYPE_STOCKEUR
                        addTYPE_STOCKEUR(NewValueList);
                        break;
                    case 2 : //Code ETAT_STOCKEUR
                        addETAT_STOCKEUR(NewValueList);
                        break;
                    case 3 : //Code STATUT_STOCKEUR
                        addSTATUT_STOCKEUR(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void addTYPE_STOCKEUR(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_STOCKEURDao TYPE_STOCKEUR_data = daoSession.getTYPE_STOCKEURDao();
        TYPE_STOCKEUR TYPE_STOCKEUR_input = new TYPE_STOCKEUR();
        TYPE_STOCKEUR_input.setNom(Nom);
        TYPE_STOCKEUR_data.insertOrReplace(TYPE_STOCKEUR_input);
        LoadSpinnerTYPE_STOCKEUR(SpinnerTYPE_STOCKEUR,getString(R.string.TextTYPE_STOCKEUR),
                getString(R.string.TextanotherTYPE_STOCKEUR),getString(R.string.TextValidateTYPE_STOCKEUR),
                getString(R.string.TextInputTYPE_STOCKEUR));
    }

    private void addETAT_STOCKEUR(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_STOCKEURDao ETAT_STOCKEUR_data = daoSession.getETAT_STOCKEURDao();
        ETAT_STOCKEUR ETAT_STOCKEUR_input = new ETAT_STOCKEUR();
        ETAT_STOCKEUR_input.setNom(Nom);
        ETAT_STOCKEUR_data.insertOrReplace(ETAT_STOCKEUR_input);
        LoadSpinnerETAT_STOCKEUR(SpinnerETAT_STOCKEUR,getString(R.string.TextETAT_STOCKEUR),
                getString(R.string.TextanotherETAT_STOCKEUR),getString(R.string.TextValidateETAT_STOCKEUR),
                getString(R.string.TextInputETAT_STOCKEUR));
    }

    private void addSTATUT_STOCKEUR(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STATUT_STOCKEURDao STATUT_STOCKEUR_data = daoSession.getSTATUT_STOCKEURDao();
        STATUT_STOCKEUR STATUT_STOCKEUR_input = new STATUT_STOCKEUR();
        STATUT_STOCKEUR_input.setNom(Nom);
        STATUT_STOCKEUR_data.insertOrReplace(STATUT_STOCKEUR_input);
        LoadSpinnerSTATUT_STOCKEUR(SpinnerSTATUT_STOCKEUR,getString(R.string.TextSTATUT_STOCKEUR),
                getString(R.string.TextanotherSTATUT_STOCKEUR),getString(R.string.TextValidateSTATUT_STOCKEUR),
                getString(R.string.TextInputSTATUT_STOCKEUR));
    }




    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }


    // Input : PARCELLE objects
    private void setDatastockageToWidget(STOCKAGE stockage_selected){
        //Example EditText
        NOM_STOCKEUR.setText(stockage_selected.getNOM_STOCKEUR().toString().trim());
        IDENTIFIANT_STOCKEUR.setText(stockage_selected.getIDENTIFIANT_STOCKEUR().toString().trim());
        CAPACITE.setText(String.valueOf(stockage_selected.getCAPACITE().toString().trim())); //Float into String
        TextDate.setText(stockage_selected.getDATE_DEBUT_PROJET().toString().trim());

        //Example Spinner
        SpinnerAdapter TextAdaptertype_stockeur = SpinnerTYPE_STOCKEUR.getAdapter();
        for (int i = 0; i < TextAdaptertype_stockeur.getCount(); i++) {
            if (TextAdaptertype_stockeur.getItem(i).toString().equals(stockage_selected.getTYPE_STOCKEUR().getNom())) {
                SpinnerTYPE_STOCKEUR.setSelection(i);
            }
        }

        //Spinner ETAT_stockeur
        SpinnerAdapter TextAdapterEtat_stockeur = SpinnerETAT_STOCKEUR.getAdapter();
        for (int i = 0; i < TextAdapterEtat_stockeur.getCount(); i++) {
            if (TextAdapterEtat_stockeur.getItem(i).toString().equals(stockage_selected.getETAT_STOCKEUR().getNom())) {
                SpinnerETAT_STOCKEUR.setSelection(i);
            }
        }

        //Spinner IRRIGATION
        SpinnerAdapter TextAdapterSTATUT_STOCKEUR = SpinnerSTATUT_STOCKEUR.getAdapter();
        for (int i = 0; i < TextAdapterSTATUT_STOCKEUR.getCount(); i++) {
            if (TextAdapterSTATUT_STOCKEUR.getItem(i).toString().equals(stockage_selected.getSTATUT_STOCKEUR().getNom())) {
                SpinnerSTATUT_STOCKEUR.setSelection(i);
            }
        }

    }


    //BD pour Jitendra

    private STOCKAGE getStockageV2(long ID_Stockage){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        STOCKAGE result = null;
        List<STOCKAGE> stockage_load = stockage_data.queryBuilder()
                .where(STOCKAGEDao.Properties.Id.eq(ID_Stockage))
                .list();
        if (stockage_load != null) {
            for (STOCKAGE S : stockage_load) {
                result = S;
            }
        }
        return result;
    }

    private void UpdateStockageIntoDatabase(STOCKAGE stockage_edit , String Nom , String Identifiant, int Capacite, Date Debut_Projet , String Type_Stockeur , String Etat_Stockeur, String Statut_Stockeur){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        TYPE_STOCKEURDao type_stockeur_data = daoSession.getTYPE_STOCKEURDao();
        ETAT_STOCKEURDao etat_stockeur_data = daoSession.getETAT_STOCKEURDao();
        STATUT_STOCKEURDao statut_stockeur_data = daoSession.getSTATUT_STOCKEURDao();

        stockage_edit.setNOM_STOCKEUR(Nom);
        stockage_edit.setIDENTIFIANT_STOCKEUR(Identifiant);
        stockage_edit.setCAPACITE(Capacite);
        stockage_edit.setDATE_DEBUT_PROJET(Debut_Projet);
        stockage_edit.setDate_modification(Today_Date());
        stockage_edit.setID_Agronome_Modification((int)ID_Compte_Selected);

        //Edit Type Stockeur
        List<TYPE_STOCKEUR> List_Type_Stockeur = type_stockeur_data.queryBuilder()
                .where(TYPE_STOCKEURDao.Properties.Nom.eq(Type_Stockeur))
                .list();
        if (List_Type_Stockeur != null) {
            for (TYPE_STOCKEUR TS : List_Type_Stockeur) {
                stockage_edit.setTYPE_STOCKEUR(TS);
                stockage_edit.setID_TYPE_STOCKEUR(TS.getId());
            }
        }

        //Edit Etat Stockeur
        List<ETAT_STOCKEUR> List_Etat_Stockeur = etat_stockeur_data.queryBuilder()
                .where(ETAT_STOCKEURDao.Properties.Nom.eq(Etat_Stockeur))
                .list();
        if (List_Etat_Stockeur != null) {
            for (ETAT_STOCKEUR ES : List_Etat_Stockeur ) {
                stockage_edit.setETAT_STOCKEUR(ES);
                stockage_edit.setID_ETAT_STOCKEUR(ES.getId());
            }
        }

        //Edit Statut Stockeur
        List<STATUT_STOCKEUR> List_Statut_Stockeur = statut_stockeur_data.queryBuilder()
                .where(STATUT_STOCKEURDao.Properties.Nom.eq(Statut_Stockeur))
                .list();
        if (List_Statut_Stockeur  != null) {
            for (STATUT_STOCKEUR SS : List_Statut_Stockeur) {
                stockage_edit.setSTATUT_STOCKEUR(SS);
                stockage_edit.setID_STATUT_STOCKEUR(SS.getId());
            }
        }
        stockage_data.update(stockage_edit); //Update in DB
    }
}



