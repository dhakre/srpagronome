package com.srp.agronome.android;

/**
 * Created by jitendra on 07-09-2017.
 */

public class VisiteClass {
    private String objetvisite;
    private String Raisonsociale;
    private String subject_principal;
    private String subject_secoundary;
    private String comment;
    private long ID_Visite;


    public VisiteClass(String Raison_sociale, long id_Visite, String objet_visite, String principal, String comment  ) {
        super();
       this.Raisonsociale = Raison_sociale;
        this.objetvisite = objet_visite;
       this.ID_Visite = id_Visite;
        this.subject_principal=principal;
        this.comment=comment;
        //this.subject_secoundary=secoundary;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getRaisonsociale() {
        return Raisonsociale;
    }

    public void setRaisonsociale(String Raison_sociale) {
        this.Raisonsociale = Raison_sociale;
    }

    public String getObjetvisite() {
        return objetvisite;
    }

    public void setObjetvisite(String objet_visite) {
        this.objetvisite = objet_visite;
    }


    public long getID_Visite() {return ID_Visite;}

    public void setID_Visite(long id_Visite) {this.ID_Visite = id_Visite;}

    public String getSubject_principal() {
        return subject_principal;
    }

    public void setSubject_principal(String subject_principal)
    {
        this.subject_principal = subject_principal;
    }

    public String getSubject_secoundary() {
        return subject_secoundary;
    }

    public void setSubject_secoundary(String subject_secoundary)
    {
        this.subject_secoundary = subject_secoundary;
    }

}
