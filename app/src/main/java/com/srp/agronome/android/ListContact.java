package com.srp.agronome.android;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.view.Menu;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.srp.agronome.android.db.Activite;
import com.srp.agronome.android.db.ActiviteDao;
import com.srp.agronome.android.db.Adresse;
import com.srp.agronome.android.db.AdresseDao;
import com.srp.agronome.android.db.Biologique;
import com.srp.agronome.android.db.BiologiqueDao;
import com.srp.agronome.android.db.Categorie_Contact;
import com.srp.agronome.android.db.Categorie_ContactDao;
import com.srp.agronome.android.db.Code_Postal_Ville;
import com.srp.agronome.android.db.Code_Postal_VilleDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.ContactDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Identite;
import com.srp.agronome.android.db.IdentiteDao;
import com.srp.agronome.android.db.Pays;
import com.srp.agronome.android.db.PaysDao;
import com.srp.agronome.android.db.Region;
import com.srp.agronome.android.db.RegionDao;
import com.srp.agronome.android.db.Sexe;
import com.srp.agronome.android.db.SexeDao;
import com.srp.agronome.android.db.Situation;
import com.srp.agronome.android.db.SituationDao;
import com.srp.agronome.android.db.Sociabilite;
import com.srp.agronome.android.db.SociabiliteDao;
import com.srp.agronome.android.db.Statut_Soytouch;
import com.srp.agronome.android.db.Statut_SoytouchDao;
import com.srp.agronome.android.db.Structure_Sociale;
import com.srp.agronome.android.db.Structure_SocialeDao;
import com.srp.agronome.android.db.Type_Bio;
import com.srp.agronome.android.db.Type_BioDao;

import static android.R.attr.data;
import static com.srp.agronome.android.CreateContact.Today_Date;
import static com.srp.agronome.android.CreateOrganization.Date_Arret;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.EXECUTE_ONES;
import static com.srp.agronome.android.MainLogin.ID_compte_server;
import static com.srp.agronome.android.MainLogin.Nom_Compte;

/**
 * Created by doucoure on 21/06/2017.
 */

public class ListContact extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private static final String TAG ="LISTCONTACT Activiy" ;
    int executeFun=1;
    private ArrayList<Contact> listContact = new ArrayList<Contact>();
    private  ArrayList<Contact> newListContact=new ArrayList<>();
    static com.srp.agronome.android.db.Contact ctOld;
    //private MenuItem searchMenuItem;
    //private TextWatcher mSearchTw;
    //hkjahsasaLJ
    int total;
    CustomImageAdapter adapter;
    EditText BarreRecherche;
    Toolbar toolbar;
    ProgressBar progressBar;
    //search text
    EditText editSearchText=null;

    TextView TitreToolbar = null;


    SharedPreferences wmbPreference1;
    boolean isFirstRun1;

    SharedPreferences wmbPreference;
    boolean isFirstRun;

    AddDataForAccounts ad= new AddDataForAccounts();

    long val=1;

    com.srp.agronome.android.db.Contact oldContact=new com.srp.agronome.android.db.Contact(val,"abc","1234abc",0,1,GlobalValues.Today_Date(),1,GlobalValues.Today_Date(),val,val,val,val,val);

    //search result
    ArrayList<Contact> contactFilteredList= new ArrayList<>();

    String newSearch=null;
    String str=null;
    public static String oldstrSearch=null;




    //widget

    private ListView listView;


    private ImageButton btnRetour = null;
    private ImageButton btnSearch = null;

    private MenuItem searchMenuItem;
    private SearchView searchView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_contact);




        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        //search
       // btnSearch= (ImageButton) findViewById(R.id.btn_search);
        //btnSearch.setOnClickListener(ButtonSearchHandler);



        //set search
        toolbar = (Toolbar) findViewById(R.id.toolbar);
       // progressBar= (ProgressBar) findViewById(R.id.progressbar);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(100);

        //get data for other accounts

        wmbPreference = PreferenceManager.getDefaultSharedPreferences(this);
        isFirstRun = wmbPreference.getBoolean("FIRSTRUN", true); //execute ones




        wmbPreference1 = PreferenceManager.getDefaultSharedPreferences(this);
        isFirstRun1 = wmbPreference1.getBoolean("FIRSTRUN1", true);

      /*  if (ID_compte_server == 1) {
            // ad.addDataJustine();

            if (isFirstRun) {
                Log.i(TAG, "doInBackground: writing data for justine");
                //check id data already exist in db
                ad.addDataJustine();

                //Code to run once
                SharedPreferences.Editor editor = wmbPreference.edit();
                editor.putBoolean("FIRSTRUN", false);
                editor.commit();
                Log.i(TAG, "onCreate:shared preference value changes for justine ");
            }
        }

        if (ID_compte_server == 2) {
            //execute ones
            // ad.addDataMathu();
            if (isFirstRun1) {
                Log.i(TAG, "doInBackground: writing data for Mathieu");
                ad.addDataMathu();

                //Code to run once
                SharedPreferences.Editor editor1 = wmbPreference1.edit();
                editor1.putBoolean("FIRSTRUN1", false);
                editor1.commit();
                Log.i("preference", "onCreate:shared preference value changes for mathieu ");
            }

        }*/








        //get search data
     editSearchText = (EditText) findViewById(R.id.searchText);
        //capture change
        oldstrSearch=editSearchText.getText().toString();
        // Capture Text in EditText
       editSearchText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg4) {
                // TODO Auto-generated method stub
               /* String text = editSearchText.getText().toString().toLowerCase(Locale.getDefault());
                contactFilteredList=adapter.filter(text);
                //check if search data
                if(contactFilteredList.size()==0)
                {
                    Toast.makeText(getApplicationContext(),R.string.searchComment,Toast.LENGTH_LONG).show();
                }
                else
                    {
                    Intent intent = new Intent(ListContact.this, FilterContact.class);
                    intent.putExtra("FilterContactData", contactFilteredList);
                    startActivity(intent);
                    }*/
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence str, int start, int end,
                                      int count) {
                // TODO Auto-generated method stub
                String text = editSearchText.getText().toString().toLowerCase(Locale.getDefault());
                if(count>=4) {
                    contactFilteredList = adapter.filter(text);
                    if(contactFilteredList.size()>0)
                    {
                        Intent intent = new Intent(ListContact.this, FilterContact.class);
                        intent.putExtra("FilterContactData", contactFilteredList);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(getApplicationContext(),R.string.searchComment,
                                Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                   // Toast.makeText(getApplicationContext(),R.string.enterMore, Toast.LENGTH_SHORT).show();
                }

            }
        });

        //get new search Data
        newSearch=getIntent().getStringExtra("newSearchData");
        //concatinate both data

       /* if(newSearch!=null)
        {
            if(newSearch.equals(oldstrSearch)) {
                //do nothing
            }
            else{
                 str = oldstrSearch + newSearch;   //concatinate both data
                editSearchText.setText(str);
            }
        }*/


        //progressBar.setVisibility(View.VISIBLE);
//        getSuppActionBar().setDisplayHomeAsUpEnabled(true);
//        getActionBar().setHomeButtonEnabled(true);
//        handleIntent(getIntent());
       // LoadList();
        listContact = LoadList();
        //check double
        newListContact=doubleContact(listContact);

        //total=795; // for admin
       // total=getProgressBarShowUp(ID_compte_server);
        total=794;
        Log.i("contact size", "listContact: "+listContact.size()); //795

        //progress bar
        /*if(listContact.size()>=total)
        {
            progressBar.setVisibility(View.GONE);
        }
        else
        {
            int count=0,pro=0,i=0;
            while(i!=listContact.size())
            {
                count+=count;
                pro=(count/total)*100;
                //publish progress
                progressBar.setProgress(pro);
                //progressBar.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);
                i++;

            }

          //  progressBar.setVisibility(View.VISIBLE);
        }*/

        for (int i = 0; i < listContact.size(); i++) {
            Log.i("DDD", "Contact /  NOM : " + listContact.get(i).nom);

        }

     // BarreRecherche = (EditText) findViewById(R.id.contact_recherche);
        //BarreRecherche.addTextChangedListener(mSearchTw);
        btnRetour = (ImageButton) findViewById(R.id.arrow_back_menu);
        btnRetour.setOnClickListener(ButtonRetourHandler);

        listView = (ListView) findViewById(R.id.listViewIndexable);
//        displayContactInLog();



//        /*adapter = new CustomImageAdapter(ListContact.this, listContact);
//        adapter.sort(new Comparator<Contact>() {
//            @Override
//            public int compare(Contact c1, Contact c2) {
//                String nom1 = c1.getNom();
//                String nom2 = c2.getNom();
//                return nom1.compareToIgnoreCase(nom2);
//            }
//        });*/
//
//        //creating new arraylist for holding names from the contacts arraylist
//        ArrayList<String> contacts = new ArrayList<>();
//        for (Contact c : listContact) {
//            // Log.i("listcontact data", "contact values: "+ c.getNom()+" "+c.getPrenom() );
//            contacts.add(c.getNom());
//        }
//
//        //show list of names after sorting please see on the logcat
//        Collections.sort(contacts, new Comparator<String>() {
//            @Override
//            public int compare(String s1, String s2) {
//                return s1.compareTo(s2);
//            }
//        });
//        for (String s : contacts) {
//            Log.i("contact nom=", "nom=" + s);
//        }

//
        //sort list according to contact names before sending to adapterINSI
        adapter = new CustomImageAdapter(ListContact.this, listContact);
        adapter.sort(new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                Log.i(TAG, "compare: INSIDE FUNCTION ");
                String nom1 = c1.getStatut_soytouch();
                Log.i(TAG, "compare: String1="+nom1);
                String nom2 = c2.getStatut_soytouch();
                Log.i(TAG, "compare: String1="+nom1);

                return nom1.compareToIgnoreCase(nom2);
            }
        });

        listView.setAdapter(adapter);
        //notify list adapter for data change
        adapter.notifyDataSetChanged();
        listView.setFastScrollEnabled(true);


        //btnRecherche = (ImageButton) findViewById(R.id.btn_recherche);
        //btnRecherche.setOnClickListener(ButtonRecherche);

        TitreToolbar = (TextView) findViewById(R.id.Title_Liste_Contact);


        //display the contacts
//        displayContactInLog();
    } //end onCreate


    //set search menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }



    //search menu create
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu:  inside search menu  ");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        /*SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        //searchMenuItem = menu.findItem(R.id.contact_recherche);  //get entered text
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(), "en development", Toast.LENGTH_SHORT);
                toast.show();
            }
        });*/

        //my code
       /* final MenuItem myActionMenuItem = menu.findItem( R.id.search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Log.i(TAG, "onQueryTextSubmit: "+"SearchOnQueryTextSubmit: " + query);
                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                Log.i(TAG, "onQueryTextChange: "+"SearchOnQueryTextChanged: " + s);
                return false;
            }
        });

        return true;
    }*/

    //set action bar

    /*private void setActionBar() {
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("Search");

        Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/vegur_2.otf");
        int titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        TextView actionBarTitle = (TextView) (this.findViewById(titleId));
        actionBarTitle.setTypeface(typeface);
    }*/


    View.OnClickListener ButtonRecherche = new View.OnClickListener() {
        public void onClick(View v) {
          /* BarreRecherche.setVisibility((BarreRecherche.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);

            TitreToolbar.setVisibility((TitreToolbar.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);

            if (BarreRecherche.getVisibility() == View.VISIBLE) {
                toolbar.setBackgroundColor(Color.WHITE);
            } else {
                toolbar.setBackgroundColor(Color.parseColor("#62B66D"));
            }*/
            //open popup

            //My correct code

            Context wrapper = new android.view.ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Assign style
            android.widget.PopupMenu popup = new android.widget.PopupMenu(wrapper, BarreRecherche); //Creatting popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_search, popup.getMenu());  //menu popup
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals(getString(R.string.Rechearch))){
                        //enteredSearchIteam= (EditText) item;
                        Toast toast = Toast.makeText(getApplicationContext(), "Please enter a contact to search", Toast.LENGTH_SHORT);
                        toast.show();
                    }


                    return true;
                }
            });
           // popup.show();//showing popup menu

            //get the entered element
              Toast toast = Toast.makeText(getApplicationContext(), "inside search funtion", Toast.LENGTH_SHORT);
                   // toast.show();

        }
    };


    private ArrayList<Contact> LoadList() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ArrayList<Contact> lste = new ArrayList<>();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                ArrayList<String> lsteName = new ArrayList<>();
                for (Entreprise e : List_Entreprise) {
                    if (e.getArchive() != 1) {
                        String Nom_Entreprise = e.getRaison_sociale();
                        Boolean Ename;
                        String value="null";
                        Ename = !Nom_Entreprise.equals(value);
                        if (Ename)
                            {
                            //String Nom_Entreprise = e.getRaison_sociale();
                        long Id_Entreprise = e.getId();
                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                                Log.i("LoadList", "LoadList:lis contact size "+List_Contact.size());

                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                            if ((ct.getArchive() != 1)) {
                                if ((ct.getIdentite().getNom() != null)) {
                                    String name = ct.getIdentite().getNom();
                                    if (name != null){
                                        name = name.trim().toUpperCase();
                                    }
                                    String surname = ct.getIdentite().getPrenom();
                                    if (surname != null){
                                        surname = surname.trim().toUpperCase();
                                    }
                                    String nameToAdd = name+"_"+surname;
                                    Log.i("CCC", "Contact /  NOM : " + ct.getIdentite().getNom() + " , PRENOM : " + ct.getIdentite().getPrenom());

                                    if (!lsteName.contains(nameToAdd)) {
                                        Log.i("LOADLIST", "Contact / nouveau NOM ajouté : " + nameToAdd);
                                        lste.add(new Contact(
                                                ct.getIdentite().getPhoto(), //Photo
                                                ct.getIdentite().getNom(), //Nom

                                                ct.getIdentite().getPrenom(), //Prenom
                                                ct.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal(), //Code Postale
                                                Nom_Entreprise, //Nom de l'entreprise
                                                ct.getIdentite().getAdresse().getCode_Postal_Ville().getVille(), //Ville
                                                ct.getIdentite().getAdresse().getAdresse_numero_rue(),
                                                "N° : " + ct.getId(), //Numéro de contact
                                                removeNull(ct.getIdentite().getTelephone_principal()),//Téléphone
                                                Id_Entreprise, // ID entreprise
                                                ct.getId())); //ID_Contact
                                        // add name in control list
                                        lsteName.add(nameToAdd);
                                    } else {
                                        Log.e("ERROR", "Contact /  NOM DEJA SAISIE: " + nameToAdd);
                                    }

                                    //assign old
                                  // oldContact=ct;
                                }
                                ctOld=ct;

                            }

                        }
                    }
                    else{
                            Log.i("enterprise", "LoadList:enterprise name is null ");
                        }
                    }
                }
            }
            // return lste;
        }

        return lste;
    }

    //remove null values
    public static String removeNull(String str)
    {   String string;
        if(str.equals("null")) {
            string = " ";
        }else {
            string=str;
        }
        return string;
    }

//    //Permet de vérifier les ajouts dans la base de données.
//    private void displayContactInLog() {
//        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
//        CompteDao compte_dao = daoSession.getCompteDao();
//        List<Compte> update_compte = compte_dao.queryBuilder()
//                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
//                .list();
//        if (update_compte != null) {
//            for (Compte c : update_compte) {
//                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
//                for (Entreprise e : List_Entreprise) {
//                    Log.i("Information Entreprise", "Liste des contacts de l'entreprise :" + e.getRaison_sociale());
//                    List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
//                    for (com.srp.agronome.android.db.Contact ct : List_Contact) {
//                        Log.i("Inside LISTCONTACT", "Contact /  NOM : " + ct.getIdentite().getNom() + " , PRENOM : " + ct.getIdentite().getPrenom());
//                        Log.i("LISTCONTACT ID", "contact ID : " + ct.getId().toString());
//                        Log.i("LISTCONTACT ARCHIEVE", "archieve=" +ct.getArchive());
//                        //Log.i("Inside LISTCONTACT", "con: ");
//                    }
//                }
//            }
//        }
//    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(ListContact.this, ContactMenu.class);
            finish();
            startActivity(intent);

        }
        return true;
    }

    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ListContact.this, ContactMenu.class);
            finish();
            startActivity(intent);
        }
    };

    View.OnClickListener ButtonSearchHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //call the textwatcher
           /* editSearchText.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg4) {
                    // TODO Auto-generated method stub
                    String text = editSearchText.getText().toString().toLowerCase(Locale.getDefault());
                    contactFilteredList=adapter.filter(text);
                    //check if search data
                    if(contactFilteredList.size()==0)
                    {
                        Toast.makeText(getApplicationContext(),R.string.searchComment,Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Intent intent = new Intent(ListContact.this, FilterContact.class);
                        intent.putExtra("FilterContactData", contactFilteredList);
                        startActivity(intent);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence str, int start, int end,
                                          int count) {
                    // TODO Auto-generated method stub

                }
            });*/
        }
    };

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }

//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        return false;
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    @Override
//    public boolean onQueryTextChange(String newText) {
//        adapter.getFilter().filter(newText);
//
//        // use to enable search view popup text
////        if (TextUtils.isEmpty(newText)) {
////            friendListView.clearTextFilter();
////        }
////        else {
////            friendListView.setFilterText(newText.toString());
////        }
//
//        return true;
//    }

    //see double contacts
    public ArrayList<Contact> doubleContact(ArrayList<Contact> list)
    {
        Contact ct,cd;
        ArrayList<Contact> newlist = new ArrayList<Contact>();
        for(int i=0;i<list.size();i++)
        {
            ct=list.get(i);
            //check double
            for(int j=i+1;j<list.size();j++)
            {
                if(((ct.getNom().equals(list.get(j).getNom()))&&((ct.getPrenom().equals(ct.getPrenom()))))||(ct.getNom().equals(" ")))
                {
                    Log.i("Double", "doubleContact:  double contact"+ct.getNom());
                }
                else
                {
                    Log.i("NoDouble", "doubleContact: No double contact");
                    cd=list.get(j);
                    newlist.add(cd);
                }
            }
        }
        return listContact;
    }

    //get the progress bar according to the user
    public int getProgressBarShowUp(long userType)
    {
        int totalSize=0;
        if(userType==1)
        {
            totalSize=209;
        }
        if (userType==2){
            totalSize=88;
        }
        if ((userType==4)||(userType==3))
        {
            totalSize=794;
        }
        return totalSize;
    }

    //get the search working
    public void searchEnabled(EditText searchText)
    {
        String str=searchText.getText().toString();
        if(str.length()>=4)
        {
            searchText.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg4) {
                    // TODO Auto-generated method stub
                    String text = editSearchText.getText().toString().toLowerCase(Locale.getDefault());
                    contactFilteredList=adapter.filter(text);
                    //check if search data
                    if(contactFilteredList.size()==0)
                    {
                        Toast.makeText(getApplicationContext(),R.string.searchComment,Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Intent intent = new Intent(ListContact.this, FilterContact.class);
                        intent.putExtra("FilterContactData", contactFilteredList);
                        startActivity(intent);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence str, int start, int end,
                                          int count) {
                    // TODO Auto-generated method stub

                }
            });
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Please enter more text",Toast.LENGTH_LONG).show();
        }
    }


    //new
    public class AddDataForAccounts {
        String name = null;
        String fname = null;
        long idEnterprise;

        //add Data for justine
        public void addDataJustine() {

            //for justine
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            name = "CERA";
            fname = "LOUIS";

            DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            Log.i("CONTACTMENU", " Reading data from file");
            InputStreamReader in;
            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            String[] csv = new String[1000];
            AssetManager assetManager = getApplicationContext().getAssets();

            int i = 1;
            try {
                in = new InputStreamReader(assetManager.open("justine.csv"));
                br = new BufferedReader(in);
                Log.i("ContactMenu", "addData: Tying to read csv file");
                while ((line = br.readLine()) != null) {
                    csv = line.split(cvsSplitBy, 33);

                    Log.i("ContactMenu", "addData: Reading Data" + csv.toString());

                    if (i != 1) {
                        String arch = "0";
                        String archive = arch;
                        Date date_creation = GlobalValues.Today_Date();
                        Date date_modif = GlobalValues.Today_Date();
                        String nom = csv[18];
                        String prenom = csv[19];
                        String code_postale = csv[10];
                        String raison_sociale = csv[1];
                        String statut_soytouch = csv[4];
                        String telephone_entreprise = csv[13];
                        Date datecreation = date_creation;
                        Date datemodification = date_modif;
                        //empty values
                        String siret = " ";
                        String projet_connu = " ";
                        String fax = " ";
                        String adresse_mail_principale = " ";
                        String adresse_mail_secondaire = " ";
                        String situation = " ";
                        String structure_sociale = " ";
                        String ville = " ";
                        String pays = " ";
                        String region = " ";
                        String type_bio = " ";
                        String sociabilite = " ";
                        String telephone_principal = " ";
                        String telephone_secondaire = " ";
                        String adresse_mail = " ";
                        String activite_principale = " ";
                        String activite_secondaire = " ";
                        String biologique = " ";
                        String cause_arret = " ";
                        String numero_ncomplement_adresseom_rue = " ";
                        String numero_nom_rue = " ";
                        String sexe = " ";
                        String categorie = " ";
                        String remarque = " ";
                        String complement_adresse = " ";

                        //dont add same value
                        name = nom;
                        fname = prenom;


                        Log.i("CSV Data", "doInBackground:" + csv[0].toString() + "   " + csv[1].toString() + " " + csv[9] + " " + csv[10] + " " + csv[13] + " " + csv[18] + " " + csv[19]);  // for justine

                        // entreprise

                        Log.i("CREATION", "writing data into DB");
                        Log.i("CSV data", "ADD_DATA: getting data from csv");

                        //check exist
                        if (alreadyExistContact(name, fname)) {
                            Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
                            SituationDao situation_data = daoSession.getSituationDao();
                            Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
                            ActiviteDao activite_data = daoSession.getActiviteDao();
                            BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
                            Type_BioDao type_bio_data = daoSession.getType_BioDao();

                            AdresseDao adresse_data = daoSession.getAdresseDao();
                            Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
                            PaysDao pays_data = daoSession.getPaysDao();
                            RegionDao region_data = daoSession.getRegionDao();

                            EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

                            //Table Entreprise
                            Entreprise entreprise_input = new Entreprise();
                            entreprise_input.setRaison_sociale(raison_sociale);
                            //                        if (nombre_salarie != null) {
                            //                            entreprise_input.setNombre_salarie(Integer.parseInt(nombre_salarie));
                            //                        }

                            entreprise_input.setSIRET(siret);
                            entreprise_input.setProjet_connu(projet_connu);
                            entreprise_input.setTelephone_entreprise(telephone_entreprise);
                            entreprise_input.setFax_entreprise(fax);
                            entreprise_input.setAdresse_email_principale(adresse_mail_principale);
                            entreprise_input.setAdresse_email_secondaire(adresse_mail_secondaire);

                            entreprise_input.setArchive(Integer.parseInt(archive));
                            entreprise_input.setDate_creation(datecreation);
                            entreprise_input.setDate_modification(datemodification);
                            entreprise_input.setID_Agronome_Creation(1);
                            entreprise_input.setID_Agronome_Modification(1);
                            entreprise_input.setID_Compte(ID_Compte_Selected);//set the compte id

                            //Ajout de la table Situation
                            List<Situation> List_Situation = situation_data.queryBuilder()
                                    .where(SituationDao.Properties.Nom.eq(situation))
                                    .list();
                            if (List_Situation != null) {
                                for (Situation S : List_Situation) {
                                    entreprise_input.setSituation(S);
                                    entreprise_input.setID_Situation(S.getId());
                                }
                            }

                            //Ajout de la table Structure Sociale
                            List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                                    .where(Structure_SocialeDao.Properties.Nom.eq(structure_sociale))
                                    .list();
                            if (List_Structure_Sociale != null) {
                                for (Structure_Sociale SS : List_Structure_Sociale) {
                                    entreprise_input.setStructure_Sociale(SS);
                                    entreprise_input.setID_Structure_Sociale(SS.getId());
                                }
                            }

                            //Ajout de la table Statut SoyTouch
                            List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                                    .where(Statut_SoytouchDao.Properties.Nom.eq(statut_soytouch))
                                    .list();
                            if (List_Statut != null) {
                                for (Statut_Soytouch Sst : List_Statut) {
                                    entreprise_input.setStatut_Soytouch(Sst);
                                    entreprise_input.setID_Statut_Soytouch(Sst.getId());
                                }
                            }

                            //Ajout de la table Activite Principale
                            List<Activite> List_Activite_P = activite_data.queryBuilder()
                                    .where(ActiviteDao.Properties.Nom.eq(activite_principale))
                                    .list();
                            if (List_Activite_P != null) {
                                for (Activite AP : List_Activite_P) {
                                    entreprise_input.setActivite_Principale(AP);
                                    entreprise_input.setID_Activite_Principale(AP.getId());
                                }
                            }

                            //Ajout de la table Activite Secondaire si renseignée
                            if (!(activite_principale.equals(getString(R.string.TextActivitePrincipale)))) {
                                List<Activite> List_Activite_S = activite_data.queryBuilder()
                                        .where(ActiviteDao.Properties.Nom.eq(activite_secondaire))
                                        .list();
                                if (List_Activite_S != null) {
                                    for (Activite AS : List_Activite_S) {
                                        entreprise_input.setActivite_Secondaire(AS);
                                        entreprise_input.setID_Activite_Secondaire(AS.getId());
                                    }
                                }
                            }


                            //Ajout de la table Biologique

                            List<Biologique> List_Bio = biologique_data.queryBuilder()
                                    .where(BiologiqueDao.Properties.Nom.eq(biologique))
                                    .list();
                            if (List_Bio != null) {
                                for (Biologique B : List_Bio) {
                                    entreprise_input.setBiologique(B);
                                    entreprise_input.setID_Biologique(B.getId());
                                }
                            }


                            entreprise_input.setArret_activite(true);
                            entreprise_input.setCause_arret_activite(cause_arret);
                            entreprise_input.setDate_arret_activite(Date_Arret);


                            //Adresse_siege_social = Adresse de l'entreprise
                            Adresse adresse_siege_social_input = new Adresse();
                            adresse_siege_social_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                            adresse_siege_social_input.setComplement_adresse(complement_adresse); //Complement Adresse
                            adresse_siege_social_input.setArchive(GlobalValues.getInstance().getGlobalArchive());    //Global Value archieve
                            adresse_siege_social_input.setDate_creation(datecreation);
                            adresse_siege_social_input.setDate_modification(datemodification);
                            adresse_siege_social_input.setID_Agronome_Creation(1);
                            adresse_siege_social_input.setID_Agronome_Modification(1);

                            //Code postal adresse siège sociale
                            Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

                            code_postal_ville_siege_input.setCode_postal(code_postale);

                            code_postal_ville_siege_input.setVille(ville);
                            code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

                            //Pays adresse siège
                            Pays pays_siege_input = new Pays();
                            pays_siege_input.setNom(pays);
                            pays_data.insertOrReplace(pays_siege_input);

                            //Région adresse siège
                            Region region_siege_input = new Region();
                            region_siege_input.setNom(region);
                            region_data.insertOrReplace(region_siege_input);

                            adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
                            adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
                            adresse_siege_social_input.setPays(pays_siege_input);
                            adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
                            adresse_siege_social_input.setRegion(region_siege_input);
                            adresse_siege_social_input.setID_Region(region_siege_input.getId());
                            adresse_data.insertOrReplace(adresse_siege_social_input);

                            entreprise_input.setAdresse_Siege(adresse_siege_social_input);
                            entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

                            if (false) {
                                //Adresse_facturation
                                //                        Adresse adresse_facturation_input = new Adresse();
                                //                        adresse_facturation_input.setAdresse_numero_rue(Adresse_facturation_1.getText().toString().trim()); //Adresse
                                //                        adresse_facturation_input.setComplement_adresse(Adresse_facturation_2.getText().toString().trim()); //Complement Adresse
                                //                        adresse_facturation_input.setArchive(1);
                                //                        adresse_facturation_input.setDate_creation(Today_Date());
                                //                        adresse_facturation_input.setDate_modification(Today_Date());
                                //                        adresse_facturation_input.setID_Agronome_Creation(1);
                                //                        adresse_facturation_input.setID_Agronome_Modification(1);
                                //
                                //                        //Code postal adresse facturation
                                //                        Code_Postal_Ville code_postal_ville_facturation_input = new Code_Postal_Ville();
                                //                        code_postal_ville_facturation_input.setCode_postal(CodePostal_facturation.getText().toString().trim());
                                //                        code_postal_ville_facturation_input.setVille(Ville_facturation.getText().toString().trim());
                                //                        code_postal_ville_data.insertOrReplace(code_postal_ville_facturation_input);
                                //
                                //                        //Pays adresse facturation
                                //                        Pays pays_facturation_input = new Pays();
                                //                        pays_facturation_input.setNom(Pays_facturation.getText().toString().trim());
                                //                        pays_data.insertOrReplace(pays_facturation_input);
                                //
                                //                        //Région adresse facturation
                                //                        Region region_facturation_input = new Region();
                                //                        region_facturation_input.setNom(Region_facturation.getText().toString().trim());
                                //                        region_data.insertOrReplace(region_facturation_input);
                                //
                                //                        adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_facturation_input);
                                //                        adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_facturation_input.getId());
                                //                        adresse_facturation_input.setPays(pays_facturation_input);
                                //                        adresse_facturation_input.setID_Pays(pays_facturation_input.getId());
                                //                        adresse_facturation_input.setRegion(region_facturation_input);
                                //                        adresse_facturation_input.setID_Region(region_facturation_input.getId());
                                //                        adresse_data.insertOrReplace(adresse_facturation_input);
                                //
                                //                        entreprise_input.setAdresse_Facturation(adresse_facturation_input);
                                //                        entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
                            } else {
                                entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
                                entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
                            }

                            if (false) {
                                //Adresse livraison
                                //                        Adresse adresse_livraison_input = new Adresse();
                                //                        adresse_livraison_input.setAdresse_numero_rue(Adresse_livraison_1.getText().toString().trim()); //Adresse
                                //                        adresse_livraison_input.setComplement_adresse(Adresse_livraison_2.getText().toString().trim()); //Complement Adresse
                                //                        adresse_livraison_input.setArchive(1);
                                //                        adresse_livraison_input.setDate_creation(Today_Date());
                                //                        adresse_livraison_input.setDate_modification(Today_Date());
                                //                        adresse_livraison_input.setID_Agronome_Creation(1);
                                //                        adresse_livraison_input.setID_Agronome_Modification(1);
                                //
                                //                        //Code postal adresse livraison
                                //                        Code_Postal_Ville code_postal_ville_livraison_input = new Code_Postal_Ville();
                                //                        code_postal_ville_livraison_input.setCode_postal(CodePostal_livraison.getText().toString().trim());
                                //                        code_postal_ville_livraison_input.setVille(Ville_livraison.getText().toString().trim());
                                //                        code_postal_ville_data.insertOrReplace(code_postal_ville_livraison_input);
                                //
                                //                        //Pays adresse livraison
                                //                        Pays pays_livraison_input = new Pays();
                                //                        pays_livraison_input.setNom(Pays_livraison.getText().toString().trim());
                                //                        pays_data.insertOrReplace(pays_livraison_input);
                                //
                                //                        //Région adresse livraison
                                //                        Region region_livraison_input = new Region();
                                //                        region_livraison_input.setNom(Region_livraison.getText().toString().trim());
                                //                        region_data.insertOrReplace(region_livraison_input);
                                //
                                //                        adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_livraison_input);
                                //                        adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_livraison_input.getId());
                                //                        adresse_livraison_input.setPays(pays_livraison_input);
                                //                        adresse_livraison_input.setID_Pays(pays_livraison_input.getId());
                                //                        adresse_livraison_input.setRegion(region_livraison_input);
                                //                        adresse_livraison_input.setID_Region(region_livraison_input.getId());
                                //                        adresse_data.insertOrReplace(adresse_livraison_input);
                                //
                                //                        entreprise_input.setAdresse_Livraison(adresse_livraison_input);
                                //                        entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
                            } else {
                                entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
                                entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
                            }
                            //add data to enterprise table
                            entreprise_data.insertOrReplace(entreprise_input);


                            if ((!(type_bio.equals(getString(R.string.TextTypeBio))))) {

                                List<Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                                        .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                                        .list();
                                if (List_Entreprise_cible != null) {
                                    for (Entreprise e : List_Entreprise_cible) {

                                        List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                                .where(Type_BioDao.Properties.Nom.eq(type_bio))
                                                .list();
                                        if (List_Type_Bio != null) {
                                            for (Type_Bio TB : List_Type_Bio) {
                                                TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                                //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                                type_bio_data.insertOrReplace(TB);
                                                e.update();
                                            }
                                        }
                                    }
                                }
                            }


                            //Associer la nouvelle entreprise au compte
                            List<Compte> update_compte = compte_dao.queryBuilder()
                                    .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                    .list();
                            if (update_compte != null) {
                                for (Compte c : update_compte) {
                                    entreprise_input.setID_Compte(c.getId());
                                    idEnterprise=entreprise_data.insertOrReplace(entreprise_input);
                                    c.update();
                                }
                            }


                            ContactDao contact_data = daoSession.getContactDao();
                            Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
                            SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

                            IdentiteDao identite_data = daoSession.getIdentiteDao();
                            SexeDao sexe_data = daoSession.getSexeDao();


                            //Table Contact
                            com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
                            contact_input.setRemarque_contact(remarque);
                            // contact_input.setContact_identifiant(contact_identifiant);
                            contact_input.setArchive(Integer.parseInt(archive));
                            contact_input.setDate_creation(datecreation);
                            contact_input.setDate_modification(datemodification);
                            contact_input.setID_Agronome_Creation(1);
                            contact_input.setID_Agronome_Modification(1);

                            //Ajout de la table Categorie
                            List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                    .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
                                    .list();
                            if (List_Categorie != null) {
                                for (Categorie_Contact CC : List_Categorie) {
                                    contact_input.setCategorie_Contact(CC);
                                    contact_input.setID_Categorie_Contact(CC.getId());
                                }
                            }

                            //Ajout de la table Sociabilite
                            List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                    .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
                                    .list();
                            if (List_Sociabilite != null) {
                                for (Sociabilite S : List_Sociabilite) {
                                    contact_input.setSociabilite(S);
                                    contact_input.setID_Sociabilite(S.getId());
                                }
                            }


                            //Table identité
                            Identite identite_input = new Identite();
                            identite_input.setNom(nom); //Nom
                            identite_input.setPrenom(prenom); //Prenom
                            identite_input.setPhoto("");
                            identite_input.setTelephone_principal(telephone_principal); //Telephone principale
                            identite_input.setTelephone_secondaire(telephone_secondaire); //Telephoe secondaire
                            identite_input.setAdresse_email(adresse_mail); //Mail 1
                            identite_input.setArchive(Integer.parseInt(archive));
                            identite_input.setDate_creation(datecreation);
                            identite_input.setDate_modification(datemodification);
                            identite_input.setID_Agronome_Creation(1);
                            identite_input.setID_Agronome_Modification(1);

                            Sexe sexe_input = new Sexe();

                            sexe_input.setNom(sexe);

                            sexe_data.insertOrReplace(sexe_input);


                            //Gestion de la photo
                            //                    if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            //                        //Photo basse qualité
                            //                        File myDir_Source1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                            //                        File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");
                            //
                            //                        File myDir_Dest1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                            //                        File photoFile_Dest1 = new File(myDir_Dest1, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                            //                        try {
                            //                            CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
                            //                        } catch (IOException e) {
                            //                            e.printStackTrace();
                            //                        }
                            //                        //Photo haute qualité
                            //                        File myDir_Source2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                            //                        File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");
                            //
                            //                        File myDir_Dest2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                            //                        File photoFile_Dest2 = new File(myDir_Dest2, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture_HD.jpg");
                            //                        try {
                            //                            CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
                            //                        } catch (IOException e) {
                            //                            e.printStackTrace();
                            //                        }
                            //
                            //                        identite_input.setPhoto(Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                            //                    }
                            //                    else{
                            //
                            //                        identite_input.setPhoto("");
                            //                    }

                            //Table Adresse
                            Adresse adresse_input = new Adresse();
                            adresse_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                            adresse_input.setComplement_adresse(complement_adresse); //Complement Adresse
                            adresse_input.setArchive(Integer.parseInt(archive));
                            adresse_input.setDate_creation(Today_Date());
                            adresse_input.setDate_modification(Today_Date());
                            adresse_input.setID_Agronome_Creation(1);
                            adresse_input.setID_Agronome_Modification(1);


                            Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
                            code_postal_ville_input.setCode_postal(code_postale);
                            code_postal_ville_input.setVille(ville);
                            code_postal_ville_data.insertOrReplace(code_postal_ville_input);

                            Region region_input = new Region();
                            region_input.setNom(region);
                            region_data.insertOrReplace(region_input);

                            Pays pays_input = new Pays();
                            pays_input.setNom(pays);
                            pays_data.insertOrReplace(pays_input);

                            //Cardinalités entre les tables :
                            adresse_input.setCode_Postal_Ville(code_postal_ville_input);
                            adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
                            adresse_input.setRegion(region_input);
                            adresse_input.setID_Region(region_input.getId());
                            adresse_input.setPays(pays_input);
                            adresse_input.setID_Pays(pays_input.getId());
                            adresse_data.insertOrReplace(adresse_input);


                            identite_input.setAdresse(adresse_input);
                            identite_input.setID_Adresse(adresse_input.getId());

                            identite_input.setSexe(sexe_input);
                            identite_input.setID_Sexe(sexe_input.getId());

                            identite_data.insertOrReplace(identite_input);
                            //Un contact possède une identité
                            contact_input.setIdentite(identite_input);
                            contact_input.setID_Identite_Contact(identite_input.getId());

                            //Ajout du contact à l'entreprise concernée
                            List<Compte> update_compte1 = compte_dao.queryBuilder()
                                    .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                    .list();
                            if (update_compte1 != null) {
                                for (Compte c : update_compte1) {
                                    List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                                    for (Entreprise e : List_Company) {
                                        if (e.getRaison_sociale().equals(raison_sociale)) {
                                            contact_input.setID_Entreprise(e.getId());
                                            // contact_input.setId(idEnterprise);
                                            contact_data.insertOrReplace(contact_input);
                                            e.update();
                                        }
                                    }
                                    c.update();
                                }
                            }

                        }
                    }
                    i++;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }



        }

        //add data for mathieu
        public void addDataMathu() {
            //for justine
            //for justine and mathieu
            Log.i("CONTACTMENU", "addDataMathu: writing data for Mathieu");
            name ="Earl";
            fname="Earl";

            //get data from file
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();

            DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            Log.i("file", "doInBackground: Reading  mathieu");
            InputStreamReader in;
            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            String[] csv = new String[1000];
            AssetManager assetManager = getApplicationContext().getAssets();

            int i = 1;
            try {
                in = new InputStreamReader(assetManager.open("mathieu.csv"));
                br = new BufferedReader(in);
                Log.i("ContactMenu", "addData: Tying to read csv file");
                while ((line = br.readLine()) != null) {
                    csv = line.split(cvsSplitBy, 33);

                    Log.i("ContactMenu", "addData: Reading Data" + csv.toString());

                    if (i != 1) {
                        String arch = "0";
                        String archive = arch;
                        Date date_creation = GlobalValues.Today_Date();
                        Date date_modif = GlobalValues.Today_Date();

                        String raison_sociale = csv[1];
                        String code_postale = csv[11];
                        String statut_soytouch = csv[5];
                        String telephone_entreprise = csv[14];
                        String email_principal = csv[16];
                        String address_principal = csv[10];
                        String nom = csv[19];
                        String prenom = csv[20];

                        Date datecreation = date_creation;
                        Date datemodification = date_modif;
                        //empty values
                        String siret = " ";
                        String projet_connu = " ";
                        String fax = " ";
                        String adresse_mail_principale = " ";
                        String adresse_mail_secondaire = " ";
                        String situation = " ";
                        String structure_sociale = " ";
                        String ville = " ";
                        String pays = " ";
                        String region = " ";
                        String type_bio = " ";
                        String sociabilite = " ";
                        String telephone_principal = " ";
                        String telephone_secondaire = " ";
                        String adresse_mail = " ";
                        String activite_principale = " ";
                        String activite_secondaire = " ";
                        String biologique = " ";
                        String cause_arret = " ";
                        String numero_ncomplement_adresseom_rue = " ";
                        String numero_nom_rue = " ";
                        String sexe = " ";
                        String categorie = " ";
                        String remarque = " ";
                        String complement_adresse = " ";

                        //check dublicate
                        name = nom;
                        fname = prenom;
                        //
                        // executeCountMathu++;


                        Log.i("CSV Data", "doInBackground:" + csv[0].toString() + "   " + csv[1].toString() + " " + csv[5] + " " + csv[10] + " " + csv[11] + " " + csv[12] + " " + csv[14] + " " + csv[16] + " " + csv[19] + csv[20]);  // for justine

                        // entreprise

                        Log.i("CREATION", "writing data into DB");
                        Log.i("CSV data", "doInBackground: getting data from csv");

                        //check

                        if (alreadyExistContact(name, fname)) {
                            Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
                            SituationDao situation_data = daoSession.getSituationDao();
                            Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
                            ActiviteDao activite_data = daoSession.getActiviteDao();
                            BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
                            Type_BioDao type_bio_data = daoSession.getType_BioDao();

                            AdresseDao adresse_data = daoSession.getAdresseDao();
                            Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
                            PaysDao pays_data = daoSession.getPaysDao();
                            RegionDao region_data = daoSession.getRegionDao();

                            EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

                            //Table Entreprise
                            Entreprise entreprise_input = new Entreprise();
                            entreprise_input.setRaison_sociale(raison_sociale);
                            //                        if (nombre_salarie != null) {
                            //                            entreprise_input.setNombre_salarie(Integer.parseInt(nombre_salarie));
                            //                        }

                            entreprise_input.setSIRET(siret);
                            entreprise_input.setProjet_connu(projet_connu);
                            entreprise_input.setTelephone_entreprise(telephone_entreprise);
                            entreprise_input.setFax_entreprise(fax);
                            entreprise_input.setAdresse_email_principale(adresse_mail_principale);
                            entreprise_input.setAdresse_email_secondaire(adresse_mail_secondaire);

                            entreprise_input.setArchive(Integer.parseInt(archive));
                            entreprise_input.setDate_creation(datecreation);
                            entreprise_input.setDate_modification(datemodification);
                            entreprise_input.setID_Agronome_Creation(1);
                            entreprise_input.setID_Agronome_Modification(1);
                            entreprise_input.setID_Compte(ID_Compte_Selected);//set the compte id

                            //Ajout de la table Situation
                            List<Situation> List_Situation = situation_data.queryBuilder()
                                    .where(SituationDao.Properties.Nom.eq(situation))
                                    .list();
                            if (List_Situation != null) {
                                for (Situation S : List_Situation) {
                                    entreprise_input.setSituation(S);
                                    entreprise_input.setID_Situation(S.getId());
                                }
                            }

                            //Ajout de la table Structure Sociale
                            List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                                    .where(Structure_SocialeDao.Properties.Nom.eq(structure_sociale))
                                    .list();
                            if (List_Structure_Sociale != null) {
                                for (Structure_Sociale SS : List_Structure_Sociale) {
                                    entreprise_input.setStructure_Sociale(SS);
                                    entreprise_input.setID_Structure_Sociale(SS.getId());
                                }
                            }

                            //Ajout de la table Statut SoyTouch
                            List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                                    .where(Statut_SoytouchDao.Properties.Nom.eq(statut_soytouch))
                                    .list();
                            if (List_Statut != null) {
                                for (Statut_Soytouch Sst : List_Statut) {
                                    entreprise_input.setStatut_Soytouch(Sst);
                                    entreprise_input.setID_Statut_Soytouch(Sst.getId());
                                }
                            }

                            //Ajout de la table Activite Principale
                            List<Activite> List_Activite_P = activite_data.queryBuilder()
                                    .where(ActiviteDao.Properties.Nom.eq(activite_principale))
                                    .list();
                            if (List_Activite_P != null) {
                                for (Activite AP : List_Activite_P) {
                                    entreprise_input.setActivite_Principale(AP);
                                    entreprise_input.setID_Activite_Principale(AP.getId());
                                    entreprise_input.setID_Activite_Principale(AP.getId());
                                }
                            }

                            //Ajout de la table Activite Secondaire si renseignée
                            if (!(activite_principale.equals(getString(R.string.TextActivitePrincipale)))) {
                                List<Activite> List_Activite_S = activite_data.queryBuilder()
                                        .where(ActiviteDao.Properties.Nom.eq(activite_secondaire))
                                        .list();
                                if (List_Activite_S != null) {
                                    for (Activite AS : List_Activite_S) {
                                        entreprise_input.setActivite_Secondaire(AS);
                                        entreprise_input.setID_Activite_Secondaire(AS.getId());
                                    }
                                }
                            }


                            //Ajout de la table Biologique

                            List<Biologique> List_Bio = biologique_data.queryBuilder()
                                    .where(BiologiqueDao.Properties.Nom.eq(biologique))
                                    .list();
                            if (List_Bio != null) {
                                for (Biologique B : List_Bio) {
                                    entreprise_input.setBiologique(B);
                                    entreprise_input.setID_Biologique(B.getId());
                                }
                            }


                            entreprise_input.setArret_activite(true);
                            entreprise_input.setCause_arret_activite(cause_arret);
                            entreprise_input.setDate_arret_activite(Date_Arret);


                            //Adresse_siege_social = Adresse de l'entreprise
                            Adresse adresse_siege_social_input = new Adresse();
                            adresse_siege_social_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                            adresse_siege_social_input.setComplement_adresse(complement_adresse); //Complement Adresse
                            adresse_siege_social_input.setArchive(GlobalValues.getInstance().getGlobalArchive());    //Global Value archieve
                            adresse_siege_social_input.setDate_creation(datecreation);
                            adresse_siege_social_input.setDate_modification(datemodification);
                            adresse_siege_social_input.setID_Agronome_Creation(1);
                            adresse_siege_social_input.setID_Agronome_Modification(1);

                            //Code postal adresse siège sociale
                            Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

                            code_postal_ville_siege_input.setCode_postal(code_postale);

                            code_postal_ville_siege_input.setVille(ville);
                            code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

                            //Pays adresse siège
                            Pays pays_siege_input = new Pays();
                            pays_siege_input.setNom(pays);
                            pays_data.insertOrReplace(pays_siege_input);

                            //Région adresse siège
                            Region region_siege_input = new Region();
                            region_siege_input.setNom(region);
                            region_data.insertOrReplace(region_siege_input);

                            adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
                            adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
                            adresse_siege_social_input.setPays(pays_siege_input);
                            adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
                            adresse_siege_social_input.setRegion(region_siege_input);
                            adresse_siege_social_input.setID_Region(region_siege_input.getId());
                            adresse_data.insertOrReplace(adresse_siege_social_input);

                            entreprise_input.setAdresse_Siege(adresse_siege_social_input);
                            entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

                            if (false) {
                                //Adresse_facturation
                                //                        Adresse adresse_facturation_input = new Adresse();
                                //                        adresse_facturation_input.setAdresse_numero_rue(Adresse_facturation_1.getText().toString().trim()); //Adresse
                                //                        adresse_facturation_input.setComplement_adresse(Adresse_facturation_2.getText().toString().trim()); //Complement Adresse
                                //                        adresse_facturation_input.setArchive(1);
                                //                        adresse_facturation_input.setDate_creation(Today_Date());
                                //                        adresse_facturation_input.setDate_modification(Today_Date());
                                //                        adresse_facturation_input.setID_Agronome_Creation(1);
                                //                        adresse_facturation_input.setID_Agronome_Modification(1);
                                //
                                //                        //Code postal adresse facturation
                                //                        Code_Postal_Ville code_postal_ville_facturation_input = new Code_Postal_Ville();
                                //                        code_postal_ville_facturation_input.setCode_postal(CodePostal_facturation.getText().toString().trim());
                                //                        code_postal_ville_facturation_input.setVille(Ville_facturation.getText().toString().trim());
                                //                        code_postal_ville_data.insertOrReplace(code_postal_ville_facturation_input);
                                //
                                //                        //Pays adresse facturation
                                //                        Pays pays_facturation_input = new Pays();
                                //                        pays_facturation_input.setNom(Pays_facturation.getText().toString().trim());
                                //                        pays_data.insertOrReplace(pays_facturation_input);
                                //
                                //                        //Région adresse facturation
                                //                        Region region_facturation_input = new Region();
                                //                        region_facturation_input.setNom(Region_facturation.getText().toString().trim());
                                //                        region_data.insertOrReplace(region_facturation_input);
                                //
                                //                        adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_facturation_input);
                                //                        adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_facturation_input.getId());
                                //                        adresse_facturation_input.setPays(pays_facturation_input);
                                //                        adresse_facturation_input.setID_Pays(pays_facturation_input.getId());
                                //                        adresse_facturation_input.setRegion(region_facturation_input);
                                //                        adresse_facturation_input.setID_Region(region_facturation_input.getId());
                                //                        adresse_data.insertOrReplace(adresse_facturation_input);
                                //
                                //                        entreprise_input.setAdresse_Facturation(adresse_facturation_input);
                                //                        entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
                            } else {
                                entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
                                entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
                            }

                            if (false) {
                                //Adresse livraison
                                //                        Adresse adresse_livraison_input = new Adresse();
                                //                        adresse_livraison_input.setAdresse_numero_rue(Adresse_livraison_1.getText().toString().trim()); //Adresse
                                //                        adresse_livraison_input.setComplement_adresse(Adresse_livraison_2.getText().toString().trim()); //Complement Adresse
                                //                        adresse_livraison_input.setArchive(1);
                                //                        adresse_livraison_input.setDate_creation(Today_Date());
                                //                        adresse_livraison_input.setDate_modification(Today_Date());
                                //                        adresse_livraison_input.setID_Agronome_Creation(1);
                                //                        adresse_livraison_input.setID_Agronome_Modification(1);
                                //
                                //                        //Code postal adresse livraison
                                //                        Code_Postal_Ville code_postal_ville_livraison_input = new Code_Postal_Ville();
                                //                        code_postal_ville_livraison_input.setCode_postal(CodePostal_livraison.getText().toString().trim());
                                //                        code_postal_ville_livraison_input.setVille(Ville_livraison.getText().toString().trim());
                                //                        code_postal_ville_data.insertOrReplace(code_postal_ville_livraison_input);
                                //
                                //                        //Pays adresse livraison
                                //                        Pays pays_livraison_input = new Pays();
                                //                        pays_livraison_input.setNom(Pays_livraison.getText().toString().trim());
                                //                        pays_data.insertOrReplace(pays_livraison_input);
                                //
                                //                        //Région adresse livraison
                                //                        Region region_livraison_input = new Region();
                                //                        region_livraison_input.setNom(Region_livraison.getText().toString().trim());
                                //                        region_data.insertOrReplace(region_livraison_input);
                                //
                                //                        adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_livraison_input);
                                //                        adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_livraison_input.getId());
                                //                        adresse_livraison_input.setPays(pays_livraison_input);
                                //                        adresse_livraison_input.setID_Pays(pays_livraison_input.getId());
                                //                        adresse_livraison_input.setRegion(region_livraison_input);
                                //                        adresse_livraison_input.setID_Region(region_livraison_input.getId());
                                //                        adresse_data.insertOrReplace(adresse_livraison_input);
                                //
                                //                        entreprise_input.setAdresse_Livraison(adresse_livraison_input);
                                //                        entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
                            } else {
                                entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
                                entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
                            }
                            //add data to enterprise table
                            entreprise_data.insertOrReplace(entreprise_input);


                            if ((!(type_bio.equals(getString(R.string.TextTypeBio))))) {

                                List<Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                                        .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                                        .list();
                                if (List_Entreprise_cible != null) {
                                    for (Entreprise e : List_Entreprise_cible) {

                                        List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                                .where(Type_BioDao.Properties.Nom.eq(type_bio))
                                                .list();
                                        if (List_Type_Bio != null) {
                                            for (Type_Bio TB : List_Type_Bio) {
                                                TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                                //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                                type_bio_data.insertOrReplace(TB);
                                                e.update();
                                            }
                                        }
                                    }
                                }
                            }


                            //Associer la nouvelle entreprise au compte
                            List<Compte> update_compte = compte_dao.queryBuilder()
                                    .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                    .list();
                            if (update_compte != null) {
                                for (Compte c : update_compte) {
                                    entreprise_input.setID_Compte(c.getId());
                                    idEnterprise= entreprise_data.insertOrReplace(entreprise_input);
                                    c.update();
                                }
                            }


                            ContactDao contact_data = daoSession.getContactDao();
                            Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
                            SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

                            IdentiteDao identite_data = daoSession.getIdentiteDao();
                            SexeDao sexe_data = daoSession.getSexeDao();


                            //Table Contact
                            com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
                            contact_input.setRemarque_contact(remarque);
                            // contact_input.setContact_identifiant(contact_identifiant);
                            contact_input.setArchive(Integer.parseInt(archive));
                            contact_input.setDate_creation(datecreation);
                            contact_input.setDate_modification(datemodification);
                            contact_input.setID_Agronome_Creation(1);
                            contact_input.setID_Agronome_Modification(1);

                            //Ajout de la table Categorie
                            List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                    .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
                                    .list();
                            if (List_Categorie != null) {
                                for (Categorie_Contact CC : List_Categorie) {
                                    contact_input.setCategorie_Contact(CC);
                                    contact_input.setID_Categorie_Contact(CC.getId());
                                }
                            }

                            //Ajout de la table Sociabilite
                            List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                    .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
                                    .list();
                            if (List_Sociabilite != null) {
                                for (Sociabilite S : List_Sociabilite) {
                                    contact_input.setSociabilite(S);
                                    contact_input.setID_Sociabilite(S.getId());
                                }
                            }


                            //Table identité
                            Identite identite_input = new Identite();
                            identite_input.setNom(nom); //Nom
                            identite_input.setPrenom(prenom); //Prenom
                            identite_input.setPhoto("");
                            identite_input.setTelephone_principal(telephone_principal); //Telephone principale
                            identite_input.setTelephone_secondaire(telephone_secondaire); //Telephoe secondaire
                            identite_input.setAdresse_email(adresse_mail); //Mail 1
                            identite_input.setArchive(Integer.parseInt(archive));
                            identite_input.setDate_creation(datecreation);
                            identite_input.setDate_modification(datemodification);
                            identite_input.setID_Agronome_Creation(1);
                            identite_input.setID_Agronome_Modification(1);

                            Sexe sexe_input = new Sexe();

                            sexe_input.setNom(sexe);

                            sexe_data.insertOrReplace(sexe_input);


                            //Gestion de la photo
                            //                    if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            //                        //Photo basse qualité
                            //                        File myDir_Source1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                            //                        File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");
                            //
                            //                        File myDir_Dest1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                            //                        File photoFile_Dest1 = new File(myDir_Dest1, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                            //                        try {
                            //                            CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
                            //                        } catch (IOException e) {
                            //                            e.printStackTrace();
                            //                        }
                            //                        //Photo haute qualité
                            //                        File myDir_Source2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                            //                        File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");
                            //
                            //                        File myDir_Dest2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                            //                        File photoFile_Dest2 = new File(myDir_Dest2, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture_HD.jpg");
                            //                        try {
                            //                            CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
                            //                        } catch (IOException e) {
                            //                            e.printStackTrace();
                            //                        }
                            //
                            //                        identite_input.setPhoto(Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                            //                    }
                            //                    else{
                            //
                            //                        identite_input.setPhoto("");
                            //                    }

                            //Table Adresse
                            Adresse adresse_input = new Adresse();
                            adresse_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                            adresse_input.setComplement_adresse(complement_adresse); //Complement Adresse
                            adresse_input.setArchive(Integer.parseInt(archive));
                            adresse_input.setDate_creation(Today_Date());
                            adresse_input.setDate_modification(Today_Date());
                            adresse_input.setID_Agronome_Creation(1);
                            adresse_input.setID_Agronome_Modification(1);


                            Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
                            code_postal_ville_input.setCode_postal(code_postale);
                            code_postal_ville_input.setVille(ville);
                            code_postal_ville_data.insertOrReplace(code_postal_ville_input);

                            Region region_input = new Region();
                            region_input.setNom(region);
                            region_data.insertOrReplace(region_input);

                            Pays pays_input = new Pays();
                            pays_input.setNom(pays);
                            pays_data.insertOrReplace(pays_input);

                            //Cardinalités entre les tables :
                            adresse_input.setCode_Postal_Ville(code_postal_ville_input);
                            adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
                            adresse_input.setRegion(region_input);
                            adresse_input.setID_Region(region_input.getId());
                            adresse_input.setPays(pays_input);
                            adresse_input.setID_Pays(pays_input.getId());
                            adresse_data.insertOrReplace(adresse_input);


                            identite_input.setAdresse(adresse_input);
                            identite_input.setID_Adresse(adresse_input.getId());

                            identite_input.setSexe(sexe_input);
                            identite_input.setID_Sexe(sexe_input.getId());

                            identite_data.insertOrReplace(identite_input);
                            //Un contact possède une identité
                            contact_input.setIdentite(identite_input);
                            contact_input.setID_Identite_Contact(identite_input.getId());

                            //Ajout du contact à l'entreprise concernée
                            List<Compte> update_compte1 = compte_dao.queryBuilder()
                                    .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                    .list();
                            if (update_compte1 != null) {
                                for (Compte c : update_compte1) {
                                    List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                                    for (Entreprise e : List_Company) {
                                        if (e.getRaison_sociale().equals(raison_sociale)) {
                                            contact_input.setID_Entreprise(e.getId());
                                            //contact_input.setId(idEnterprise);
                                            contact_data.insertOrReplace(contact_input);
                                            e.update();
                                        }
                                    }
                                    c.update();
                                }
                            }

                        }
                    }
                    i++;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }



        }

        //check if data already exist in contact
        public boolean alreadyExistContact(String nom, String prenom) {
            boolean b = true;
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> update_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (update_compte != null) {
                for (Compte c : update_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise) {

                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {

                            if (ct.getIdentite().getNom().toString().trim().equals(nom.trim())) {
                                //if (ct.getIdentite().getPrenom().trim().equals(prenom.trim())) {
                                Log.i("already", "alreadyNOTExistContact: contact name=" + ct.getIdentite().getNom().toString() + " nom already=" + nom);
                                b = false;
                                /*} else {
                                    //if first name is null
                                    Log.i("already", "alreadyNOTExistContact: contact name=" + ct.getIdentite().getNom().toString() + " nom already=" + nom);
                                    b = false;

                                }*/
                            }
                            else {
                                Log.i(TAG, "alreadyExistContact: contact name=" + ct.getIdentite().getNom().toString() + " nom already=" + nom);
                            }


                        }
                    }
                }

            }
            return b;
        }


    }

}
