package com.srp.agronome.android.webservice.pojo;
import com.google.gson.annotations.SerializedName;

/**
 * Created by soytouchrp on 04/12/2017.
 */

public class CreateUserResponse {

        @SerializedName("name")
        public String name;
        @SerializedName("job")
        public String job;
        @SerializedName("id")
        public String id;
        @SerializedName("createdAt")
        public String createdAt;
    }