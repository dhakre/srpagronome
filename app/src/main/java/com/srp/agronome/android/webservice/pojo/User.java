package com.srp.agronome.android.webservice.pojo;

/**
 * Created by soytouchrp on 04/12/2017.
 */




//    @SerializedName("message")
//    public String name;
//    @SerializedName("status_code")
//    public int idAdresse;
//    @SerializedName("ID_FOURNISSEUR")
//    public String id;
//    @SerializedName("CREATION_DATE")
//    public String createdAt;
//
//    public User(String name, int idAdresse) {
//        this.name = name;
//        this.idAdresse = idAdresse;
//    }
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class User {

    @SerializedName("data")
    @Expose
    private DataUser data;


    public DataUser getData() {
        return data;
    }

    public void setData(DataUser data) {
        this.data = data;
    }
}




