package com.srp.agronome.android.webservice;

/**
 * Created by soytouchrp on 05/12/2017.
 */
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.internal.platform.Platform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoggingInterceptor implements Interceptor {

    public interface Logger {
        void log(String message);

        /** A {@link Logger} defaults output appropriate for the current platform. */
        Logger DEFAULT = new Logger() {
            @Override public void log(String message) {
                Platform.get().log(Platform.INFO, message, null);
            }
        };
    }
    private final Logger logger;

    public LoggingInterceptor(){
        this.logger = Logger.DEFAULT;
    }
    //Code pasted from okHttp webSite itself
    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        long t1 = System.nanoTime();
        logger.log("Interceptor Sample" + String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers()));

        try{
            okhttp3.Response response = chain.proceed(request);
            long t2 = System.nanoTime();
            logger.log("Interceptor Sample"+ String.format("Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers()));
        } catch (java.net.SocketTimeoutException e){

        }
        long t2 = System.nanoTime();

        return chain.proceed(request);

    }


}

