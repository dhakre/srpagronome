package com.srp.agronome.android;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

//this represents the basic Menu Activity
public abstract class BasicListViewActivity extends Activity {
    private ImageButton btnRetour =null;
    //private ListView listView;
    Button bnBasicList,basicCreate;
    private Activity basicActivity;
    CheckedTextView title;
    Toolbar toolbar;
    //private ArrayList<T> arrayListvisite = new ArrayList<T>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_list_view);


        basicActivity=this;


    }
    //map ui
    public void mapWidgets()
    {
       // listView= (ListView) findViewById(R.id.listview);
        bnBasicList= (Button) findViewById(R.id.btn_list_visite);
        basicCreate= (Button) findViewById(R.id.btn_ajout_visite);
       // title= (CheckedTextView) findViewById(R.id.BasicToolbar);
        toolbar=(Toolbar) findViewById(R.id.BasicToolbar);

    }

    //function to set the name of the buttons
    public abstract void setBasicListButtonName(String Bname);
    /*
    {
        bnBasicList.setText(Bname);
    }*/

    public abstract void setBasicButtonCreateList(String listname);

    /*{
        BasicListview.setText(listname);
    }*/
    //Button functions

    //set toolbar title according to the activity
    public abstract void setToolbarTitle(String name);

}
