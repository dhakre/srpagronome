package com.srp.agronome.android;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;

import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;


import com.srp.agronome.android.db.*;
import com.srp.agronome.android.db.Contact;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.Nom_Contact;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;
import static com.srp.agronome.android.MainLogin.Prenom_Contact;

/**
 * Created by doucoure on 12/06/2017.
 */

public class CreateContact extends AppCompatActivity {


    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    private Spinner Spinner_Entreprise = null;
    private Spinner Spinner_Categorie = null;
    private Spinner Spinner_Sociabilite = null;
    private ImageView Contact_Photo = null;

    private EditText First_Name = null;
    private final static String KeyFirstName = "KeyFirstName_CD";
    private EditText Last_Name = null;
    private final static String KeyLastName = "KeyLastName_CD";

    static TextView TextDate = null;
    static Date Birth_Date = null;
    private final static String KeyDate = "KeyDate_CD";

    private RadioGroup Sex = null;
    private EditText StreetAddress1 = null;
    private final static String KeyAddress1 = "KeyAddress1_CD";
    private EditText StreetAddress2 = null;
    private final static String KeyAddress2 = "KeyAddress2_CD";
    private EditText City = null;
    private final static String KeyCity = "KeyCity_CD";
    private EditText State = null;
    private final static String KeyState = "KeyState_CD";
    private EditText PostalCode = null;
    private final static String KeyPostalCode = "KeyPostalCode_CD";
    private EditText Country = null;
    private final static String KeyCountry = "KeyCountry_CD";
    private EditText PhoneNumber1 = null;
    private final static String KeyPhoneNumber1 = "KeyPhoneNumber1_CD";
    private EditText PhoneNumber2 = null;
    private final static String KeyPhoneNumber2 = "KeyPhoneNumber2_CD";
    private EditText Mail1 = null;
    private final static String KeyMail1 = "KeyMail1";
    private EditText Remarques = null;
    private final static String KeyRemarques = "KeyRemarques_CD";

    /**
     * Image bouton pour valider la saisie
     */
    private ImageButton imgBtnValidate = null;
    private Button BtnValidate = null;

    /**
     * Image bouton pour annuler
     */
    private ImageButton imgBtnHome = null;

    private static final int CAMERA_PIC_REQUEST = 2;
    private static final int GALLERY_PICK = 1;
    private static final int PICK_CROP = 3;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);
        FindWidgetViewbyId(); //Associate Widgets
        AddListenerEvent(); //Add Widgets Event
        GetStringData(); //Load String
        GetDate(); //Load Date
        CreateListIntoDatabase();
        LoadSpinnerCategorie();
        LoadSpinnerEntreprise();
        LoadSpinnerSociabilite();

        BtnValidate = (Button) findViewById(R.id.btnvalidate);
        BtnValidate.setOnClickListener(ButtonValidateHandler);

        imgBtnValidate = (ImageButton) findViewById(R.id.btn_ajoutContact);
        imgBtnValidate.setOnClickListener(ButtonValidateHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menu_annuaire);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);
    }


    View.OnClickListener TextViewHandler = new View.OnClickListener() {
        public void onClick(View v) {
            showDatePickerDialog(v);
        }
    };

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new CreateContact.DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Date affichée à l'ouverture de la boite de dialogue
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR) - 30;  //
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Crée une nouvelle instance et la retourne
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (month <= 8) {
                CreateContact.TextDate.setText(day + "/0" + (month + 1) + "/" + year);
                CreateContact.SaveDateInput(KeyDate, TextDate.getText().toString());
                Birth_Date = Create_date(year, month, day);

            } else {
                TextDate.setText(day + "/" + (month + 1) + "/" + year);
                CreateContact.SaveDateInput(KeyDate, TextDate.getText().toString());
                Birth_Date = Create_date(year, month, day);
            }
        }
    }

    /**
     * Retourne une date.
     *
     * @param year  Année
     * @param month Mois
     * @param day   Jour
     * @return Retourne la date sous le format "Date"
     */
    public static Date Create_date(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Retourne la date et l'heure actuelle
     *
     * @return Retourne la date et l'heure actuelle sous le format Date
     */

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    private void FindWidgetViewbyId() {
        Contact_Photo = (ImageView) findViewById(R.id.contact_picture);

        Last_Name = (EditText) findViewById(R.id.Contact_LastName);
        First_Name = (EditText) findViewById(R.id.Contact_FirstName);
        Sex = (RadioGroup) findViewById(R.id.CMaleOrFemale);
        TextDate = (TextView) findViewById(R.id.CBirthday);
        StreetAddress1 = (EditText) findViewById(R.id.CAddress1);
        StreetAddress2 = (EditText) findViewById(R.id.CAddress2);
        City = (EditText) findViewById(R.id.CCity);
        State = (EditText) findViewById(R.id.CState);
        PostalCode = (EditText) findViewById(R.id.CPostalCode);
        Country = (EditText) findViewById(R.id.CCountry);
        PhoneNumber1 = (EditText) findViewById(R.id.CPhoneNumber1);
        PhoneNumber2 = (EditText) findViewById(R.id.CPhoneNumber2);
        Mail1 = (EditText) findViewById(R.id.CMail1);
        Remarques = (EditText) findViewById(R.id.remarques_id);

        Spinner_Entreprise = (Spinner) findViewById(R.id.Spinner_Organization);
        Spinner_Categorie = (Spinner) findViewById(R.id.Spinner_Categorie);
        Spinner_Sociabilite = (Spinner) findViewById(R.id.Spinner_Sociabilite);
    }


    private void AddListenerEvent() {
        Contact_Photo.setOnClickListener(PhotoHandler);

        Last_Name.addTextChangedListener(generalTextWatcher);
        First_Name.addTextChangedListener(generalTextWatcher);
        TextDate.setOnClickListener(TextViewHandler);
        StreetAddress1.addTextChangedListener(generalTextWatcher);
        StreetAddress2.addTextChangedListener(generalTextWatcher);
        City.addTextChangedListener(generalTextWatcher);
        State.addTextChangedListener(generalTextWatcher);
        PostalCode.addTextChangedListener(generalTextWatcher);
        Country.addTextChangedListener(generalTextWatcher);
        PhoneNumber1.addTextChangedListener(generalTextWatcher);
        PhoneNumber2.addTextChangedListener(generalTextWatcher);
        Mail1.addTextChangedListener(generalTextWatcher);
        Remarques.addTextChangedListener(generalTextWatcher);
    }

    //Event Multiple EditText = save data
    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (Last_Name.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyLastName, Last_Name.getText().toString());
            } else if (First_Name.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyFirstName, First_Name.getText().toString());
            } else if (StreetAddress1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAddress1, StreetAddress1.getText().toString());
            } else if (StreetAddress2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAddress2, StreetAddress2.getText().toString());
            } else if (City.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCity, City.getText().toString());
            } else if (State.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyState, State.getText().toString());
            } else if (PostalCode.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPostalCode, PostalCode.getText().toString());
            } else if (Country.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCountry, Country.getText().toString());
            } else if (PhoneNumber1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPhoneNumber1, PhoneNumber1.getText().toString());
            } else if (PhoneNumber2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPhoneNumber2, PhoneNumber2.getText().toString());
            } else if (Mail1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyMail1, Mail1.getText().toString());
            } else if (Remarques.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyRemarques, Remarques.getText().toString());
            }
        }
    };


    private void SaveStringDataInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }

    private static void SaveDateInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }


    // Lire les données enregistrées et les mettre dans les definir dans les edit text
    private void GetStringData() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
        if (sharedpreferences.contains(KeyLastName)) {
            Last_Name.setText(sharedpreferences.getString(KeyLastName, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyFirstName)) {
            First_Name.setText(sharedpreferences.getString(KeyFirstName, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAddress1)) {
            StreetAddress1.setText(sharedpreferences.getString(KeyAddress1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAddress2)) {
            StreetAddress2.setText(sharedpreferences.getString(KeyAddress2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCity)) {
            City.setText(sharedpreferences.getString(KeyCity, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyState)) {
            State.setText(sharedpreferences.getString(KeyState, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPostalCode)) {
            PostalCode.setText(sharedpreferences.getString(KeyPostalCode, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCountry)) {
            Country.setText(sharedpreferences.getString(KeyCountry, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPhoneNumber1)) {
            PhoneNumber1.setText(sharedpreferences.getString(KeyPhoneNumber1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPhoneNumber2)) {
            PhoneNumber2.setText(sharedpreferences.getString(KeyPhoneNumber2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyMail1)) {
            Mail1.setText(sharedpreferences.getString(KeyMail1, ""), TextView.BufferType.EDITABLE);
        }
    }

    private static void GetDate() {
        if (sharedpreferences.contains(KeyDate)) {
            //Log.i("Test date", sharedpreferences.getString(KeyDate, ""));
            TextDate.setText(sharedpreferences.getString(KeyDate, ""));
        }
    }

    //Clear data in sharepreference
    private void clearData() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(KeyLastName);
        editor.remove(KeyFirstName);
        editor.remove(KeyDate);
        editor.remove(KeyAddress1);
        editor.remove(KeyAddress2);
        editor.remove(KeyCity);
        editor.remove(KeyState);
        editor.remove(KeyPostalCode);
        editor.remove(KeyCountry);
        editor.remove(KeyPhoneNumber1);
        editor.remove(KeyPhoneNumber2);
        editor.remove(KeyMail1);
        editor.remove(KeyRemarques);
        editor.apply();
    }


    //Event Clic on Photo :
    View.OnClickListener PhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Selection_Photo_Choice();
        }
    };

    //Afficher une boîte de dialogue contenant une liste de choix pour affecter une photo
    public void Selection_Photo_Choice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateContact.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionPhoto, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Prendre une photo
                        TakePicturefromCamera();
                        break;
                    case 1: //Choisir une photo depuis la galerie
                        pickImagefromGallery();
                        break;
                    case 2: //Rogner la photo affichée
                        //Verifie si il y a une photo
                        if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            CropImage();
                        }
                        break;
                    case 3: //Supprimer la photo -> mettre l'image de base à la place
                        Contact_Photo.setImageResource(R.mipmap.ic_add_photo);
                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }


    //Pick a photo from Gallery
    public void pickImagefromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICK);
    }

    //Take a photo from Camera
    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile("cache_picture_HD.jpg");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        return file;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == GALLERY_PICK) { //Depuis la galerie
            Uri uri = data.getData();
            Bitmap Picture_Load;
            try {
                Picture_Load = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                SaveImage(Picture_Load, "cache_picture_HD.jpg");
                Picture_Load = getResizedBitmap(Picture_Load, 256, 256);
                Contact_Photo.setImageBitmap(Picture_Load);
                SaveImage(Picture_Load, "cache_picture.jpg"); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_PIC_REQUEST) { //Depuis la caméra : pas de bitmap en sortie
            File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
            File file = new File(myDir, "cache_picture_HD.jpg");
            Uri uri = Uri.fromFile(file);
            Bitmap newPicture;
            try {
                newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
                SaveImage(newPicture, "cache_picture_HD.jpg");
                newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
                Contact_Photo.setImageBitmap(newPicture);
                SaveImage(newPicture, "cache_picture.jpg"); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_CROP) {
            File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
            File file = new File(myDir,"cache_picture_HD.jpg");
            Uri URI = Uri.fromFile(file);
            Bitmap Picture_Crop;
            try {
                Picture_Crop = MediaStore.Images.Media.getBitmap(getContentResolver(), URI);
                Contact_Photo.setImageBitmap(Picture_Crop);
                SaveImage(Picture_Crop, "cache_picture.jpg"); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Permet de rogner ou recadrer une image
    private void CropImage() {
        try {
            //Acceder à la photo à rogner
            File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
            File photoFile = new File(myDir, "cache_picture.jpg");
            Uri photoURI = FileProvider.getUriForFile(this,
                    "com.srp.android.fileprovider",
                    photoFile);
            getApplicationContext().grantUriPermission("com.android.camera", photoURI,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //Paramètres de l'Intent Crop
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //Android N need set permission to uri otherwise system camera don't has permission to access file wait crop
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // indicate image type and Uri
            cropIntent.setDataAndType(photoURI, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PICK_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
        }
    }


    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        //Log.i("File Location",file.toString());
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Méthode pour charger une image depuis le téléphone dans l'application.
    private void loadImageFromStorage(String FileName) {
        try {
            File f = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_Pictures/" + FileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            //Log.i("Load Bitmap","L'image est remplacée à l'écran");
            Contact_Photo.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            //Log.i("Load Bitmap","L'image est introuvable");
        }
    }

    //Copie un fichier source vers un fichier de destination
    public void CopyAndDeleteFileCache(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = new FileInputStream(sourceFile).getChannel();
        FileChannel destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size()); //Copie le fichier
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
        sourceFile.delete(); //Supprime le fichier une fois qu'il a ete copié
    }




    //Methode qui permet de créer les listes de choix dans la base de données.
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Categorie_ContactDao categorie_contact_data = daoSession.getCategorie_ContactDao();
        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

        List<Categorie_Contact> List_Categorie_Contact = categorie_contact_data.loadAll();
        if (List_Categorie_Contact.size() == 0) {

            Categorie_Contact Categorie_Contact_input_1 = new Categorie_Contact();
            Categorie_Contact_input_1.setNom("Prospect");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_1);

            Categorie_Contact Categorie_Contact_input_2 = new Categorie_Contact();
            Categorie_Contact_input_2.setNom("Premium");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_2);

            Categorie_Contact Categorie_Contact_input_3 = new Categorie_Contact();
            Categorie_Contact_input_3.setNom("Volatile");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_3);

            Categorie_Contact Categorie_Contact_input_4 = new Categorie_Contact();
            Categorie_Contact_input_4.setNom("Expert");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_4);

            Categorie_Contact Categorie_Contact_input_5 = new Categorie_Contact();
            Categorie_Contact_input_5.setNom("Simple");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_5);

            Categorie_Contact Categorie_Contact_input_6 = new Categorie_Contact();
            Categorie_Contact_input_6.setNom("Non intéressé");
            categorie_contact_data.insertOrReplace(Categorie_Contact_input_6);
        }

        List<Sociabilite> List_Sociabilite = sociabilite_data.loadAll();
        if (List_Sociabilite.size() == 0) {

            Sociabilite Sociabilite_input_1 = new Sociabilite();
            Sociabilite_input_1.setNom("Accueillant");
            sociabilite_data.insertOrReplace(Sociabilite_input_1);

            Sociabilite Sociabilite_input_2 = new Sociabilite();
            Sociabilite_input_2.setNom("Méfiant");
            sociabilite_data.insertOrReplace(Sociabilite_input_2);

            Sociabilite Sociabilite_input_3 = new Sociabilite();
            Sociabilite_input_3.setNom("Réfractaire");
            sociabilite_data.insertOrReplace(Sociabilite_input_3);

            Sociabilite Sociabilite_input_4 = new Sociabilite();
            Sociabilite_input_4.setNom("Antipathique");
            sociabilite_data.insertOrReplace(Sociabilite_input_4);

            Sociabilite Sociabilite_input_5 = new Sociabilite();
            Sociabilite_input_5.setNom("Sympathique");
            sociabilite_data.insertOrReplace(Sociabilite_input_5);

        }

    }

    //Charger le spinner Categorie Contact
    private void LoadSpinnerCategorie() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Categorie_ContactDao Categorie_Contact_data = daoSession.getCategorie_ContactDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherCategorie)); //Text Add a new structure sociale
        //Charger les éléments de la liste
        List<Categorie_Contact> List_Categorie = Categorie_Contact_data.loadAll();
        if (List_Categorie != null) {
            for (Categorie_Contact CC : List_Categorie) {
                adapter.add(CC.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextCategorie)); //This is the text that will be displayed as hint.
        Spinner_Categorie.setAdapter(adapter);
        Spinner_Categorie.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Categorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidateCategorie),getString(R.string.TextInputCategorie),1);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Charger le spinner Sociabilité
    private void LoadSpinnerSociabilite() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SociabiliteDao Sociabilite_data = daoSession.getSociabiliteDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherSociabilite)); //Text Add a new structure sociale
        //Charger les éléments de la liste
        List<Sociabilite> List_Sociabilite = Sociabilite_data.loadAll();
        if (List_Sociabilite != null) {
            for (Sociabilite S : List_Sociabilite) {
                adapter.add(S.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextSociabilite)); //This is the text that will be displayed as hint.
        Spinner_Sociabilite.setAdapter(adapter);
        Spinner_Sociabilite.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Sociabilite.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidateSociabilite),getString(R.string.TextInputSociabilite),2);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Charger le spinner Entreprise
    private void LoadSpinnerEntreprise() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAddCompany));
        //Charger les éléments de la liste
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    adapter.add(e.getRaison_sociale());
                }
            }
        }

        adapter.add(getString(R.string.TextSpinner)); //This is the text that will be displayed as hint.
        Spinner_Entreprise.setAdapter(adapter);
        Spinner_Entreprise.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Entreprise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouvelle entreprise
                Show_Dialog_New_Company(getString(R.string.TextAddCompany),getString(R.string.TextNewOrganization));
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateContact.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code Categorie Contact
                        AddCategorieContact(NewValueList);
                        break;
                    case 2 : //Code Sociabilité
                        AddSociabilite(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void AddCategorieContact(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
        Categorie_Contact categorie_input = new Categorie_Contact();
        categorie_input.setNom(Nom);
        categorie_data.insertOrReplace(categorie_input);
        LoadSpinnerCategorie();
    }

    private void AddSociabilite(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();
        Sociabilite sociabilite_input = new Sociabilite();
        sociabilite_input.setNom(Nom);
        sociabilite_data.insertOrReplace(sociabilite_input);
        LoadSpinnerSociabilite();
    }




    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {

                String email1 = Mail1.getText().toString().trim();
                String number1 = PhoneNumber1.getText().toString().trim();
                String number2 = PhoneNumber2.getText().toString().trim();


                String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                String MobilePattern="";
                String CP_Pattern = "";
                //Pattern selon Pays de selection :
                sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
                String Format_Pays = sharedpreferences.getString("FormatCode","FR");
                switch (Format_Pays){
                    case "FR" : {
                        MobilePattern = "\"(0|\\\\+33|0033)[1-9][0-9]{8}\"";
                        CP_Pattern ="^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$";
                        break;}
                    case "US" :{
                        MobilePattern = "^[0-9]{3,3}[-]{1,1}[0-9]{3,3}[-]{1,1}[0-9]{4,4}$";
                        CP_Pattern = "^[A-Z]{1,2}([0-9]{1,2}|[0-9]{1,1}[A-Z]{1,1})( |)[0-9]{1,1}[A-Z]{2,2}$";
                        break;}
                }

                MobilePattern = "[0-9]{10}"; //Ancien a supprimer si les tests sont bons


                if (email1.matches(emailPattern)){
                    if (number1.matches(MobilePattern) & (TextUtils.isEmpty(PhoneNumber2.getText().toString())) || (number1.matches(MobilePattern) & (number2.matches(MobilePattern)) )) {

                        // Validation du Sexe
                        String Text_Sex;
                        if (Sex.getCheckedRadioButtonId() == R.id.Cradiobtn_male) {
                            Text_Sex = getString(R.string.Radiobtn1);
                        } else {
                            Text_Sex = getString(R.string.Radiobtn2);
                        }

                        Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm), getString(R.string.textConfirm) + "\n" +
                                "\n" + getString(R.string.raison) + " : " + Spinner_Entreprise.getSelectedItem().toString() + "\n" +
                                "\n" + getString(R.string.TextLastName) + " : " + Last_Name.getText().toString() +
                                "\n" + getString(R.string.TextFirstName) + " : " + First_Name.getText().toString() + "\n" +
                                "\n" + getString(R.string.TextValidateCategorie) + " : " + Spinner_Categorie.getSelectedItem().toString() +
                                "\n" + getString(R.string.TextValidateSociabilite) + " : " + Spinner_Sociabilite.getSelectedItem().toString() + "\n" +
                                "\n" + getString(R.string.datenaissance) + " : " + TextDate.getText().toString() +
                                "\n" + getString(R.string.TextSex) + " : " + Text_Sex + "\n" +
                                "\n" + getString(R.string.TextAddress) + " : " + StreetAddress1.getText().toString() +
                                "\n" + getString(R.string.TextAddress2) + " : " + StreetAddress2.getText().toString() +
                                "\n" + getString(R.string.TextCity) + " : " + City.getText().toString() +
                                "\n" + getString(R.string.TextState) + " : " + State.getText().toString() +
                                "\n" + getString(R.string.TextPostalCode) + " : " + PostalCode.getText().toString() +
                                "\n" + getString(R.string.TextCountry) + " : " + Country.getText().toString() + "\n" +
                                "\n" + getString(R.string.TextPhoneNumber1) + " : " + PhoneNumber1.getText().toString() +
                                "\n" + getString(R.string.TextPhoneNumber2) + " : " + PhoneNumber2.getText().toString() + "\n" +
                                "\n" + getString(R.string.TextSection4) + " : " + Mail1.getText().toString() + "\n" +
                                "\n" + getString(R.string.rmk) + " : " + Remarques.getText().toString());


                    } else {
                        Show_Dialog(getString(R.string.warntitle), getString(R.string.warnphone));
                    }

                } else {
                    Show_Dialog(getString(R.string.warntitle), getString(R.string.warnemail));
                }


            } else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();
            }

        }
    };


    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewContact);
        int position = 0 ;

        if (TextUtils.isEmpty(Mail1.getText().toString())) {
            position = Mail1.getTop();
        }
        if (TextUtils.isEmpty(PhoneNumber1.getText().toString())) {
            position = PhoneNumber1.getTop();
        }
        if (TextUtils.isEmpty(Country.getText().toString())) {
            position = Country.getTop();
        }
        if (TextUtils.isEmpty(PostalCode.getText().toString())) {
            position = PostalCode.getTop();
        }
        if (TextUtils.isEmpty(City.getText().toString())) {
            position = City.getTop();
        }
        if (TextUtils.isEmpty(StreetAddress1.getText().toString())) {
            position = StreetAddress1.getTop();
        }
        if (Spinner_Sociabilite.getSelectedItem().toString().equals(getString(R.string.TextSociabilite))) {
            position = Spinner_Sociabilite.getTop();
        }
        if (Spinner_Categorie.getSelectedItem().toString().equals(getString(R.string.TextCategorie))) {
            position =Spinner_Categorie.getTop();
        }
        if (TextUtils.isEmpty(First_Name.getText().toString())) {
            position = First_Name.getTop();
        }
        if (TextUtils.isEmpty(Last_Name.getText().toString())) {
            position = Last_Name.getTop();
        }
        if (Spinner_Entreprise.getSelectedItem().toString().equals(getString(R.string.TextSpinner))) {
            position = Spinner_Entreprise.getTop();
        }

        final int Position_Y = position;
        final int Position_X = 0;
        sc.post(new Runnable() {
            public void run() {
                sc.scrollTo(Position_X, Position_Y); // these are your x and y coordinates
            }
        });
    }

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateContact.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }



    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };

    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateContact.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                CreateContactIntoDataBase();
                Intent intent = new Intent(CreateContact.this, ContactMenu.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Display Dialog Confirm Data
    private void Show_Dialog_New_Company(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateContact.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                Intent intent = new Intent(CreateContact.this, CreateOrganization.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(CreateContact.this, ContactMenu.class);
            finish();
            startActivity(intent);

        }
        return true;
    }

    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateContact.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(CreateContact.this, ContactMenu.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                Intent intent = new Intent(CreateContact.this, ContactMenu.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    /**
     * Verifie si les champs sont renseignées
     *
     * @return Retourne un booleen
     */
    private boolean CheckInput() {
        boolean b = true;

        if (TextUtils.isEmpty(Last_Name.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(First_Name.getText().toString())) {
            b = false;
        }
        if (TextDate.getText().toString().equals(getString(R.string.DefaultBirthday))){
            b = false;
        }
        if (TextUtils.isEmpty(StreetAddress1.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(City.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(PostalCode.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Country.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(PhoneNumber1.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Mail1.getText().toString())) {
            b = false;
        }

        if (Spinner_Entreprise.getSelectedItem().toString().equals(getString(R.string.TextSpinner))) {
            b = false;
        }
        if (Spinner_Categorie.getSelectedItem().toString().equals(getString(R.string.TextCategorie))) {
            b = false;
        }
        if (Spinner_Sociabilite.getSelectedItem().toString().equals(getString(R.string.TextSociabilite))) {
            b = false;
        }
        return b;
    }


    private void CreateContactIntoDataBase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        ContactDao contact_data = daoSession.getContactDao();
        Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

        IdentiteDao identite_data = daoSession.getIdentiteDao();
        SexeDao sexe_data = daoSession.getSexeDao();
        AdresseDao adresse_data = daoSession.getAdresseDao();
        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
        RegionDao region_data = daoSession.getRegionDao();
        PaysDao pays_data = daoSession.getPaysDao();


        //Table Contact
        com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
        contact_input.setRemarque_contact(Remarques.getText().toString().trim());
        contact_input.setArchive(0);
        contact_input.setDate_creation(Today_Date());
        contact_input.setDate_modification(Today_Date());
        contact_input.setID_Agronome_Creation(1);
        contact_input.setID_Agronome_Modification(1);

        //Ajout de la table Categorie
        List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                .where(Categorie_ContactDao.Properties.Nom.eq(Spinner_Categorie.getSelectedItem().toString()))
                .list();
        if (List_Categorie != null) {
            for (Categorie_Contact CC : List_Categorie) {
                contact_input.setCategorie_Contact(CC);
                contact_input.setID_Categorie_Contact(CC.getId());
            }
        }

        //Ajout de la table Sociabilite
        List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                .where(SociabiliteDao.Properties.Nom.eq(Spinner_Sociabilite.getSelectedItem().toString()))
                .list();
        if (List_Sociabilite != null) {
            for (Sociabilite S : List_Sociabilite) {
                contact_input.setSociabilite(S);
                contact_input.setID_Sociabilite(S.getId());
            }
        }


        //Table identité
        Identite identite_input = new Identite();
        identite_input.setNom(Last_Name.getText().toString().trim()); //Nom
        identite_input.setPrenom(First_Name.getText().toString().trim()); //Prenom
        identite_input.setPhoto("");
        identite_input.setDate_naissance(Birth_Date); //Date_de_naissance
        identite_input.setTelephone_principal(PhoneNumber1.getText().toString().trim()); //Telephone principale
        identite_input.setTelephone_secondaire(PhoneNumber2.getText().toString().trim()); //Telephoe secondaire
        identite_input.setAdresse_email(Mail1.getText().toString().trim()); //Mail 1
        identite_input.setArchive(0);
        identite_input.setDate_creation(Today_Date());
        identite_input.setDate_modification(Today_Date());
        identite_input.setID_Agronome_Creation(1);
        identite_input.setID_Agronome_Modification(1);

        Sexe sexe_input = new Sexe();
        if (Sex.getCheckedRadioButtonId() == R.id.Cradiobtn_male) {
            sexe_input.setNom(getString(R.string.Radiobtn1));
        } else {
            sexe_input.setNom(getString(R.string.Radiobtn2));
        }
        sexe_data.insertOrReplace(sexe_input);


        //Gestion de la photo
        if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
            //Photo basse qualité
            File myDir_Source1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
            File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");

            File myDir_Dest1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
            File photoFile_Dest1 = new File(myDir_Dest1, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
            try {
                CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Photo haute qualité
            File myDir_Source2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
            File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");

            File myDir_Dest2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
            File photoFile_Dest2 = new File(myDir_Dest2, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture_HD.jpg");
            try {
                CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
            } catch (IOException e) {
                e.printStackTrace();
            }

            identite_input.setPhoto(Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
        }
        else{

            identite_input.setPhoto("");
        }

        //Table Adresse
        Adresse adresse_input = new Adresse();
        adresse_input.setAdresse_numero_rue(StreetAddress1.getText().toString().trim()); //Adresse
        adresse_input.setComplement_adresse(StreetAddress2.getText().toString().trim()); //Complement Adresse
        adresse_input.setArchive(0);
        adresse_input.setDate_creation(Today_Date());
        adresse_input.setDate_modification(Today_Date());
        adresse_input.setID_Agronome_Creation(1);
        adresse_input.setID_Agronome_Modification(1);


        Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
        code_postal_ville_input.setCode_postal(PostalCode.getText().toString().trim());
        code_postal_ville_input.setVille(City.getText().toString().trim());
        code_postal_ville_data.insertOrReplace(code_postal_ville_input);

        Region region_input = new Region();
        region_input.setNom(State.getText().toString().trim());
        region_data.insertOrReplace(region_input);

        Pays pays_input = new Pays();
        pays_input.setNom(Country.getText().toString().trim());
        pays_data.insertOrReplace(pays_input);

        //Cardinalités entre les tables :
        adresse_input.setCode_Postal_Ville(code_postal_ville_input);
        adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
        adresse_input.setRegion(region_input);
        adresse_input.setID_Region(region_input.getId());
        adresse_input.setPays(pays_input);
        adresse_input.setID_Pays(pays_input.getId());
        adresse_data.insertOrReplace(adresse_input);

        identite_input.setAdresse(adresse_input);
        identite_input.setID_Adresse(adresse_input.getId());

        identite_input.setSexe(sexe_input);
        identite_input.setID_Sexe(sexe_input.getId());

        identite_data.insertOrReplace(identite_input);
        //Un contact possède une identité
        contact_input.setIdentite(identite_input);
        contact_input.setID_Identite_Contact(identite_input.getId());

        //Ajout du contact à l'entreprise concernée
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                for (Entreprise e : List_Company) {
                    if (e.getRaison_sociale().equals(Spinner_Entreprise.getSelectedItem().toString())) {
                        contact_input.setID_Entreprise(e.getId());
                        contact_data.insertOrReplace(contact_input);
                        e.update();
                    }
                }
                c.update();
            }
        }

    }
}