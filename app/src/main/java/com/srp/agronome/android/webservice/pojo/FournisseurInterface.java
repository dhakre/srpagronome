package com.srp.agronome.android.webservice.pojo;


import com.squareup.okhttp.ResponseBody;
import com.srp.agronome.android.webservice.pojo.MultipleResource;
import com.srp.agronome.android.webservice.pojo.User;
import com.srp.agronome.android.webservice.pojo.UserList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.Headers;

import java.util.List;

import retrofit2.http.Path;

/**
 * Created by soytouchrp on 04/12/2017.
 */

public interface FournisseurInterface {


    @GET("fournisseurs")
    Call<DataUser> doGetListResources();

    @POST("fournisseurs")
    Call<DataUser> createUser(@Body DataUser user);

    @GET("fournisseurs/{id}")
//    Call<User> doGetUserById(@Path("id") int id);
    Call<DataUser> doGetUserById(@Path("id") int id);

    @PUT("fournisseurs/{id}")
//    Call<User> doUpdateUser(@Path("id") int id, @Body User user);
    Call<DataUser> doUpdateUser(@Path("id") int id, @Body DataUser dataUser);

    @DELETE("fournisseurs/{id}")
//    Call<User> doUpdateUser(@Path("id") int id, @Body User user);
    Call<DataUser> doDeleteUser(@Path("id") int id);

    @FormUrlEncoded
    @PUT("fournisseurs/{id}")
    Call doCreateUserWithField(@Field("message") String name, @Field("status_code") Integer idAdresse);

//    @Headers("Cache-Control: max-age=640000")
//    @GET("/users/{user}/repos")
//    List<Repo> listRepos(@Path("user") String user);
}