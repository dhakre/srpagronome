package com.srp.agronome.android;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.LoginFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;


import com.srp.agronome.android.db.*;
import com.srp.agronome.android.db.Contact;
import com.srp.agronome.android.webservice.Service;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.R.attr.borderlessButtonStyle;
import static android.R.attr.data;
import static android.R.attr.stackFromBottom;
import static com.srp.agronome.android.CreateContact.Today_Date;
import static com.srp.agronome.android.CreateOrganization.Date_Arret;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.CreateQuickContact.AddCONTACTID;
import static com.srp.agronome.android.CreateQuickContact.AddENTERPRISEID;
import static com.srp.agronome.android.MenuAgriculteur.DELETE_CONTACT_ID;
import static com.srp.agronome.android.MenuAgriculteur.DELETE_enterprise_ID;
import static com.srp.agronome.android.EditContact.EDIT_CONTACT_ID;
import static com.srp.agronome.android.EditOrganization.EDIT_ENTERPRISE_ID;

/**
 * Created by doucoure on 13/06/2017.
 */

import static com.srp.agronome.android.MainLogin.ID_compte_server;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.EXECUTE_ONES;
public class ContactMenu extends AppCompatActivity {

    private Button Recherche = null;
    private Button NomPhoto = null;
    private Button Carte = null;
    private Button Ajout = null;
    static int count=1;
    private static String delete_ID_Identificant;
    //sync button to syncronize the db Local and DB server
    private ImageButton syncButton=null;

    private ImageButton imgBtnHome = null;

    static boolean  codeRun=true;  //c
    static boolean  codeDelete=true;
    static boolean  codeUpdate=true;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setContentView(R.layout.activity_menu_annuaire);

        Recherche = (Button) findViewById(R.id.btnannuaire1);
        Recherche.setOnClickListener(ButtonRechercheHandler);

        NomPhoto = (Button) findViewById(R.id.btnannuaire2);
        NomPhoto.setOnClickListener(ButtonPhotoHandler);

        Carte = (Button) findViewById(R.id.btnannuaire3);
        Carte.setOnClickListener(ButtonCarteHandler);

        Ajout = (Button) findViewById(R.id.btn_ajoutContact);
        Ajout.setOnClickListener(ButtonNouveaueHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menu);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

        syncButton= (ImageButton) findViewById(R.id.sync);
        syncButton.setOnClickListener(ButtonsyncHandler);


        AddDataForAccounts ad=new AddDataForAccounts();
        if(ID_compte_server==1) {
            //ad.addDataJustine();
        }
        if (ID_compte_server==2){
            //ad.addDataMathu();
        }

    } //onCreate

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(ContactMenu.this, MainMenu.class);
            finish();
            startActivity(intent);

        }
        return true;
    }

    View.OnClickListener ButtonNouveaueHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ContactMenu.this, CreateQuickContact.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener ButtonRechercheHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(),"Prochainement disponible",Toast.LENGTH_SHORT).show();
        }
    };

    View.OnClickListener ButtonPhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ContactMenu.this, ListContact.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener ButtonCarteHandler = new View.OnClickListener() {
        public void onClick(View v) {
           // Toast.makeText(getApplicationContext(), "En developpement", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ContactMenu.this, MapsActivity.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ContactMenu.this, MainMenu.class);
            finish();
            startActivity(intent);
        }
    };

   /* View.OnClickListener sync= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), " data sync with server", Toast.LENGTH_SHORT).show();
        }
    };*/
   View.OnClickListener ButtonsyncHandler= new View.OnClickListener() {
       @Override
       public void onClick(View v) {


           if(codeRun==true)
           {
               putData pd = new putData();
               pd.execute();
               Toast.makeText(getApplicationContext(), R.string.syncMessage, Toast.LENGTH_SHORT).show();
           }else
           {
               Toast.makeText(getApplicationContext(), R.string.dataAdd, Toast.LENGTH_SHORT).show();
           }

           //d
           if(codeDelete==true)
           {
               Sync1 delete = new Sync1();
               delete.execute();
               Toast.makeText(getApplicationContext(), R.string.syncMessage, Toast.LENGTH_SHORT).show();
           }else
           {
               Toast.makeText(getApplicationContext(), R.string.dataAdd, Toast.LENGTH_SHORT).show();
           }
           //u
           if(codeUpdate==true)
           {
               Sync2  up = new Sync2();
               up.execute();
               Toast.makeText(getApplicationContext(), R.string.syncMessage, Toast.LENGTH_SHORT).show();
           }else
           {
               Toast.makeText(getApplicationContext(), R.string.dataAdd, Toast.LENGTH_SHORT).show();
           }

       }
   };




   //put data to server
   public class putData extends AsyncTask<String, String, String> {

//        @Override
       protected void onPreExecute() {

       }

       @Override
       protected String doInBackground(String... args) {

           HttpURLConnection urlConnection;
           OutputStream out;
           URL url;

           String result;
           int code=0;
           //send id
           Log.i("postData", "doInBackground: new e id"+AddENTERPRISEID);
           Log.i("postData", "doInBackground: new c id"+AddCONTACTID);

           try {

               //create json data to send
               Log.i("postData", "doInBackground: start");
               DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
               CompteDao compte_dao = daoSession.getCompteDao();
               List<Compte> update_compte = compte_dao.queryBuilder()
                       .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                       .list();

               if (update_compte != null) {
                   for (Compte c : update_compte) {
                       List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                       for (Entreprise e : List_Entreprise) {
                           if (e.getId() == AddENTERPRISEID)
                           {
                               Log.i("postData", "doInBackground:Enterprise added "+e.getId());
                               List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                           for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                               if ((ct.getId() == AddCONTACTID )&&(ct.getContact_identifiant()==null)) {

                                   Log.i("postData", "contact added=" + ct.getId());
                                   Log.i("postData", "contact identificant=" + ct.getContact_identifiant());
                                   JSONObject datapost = new JSONObject(); //json data to be send
                                   datapost.put("raison_sociale", e.getRaison_sociale());
                                   datapost.put("archive", ct.getArchive());
                                   datapost.put("date_creation", ct.getDate_modification());
                                   datapost.put("date_modification", ct.getDate_creation());
                                   datapost.put("structure_sociale", e.getStructure_Sociale());
                                   // datapost.put("situation", e.getSituation().getNom());
                                  /* if(e.getStatut_Soytouch().getNom()==null)
                                   {
                                       datapost.put("statut_soytouch"," ");
                                   }else
                                   {
                                       datapost.put("statut_soytouch", e.getStatut_Soytouch().getNom());
                                   }

                                   if((e.getActivite_Principale().getNom()==null)||(e.getActivite_Secondaire().getNom()==null))
                                   {
                                       datapost.put("activite_principale", " ");
                                       datapost.put("activite_secondaire", " ");
                                   }
                                   else{
                                       datapost.put("activite_principale", e.getActivite_Principale().getNom());
                                       datapost.put("activite_secondaire", e.getActivite_Secondaire().getNom());
                                   }*/

                                  // datapost.put("biologique", e.getBiologique().getNom());
                                  // datapost.put("type_bio", e.getType_BioList());
                                   datapost.put("numero_nom_rue", e.getAdresse_Siege().getAdresse_numero_rue());
                                   datapost.put("code_postale", e.getAdresse_Siege().getCode_Postal_Ville().getCode_postal());
                                   datapost.put("ville", e.getAdresse_Siege().getCode_Postal_Ville().getVille());
                                   datapost.put("pays", e.getAdresse_Siege().getPays().getNom());
                                   datapost.put("telephone_entreprise", e.getTelephone_entreprise());
                                   //datapost.put("fax", e.getFax_entreprise());
                                   datapost.put("adresse_mail_principale", e.getAdresse_email_principale());
                                   datapost.put("adresse_mail_secondaire", e.getAdresse_email_secondaire());
                                  // String ct_id = ct.getContact_identifiant();
                                   datapost.put("nom", ct.getIdentite().getNom());
                                   datapost.put("prenom", ct.getIdentite().getPrenom());
                                   datapost.put("adresse_mail", ct.getIdentite().getAdresse_email());
                                   //datapost.put("sexe", ct.getIdentite().getSexe().getNom());
                                   datapost.put("telephone_principal", ct.getIdentite().getTelephone_principal());
                                   datapost.put("telephone_secondaire", ct.getIdentite().getTelephone_secondaire());
                                   datapost.put("id_agronome_creation",ct.getID_Agronome_Creation());
                                   datapost.put("date_naissance",ct.getIdentite().getDate_naissance());

                                   // datapost.put("categorie", ct.getCategorie_Contact().getNom());
                                   // datapost.put("sociabilite", ct.getSociabilite().getNom());
                                   datapost.put("remarque", ct.getRemarque_contact());
                                   //manage users
                                   if((ID_compte_server!=4)&&(ID_compte_server!=3))
                                   {
                                       datapost.put("id_compte", ID_compte_server);
                                   }

                                   Log.i("postData", "doInBackground: datapost=" + datapost.toString());

                                   if (ct.getContact_identifiant() == null) {

                                       url = new URL(Service.ENDPOINT);
                                       Log.i("postData", "doInBackground: inside condition");

                                       urlConnection = (HttpURLConnection) url.openConnection();
                                       // httpCon.setDoOutput(true);
                                       //httpCon.setDoInput(true);
                                       //httpCon.setUseCaches(false);
                                       urlConnection.setRequestProperty("Content-Type", "application/json");
                                       //httpCon.setRequestProperty("Accept", "application/json");
                                       urlConnection.setRequestMethod("POST");
                                       out = new BufferedOutputStream(urlConnection.getOutputStream());
                                       //OutputStreamWriter out = new OutputStreamWriter(os, "UTF-8");
                                       BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                                       writer.write(datapost.toString());
                                       writer.flush();
                                       writer.close();
                                       Log.i("postData","json="+ datapost.toString());

                                       out.close();
                                       urlConnection.connect();
                                       code = urlConnection.getResponseCode();
                                       Log.i("postData", "doInBackground: datasend code= " + code);


                                   }
                                   else{
                                       //put
                                   }

                               }
                           }
                       }//if enterprise
                   }
                   }
               }

           } catch (Exception e) {
               e.printStackTrace();
           }
           //return <code></code>
           result=String.valueOf(code);
           return result;

       }

       @Override
       protected void onPostExecute(String result) {
           Log.i("App", "debut post");
           if(result.equals("201"))
           {
               codeRun=false;
           }

       }


       public boolean comparedate(Date date1, Date date2) {
           boolean b = true;
           if (date1.compareTo(date2) == 0) {
               b = false;
           }
           return b;
       }


   }

   //sync delete and update data
   //put data to server
    //update
   public class Sync1 extends AsyncTask<String, String, String> {

       @Override
       protected void onPreExecute() {

       }

       @Override
       protected String doInBackground(String... args) {

           HttpURLConnection urlConnection = null;
           URL url;

           String result;
           int code=0;
           //send id
           Log.i("delete", "doInBackground: delete e id"+DELETE_enterprise_ID);
           Log.i("delete", "doInBackground: delete c id"+DELETE_CONTACT_ID);

           try {

               //create json data to send
               Log.i("delete", "doInBackground: start");
               DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
               CompteDao compte_dao = daoSession.getCompteDao();
               List<Compte> update_compte = compte_dao.queryBuilder()
                       .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                       .list();

               if (update_compte != null) {
                   for (Compte c : update_compte) {
                       List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                       for (Entreprise e : List_Entreprise) {
                           if (e.getArchive() == 1)
                           {
                               Log.i("delete", "doInBackground:Enterprise deleted "+e.getId());
                               List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                               for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                   if ((ct.getId() == DELETE_CONTACT_ID)||(ct.getArchive()==1) ) {

                                       delete_ID_Identificant=ct.getContact_identifiant();  ///id at server to delete


                                       url = new URL(Service.ENDPOINT + delete_ID_Identificant);
                                       urlConnection = (HttpURLConnection) url.openConnection();
                                       Log.i("delete", "doInBackground: inside condition");
                                       urlConnection.setDoOutput(true);
                                       urlConnection.setRequestProperty("Content-Type", "application/json");
                                       urlConnection.setRequestMethod("DELETE");
                                       urlConnection.connect();

                                       code = urlConnection.getResponseCode();
                                       Log.i("delete", "doInBackground: data delete code= " + code);

                                       Log.i("delete", "doInBackground:Contact deleted "+ct.getId());

                                       }

                                   }
                               }
                               else{

                              // Log.i("delete", "doInBackground:Enterprise deleted "+e.getId());
                               List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                               for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                   if ((ct.getId() == DELETE_CONTACT_ID)&& (ct.getArchive()==1)) {

                                       url = new URL(Service.ENDPOINT + ct.getContact_identifiant());
                                       urlConnection = (HttpURLConnection) url.openConnection();
                                       Log.i("delete", "doInBackground: inside condition");
                                       urlConnection.setDoOutput(true);
                                       urlConnection.setRequestProperty("Content-Type", "application/json");
                                       urlConnection.setRequestMethod("DELETE");
                                       urlConnection.connect();

                                       code = urlConnection.getResponseCode();
                                       Log.i("delete", "doInBackground: data delete code= " + code);

                                       Log.i("delete", "doInBackground:Contact deleted "+ct.getId());

                                   }

                               }

                           }
                           }
                       }
                   }


           } catch (Exception e) {
               e.printStackTrace();
           }
           //return <code></code>
           result=String.valueOf(code);
           return result;

       }

       @Override
       protected void onPostExecute(String result) {
           Log.i("App", "debut post");
           if(result.equals("200"))   //status ok
           {
               codeDelete=false;
           }

       }
   }

   //put
   public class Sync2 extends AsyncTask<String, String, String> {

       @Override
       protected void onPreExecute() {

       }

       @Override
       protected String doInBackground(String... params) {

           HttpURLConnection urlConnection = null;
           URL url;
           OutputStream out;

           String result;
           int code = 0;
           //send id
           Log.i("put", "doInBackground: update e id" + EDIT_ENTERPRISE_ID);
           Log.i("put", "doInBackground: update c id" + EDIT_CONTACT_ID);

           try {

               //create json data to send
               Log.i("put", "doInBackground: start");
               DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
               CompteDao compte_dao = daoSession.getCompteDao();
               List<Compte> update_compte = compte_dao.queryBuilder()
                       .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                       .list();

               if (update_compte != null) {
                   for (Compte c : update_compte) {
                       List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                       for (Entreprise e : List_Entreprise) {
                           if (e.getId() == EDIT_ENTERPRISE_ID) {
                               Log.i("put", "doInBackground:Enterprise added " + e.getId());
                               List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                               for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                   if ((ct.getId() == EDIT_CONTACT_ID )) {

                                       Log.i("put", "contact updated=" + ct.getId());
                                       Log.i("put", "contact identificant=" + ct.getContact_identifiant());
                                       JSONObject datapost = new JSONObject(); //json data to be send
                                       datapost.put("raison_sociale", e.getRaison_sociale());
                                       datapost.put("archive", ct.getArchive());
                                       datapost.put("date_creation", ct.getDate_modification());
                                       datapost.put("date_modification", ct.getDate_creation());
                                       datapost.put("structure_sociale", e.getStructure_Sociale());
                                       // datapost.put("situation", e.getSituation().getNom());
                                        // datapost.put("statut_soytouch", e.getStatut_Soytouch().getNom());
                                       // datapost.put("activite_principale", e.getActivite_Principale().getNom());
                                       //datapost.put("activite_secondaire", e.getActivite_Secondaire().getNom());
                                       //datapost.put("biologique", e.getBiologique().getNom());
                                       //datapost.put("type_bio", e.getType_BioList());
                                       datapost.put("numero_nom_rue", e.getAdresse_Siege().getAdresse_numero_rue());
                                       datapost.put("code_postale", e.getAdresse_Siege().getCode_Postal_Ville().getCode_postal());
                                       datapost.put("ville", e.getAdresse_Siege().getCode_Postal_Ville().getVille());
                                       datapost.put("pays", e.getAdresse_Siege().getPays().getNom());
                                       datapost.put("telephone_entreprise", e.getTelephone_entreprise());
                                       //datapost.put("fax", e.getFax_entreprise());
                                       datapost.put("adresse_mail_principale", e.getAdresse_email_principale());
                                       datapost.put("adresse_mail_secondaire", e.getAdresse_email_secondaire());
                                      // String ct_id = ct.getContact_identifiant();
                                       datapost.put("nom", ct.getIdentite().getNom());
                                       datapost.put("prenom", ct.getIdentite().getPrenom());
                                       datapost.put("adresse_mail", ct.getIdentite().getAdresse_email());
                                       datapost.put("sexe", ct.getIdentite().getSexe().getNom());
                                       datapost.put("telephone_principal", ct.getIdentite().getTelephone_principal());
                                       datapost.put("telephone_secondaire", ct.getIdentite().getTelephone_secondaire());
                                       datapost.put("id_agronome_creation",ct.getID_Agronome_Creation());
                                       datapost.put("date_naissance",ct.getIdentite().getDate_naissance());
                                       // datapost.put("categorie", ct.getCategorie_Contact().getNom());
                                       // datapost.put("sociabilite", ct.getSociabilite().getNom());
                                       //datapost.put("remarque", ct.getRemarque_contact());
                                       //manage users
                                       if ((ID_compte_server != 4) && (ID_compte_server != 3))
                                       {
                                           datapost.put("id_compte", ID_compte_server);
                                       }
                                       Log.i("put", "doInBackground: datapost=" + datapost.toString());


                                       if (ct.getContact_identifiant() != null) {

                                           url = new URL(Service.ENDPOINT + ct.getContact_identifiant());
                                           Log.i("put", "doInBackground: inside condition" + ct.getContact_identifiant());

                                           urlConnection = (HttpURLConnection) url.openConnection();
                                           // httpCon.setDoOutput(true);
                                           //httpCon.setDoInput(true);
                                           //httpCon.setUseCaches(false);
                                           urlConnection.setRequestProperty("Content-Type", "application/json");
                                           //httpCon.setRequestProperty("Accept", "application/json");
                                           urlConnection.setRequestMethod("PUT");
                                           out = new BufferedOutputStream(urlConnection.getOutputStream());
                                           //OutputStreamWriter out = new OutputStreamWriter(os, "UTF-8");
                                           BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                                           writer.write(datapost.toString());
                                           writer.flush();
                                           writer.close();
                                           Log.i("put","put str="+ datapost.toString());

                                           out.close();
                                           urlConnection.connect();
                                           code = urlConnection.getResponseCode();
                                           Log.i("put", "doInBackground: datasend code= " + code);


                                       }

                                   }
                               }
                           }
                           else
                               {
                                   Log.i("update", "doInBackground: edit only contact");
                                   List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                                   for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                       if (ct.getId() == EDIT_CONTACT_ID) {

                                           Log.i("postData", "contact added=" + ct.getId());
                                           Log.i("postData", "contact identificant=" + ct.getContact_identifiant());
                                           JSONObject datapost = new JSONObject(); //json data to be send
                                           datapost.put("raison_sociale", e.getRaison_sociale());
                                           datapost.put("archive", ct.getArchive());
                                           datapost.put("date_creation", ct.getDate_modification());
                                           datapost.put("date_modification", ct.getDate_creation());
                                           datapost.put("structure_sociale", e.getStructure_Sociale());
                                           // datapost.put("situation", e.getSituation().getNom());
                                           //datapost.put("statut_soytouch", e.getStatut_Soytouch().getNom());
                                           // datapost.put("activite_principale", e.getActivite_Principale().getNom());
                                           //datapost.put("activite_secondaire", e.getActivite_Secondaire().getNom());
                                           //datapost.put("biologique", e.getBiologique().getNom());
                                           //datapost.put("type_bio", e.getType_BioList());
                                           datapost.put("numero_nom_rue", e.getAdresse_Siege().getAdresse_numero_rue());
                                           datapost.put("code_postale", e.getAdresse_Siege().getCode_Postal_Ville().getCode_postal());
                                           datapost.put("ville", e.getAdresse_Siege().getCode_Postal_Ville().getVille());
                                           datapost.put("pays", e.getAdresse_Siege().getPays().getNom());
                                           ///datapost.put("telephone_entreprise", e.getTelephone_entreprise());
                                           //datapost.put("fax", e.getFax_entreprise());
                                           datapost.put("adresse_mail_principale", e.getAdresse_email_principale());
                                           datapost.put("adresse_mail_secondaire", e.getAdresse_email_secondaire());
                                           // String ct_id = ct.getContact_identifiant();
                                           datapost.put("nom", ct.getIdentite().getNom());
                                           datapost.put("prenom", ct.getIdentite().getPrenom());
                                           datapost.put("adresse_mail", ct.getIdentite().getAdresse_email());
                                           datapost.put("sexe", ct.getIdentite().getSexe().getNom());
                                           datapost.put("telephone_principal", ct.getIdentite().getTelephone_principal());
                                           datapost.put("telephone_secondaire", ct.getIdentite().getTelephone_secondaire());
                                           datapost.put("id_agronome_creation",ct.getID_Agronome_Creation());
                                           datapost.put("date_naissance",ct.getIdentite().getDate_naissance());
                                           // datapost.put("categorie", ct.getCategorie_Contact().getNom());
                                           // datapost.put("sociabilite", ct.getSociabilite().getNom());
                                           //datapost.put("remarque", ct.getRemarque_contact());
                                           //manage users
                                           if ((ID_compte_server != 4) && (ID_compte_server != 3))
                                           {
                                               datapost.put("id_compte", ID_compte_server);
                                           }
                                           Log.i("postData", "doInBackground: datapost=" + datapost.toString());


                                           if (ct.getContact_identifiant() != null) {

                                               url = new URL(Service.ENDPOINT + ct.getContact_identifiant());
                                               Log.i("update", "doInBackground: inside condition");

                                               urlConnection = (HttpURLConnection) url.openConnection();
                                               // httpCon.setDoOutput(true);
                                               //httpCon.setDoInput(true);
                                               //httpCon.setUseCaches(false);
                                               urlConnection.setRequestProperty("Content-Type", "application/json");
                                               //httpCon.setRequestProperty("Accept", "application/json");
                                               urlConnection.setRequestMethod("PUT");
                                               out = new BufferedOutputStream(urlConnection.getOutputStream());
                                               //OutputStreamWriter out = new OutputStreamWriter(os, "UTF-8");
                                               BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                                               writer.write(datapost.toString());
                                               writer.flush();
                                               writer.close();
                                               Log.i("update","put str="+ datapost.toString());

                                               out.close();
                                               urlConnection.connect();
                                               code = urlConnection.getResponseCode();
                                               Log.i("update", "doInBackground: datasend code= " + code);

                                           }

                                       }
                                   }

                           }
                       }
                   }
               }
           } catch (Exception e) {
               e.printStackTrace();
           }
           //return
           result = String.valueOf(code);
           return result;
       }

       @Override
       protected void onPostExecute(String result) {
           Log.i("App", "debut update");
           if (result.equals("200"))   //updated data
           {
               codeUpdate = false;
           }

       }
   }


   //add data to db

    public  class AddDataForAccounts
    {
        String name=null;
       
        //add Data for justine
        public void addDataJustine() {

            //for justine
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
             name="CERA";

            DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Log.i("CONTACTMENU", " Reading data from file");
            InputStreamReader in;
            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            String[] csv = new String[1000];
            AssetManager assetManager = getApplicationContext().getAssets();

            int i = 1;
            try {
                in = new InputStreamReader(assetManager.open("justine.csv"));
                br = new BufferedReader(in);
                Log.i("ContactMenu", "addData: Tying to read csv file");
                while ((line = br.readLine()) != null) {
                    csv = line.split(cvsSplitBy, 33);

                    Log.i("ContactMenu", "addData: Reading Data" + csv.toString());

                    if (i != 1) {
                        String arch = "0";
                        String archive = arch;
                        Date date_creation = GlobalValues.Today_Date();
                        Date date_modif = GlobalValues.Today_Date();
                        String nom = csv[18];
                        String prenom = csv[19];
                        String code_postale = csv[10];
                        String raison_sociale = csv[1];
                        String statut_soytouch = csv[4];
                        String telephone_entreprise = csv[13];
                        Date datecreation = date_creation;
                        Date datemodification = date_modif;
                        //empty values
                        String siret = " ";
                        String projet_connu = " ";
                        String fax = " ";
                        String adresse_mail_principale = " ";
                        String adresse_mail_secondaire = " ";
                        String situation = " ";
                        String structure_sociale = " ";
                        String ville = " ";
                        String pays = " ";
                        String region = " ";
                        String type_bio = " ";
                        String sociabilite = " ";
                        String telephone_principal = " ";
                        String telephone_secondaire = " ";
                        String adresse_mail = " ";
                        String activite_principale = " ";
                        String activite_secondaire = " ";
                        String biologique = " ";
                        String cause_arret = " ";
                        String numero_ncomplement_adresseom_rue = " ";
                        String numero_nom_rue = " ";
                        String sexe = " ";
                        String categorie = " ";
                        String remarque = " ";
                        String complement_adresse = " ";

                        //dont add same value
                        name = nom;


                        Log.i("CSV Data", "doInBackground:" + csv[0].toString() + "   " + csv[1].toString() + " " + csv[9] + " " + csv[10] + " " + csv[13] + " " + csv[18] + " " + csv[19]);  // for justine

                        // entreprise

                        Log.i("CREATION", "writing data into DB");
                        Log.i("CSV data", "ADD_DATA: getting data from csv");

                        //check exist
                        if (alreadyExistContact(name))
                        {
                        Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
                        SituationDao situation_data = daoSession.getSituationDao();
                        Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
                        ActiviteDao activite_data = daoSession.getActiviteDao();
                        BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
                        Type_BioDao type_bio_data = daoSession.getType_BioDao();

                        AdresseDao adresse_data = daoSession.getAdresseDao();
                        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
                        PaysDao pays_data = daoSession.getPaysDao();
                        RegionDao region_data = daoSession.getRegionDao();

                        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

                        //Table Entreprise
                        Entreprise entreprise_input = new Entreprise();
                        entreprise_input.setRaison_sociale(raison_sociale);
                        //                        if (nombre_salarie != null) {
                        //                            entreprise_input.setNombre_salarie(Integer.parseInt(nombre_salarie));
                        //                        }

                        entreprise_input.setSIRET(siret);
                        entreprise_input.setProjet_connu(projet_connu);
                        entreprise_input.setTelephone_entreprise(telephone_entreprise);
                        entreprise_input.setFax_entreprise(fax);
                        entreprise_input.setAdresse_email_principale(adresse_mail_principale);
                        entreprise_input.setAdresse_email_secondaire(adresse_mail_secondaire);

                        entreprise_input.setArchive(Integer.parseInt(archive));
                        entreprise_input.setDate_creation(datecreation);
                        entreprise_input.setDate_modification(datemodification);
                        entreprise_input.setID_Agronome_Creation(1);
                        entreprise_input.setID_Agronome_Modification(1);
                        entreprise_input.setID_Compte(ID_Compte_Selected);//set the compte id

                        //Ajout de la table Situation
                        List<Situation> List_Situation = situation_data.queryBuilder()
                                .where(SituationDao.Properties.Nom.eq(situation))
                                .list();
                        if (List_Situation != null) {
                            for (Situation S : List_Situation) {
                                entreprise_input.setSituation(S);
                                entreprise_input.setID_Situation(S.getId());
                            }
                        }

                        //Ajout de la table Structure Sociale
                        List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                                .where(Structure_SocialeDao.Properties.Nom.eq(structure_sociale))
                                .list();
                        if (List_Structure_Sociale != null) {
                            for (Structure_Sociale SS : List_Structure_Sociale) {
                                entreprise_input.setStructure_Sociale(SS);
                                entreprise_input.setID_Structure_Sociale(SS.getId());
                            }
                        }

                        //Ajout de la table Statut SoyTouch
                        List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                                .where(Statut_SoytouchDao.Properties.Nom.eq(statut_soytouch))
                                .list();
                        if (List_Statut != null) {
                            for (Statut_Soytouch Sst : List_Statut) {
                                entreprise_input.setStatut_Soytouch(Sst);
                                entreprise_input.setID_Statut_Soytouch(Sst.getId());
                            }
                        }

                        //Ajout de la table Activite Principale
                        List<Activite> List_Activite_P = activite_data.queryBuilder()
                                .where(ActiviteDao.Properties.Nom.eq(activite_principale))
                                .list();
                        if (List_Activite_P != null) {
                            for (Activite AP : List_Activite_P) {
                                entreprise_input.setActivite_Principale(AP);
                                entreprise_input.setID_Activite_Principale(AP.getId());
                            }
                        }

                        //Ajout de la table Activite Secondaire si renseignée
                        if (!(activite_principale.equals(getString(R.string.TextActivitePrincipale)))) {
                            List<Activite> List_Activite_S = activite_data.queryBuilder()
                                    .where(ActiviteDao.Properties.Nom.eq(activite_secondaire))
                                    .list();
                            if (List_Activite_S != null) {
                                for (Activite AS : List_Activite_S) {
                                    entreprise_input.setActivite_Secondaire(AS);
                                    entreprise_input.setID_Activite_Secondaire(AS.getId());
                                }
                            }
                        }


                        //Ajout de la table Biologique

                        List<Biologique> List_Bio = biologique_data.queryBuilder()
                                .where(BiologiqueDao.Properties.Nom.eq(biologique))
                                .list();
                        if (List_Bio != null) {
                            for (Biologique B : List_Bio) {
                                entreprise_input.setBiologique(B);
                                entreprise_input.setID_Biologique(B.getId());
                            }
                        }


                        entreprise_input.setArret_activite(true);
                        entreprise_input.setCause_arret_activite(cause_arret);
                        entreprise_input.setDate_arret_activite(Date_Arret);


                        //Adresse_siege_social = Adresse de l'entreprise
                        Adresse adresse_siege_social_input = new Adresse();
                        adresse_siege_social_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                        adresse_siege_social_input.setComplement_adresse(complement_adresse); //Complement Adresse
                        adresse_siege_social_input.setArchive(GlobalValues.getInstance().getGlobalArchive());    //Global Value archieve
                        adresse_siege_social_input.setDate_creation(datecreation);
                        adresse_siege_social_input.setDate_modification(datemodification);
                        adresse_siege_social_input.setID_Agronome_Creation(1);
                        adresse_siege_social_input.setID_Agronome_Modification(1);

                        //Code postal adresse siège sociale
                        Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

                        code_postal_ville_siege_input.setCode_postal(code_postale);

                        code_postal_ville_siege_input.setVille(ville);
                        code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

                        //Pays adresse siège
                        Pays pays_siege_input = new Pays();
                        pays_siege_input.setNom(pays);
                        pays_data.insertOrReplace(pays_siege_input);

                        //Région adresse siège
                        Region region_siege_input = new Region();
                        region_siege_input.setNom(region);
                        region_data.insertOrReplace(region_siege_input);

                        adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
                        adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
                        adresse_siege_social_input.setPays(pays_siege_input);
                        adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
                        adresse_siege_social_input.setRegion(region_siege_input);
                        adresse_siege_social_input.setID_Region(region_siege_input.getId());
                        adresse_data.insertOrReplace(adresse_siege_social_input);

                        entreprise_input.setAdresse_Siege(adresse_siege_social_input);
                        entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

                        if (false) {
                            //Adresse_facturation
                            //                        Adresse adresse_facturation_input = new Adresse();
                            //                        adresse_facturation_input.setAdresse_numero_rue(Adresse_facturation_1.getText().toString().trim()); //Adresse
                            //                        adresse_facturation_input.setComplement_adresse(Adresse_facturation_2.getText().toString().trim()); //Complement Adresse
                            //                        adresse_facturation_input.setArchive(1);
                            //                        adresse_facturation_input.setDate_creation(Today_Date());
                            //                        adresse_facturation_input.setDate_modification(Today_Date());
                            //                        adresse_facturation_input.setID_Agronome_Creation(1);
                            //                        adresse_facturation_input.setID_Agronome_Modification(1);
                            //
                            //                        //Code postal adresse facturation
                            //                        Code_Postal_Ville code_postal_ville_facturation_input = new Code_Postal_Ville();
                            //                        code_postal_ville_facturation_input.setCode_postal(CodePostal_facturation.getText().toString().trim());
                            //                        code_postal_ville_facturation_input.setVille(Ville_facturation.getText().toString().trim());
                            //                        code_postal_ville_data.insertOrReplace(code_postal_ville_facturation_input);
                            //
                            //                        //Pays adresse facturation
                            //                        Pays pays_facturation_input = new Pays();
                            //                        pays_facturation_input.setNom(Pays_facturation.getText().toString().trim());
                            //                        pays_data.insertOrReplace(pays_facturation_input);
                            //
                            //                        //Région adresse facturation
                            //                        Region region_facturation_input = new Region();
                            //                        region_facturation_input.setNom(Region_facturation.getText().toString().trim());
                            //                        region_data.insertOrReplace(region_facturation_input);
                            //
                            //                        adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_facturation_input);
                            //                        adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_facturation_input.getId());
                            //                        adresse_facturation_input.setPays(pays_facturation_input);
                            //                        adresse_facturation_input.setID_Pays(pays_facturation_input.getId());
                            //                        adresse_facturation_input.setRegion(region_facturation_input);
                            //                        adresse_facturation_input.setID_Region(region_facturation_input.getId());
                            //                        adresse_data.insertOrReplace(adresse_facturation_input);
                            //
                            //                        entreprise_input.setAdresse_Facturation(adresse_facturation_input);
                            //                        entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
                        } else {
                            entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
                            entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
                        }

                        if (false) {
                            //Adresse livraison
                            //                        Adresse adresse_livraison_input = new Adresse();
                            //                        adresse_livraison_input.setAdresse_numero_rue(Adresse_livraison_1.getText().toString().trim()); //Adresse
                            //                        adresse_livraison_input.setComplement_adresse(Adresse_livraison_2.getText().toString().trim()); //Complement Adresse
                            //                        adresse_livraison_input.setArchive(1);
                            //                        adresse_livraison_input.setDate_creation(Today_Date());
                            //                        adresse_livraison_input.setDate_modification(Today_Date());
                            //                        adresse_livraison_input.setID_Agronome_Creation(1);
                            //                        adresse_livraison_input.setID_Agronome_Modification(1);
                            //
                            //                        //Code postal adresse livraison
                            //                        Code_Postal_Ville code_postal_ville_livraison_input = new Code_Postal_Ville();
                            //                        code_postal_ville_livraison_input.setCode_postal(CodePostal_livraison.getText().toString().trim());
                            //                        code_postal_ville_livraison_input.setVille(Ville_livraison.getText().toString().trim());
                            //                        code_postal_ville_data.insertOrReplace(code_postal_ville_livraison_input);
                            //
                            //                        //Pays adresse livraison
                            //                        Pays pays_livraison_input = new Pays();
                            //                        pays_livraison_input.setNom(Pays_livraison.getText().toString().trim());
                            //                        pays_data.insertOrReplace(pays_livraison_input);
                            //
                            //                        //Région adresse livraison
                            //                        Region region_livraison_input = new Region();
                            //                        region_livraison_input.setNom(Region_livraison.getText().toString().trim());
                            //                        region_data.insertOrReplace(region_livraison_input);
                            //
                            //                        adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_livraison_input);
                            //                        adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_livraison_input.getId());
                            //                        adresse_livraison_input.setPays(pays_livraison_input);
                            //                        adresse_livraison_input.setID_Pays(pays_livraison_input.getId());
                            //                        adresse_livraison_input.setRegion(region_livraison_input);
                            //                        adresse_livraison_input.setID_Region(region_livraison_input.getId());
                            //                        adresse_data.insertOrReplace(adresse_livraison_input);
                            //
                            //                        entreprise_input.setAdresse_Livraison(adresse_livraison_input);
                            //                        entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
                        } else {
                            entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
                            entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
                        }
                        //add data to enterprise table
                        entreprise_data.insertOrReplace(entreprise_input);


                        if ((!(type_bio.equals(getString(R.string.TextTypeBio))))) {

                            List<Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                                    .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                                    .list();
                            if (List_Entreprise_cible != null) {
                                for (Entreprise e : List_Entreprise_cible) {

                                    List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                            .where(Type_BioDao.Properties.Nom.eq(type_bio))
                                            .list();
                                    if (List_Type_Bio != null) {
                                        for (Type_Bio TB : List_Type_Bio) {
                                            TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                            //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                            type_bio_data.insertOrReplace(TB);
                                            e.update();
                                        }
                                    }
                                }
                            }
                        }


                        //Associer la nouvelle entreprise au compte
                        List<Compte> update_compte = compte_dao.queryBuilder()
                                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                .list();
                        if (update_compte != null) {
                            for (Compte c : update_compte) {
                                entreprise_input.setID_Compte(c.getId());
                                entreprise_data.insertOrReplace(entreprise_input);
                                c.update();
                            }
                        }


                        ContactDao contact_data = daoSession.getContactDao();
                        Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
                        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

                        IdentiteDao identite_data = daoSession.getIdentiteDao();
                        SexeDao sexe_data = daoSession.getSexeDao();


                        //Table Contact
                        com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
                        contact_input.setRemarque_contact(remarque);
                        // contact_input.setContact_identifiant(contact_identifiant);
                        contact_input.setArchive(Integer.parseInt(archive));
                        contact_input.setDate_creation(datecreation);
                        contact_input.setDate_modification(datemodification);
                        contact_input.setID_Agronome_Creation(1);
                        contact_input.setID_Agronome_Modification(1);

                        //Ajout de la table Categorie
                        List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
                                .list();
                        if (List_Categorie != null) {
                            for (Categorie_Contact CC : List_Categorie) {
                                contact_input.setCategorie_Contact(CC);
                                contact_input.setID_Categorie_Contact(CC.getId());
                            }
                        }

                        //Ajout de la table Sociabilite
                        List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
                                .list();
                        if (List_Sociabilite != null) {
                            for (Sociabilite S : List_Sociabilite) {
                                contact_input.setSociabilite(S);
                                contact_input.setID_Sociabilite(S.getId());
                            }
                        }


                        //Table identité
                        Identite identite_input = new Identite();
                        identite_input.setNom(nom); //Nom
                        identite_input.setPrenom(prenom); //Prenom
                        identite_input.setPhoto("");
                        identite_input.setTelephone_principal(telephone_principal); //Telephone principale
                        identite_input.setTelephone_secondaire(telephone_secondaire); //Telephoe secondaire
                        identite_input.setAdresse_email(adresse_mail); //Mail 1
                        identite_input.setArchive(Integer.parseInt(archive));
                        identite_input.setDate_creation(datecreation);
                        identite_input.setDate_modification(datemodification);
                        identite_input.setID_Agronome_Creation(1);
                        identite_input.setID_Agronome_Modification(1);

                        Sexe sexe_input = new Sexe();

                        sexe_input.setNom(sexe);

                        sexe_data.insertOrReplace(sexe_input);


                        //Gestion de la photo
                        //                    if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                        //                        //Photo basse qualité
                        //                        File myDir_Source1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                        //                        File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");
                        //
                        //                        File myDir_Dest1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                        //                        File photoFile_Dest1 = new File(myDir_Dest1, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                        //                        try {
                        //                            CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
                        //                        } catch (IOException e) {
                        //                            e.printStackTrace();
                        //                        }
                        //                        //Photo haute qualité
                        //                        File myDir_Source2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                        //                        File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");
                        //
                        //                        File myDir_Dest2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                        //                        File photoFile_Dest2 = new File(myDir_Dest2, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture_HD.jpg");
                        //                        try {
                        //                            CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
                        //                        } catch (IOException e) {
                        //                            e.printStackTrace();
                        //                        }
                        //
                        //                        identite_input.setPhoto(Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                        //                    }
                        //                    else{
                        //
                        //                        identite_input.setPhoto("");
                        //                    }

                        //Table Adresse
                        Adresse adresse_input = new Adresse();
                        adresse_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                        adresse_input.setComplement_adresse(complement_adresse); //Complement Adresse
                        adresse_input.setArchive(Integer.parseInt(archive));
                        adresse_input.setDate_creation(Today_Date());
                        adresse_input.setDate_modification(Today_Date());
                        adresse_input.setID_Agronome_Creation(1);
                        adresse_input.setID_Agronome_Modification(1);


                        Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
                        code_postal_ville_input.setCode_postal(code_postale);
                        code_postal_ville_input.setVille(ville);
                        code_postal_ville_data.insertOrReplace(code_postal_ville_input);

                        Region region_input = new Region();
                        region_input.setNom(region);
                        region_data.insertOrReplace(region_input);

                        Pays pays_input = new Pays();
                        pays_input.setNom(pays);
                        pays_data.insertOrReplace(pays_input);

                        //Cardinalités entre les tables :
                        adresse_input.setCode_Postal_Ville(code_postal_ville_input);
                        adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
                        adresse_input.setRegion(region_input);
                        adresse_input.setID_Region(region_input.getId());
                        adresse_input.setPays(pays_input);
                        adresse_input.setID_Pays(pays_input.getId());
                        adresse_data.insertOrReplace(adresse_input);


                        identite_input.setAdresse(adresse_input);
                        identite_input.setID_Adresse(adresse_input.getId());

                        identite_input.setSexe(sexe_input);
                        identite_input.setID_Sexe(sexe_input.getId());

                        identite_data.insertOrReplace(identite_input);
                        //Un contact possède une identité
                        contact_input.setIdentite(identite_input);
                        contact_input.setID_Identite_Contact(identite_input.getId());

                        //Ajout du contact à l'entreprise concernée
                        List<Compte> update_compte1 = compte_dao.queryBuilder()
                                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                .list();
                        if (update_compte1 != null) {
                            for (Compte c : update_compte1) {
                                List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                                for (Entreprise e : List_Company) {
                                    if (e.getRaison_sociale().equals(raison_sociale)) {
                                        contact_input.setID_Entreprise(e.getId());
                                        contact_data.insertOrReplace(contact_input);
                                        e.update();
                                    }
                                }
                                c.update();
                            }
                        }

                    }
                }
                    i++;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            //increase for the execution

                count++;



        }

        //add data for mathieu
        public void addDataMathu()
        {
            //for justine
            //for justine and mathieu
            Log.i("CONTACTMENU", "addDataMathu: writing data for Mathieu");
           name="CERA";

            //get data from file
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();

            DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            Log.i("file", "doInBackground: Reading file");
            InputStreamReader in;
            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            String[] csv = new String[1000];
            AssetManager assetManager = getApplicationContext().getAssets();

            int i = 1;
            try {
                in=new InputStreamReader(assetManager.open("mathieu.csv"));
                br = new BufferedReader(in);
                Log.i("ContactMenu", "addData: Tying to read csv file");
                while ((line = br.readLine()) != null) {
                    csv = line.split(cvsSplitBy, 33);

                    Log.i("ContactMenu", "addData: Reading Data" + csv.toString());

                    if (i != 1) {
                        String arch = "0";
                        String archive = arch;
                        Date date_creation = GlobalValues.Today_Date();
                        Date date_modif = GlobalValues.Today_Date();

                        String raison_sociale = csv[1];
                        String code_postale = csv[11];
                        String statut_soytouch = csv[5];
                        String telephone_entreprise = csv[14];
                        String email_principal = csv[16];
                        String address_principal = csv[10];
                        String nom = csv[19];
                        String prenom = csv[20];

                        Date datecreation = date_creation;
                        Date datemodification = date_modif;
                        //empty values
                        String siret = " ";
                        String projet_connu = " ";
                        String fax = " ";
                        String adresse_mail_principale = " ";
                        String adresse_mail_secondaire = " ";
                        String situation = " ";
                        String structure_sociale = " ";
                        String ville = " ";
                        String pays = " ";
                        String region = " ";
                        String type_bio = " ";
                        String sociabilite = " ";
                        String telephone_principal = " ";
                        String telephone_secondaire = " ";
                        String adresse_mail = " ";
                        String activite_principale = " ";
                        String activite_secondaire = " ";
                        String biologique = " ";
                        String cause_arret = " ";
                        String numero_ncomplement_adresseom_rue = " ";
                        String numero_nom_rue = " ";
                        String sexe = " ";
                        String categorie = " ";
                        String remarque = " ";
                        String complement_adresse = " ";

                        //check dublicate
                        name=nom;


                        Log.i("CSV Data", "doInBackground:" + csv[0].toString() + "   " + csv[1].toString() + " " + csv[5] + " " + csv[10] + " " + csv[11] + " " + csv[12] + " " + csv[14] + " " + csv[16] + " " + csv[19] + csv[20]);  // for justine

                        // entreprise

                        Log.i("CREATION", "writing data into DB");
                        Log.i("CSV data", "doInBackground: getting data from csv");

                        //check

                        if(alreadyExistContact(name))
                        {
                        Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
                        SituationDao situation_data = daoSession.getSituationDao();
                        Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
                        ActiviteDao activite_data = daoSession.getActiviteDao();
                        BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
                        Type_BioDao type_bio_data = daoSession.getType_BioDao();

                        AdresseDao adresse_data = daoSession.getAdresseDao();
                        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
                        PaysDao pays_data = daoSession.getPaysDao();
                        RegionDao region_data = daoSession.getRegionDao();

                        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

                        //Table Entreprise
                        Entreprise entreprise_input = new Entreprise();
                        entreprise_input.setRaison_sociale(raison_sociale);
                        //                        if (nombre_salarie != null) {
                        //                            entreprise_input.setNombre_salarie(Integer.parseInt(nombre_salarie));
                        //                        }

                        entreprise_input.setSIRET(siret);
                        entreprise_input.setProjet_connu(projet_connu);
                        entreprise_input.setTelephone_entreprise(telephone_entreprise);
                        entreprise_input.setFax_entreprise(fax);
                        entreprise_input.setAdresse_email_principale(adresse_mail_principale);
                        entreprise_input.setAdresse_email_secondaire(adresse_mail_secondaire);

                        entreprise_input.setArchive(Integer.parseInt(archive));
                        entreprise_input.setDate_creation(datecreation);
                        entreprise_input.setDate_modification(datemodification);
                        entreprise_input.setID_Agronome_Creation(1);
                        entreprise_input.setID_Agronome_Modification(1);
                        entreprise_input.setID_Compte(ID_Compte_Selected);//set the compte id

                        //Ajout de la table Situation
                        List<Situation> List_Situation = situation_data.queryBuilder()
                                .where(SituationDao.Properties.Nom.eq(situation))
                                .list();
                        if (List_Situation != null) {
                            for (Situation S : List_Situation) {
                                entreprise_input.setSituation(S);
                                entreprise_input.setID_Situation(S.getId());
                            }
                        }

                        //Ajout de la table Structure Sociale
                        List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                                .where(Structure_SocialeDao.Properties.Nom.eq(structure_sociale))
                                .list();
                        if (List_Structure_Sociale != null) {
                            for (Structure_Sociale SS : List_Structure_Sociale) {
                                entreprise_input.setStructure_Sociale(SS);
                                entreprise_input.setID_Structure_Sociale(SS.getId());
                            }
                        }

                        //Ajout de la table Statut SoyTouch
                        List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                                .where(Statut_SoytouchDao.Properties.Nom.eq(statut_soytouch))
                                .list();
                        if (List_Statut != null) {
                            for (Statut_Soytouch Sst : List_Statut) {
                                entreprise_input.setStatut_Soytouch(Sst);
                                entreprise_input.setID_Statut_Soytouch(Sst.getId());
                            }
                        }

                        //Ajout de la table Activite Principale
                        List<Activite> List_Activite_P = activite_data.queryBuilder()
                                .where(ActiviteDao.Properties.Nom.eq(activite_principale))
                                .list();
                        if (List_Activite_P != null) {
                            for (Activite AP : List_Activite_P) {
                                entreprise_input.setActivite_Principale(AP);
                                entreprise_input.setID_Activite_Principale(AP.getId());
                                entreprise_input.setID_Activite_Principale(AP.getId());
                            }
                        }

                        //Ajout de la table Activite Secondaire si renseignée
                        if (!(activite_principale.equals(getString(R.string.TextActivitePrincipale)))) {
                            List<Activite> List_Activite_S = activite_data.queryBuilder()
                                    .where(ActiviteDao.Properties.Nom.eq(activite_secondaire))
                                    .list();
                            if (List_Activite_S != null) {
                                for (Activite AS : List_Activite_S) {
                                    entreprise_input.setActivite_Secondaire(AS);
                                    entreprise_input.setID_Activite_Secondaire(AS.getId());
                                }
                            }
                        }


                        //Ajout de la table Biologique

                        List<Biologique> List_Bio = biologique_data.queryBuilder()
                                .where(BiologiqueDao.Properties.Nom.eq(biologique))
                                .list();
                        if (List_Bio != null) {
                            for (Biologique B : List_Bio) {
                                entreprise_input.setBiologique(B);
                                entreprise_input.setID_Biologique(B.getId());
                            }
                        }


                        entreprise_input.setArret_activite(true);
                        entreprise_input.setCause_arret_activite(cause_arret);
                        entreprise_input.setDate_arret_activite(Date_Arret);


                        //Adresse_siege_social = Adresse de l'entreprise
                        Adresse adresse_siege_social_input = new Adresse();
                        adresse_siege_social_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                        adresse_siege_social_input.setComplement_adresse(complement_adresse); //Complement Adresse
                        adresse_siege_social_input.setArchive(GlobalValues.getInstance().getGlobalArchive());    //Global Value archieve
                        adresse_siege_social_input.setDate_creation(datecreation);
                        adresse_siege_social_input.setDate_modification(datemodification);
                        adresse_siege_social_input.setID_Agronome_Creation(1);
                        adresse_siege_social_input.setID_Agronome_Modification(1);

                        //Code postal adresse siège sociale
                        Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

                        code_postal_ville_siege_input.setCode_postal(code_postale);

                        code_postal_ville_siege_input.setVille(ville);
                        code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

                        //Pays adresse siège
                        Pays pays_siege_input = new Pays();
                        pays_siege_input.setNom(pays);
                        pays_data.insertOrReplace(pays_siege_input);

                        //Région adresse siège
                        Region region_siege_input = new Region();
                        region_siege_input.setNom(region);
                        region_data.insertOrReplace(region_siege_input);

                        adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
                        adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
                        adresse_siege_social_input.setPays(pays_siege_input);
                        adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
                        adresse_siege_social_input.setRegion(region_siege_input);
                        adresse_siege_social_input.setID_Region(region_siege_input.getId());
                        adresse_data.insertOrReplace(adresse_siege_social_input);

                        entreprise_input.setAdresse_Siege(adresse_siege_social_input);
                        entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

                        if (false) {
                            //Adresse_facturation
                            //                        Adresse adresse_facturation_input = new Adresse();
                            //                        adresse_facturation_input.setAdresse_numero_rue(Adresse_facturation_1.getText().toString().trim()); //Adresse
                            //                        adresse_facturation_input.setComplement_adresse(Adresse_facturation_2.getText().toString().trim()); //Complement Adresse
                            //                        adresse_facturation_input.setArchive(1);
                            //                        adresse_facturation_input.setDate_creation(Today_Date());
                            //                        adresse_facturation_input.setDate_modification(Today_Date());
                            //                        adresse_facturation_input.setID_Agronome_Creation(1);
                            //                        adresse_facturation_input.setID_Agronome_Modification(1);
                            //
                            //                        //Code postal adresse facturation
                            //                        Code_Postal_Ville code_postal_ville_facturation_input = new Code_Postal_Ville();
                            //                        code_postal_ville_facturation_input.setCode_postal(CodePostal_facturation.getText().toString().trim());
                            //                        code_postal_ville_facturation_input.setVille(Ville_facturation.getText().toString().trim());
                            //                        code_postal_ville_data.insertOrReplace(code_postal_ville_facturation_input);
                            //
                            //                        //Pays adresse facturation
                            //                        Pays pays_facturation_input = new Pays();
                            //                        pays_facturation_input.setNom(Pays_facturation.getText().toString().trim());
                            //                        pays_data.insertOrReplace(pays_facturation_input);
                            //
                            //                        //Région adresse facturation
                            //                        Region region_facturation_input = new Region();
                            //                        region_facturation_input.setNom(Region_facturation.getText().toString().trim());
                            //                        region_data.insertOrReplace(region_facturation_input);
                            //
                            //                        adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_facturation_input);
                            //                        adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_facturation_input.getId());
                            //                        adresse_facturation_input.setPays(pays_facturation_input);
                            //                        adresse_facturation_input.setID_Pays(pays_facturation_input.getId());
                            //                        adresse_facturation_input.setRegion(region_facturation_input);
                            //                        adresse_facturation_input.setID_Region(region_facturation_input.getId());
                            //                        adresse_data.insertOrReplace(adresse_facturation_input);
                            //
                            //                        entreprise_input.setAdresse_Facturation(adresse_facturation_input);
                            //                        entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
                        } else {
                            entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
                            entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
                        }

                        if (false) {
                            //Adresse livraison
                            //                        Adresse adresse_livraison_input = new Adresse();
                            //                        adresse_livraison_input.setAdresse_numero_rue(Adresse_livraison_1.getText().toString().trim()); //Adresse
                            //                        adresse_livraison_input.setComplement_adresse(Adresse_livraison_2.getText().toString().trim()); //Complement Adresse
                            //                        adresse_livraison_input.setArchive(1);
                            //                        adresse_livraison_input.setDate_creation(Today_Date());
                            //                        adresse_livraison_input.setDate_modification(Today_Date());
                            //                        adresse_livraison_input.setID_Agronome_Creation(1);
                            //                        adresse_livraison_input.setID_Agronome_Modification(1);
                            //
                            //                        //Code postal adresse livraison
                            //                        Code_Postal_Ville code_postal_ville_livraison_input = new Code_Postal_Ville();
                            //                        code_postal_ville_livraison_input.setCode_postal(CodePostal_livraison.getText().toString().trim());
                            //                        code_postal_ville_livraison_input.setVille(Ville_livraison.getText().toString().trim());
                            //                        code_postal_ville_data.insertOrReplace(code_postal_ville_livraison_input);
                            //
                            //                        //Pays adresse livraison
                            //                        Pays pays_livraison_input = new Pays();
                            //                        pays_livraison_input.setNom(Pays_livraison.getText().toString().trim());
                            //                        pays_data.insertOrReplace(pays_livraison_input);
                            //
                            //                        //Région adresse livraison
                            //                        Region region_livraison_input = new Region();
                            //                        region_livraison_input.setNom(Region_livraison.getText().toString().trim());
                            //                        region_data.insertOrReplace(region_livraison_input);
                            //
                            //                        adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_livraison_input);
                            //                        adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_livraison_input.getId());
                            //                        adresse_livraison_input.setPays(pays_livraison_input);
                            //                        adresse_livraison_input.setID_Pays(pays_livraison_input.getId());
                            //                        adresse_livraison_input.setRegion(region_livraison_input);
                            //                        adresse_livraison_input.setID_Region(region_livraison_input.getId());
                            //                        adresse_data.insertOrReplace(adresse_livraison_input);
                            //
                            //                        entreprise_input.setAdresse_Livraison(adresse_livraison_input);
                            //                        entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
                        } else {
                            entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
                            entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
                        }
                        //add data to enterprise table
                        entreprise_data.insertOrReplace(entreprise_input);


                        if ((!(type_bio.equals(getString(R.string.TextTypeBio))))) {

                            List<Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                                    .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                                    .list();
                            if (List_Entreprise_cible != null) {
                                for (Entreprise e : List_Entreprise_cible) {

                                    List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                            .where(Type_BioDao.Properties.Nom.eq(type_bio))
                                            .list();
                                    if (List_Type_Bio != null) {
                                        for (Type_Bio TB : List_Type_Bio) {
                                            TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                            //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                            type_bio_data.insertOrReplace(TB);
                                            e.update();
                                        }
                                    }
                                }
                            }
                        }


                        //Associer la nouvelle entreprise au compte
                        List<Compte> update_compte = compte_dao.queryBuilder()
                                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                .list();
                        if (update_compte != null) {
                            for (Compte c : update_compte) {
                                entreprise_input.setID_Compte(c.getId());
                                entreprise_data.insertOrReplace(entreprise_input);
                                c.update();
                            }
                        }


                        ContactDao contact_data = daoSession.getContactDao();
                        Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
                        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

                        IdentiteDao identite_data = daoSession.getIdentiteDao();
                        SexeDao sexe_data = daoSession.getSexeDao();


                        //Table Contact
                        com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
                        contact_input.setRemarque_contact(remarque);
                        // contact_input.setContact_identifiant(contact_identifiant);
                        contact_input.setArchive(Integer.parseInt(archive));
                        contact_input.setDate_creation(datecreation);
                        contact_input.setDate_modification(datemodification);
                        contact_input.setID_Agronome_Creation(1);
                        contact_input.setID_Agronome_Modification(1);

                        //Ajout de la table Categorie
                        List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
                                .list();
                        if (List_Categorie != null) {
                            for (Categorie_Contact CC : List_Categorie) {
                                contact_input.setCategorie_Contact(CC);
                                contact_input.setID_Categorie_Contact(CC.getId());
                            }
                        }

                        //Ajout de la table Sociabilite
                        List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
                                .list();
                        if (List_Sociabilite != null) {
                            for (Sociabilite S : List_Sociabilite) {
                                contact_input.setSociabilite(S);
                                contact_input.setID_Sociabilite(S.getId());
                            }
                        }


                        //Table identité
                        Identite identite_input = new Identite();
                        identite_input.setNom(nom); //Nom
                        identite_input.setPrenom(prenom); //Prenom
                        identite_input.setPhoto("");
                        identite_input.setTelephone_principal(telephone_principal); //Telephone principale
                        identite_input.setTelephone_secondaire(telephone_secondaire); //Telephoe secondaire
                        identite_input.setAdresse_email(adresse_mail); //Mail 1
                        identite_input.setArchive(Integer.parseInt(archive));
                        identite_input.setDate_creation(datecreation);
                        identite_input.setDate_modification(datemodification);
                        identite_input.setID_Agronome_Creation(1);
                        identite_input.setID_Agronome_Modification(1);

                        Sexe sexe_input = new Sexe();

                        sexe_input.setNom(sexe);

                        sexe_data.insertOrReplace(sexe_input);


                        //Gestion de la photo
                        //                    if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                        //                        //Photo basse qualité
                        //                        File myDir_Source1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                        //                        File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");
                        //
                        //                        File myDir_Dest1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                        //                        File photoFile_Dest1 = new File(myDir_Dest1, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                        //                        try {
                        //                            CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
                        //                        } catch (IOException e) {
                        //                            e.printStackTrace();
                        //                        }
                        //                        //Photo haute qualité
                        //                        File myDir_Source2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                        //                        File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");
                        //
                        //                        File myDir_Dest2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                        //                        File photoFile_Dest2 = new File(myDir_Dest2, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture_HD.jpg");
                        //                        try {
                        //                            CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
                        //                        } catch (IOException e) {
                        //                            e.printStackTrace();
                        //                        }
                        //
                        //                        identite_input.setPhoto(Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                        //                    }
                        //                    else{
                        //
                        //                        identite_input.setPhoto("");
                        //                    }

                        //Table Adresse
                        Adresse adresse_input = new Adresse();
                        adresse_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                        adresse_input.setComplement_adresse(complement_adresse); //Complement Adresse
                        adresse_input.setArchive(Integer.parseInt(archive));
                        adresse_input.setDate_creation(Today_Date());
                        adresse_input.setDate_modification(Today_Date());
                        adresse_input.setID_Agronome_Creation(1);
                        adresse_input.setID_Agronome_Modification(1);


                        Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
                        code_postal_ville_input.setCode_postal(code_postale);
                        code_postal_ville_input.setVille(ville);
                        code_postal_ville_data.insertOrReplace(code_postal_ville_input);

                        Region region_input = new Region();
                        region_input.setNom(region);
                        region_data.insertOrReplace(region_input);

                        Pays pays_input = new Pays();
                        pays_input.setNom(pays);
                        pays_data.insertOrReplace(pays_input);

                        //Cardinalités entre les tables :
                        adresse_input.setCode_Postal_Ville(code_postal_ville_input);
                        adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
                        adresse_input.setRegion(region_input);
                        adresse_input.setID_Region(region_input.getId());
                        adresse_input.setPays(pays_input);
                        adresse_input.setID_Pays(pays_input.getId());
                        adresse_data.insertOrReplace(adresse_input);


                        identite_input.setAdresse(adresse_input);
                        identite_input.setID_Adresse(adresse_input.getId());

                        identite_input.setSexe(sexe_input);
                        identite_input.setID_Sexe(sexe_input.getId());

                        identite_data.insertOrReplace(identite_input);
                        //Un contact possède une identité
                        contact_input.setIdentite(identite_input);
                        contact_input.setID_Identite_Contact(identite_input.getId());

                        //Ajout du contact à l'entreprise concernée
                        List<Compte> update_compte1 = compte_dao.queryBuilder()
                                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                .list();
                        if (update_compte1 != null) {
                            for (Compte c : update_compte1) {
                                List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                                for (Entreprise e : List_Company) {
                                    if (e.getRaison_sociale().equals(raison_sociale)) {
                                        contact_input.setID_Entreprise(e.getId());
                                        contact_data.insertOrReplace(contact_input);
                                        e.update();
                                    }
                                }
                                c.update();
                            }
                        }

                    }
                }
                    i++;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        //check if data already exist in contact
       public boolean alreadyExistContact(String nom)
       {
           boolean b = true;
           DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
           CompteDao compte_dao = daoSession.getCompteDao();
           List<Compte> update_compte = compte_dao.queryBuilder()
                   .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                   .list();
           if (update_compte != null) {
               for (Compte c : update_compte) {
                   List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                   for (Entreprise e : List_Entreprise) {

                       List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                       for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                           if (ct.getContact_identifiant() != null) {
                               if (ct.getIdentite().getNom().toString().trim().equals(nom.trim())) {
                                   b = false;

                               }
                           }

                       }

                   }
               }
           }
           return b;

       }


    }

}
