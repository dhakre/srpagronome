package com.srp.agronome.android;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.srp.agronome.android.db.Activite;
import com.srp.agronome.android.db.ActiviteDao;
import com.srp.agronome.android.db.Adresse;
import com.srp.agronome.android.db.AdresseDao;
import com.srp.agronome.android.db.Biologique;
import com.srp.agronome.android.db.BiologiqueDao;
import com.srp.agronome.android.db.Categorie_Contact;
import com.srp.agronome.android.db.Categorie_ContactDao;
import com.srp.agronome.android.db.Code_Postal_Ville;
import com.srp.agronome.android.db.Code_Postal_VilleDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.ContactDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Geolocalisation;
import com.srp.agronome.android.db.GeolocalisationDao;
import com.srp.agronome.android.db.Identite;
import com.srp.agronome.android.db.IdentiteDao;
import com.srp.agronome.android.db.Metier;
import com.srp.agronome.android.db.MetierDao;
import com.srp.agronome.android.db.Pays;
import com.srp.agronome.android.db.PaysDao;
import com.srp.agronome.android.db.Region;
import com.srp.agronome.android.db.RegionDao;
import com.srp.agronome.android.db.Sexe;
import com.srp.agronome.android.db.SexeDao;
import com.srp.agronome.android.db.Situation;
import com.srp.agronome.android.db.SituationDao;
import com.srp.agronome.android.db.Sociabilite;
import com.srp.agronome.android.db.SociabiliteDao;
import com.srp.agronome.android.db.Statut_Soytouch;
import com.srp.agronome.android.db.Statut_SoytouchDao;
import com.srp.agronome.android.db.Structure_Sociale;
import com.srp.agronome.android.db.Structure_SocialeDao;
import com.srp.agronome.android.db.Type_Bio;
import com.srp.agronome.android.db.Type_BioDao;
import com.srp.agronome.android.webservice.Service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.CreateContact.Today_Date;
import static com.srp.agronome.android.CreateOrganization.Date_Arret;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_compte_server;
import static com.srp.agronome.android.MainLogin.ID_Contact_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.Nom_Contact;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;
import static com.srp.agronome.android.MainLogin.Prenom_Contact;

/**
 * Created by doucoure on 13/06/2017.
 */

public class MainMenu extends AppCompatActivity {

    private static final String TAG = "MAINMENU";
    private Button BtnContact = null;

    private Button BtnPhoto = null;

    private Button BtnDiscuterPartager = null;

    private ImageButton imgBtnHome = null;

    private ImageButton imgBtnSyncro = null;

    private ImageButton imgBtnSettingsAccount = null;

    private ProgressBar progressBar = null;

    //fun complete
    int returnCode, returnCode2;
    long compteID;


    SharedPreferences wmbPreference1;
    boolean isFirstRun1;

    SharedPreferences wmbPreference;
    boolean isFirstRun;

    private static int executeCountJust = 1;
    private static int executeCountMathu = 1;

    //add accounts data
    static String name = null;
    static String fname = null;
    static String nameM = null;
    static String fnameM = null;



    private AddDataForAccounts ad = new AddDataForAccounts();


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_menu_principale);

        BtnContact = (Button) findViewById(R.id.btnmenu1);
        BtnContact.setOnClickListener(ButtonContactHandler);

        BtnPhoto = (Button) findViewById(R.id.btnmenu2);
        BtnPhoto.setOnClickListener(ButtonPhotoHandler);

        BtnDiscuterPartager = (Button) findViewById(R.id.btnmenu3);
        BtnDiscuterPartager.setOnClickListener(ButtontDiscussionHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_main);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

        imgBtnSyncro = (ImageButton) findViewById(R.id.sync);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        // imgBtnSyncro
        // .setOnClickListener(ButtonSyngHandler);

        //progress bar


        imgBtnSettingsAccount = (ImageButton) findViewById(R.id.btn_settings);
        imgBtnSettingsAccount.setOnClickListener(ButtonSettingsHandler);

        getData gd = new getData();
        gd.execute();

        putData pd = new putData();
        pd.execute();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Show_Dialog_Home(getString(R.string.TitleDisconnect), getString(R.string.TextDisconnect));
        }
        return true;
    }

    View.OnClickListener ButtonContactHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MainMenu.this, ContactMenu.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener ButtonPhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MainMenu.this, MenuPhoto.class);
            startActivity(intent);
            finish();
//            Toast.makeText(getApplicationContext(),"Modifications à venir prochainement",Toast.LENGTH_SHORT).show();
        }
    };

    View.OnClickListener ButtontDiscussionHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "Prochainement disponible", Toast.LENGTH_SHORT).show();
        }
    };

//    View.OnClickListener ButtonSyngHandler = new View.OnClickListener() {
//        public void onClick(View v) {
//            getData gd = new getData();
//            gd.execute();
//
//            putData pd = new putData();
//            pd.execute();
//        }
//    };

    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDisconnect), getString(R.string.TextDisconnect));
        }
    };

    View.OnClickListener ButtonSettingsHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MainMenu.this, EditAccount.class);
            startActivity(intent);
            finish();
        }
    };


    //Display Dialog return Homege
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenu.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(MainMenu.this, MainLogin.class);
                finish();
                startActivity(intent);

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    // communication database

    public class getData extends AsyncTask<String, String, String> {
        //all get and post methods here
        HttpURLConnection urlConnection;
        //        private ProgressDialog dialog;
        ProgressDialog dialog = new ProgressDialog(MainMenu.this);

        //
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Loading...");

            // progressBar.setVisibility(View.VISIBLE);

        }


       /* @Override
        protected void onProgressUpdate(String result) {
            super.onProgressUpdate(result);
            int total=1060162;
            publishProgress();
        }*/

        @Override
        protected String doInBackground(String... args) {
            Log.i("App", "debut");
            StringBuilder result = new StringBuilder();
                     //j                       //m
            if ((ID_compte_server != 10)) {
                try {
                    URL url = new URL(Service.ENDPOINT);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                        Log.i("App", line + "---------------------------------");

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }
                Log.i("App", "fin background");

                try {
                    Log.i("result string size", "doInBackground: " + result.toString().length()); //1060162
                    JSONObject jsonObj = new JSONObject(result.toString());
                    JSONArray jsonArray = jsonObj.getJSONArray("testData");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject explrObject = jsonArray.getJSONObject(i);
                        String id_agronome = explrObject.getString("id_agronome_creation");
                        String archive = explrObject.getString("archive");
                        String categorie = explrObject.getString("categorie");
                        String date_creation = explrObject.getString("date_creation");
                        String date_modif = explrObject.getString("date_modification");
                        String metier = explrObject.getString("metier");
                        String numero_contact = explrObject.getString("numero_contact");
                        String remarque = explrObject.getString("remarque");
                        String contact_identifiant = explrObject.getString("contact_identifiant");
                        String sociabilite = explrObject.getString("sociabilite");
                        String adresse_mail = explrObject.getString("adresse_mail");
                        String date_naissance = explrObject.getString("date_naissance");
                        String nom = explrObject.getString("nom");
                        String prenom = explrObject.getString("prenom");
                        String sexe = explrObject.getString("sexe");
                        String telephone_principal = explrObject.getString("telephone_principal");
                        String telephone_secondaire = explrObject.getString("telephone_secondaire");
                        String code_postale = explrObject.getString("code_postale");
                        String complement_adresse = explrObject.getString("complement_adresse");
                        String groupement = explrObject.getString("groupement");
                        String numero_nom_rue = explrObject.getString("numero_nom_rue");
                        String pays = explrObject.getString("pays");
                        String region = explrObject.getString("region");
                        String ville = explrObject.getString("ville");
                        String zone = explrObject.getString("zone");
                        String activite_principale = explrObject.getString("activite_principale");
                        String activite_secondaire = explrObject.getString("activite_secondaire");
                        String adresse_mail_principale = explrObject.getString("adresse_mail_principale");
                        String adresse_mail_secondaire = explrObject.getString("adresse_mail_secondaire");
                        String ancien = explrObject.getString("ancien");
                        String biologique = explrObject.getString("biologique");
                        String cause_arret = explrObject.getString("cause_arret");
                        String fax = explrObject.getString("fax");
                        String nombre_salarie = explrObject.getString("nombre_salarie");
                        String projet_connu = explrObject.getString("projet_connu");
                        String raison_sociale = explrObject.getString("raison_sociale");
                        String siret = explrObject.getString("siret");
                        String situation = explrObject.getString("situation");
                        String statut_soytouch = explrObject.getString("statut_soytouch");
                        String structure_sociale = explrObject.getString("structure_sociale");
                        String telephone_entreprise = explrObject.getString("telephone_entreprise");
                        String type_bio = explrObject.getString("type_bio");
                        String compte_id = explrObject.getString("id_compte");   //get compte id
                        compteID = 0;
                        if (compte_id.equals("null")) {
                            //compteID = Long.parseLong(compte_id.trim());
                            //Log.i("ID", "doInBackground: compte id=" + compteID);
                        } else {
                            // compteID = 4;
                            compteID = Long.parseLong(compte_id.trim());
                            Log.i("ID", "doInBackground: compte id=" + compteID);
                        }


                        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
                        CompteDao compte_dao = daoSession.getCompteDao();

                        DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        Date datecreation = sourceFormat.parse(date_creation);
                        Date datemodification = sourceFormat.parse(date_modif);


                        //insertion dans la base de données locale
                        //existeContact retourne vrai si le contact n'existe pas

                        if (( compteID==ID_compte_server)||(ID_compte_server==4)||(ID_compte_server==3))
                        {
                            Log.i(TAG, "doInBackground:inside condition of compteID ");
                            if (existeContact(contact_identifiant, nom, prenom)) {
                                if (nom != null)

                                {
                                    //if(ID_compte_server==compteID)

                                    //account check no justine and mathieu get from server
                                    if ((ID_compte_server != 10)) {
                                        // entreprise

                                        Log.i("CREATION", datecreation.toString());
                                        Log.i("MODIF", datemodification.toString());

                                        Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
                                        SituationDao situation_data = daoSession.getSituationDao();
                                        Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
                                        ActiviteDao activite_data = daoSession.getActiviteDao();
                                        BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
                                        Type_BioDao type_bio_data = daoSession.getType_BioDao();

                                        AdresseDao adresse_data = daoSession.getAdresseDao();
                                        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
                                        PaysDao pays_data = daoSession.getPaysDao();
                                        RegionDao region_data = daoSession.getRegionDao();

                                        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

                                        //Table Entreprise
                                        Entreprise entreprise_input = new Entreprise();
                                        entreprise_input.setRaison_sociale(raison_sociale);
//                        if (nombre_salarie != null) {
//                            entreprise_input.setNombre_salarie(Integer.parseInt(nombre_salarie));
//                        }

                                        entreprise_input.setSIRET(siret);
                                        entreprise_input.setProjet_connu(projet_connu);
                                        entreprise_input.setTelephone_entreprise(telephone_entreprise);
                                        entreprise_input.setFax_entreprise(fax);
                                        entreprise_input.setAdresse_email_principale(adresse_mail_principale);
                                        entreprise_input.setAdresse_email_secondaire(adresse_mail_secondaire);

                                        entreprise_input.setArchive(Integer.parseInt(archive));
                                        entreprise_input.setDate_creation(datecreation);
                                        entreprise_input.setDate_modification(datemodification);
                                        entreprise_input.setID_Agronome_Creation(1);
                                        entreprise_input.setID_Agronome_Modification(1);
                                        // entreprise_input.setID_Compte(Long.parseLong(compte_id));//set the compte id

                                        //Ajout de la table Situation
                                        List<Situation> List_Situation = situation_data.queryBuilder()
                                                .where(SituationDao.Properties.Nom.eq(situation))
                                                .list();
                                        if (List_Situation != null) {
                                            for (Situation S : List_Situation) {
                                                entreprise_input.setSituation(S);
                                                entreprise_input.setID_Situation(S.getId());
                                            }
                                        }

                                        //Ajout de la table Structure Sociale
                                        List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                                                .where(Structure_SocialeDao.Properties.Nom.eq(structure_sociale))
                                                .list();
                                        if (List_Structure_Sociale != null) {
                                            for (Structure_Sociale SS : List_Structure_Sociale) {
                                                entreprise_input.setStructure_Sociale(SS);
                                                entreprise_input.setID_Structure_Sociale(SS.getId());
                                            }
                                        }

                                        //Ajout de la table Statut SoyTouch
                                        List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                                                .where(Statut_SoytouchDao.Properties.Nom.eq(statut_soytouch))
                                                .list();
                                        if (List_Statut != null) {
                                            for (Statut_Soytouch Sst : List_Statut) {
                                                entreprise_input.setStatut_Soytouch(Sst);
                                                entreprise_input.setID_Statut_Soytouch(Sst.getId());
                                            }
                                        }

                                        //Ajout de la table Activite Principale
                                        List<Activite> List_Activite_P = activite_data.queryBuilder()
                                                .where(ActiviteDao.Properties.Nom.eq(activite_principale))
                                                .list();
                                        if (List_Activite_P != null) {
                                            for (Activite AP : List_Activite_P) {
                                                entreprise_input.setActivite_Principale(AP);
                                                entreprise_input.setID_Activite_Principale(AP.getId());
                                            }
                                        }

                                        //Ajout de la table Activite Secondaire si renseignée
                                        if (!(activite_principale.equals(getString(R.string.TextActivitePrincipale)))) {
                                            List<Activite> List_Activite_S = activite_data.queryBuilder()
                                                    .where(ActiviteDao.Properties.Nom.eq(activite_secondaire))
                                                    .list();
                                            if (List_Activite_S != null) {
                                                for (Activite AS : List_Activite_S) {
                                                    entreprise_input.setActivite_Secondaire(AS);
                                                    entreprise_input.setID_Activite_Secondaire(AS.getId());
                                                }
                                            }
                                        }


                                        //Ajout de la table Biologique

                                        List<Biologique> List_Bio = biologique_data.queryBuilder()
                                                .where(BiologiqueDao.Properties.Nom.eq(biologique))
                                                .list();
                                        if (List_Bio != null) {
                                            for (Biologique B : List_Bio) {
                                                entreprise_input.setBiologique(B);
                                                entreprise_input.setID_Biologique(B.getId());
                                            }
                                        }


                                        entreprise_input.setArret_activite(true);
                                        entreprise_input.setCause_arret_activite(cause_arret);
                                        entreprise_input.setDate_arret_activite(Date_Arret);


                                        //Adresse_siege_social = Adresse de l'entreprise
                                        Adresse adresse_siege_social_input = new Adresse();
                                        adresse_siege_social_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                                        adresse_siege_social_input.setComplement_adresse(complement_adresse); //Complement Adresse
                                        adresse_siege_social_input.setArchive(GlobalValues.getInstance().getGlobalArchive());    //Global Value archieve
                                        adresse_siege_social_input.setDate_creation(datecreation);
                                        adresse_siege_social_input.setDate_modification(datemodification);
                                        adresse_siege_social_input.setID_Agronome_Creation(1);
                                        adresse_siege_social_input.setID_Agronome_Modification(1);

                                        //Code postal adresse siège sociale
                                        Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

                                        code_postal_ville_siege_input.setCode_postal(code_postale);

                                        code_postal_ville_siege_input.setVille(ville);
                                        code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

                                        //Pays adresse siège
                                        Pays pays_siege_input = new Pays();
                                        pays_siege_input.setNom(pays);
                                        pays_data.insertOrReplace(pays_siege_input);

                                        //Région adresse siège
                                        Region region_siege_input = new Region();
                                        region_siege_input.setNom(region);
                                        region_data.insertOrReplace(region_siege_input);

                                        adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
                                        adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
                                        adresse_siege_social_input.setPays(pays_siege_input);
                                        adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
                                        adresse_siege_social_input.setRegion(region_siege_input);
                                        adresse_siege_social_input.setID_Region(region_siege_input.getId());
                                        adresse_data.insertOrReplace(adresse_siege_social_input);

                                        entreprise_input.setAdresse_Siege(adresse_siege_social_input);
                                        entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

                                        if (false) {
                                            //Adresse_facturation
//                        Adresse adresse_facturation_input = new Adresse();
//                        adresse_facturation_input.setAdresse_numero_rue(Adresse_facturation_1.getText().toString().trim()); //Adresse
//                        adresse_facturation_input.setComplement_adresse(Adresse_facturation_2.getText().toString().trim()); //Complement Adresse
//                        adresse_facturation_input.setArchive(1);
//                        adresse_facturation_input.setDate_creation(Today_Date());
//                        adresse_facturation_input.setDate_modification(Today_Date());
//                        adresse_facturation_input.setID_Agronome_Creation(1);
//                        adresse_facturation_input.setID_Agronome_Modification(1);
//
//                        //Code postal adresse facturation
//                        Code_Postal_Ville code_postal_ville_facturation_input = new Code_Postal_Ville();
//                        code_postal_ville_facturation_input.setCode_postal(CodePostal_facturation.getText().toString().trim());
//                        code_postal_ville_facturation_input.setVille(Ville_facturation.getText().toString().trim());
//                        code_postal_ville_data.insertOrReplace(code_postal_ville_facturation_input);
//
//                        //Pays adresse facturation
//                        Pays pays_facturation_input = new Pays();
//                        pays_facturation_input.setNom(Pays_facturation.getText().toString().trim());
//                        pays_data.insertOrReplace(pays_facturation_input);
//
//                        //Région adresse facturation
//                        Region region_facturation_input = new Region();
//                        region_facturation_input.setNom(Region_facturation.getText().toString().trim());
//                        region_data.insertOrReplace(region_facturation_input);
//
//                        adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_facturation_input);
//                        adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_facturation_input.getId());
//                        adresse_facturation_input.setPays(pays_facturation_input);
//                        adresse_facturation_input.setID_Pays(pays_facturation_input.getId());
//                        adresse_facturation_input.setRegion(region_facturation_input);
//                        adresse_facturation_input.setID_Region(region_facturation_input.getId());
//                        adresse_data.insertOrReplace(adresse_facturation_input);
//
//                        entreprise_input.setAdresse_Facturation(adresse_facturation_input);
//                        entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
                                        } else {
                                            entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
                                            entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
                                        }

                                        if (false) {
                                            //Adresse livraison
//                        Adresse adresse_livraison_input = new Adresse();
//                        adresse_livraison_input.setAdresse_numero_rue(Adresse_livraison_1.getText().toString().trim()); //Adresse
//                        adresse_livraison_input.setComplement_adresse(Adresse_livraison_2.getText().toString().trim()); //Complement Adresse
//                        adresse_livraison_input.setArchive(1);
//                        adresse_livraison_input.setDate_creation(Today_Date());
//                        adresse_livraison_input.setDate_modification(Today_Date());
//                        adresse_livraison_input.setID_Agronome_Creation(1);
//                        adresse_livraison_input.setID_Agronome_Modification(1);
//
//                        //Code postal adresse livraison
//                        Code_Postal_Ville code_postal_ville_livraison_input = new Code_Postal_Ville();
//                        code_postal_ville_livraison_input.setCode_postal(CodePostal_livraison.getText().toString().trim());
//                        code_postal_ville_livraison_input.setVille(Ville_livraison.getText().toString().trim());
//                        code_postal_ville_data.insertOrReplace(code_postal_ville_livraison_input);
//
//                        //Pays adresse livraison
//                        Pays pays_livraison_input = new Pays();
//                        pays_livraison_input.setNom(Pays_livraison.getText().toString().trim());
//                        pays_data.insertOrReplace(pays_livraison_input);
//
//                        //Région adresse livraison
//                        Region region_livraison_input = new Region();
//                        region_livraison_input.setNom(Region_livraison.getText().toString().trim());
//                        region_data.insertOrReplace(region_livraison_input);
//
//                        adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_livraison_input);
//                        adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_livraison_input.getId());
//                        adresse_livraison_input.setPays(pays_livraison_input);
//                        adresse_livraison_input.setID_Pays(pays_livraison_input.getId());
//                        adresse_livraison_input.setRegion(region_livraison_input);
//                        adresse_livraison_input.setID_Region(region_livraison_input.getId());
//                        adresse_data.insertOrReplace(adresse_livraison_input);
//
//                        entreprise_input.setAdresse_Livraison(adresse_livraison_input);
//                        entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
                                        } else {
                                            entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
                                            entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
                                        }
                                        //add data to enterprise table
                                        entreprise_data.insertOrReplace(entreprise_input);


                                        if ((!(type_bio.equals(getString(R.string.TextTypeBio))))) {

                                            List<Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                                                    .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                                                    .list();
                                            if (List_Entreprise_cible != null) {
                                                for (Entreprise e : List_Entreprise_cible) {

                                                    List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                                            .where(Type_BioDao.Properties.Nom.eq(type_bio))
                                                            .list();
                                                    if (List_Type_Bio != null) {
                                                        for (Type_Bio TB : List_Type_Bio) {
                                                            TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                                            //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                                            type_bio_data.insertOrReplace(TB);
                                                            e.update();
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                        //Associer la nouvelle entreprise au compte
                                        List<Compte> update_compte = compte_dao.queryBuilder()
                                                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                                .list();
                                        if (update_compte != null) {
                                            for (Compte c : update_compte) {
                                                entreprise_input.setID_Compte(c.getId());
                                                entreprise_data.insertOrReplace(entreprise_input);
                                                c.update();
                                            }
                                        }


                                        ContactDao contact_data = daoSession.getContactDao();
                                        Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
                                        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

                                        IdentiteDao identite_data = daoSession.getIdentiteDao();
                                        SexeDao sexe_data = daoSession.getSexeDao();


                                        //Table Contact
                                        com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
                                        contact_input.setRemarque_contact(remarque);
                                        contact_input.setContact_identifiant(contact_identifiant);
                                        contact_input.setArchive(Integer.parseInt(archive));
                                        contact_input.setDate_creation(datecreation);
                                        contact_input.setDate_modification(datemodification);
                                        contact_input.setID_Agronome_Creation(1);
                                        contact_input.setID_Agronome_Modification(1);

                                        //Ajout de la table Categorie
                                        List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                                .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
                                                .list();
                                        if (List_Categorie != null) {
                                            for (Categorie_Contact CC : List_Categorie) {
                                                contact_input.setCategorie_Contact(CC);
                                                contact_input.setID_Categorie_Contact(CC.getId());
                                            }
                                        }

                                        //Ajout de la table Sociabilite
                                        List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                                .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
                                                .list();
                                        if (List_Sociabilite != null) {
                                            for (Sociabilite S : List_Sociabilite) {
                                                contact_input.setSociabilite(S);
                                                contact_input.setID_Sociabilite(S.getId());
                                            }
                                        }


                                        //Table identité
                                        Identite identite_input = new Identite();
                                        identite_input.setNom(nom); //Nom
                                        identite_input.setPrenom(prenom); //Prenom
                                        identite_input.setPhoto("");
                                        identite_input.setTelephone_principal(telephone_principal); //Telephone principale
                                        identite_input.setTelephone_secondaire(telephone_secondaire); //Telephoe secondaire
                                        identite_input.setAdresse_email(adresse_mail); //Mail 1
                                        identite_input.setArchive(Integer.parseInt(archive));
                                        identite_input.setDate_creation(datecreation);
                                        identite_input.setDate_modification(datemodification);
                                        identite_input.setID_Agronome_Creation(1);
                                        identite_input.setID_Agronome_Modification(1);

                                        Sexe sexe_input = new Sexe();

                                        sexe_input.setNom(sexe);

                                        sexe_data.insertOrReplace(sexe_input);


                                        //Gestion de la photo
//                    if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
//                        //Photo basse qualité
//                        File myDir_Source1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
//                        File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");
//
//                        File myDir_Dest1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
//                        File photoFile_Dest1 = new File(myDir_Dest1, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
//                        try {
//                            CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        //Photo haute qualité
//                        File myDir_Source2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
//                        File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");
//
//                        File myDir_Dest2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
//                        File photoFile_Dest2 = new File(myDir_Dest2, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture_HD.jpg");
//                        try {
//                            CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                        identite_input.setPhoto(Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
//                    }
//                    else{
//
//                        identite_input.setPhoto("");
//                    }

                                        //Table Adresse
                                        Adresse adresse_input = new Adresse();
                                        adresse_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                                        adresse_input.setComplement_adresse(complement_adresse); //Complement Adresse
                                        adresse_input.setArchive(Integer.parseInt(archive));
                                        adresse_input.setDate_creation(Today_Date());
                                        adresse_input.setDate_modification(Today_Date());
                                        adresse_input.setID_Agronome_Creation(1);
                                        adresse_input.setID_Agronome_Modification(1);


                                        Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
                                        code_postal_ville_input.setCode_postal(code_postale);
                                        code_postal_ville_input.setVille(ville);
                                        code_postal_ville_data.insertOrReplace(code_postal_ville_input);

                                        Region region_input = new Region();
                                        region_input.setNom(region);
                                        region_data.insertOrReplace(region_input);

                                        Pays pays_input = new Pays();
                                        pays_input.setNom(pays);
                                        pays_data.insertOrReplace(pays_input);

                                        //Cardinalités entre les tables :
                                        adresse_input.setCode_Postal_Ville(code_postal_ville_input);
                                        adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
                                        adresse_input.setRegion(region_input);
                                        adresse_input.setID_Region(region_input.getId());
                                        adresse_input.setPays(pays_input);
                                        adresse_input.setID_Pays(pays_input.getId());
                                        adresse_data.insertOrReplace(adresse_input);


                                        identite_input.setAdresse(adresse_input);
                                        identite_input.setID_Adresse(adresse_input.getId());

                                        identite_input.setSexe(sexe_input);
                                        identite_input.setID_Sexe(sexe_input.getId());

                                        identite_data.insertOrReplace(identite_input);
                                        //Un contact possède une identité
                                        contact_input.setIdentite(identite_input);
                                        contact_input.setID_Identite_Contact(identite_input.getId());

                                        //Ajout du contact à l'entreprise concernée
                                        List<Compte> update_compte1 = compte_dao.queryBuilder()
                                                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                                .list();
                                        if (update_compte1 != null) {
                                            for (Compte c : update_compte1) {
                                                List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                                                for (Entreprise e : List_Company) {
                                                    if (e.getRaison_sociale().equals(raison_sociale)) {
                                                        contact_input.setID_Entreprise(e.getId());
                                                        contact_data.insertOrReplace(contact_input);
                                                        e.update();
                                                    }
                                                }
                                                c.update();
                                            }
                                        }


                                    } else {
                                        // Log.i(TAG, "doInBackground: reading data fot new added justine and mathieu");
                                        //if ((ID_compte_server==2)&& (returnCode==1)&&(compte_id==2))


                                    }
                                }
                            }
                    }
//                    if (!existeContact(contact_identifiant)) {
//                        ContactDao contact_data = daoSession.getContactDao();
//                        Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
//                        MetierDao metier_data = daoSession.getMetierDao();
//                        SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();
//                        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
//                        IdentiteDao identite_data = daoSession.getIdentiteDao();
//                        SexeDao sexe_data = daoSession.getSexeDao();
//                        AdresseDao adresse_data = daoSession.getAdresseDao();
//                        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
//                        RegionDao region_data = daoSession.getRegionDao();
//                        PaysDao pays_data = daoSession.getPaysDao();
//                        GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();
//
//
//                        List<Compte> update_compte = compte_dao.queryBuilder()
//                                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
//                                .list();
//                        if (update_compte != null) {
//                            for (Compte c : update_compte) {
//                                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
//                                for (Entreprise e : List_Entreprise) {
//                                    List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
//                                    for (com.srp.agronome.android.db.Contact ct : List_Contact) {
//                                        if (comparedate(datemodification, ct.getDate_modification())) {
//
//                                            ct.getIdentite().setNom(nom);
//                                            ct.getIdentite().setPrenom(prenom);
//                                            ct.setID_Agronome_Modification((int) ID_Compte_Selected);
//                                            ct.setDate_modification(datemodification);
//
//
//                                            //Edit Spinner Categorie
//                                            List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
//                                                    .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
//                                                    .list();
//                                            if (List_Categorie != null) {
//                                                for (Categorie_Contact CC : List_Categorie) {
//                                                    ct.setCategorie_Contact(CC);
//                                                    ct.setID_Categorie_Contact(CC.getId());
//                                                }
//                                            }
//
//                                            //Edit Spinner Metier
//                                            List<Metier> List_Metier = metier_data.queryBuilder()
//                                                    .where(MetierDao.Properties.Nom.eq(metier))
//                                                    .list();
//                                            if (List_Metier != null) {
//                                                for (Metier M : List_Metier) {
//                                                    ct.setMetier(M);
//                                                    ct.setID_Metier(M.getId());
//                                                }
//                                            }
//
//                                            //Edit Spinner Sociabilite
//                                            List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
//                                                    .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
//                                                    .list();
//                                            if (List_Sociabilite != null) {
//                                                for (Sociabilite S : List_Sociabilite) {
//                                                    ct.setSociabilite(S);
//                                                    ct.setID_Sociabilite(S.getId());
//                                                }
//                                            }
//
//                                            //Edit or Create Sexe
//
//                                            Sexe sexe_input = new Sexe();
//
//                                            sexe_input.setNom(sexe);
//
//                                            sexe_data.insertOrReplace(sexe_input);
//                                            ct.getIdentite().setSexe(sexe_input);
//                                            ct.getIdentite().setID_Sexe(sexe_input.getId());
//                                        }
//
//                                        //Edit date de naissance
//                                        ct.getIdentite().setDate_naissance(datecreation);
//
//                                        ct.getIdentite().getAdresse().setAdresse_numero_rue(numero_nom_rue);
//                                        ct.getIdentite().getAdresse().setComplement_adresse(complement_adresse);
//                                        ct.getIdentite().getAdresse().setID_Agronome_Modification((int) ID_Compte_Selected);
//                                        ct.getIdentite().getAdresse().setDate_modification(Today_Date());
//                                        ct.getIdentite().getAdresse().getCode_Postal_Ville().setCode_postal(code_postale);
//                                        ct.getIdentite().getAdresse().getCode_Postal_Ville().setVille(ville);
//
//                                        //Edit or create Region
//                                        if (ct.getIdentite().getAdresse().getRegion() != null) {
//                                            ct.getIdentite().getAdresse().getRegion().setNom(region);
//                                        }
//
//
//                                        //Edit Pays
//                                        ct.getIdentite().getAdresse().getPays().setNom(pays);
//
//                                        //Edit Telephone - Mail - Remarques
//                                        ct.getIdentite().setTelephone_principal(telephone_principal);
//                                        ct.getIdentite().setTelephone_secondaire(telephone_secondaire);
//                                        ct.getIdentite().setAdresse_email(adresse_mail);
//                                        ct.setRemarque_contact(remarque);
//
//
//                                        //Changement d'entreprise du contact
//                                        if (!(e.getRaison_sociale().equals(raison_sociale))) {
//                                            List<Compte> update_compte_entr = compte_dao.queryBuilder()
//                                                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
//                                                    .list();
//                                            if (update_compte_entr != null) {
//                                                for (Compte compte : update_compte_entr) {
//                                                    List<Entreprise> List_Company = compte.getEntrepriseList(); //get All Company
//                                                    for (Entreprise e2 : List_Company) {
//                                                        if (e2.getRaison_sociale().equals(raison_sociale)) {
//                                                            ct.setID_Entreprise(e2.getId());
//                                                            e.resetContactList();
//                                                            e2.resetContactList();
//                                                            entreprise_data.update(e2);
//                                                            compte_dao.update(compte);
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                        }
//
////                                        //Changement du répertoire de la photo si Changement Entreprise
////                                        if (!(e.getRaison_sociale().equals(Spinner_Entreprise.getSelectedItem().toString()))) {
////                                            //Photo basse qualité
////                                            File myDir_Source1 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
////                                            File photoFile_Source1 = new File(myDir_Source1, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
////
////                                            File myDir_Dest1 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
////                                            File photoFile_Dest1 = new File(myDir_Dest1, getPhotoContactNameLQ(Nom_Compte, Spinner_Entreprise.getSelectedItem().toString(), Nom_Contact, Prenom_Contact));
////                                            try {
////                                                CopyAndDeleteFileCache(photoFile_Source1, photoFile_Dest1);
////                                            } catch (IOException exception) {
////                                                exception.printStackTrace();
////                                            }
////                                            //Photo haute qualité
////                                            File myDir_Source2 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
////                                            File photoFile_Source2 = new File(myDir_Source2, getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
////
////                                            File myDir_Dest2 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
////                                            File photoFile_Dest2 = new File(myDir_Dest2, getPhotoContactNameHQ(Nom_Compte, Spinner_Entreprise.getSelectedItem().toString(), Nom_Contact, Prenom_Contact));
////                                            try {
////                                                CopyAndDeleteFileCache(photoFile_Source2, photoFile_Dest2);
////                                            } catch (IOException exception) {
////                                                exception.printStackTrace();
////                                            }
////                                            ct.getIdentite().setPhoto(getPhotoContactNameLQ(Nom_Compte, Spinner_Entreprise.getSelectedItem().toString(), Nom_Contact, Prenom_Contact));
////                                        }
//
//
//                                        //Update des tables : DAO
//                                        code_postal_ville_data.update(ct.getIdentite().getAdresse().getCode_Postal_Ville());
//                                        region_data.update(ct.getIdentite().getAdresse().getRegion());
//                                        pays_data.update(ct.getIdentite().getAdresse().getPays());
//                                        adresse_data.update(ct.getIdentite().getAdresse());
//                                        sexe_data.update(ct.getIdentite().getSexe());
//                                        identite_data.update(ct.getIdentite());
//                                        contact_data.update(ct);
//                                        entreprise_data.update(e);
//                                        compte_dao.update(c);
//
//                                        Nom_Contact = ct.getIdentite().getNom();
//                                        Prenom_Contact = ct.getIdentite().getPrenom();
//                                        Nom_Entreprise = raison_sociale;
//
//                                        ID_Entreprise_Selected = getIdEntreprise(raison_sociale);
//                                        ID_Contact_Selected = ct.getId();
//                                    }
//
//
//                                }
//                            }
//                        }
//                    }
                    }
                } catch (ParseException e2) {
                    e2.printStackTrace();
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            } else {
                Log.i(TAG, "doInBackground: reading data for justine");

                //get data for other accounts
                if (ID_compte_server == 1) {

                        Log.i(TAG, "doInBackground: writing data for justine");
                        //check id data already exist in db
                         //returnCode=ad.addDataJustine();
                    Log.i("justine", "doInBackground: returncode ="+returnCode);

                      //get the other data from server
                    if (returnCode==1)
                    {
                        Log.i("justine", "doInBackground: adding new data");

                            //add data
                            try {
                                URL url = new URL(Service.ENDPOINT);
                                urlConnection = (HttpURLConnection) url.openConnection();
                                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                                String line;
                                while ((line = reader.readLine()) != null) {
                                    result.append(line);
                                    Log.i("App", line + "---------------------------------");

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                urlConnection.disconnect();
                            }
                            Log.i("App", "fin background for j");

                            try {
                                Log.i("result string size", "doInBackground: " + result.toString().length()); //1060162
                                JSONObject jsonObj = new JSONObject(result.toString());
                                JSONArray jsonArray = jsonObj.getJSONArray("testData");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject explrObject = jsonArray.getJSONObject(i);
                                    String id_agronome = explrObject.getString("id_agronome_creation");
                                    String archive = explrObject.getString("archive");
                                    String categorie = explrObject.getString("categorie");
                                    String date_creation = explrObject.getString("date_creation");
                                    String date_modif = explrObject.getString("date_modification");
                                    String metier = explrObject.getString("metier");
                                    String numero_contact = explrObject.getString("numero_contact");
                                    String remarque = explrObject.getString("remarque");
                                    String contact_identifiant = explrObject.getString("contact_identifiant");
                                    String sociabilite = explrObject.getString("sociabilite");
                                    String adresse_mail = explrObject.getString("adresse_mail");
                                    String date_naissance = explrObject.getString("date_naissance");
                                    String nom = explrObject.getString("nom");
                                    String prenom = explrObject.getString("prenom");
                                    String sexe = explrObject.getString("sexe");
                                    String telephone_principal = explrObject.getString("telephone_principal");
                                    String telephone_secondaire = explrObject.getString("telephone_secondaire");
                                    String code_postale = explrObject.getString("code_postale");
                                    String complement_adresse = explrObject.getString("complement_adresse");
                                    String groupement = explrObject.getString("groupement");
                                    String numero_nom_rue = explrObject.getString("numero_nom_rue");
                                    String pays = explrObject.getString("pays");
                                    String region = explrObject.getString("region");
                                    String ville = explrObject.getString("ville");
                                    String zone = explrObject.getString("zone");
                                    String activite_principale = explrObject.getString("activite_principale");
                                    String activite_secondaire = explrObject.getString("activite_secondaire");
                                    String adresse_mail_principale = explrObject.getString("adresse_mail_principale");
                                    String adresse_mail_secondaire = explrObject.getString("adresse_mail_secondaire");
                                    String ancien = explrObject.getString("ancien");
                                    String biologique = explrObject.getString("biologique");
                                    String cause_arret = explrObject.getString("cause_arret");
                                    String fax = explrObject.getString("fax");
                                    String nombre_salarie = explrObject.getString("nombre_salarie");
                                    String projet_connu = explrObject.getString("projet_connu");
                                    String raison_sociale = explrObject.getString("raison_sociale");
                                    String siret = explrObject.getString("siret");
                                    String situation = explrObject.getString("situation");
                                    String statut_soytouch = explrObject.getString("statut_soytouch");
                                    String structure_sociale = explrObject.getString("structure_sociale");
                                    String telephone_entreprise = explrObject.getString("telephone_entreprise");
                                    String type_bio = explrObject.getString("type_bio");
                                    String compte_id = explrObject.getString("id_compte");   //get compte id
                                    long newcompteID = Long.parseLong(compte_id.trim());
                                    if (compte_id.equals(" ")) {
                                        newcompteID = Long.parseLong(compte_id.trim());
                                        Log.i("ID", "doInBackground: compte id=" + newcompteID);
                                    }

                                    //add to db
                                    DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
                                    CompteDao compte_dao = daoSession.getCompteDao();

                                    DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                    Date datecreation = sourceFormat.parse(date_creation);
                                    Date datemodification = sourceFormat.parse(date_modif);


                                    //insertion dans la base de données locale
                                    //existeContact retourne vrai si le contact n'existe pas
                                    if (newcompteID==1) {
                                        Log.i("justine", "doInBackground: compte id= "+newcompteID);
                                        if (alreadyExistContact(nom, prenom)) {
                                            if (nom != null)

                                            {
                                            //account check no j
                                                if ((ID_compte_server == 1)) {
                                                    // entreprise

                                                    Log.i("justine", datecreation.toString());
                                                    Log.i("justine", datemodification.toString());

                                                    Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
                                                    SituationDao situation_data = daoSession.getSituationDao();
                                                    Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
                                                    ActiviteDao activite_data = daoSession.getActiviteDao();
                                                    BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
                                                    Type_BioDao type_bio_data = daoSession.getType_BioDao();

                                                    AdresseDao adresse_data = daoSession.getAdresseDao();
                                                    Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
                                                    PaysDao pays_data = daoSession.getPaysDao();
                                                    RegionDao region_data = daoSession.getRegionDao();

                                                    EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

                                                    //Table Entreprise
                                                    Entreprise entreprise_input = new Entreprise();
                                                    entreprise_input.setRaison_sociale(raison_sociale);
//                        if (nombre_salarie != null) {
//                            entreprise_input.setNombre_salarie(Integer.parseInt(nombre_salarie));
//                        }

                                                    entreprise_input.setSIRET(siret);
                                                    entreprise_input.setProjet_connu(projet_connu);
                                                    entreprise_input.setTelephone_entreprise(telephone_entreprise);
                                                    entreprise_input.setFax_entreprise(fax);
                                                    entreprise_input.setAdresse_email_principale(adresse_mail_principale);
                                                    entreprise_input.setAdresse_email_secondaire(adresse_mail_secondaire);

                                                    entreprise_input.setArchive(Integer.parseInt(archive));
                                                    entreprise_input.setDate_creation(datecreation);
                                                    entreprise_input.setDate_modification(datemodification);
                                                    entreprise_input.setID_Agronome_Creation(1);
                                                    entreprise_input.setID_Agronome_Modification(1);
                                                    // entreprise_input.setID_Compte(Long.parseLong(compte_id));//set the compte id

                                                    //Ajout de la table Situation
                                                    List<Situation> List_Situation = situation_data.queryBuilder()
                                                            .where(SituationDao.Properties.Nom.eq(situation))
                                                            .list();
                                                    if (List_Situation != null) {
                                                        for (Situation S : List_Situation) {
                                                            entreprise_input.setSituation(S);
                                                            entreprise_input.setID_Situation(S.getId());
                                                        }
                                                    }

                                                    //Ajout de la table Structure Sociale
                                                    List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                                                            .where(Structure_SocialeDao.Properties.Nom.eq(structure_sociale))
                                                            .list();
                                                    if (List_Structure_Sociale != null) {
                                                        for (Structure_Sociale SS : List_Structure_Sociale) {
                                                            entreprise_input.setStructure_Sociale(SS);
                                                            entreprise_input.setID_Structure_Sociale(SS.getId());
                                                        }
                                                    }

                                                    //Ajout de la table Statut SoyTouch
                                                    List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                                                            .where(Statut_SoytouchDao.Properties.Nom.eq(statut_soytouch))
                                                            .list();
                                                    if (List_Statut != null) {
                                                        for (Statut_Soytouch Sst : List_Statut) {
                                                            entreprise_input.setStatut_Soytouch(Sst);
                                                            entreprise_input.setID_Statut_Soytouch(Sst.getId());
                                                        }
                                                    }

                                                    //Ajout de la table Activite Principale
                                                    List<Activite> List_Activite_P = activite_data.queryBuilder()
                                                            .where(ActiviteDao.Properties.Nom.eq(activite_principale))
                                                            .list();
                                                    if (List_Activite_P != null) {
                                                        for (Activite AP : List_Activite_P) {
                                                            entreprise_input.setActivite_Principale(AP);
                                                            entreprise_input.setID_Activite_Principale(AP.getId());
                                                        }
                                                    }

                                                    //Ajout de la table Activite Secondaire si renseignée
                                                    if (!(activite_principale.equals(getString(R.string.TextActivitePrincipale)))) {
                                                        List<Activite> List_Activite_S = activite_data.queryBuilder()
                                                                .where(ActiviteDao.Properties.Nom.eq(activite_secondaire))
                                                                .list();
                                                        if (List_Activite_S != null) {
                                                            for (Activite AS : List_Activite_S) {
                                                                entreprise_input.setActivite_Secondaire(AS);
                                                                entreprise_input.setID_Activite_Secondaire(AS.getId());
                                                            }
                                                        }
                                                    }


                                                    //Ajout de la table Biologique

                                                    List<Biologique> List_Bio = biologique_data.queryBuilder()
                                                            .where(BiologiqueDao.Properties.Nom.eq(biologique))
                                                            .list();
                                                    if (List_Bio != null) {
                                                        for (Biologique B : List_Bio) {
                                                            entreprise_input.setBiologique(B);
                                                            entreprise_input.setID_Biologique(B.getId());
                                                        }
                                                    }


                                                    entreprise_input.setArret_activite(true);
                                                    entreprise_input.setCause_arret_activite(cause_arret);
                                                    entreprise_input.setDate_arret_activite(Date_Arret);


                                                    //Adresse_siege_social = Adresse de l'entreprise
                                                    Adresse adresse_siege_social_input = new Adresse();
                                                    adresse_siege_social_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                                                    adresse_siege_social_input.setComplement_adresse(complement_adresse); //Complement Adresse
                                                    adresse_siege_social_input.setArchive(GlobalValues.getInstance().getGlobalArchive());    //Global Value archieve
                                                    adresse_siege_social_input.setDate_creation(datecreation);
                                                    adresse_siege_social_input.setDate_modification(datemodification);
                                                    adresse_siege_social_input.setID_Agronome_Creation(1);
                                                    adresse_siege_social_input.setID_Agronome_Modification(1);

                                                    //Code postal adresse siège sociale
                                                    Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

                                                    code_postal_ville_siege_input.setCode_postal(code_postale);

                                                    code_postal_ville_siege_input.setVille(ville);
                                                    code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

                                                    //Pays adresse siège
                                                    Pays pays_siege_input = new Pays();
                                                    pays_siege_input.setNom(pays);
                                                    pays_data.insertOrReplace(pays_siege_input);

                                                    //Région adresse siège
                                                    Region region_siege_input = new Region();
                                                    region_siege_input.setNom(region);
                                                    region_data.insertOrReplace(region_siege_input);

                                                    adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
                                                    adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
                                                    adresse_siege_social_input.setPays(pays_siege_input);
                                                    adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
                                                    adresse_siege_social_input.setRegion(region_siege_input);
                                                    adresse_siege_social_input.setID_Region(region_siege_input.getId());
                                                    adresse_data.insertOrReplace(adresse_siege_social_input);

                                                    entreprise_input.setAdresse_Siege(adresse_siege_social_input);
                                                    entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

                                                    if (false) {

                                                    } else {
                                                        entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
                                                        entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
                                                    }

                                                    if (false) {

                                                    } else {
                                                        entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
                                                        entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
                                                    }
                                                    //add data to enterprise table
                                                    entreprise_data.insertOrReplace(entreprise_input);


                                                    if ((!(type_bio.equals(getString(R.string.TextTypeBio))))) {

                                                        List<Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                                                                .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                                                                .list();
                                                        if (List_Entreprise_cible != null) {
                                                            for (Entreprise e : List_Entreprise_cible) {

                                                                List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                                                        .where(Type_BioDao.Properties.Nom.eq(type_bio))
                                                                        .list();
                                                                if (List_Type_Bio != null) {
                                                                    for (Type_Bio TB : List_Type_Bio) {
                                                                        TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                                                        //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                                                        type_bio_data.insertOrReplace(TB);
                                                                        e.update();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }


                                                    //Associer la nouvelle entreprise au compte
                                                    List<Compte> update_compte = compte_dao.queryBuilder()
                                                            .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                                            .list();
                                                    if (update_compte != null) {
                                                        for (Compte c : update_compte) {
                                                            entreprise_input.setID_Compte(c.getId());
                                                            entreprise_data.insertOrReplace(entreprise_input);
                                                            c.update();
                                                        }
                                                    }


                                                    ContactDao contact_data = daoSession.getContactDao();
                                                    Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
                                                    SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

                                                    IdentiteDao identite_data = daoSession.getIdentiteDao();
                                                    SexeDao sexe_data = daoSession.getSexeDao();


                                                    //Table Contact
                                                    com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
                                                    contact_input.setRemarque_contact(remarque);
                                                    contact_input.setContact_identifiant(contact_identifiant);
                                                    contact_input.setArchive(Integer.parseInt(archive));
                                                    contact_input.setDate_creation(datecreation);
                                                    contact_input.setDate_modification(datemodification);
                                                    contact_input.setID_Agronome_Creation(1);
                                                    contact_input.setID_Agronome_Modification(1);

                                                    //Ajout de la table Categorie
                                                    List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                                            .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
                                                            .list();
                                                    if (List_Categorie != null) {
                                                        for (Categorie_Contact CC : List_Categorie) {
                                                            contact_input.setCategorie_Contact(CC);
                                                            contact_input.setID_Categorie_Contact(CC.getId());
                                                        }
                                                    }

                                                    //Ajout de la table Sociabilite
                                                    List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                                            .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
                                                            .list();
                                                    if (List_Sociabilite != null) {
                                                        for (Sociabilite S : List_Sociabilite) {
                                                            contact_input.setSociabilite(S);
                                                            contact_input.setID_Sociabilite(S.getId());
                                                        }
                                                    }


                                                    //Table identité
                                                    Identite identite_input = new Identite();
                                                    identite_input.setNom(nom); //Nom
                                                    identite_input.setPrenom(prenom); //Prenom
                                                    identite_input.setPhoto("");
                                                    identite_input.setTelephone_principal(telephone_principal); //Telephone principale
                                                    identite_input.setTelephone_secondaire(telephone_secondaire); //Telephoe secondaire
                                                    identite_input.setAdresse_email(adresse_mail); //Mail 1
                                                    identite_input.setArchive(Integer.parseInt(archive));
                                                    identite_input.setDate_creation(datecreation);
                                                    identite_input.setDate_modification(datemodification);
                                                    identite_input.setID_Agronome_Creation(1);
                                                    identite_input.setID_Agronome_Modification(1);

                                                    Sexe sexe_input = new Sexe();

                                                    sexe_input.setNom(sexe);

                                                    sexe_data.insertOrReplace(sexe_input);


                                                    //Table Adresse
                                                    Adresse adresse_input = new Adresse();
                                                    adresse_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                                                    adresse_input.setComplement_adresse(complement_adresse); //Complement Adresse
                                                    adresse_input.setArchive(Integer.parseInt(archive));
                                                    adresse_input.setDate_creation(Today_Date());
                                                    adresse_input.setDate_modification(Today_Date());
                                                    adresse_input.setID_Agronome_Creation(1);
                                                    adresse_input.setID_Agronome_Modification(1);


                                                    Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
                                                    code_postal_ville_input.setCode_postal(code_postale);
                                                    code_postal_ville_input.setVille(ville);
                                                    code_postal_ville_data.insertOrReplace(code_postal_ville_input);

                                                    Region region_input = new Region();
                                                    region_input.setNom(region);
                                                    region_data.insertOrReplace(region_input);

                                                    Pays pays_input = new Pays();
                                                    pays_input.setNom(pays);
                                                    pays_data.insertOrReplace(pays_input);

                                                    //Cardinalités entre les tables :
                                                    adresse_input.setCode_Postal_Ville(code_postal_ville_input);
                                                    adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
                                                    adresse_input.setRegion(region_input);
                                                    adresse_input.setID_Region(region_input.getId());
                                                    adresse_input.setPays(pays_input);
                                                    adresse_input.setID_Pays(pays_input.getId());
                                                    adresse_data.insertOrReplace(adresse_input);


                                                    identite_input.setAdresse(adresse_input);
                                                    identite_input.setID_Adresse(adresse_input.getId());

                                                    identite_input.setSexe(sexe_input);
                                                    identite_input.setID_Sexe(sexe_input.getId());

                                                    identite_data.insertOrReplace(identite_input);
                                                    //Un contact possède une identité
                                                    contact_input.setIdentite(identite_input);
                                                    contact_input.setID_Identite_Contact(identite_input.getId());

                                                    //Ajout du contact à l'entreprise concernée
                                                    List<Compte> update_compte1 = compte_dao.queryBuilder()
                                                            .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                                            .list();
                                                    if (update_compte1 != null) {
                                                        for (Compte c : update_compte1) {
                                                            List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                                                            for (Entreprise e : List_Company) {
                                                                if (e.getRaison_sociale().equals(raison_sociale)) {
                                                                    contact_input.setID_Entreprise(e.getId());
                                                                    contact_data.insertOrReplace(contact_input);
                                                                    e.update();
                                                                }
                                                            }
                                                            c.update();
                                                        }
                                                    }


                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (ParseException e2) {
                                e2.printStackTrace();
                            } catch (JSONException e2) {
                                e2.printStackTrace();
                            }

                    }


                }

                if (ID_compte_server == 2) {

                      /*  Log.i(TAG, "doInBackground: writing data for Mathieu");
                        returnCode2=ad.addDataMathu();
                    Log.i("mathieu", "doInBackground: returncode ="+returnCode2);*/


                }
            }

            return result.toString();


        }


//        protected void onProgressUpdate(Integer... progress) {
//            // Update the progress
//            dialog.setProgress(progress[0]);
//        }


        @Override
        protected void onPostExecute(String result) {
            Log.i("App", "debut post");


            if (dialog != null && dialog.isShowing()) {

                dialog.dismiss();
            }

            //progressBar.setVisibility(View.GONE);

            //    dialog.dismiss();
//
        }

    }


    private boolean existeContact(String contact_identifiant,String nom1,String prenom1) {
        boolean b = true;
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {

                    List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                    for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                        if (ct.getContact_identifiant() != null) {
                            if (ct.getContact_identifiant().toString().trim().equals(contact_identifiant.trim())&&(ct.getIdentite().getNom().equals(nom1.trim())&&(ct.getIdentite().getPrenom().equals(prenom1.trim())))) {
                                b = false;
                                Log.i("doubleData", "existeContact:nom already exists ");

                            }
                            else{
                                Log.i("NotdoubleData", "existeContact:nom not already exists ");
                            }
                        }

                    }

                }
            }
        }
        return b;
    }

    //check if data already exist in contact
    public boolean alreadyExistContact(String nom, String prenom) {
        boolean b = true;
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {

                    List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                    if((List_Contact!=null))
                    {
                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                            if(ct.getIdentite().getNom()!=null)
                            {
                                if ((ct.getIdentite().getNom().toString().trim().equals(nom.trim())) && (ct.getIdentite().getPrenom().equals(prenom.trim()))) {
                                    Log.i("already", "alreadyExistContact: contact name=" + ct.getIdentite().getNom().toString() + " nom already=" + nom);
                                    b = false;
                                } else {
                                    //Log.i(TAG, "alreadyNOTExistContact: contact name=" + ct.getIdentite().getNom().toString() + " nom already=" + nom);
                                }
                            }

                        }
                    }
                }
            }

        }
        return b;
    }


    public class putData extends AsyncTask<String, String, String> {
        HttpURLConnection urlConnection;

        //        private ProgressDialog dialog;
//
//        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... args) {


            Log.i("App", "debut");


            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL(Service.ENDPOINT);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                    Log.i("App", line + "---------------------------------");

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            Log.i("App", "fin background");

            try {
                //JSONObject jsonObj = new JSONObject(result.toString());
                JSONObject jsonObj = new JSONObject(result.toString());
                JSONArray jsonArray = jsonObj.getJSONArray("testData");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject explrObject = jsonArray.getJSONObject(i);
                    String date_creation = explrObject.getString("date_creation");
                    String date_modif = explrObject.getString("date_modification");
                    String contact_identifiant = explrObject.getString("contact_identifiant");

                    DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    Date datemodification = sourceFormat.parse(date_modif);

                    DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
                    CompteDao compte_dao = daoSession.getCompteDao();
                    List<Compte> update_compte = compte_dao.queryBuilder()
                            .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                            .list();
                    if (update_compte != null) {
                        for (Compte c : update_compte) {
                            List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                            for (Entreprise e : List_Entreprise) {
                                List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                                for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                    Log.i("contact", ct.getContact_identifiant());
                                    JSONObject datapost = new JSONObject();
                                    datapost.put("raison_sociale", e.getRaison_sociale());
                                    datapost.put("archive", ct.getArchive());
                                    datapost.put("date_creation", ct.getDate_modification());
                                    datapost.put("date_modification", ct.getDate_creation());
                                    datapost.put("structure_sociale", e.getStructure_Sociale());
                                    try {
                                        // datapost.put("situation", e.getSituation().getNom());
                                    } catch (NullPointerException ne) {
                                        ne.printStackTrace();
                                        datapost.put("situation", "");
                                    }

                                    datapost.put("statut_soytouch", e.getStatut_Soytouch() != null ? e.getStatut_Soytouch().getNom():"");
                                    datapost.put("activite_principale", e.getActivite_Principale() != null ? e.getActivite_Principale().getNom():"");
                                    datapost.put("activite_secondaire", e.getActivite_Secondaire() != null ? e.getActivite_Secondaire().getNom():"");
                                    datapost.put("biologique", e.getBiologique() != null ? e.getBiologique().getNom():"");
                                    datapost.put("type_bio", e.getType_BioList());
                                    datapost.put("numero_nom_rue", e.getAdresse_Siege() != null ? e.getAdresse_Siege().getAdresse_numero_rue():"");
                                    datapost.put("code_postale",  e.getAdresse_Siege() != null ? e.getAdresse_Siege().getCode_Postal_Ville().getCode_postal():"");
                                    datapost.put("ville",  e.getAdresse_Siege() != null ? e.getAdresse_Siege().getCode_Postal_Ville().getVille():"");
                                    datapost.put("pays", e.getAdresse_Siege() != null ? e.getAdresse_Siege().getPays().getNom():"");
                                    datapost.put("telephone_entreprise", e.getTelephone_entreprise());
                                    datapost.put("fax", e.getFax_entreprise());
                                    datapost.put("adresse_mail_principale", e.getAdresse_email_principale());
                                    datapost.put("adresse_mail_secondaire", e.getAdresse_email_secondaire());
                                    String ct_id = ct.getContact_identifiant();

                                    datapost.put("nom", ct.getIdentite().getNom());
                                    datapost.put("prenom", ct.getIdentite().getPrenom());
                                    datapost.put("adresse_mail", ct.getIdentite().getAdresse_email());
                                    datapost.put("sexe", ct.getIdentite().getSexe().getNom());
                                    datapost.put("telephone_principale", ct.getIdentite().getTelephone_principal());
                                    datapost.put("telephone_secondaire", ct.getIdentite().getTelephone_secondaire());

                                    datapost.put("categorie", ct.getCategorie_Contact() != null ? ct.getCategorie_Contact().getNom():"");
                                    datapost.put("sociabilite", ct.getSociabilite() != null ? ct.getSociabilite().getNom():"");
                                    datapost.put("remarque", ct.getRemarque_contact());

                                    if (ct.getContact_identifiant() == null) {

                                        URL url = new URL(Service.ENDPOINT);

                                        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                                        httpCon.setDoOutput(true);
                                        httpCon.setDoInput(true);
                                        httpCon.setUseCaches(false);
                                        httpCon.setRequestProperty("Content-Type", "application/json");
                                        httpCon.setRequestProperty("Accept", "application/json");
                                        httpCon.setRequestMethod("POST");
                                        OutputStream os = httpCon.getOutputStream();
                                        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                                        osw.write(datapost.toString());
                                        Log.i("post", datapost.toString());
                                        osw.flush();
                                        osw.close();

                                    } else if (contact_identifiant.equals(ct.getContact_identifiant()) && comparedate(datemodification, ct.getDate_modification())) {
                                        URL url = new URL(Service.ENDPOINT + ct_id);

                                        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                                        httpCon.setDoOutput(true);
                                        httpCon.setDoInput(true);
                                        httpCon.setUseCaches(false);
                                        httpCon.setRequestProperty("Content-Type", "application/json");
                                        httpCon.setRequestProperty("Accept", "application/json");
                                        httpCon.setRequestMethod("PUT");
                                        OutputStream os = httpCon.getOutputStream();
                                        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                                        osw.write(datapost.toString());
                                        Log.i("PUT", datapost.toString());
                                        osw.flush();
                                        osw.close();

                                    }


                                }
                            }
                        }
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result.toString();


        }

        @Override
        protected void onPostExecute(String result) {
            Log.i("App", "debut post");


        }


    }


    private long getIdEntreprise(String Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        long result = 0;
        List<Compte> list_account = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_account != null) {
            for (Compte c : list_account) {
                List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                for (Entreprise e : List_Company) {
                    if (e.getRaison_sociale().equals(Entreprise)) {
                        result = e.getId();
                    }
                }
            }
        }
        return result;
    }


    public boolean comparedate(Date date1, Date date2) {
        boolean b = true;
        if (date1.compareTo(date2) == 0) {
            b = false;
        }
        return b;
    }

    //add new data into DB
    public class AddDataForAccounts {
        //String name = null;
        //String fname = null;
        long idEnterprise;

        //add Data for justine
        public int addDataJustine() {

            //for justine
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();

            name ="Armingaud";//double contact
            fname="Guillaume";

            DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            Log.i("CONTACTMENU", " Reading data from file");
            InputStreamReader in;
            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            String[] csv = new String[1000];
            AssetManager assetManager = getApplicationContext().getAssets();

            int i = 1;
            try {
                in = new InputStreamReader(assetManager.open("justine.csv"));
                br = new BufferedReader(in);
                Log.i("ContactMenu", "addData: Tying to read csv file");
                while ((line = br.readLine()) != null) {
                    csv = line.split(cvsSplitBy, 33);

                    Log.i("ContactMenu", "addData: Reading Data" + csv.toString());

                    if (i != 1) {
                        String arch = "0";
                        String archive = arch;
                        Date date_creation = GlobalValues.Today_Date();
                        Date date_modif = GlobalValues.Today_Date();
                        String nom = csv[18];
                        String prenom = csv[19];
                        String code_postale = csv[10];
                        String raison_sociale = csv[1];
                        String statut_soytouch = csv[4];
                        String telephone_entreprise = csv[13];
                        Date datecreation = date_creation;
                        Date datemodification = date_modif;
                        //empty values
                        String siret = " ";
                        String projet_connu = " ";
                        String fax = " ";
                        String adresse_mail_principale = " ";
                        String adresse_mail_secondaire = " ";
                        String situation = " ";
                        String structure_sociale = " ";
                        String ville = " ";
                        String pays = " ";
                        String region = " ";
                        String type_bio = " ";
                        String sociabilite = " ";
                        String telephone_principal = " ";
                        String telephone_secondaire = " ";
                        String adresse_mail = " ";
                        String activite_principale = " ";
                        String activite_secondaire = " ";
                        String biologique = " ";
                        String cause_arret = " ";
                        String numero_ncomplement_adresseom_rue = " ";
                        String numero_nom_rue = " ";
                        String sexe = " ";
                        String categorie = " ";
                        String remarque = " ";
                        String complement_adresse = " ";

                        //dont add same value
                        name = nom;
                        fname = prenom;


                        Log.i("CSV Data", "doInBackground:" + csv[0].toString() + "   " + csv[1].toString() + " " + csv[9] + " " + csv[10] + " " + csv[13] + " " + csv[18] + " " + csv[19]);  // for justine

                        // entreprise

                        Log.i("CREATION", "writing data into DB");
                        Log.i("CSV data", "ADD_DATA: getting data from csv");

                        //check exist
                        if (alreadyExistContact(name, fname)) {
                            Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
                            SituationDao situation_data = daoSession.getSituationDao();
                            Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
                            ActiviteDao activite_data = daoSession.getActiviteDao();
                            BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
                            Type_BioDao type_bio_data = daoSession.getType_BioDao();

                            AdresseDao adresse_data = daoSession.getAdresseDao();
                            Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
                            PaysDao pays_data = daoSession.getPaysDao();
                            RegionDao region_data = daoSession.getRegionDao();

                            EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

                            //Table Entreprise
                            Entreprise entreprise_input = new Entreprise();
                            entreprise_input.setRaison_sociale(raison_sociale);
                            //                        if (nombre_salarie != null) {
                            //                            entreprise_input.setNombre_salarie(Integer.parseInt(nombre_salarie));
                            //                        }

                            entreprise_input.setSIRET(siret);
                            entreprise_input.setProjet_connu(projet_connu);
                            entreprise_input.setTelephone_entreprise(telephone_entreprise);
                            entreprise_input.setFax_entreprise(fax);
                            entreprise_input.setAdresse_email_principale(adresse_mail_principale);
                            entreprise_input.setAdresse_email_secondaire(adresse_mail_secondaire);

                            entreprise_input.setArchive(Integer.parseInt(archive));
                            entreprise_input.setDate_creation(datecreation);
                            entreprise_input.setDate_modification(datemodification);
                            entreprise_input.setID_Agronome_Creation(1);
                            entreprise_input.setID_Agronome_Modification(1);
                            entreprise_input.setID_Compte(ID_Compte_Selected);//set the compte id

                            //Ajout de la table Situation
                            List<Situation> List_Situation = situation_data.queryBuilder()
                                    .where(SituationDao.Properties.Nom.eq(situation))
                                    .list();
                            if (List_Situation != null) {
                                for (Situation S : List_Situation) {
                                    entreprise_input.setSituation(S);
                                    entreprise_input.setID_Situation(S.getId());
                                }
                            }

                            //Ajout de la table Structure Sociale
                            List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                                    .where(Structure_SocialeDao.Properties.Nom.eq(structure_sociale))
                                    .list();
                            if (List_Structure_Sociale != null) {
                                for (Structure_Sociale SS : List_Structure_Sociale) {
                                    entreprise_input.setStructure_Sociale(SS);
                                    entreprise_input.setID_Structure_Sociale(SS.getId());
                                }
                            }

                            //Ajout de la table Statut SoyTouch
                            List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                                    .where(Statut_SoytouchDao.Properties.Nom.eq(statut_soytouch))
                                    .list();
                            if (List_Statut != null) {
                                for (Statut_Soytouch Sst : List_Statut) {
                                    entreprise_input.setStatut_Soytouch(Sst);
                                    entreprise_input.setID_Statut_Soytouch(Sst.getId());
                                }
                            }

                            //Ajout de la table Activite Principale
                            List<Activite> List_Activite_P = activite_data.queryBuilder()
                                    .where(ActiviteDao.Properties.Nom.eq(activite_principale))
                                    .list();
                            if (List_Activite_P != null) {
                                for (Activite AP : List_Activite_P) {
                                    entreprise_input.setActivite_Principale(AP);
                                    entreprise_input.setID_Activite_Principale(AP.getId());
                                }
                            }

                            //Ajout de la table Activite Secondaire si renseignée
                            if (!(activite_principale.equals(getString(R.string.TextActivitePrincipale)))) {
                                List<Activite> List_Activite_S = activite_data.queryBuilder()
                                        .where(ActiviteDao.Properties.Nom.eq(activite_secondaire))
                                        .list();
                                if (List_Activite_S != null) {
                                    for (Activite AS : List_Activite_S) {
                                        entreprise_input.setActivite_Secondaire(AS);
                                        entreprise_input.setID_Activite_Secondaire(AS.getId());
                                    }
                                }
                            }


                            //Ajout de la table Biologique

                            List<Biologique> List_Bio = biologique_data.queryBuilder()
                                    .where(BiologiqueDao.Properties.Nom.eq(biologique))
                                    .list();
                            if (List_Bio != null) {
                                for (Biologique B : List_Bio) {
                                    entreprise_input.setBiologique(B);
                                    entreprise_input.setID_Biologique(B.getId());
                                }
                            }


                            entreprise_input.setArret_activite(true);
                            entreprise_input.setCause_arret_activite(cause_arret);
                            entreprise_input.setDate_arret_activite(Date_Arret);


                            //Adresse_siege_social = Adresse de l'entreprise
                            Adresse adresse_siege_social_input = new Adresse();
                            adresse_siege_social_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                            adresse_siege_social_input.setComplement_adresse(complement_adresse); //Complement Adresse
                            adresse_siege_social_input.setArchive(GlobalValues.getInstance().getGlobalArchive());    //Global Value archieve
                            adresse_siege_social_input.setDate_creation(datecreation);
                            adresse_siege_social_input.setDate_modification(datemodification);
                            adresse_siege_social_input.setID_Agronome_Creation(1);
                            adresse_siege_social_input.setID_Agronome_Modification(1);

                            //Code postal adresse siège sociale
                            Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

                            code_postal_ville_siege_input.setCode_postal(code_postale);

                            code_postal_ville_siege_input.setVille(ville);
                            code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

                            //Pays adresse siège
                            Pays pays_siege_input = new Pays();
                            pays_siege_input.setNom(pays);
                            pays_data.insertOrReplace(pays_siege_input);

                            //Région adresse siège
                            Region region_siege_input = new Region();
                            region_siege_input.setNom(region);
                            region_data.insertOrReplace(region_siege_input);

                            adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
                            adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
                            adresse_siege_social_input.setPays(pays_siege_input);
                            adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
                            adresse_siege_social_input.setRegion(region_siege_input);
                            adresse_siege_social_input.setID_Region(region_siege_input.getId());
                            adresse_data.insertOrReplace(adresse_siege_social_input);

                            entreprise_input.setAdresse_Siege(adresse_siege_social_input);
                            entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

                            if (false) {
                                //Adresse_facturation
                                //                        Adresse adresse_facturation_input = new Adresse();
                                //                        adresse_facturation_input.setAdresse_numero_rue(Adresse_facturation_1.getText().toString().trim()); //Adresse
                                //                        adresse_facturation_input.setComplement_adresse(Adresse_facturation_2.getText().toString().trim()); //Complement Adresse
                                //                        adresse_facturation_input.setArchive(1);
                                //                        adresse_facturation_input.setDate_creation(Today_Date());
                                //                        adresse_facturation_input.setDate_modification(Today_Date());
                                //                        adresse_facturation_input.setID_Agronome_Creation(1);
                                //                        adresse_facturation_input.setID_Agronome_Modification(1);
                                //
                                //                        //Code postal adresse facturation
                                //                        Code_Postal_Ville code_postal_ville_facturation_input = new Code_Postal_Ville();
                                //                        code_postal_ville_facturation_input.setCode_postal(CodePostal_facturation.getText().toString().trim());
                                //                        code_postal_ville_facturation_input.setVille(Ville_facturation.getText().toString().trim());
                                //                        code_postal_ville_data.insertOrReplace(code_postal_ville_facturation_input);
                                //
                                //                        //Pays adresse facturation
                                //                        Pays pays_facturation_input = new Pays();
                                //                        pays_facturation_input.setNom(Pays_facturation.getText().toString().trim());
                                //                        pays_data.insertOrReplace(pays_facturation_input);
                                //
                                //                        //Région adresse facturation
                                //                        Region region_facturation_input = new Region();
                                //                        region_facturation_input.setNom(Region_facturation.getText().toString().trim());
                                //                        region_data.insertOrReplace(region_facturation_input);
                                //
                                //                        adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_facturation_input);
                                //                        adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_facturation_input.getId());
                                //                        adresse_facturation_input.setPays(pays_facturation_input);
                                //                        adresse_facturation_input.setID_Pays(pays_facturation_input.getId());
                                //                        adresse_facturation_input.setRegion(region_facturation_input);
                                //                        adresse_facturation_input.setID_Region(region_facturation_input.getId());
                                //                        adresse_data.insertOrReplace(adresse_facturation_input);
                                //
                                //                        entreprise_input.setAdresse_Facturation(adresse_facturation_input);
                                //                        entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
                            } else {
                                entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
                                entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
                            }

                            if (false) {
                                //Adresse livraison
                                //                        Adresse adresse_livraison_input = new Adresse();
                                //                        adresse_livraison_input.setAdresse_numero_rue(Adresse_livraison_1.getText().toString().trim()); //Adresse
                                //                        adresse_livraison_input.setComplement_adresse(Adresse_livraison_2.getText().toString().trim()); //Complement Adresse
                                //                        adresse_livraison_input.setArchive(1);
                                //                        adresse_livraison_input.setDate_creation(Today_Date());
                                //                        adresse_livraison_input.setDate_modification(Today_Date());
                                //                        adresse_livraison_input.setID_Agronome_Creation(1);
                                //                        adresse_livraison_input.setID_Agronome_Modification(1);
                                //
                                //                        //Code postal adresse livraison
                                //                        Code_Postal_Ville code_postal_ville_livraison_input = new Code_Postal_Ville();
                                //                        code_postal_ville_livraison_input.setCode_postal(CodePostal_livraison.getText().toString().trim());
                                //                        code_postal_ville_livraison_input.setVille(Ville_livraison.getText().toString().trim());
                                //                        code_postal_ville_data.insertOrReplace(code_postal_ville_livraison_input);
                                //
                                //                        //Pays adresse livraison
                                //                        Pays pays_livraison_input = new Pays();
                                //                        pays_livraison_input.setNom(Pays_livraison.getText().toString().trim());
                                //                        pays_data.insertOrReplace(pays_livraison_input);
                                //
                                //                        //Région adresse livraison
                                //                        Region region_livraison_input = new Region();
                                //                        region_livraison_input.setNom(Region_livraison.getText().toString().trim());
                                //                        region_data.insertOrReplace(region_livraison_input);
                                //
                                //                        adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_livraison_input);
                                //                        adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_livraison_input.getId());
                                //                        adresse_livraison_input.setPays(pays_livraison_input);
                                //                        adresse_livraison_input.setID_Pays(pays_livraison_input.getId());
                                //                        adresse_livraison_input.setRegion(region_livraison_input);
                                //                        adresse_livraison_input.setID_Region(region_livraison_input.getId());
                                //                        adresse_data.insertOrReplace(adresse_livraison_input);
                                //
                                //                        entreprise_input.setAdresse_Livraison(adresse_livraison_input);
                                //                        entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
                            } else {
                                entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
                                entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
                            }
                            //add data to enterprise table
                            entreprise_data.insertOrReplace(entreprise_input);


                            if ((!(type_bio.equals(getString(R.string.TextTypeBio))))) {

                                List<Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                                        .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                                        .list();
                                if (List_Entreprise_cible != null) {
                                    for (Entreprise e : List_Entreprise_cible) {

                                        List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                                .where(Type_BioDao.Properties.Nom.eq(type_bio))
                                                .list();
                                        if (List_Type_Bio != null) {
                                            for (Type_Bio TB : List_Type_Bio) {
                                                TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                                //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                                type_bio_data.insertOrReplace(TB);
                                                e.update();
                                            }
                                        }
                                    }
                                }
                            }


                            //Associer la nouvelle entreprise au compte
                            List<Compte> update_compte = compte_dao.queryBuilder()
                                    .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                    .list();
                            if (update_compte != null) {
                                for (Compte c : update_compte) {
                                    entreprise_input.setID_Compte(c.getId());
                                    idEnterprise=entreprise_data.insertOrReplace(entreprise_input);
                                    c.update();
                                }
                            }


                            ContactDao contact_data = daoSession.getContactDao();
                            Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
                            SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

                            IdentiteDao identite_data = daoSession.getIdentiteDao();
                            SexeDao sexe_data = daoSession.getSexeDao();


                            //Table Contact
                            com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
                            contact_input.setRemarque_contact(remarque);
                            // contact_input.setContact_identifiant(contact_identifiant);
                            contact_input.setArchive(Integer.parseInt(archive));
                            contact_input.setDate_creation(datecreation);
                            contact_input.setDate_modification(datemodification);
                            contact_input.setID_Agronome_Creation(1);
                            contact_input.setID_Agronome_Modification(1);

                            //Ajout de la table Categorie
                            List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                    .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
                                    .list();
                            if (List_Categorie != null) {
                                for (Categorie_Contact CC : List_Categorie) {
                                    contact_input.setCategorie_Contact(CC);
                                    contact_input.setID_Categorie_Contact(CC.getId());
                                }
                            }

                            //Ajout de la table Sociabilite
                            List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                    .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
                                    .list();
                            if (List_Sociabilite != null) {
                                for (Sociabilite S : List_Sociabilite) {
                                    contact_input.setSociabilite(S);
                                    contact_input.setID_Sociabilite(S.getId());
                                }
                            }


                            //Table identité
                            Identite identite_input = new Identite();
                            identite_input.setNom(nom); //Nom
                            identite_input.setPrenom(prenom); //Prenom
                            identite_input.setPhoto("");
                            identite_input.setTelephone_principal(telephone_principal); //Telephone principale
                            identite_input.setTelephone_secondaire(telephone_secondaire); //Telephoe secondaire
                            identite_input.setAdresse_email(adresse_mail); //Mail 1
                            identite_input.setArchive(Integer.parseInt(archive));
                            identite_input.setDate_creation(datecreation);
                            identite_input.setDate_modification(datemodification);
                            identite_input.setID_Agronome_Creation(1);
                            identite_input.setID_Agronome_Modification(1);

                            Sexe sexe_input = new Sexe();

                            sexe_input.setNom(sexe);

                            sexe_data.insertOrReplace(sexe_input);


                            //Gestion de la photo
                            //                    if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            //                        //Photo basse qualité
                            //                        File myDir_Source1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                            //                        File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");
                            //
                            //                        File myDir_Dest1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                            //                        File photoFile_Dest1 = new File(myDir_Dest1, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                            //                        try {
                            //                            CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
                            //                        } catch (IOException e) {
                            //                            e.printStackTrace();
                            //                        }
                            //                        //Photo haute qualité
                            //                        File myDir_Source2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                            //                        File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");
                            //
                            //                        File myDir_Dest2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                            //                        File photoFile_Dest2 = new File(myDir_Dest2, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture_HD.jpg");
                            //                        try {
                            //                            CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
                            //                        } catch (IOException e) {
                            //                            e.printStackTrace();
                            //                        }
                            //
                            //                        identite_input.setPhoto(Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                            //                    }
                            //                    else{
                            //
                            //                        identite_input.setPhoto("");
                            //                    }

                            //Table Adresse
                            Adresse adresse_input = new Adresse();
                            adresse_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                            adresse_input.setComplement_adresse(complement_adresse); //Complement Adresse
                            adresse_input.setArchive(Integer.parseInt(archive));
                            adresse_input.setDate_creation(Today_Date());
                            adresse_input.setDate_modification(Today_Date());
                            adresse_input.setID_Agronome_Creation(1);
                            adresse_input.setID_Agronome_Modification(1);


                            Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
                            code_postal_ville_input.setCode_postal(code_postale);
                            code_postal_ville_input.setVille(ville);
                            code_postal_ville_data.insertOrReplace(code_postal_ville_input);

                            Region region_input = new Region();
                            region_input.setNom(region);
                            region_data.insertOrReplace(region_input);

                            Pays pays_input = new Pays();
                            pays_input.setNom(pays);
                            pays_data.insertOrReplace(pays_input);

                            //Cardinalités entre les tables :
                            adresse_input.setCode_Postal_Ville(code_postal_ville_input);
                            adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
                            adresse_input.setRegion(region_input);
                            adresse_input.setID_Region(region_input.getId());
                            adresse_input.setPays(pays_input);
                            adresse_input.setID_Pays(pays_input.getId());
                            adresse_data.insertOrReplace(adresse_input);


                            identite_input.setAdresse(adresse_input);
                            identite_input.setID_Adresse(adresse_input.getId());

                            identite_input.setSexe(sexe_input);
                            identite_input.setID_Sexe(sexe_input.getId());

                            identite_data.insertOrReplace(identite_input);
                            //Un contact possède une identité
                            contact_input.setIdentite(identite_input);
                            contact_input.setID_Identite_Contact(identite_input.getId());

                            //Ajout du contact à l'entreprise concernée
                            List<Compte> update_compte1 = compte_dao.queryBuilder()
                                    .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                    .list();
                            if (update_compte1 != null) {
                                for (Compte c : update_compte1) {
                                    List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                                    for (Entreprise e : List_Company) {
                                        if (e.getRaison_sociale().equals(raison_sociale)) {
                                            contact_input.setID_Entreprise(e.getId());
                                           // contact_input.setId(idEnterprise);
                                            contact_data.insertOrReplace(contact_input);
                                            e.update();
                                        }
                                    }
                                    c.update();
                                }
                            }

                        }
                    }
                    i++;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            executeCountJust++;
            return 1;

        }

        //add data for mathieu
        public int addDataMathu() {
            //for justine
            //for justine and mathieu
            Log.i("CONTACTMENU", "addDataMathu: writing data for Mathieu");
            nameM="CERA";
            fnameM="Louis";

            //get data from file
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();

            DateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            Log.i("file", "doInBackground: Reading  mathieu");
            InputStreamReader in;
            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            String[] csv = new String[1000];
            AssetManager assetManager = getApplicationContext().getAssets();

            int i = 1;
            try {
                in = new InputStreamReader(assetManager.open("mathieu.csv"));
                br = new BufferedReader(in);
                Log.i("ContactMenu", "addData: Tying to read csv file");
                while ((line = br.readLine()) != null) {
                    csv = line.split(cvsSplitBy, 33);

                    Log.i("ContactMenu", "addData: Reading Data" + csv.toString());

                    if (i != 1) {
                        String arch = "0";
                        String archive = arch;
                        Date date_creation = GlobalValues.Today_Date();
                        Date date_modif = GlobalValues.Today_Date();

                        String raison_sociale = csv[1];
                        String code_postale = csv[11];
                        String statut_soytouch = csv[5];
                        String telephone_entreprise = csv[14];
                        String email_principal = csv[16];
                        String address_principal = csv[10];
                        String nom = csv[19];
                        String prenom = csv[20];

                        Date datecreation = date_creation;
                        Date datemodification = date_modif;
                        //empty values
                        String siret = " ";
                        String projet_connu = " ";
                        String fax = " ";
                        String adresse_mail_principale = " ";
                        String adresse_mail_secondaire = " ";
                        String situation = " ";
                        String structure_sociale = " ";
                        String ville = " ";
                        String pays = " ";
                        String region = " ";
                        String type_bio = " ";
                        String sociabilite = " ";
                        String telephone_principal = " ";
                        String telephone_secondaire = " ";
                        String adresse_mail = " ";
                        String activite_principale = " ";
                        String activite_secondaire = " ";
                        String biologique = " ";
                        String cause_arret = " ";
                        String numero_ncomplement_adresseom_rue = " ";
                        String numero_nom_rue = " ";
                        String sexe = " ";
                        String categorie = " ";
                        String remarque = " ";
                        String complement_adresse = " ";

                        //check dublicate
                        nameM = nom;
                        fnameM = prenom;
                        //
                        // executeCountMathu++;


                        Log.i("CSV Data", "doInBackground:" + csv[0].toString() + "   " + csv[1].toString() + " " + csv[5] + " " + csv[10] + " " + csv[11] + " " + csv[12] + " " + csv[14] + " " + csv[16] + " " + csv[19] + csv[20]);  // for justine

                        // entreprise

                        Log.i("CREATION", "writing data into DB");
                        Log.i("CSV data", "doInBackground: getting data from csv");

                        //check

                        if (alreadyExistContact(name, fname)) {
                            Structure_SocialeDao structure_sociale_data = daoSession.getStructure_SocialeDao();
                            SituationDao situation_data = daoSession.getSituationDao();
                            Statut_SoytouchDao statut_soytouch_data = daoSession.getStatut_SoytouchDao();
                            ActiviteDao activite_data = daoSession.getActiviteDao();
                            BiologiqueDao biologique_data = daoSession.getBiologiqueDao();
                            Type_BioDao type_bio_data = daoSession.getType_BioDao();

                            AdresseDao adresse_data = daoSession.getAdresseDao();
                            Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
                            PaysDao pays_data = daoSession.getPaysDao();
                            RegionDao region_data = daoSession.getRegionDao();

                            EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();

                            //Table Entreprise
                            Entreprise entreprise_input = new Entreprise();
                            entreprise_input.setRaison_sociale(raison_sociale);
                            //                        if (nombre_salarie != null) {
                            //                            entreprise_input.setNombre_salarie(Integer.parseInt(nombre_salarie));
                            //                        }

                            entreprise_input.setSIRET(siret);
                            entreprise_input.setProjet_connu(projet_connu);
                            entreprise_input.setTelephone_entreprise(telephone_entreprise);
                            entreprise_input.setFax_entreprise(fax);
                            entreprise_input.setAdresse_email_principale(adresse_mail_principale);
                            entreprise_input.setAdresse_email_secondaire(adresse_mail_secondaire);

                            entreprise_input.setArchive(Integer.parseInt(archive));
                            entreprise_input.setDate_creation(datecreation);
                            entreprise_input.setDate_modification(datemodification);
                            entreprise_input.setID_Agronome_Creation(1);
                            entreprise_input.setID_Agronome_Modification(1);
                            entreprise_input.setID_Compte(ID_Compte_Selected);//set the compte id

                            //Ajout de la table Situation
                            List<Situation> List_Situation = situation_data.queryBuilder()
                                    .where(SituationDao.Properties.Nom.eq(situation))
                                    .list();
                            if (List_Situation != null) {
                                for (Situation S : List_Situation) {
                                    entreprise_input.setSituation(S);
                                    entreprise_input.setID_Situation(S.getId());
                                }
                            }

                            //Ajout de la table Structure Sociale
                            List<Structure_Sociale> List_Structure_Sociale = structure_sociale_data.queryBuilder()
                                    .where(Structure_SocialeDao.Properties.Nom.eq(structure_sociale))
                                    .list();
                            if (List_Structure_Sociale != null) {
                                for (Structure_Sociale SS : List_Structure_Sociale) {
                                    entreprise_input.setStructure_Sociale(SS);
                                    entreprise_input.setID_Structure_Sociale(SS.getId());
                                }
                            }

                            //Ajout de la table Statut SoyTouch
                            List<Statut_Soytouch> List_Statut = statut_soytouch_data.queryBuilder()
                                    .where(Statut_SoytouchDao.Properties.Nom.eq(statut_soytouch))
                                    .list();
                            if (List_Statut != null) {
                                for (Statut_Soytouch Sst : List_Statut) {
                                    entreprise_input.setStatut_Soytouch(Sst);
                                    entreprise_input.setID_Statut_Soytouch(Sst.getId());
                                }
                            }

                            //Ajout de la table Activite Principale
                            List<Activite> List_Activite_P = activite_data.queryBuilder()
                                    .where(ActiviteDao.Properties.Nom.eq(activite_principale))
                                    .list();
                            if (List_Activite_P != null) {
                                for (Activite AP : List_Activite_P) {
                                    entreprise_input.setActivite_Principale(AP);
                                    entreprise_input.setID_Activite_Principale(AP.getId());
                                    entreprise_input.setID_Activite_Principale(AP.getId());
                                }
                            }

                            //Ajout de la table Activite Secondaire si renseignée
                            if (!(activite_principale.equals(getString(R.string.TextActivitePrincipale)))) {
                                List<Activite> List_Activite_S = activite_data.queryBuilder()
                                        .where(ActiviteDao.Properties.Nom.eq(activite_secondaire))
                                        .list();
                                if (List_Activite_S != null) {
                                    for (Activite AS : List_Activite_S) {
                                        entreprise_input.setActivite_Secondaire(AS);
                                        entreprise_input.setID_Activite_Secondaire(AS.getId());
                                    }
                                }
                            }


                            //Ajout de la table Biologique

                            List<Biologique> List_Bio = biologique_data.queryBuilder()
                                    .where(BiologiqueDao.Properties.Nom.eq(biologique))
                                    .list();
                            if (List_Bio != null) {
                                for (Biologique B : List_Bio) {
                                    entreprise_input.setBiologique(B);
                                    entreprise_input.setID_Biologique(B.getId());
                                }
                            }


                            entreprise_input.setArret_activite(true);
                            entreprise_input.setCause_arret_activite(cause_arret);
                            entreprise_input.setDate_arret_activite(Date_Arret);


                            //Adresse_siege_social = Adresse de l'entreprise
                            Adresse adresse_siege_social_input = new Adresse();
                            adresse_siege_social_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                            adresse_siege_social_input.setComplement_adresse(complement_adresse); //Complement Adresse
                            adresse_siege_social_input.setArchive(GlobalValues.getInstance().getGlobalArchive());    //Global Value archieve
                            adresse_siege_social_input.setDate_creation(datecreation);
                            adresse_siege_social_input.setDate_modification(datemodification);
                            adresse_siege_social_input.setID_Agronome_Creation(1);
                            adresse_siege_social_input.setID_Agronome_Modification(1);

                            //Code postal adresse siège sociale
                            Code_Postal_Ville code_postal_ville_siege_input = new Code_Postal_Ville();

                            code_postal_ville_siege_input.setCode_postal(code_postale);

                            code_postal_ville_siege_input.setVille(ville);
                            code_postal_ville_data.insertOrReplace(code_postal_ville_siege_input);

                            //Pays adresse siège
                            Pays pays_siege_input = new Pays();
                            pays_siege_input.setNom(pays);
                            pays_data.insertOrReplace(pays_siege_input);

                            //Région adresse siège
                            Region region_siege_input = new Region();
                            region_siege_input.setNom(region);
                            region_data.insertOrReplace(region_siege_input);

                            adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_siege_input);
                            adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_siege_input.getId());
                            adresse_siege_social_input.setPays(pays_siege_input);
                            adresse_siege_social_input.setID_Pays(pays_siege_input.getId());
                            adresse_siege_social_input.setRegion(region_siege_input);
                            adresse_siege_social_input.setID_Region(region_siege_input.getId());
                            adresse_data.insertOrReplace(adresse_siege_social_input);

                            entreprise_input.setAdresse_Siege(adresse_siege_social_input);
                            entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());

                            if (false) {
                                //Adresse_facturation
                                //                        Adresse adresse_facturation_input = new Adresse();
                                //                        adresse_facturation_input.setAdresse_numero_rue(Adresse_facturation_1.getText().toString().trim()); //Adresse
                                //                        adresse_facturation_input.setComplement_adresse(Adresse_facturation_2.getText().toString().trim()); //Complement Adresse
                                //                        adresse_facturation_input.setArchive(1);
                                //                        adresse_facturation_input.setDate_creation(Today_Date());
                                //                        adresse_facturation_input.setDate_modification(Today_Date());
                                //                        adresse_facturation_input.setID_Agronome_Creation(1);
                                //                        adresse_facturation_input.setID_Agronome_Modification(1);
                                //
                                //                        //Code postal adresse facturation
                                //                        Code_Postal_Ville code_postal_ville_facturation_input = new Code_Postal_Ville();
                                //                        code_postal_ville_facturation_input.setCode_postal(CodePostal_facturation.getText().toString().trim());
                                //                        code_postal_ville_facturation_input.setVille(Ville_facturation.getText().toString().trim());
                                //                        code_postal_ville_data.insertOrReplace(code_postal_ville_facturation_input);
                                //
                                //                        //Pays adresse facturation
                                //                        Pays pays_facturation_input = new Pays();
                                //                        pays_facturation_input.setNom(Pays_facturation.getText().toString().trim());
                                //                        pays_data.insertOrReplace(pays_facturation_input);
                                //
                                //                        //Région adresse facturation
                                //                        Region region_facturation_input = new Region();
                                //                        region_facturation_input.setNom(Region_facturation.getText().toString().trim());
                                //                        region_data.insertOrReplace(region_facturation_input);
                                //
                                //                        adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_facturation_input);
                                //                        adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_facturation_input.getId());
                                //                        adresse_facturation_input.setPays(pays_facturation_input);
                                //                        adresse_facturation_input.setID_Pays(pays_facturation_input.getId());
                                //                        adresse_facturation_input.setRegion(region_facturation_input);
                                //                        adresse_facturation_input.setID_Region(region_facturation_input.getId());
                                //                        adresse_data.insertOrReplace(adresse_facturation_input);
                                //
                                //                        entreprise_input.setAdresse_Facturation(adresse_facturation_input);
                                //                        entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
                            } else {
                                entreprise_input.setAdresse_Facturation(adresse_siege_social_input);
                                entreprise_input.setID_Adresse_Facturation(adresse_siege_social_input.getId());
                            }

                            if (false) {
                                //Adresse livraison
                                //                        Adresse adresse_livraison_input = new Adresse();
                                //                        adresse_livraison_input.setAdresse_numero_rue(Adresse_livraison_1.getText().toString().trim()); //Adresse
                                //                        adresse_livraison_input.setComplement_adresse(Adresse_livraison_2.getText().toString().trim()); //Complement Adresse
                                //                        adresse_livraison_input.setArchive(1);
                                //                        adresse_livraison_input.setDate_creation(Today_Date());
                                //                        adresse_livraison_input.setDate_modification(Today_Date());
                                //                        adresse_livraison_input.setID_Agronome_Creation(1);
                                //                        adresse_livraison_input.setID_Agronome_Modification(1);
                                //
                                //                        //Code postal adresse livraison
                                //                        Code_Postal_Ville code_postal_ville_livraison_input = new Code_Postal_Ville();
                                //                        code_postal_ville_livraison_input.setCode_postal(CodePostal_livraison.getText().toString().trim());
                                //                        code_postal_ville_livraison_input.setVille(Ville_livraison.getText().toString().trim());
                                //                        code_postal_ville_data.insertOrReplace(code_postal_ville_livraison_input);
                                //
                                //                        //Pays adresse livraison
                                //                        Pays pays_livraison_input = new Pays();
                                //                        pays_livraison_input.setNom(Pays_livraison.getText().toString().trim());
                                //                        pays_data.insertOrReplace(pays_livraison_input);
                                //
                                //                        //Région adresse livraison
                                //                        Region region_livraison_input = new Region();
                                //                        region_livraison_input.setNom(Region_livraison.getText().toString().trim());
                                //                        region_data.insertOrReplace(region_livraison_input);
                                //
                                //                        adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_livraison_input);
                                //                        adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_livraison_input.getId());
                                //                        adresse_livraison_input.setPays(pays_livraison_input);
                                //                        adresse_livraison_input.setID_Pays(pays_livraison_input.getId());
                                //                        adresse_livraison_input.setRegion(region_livraison_input);
                                //                        adresse_livraison_input.setID_Region(region_livraison_input.getId());
                                //                        adresse_data.insertOrReplace(adresse_livraison_input);
                                //
                                //                        entreprise_input.setAdresse_Livraison(adresse_livraison_input);
                                //                        entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
                            } else {
                                entreprise_input.setAdresse_Livraison(adresse_siege_social_input);
                                entreprise_input.setID_Adresse_Livraison(adresse_siege_social_input.getId());
                            }
                            //add data to enterprise table
                            entreprise_data.insertOrReplace(entreprise_input);


                            if ((!(type_bio.equals(getString(R.string.TextTypeBio))))) {

                                List<Entreprise> List_Entreprise_cible = entreprise_data.queryBuilder()
                                        .where(EntrepriseDao.Properties.Raison_sociale.eq(entreprise_input.getRaison_sociale()))
                                        .list();
                                if (List_Entreprise_cible != null) {
                                    for (Entreprise e : List_Entreprise_cible) {

                                        List<Type_Bio> List_Type_Bio = type_bio_data.queryBuilder()
                                                .where(Type_BioDao.Properties.Nom.eq(type_bio))
                                                .list();
                                        if (List_Type_Bio != null) {
                                            for (Type_Bio TB : List_Type_Bio) {
                                                TB.setID_Entreprise(e.getId()); //Ajout type Bio à l'entreprise
                                                //Log.i("Ajout Type Bio", "Type Bio : " + TextAdapter.getItem(i).toString());
                                                type_bio_data.insertOrReplace(TB);
                                                e.update();
                                            }
                                        }
                                    }
                                }
                            }


                            //Associer la nouvelle entreprise au compte
                            List<Compte> update_compte = compte_dao.queryBuilder()
                                    .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                    .list();
                            if (update_compte != null) {
                                for (Compte c : update_compte) {
                                    entreprise_input.setID_Compte(c.getId());
                                     idEnterprise= entreprise_data.insertOrReplace(entreprise_input);
                                    c.update();
                                }
                            }


                            ContactDao contact_data = daoSession.getContactDao();
                            Categorie_ContactDao categorie_data = daoSession.getCategorie_ContactDao();
                            SociabiliteDao sociabilite_data = daoSession.getSociabiliteDao();

                            IdentiteDao identite_data = daoSession.getIdentiteDao();
                            SexeDao sexe_data = daoSession.getSexeDao();


                            //Table Contact
                            com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
                            contact_input.setRemarque_contact(remarque);
                            // contact_input.setContact_identifiant(contact_identifiant);
                            contact_input.setArchive(Integer.parseInt(archive));
                            contact_input.setDate_creation(datecreation);
                            contact_input.setDate_modification(datemodification);
                            contact_input.setID_Agronome_Creation(1);
                            contact_input.setID_Agronome_Modification(1);

                            //Ajout de la table Categorie
                            List<Categorie_Contact> List_Categorie = categorie_data.queryBuilder()
                                    .where(Categorie_ContactDao.Properties.Nom.eq(categorie))
                                    .list();
                            if (List_Categorie != null) {
                                for (Categorie_Contact CC : List_Categorie) {
                                    contact_input.setCategorie_Contact(CC);
                                    contact_input.setID_Categorie_Contact(CC.getId());
                                }
                            }

                            //Ajout de la table Sociabilite
                            List<Sociabilite> List_Sociabilite = sociabilite_data.queryBuilder()
                                    .where(SociabiliteDao.Properties.Nom.eq(sociabilite))
                                    .list();
                            if (List_Sociabilite != null) {
                                for (Sociabilite S : List_Sociabilite) {
                                    contact_input.setSociabilite(S);
                                    contact_input.setID_Sociabilite(S.getId());
                                }
                            }


                            //Table identité
                            Identite identite_input = new Identite();
                            identite_input.setNom(nom); //Nom
                            identite_input.setPrenom(prenom); //Prenom
                            identite_input.setPhoto("");
                            identite_input.setTelephone_principal(telephone_principal); //Telephone principale
                            identite_input.setTelephone_secondaire(telephone_secondaire); //Telephoe secondaire
                            identite_input.setAdresse_email(adresse_mail); //Mail 1
                            identite_input.setArchive(Integer.parseInt(archive));
                            identite_input.setDate_creation(datecreation);
                            identite_input.setDate_modification(datemodification);
                            identite_input.setID_Agronome_Creation(1);
                            identite_input.setID_Agronome_Modification(1);

                            Sexe sexe_input = new Sexe();

                            sexe_input.setNom(sexe);

                            sexe_data.insertOrReplace(sexe_input);


                            //Gestion de la photo
                            //                    if (Contact_Photo.getDrawable().getConstantState() != CreateContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            //                        //Photo basse qualité
                            //                        File myDir_Source1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                            //                        File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");
                            //
                            //                        File myDir_Dest1 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                            //                        File photoFile_Dest1 = new File(myDir_Dest1, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                            //                        try {
                            //                            CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
                            //                        } catch (IOException e) {
                            //                            e.printStackTrace();
                            //                        }
                            //                        //Photo haute qualité
                            //                        File myDir_Source2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Cache");
                            //                        File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");
                            //
                            //                        File myDir_Dest2 = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Spinner_Entreprise.getSelectedItem().toString() + "/Contacts");
                            //                        File photoFile_Dest2 = new File(myDir_Dest2, Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture_HD.jpg");
                            //                        try {
                            //                            CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
                            //                        } catch (IOException e) {
                            //                            e.printStackTrace();
                            //                        }
                            //
                            //                        identite_input.setPhoto(Last_Name.getText().toString().trim() + "_" + First_Name.getText().toString().trim() + "_picture.jpg");
                            //                    }
                            //                    else{
                            //
                            //                        identite_input.setPhoto("");
                            //                    }

                            //Table Adresse
                            Adresse adresse_input = new Adresse();
                            adresse_input.setAdresse_numero_rue(numero_nom_rue); //Adresse
                            adresse_input.setComplement_adresse(complement_adresse); //Complement Adresse
                            adresse_input.setArchive(Integer.parseInt(archive));
                            adresse_input.setDate_creation(Today_Date());
                            adresse_input.setDate_modification(Today_Date());
                            adresse_input.setID_Agronome_Creation(1);
                            adresse_input.setID_Agronome_Modification(1);


                            Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
                            code_postal_ville_input.setCode_postal(code_postale);
                            code_postal_ville_input.setVille(ville);
                            code_postal_ville_data.insertOrReplace(code_postal_ville_input);

                            Region region_input = new Region();
                            region_input.setNom(region);
                            region_data.insertOrReplace(region_input);

                            Pays pays_input = new Pays();
                            pays_input.setNom(pays);
                            pays_data.insertOrReplace(pays_input);

                            //Cardinalités entre les tables :
                            adresse_input.setCode_Postal_Ville(code_postal_ville_input);
                            adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
                            adresse_input.setRegion(region_input);
                            adresse_input.setID_Region(region_input.getId());
                            adresse_input.setPays(pays_input);
                            adresse_input.setID_Pays(pays_input.getId());
                            adresse_data.insertOrReplace(adresse_input);


                            identite_input.setAdresse(adresse_input);
                            identite_input.setID_Adresse(adresse_input.getId());

                            identite_input.setSexe(sexe_input);
                            identite_input.setID_Sexe(sexe_input.getId());

                            identite_data.insertOrReplace(identite_input);
                            //Un contact possède une identité
                            contact_input.setIdentite(identite_input);
                            contact_input.setID_Identite_Contact(identite_input.getId());

                            //Ajout du contact à l'entreprise concernée
                            List<Compte> update_compte1 = compte_dao.queryBuilder()
                                    .where(CompteDao.Properties.Login.eq(Nom_Compte))
                                    .list();
                            if (update_compte1 != null) {
                                for (Compte c : update_compte1) {
                                    List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                                    for (Entreprise e : List_Company) {
                                        if (e.getRaison_sociale().equals(raison_sociale)) {
                                            contact_input.setID_Entreprise(e.getId());
                                           //contact_input.setId(idEnterprise);
                                            contact_data.insertOrReplace(contact_input);
                                            e.update();
                                        }
                                    }
                                    c.update();
                                }
                            }

                        }
                    }
                    i++;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            executeCountMathu++;
            return 1;


        }




    }


}

//on server
//justine=1
//mathieu=2

