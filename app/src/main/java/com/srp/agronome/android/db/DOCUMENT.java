package com.srp.agronome.android.db;

import org.greenrobot.greendao.annotation.*;

import com.srp.agronome.android.db.DaoSession;
import org.greenrobot.greendao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "DOCUMENT".
 */
@Entity(active = true)
public class DOCUMENT {

    @Id
    private Long id;
    private Long ID_AUDIT_DOCUMENT_DOCUMENT  ;
    private long ID_TYPE_DOCUMENT  ;
    private long ID_DOCUMENT_FOURNI  ;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient DOCUMENTDao myDao;

    @ToOne(joinProperty = "ID_TYPE_DOCUMENT  ")
    private TYPE_DOCUMENT tYPE_DOCUMENT;

    @Generated
    private transient Long tYPE_DOCUMENT__resolvedKey;

    @ToOne(joinProperty = "ID_DOCUMENT_FOURNI  ")
    private DOCUMENT_FOURNI dOCUMENT_FOURNI;

    @Generated
    private transient Long dOCUMENT_FOURNI__resolvedKey;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public DOCUMENT() {
    }

    public DOCUMENT(Long id) {
        this.id = id;
    }

    @Generated
    public DOCUMENT(Long id, Long ID_AUDIT_DOCUMENT_DOCUMENT  , long ID_TYPE_DOCUMENT  , long ID_DOCUMENT_FOURNI  ) {
        this.id = id;
        this.ID_AUDIT_DOCUMENT_DOCUMENT   = ID_AUDIT_DOCUMENT_DOCUMENT  ;
        this.ID_TYPE_DOCUMENT   = ID_TYPE_DOCUMENT  ;
        this.ID_DOCUMENT_FOURNI   = ID_DOCUMENT_FOURNI  ;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getDOCUMENTDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getID_AUDIT_DOCUMENT_DOCUMENT  () {
        return ID_AUDIT_DOCUMENT_DOCUMENT  ;
    }

    public void setID_AUDIT_DOCUMENT_DOCUMENT  (Long ID_AUDIT_DOCUMENT_DOCUMENT  ) {
        this.ID_AUDIT_DOCUMENT_DOCUMENT   = ID_AUDIT_DOCUMENT_DOCUMENT  ;
    }

    public long getID_TYPE_DOCUMENT  () {
        return ID_TYPE_DOCUMENT  ;
    }

    public void setID_TYPE_DOCUMENT  (long ID_TYPE_DOCUMENT  ) {
        this.ID_TYPE_DOCUMENT   = ID_TYPE_DOCUMENT  ;
    }

    public long getID_DOCUMENT_FOURNI  () {
        return ID_DOCUMENT_FOURNI  ;
    }

    public void setID_DOCUMENT_FOURNI  (long ID_DOCUMENT_FOURNI  ) {
        this.ID_DOCUMENT_FOURNI   = ID_DOCUMENT_FOURNI  ;
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public TYPE_DOCUMENT getTYPE_DOCUMENT() {
        long __key = this.ID_TYPE_DOCUMENT  ;
        if (tYPE_DOCUMENT__resolvedKey == null || !tYPE_DOCUMENT__resolvedKey.equals(__key)) {
            __throwIfDetached();
            TYPE_DOCUMENTDao targetDao = daoSession.getTYPE_DOCUMENTDao();
            TYPE_DOCUMENT tYPE_DOCUMENTNew = targetDao.load(__key);
            synchronized (this) {
                tYPE_DOCUMENT = tYPE_DOCUMENTNew;
            	tYPE_DOCUMENT__resolvedKey = __key;
            }
        }
        return tYPE_DOCUMENT;
    }

    @Generated
    public void setTYPE_DOCUMENT(TYPE_DOCUMENT tYPE_DOCUMENT) {
        if (tYPE_DOCUMENT == null) {
            throw new DaoException("To-one property 'ID_TYPE_DOCUMENT  ' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.tYPE_DOCUMENT = tYPE_DOCUMENT;
            ID_TYPE_DOCUMENT   = tYPE_DOCUMENT.getId();
            tYPE_DOCUMENT__resolvedKey = ID_TYPE_DOCUMENT  ;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public DOCUMENT_FOURNI getDOCUMENT_FOURNI() {
        long __key = this.ID_DOCUMENT_FOURNI  ;
        if (dOCUMENT_FOURNI__resolvedKey == null || !dOCUMENT_FOURNI__resolvedKey.equals(__key)) {
            __throwIfDetached();
            DOCUMENT_FOURNIDao targetDao = daoSession.getDOCUMENT_FOURNIDao();
            DOCUMENT_FOURNI dOCUMENT_FOURNINew = targetDao.load(__key);
            synchronized (this) {
                dOCUMENT_FOURNI = dOCUMENT_FOURNINew;
            	dOCUMENT_FOURNI__resolvedKey = __key;
            }
        }
        return dOCUMENT_FOURNI;
    }

    @Generated
    public void setDOCUMENT_FOURNI(DOCUMENT_FOURNI dOCUMENT_FOURNI) {
        if (dOCUMENT_FOURNI == null) {
            throw new DaoException("To-one property 'ID_DOCUMENT_FOURNI  ' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.dOCUMENT_FOURNI = dOCUMENT_FOURNI;
            ID_DOCUMENT_FOURNI   = dOCUMENT_FOURNI.getId();
            dOCUMENT_FOURNI__resolvedKey = ID_DOCUMENT_FOURNI  ;
        }
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
