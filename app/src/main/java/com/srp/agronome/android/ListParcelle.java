package com.srp.agronome.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


import android.widget.ListView;
import android.widget.PopupMenu;


import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.ILOT;
import com.srp.agronome.android.db.ILOTDao;
import com.srp.agronome.android.db.PARCELLE;

import com.srp.agronome.android.db.PARCELLEDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;

import static com.srp.agronome.android.MenuParcelle.Id_suivi_producteur;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuParcelle.Id_ilot_selected;
import static com.srp.agronome.android.MenuParcelle.Id_Parcelle_selected;


/**
 * Created by JItendra on 08/08/2017.
 */

public class ListParcelle extends AppCompatActivity {
    private ArrayList<ParcelleClass> arrayListparcelle = new ArrayList<ParcelleClass>();
    CustomparcelleAdapter adapter;
    private ImageButton btnRetour = null;
    private ListView listView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_parcelle);


        btnRetour = (ImageButton) findViewById(R.id.arrow_back_menu);
        btnRetour.setOnClickListener(ButtonRetourHandler);
        LoadListParcelle();

//        arrayListparcelle.add(new ParcelleClass("Nom Ilot : homi ", "Numero Ilot : 2", "Nom parcelle : p1", "numero parcelle : 34"));
//        arrayListparcelle.add(new ParcelleClass("Nom Ilot : teryu ", "Numero Ilot : 3", "Nom parcelle : p2", "numero parcelle : 34"));
//        arrayListparcelle.add(new ParcelleClass("Nom Ilot : boin ", "Numero Ilot : 4", "Nom parcelle : p3", "numero parcelle : 34"));

        listView = (ListView) findViewById(R.id.listview); // see layout  activity_list_parcelle

          /* This comparator will sort objects alphabetically. */
        Collections.sort(arrayListparcelle, new Comparator<ParcelleClass>() {
            @Override
            public int compare(ParcelleClass a1, ParcelleClass a2) {

                // String implements Comparable
                return (a1.getNom_Ilot().toString()).compareTo(a2.getNom_Ilot().toString());
            }
        });

// see layout list_parcelle to display the customparcelleAdapter.java value
        adapter = new CustomparcelleAdapter(this, R.layout.list_parcelle, arrayListparcelle);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                adapter.onItemselected(arg2);
            }
        });

    }

    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ListParcelle.this, MenuParcelle.class);
            finish();
            startActivity(intent);
        }
    };


    private void LoadListParcelle() {
        //Get Suivi Producteur
        SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        List <ILOT> list_Ilot = getIlotList(sp);
        if (list_Ilot != null){
            for (ILOT I : list_Ilot) {
                if (I.getArchive() != 0) {
                    String Nom_ilot = I.getNOM_ILOT();
                    String Numero_ilot = I.getNUMERO_ILOT();
                    List <PARCELLE> List_Parcelle = getParcelleList(I);
                    if (List_Parcelle != null){
                        for (PARCELLE pr : List_Parcelle) {
                            if (pr.getArchive() != 0) {
                                arrayListparcelle.add(new ParcelleClass(
                                      "Nom_ilot :" + Nom_ilot, //Nom ILOT
                                        "Numero_ilot :"+ Numero_ilot,//Numero ILOT
                                       "Nom_Parcelle  :"+ pr.getNOM_PARCELLE(), //NOM Parcelle
                                        "Numero_Parcelle  :"+ pr.getNUMERO_PARCELLE(),//Numero Parcelle
                                        I.getId(),
                                        pr.getId()));
                            }
                        }
                    }
                }
            }
        }
    }



    //Return a list of Ilots
    private List<ILOT> getIlotList(SUIVI_PRODUCTEUR Suivi_producteur) {
        return Suivi_producteur.getILOTList();
    }

    //Return an ilot
    private ILOT getIlot(List<ILOT> list_ilot, long ID_Ilot) {
        ILOT result = null;
        for (ILOT I : list_ilot) {
            if (I.getId() == ID_Ilot) {
                result = I;
            }
        }
        return result;
    }

    //Return a list of Parcelle
    private List<PARCELLE> getParcelleList(ILOT ilot) {
        return ilot.getPARCELLEList();
    }

    //Return a parcelle
    private PARCELLE getParcelle(List<PARCELLE> list_parcelle, long ID_Parcelle) {
        PARCELLE result = null;
        for (PARCELLE P : list_parcelle) {
            if (P.getId() == ID_Parcelle) {
                result = P;
            }
        }
        return result;
    }

    private SUIVI_PRODUCTEUR getSuiviProducteurOfOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        SUIVI_PRODUCTEUR suivi_producteur_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        suivi_producteur_result = e.getSUIVI_PRODUCTEUR();
                    }
                }
            }
        }
        return suivi_producteur_result;
    }
}