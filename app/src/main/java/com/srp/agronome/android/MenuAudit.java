package com.srp.agronome.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MenuAudit extends BasicListViewActivity {
    public static long Audit_Id_Selected;
    String n="List Audit";
    //String l="Create Audit";
    String str;
    String toolbartitle="AUDIT";
    ImageButton back=null;
    //check the BasicActivity layout file
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_list_view);

        //set common listview
        mapWidgets();
        //call abstract methods
        setBasicListButtonName(n);
        str=getResources().getString(R.string.create_audit); //set translation text
        setBasicButtonCreateList(str);
        setToolbarTitle(toolbartitle);

        back= (ImageButton) findViewById(R.id.arrow_back_contact);


        //btn functions
        basicCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MenuAudit.this,Audit.class);
                startActivity(intent);
            }
        });

        bnBasicList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuAudit.this,ListAudit.class));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuAudit.this,MenuAgriculteur.class));
            }
        });



    }

    @Override
    public void setBasicListButtonName(String Bname) {
        bnBasicList.setText(Bname);

    }

    @Override
    public void setBasicButtonCreateList(String listname) {
        basicCreate.setText(listname);

    }
    @Override
    public void setToolbarTitle(String name)
    {
       toolbar.setTitle(name);
    }

}
