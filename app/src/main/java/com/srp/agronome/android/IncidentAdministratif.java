package com.srp.agronome.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Etat_Incident;
import com.srp.agronome.android.db.Etat_IncidentDao;
import com.srp.agronome.android.db.Incident;
import com.srp.agronome.android.db.IncidentDao;
import com.srp.agronome.android.db.Incident_Administratif;
import com.srp.agronome.android.db.Incident_AdministratifDao;
import com.srp.agronome.android.db.Incident_Contrat;
import com.srp.agronome.android.db.Incident_ContratDao;
import com.srp.agronome.android.db.Sujet_Message;
import com.srp.agronome.android.db.Sujet_MessageDao;
import com.srp.agronome.android.db.Type_Incident;
import com.srp.agronome.android.db.Type_IncidentDao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;

/**
 * Created by vincent on 28/08/17.
 */

public class IncidentAdministratif extends AppCompatActivity {

    private ImageButton BtnRetour = null;
    private ImageButton BtnAdd = null;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_administratif);

        BtnRetour = (ImageButton) findViewById(R.id.arrow_back_list_incident);
        BtnRetour.setOnClickListener(ButtonRetourHandler);

        BtnAdd = (ImageButton)findViewById(R.id.btn_add_incident);
        BtnAdd.setOnClickListener(ButtonAddIncidentHandler);

        DisplayListIncidentInLog();

        ArrayList<Element_Incident> list_incident = CreateListIncident();
        final ListView listview_incident = (ListView) findViewById(R.id.ListViewIncident);


        listview_incident.setAdapter(new CustomIncidentAdapter(getApplicationContext(), list_incident)); //Load list

        //Event Clic :
        listview_incident.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = listview_incident.getItemAtPosition(position);
                Element_Incident fullObject = (Element_Incident)o;
                Toast.makeText(getApplicationContext(), "You have chosen: " + " " + fullObject.getObjetIncident(), Toast.LENGTH_LONG).show();
            }
        });

    }

    View.OnClickListener ButtonAddIncidentHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style
            PopupMenu popup = new PopupMenu(wrapper,BtnAdd); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_add_incident, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals(getString(R.string.TextAddIncidentAdministratif))){
                        Intent intent = new Intent(IncidentAdministratif.this, CreateIncidentAdministratif.class);
                        finish();
                        startActivity(intent);
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    //Bouton Retour
    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(IncidentAdministratif.this, MenuSuiviProducteur.class);
            finish();
            startActivity(intent);
        }
    };

    //Event BackPressed
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(IncidentAdministratif.this, MenuSuiviProducteur.class);
            finish();
            startActivity(intent);
        }
        return true;
    }


    //Example of List of Incident
    private ArrayList<Element_Incident> CreateListIncident() {
        ArrayList<Element_Incident> results = new ArrayList<Element_Incident>();
        Entreprise entreprise_load = getOrganization(ID_Compte_Selected, ID_Entreprise_Selected);
        List<Incident> list_incident = entreprise_load.getIncidentList();
        if (list_incident != null) {
            for (Incident I : list_incident) {
                Element_Incident sr1 = new Element_Incident();
                if (I.getSujet_Message().getIncident_Administratif() != null){
                    sr1.setObjetIncident(I.getSujet_Message().getIncident_Administratif().getNom());
                }
                if (I.getSujet_Message().getIncident_Contrat() != null){
                    sr1.setObjetIncident(I.getSujet_Message().getIncident_Contrat().getNom());
                }
                sr1.setTexteIncident(I.getMessage_Incident());
                sr1.setDateIncident(DateToStringFormat(I.getDate_creation()));
                results.add(sr1);
            }
        }
            return results;
        }

        private String DateToStringFormat (Date date_input){
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM"); // Set your date format
            return sdf.format(date_input); // Get Date String according to date format
        }

        //Return an Organization
        private Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_data = daoSession.getCompteDao();
            Entreprise entreprise_result = null;
            List<Compte> List_compte = compte_data.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(Id_Compte))
                    .list();
            if (List_compte != null) {
                for (Compte c : List_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList();
                    for (Entreprise e : List_Entreprise) {
                        if (e.getId() == Id_Entreprise){
                            entreprise_result = e;
                        }
                    }
                }
            }
            return entreprise_result;
        }

    private void DisplayListIncidentInLog(){
        Entreprise entreprise_load = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        List<Incident> list_incident = entreprise_load.getIncidentList();
        if (list_incident != null){
            for (Incident I : list_incident){

                Log.i("TypeIncident",I.getSujet_Message().getType_Incident().getNom());

                if (I.getSujet_Message().getIncident_Administratif() != null){
                    Log.i("SujetIncident",I.getSujet_Message().getIncident_Administratif().getNom());
                }
                if (I.getSujet_Message().getIncident_Contrat() != null){
                    Log.i("SujetIncident",I.getSujet_Message().getIncident_Contrat().getNom());
                }
                Log.i("EtatIncident", I.getEtat_Incident().getNom());
                Log.i("MessageIncident", I.getMessage_Incident());
            }
        }
    }


}
