package com.srp.agronome.android;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.VectorEnabledTintResources;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.AUDIT_DOCUMENTDao;
import com.srp.agronome.android.db.AUDIT_PARCELLEDao;
import com.srp.agronome.android.db.AUDIT_STOCKAGE;
import com.srp.agronome.android.db.AUDIT_STOCKAGEDao;
import com.srp.agronome.android.db.Attitude_Entreprise_Visite;
import com.srp.agronome.android.db.Attitude_Entreprise_VisiteDao;
import com.srp.agronome.android.db.CONTAMINATION_CROISE;
import com.srp.agronome.android.db.CONTAMINATION_CROISEDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.ETAT_NUISIBLE;
import com.srp.agronome.android.db.ETAT_NUISIBLEDao;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Interet_Visite;
import com.srp.agronome.android.db.Interet_VisiteDao;
import com.srp.agronome.android.db.NUISIBLE;
import com.srp.agronome.android.db.NUISIBLEDao;
import com.srp.agronome.android.db.Objet_Visite;
import com.srp.agronome.android.db.Objet_VisiteDao;
import com.srp.agronome.android.db.PROPRETE;
import com.srp.agronome.android.db.PROPRETEDao;
import com.srp.agronome.android.db.TEMPERATURE;
import com.srp.agronome.android.db.TEMPERATUREDao;
import com.srp.agronome.android.db.TRIAGE_GRAIN;
import com.srp.agronome.android.db.TRIAGE_GRAINDao;
import com.srp.agronome.android.db.TYPE_DOCUMENT;
import com.srp.agronome.android.db.TYPE_DOCUMENTDao;
import com.srp.agronome.android.db.VENTILATION;
import com.srp.agronome.android.db.VENTILATIONDao;
import com.srp.agronome.android.db.Visite;

import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuAudit.Audit_Id_Selected;
import static com.srp.agronome.android.Audit.AUDIT_TYPE;

/**
 * Created by doucoure on 18/07/2017.
 */

public class AuditStockage extends AppCompatActivity {

    private Spinner spinnerproper = null;
    private Spinner spinnerVentilation = null;
    private Spinner spinnertemperature = null;
    private Spinner spinnercontamination = null;
    private Spinner spinnernuisibles = null;
    private Spinner spinnergrain = null;
    Button valid=null;
    Toolbar toolbar=null;
    TextView toolbarName=null;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_stock);

        toolbarName= (TextView) findViewById(R.id.toolbar_title);
        toolbarName.setText("Audit Stockage");

        //set audit type
        AUDIT_TYPE="Stockage";

        //setup ui
        findWidget();

        //create choice list into DB
        CreateListIntoDatabase();



        //load spinners
        LoadSpinnerProper();
        LoadSpinnerVentilation();
        LoadSpinnerTemperature();
        LoadSpinnerContamination();
        LoadSpinnerNusibles();
        LoadSpinnerGrain();

        //btn
        valid.setOnClickListener(ButtonValidateHandler);

    }


    //widget
    private void findWidget()
    {
        spinnerproper = (Spinner) findViewById(R.id.SpinnerProper);
        spinnerVentilation= (Spinner) findViewById(R.id.SpinnerVentilation);
        spinnertemperature= (Spinner) findViewById(R.id.SpinnerTemperature);
        spinnercontamination= (Spinner) findViewById(R.id.SpinnerContamination);
        spinnernuisibles= (Spinner) findViewById(R.id.SpinnerNuisibles);
        spinnergrain= (Spinner) findViewById(R.id.SpinnerGrain);
        toolbar= (Toolbar) findViewById(R.id.toolbarLayout);
        //btn
        valid= (Button) findViewById(R.id.AstockValid);

    }
    //handler
    View.OnClickListener ButtonValidateHandler=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //create data into DB
            //insertAuditStockageintoDB();
           // startActivity(new Intent(AuditStockage.this,MenuAudit.class));

            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),inputtoText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
            }

        }
    };

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

    //display dialongue box
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                startActivity(new Intent(AuditStockage.this,MenuAudit.class));

                LoadingDialogInsertAuditStockageDB();
                //DisplayCreateAuditStockageInLog();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    public void LoadingDialogInsertAuditStockageDB(){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(AuditStockage.this,
                getString(R.string.TitleChargement), getString(R.string.TextChargementvisite), true);
        ringProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InsertAuditStockageIntoDatabase();
                } catch (Exception e) {
                }
                ringProgressDialog.dismiss();
                Intent intent = new Intent(AuditStockage.this, MenuVisite.class);
                finish();
                startActivity(intent);
            }
        }).start();
    }

    //insert audit stockage into DB
    private void InsertAuditStockageIntoDatabase(){
        //Get the Organization
        Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        //create audit stockage
       // AUDIT_STOCKAGE audit_stockage_insert =  CreateAuditStockageIntoDatabase(spinnerproper.getSelectedItem().toString() ,spinnercontamination.getSelectedItem().toString(),spinnernuisibles.getSelectedItem().toString(),spinnertemperature.getSelectedItem().toString(),spinnerVentilation.getSelectedItem().toString(),spinnergrain.getSelectedItem().toString());
        //Add One Sujet Visite Principale

    }

    private AUDIT_STOCKAGE CreateAuditStockageIntoDatabase(String proper, String contamination, String nuisibles, String temperature, String ventilation, String grain) {

        DaoSession daoSession=((AppController) getApplication()).getDaoSession();
        AUDITDao audit_data=daoSession.getAUDITDao();
        AUDIT_STOCKAGEDao stockage_data=daoSession.getAUDIT_STOCKAGEDao();
        NUISIBLEDao nuisible_dat=daoSession.getNUISIBLEDao();
        TEMPERATUREDao temerature_data=daoSession.getTEMPERATUREDao();
        TRIAGE_GRAINDao grain_data=daoSession.getTRIAGE_GRAINDao();
        VENTILATIONDao ventilation_data=daoSession.getVENTILATIONDao();
        CONTAMINATION_CROISEDao contamin_data=daoSession.getCONTAMINATION_CROISEDao();

        //table stockage
        AUDIT_STOCKAGE audit_stockage_input=new AUDIT_STOCKAGE();
        audit_stockage_input.setArchive(1);
        audit_stockage_input.setID_Agronome_Creation(1);
        audit_stockage_input.setID_Agronome_Modification(1);
        audit_stockage_input.setDate_creation(AuditParcelle.Today_Date());
        audit_stockage_input.setDate_modification(AuditParcelle.Today_Date());


        //table temp
        TEMPERATURE temp_input=new TEMPERATURE();
        temp_input.setNom(temperature);
        temerature_data.insertOrReplace(temp_input);
        //add list temp to stockage
        List<TEMPERATURE> tempList=temerature_data.queryBuilder().where(TEMPERATUREDao.Properties.Nom.eq(spinnertemperature.getSelectedItem().toString())).list();
        for(TEMPERATURE t: tempList)
        {
            audit_stockage_input.setTEMPERATURE(t);
            audit_stockage_input.setID_TEMPERATURE(t.getId());
        }

        //table contamin
        CONTAMINATION_CROISE contam_input=new CONTAMINATION_CROISE();
        contam_input.setNom(contamination);
        contamin_data.insertOrReplace(contam_input);
        //add list contam to stockage
        List<CONTAMINATION_CROISE> contamList=contamin_data.queryBuilder().where(CONTAMINATION_CROISEDao.Properties.Nom.eq(spinnercontamination.getSelectedItem().toString())).list();
        for(CONTAMINATION_CROISE c: contamList)
        {
            audit_stockage_input.setCONTAMINATION_CROISE(c);
            audit_stockage_input.setID_CONTAMINATION_CROISE(c.getId());
        }

        //table grain
        TRIAGE_GRAIN grain_input=new TRIAGE_GRAIN();
        grain_input.setNom(grain);
        grain_data.insertOrReplace(grain_input);
        //add list grain to stockage
        List<TRIAGE_GRAIN> grainList=grain_data.queryBuilder().where(TRIAGE_GRAINDao.Properties.Nom.eq(spinnergrain.getSelectedItem().toString())).list();
        for(TRIAGE_GRAIN g:grainList)
        {
            audit_stockage_input.setTRIAGE_GRAIN(g);
            audit_stockage_input.setID_TRIAGE_GRAIN(g.getId());
        }

        //table nuisible
        NUISIBLE nuisible_input=new NUISIBLE();
       // nuisible_input.setID



        return audit_stockage_input;
    }


    //Return an Organization
    private Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }

    //
    private Boolean CheckInput(){
        boolean b = true;

        //Vérification des listes de choix :
        if (spinnerproper.getSelectedItem().toString().equals(getString(R.string.TextProperType))) {
            b = false;
        }
        if (spinnerVentilation.getSelectedItem().toString().equals(getString(R.string.Venti))) {
            b = false;
        }
        if (spinnernuisibles.getSelectedItem().toString().equals(getString(R.string.textnuisi))) {
            b = false;
        }
        if (spinnertemperature.getSelectedItem().toString().equals(getString(R.string.textTemp))) {
            b = false;
        }
        if (spinnercontamination.getSelectedItem().toString().equals(getString(R.string.textcontam))) {
            b = false;
        }
        if (spinnergrain.getSelectedItem().toString().equals(getString(R.string.grain))) {
            b = false;
        }

        return b;
    }

    private String inputtoText()
    {
        String text=getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.TextPROPRETE) + " : " + spinnerproper.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.venti) + " : " + spinnerVentilation.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.textTemp) + " : " + spinnertemperature.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.textcontam) + " : " + spinnercontamination.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.textnuisi) + " : " + spinnernuisibles.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.grain) + " : " + spinnergrain.getSelectedItem().toString() + "\n\n";

        return  text;
    }


    //create validation list into DB
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();

        VENTILATIONDao ventilationDao_data=daoSession.getVENTILATIONDao();
        TEMPERATUREDao temperatureDao_data=daoSession.getTEMPERATUREDao();
        CONTAMINATION_CROISEDao contamination_croiseDao_data=daoSession.getCONTAMINATION_CROISEDao();
        //NUISIBLEDao nuisibleDao_data=daoSession.getNUISIBLEDao();
        TRIAGE_GRAINDao triage_grainDao_data=daoSession.getTRIAGE_GRAINDao();
        PROPRETEDao propreteDao_data=daoSession.getPROPRETEDao();
        ETAT_NUISIBLEDao etat_nuisibleDao_data=daoSession.getETAT_NUISIBLEDao();

        //set ventilation list
        List<VENTILATION> List_validation = ventilationDao_data.loadAll();
        if (List_validation.size() == 0) {
            VENTILATION List_validation_input_1 = new VENTILATION();
            List_validation_input_1.setNom("Absence");
            //Log.i("data", "CreateListIntoDatabase: "+List_validation_input_1.getNom());
            ventilationDao_data.insertOrReplace(List_validation_input_1);

            VENTILATION List_validation_input_2 = new VENTILATION();
            List_validation_input_2.setNom("A Développer");
            Log.i("data", "CreateListIntoDatabase: "+List_validation_input_2.getNom());
            ventilationDao_data.insertOrReplace(List_validation_input_2);

            VENTILATION List_validation_input_3 = new VENTILATION();
            List_validation_input_3.setNom("Présence");
            Log.i("data", "CreateListIntoDatabase: "+List_validation_input_3.getNom());
            ventilationDao_data.insertOrReplace(List_validation_input_3);


        }
        //set list temperature
        List<TEMPERATURE> List_temp = temperatureDao_data.loadAll();
        if (List_temp.size() == 0) {

            TEMPERATURE temp_input_1 = new TEMPERATURE();
            temp_input_1.setNom("Maitrise");
            temperatureDao_data.insertOrReplace(temp_input_1);

            TEMPERATURE temp_input_2 = new TEMPERATURE();
            temp_input_2.setNom("Pas de Maitrise");
            temperatureDao_data.insertOrReplace(temp_input_2);

        }

        //set contamination list
        List<CONTAMINATION_CROISE> List_contamin = contamination_croiseDao_data.loadAll();
        if (List_contamin.size() == 0) {
            CONTAMINATION_CROISE contamination_croise_input_1 = new CONTAMINATION_CROISE();
            contamination_croise_input_1.setNom("Aucun risque");
            contamination_croiseDao_data.insertOrReplace(contamination_croise_input_1);

            CONTAMINATION_CROISE contamination_croise_input_2 = new CONTAMINATION_CROISE();
            contamination_croise_input_2.setNom("Risque maitrisé");
            contamination_croiseDao_data.insertOrReplace(contamination_croise_input_2);

            CONTAMINATION_CROISE contamination_croise_input_3 = new CONTAMINATION_CROISE();
            contamination_croise_input_3.setNom("Non maitrisée");
            contamination_croiseDao_data.insertOrReplace(contamination_croise_input_3);
        }
        //set proper list
        List<PROPRETE> list_proper=propreteDao_data.loadAll();
        if(list_proper.size()==0)
        {
            PROPRETE proper_input1 =new PROPRETE();
            proper_input1.setNom("Très propre");
            propreteDao_data.insertOrReplace(proper_input1);

            PROPRETE proper_input2 =new PROPRETE();
            proper_input2.setNom(" propre");
            propreteDao_data.insertOrReplace(proper_input2);

            PROPRETE proper_input3 =new PROPRETE();
            proper_input3.setNom("Sale");
            propreteDao_data.insertOrReplace(proper_input3);

            PROPRETE proper_input4 =new PROPRETE();
            proper_input4.setNom("Très Sale");
            propreteDao_data.insertOrReplace(proper_input4);
        }

        //set list Nusiblies

        List<ETAT_NUISIBLE> nuisibleList=etat_nuisibleDao_data.loadAll();
        if(nuisibleList.size()==0)
        {
            ETAT_NUISIBLE nuisible_1=new ETAT_NUISIBLE();
            nuisible_1.setNom("Présence ");
            etat_nuisibleDao_data.insertOrReplace(nuisible_1);

            ETAT_NUISIBLE nuisible_2=new ETAT_NUISIBLE();
            nuisible_2.setNom("Présence maitrisée ");
            etat_nuisibleDao_data.insertOrReplace(nuisible_2);

            ETAT_NUISIBLE nuisible_3=new ETAT_NUISIBLE();
            nuisible_3.setNom("Absence ");
            etat_nuisibleDao_data.insertOrReplace(nuisible_3);
        }
        //set list triage gain

        List<TRIAGE_GRAIN> triage_grainList=triage_grainDao_data.loadAll();
        if (triage_grainList.size()==0)
        {
            TRIAGE_GRAIN triage_grain_1=new TRIAGE_GRAIN();
            triage_grain_1.setNom("Oui");
            triage_grainDao_data.insertOrReplace(triage_grain_1);

            TRIAGE_GRAIN triage_grain_2=new TRIAGE_GRAIN();
            triage_grain_2.setNom("à améliorer conformément");
            triage_grainDao_data.insertOrReplace(triage_grain_2);

            TRIAGE_GRAIN triage_grain_3=new TRIAGE_GRAIN();
            triage_grain_3.setNom("Non");
            triage_grainDao_data.insertOrReplace(triage_grain_3);






        }
    }



//load spineer values
    private void LoadSpinnerProper() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PROPRETEDao Type_proper_data = daoSession.getPROPRETEDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste

        List<PROPRETE> type_ProperList = Type_proper_data.queryBuilder().list();
        if (type_ProperList != null) {
            for (PROPRETE P : type_ProperList) {
                adapter.add(P.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextProperType)); //This is the text that will be displayed as hint.
        spinnerproper.setAdapter(adapter);
        spinnerproper.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

    }

    //load spinner for temperature

    private void LoadSpinnerTemperature() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TEMPERATUREDao Type_temp_data = daoSession.getTEMPERATUREDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste

        List<TEMPERATURE> type_tempList = Type_temp_data.queryBuilder().list();
        if (type_tempList != null) {
            for (TEMPERATURE t : type_tempList) {
                adapter.add(t.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.textTemp)); //This is the text that will be displayed as hint.
        spinnertemperature.setAdapter(adapter);
        spinnertemperature.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

    }

    //load list different way ventilation
    private void LoadSpinnerVentilation() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        VENTILATIONDao venti_data = daoSession.getVENTILATIONDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste

        //Load items from the list
        List<VENTILATION> List_ventilation = venti_data.loadAll();
        if (List_ventilation != null) {
            for (VENTILATION v : List_ventilation) {
                adapter.add(v.getNom()); //Ajouter les éléments
                Log.i("Spinner", "LoadSpinnerVentilation: "+v.getNom());
            }
        }
        adapter.add("Select validation"); //This is the text that will be displayed as hint.
        spinnerVentilation.setAdapter(adapter);  //pay attention
        spinnerVentilation.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

    }

    //spinner contamination

    private void LoadSpinnerContamination() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CONTAMINATION_CROISEDao contam_data = daoSession.getCONTAMINATION_CROISEDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste

        List<CONTAMINATION_CROISE> type_List = contam_data.queryBuilder().list();
        if (type_List != null) {
            for (CONTAMINATION_CROISE c : type_List) {
                adapter.add(c.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.textcontam)); //This is the text that will be displayed as hint.
        spinnercontamination.setAdapter(adapter);
        spinnercontamination.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

    }

    //spinner nusibles
    private void LoadSpinnerNusibles() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_NUISIBLEDao nusi_data = daoSession.getETAT_NUISIBLEDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste

        List<ETAT_NUISIBLE> type_List = nusi_data.queryBuilder().list();
        if (type_List != null) {
            for (ETAT_NUISIBLE n : type_List) {
                adapter.add(n.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.textnuisi)); //This is the text that will be displayed as hint.
        spinnernuisibles.setAdapter(adapter);
        spinnernuisibles.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

    }

    //spinner gain
    private void LoadSpinnerGrain() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TRIAGE_GRAINDao gain_data = daoSession.getTRIAGE_GRAINDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Charger les éléments de la liste

        List<TRIAGE_GRAIN> type_List = gain_data.queryBuilder().list();
        if (type_List != null) {
            for (TRIAGE_GRAIN t : type_List) {
                adapter.add(t.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.grain)); //This is the text that will be displayed as hint.
        spinnergrain.setAdapter(adapter);
        spinnergrain.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

    }



}
