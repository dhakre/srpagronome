package com.srp.agronome.android;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SpinnerAdapter;

import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Objet_Visite;
import com.srp.agronome.android.db.Objet_VisiteDao;
import com.srp.agronome.android.db.Visite;
import com.srp.agronome.android.db.VisiteDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuAudit.Audit_Id_Selected;
import static com.srp.agronome.android.MenuVisite.Id_Visite_Selected;

public class EditAudit extends AppCompatActivity {

    private ImageButton imgBtnHome = null;
    private Button BtnValidate = null;
    public EditText year=null;
    public EditText remarque=null;
    private Button validate=null;
    long audit_id=Audit_Id_Selected;
    String a,b;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_audit);   //reuse the create audit layout

        year= (EditText)findViewById(R.id.annee_id);
        remarque= (EditText) findViewById(R.id.remarque_doc_id);
        validate= (Button) findViewById(R.id.objet_id);


        AUDIT audit_loaded=getAuditV2(Audit_Id_Selected);

        setDataAuditToWidget(audit_loaded);
        // already the selected data
        //home
        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_contact);
        imgBtnHome.setOnClickListener(ButtonbackHandler);
        validate.setOnClickListener(ButtonvalidHandler);


    }

    private void setDataAuditToWidget(AUDIT audit_selected){
      //edit data

        remarque.setText(audit_selected.getREMARQUES_AUDITS().toString().trim());
        Date auditdate=audit_selected.getDATE();
        SimpleDateFormat sdf = new SimpleDateFormat("d/MM/yyyy");
        String date=sdf.format(auditdate);
      //  year.setText(audit_selected.getDATE().toString());
        year.setText(date);

    }


    //Bouton back Toolbar
    View.OnClickListener ButtonbackHandler = new View.OnClickListener() {
        public void onClick(View v) {
           startActivity(new Intent(EditAudit.this,MenuAudit.class));
        }
    };

    //valide handler
    View.OnClickListener ButtonvalidHandler=new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
                Selection_objetaudit_Choice();
                AUDIT audit_loaded=getAuditV2(Audit_Id_Selected);
                updateAudit(remarque.getText().toString(), year.getText().toString(),audit_loaded);
                //startActivity(new Intent(EditAudit.this,MenuAudit.class));

            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));

            }

        }
    };




    public void Selection_objetaudit_Choice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditAudit.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionEditAudit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Audit document
                        Intent intent = new Intent(EditAudit.this, EditDocument.class);
                        finish();
                       startActivity(intent);


                        break;
                    case 1: // Audit parcelle
                        //  Show_Dialog("Information", "En developpement");
                        Intent intent_parcelle = new Intent(EditAudit.this, AuditParcelle.class);
                        finish();
                        //startActivity(intent_parcelle);

                        break;
                    case 2: //Audit stockahe
                        Show_Dialog("Information", "En developpement");

                        //Verifie si il y a une photo
//                        Intent intentstock = new Intent(Audit.this, AuditStockage.class);
//                        finish();
//                        startActivity(intentstock);
                        break;

                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }
//

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditAudit.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

    //add audit
    private  void updateAudit(String remarque,String year,AUDIT auditSelected)
    {
        auditSelected.setREMARQUES_AUDITS(remarque);
        auditSelected.setDATE(stringtoDate(year));


    }

    //date
    public  Date stringtoDate(String date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return  convertedDate;
    }


    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditAudit.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
               // dialog.cancel();
                startActivity(new Intent(EditAudit.this,MenuAudit.class));



            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


   /* public void LoadingDialogeAuditDB(){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(EditAudit.this,
                getString(R.string.TitleChargement), getString(R.string.TextChargementStockeur), true);
        ringProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //  Load data from databse
                    Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
                    List <Audit> list_audit = ;
                    Visite    visite_input =  getVisite(Id_Visite_Selected,list_visite);
                    //update data into databse
                    Visite update_visit= UpdateVisiteIntoDatabase(visite_input,SpinnerAttitudeEntreprise.getSelectedItem().toString() ,SpinnerInteretVisite.getSelectedItem().toString());
                    UpdateSujetVisitePrincipalToVisite(visite_input,Prinicplesujet_Discussion.getText().toString().trim(), SpinnerObjetVisite.getSelectedItem().toString());
                    addSujetVisiteSecondaireToVisite(update_visit,Secondairesujet_Discussion.getText().toString().trim(), SpinnerObjetVisite.getSelectedItem().toString());
                } catch (Exception e) {
                }
                ringProgressDialog.dismiss();
                Intent intent = new Intent(EditAudit.this, MenuAudit.class);
                finish();
                startActivity(intent);
            }
        }).start();
    }*/


    //Get the list of visit in a Organization
  /*  private List<Audit> getListAudit(Entreprise entreprise_input){
        DaoSession daoSession=((AppController) getApplication()).getDaoSession();
        return entreprise_input.getVisiteList();
    }*/


    // Method to check if the user has entered the data

    private Boolean CheckInput(){
        boolean b = false;

        if (remarque!=null) {
            b = true;
        }
        if (year!=null) {
            b = true;
        }

        return b;
    }

    public Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }

    //Get the audit only with the ID common function
    private AUDIT getAuditV2(long ID_audit){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        AUDITDao audit_data = daoSession.getAUDITDao();
        AUDIT result = null;
        List<AUDIT> audit_load = audit_data.queryBuilder()
                .where(AUDITDao.Properties.Id.eq(ID_audit))
                .list();
        if (audit_load != null) {
            for (AUDIT A: audit_load) {

                result = A;
            }
        }
        return result;
    }


    //A method that creates a string listing the data entered.
    private String InputToText(){
        String text = getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.Remarque) + " : " + remarque.getText().toString() + "\n";
        text += getString(R.string.year) + " : " +year.getText().toString() + "\n";


        return text;
    }
}
