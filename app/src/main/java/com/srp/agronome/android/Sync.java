package com.srp.agronome.android;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.webservice.Service;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import static com.srp.agronome.android.CreateQuickContact.AddCONTACTID;
import static com.srp.agronome.android.CreateQuickContact.AddENTERPRISEID;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_compte_server;

/**
 * Created by sumitra on 1/22/2018.
 */

public class Sync extends AsyncTask<String, String, String> {
    Context context;

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(String... args) {

        HttpURLConnection urlConnection;
        OutputStream out;
        URL url;

        String result;
        int code = 0;
        //send id
        Log.i("putData", "doInBackground: new e id" + AddENTERPRISEID);
        Log.i("putData", "doInBackground: new c id" + AddCONTACTID);

        try {

            //create json data to send
            Log.i("putData", "doInBackground: start");
            DaoSession daoSession = ((AppController) ((Activity) context).getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> update_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();

            if (update_compte != null) {
                for (Compte c : update_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise) {
                        if (e.getId() == AddENTERPRISEID) {
                            Log.i("putData", "doInBackground:Enterprise added " + e.getId());
                            List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                            for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                if (ct.getId() == AddCONTACTID) {

                                    Log.i("putData", "contact added=" + ct.getId());
                                    Log.i("putData", "contact identificant=" + ct.getContact_identifiant());
                                    JSONObject datapost = new JSONObject(); //json data to be send
                                    datapost.put("raison_sociale", e.getRaison_sociale());
                                    datapost.put("archive", ct.getArchive());
                                    datapost.put("date_creation", ct.getDate_modification());
                                    datapost.put("date_modification", ct.getDate_creation());
                                    datapost.put("structure_sociale", e.getStructure_Sociale());
                                    // datapost.put("situation", e.getSituation().getNom());
                                    //  datapost.put("statut_soytouch", e.getStatut_Soytouch().getNom());
                                    // datapost.put("activite_principale", e.getActivite_Principale().getNom());
                                    //datapost.put("activite_secondaire", e.getActivite_Secondaire().getNom());
                                    //datapost.put("biologique", e.getBiologique().getNom());
                                    //datapost.put("type_bio", e.getType_BioList());
                                    //datapost.put("numero_nom_rue", e.getAdresse_Siege().getAdresse_numero_rue());
                                    //datapost.put("code_postale", e.getAdresse_Siege().getCode_Postal_Ville().getCode_postal());
                                    //datapost.put("ville", e.getAdresse_Siege().getCode_Postal_Ville().getVille());
                                    //datapost.put("pays", e.getAdresse_Siege().getPays().getNom());
                                    ///datapost.put("telephone_entreprise", e.getTelephone_entreprise());
                                    //datapost.put("fax", e.getFax_entreprise());
                                    datapost.put("adresse_mail_principale", e.getAdresse_email_principale());
                                    datapost.put("adresse_mail_secondaire", e.getAdresse_email_secondaire());
                                    String ct_id = ct.getContact_identifiant();
                                    datapost.put("nom", ct.getIdentite().getNom());
                                    datapost.put("prenom", ct.getIdentite().getPrenom());
                                    datapost.put("adresse_mail", ct.getIdentite().getAdresse_email());
                                    //datapost.put("sexe", ct.getIdentite().getSexe().getNom());
                                    datapost.put("telephone_principale", ct.getIdentite().getTelephone_principal());
                                    datapost.put("telephone_secondaire", ct.getIdentite().getTelephone_secondaire());

                                    // datapost.put("categorie", ct.getCategorie_Contact().getNom());
                                    // datapost.put("sociabilite", ct.getSociabilite().getNom());
                                    //datapost.put("remarque", ct.getRemarque_contact());
                                    datapost.put("id_compte", ID_compte_server);
                                    Log.i("putData", "doInBackground: datapost=" + datapost.toString());
                                    int i = 1;
                                    if (ct.getContact_identifiant() == null) {

                                        url = new URL(Service.ENDPOINT);
                                        Log.i("putData", "doInBackground: inside condition");

                                        urlConnection = (HttpURLConnection) url.openConnection();
                                        // httpCon.setDoOutput(true);
                                        //httpCon.setDoInput(true);
                                        //httpCon.setUseCaches(false);
                                        urlConnection.setRequestProperty("Content-Type", "application/json");
                                        //httpCon.setRequestProperty("Accept", "application/json");
                                        urlConnection.setRequestMethod("POST");
                                        out = new BufferedOutputStream(urlConnection.getOutputStream());
                                        //OutputStreamWriter out = new OutputStreamWriter(os, "UTF-8");
                                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                                        writer.write(datapost.toString());
                                        writer.flush();
                                        writer.close();
                                        Log.i("post", datapost.toString());

                                        out.close();
                                        urlConnection.connect();
                                        code = urlConnection.getResponseCode();
                                        Log.i("putData", "doInBackground: datasend code= " + code);
                                        i = 0;

                                    }

                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //return <code></code>
        result = String.valueOf(code);
        return result;

    }

    @Override
    protected void onPostExecute(String result) {
        Log.i("App", "debut post");
        if (result.equals("201")) {
            //codeRun=false;
        }

    }
}
