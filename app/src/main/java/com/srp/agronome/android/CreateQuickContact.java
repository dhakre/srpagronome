package com.srp.agronome.android;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import com.srp.agronome.android.db.*;
import com.srp.agronome.android.db.Contact;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Contact_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.ID_compte_server;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.Nom_Contact;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;
import static com.srp.agronome.android.MainLogin.Prenom_Contact;


/**
 * Created by doucoure on 20/06/2017.
 */

public class CreateQuickContact extends AppCompatActivity {

    private static final String TAG = "CREATEnewCONTACT";
    //size
    private static long justineList=210;
    private static long mathieuList=89;
    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    public static long AddCONTACTID;
    public static long AddENTERPRISEID;


    private ImageView Contact_Photo = null;
    private EditText First_Name = null;
    private final static String KeyFirstName = "QC_KeyFirstName";
    private EditText Last_Name = null;
    private final static String KeyLastName = "QC_KeyLastName";
    private EditText Organization = null;
    private final static String KeyOrganization = "QC_KeyOrganization";

    //Adresse Personnelle
    private EditText StreetAddress1 = null;
    private final static String KeyAddress1 = "QC_KeyAddress1";
    private EditText StreetAddress2 = null;
    private final static String KeyAddress2 = "QC_KeyAddress2";
    private EditText City1 = null;
    private final static String KeyCity1 = "QC_KeyCity";
    private EditText PostalCode1 = null;
    private final static String KeyPostalCode1 = "QC_KeyPostalCode";
    private EditText Country1 = null;
    private final static String KeyCountry1 = "QC_KeyCountry";

    private ImageView Geolocalisation_AdressePersonnelle = null;

    //Adresse Entreprise
    private TextView TitleAdresse2 = null;
    private TextView TitleAdress2Identical = null;
    private Switch SwitchAdressIdentical = null;

    private EditText StreetAddress3 = null;
    private final static String KeyAddress3 = "QC_KeyAddress3";
    private EditText StreetAddress4 = null;
    private final static String KeyAddress4 = "QC_KeyAddress4";
    private EditText City2 = null;
    private final static String KeyCity2 = "QC_KeyCity_2";
    private EditText PostalCode2 = null;
    private final static String KeyPostalCode2 = "QC_KeyPostalCode_2";
    private EditText Country2 = null;
    private final static String KeyCountry2 = "QC_KeyCountry_2";

    private ImageView Geolocalisation_AdresseEntreprise = null;

    //Telephone
    private EditText PhoneNumber = null;
    private final static String KeyPhoneNumber = "QC_KeyPhoneNumber";


    /**
     * Image bouton pour valider la saisie
     */
    private ImageButton imgBtnValidate = null;
    /**
     * Image bouton pour annuler
     */
    private ImageButton imgBtnHome = null;

    private Button BtnValidate = null;

    private Spinner Spinner_Entreprise = null;
    private Spinner Spinner_Metier = null;

    private static final int GALLERY_PICK = 1;
    private static final int CAMERA_PIC_REQUEST = 2;
    private static final int PICK_CROP = 3;

    private double latitude_adresse_personnnelle = 0;
    private double longitude_adresse_personnnelle = 0;

    private double latitude_adresse_entreprise = 0;
    private double longitude_adresse_entreprise = 0;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quick_contact);
        FindWidgetViewbyId(); //Associate Widgets
        AddListenerEvent(); //Add Widgets Event
        GetStringData(); //Load String
        CreateListMetier(); //Create list of Metier in DB
        LoadCompanyInSpinner(); //Load spinner data
        LoadSpinnerMetier();
        displayContactInLog();


        //Gerer le passage d'un widget à l'autre sur un spinner
        First_Name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_NEXT)){
                    Spinner_Metier.performClick();
                }
                return false;
            }
        });


        imgBtnValidate = (ImageButton) findViewById(R.id.btn_ajoutContact);
        imgBtnValidate.setOnClickListener(ButtonValidateHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menu_annuaire);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

        BtnValidate = (Button) findViewById(R.id.btnvalidate_CQ);
        BtnValidate.setOnClickListener(ButtonValidateHandler);

        SwitchAdressIdentical.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean checked) {
                if (checked) {
                    TitleAdresse2.setVisibility(View.GONE);
                    Geolocalisation_AdresseEntreprise.setVisibility(View.GONE);
                    StreetAddress3.setVisibility(View.GONE);
                    StreetAddress4.setVisibility(View.GONE);
                    City2.setVisibility(View.GONE);
                    PostalCode2.setVisibility(View.GONE);
                    Country2.setVisibility(View.GONE);
                } else {
                    TitleAdresse2.setVisibility(View.VISIBLE);
                    Geolocalisation_AdresseEntreprise.setVisibility(View.VISIBLE);
                    StreetAddress3.setVisibility(View.VISIBLE);
                    StreetAddress4.setVisibility(View.VISIBLE);
                    City2.setVisibility(View.VISIBLE);
                    PostalCode2.setVisibility(View.VISIBLE);
                    Country2.setVisibility(View.VISIBLE);
                    StreetAddress3.setText("", TextView.BufferType.EDITABLE);
                    StreetAddress4.setText("", TextView.BufferType.EDITABLE);
                    City2.setText("", TextView.BufferType.EDITABLE);
                    PostalCode2.setText("", TextView.BufferType.EDITABLE);
                    Country2.setText("", TextView.BufferType.EDITABLE);
                }
            }
        });
    }




    private void FindWidgetViewbyId() {
        Contact_Photo = (ImageView) findViewById(R.id.contact_picture);
        Last_Name = (EditText) findViewById(R.id.ContactQ_LastName);
        First_Name = (EditText) findViewById(R.id.ContactQ_FirstName);
        Spinner_Metier = (Spinner) findViewById(R.id.SpinnerMetier);
        Spinner_Entreprise = (Spinner) findViewById(R.id.SpinnerOrganization);
        Organization = (EditText) findViewById(R.id.QC_Organization);

        StreetAddress1 = (EditText) findViewById(R.id.CQAddress1);
        StreetAddress2 = (EditText) findViewById(R.id.CQAddress2);
        City1 = (EditText) findViewById(R.id.CQCity1);
        PostalCode1 = (EditText) findViewById(R.id.CQPostalCode1);
        Country1 = (EditText) findViewById(R.id.CQCountry1);
        Geolocalisation_AdressePersonnelle = (ImageView) findViewById(R.id.BtnGeolocalisation_AdressePersonnelle);

        TitleAdress2Identical = (TextView) findViewById(R.id.TextAdressCQ2);
        SwitchAdressIdentical = (Switch)  findViewById(R.id.SwitchCompanyAddress);
        TitleAdresse2 = (TextView) findViewById(R.id.ContactQ_address2);
        StreetAddress3 = (EditText) findViewById(R.id.CQAddress3);
        StreetAddress4 = (EditText) findViewById(R.id.CQAddress4);
        City2 = (EditText) findViewById(R.id.CQCity2);
        PostalCode2 = (EditText) findViewById(R.id.CQPostalCode2);
        Country2 = (EditText) findViewById(R.id.CQCountry2);
        Geolocalisation_AdresseEntreprise = (ImageView) findViewById(R.id.BtnGeolocalisation_AdresseEntreprise);
        PhoneNumber = (EditText) findViewById(R.id.CQPhoneNumber);
    }


    private void AddListenerEvent() {
        Contact_Photo.setOnClickListener(PhotoHandler);
        Last_Name.addTextChangedListener(generalTextWatcher);
        First_Name.addTextChangedListener(generalTextWatcher);
        Organization.addTextChangedListener(generalTextWatcher);

        StreetAddress1.addTextChangedListener(generalTextWatcher);
        StreetAddress2.addTextChangedListener(generalTextWatcher);
        City1.addTextChangedListener(generalTextWatcher);
        PostalCode1.addTextChangedListener(generalTextWatcher);
        Country1.addTextChangedListener(generalTextWatcher);
        Geolocalisation_AdressePersonnelle.setOnClickListener(Handler_Geolocalisation_Adresse_Personnelle);

        StreetAddress3.addTextChangedListener(generalTextWatcher);
        StreetAddress4.addTextChangedListener(generalTextWatcher);
        City2.addTextChangedListener(generalTextWatcher);
        PostalCode2.addTextChangedListener(generalTextWatcher);
        Country2.addTextChangedListener(generalTextWatcher);
        Geolocalisation_AdresseEntreprise.setOnClickListener(Handler_Geolocalisation_Adresse_Entreprise);

        PhoneNumber.addTextChangedListener(generalTextWatcher);
    }

    //Event Multiple EditText = save data
    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (Last_Name.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyLastName, Last_Name.getText().toString());
            } else if (First_Name.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyFirstName, First_Name.getText().toString());
            } else if (Organization.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyOrganization, Organization.getText().toString());

            } else if (StreetAddress1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAddress1, StreetAddress1.getText().toString());
            } else if (StreetAddress2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAddress2, StreetAddress2.getText().toString());
            } else if (City1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCity1, City1.getText().toString());
            } else if (PostalCode1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPostalCode1, PostalCode1.getText().toString());
            } else if (Country1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCountry1, Country1.getText().toString());

            } else if (StreetAddress3.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAddress3, StreetAddress3.getText().toString());
            } else if (StreetAddress4.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAddress4, StreetAddress4.getText().toString());
            } else if (City2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCity2, City2.getText().toString());
            } else if (PostalCode2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPostalCode2, PostalCode2.getText().toString());
            } else if (Country2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCountry2, Country2.getText().toString());

            } else if (PhoneNumber.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPhoneNumber, PhoneNumber.getText().toString());

            }
        }

    };


    private void SaveStringDataInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }



    // Lire les données enregistrées et les mettre dans les definir dans les edit text
    private void GetStringData() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
        if (sharedpreferences.contains(KeyLastName)) {
            Last_Name.setText(sharedpreferences.getString(KeyLastName, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyFirstName)) {
            First_Name.setText(sharedpreferences.getString(KeyFirstName, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyOrganization)) {
            Organization.setText(sharedpreferences.getString(KeyOrganization, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAddress1)) {
            StreetAddress1.setText(sharedpreferences.getString(KeyAddress1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAddress2)) {
            StreetAddress2.setText(sharedpreferences.getString(KeyAddress2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCity1)) {
            City1.setText(sharedpreferences.getString(KeyCity1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPostalCode1)) {
            PostalCode1.setText(sharedpreferences.getString(KeyPostalCode1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCountry1)) {
            Country1.setText(sharedpreferences.getString(KeyCountry1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAddress3)) {
            StreetAddress3.setText(sharedpreferences.getString(KeyAddress3, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAddress4)) {
            StreetAddress4.setText(sharedpreferences.getString(KeyAddress4, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCity2)) {
            City2.setText(sharedpreferences.getString(KeyCity2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPostalCode2)) {
            PostalCode2.setText(sharedpreferences.getString(KeyPostalCode2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCountry2)) {
            Country2.setText(sharedpreferences.getString(KeyCountry2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPhoneNumber)) {
            PhoneNumber.setText(sharedpreferences.getString(KeyPhoneNumber, ""), TextView.BufferType.EDITABLE);
        }

    }


    //Clear data in sharepreference
    private void clearData() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(KeyLastName);
        editor.remove(KeyFirstName);
        editor.remove(KeyOrganization);

        editor.remove(KeyAddress1);
        editor.remove(KeyAddress2);
        editor.remove(KeyCity1);
        editor.remove(KeyPostalCode1);
        editor.remove(KeyCountry1);

        editor.remove(KeyAddress3);
        editor.remove(KeyAddress4);
        editor.remove(KeyCity2);
        editor.remove(KeyPostalCode2);
        editor.remove(KeyCountry2);

        editor.remove(KeyPhoneNumber);

        editor.apply();
    }


    View.OnClickListener Handler_Geolocalisation_Adresse_Personnelle = new View.OnClickListener() {
        public void onClick(View v) {
            // create class object
            GPSTracker gps = new GPSTracker(CreateQuickContact.this);
            // check if GPS enabled
            if (gps.canGetLocation()) {
                latitude_adresse_personnnelle = gps.getLatitude();
                longitude_adresse_personnnelle = gps.getLongitude();
                //Display Values
                Toast.makeText(getApplicationContext(), getString(R.string.TitleLocationFound) + "\n" +
                        getString(R.string.TextLatitude) + " " + latitude_adresse_personnnelle + "\n" +
                                getString(R.string.TextLongitude) + " " + longitude_adresse_personnnelle
                        , Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    };



    View.OnClickListener Handler_Geolocalisation_Adresse_Entreprise = new View.OnClickListener() {
        public void onClick(View v) {
            // create class object
            GPSTracker gps = new GPSTracker(CreateQuickContact.this);
            // check if GPS enabled
            if (gps.canGetLocation()) {
                latitude_adresse_entreprise = gps.getLatitude();
                longitude_adresse_entreprise = gps.getLongitude();
                //Display Values
                Toast.makeText(getApplicationContext(), getString(R.string.TitleLocationFound) + "\n" +
                                getString(R.string.TextLatitude) + " " + latitude_adresse_entreprise + "\n" +
                                getString(R.string.TextLongitude) + " " + longitude_adresse_entreprise
                        , Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    };


    //Event Clic on Photo :
    View.OnClickListener PhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Selection_Photo_Choice();
        }
    };

    //Afficher une boîte de dialogue contenant une liste de choix pour affecter une photo
    public void Selection_Photo_Choice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateQuickContact.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionPhoto, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Prendre une photo
                        TakePicturefromCamera();
                        break;
                    case 1: //Choisir une photo depuis la galerie
                        pickImagefromGallery();
                        break;
                    case 2: //Rogner la photo affichée
                        //Verifie si il y a une photo
                        if (Contact_Photo.getDrawable().getConstantState() != CreateQuickContact.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            CropImage();
                        }
                        break;
                    case 3: //Supprimer la photo -> mettre l'image de base à la place
                        Contact_Photo.setImageResource(R.mipmap.ic_add_photo);
                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }


    //Pick a photo from Gallery
    public void pickImagefromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICK);
    }

    //Take a photo from Camera
    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile("cache_picture_HD.jpg");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Cache");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        return file;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == GALLERY_PICK) { //Depuis la galerie
            Uri uri = data.getData();
            Bitmap Picture_Load;
            try {
                Picture_Load = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                Picture_Load = rotateImageIfRequired(Picture_Load,uri,true); //Rotation si necessaire
                SaveImage(Picture_Load, "cache_picture_HD.jpg");
                Picture_Load = getResizedBitmap(Picture_Load, 256, 256);
                Contact_Photo.setImageBitmap(Picture_Load);
                SaveImage(Picture_Load, "cache_picture.jpg"); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_PIC_REQUEST) { //Depuis la caméra : pas de bitmap en sortie
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Cache");
            File file = new File(myDir, "cache_picture_HD.jpg");
            Uri uri = Uri.fromFile(file);
            Bitmap newPicture;
            try {
                newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
                newPicture = rotateImageIfRequired(newPicture,uri,false); //Rotation si necessaire
                SaveImage(newPicture, "cache_picture_HD.jpg");
                newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
                Contact_Photo.setImageBitmap(newPicture);
                SaveImage(newPicture, "cache_picture.jpg"); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_CROP) {
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Cache");
            File file = new File(myDir,"cache_picture.jpg");
            Uri URI = Uri.fromFile(file);
            Bitmap Picture_Crop;
            try {
                Picture_Crop = MediaStore.Images.Media.getBitmap(getContentResolver(), URI);
                Contact_Photo.setImageBitmap(Picture_Crop);
                SaveImage(Picture_Crop, "cache_picture.jpg"); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Permet de rogner ou recadrer une image
    private void CropImage() {
        try {
            //Acceder à la photo à rogner
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Cache");
            File photoFile = new File(myDir, "cache_picture.jpg");
            Uri photoURI = FileProvider.getUriForFile(this,
                    "com.srp.android.fileprovider",
                    photoFile);
            getApplicationContext().grantUriPermission("com.android.camera", photoURI,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //Paramètres de l'Intent Crop
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //Android N need set permission to uri otherwise system camera don't has permission to access file wait crop
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // indicate image type and Uri
            cropIntent.setDataAndType(photoURI, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PICK_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
        }
    }


    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Cache");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        //Log.i("File Location",file.toString());
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage, boolean PickingGallery) throws IOException {

        InputStream input = getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
        if (PickingGallery){
            ei = new ExifInterface(getPath(selectedImage));}
        else{
            ei =  new ExifInterface(selectedImage.getPath());}

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    //Get Path = Pick up a picture in Gallery = getRealPath
    private String getPath(Uri uri) {
        String[]  data = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    //Get the path of the directory : Intern or SD path
    private File getDirectoryPath(){
        SharedPreferences SP = getSharedPreferences("PrefValues", 0); // 0 = Private Mode
        String location = SP.getString("LocationStorage",null);
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (location.equals("Intern")){
            return Dirs[0];}
        else{
            return Dirs[1];}
    }

    //Rotate an image with angle in degree
    private Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        Log.i("Info Rotation","Rotation de l image de : " + degree);
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }

    //Copie un fichier source vers un fichier de destination
    public void CopyAndDeleteFileCache(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = new FileInputStream(sourceFile).getChannel();
        FileChannel destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size()); //Copie le fichier
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
        sourceFile.delete(); //Supprime le fichier une fois qu'il a ete copié
    }


    private void CreateListMetier() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        MetierDao metier_data = daoSession.getMetierDao();

        List<Metier> List_Metier = metier_data.loadAll();
        if (List_Metier.size() == 0) {

            Metier metier_input = new Metier();
            metier_input.setNom("Agriculteur");
            metier_data.insertOrReplace(metier_input);
        }
    }

    //Charger le spinner Metier
    private void LoadSpinnerMetier() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        MetierDao metier_data = daoSession.getMetierDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter.add(getString(R.string.TextAnotherMetier)); //Text Add a new structure sociale
        //Charger les éléments de la liste
        List<Metier> List_Metier = metier_data.loadAll();
        if (List_Metier != null) {
            for (Metier M : List_Metier) {
                adapter.add(M.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(getString(R.string.TextMetier)); //This is the text that will be displayed as hint.
        Spinner_Metier.setAdapter(adapter);
        Spinner_Metier.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_Metier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidateMetier),getString(R.string.TextInputMetier),1);
            }
            else{
                Spinner_Entreprise.performClick();
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    private void AddMetier(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        MetierDao metier_data = daoSession.getMetierDao();
        Metier metier_input = new Metier();
        metier_input.setNom(Nom);
        metier_data.insertOrReplace(metier_input);
        LoadSpinnerMetier();
    }

    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateQuickContact.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code Structure Sociale
                        AddMetier(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Charger le spinner Entreprise
    private void LoadCompanyInSpinner(){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView)v.findViewById(android.R.id.text1)).setText("");
                    ((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount()-1; // you dont display last item. It is used as hint.
            }

        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAddCompany));
        //Charger la liste des entreprises
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getArchive() != 0){
                    adapter.add(e.getRaison_sociale());}
                }
            }
        }
        adapter.add(getString(R.string.TextSpinner)); //This is the text that will be displayed as hint.


        Spinner_Entreprise.setAdapter(adapter);
        Spinner_Entreprise.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

        Spinner_Entreprise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
//                Log.i("Information Position","Position : " + position);
//                Log.i("Information Longeur", "Longeur : " + Spinner_Entreprise.getCount());

                if (position == 0 ){ //Cas où l'on ajoute une entreprise
                    TitleAdress2Identical.setVisibility(View.VISIBLE);
                    SwitchAdressIdentical.setVisibility(View.VISIBLE);
                    Organization.setVisibility(View.VISIBLE);
                }
                else { //Cas de sélection d'une entreprise
                    TitleAdress2Identical.setVisibility(View.GONE);
                    SwitchAdressIdentical.setVisibility(View.GONE);
                    Organization.setVisibility(View.GONE);
                    TitleAdresse2.setVisibility(View.GONE);
                    Geolocalisation_AdresseEntreprise.setVisibility(View.GONE);
                    StreetAddress3.setVisibility(View.GONE);
                    StreetAddress4.setVisibility(View.GONE);
                    City2.setVisibility(View.GONE);
                    PostalCode2.setVisibility(View.GONE);
                    Country2.setVisibility(View.GONE);

                    StreetAddress3.setText("", TextView.BufferType.EDITABLE);
                    StreetAddress4.setText("", TextView.BufferType.EDITABLE);
                    City2.setText("", TextView.BufferType.EDITABLE);
                    PostalCode2.setText("", TextView.BufferType.EDITABLE);
                    Country2.setText("", TextView.BufferType.EDITABLE);
                    Organization.setText("", TextView.BufferType.EDITABLE);
                }
            }

            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub
            }
        });
    }



    private boolean CheckInput() {
        boolean b = true;

        if (TextUtils.isEmpty(Last_Name.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(First_Name.getText().toString())) {
            b = false;
        }
        if (Spinner_Metier.getSelectedItem().toString().equals(getString(R.string.TextMetier))) {
            b = false;
        }
        if (Spinner_Entreprise.getSelectedItem().toString().equals(getString(R.string.TextSpinner))) {
            b = false;
        }
        if (Spinner_Entreprise.getSelectedItem().toString().equals(getString(R.string.TextAddCompany))&&(TextUtils.isEmpty(Organization.getText().toString())) ) {
            b = false;
        }

        if (TextUtils.isEmpty(PhoneNumber.getText().toString())) {
            b = false;
        }
        return b;
    }


    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {

                String number = PhoneNumber.getText().toString().trim();


                String MobilePattern="";

                //Pattern selon Pays de selection :
                sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
                String Format_Pays = sharedpreferences.getString("FormatCode","FR");
                switch (Format_Pays){
                    case "FR" : {
                        MobilePattern = "\"(0|\\\\+33|0033)[1-9][0-9]{8}\"";
                        break;}
                    case "US" :{
                        MobilePattern = "^[0-9]{3,3}[-]{1,1}[0-9]{3,3}[-]{1,1}[0-9]{4,4}$";
                        break;}
                }

                MobilePattern = "[0-9]{10}"; //Ancien a supprimer si les tests sont bons

                if (number.matches(MobilePattern)) {
                    Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm), InputToText());
                } else {
                    Show_Dialog(getString(R.string.warntitle), getString(R.string.warnphone));
                }
            }
            else {
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();
            }

        }
    };

    private String InputToText(){
        String result = getString(R.string.textConfirm);
        result += "\n" + getString(R.string.TextLastName) + " : " + Last_Name.getText().toString();
        result += "\n" + getString(R.string.TextFirstName) + " : " + First_Name.getText().toString();
        result += "\n" + getString(R.string.TextValidateMetier) + " : " + Spinner_Metier.getSelectedItem().toString();

        if ((TextUtils.isEmpty(Organization.getText().toString()))){
            result += "\n" + getString(R.string.TextOrganization) + " : " + Spinner_Entreprise.getSelectedItem().toString() + "\n";}
        else{
            result += "\n" + getString(R.string.TextOrganization) + " : " + Organization.getText().toString() + "\n";}

        result += "\n" + getString(R.string.TitlePersonnalAddress);
        result += "\n" + getString(R.string.TextAddress) + " : " + StreetAddress1.getText().toString();
        result += "\n" + getString(R.string.TextAddress2) + " : " + StreetAddress2.getText().toString();
        result += "\n" + getString(R.string.TextCity) + " : " + City1.getText().toString();
        result += "\n" + getString(R.string.TextPostalCode) + " : " + PostalCode1.getText().toString();
        result += "\n" + getString(R.string.TextCountry) + " : " + Country1.getText().toString() + "\n";
        result +=  "\n" + getString(R.string.TitleCompanyAddress);
        if (!(TextUtils.isEmpty(Organization.getText().toString()))){
        if (SwitchAdressIdentical.isChecked()){
            result += "\n" + getString(R.string.TextAddress) + " : " + StreetAddress1.getText().toString();
            result += "\n" + getString(R.string.TextAddress2) + " : " + StreetAddress2.getText().toString();
            result += "\n" + getString(R.string.TextCity) + " : " + City1.getText().toString();
            result += "\n" + getString(R.string.TextPostalCode) + " : " + PostalCode1.getText().toString();
            result += "\n" + getString(R.string.TextCountry) + " : " + Country1.getText().toString() + "\n";
        }
        else {
            result += "\n" + getString(R.string.TextAddress) + " : " + StreetAddress3.getText().toString();
            result += "\n" + getString(R.string.TextAddress2) + " : " + StreetAddress4.getText().toString();
            result += "\n" + getString(R.string.TextCity) + " : " + City2.getText().toString();
            result += "\n" + getString(R.string.TextPostalCode) + " : " + PostalCode2.getText().toString();
            result += "\n" + getString(R.string.TextCountry) + " : " + Country2.getText().toString() + "\n";
        }}
        result += "\n" + getString(R.string.TextPhoneNumberQC) + " : " + PhoneNumber.getText().toString();
        return result;
    }

    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };


    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateQuickContact.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                CreateContactIntoDataBase();   //id= -1
                displayContactInLog();

                startActivity(new Intent(CreateQuickContact.this,ListContact.class));
               Show_Dialog_ContactDetaille(getString(R.string.TitreContactDetaille),getString(R.string.TextContactDetaille));
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Display Dialog go to Contact Detaillee
    private void Show_Dialog_ContactDetaille(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateQuickContact.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.TextYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(CreateQuickContact.this, EditContact.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.TextNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Show_Dialog_EntrepriseDetaille(getString(R.string.TitreEntrepriseDetaille),getString(R.string.TextEntrepriseDetaille));
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Display Dialog go to Entreprise detaillé
    private void Show_Dialog_EntrepriseDetaille(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateQuickContact.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.TextYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(CreateQuickContact.this, EditOrganization.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.TextNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(CreateQuickContact.this, ContactMenu.class);
                finish();
                startActivity(intent);

            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
        return true;
    }

    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateQuickContact.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(CreateQuickContact.this, ContactMenu.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                Intent intent = new Intent(CreateQuickContact.this, ContactMenu.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateQuickContact.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewQC);
        int position = 0 ;

        if (TextUtils.isEmpty(PhoneNumber.getText().toString())) {
            position = PhoneNumber.getTop();
        }
        if (Spinner_Entreprise.getSelectedItem().toString().equals(getString(R.string.TextAddCompany))&&(TextUtils.isEmpty(Organization.getText().toString())) ) {
            position = Organization.getTop();
        }
        if (Spinner_Entreprise.getSelectedItem().toString().equals(getString(R.string.TextSpinner))) {
            position = Spinner_Entreprise.getTop();
        }
        if (Spinner_Metier.getSelectedItem().toString().equals(getString(R.string.TextMetier))) {
            position = Spinner_Metier.getTop();
        }
        if (TextUtils.isEmpty(First_Name.getText().toString())) {
            position = First_Name.getTop();
        }
        if (TextUtils.isEmpty(Last_Name.getText().toString())) {
            position = Last_Name.getTop();
        }

        final int Position_Y = position;
        final int Position_X = 0;
        sc.post(new Runnable() {
            public void run() {
                sc.scrollTo(Position_X, Position_Y); // these are your x and y coordinates
            }
        });
    }

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }


    public static boolean checkImageResource(Context ctx, ImageView imageView, int imageResource) {
        boolean result = false;
        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }
            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }
        return result;
    }

    private void CreateDirectoryOrganization(String OrganizationName){
        File OrganizationDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + OrganizationName + "/Contacts" );
        if (!OrganizationDir.exists()) {
            if (!OrganizationDir.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
        File OrganizationDir2 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + OrganizationName + "/Audits" );
        if (!OrganizationDir2.exists()) {
            if (!OrganizationDir2.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
        File OrganizationDir3 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + OrganizationName + "/Parcelles" );
        if (!OrganizationDir3.exists()) {
            if (!OrganizationDir3.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
        File OrganizationDir4 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + OrganizationName + "/Stockeurs" );
        if (!OrganizationDir4.exists()) {
            if (!OrganizationDir4.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
    }

    //Permet de retourner les 3 premiers caracteres d'un texte
    private String getSubString(String chaine){
        String result="";
        if (chaine.length() >= 3){ result = chaine.substring(0,3);}
        if (chaine.length() == 2){ result = chaine.substring(0,2);}
        if (chaine.length() == 1){ result = chaine.substring(0,1);}
        return result ;
    }


    private void CreateContactIntoDataBase() {
        Log.i(TAG, "CreateContactIntoDataBase: creating contact in DB");
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        IdentiteDao identite_data = daoSession.getIdentiteDao();
        AdresseDao adresse_data = daoSession.getAdresseDao();
        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
        PaysDao pays_data = daoSession.getPaysDao();
        MetierDao metier_data = daoSession.getMetierDao();
        ContactDao contact_data = daoSession.getContactDao();
        GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();

        if (!(TextUtils.isEmpty(Organization.getText().toString()))) {
            CreateEntrepriseIntoDatabase();
        }

        //Adresse_personnel = Adresse de personnelle
        Adresse adresse_personnelle_input = new Adresse();
        adresse_personnelle_input.setAdresse_numero_rue(StreetAddress1.getText().toString().trim()); //Adresse
        adresse_personnelle_input.setComplement_adresse(StreetAddress2.getText().toString().trim()); //Complement Adresse
        adresse_personnelle_input.setArchive(0);
        adresse_personnelle_input.setDate_creation(Today_Date());
        adresse_personnelle_input.setDate_modification(Today_Date());
        adresse_personnelle_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        adresse_personnelle_input.setID_Agronome_Modification((int)ID_Compte_Selected);

        //Code postal adresse personnelle
        Code_Postal_Ville code_postal_ville_personnelle_input = new Code_Postal_Ville();
        code_postal_ville_personnelle_input.setCode_postal(PostalCode1.getText().toString().trim());
        code_postal_ville_personnelle_input.setVille(City1.getText().toString().trim());
        code_postal_ville_data.insertOrReplace(code_postal_ville_personnelle_input);

        //Pays adresse personnelle
        Pays pays_personnelle_input = new Pays();
        pays_personnelle_input.setNom(Country1.getText().toString().trim());
        pays_data.insertOrReplace(pays_personnelle_input);

        //Geolocalisation adresse personnelle
        if ((latitude_adresse_personnnelle != 0)&(longitude_adresse_personnnelle !=0)){
            Geolocalisation localisation_adresse_personnnelle_input = new Geolocalisation();
            localisation_adresse_personnnelle_input.setLatitude(latitude_adresse_personnnelle);
            localisation_adresse_personnnelle_input.setLongitude(longitude_adresse_personnnelle);
            geolocalisation_data.insertOrReplace(localisation_adresse_personnnelle_input);
            adresse_personnelle_input.setGeolocalisation(localisation_adresse_personnnelle_input);
            adresse_personnelle_input.setID_Geolocalisation(localisation_adresse_personnnelle_input.getId());
        }

        //Table identité
        Identite identite_input = new Identite();
        identite_input.setNom(Last_Name.getText().toString().trim()); //Nom
        identite_input.setPrenom(First_Name.getText().toString().trim()); //Prenom
        identite_input.setTelephone_principal(PhoneNumber.getText().toString().trim()); //Telephone principale
        identite_input.setArchive(0);
        identite_input.setDate_creation(Today_Date());
        identite_input.setDate_modification(Today_Date());
        identite_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        identite_input.setID_Agronome_Modification((int)ID_Compte_Selected);

        Nom_Contact = Last_Name.getText().toString().trim();
        Prenom_Contact = First_Name.getText().toString().trim();

        //Gestion de la photo
        //Si la photo est différente de la photo de base
        if (!(checkImageResource(this,Contact_Photo,R.mipmap.ic_add_photo))){
            String TextOrganization = "";
            if (TextUtils.isEmpty(Organization.getText().toString())) {
                TextOrganization = Spinner_Entreprise.getSelectedItem().toString();
            }
            else{
                TextOrganization = Organization.getText().toString();
            }

            //Photo basse qualité
            File myDir_Source1 = new File(getDirectoryPath(), "SRP_Agronome/Cache");
            File photoFile_Source1 = new File(myDir_Source1, "cache_picture.jpg");

            File myDir_Dest1 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + TextOrganization + "/Contacts");
            String PhotoName_LQ = getSubString(Nom_Compte) + "_" +
                    TextOrganization + "_C_" +
                    getSubString(Nom_Contact) + "_" +
                    getSubString(Prenom_Contact) + "_LQ.jpg";

            File photoFile_Dest1 = new File(myDir_Dest1, PhotoName_LQ);
            try {
                CopyAndDeleteFileCache(photoFile_Source1,photoFile_Dest1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Photo haute qualité
            File myDir_Source2 = new File(getDirectoryPath(), "SRP_Agronome/Cache");
            File photoFile_Source2 = new File(myDir_Source2, "cache_picture_HD.jpg");

            File myDir_Dest2 = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + TextOrganization + "/Contacts");
            String PhotoName_HQ = getSubString(Nom_Compte) + "_" +
                    TextOrganization + "_C_" +
                    getSubString(Nom_Contact) + "_" +
                    getSubString(Prenom_Contact) + "_HQ.jpg";

            File photoFile_Dest2 = new File(myDir_Dest2, PhotoName_HQ);
            try {
                CopyAndDeleteFileCache(photoFile_Source2,photoFile_Dest2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            identite_input.setPhoto(PhotoName_LQ);
        }
        else{
            identite_input.setPhoto("");
        }

        //Table Contact
        com.srp.agronome.android.db.Contact contact_input = new com.srp.agronome.android.db.Contact();
        contact_input.setArchive(0);
        contact_input.setDate_creation(Today_Date());
        contact_input.setDate_modification(Today_Date());
        contact_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        contact_input.setID_Agronome_Modification((int)ID_Compte_Selected);
       // contact_input.setId(mathieuList+1);

        //Ajout de la table Metier
        List<Metier> List_Metier = metier_data.queryBuilder()
                .where(MetierDao.Properties.Nom.eq(Spinner_Metier.getSelectedItem().toString()))
                .list();
        if (List_Metier != null) {
            for (Metier M : List_Metier) {
                contact_input.setMetier(M);
                contact_input.setID_Metier(M.getId());
            }
        }

        //Cardinalités entre les tables :
        adresse_personnelle_input.setCode_Postal_Ville(code_postal_ville_personnelle_input);
        adresse_personnelle_input.setID_Code_Postal_Ville(code_postal_ville_personnelle_input.getId());
        adresse_personnelle_input.setPays(pays_personnelle_input);
        adresse_personnelle_input.setID_Pays(pays_personnelle_input.getId());



        adresse_data.insertOrReplace(adresse_personnelle_input);

        identite_input.setAdresse(adresse_personnelle_input);
        identite_input.setID_Adresse(adresse_personnelle_input.getId());
        identite_data.insertOrReplace(identite_input);

        //Un contact possède une identité
        contact_input.setIdentite(identite_input);
        contact_input.setID_Identite_Contact(identite_input.getId());



        //L'entreprise est existe déjà, ajout de contact sur l'entreprise
        if (TextUtils.isEmpty(Organization.getText().toString())) {
            Nom_Entreprise = Spinner_Entreprise.getSelectedItem().toString();
            List<Compte> update_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (update_compte != null) {
                for (Compte c : update_compte) {
                    List <Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                    for (Entreprise e : List_Company){
                        if (e.getRaison_sociale().equals(Spinner_Entreprise.getSelectedItem().toString())){
                            ID_Entreprise_Selected = e.getId();
                           // ID_Contact_Selected = getIdNewContact(ID_Entreprise_Selected,Nom_Contact,Prenom_Contact);
                            ID_Contact_Selected=getIdSize(ID_compte_server);
                            contact_input.setID_Entreprise(e.getId());
                            contact_input.setId(ID_Contact_Selected++);
                            AddCONTACTID=contact_input.getId();
                            AddENTERPRISEID=e.getId();
                            long newID=contact_data.insertOrReplace(contact_input);
                            Log.i("NEWID", "CreateContactIntoDataBase: "+newID);
                            e.resetContactList();
                            entreprise_data.update(e);
                        }
                    }
                    compte_dao.update(c);

                }
            }
        }
        else { //L'entreprise n'existe pas, elle vient d'être crée.
            Nom_Entreprise = Organization.getText().toString().trim();
            List<Compte> update_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (update_compte != null) {
                for (Compte c : update_compte) {
                    List <Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                    for (Entreprise e : List_Company){
                        if (e.getRaison_sociale().equals(Organization.getText().toString())){
                            ID_Entreprise_Selected = e.getId();
                            //get id for contact
                            //ID_Contact_Selected = getIdNewContact(ID_Entreprise_Selected,Nom_Contact,Prenom_Contact);
                            ID_Contact_Selected=justineList++;
                            contact_input.setId(ID_Contact_Selected++);
                            contact_input.setID_Entreprise(e.getId());
                            contact_data.insertOrReplace(contact_input);
                            AddCONTACTID=contact_input.getId();
                            AddENTERPRISEID=e.getId();
                            e.resetContactList();
                            entreprise_data.update(e);
                        }
                    }
                    compte_dao.update(c);
                }
            }
        }
      //  ID_Contact_Selected = getIdNewContact(ID_Entreprise_Selected,Nom_Contact,Prenom_Contact);  //here the contact has id
    }

    private long getIdNewContact(long Id_Entreprise, String Nom , String Prenom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        long result = 0;
        List<Compte> list_account = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_account != null) {
            for (Compte c : list_account) {
                List<Entreprise> List_Company = c.getEntrepriseList(); //get All Company
                for (Entreprise e : List_Company) {
                    if (e.getId() == Id_Entreprise){
                        List <Contact> list_contact = e.getContactList();
                        for (Contact ct : list_contact){
                            if ((ct.getIdentite().getNom().equals(Nom))&(ct.getIdentite().getPrenom().equals(Prenom))){
                                result = ct.getId();
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private void CreateEntrepriseIntoDatabase(){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        AdresseDao adresse_data = daoSession.getAdresseDao();
        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
        PaysDao pays_data = daoSession.getPaysDao();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
        GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();

        //Table Entreprise
        Entreprise entreprise_input = new Entreprise();
        entreprise_input.setRaison_sociale(Organization.getText().toString().trim());
        entreprise_input.setArchive(0);
        entreprise_input.setDate_creation(Today_Date());
        entreprise_input.setDate_modification(Today_Date());
        entreprise_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        entreprise_input.setID_Agronome_Modification((int)ID_Compte_Selected);

        if ( SwitchAdressIdentical.isChecked() ){
            //Adresse facturation = Adresse de l'entreprise
            Adresse adresse_facturation_input = new Adresse();
            adresse_facturation_input.setAdresse_numero_rue(StreetAddress1.getText().toString().trim()); //Adresse
            adresse_facturation_input.setComplement_adresse(StreetAddress2.getText().toString().trim()); //Complement Adresse
            adresse_facturation_input.setArchive(0);
            adresse_facturation_input.setDate_creation(Today_Date());
            adresse_facturation_input.setDate_modification(Today_Date());
            adresse_facturation_input.setID_Agronome_Creation((int) ID_Compte_Selected);
            adresse_facturation_input.setID_Agronome_Modification((int) ID_Compte_Selected);

            //Adresse_livraison = Adresse de l'entreprise
            Adresse adresse_livraison_input = new Adresse();
            adresse_livraison_input.setAdresse_numero_rue(StreetAddress1.getText().toString().trim()); //Adresse
            adresse_livraison_input.setComplement_adresse(StreetAddress2.getText().toString().trim()); //Complement Adresse
            adresse_livraison_input.setArchive(0);
            adresse_livraison_input.setDate_creation(Today_Date());
            adresse_livraison_input.setDate_modification(Today_Date());
            adresse_livraison_input.setID_Agronome_Creation((int) ID_Compte_Selected);
            adresse_livraison_input.setID_Agronome_Modification((int) ID_Compte_Selected);

            //Adresse_siege_social = Adresse de l'entreprise
            Adresse adresse_siege_social_input = new Adresse();
            adresse_siege_social_input.setAdresse_numero_rue(StreetAddress1.getText().toString().trim()); //Adresse
            adresse_siege_social_input.setComplement_adresse(StreetAddress2.getText().toString().trim()); //Complement Adresse
            adresse_siege_social_input.setArchive(0);
            adresse_siege_social_input.setDate_creation(Today_Date());
            adresse_siege_social_input.setDate_modification(Today_Date());
            adresse_siege_social_input.setID_Agronome_Creation((int) ID_Compte_Selected);
            adresse_siege_social_input.setID_Agronome_Modification((int) ID_Compte_Selected);

            //Geolocalisation :
            if ((latitude_adresse_personnnelle != 0)&(longitude_adresse_personnnelle !=0)){
                Geolocalisation localisation_adresse_siege_input = new Geolocalisation();
                localisation_adresse_siege_input.setLatitude(latitude_adresse_personnnelle);
                localisation_adresse_siege_input.setLongitude(longitude_adresse_personnnelle);
                geolocalisation_data.insertOrReplace(localisation_adresse_siege_input);

                Geolocalisation localisation_adresse_facturation_input = new Geolocalisation();
                localisation_adresse_facturation_input.setLatitude(latitude_adresse_personnnelle);
                localisation_adresse_facturation_input.setLongitude(longitude_adresse_personnnelle);
                geolocalisation_data.insertOrReplace(localisation_adresse_facturation_input);

                Geolocalisation localisation_adresse_livraison_input = new Geolocalisation();
                localisation_adresse_livraison_input.setLatitude(latitude_adresse_personnnelle);
                localisation_adresse_livraison_input.setLongitude(longitude_adresse_personnnelle);
                geolocalisation_data.insertOrReplace(localisation_adresse_livraison_input);

                //Relier les tables
                adresse_siege_social_input.setGeolocalisation(localisation_adresse_siege_input);
                adresse_siege_social_input.setID_Geolocalisation(localisation_adresse_siege_input.getId());

                adresse_facturation_input.setGeolocalisation(localisation_adresse_facturation_input);
                adresse_facturation_input.setID_Geolocalisation(localisation_adresse_facturation_input.getId());

                adresse_livraison_input.setGeolocalisation(localisation_adresse_livraison_input);
                adresse_livraison_input.setID_Geolocalisation(localisation_adresse_livraison_input.getId());
            }

            //Code postal adresse entreprise siege
            Code_Postal_Ville code_postal_ville_entreprise_siege_input = new Code_Postal_Ville();
            code_postal_ville_entreprise_siege_input.setCode_postal(PostalCode1.getText().toString().trim());
            code_postal_ville_entreprise_siege_input.setVille(City1.getText().toString().trim());
            code_postal_ville_data.insertOrReplace(code_postal_ville_entreprise_siege_input);

            // Pays adresse entreprise siege
            Pays pays_entreprise_siege_input = new Pays();
            pays_entreprise_siege_input.setNom(Country1.getText().toString().trim());
            pays_data.insertOrReplace(pays_entreprise_siege_input);

            //Adresse Siege
            adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_entreprise_siege_input);
            adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_entreprise_siege_input.getId());
            adresse_siege_social_input.setPays(pays_entreprise_siege_input);
            adresse_siege_social_input.setID_Pays(pays_entreprise_siege_input.getId());
            adresse_data.insertOrReplace(adresse_siege_social_input);

            //Code postal adresse entreprise facturation
            Code_Postal_Ville code_postal_ville_entreprise_facturation_input = new Code_Postal_Ville();
            code_postal_ville_entreprise_facturation_input.setCode_postal(PostalCode1.getText().toString().trim());
            code_postal_ville_entreprise_facturation_input.setVille(City1.getText().toString().trim());
            code_postal_ville_data.insertOrReplace(code_postal_ville_entreprise_facturation_input);

            // Pays adresse entreprise siege
            Pays pays_entreprise_facturation_input = new Pays();
            pays_entreprise_facturation_input.setNom(Country1.getText().toString().trim());
            pays_data.insertOrReplace(pays_entreprise_facturation_input);

            //Adresse Facturation
            adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_entreprise_facturation_input);
            adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_entreprise_facturation_input.getId());
            adresse_facturation_input.setPays(pays_entreprise_facturation_input);
            adresse_facturation_input.setID_Pays(pays_entreprise_facturation_input.getId());
            adresse_data.insertOrReplace(adresse_facturation_input);

            //Code postal adresse entreprise siege
            Code_Postal_Ville code_postal_ville_entreprise_livraison_input = new Code_Postal_Ville();
            code_postal_ville_entreprise_livraison_input.setCode_postal(PostalCode1.getText().toString().trim());
            code_postal_ville_entreprise_livraison_input.setVille(City1.getText().toString().trim());
            code_postal_ville_data.insertOrReplace(code_postal_ville_entreprise_livraison_input);

            // Pays adresse entreprise siege
            Pays pays_entreprise_livraison_input = new Pays();
            pays_entreprise_livraison_input.setNom(Country1.getText().toString().trim());
            pays_data.insertOrReplace(pays_entreprise_livraison_input);

            //Adresse Livraison
            adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_entreprise_livraison_input);
            adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_entreprise_livraison_input.getId());
            adresse_livraison_input.setPays(pays_entreprise_livraison_input);
            adresse_livraison_input.setID_Pays(pays_entreprise_livraison_input.getId());
            adresse_data.insertOrReplace(adresse_livraison_input);



            //Relier les adresse à l'entreprise
            entreprise_input.setAdresse_Facturation(adresse_facturation_input);
            entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
            entreprise_input.setAdresse_Livraison(adresse_livraison_input);
            entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
            entreprise_input.setAdresse_Siege(adresse_siege_social_input);
            entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());
        }
        else{
            //Adresse facturation = Adresse de l'entreprise
            Adresse adresse_facturation_input = new Adresse();
            adresse_facturation_input.setAdresse_numero_rue(StreetAddress3.getText().toString().trim()); //Adresse
            adresse_facturation_input.setComplement_adresse(StreetAddress4.getText().toString().trim()); //Complement Adresse
            adresse_facturation_input.setArchive(0);
            adresse_facturation_input.setDate_creation(Today_Date());
            adresse_facturation_input.setDate_modification(Today_Date());
            adresse_facturation_input.setID_Agronome_Creation((int) ID_Compte_Selected);
            adresse_facturation_input.setID_Agronome_Modification((int) ID_Compte_Selected);

            //Adresse_livraison = Adresse de l'entreprise
            Adresse adresse_livraison_input = new Adresse();
            adresse_livraison_input.setAdresse_numero_rue(StreetAddress3.getText().toString().trim()); //Adresse
            adresse_livraison_input.setComplement_adresse(StreetAddress4.getText().toString().trim()); //Complement Adresse
            adresse_livraison_input.setArchive(0);
            adresse_livraison_input.setDate_creation(Today_Date());
            adresse_livraison_input.setDate_modification(Today_Date());
            adresse_livraison_input.setID_Agronome_Creation((int) ID_Compte_Selected);
            adresse_livraison_input.setID_Agronome_Modification((int) ID_Compte_Selected);

            //Adresse_siege_social = Adresse de l'entreprise
            Adresse adresse_siege_social_input = new Adresse();
            adresse_siege_social_input.setAdresse_numero_rue(StreetAddress3.getText().toString().trim()); //Adresse
            adresse_siege_social_input.setComplement_adresse(StreetAddress4.getText().toString().trim()); //Complement Adresse
            adresse_siege_social_input.setArchive(0);
            adresse_siege_social_input.setDate_creation(Today_Date());
            adresse_siege_social_input.setDate_modification(Today_Date());
            adresse_siege_social_input.setID_Agronome_Creation((int) ID_Compte_Selected);
            adresse_siege_social_input.setID_Agronome_Modification((int) ID_Compte_Selected);

            //Geolocalisation :
            if ((latitude_adresse_entreprise != 0)&(longitude_adresse_entreprise !=0)){
                Geolocalisation localisation_adresse_siege_input = new Geolocalisation();
                localisation_adresse_siege_input.setLatitude(latitude_adresse_entreprise);
                localisation_adresse_siege_input.setLongitude(longitude_adresse_entreprise);
                geolocalisation_data.insertOrReplace(localisation_adresse_siege_input);

                Geolocalisation localisation_adresse_facturation_input = new Geolocalisation();
                localisation_adresse_facturation_input.setLatitude(latitude_adresse_entreprise);
                localisation_adresse_facturation_input.setLongitude(longitude_adresse_entreprise);
                geolocalisation_data.insertOrReplace(localisation_adresse_facturation_input);

                Geolocalisation localisation_adresse_livraison_input = new Geolocalisation();
                localisation_adresse_livraison_input.setLatitude(latitude_adresse_entreprise);
                localisation_adresse_livraison_input.setLongitude(longitude_adresse_entreprise);
                geolocalisation_data.insertOrReplace(localisation_adresse_livraison_input);

                //Relier les tables
                adresse_siege_social_input.setGeolocalisation(localisation_adresse_siege_input);
                adresse_siege_social_input.setID_Geolocalisation(localisation_adresse_siege_input.getId());

                adresse_facturation_input.setGeolocalisation(localisation_adresse_facturation_input);
                adresse_facturation_input.setID_Geolocalisation(localisation_adresse_facturation_input.getId());

                adresse_livraison_input.setGeolocalisation(localisation_adresse_livraison_input);
                adresse_livraison_input.setID_Geolocalisation(localisation_adresse_livraison_input.getId());
            }

            //Code postal adresse entreprise siege
            Code_Postal_Ville code_postal_ville_entreprise_siege_input = new Code_Postal_Ville();
            code_postal_ville_entreprise_siege_input.setCode_postal(PostalCode2.getText().toString().trim());
            code_postal_ville_entreprise_siege_input.setVille(City2.getText().toString().trim());
            code_postal_ville_data.insertOrReplace(code_postal_ville_entreprise_siege_input);

            // Pays adresse entreprise siege
            Pays pays_entreprise_siege_input = new Pays();
            pays_entreprise_siege_input.setNom(Country2.getText().toString().trim());
            pays_data.insertOrReplace(pays_entreprise_siege_input);

            //Adresse Siege
            adresse_siege_social_input.setCode_Postal_Ville(code_postal_ville_entreprise_siege_input);
            adresse_siege_social_input.setID_Code_Postal_Ville(code_postal_ville_entreprise_siege_input.getId());
            adresse_siege_social_input.setPays(pays_entreprise_siege_input);
            adresse_siege_social_input.setID_Pays(pays_entreprise_siege_input.getId());
            adresse_data.insertOrReplace(adresse_siege_social_input);

            //Code postal adresse entreprise facturation
            Code_Postal_Ville code_postal_ville_entreprise_facturation_input = new Code_Postal_Ville();
            code_postal_ville_entreprise_facturation_input.setCode_postal(PostalCode2.getText().toString().trim());
            code_postal_ville_entreprise_facturation_input.setVille(City2.getText().toString().trim());
            code_postal_ville_data.insertOrReplace(code_postal_ville_entreprise_facturation_input);

            // Pays adresse entreprise siege
            Pays pays_entreprise_facturation_input = new Pays();
            pays_entreprise_facturation_input.setNom(Country2.getText().toString().trim());
            pays_data.insertOrReplace(pays_entreprise_facturation_input);

            //Adresse Facturation
            adresse_facturation_input.setCode_Postal_Ville(code_postal_ville_entreprise_facturation_input);
            adresse_facturation_input.setID_Code_Postal_Ville(code_postal_ville_entreprise_facturation_input.getId());
            adresse_facturation_input.setPays(pays_entreprise_facturation_input);
            adresse_facturation_input.setID_Pays(pays_entreprise_facturation_input.getId());
            adresse_data.insertOrReplace(adresse_facturation_input);

            //Code postal adresse entreprise siege
            Code_Postal_Ville code_postal_ville_entreprise_livraison_input = new Code_Postal_Ville();
            code_postal_ville_entreprise_livraison_input.setCode_postal(PostalCode2.getText().toString().trim());
            code_postal_ville_entreprise_livraison_input.setVille(City2.getText().toString().trim());
            code_postal_ville_data.insertOrReplace(code_postal_ville_entreprise_livraison_input);

            // Pays adresse entreprise siege
            Pays pays_entreprise_livraison_input = new Pays();
            pays_entreprise_livraison_input.setNom(Country2.getText().toString().trim());
            pays_data.insertOrReplace(pays_entreprise_livraison_input);

            //Adresse Livraison
            adresse_livraison_input.setCode_Postal_Ville(code_postal_ville_entreprise_livraison_input);
            adresse_livraison_input.setID_Code_Postal_Ville(code_postal_ville_entreprise_livraison_input.getId());
            adresse_livraison_input.setPays(pays_entreprise_livraison_input);
            adresse_livraison_input.setID_Pays(pays_entreprise_livraison_input.getId());
            adresse_data.insertOrReplace(adresse_livraison_input);

            //Relier les adresse à l'entreprise
            entreprise_input.setAdresse_Facturation(adresse_facturation_input);
            entreprise_input.setID_Adresse_Facturation(adresse_facturation_input.getId());
            entreprise_input.setAdresse_Livraison(adresse_livraison_input);
            entreprise_input.setID_Adresse_Livraison(adresse_livraison_input.getId());
            entreprise_input.setAdresse_Siege(adresse_siege_social_input);
            entreprise_input.setID_Adresse_Siege(adresse_siege_social_input.getId());
        }



        //SuiviProducteur Associé a l'entreprise
        SUIVI_PRODUCTEUR Suivi_Producteur_input = new SUIVI_PRODUCTEUR();
        Suivi_Producteur_input.setArchive(0);
        Suivi_Producteur_input.setDate_creation(Today_Date());
        Suivi_Producteur_input.setDate_modification(Today_Date());
        Suivi_Producteur_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        Suivi_Producteur_input.setID_Agronome_Modification((int)ID_Compte_Selected);
        suivi_producteur_data.insertOrReplace(Suivi_Producteur_input);
        entreprise_input.setSUIVI_PRODUCTEUR(Suivi_Producteur_input);
        entreprise_input.setID_SUIVI_PRODUCTEUR(Suivi_Producteur_input.getId());


        //Associer les tables au comptes
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                Log.i("Information 1", "Création de l'entreprise et du contact sur le compte :");
                Log.i("Information 2", "Login : " + c.getLogin() + "\nPassword : " + c.getPassword());
                entreprise_input.setID_Compte(c.getId());
                entreprise_data.insertOrReplace(entreprise_input);
                c.resetEntrepriseList();
                compte_dao.update(c);
            }
        }
        CreateDirectoryOrganization(Organization.getText().toString().trim());
    }

    //Permet de vérifier les ajouts dans la base de données.
    private void displayContactInLog(){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise){
                    Log.i("NEW", "the new enterprise :" + e.getRaison_sociale());
                    List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                    for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                        Log.i("NEW","Contact /  NOM : " + ct.getIdentite().getNom() + " , PRENOM : " + ct.getIdentite().getPrenom());
                        Log.i("NEW" , "Numero ID : " + ct.getId().toString());
                    }
                }
            }
        }
    }

    //check if data in db

    public  long getIdSize(long compte_id_server)
    {
        long size=0;
        if(compte_id_server==1)
            size= justineList;
        if(compte_id_server==2)
            size= mathieuList;

        return size;


    }

}
