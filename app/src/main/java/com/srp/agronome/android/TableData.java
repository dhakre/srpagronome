package com.srp.agronome.android;

import java.util.Date;
import java.util.List;

/**
 * Created by sumitra on 10/18/2017.
 */

//generic
public class TableData<T> {
    //columns same in all the DB
    int archive;
    Date date_creation;
    Date date_modification;
    int ID_Agronome_Creation;
    int ID_Agronome_Modification;

    //for the uncommon column names and types
    List<String> stringColumnValues;
    List<Long> longColumnValues;


  //getters
    public Date getDate_creation() {
        return date_creation;
    }

    public Date getDate_modification() {
        return date_modification;
    }

    public int getID_Agronome_Creation() {
        return ID_Agronome_Creation;
    }

    public int getID_Agronome_Modification() {
        return ID_Agronome_Modification;
    }

    public int getArchive() {

        return archive;
    }

    public List<String> getstringColumnValues()
    {
        return stringColumnValues;
    }

    public List<Long> getlongColumnValues()
    {
        return longColumnValues;
    }

    //setters


    public void setArchive(int archive) {
        this.archive = archive;
    }

    public void setDate_creation(Date date_creation) {
        this.date_creation = date_creation;
    }

    public void setDate_modification(Date date_modification) {
        this.date_modification = date_modification;
    }

    public void setID_Agronome_Creation(int ID_Agronome_Creation) {
        this.ID_Agronome_Creation = ID_Agronome_Creation;
    }

    public void setID_Agronome_Modification(int ID_Agronome_Modification) {
        this.ID_Agronome_Modification = ID_Agronome_Modification;
    }

    public void setStringColumnValues(List<String> stringColumnValues) {
        this.stringColumnValues = stringColumnValues;
    }

    public void setLongColumnValues(List<Long> longColumnValues) {
        this.longColumnValues = longColumnValues;
    }
}
