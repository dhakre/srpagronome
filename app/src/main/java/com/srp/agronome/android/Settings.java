package com.srp.agronome.android;

/**
 * Created by Vincent on 30/05/17.
 */

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Locale;

import static com.srp.agronome.android.MainLogin.Nom_Compte;


public class Settings extends PreferenceActivity {

    private ListPreference languageList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref); //Charger le fichier xml des options

        final ListPreference languageList = (ListPreference) findPreference("listlang");
        if (languageList != null) {
            languageList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object value) {

                    String Lang_code = value.toString(); // Retourne le code du pays avec changement
                    Save_LanguageChoice(Lang_code);//Sauvegarde de la langue en mémoire
                    EditLanguage(Lang_code); // Change la langue
                    return true;
                }
            });
        }


        final ListPreference formatList =(ListPreference) findPreference("listformat");
        if (formatList != null) {
            formatList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object value) {

                   String Format_Code = value.toString(); // Retourne le format
                   Save_FormatChoice(Format_Code);//Sauvegarde de la langue en mémoire
                    return true;
                }
            });
        }


        //Option carte SD ou Stockage Intern
        final String[] liste_valeur_tableau = getResources().getStringArray(R.array.listStorage);
        final ListPreference storageList = (ListPreference) findPreference("liststorage");
        //storageList.setSummary(element_tableau);
        if (storageList.getValue() == null) {
            storageList.setValueIndex(0); //Defaut value = Intern Storage
            storageList.setSummary(liste_valeur_tableau[0]);
        } else {
            if (storageList.getValue().equals("Intern")) {
                storageList.setSummary(liste_valeur_tableau[0]);
            }
            if (storageList.getValue().equals("Extern")) {
                storageList.setSummary(liste_valeur_tableau[1]);
            }
        }

        storageList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                String valeur = value.toString();
                File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
                if (valeur.equals("Intern")) {//Stockage Intern Selectionnee
                    if (!get_Storage_Location_Preference().equals("Intern")) {
                        if (CheckSizeDirectory(new File(Dirs[1], "/SRP_Agronome"), Dirs[0])) {
                            storageList.setSummary(liste_valeur_tableau[0]);
                            Save_Storage_Location("Intern");
                            CopyDirectoryToIntern();
                        } else {
                            Toast.makeText(getApplicationContext(), "Mémoire insuffisante", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                if (valeur.equals("Extern")) {
                    if (!get_Storage_Location_Preference().equals("Extern")) {
                        if (hasExternalSDCard()) { //Si le telephone possede une carte SD
                            if (CheckSizeDirectory(new File(Dirs[0], "/SRP_Agronome"), Dirs[1])) {
                                storageList.setSummary(liste_valeur_tableau[1]);
                                Save_Storage_Location("Extern");
                                CopyDirectoryToSD();
                            } else {
                                Toast.makeText(getApplicationContext(), "Mémoire insuffisante", Toast.LENGTH_SHORT).show();
                            }
                        } else { //Si le telephone ne possede pas de carte SD
                            Toast.makeText(getApplicationContext(), getString(R.string.TextNoSDCard), Toast.LENGTH_SHORT).show();
                            storageList.setValueIndex(0);
                            storageList.setSummary(liste_valeur_tableau[0]);
                        }
                    }
                }
                return true;
            }
        });
    }


    // Permet de changer la langue de l'application
    public void EditLanguage(String lang) {
        Locale locale;
        Configuration config = new Configuration();
        locale = new Locale(lang);
        Locale.setDefault(locale);
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, null);
        onConfigurationChanged(config);
        this.recreate(); // Rafraichir Activité courante
    }

    //Retour à la login Page
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Settings.this, MainLogin.class);
        finish();
        startActivity(intent);
    }

    public void Save_LanguageChoice(String Lang) {
        MainLogin.Language_saved = Lang;
        SharedPreferences.Editor editor = MainLogin.sharedpreferences.edit();
        editor.putString(MainLogin.Language, Lang);
        editor.apply();
    }

    public void Save_FormatChoice(String Format){
        //Code sauv
        SharedPreferences.Editor editor = MainLogin.sharedpreferences.edit();
        editor.putString("FormatCode", Format);
        editor.apply();
    }

    public void Save_Storage_Location(String Storage){
        SharedPreferences.Editor editor = MainLogin.sharedpreferences.edit();
        editor.putString("LocationStorage",Storage);
        editor.apply();
    }

    public String get_Storage_Location_Preference(){
        String result="";
        SharedPreferences sharedpreferences = getSharedPreferences("PrefValues", 0); // 0 = Private Mode
        if (sharedpreferences.contains("LocationStorage")) {
            result = sharedpreferences.getString("LocationStorage", ""); // Default value -> false
        }
        return result;
    }

    private void CopyAndDelete_Directory_to_SD() {
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        File myDir_Source = new File(Dirs[0],"/SRP_Agronome");
        File myDir_Dest = new File(Dirs[1], "/SRP_Agronome");
        try{
        FileUtils.copyDirectory(myDir_Source, myDir_Dest); //Copy the directory
        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
        FileUtils.deleteDirectory(myDir_Source); //Delete the directory
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CopyAndDelete_Directory_to_Intern() {
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        File myDir_Source = new File(Dirs[1], "/SRP_Agronome");
        File myDir_Dest = new File(Dirs[0],"/SRP_Agronome");
        try{
            FileUtils.copyDirectory(myDir_Source, myDir_Dest); //Copy the directory
        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
            FileUtils.deleteDirectory(myDir_Source); //Delete the directory
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Check size before copy/paste directory
    private boolean CheckSizeDirectory(File file_input , File file_output){
        long size_input = FileUtils.sizeOfDirectory(file_input);
        long size_output = getAvailableExternalMemorySize(file_output);
        Log.i("Info Taille","Taille du repertoire source : " + size_input);
        Log.i("Info Taille","Memoire disponible repertoire cible : " + size_output);
        return (size_output > size_input);
    }


    //Return true if the device has a extern SD Card
    public boolean hasExternalSDCard()
    {
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (Dirs.length == 1){
            return false;
        }
        else{
        return (!(Dirs[1] == null));}
    }


    //Retourne la mémoire disponible pour un stockage externe
    public long getAvailableExternalMemorySize(File file) {
        File path = file;
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return (availableBlocks * blockSize);
    }

    public void CopyDirectoryToIntern() {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(Settings.this,
                getString(R.string.TitleChargement), getString(R.string.TextChargement), true);
        ringProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    CopyAndDelete_Directory_to_Intern();
                } catch (Exception e) {
                }
                ringProgressDialog.dismiss();
            }
        }).start();
    }

    public void CopyDirectoryToSD() {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(Settings.this,
                getString(R.string.TitleChargement), getString(R.string.TextChargement), true);
        ringProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    CopyAndDelete_Directory_to_SD();
                } catch (Exception e) {
                }
                ringProgressDialog.dismiss();
            }
        }).start();
    }

}