package com.srp.agronome.android;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;


import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.Activite;
import com.srp.agronome.android.db.ActiviteDao;
import com.srp.agronome.android.db.Adresse;
import com.srp.agronome.android.db.AdresseDao;
import com.srp.agronome.android.db.Biologique;
import com.srp.agronome.android.db.BiologiqueDao;
import com.srp.agronome.android.db.Categorie_Contact;
import com.srp.agronome.android.db.Categorie_ContactDao;
import com.srp.agronome.android.db.Code_Postal_Ville;
import com.srp.agronome.android.db.Code_Postal_VilleDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.ContactDao;
import com.srp.agronome.android.db.Entreprise;

import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Identite;
import com.srp.agronome.android.db.IdentiteDao;
import com.srp.agronome.android.db.Pays;
import com.srp.agronome.android.db.PaysDao;
import com.srp.agronome.android.db.Region;
import com.srp.agronome.android.db.RegionDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.Sexe;
import com.srp.agronome.android.db.SexeDao;
import com.srp.agronome.android.db.Situation;
import com.srp.agronome.android.db.SituationDao;
import com.srp.agronome.android.db.Sociabilite;
import com.srp.agronome.android.db.SociabiliteDao;
import com.srp.agronome.android.db.Statut_Soytouch;
import com.srp.agronome.android.db.Statut_SoytouchDao;
import com.srp.agronome.android.db.Structure_Sociale;
import com.srp.agronome.android.db.Structure_SocialeDao;
import com.srp.agronome.android.db.Type_Bio;
import com.srp.agronome.android.db.Type_BioDao;


import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.CreateContact.Today_Date;
import static com.srp.agronome.android.CreateOrganization.Date_Arret;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Contact_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.Nom_Contact;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;
import static com.srp.agronome.android.MainLogin.Prenom_Contact;

/**
 * Created by doucoure on 11/07/2017.
 */

public class MenuAgriculteur extends AppCompatActivity {


    public static long DELETE_CONTACT_ID;
    public static long DELETE_enterprise_ID;

    private Button BtnContactDetaille = null;

    private Button BtnAudit = null;

    private Button BtnSuiviProducteur = null;

    private Button BtnDonneesExploitations = null;

    private Button BtnVenteCommandeSourcing = null;

    private Button BtnAide = null;

    private ImageButton imgBtnHome = null;

    private ImageButton btn_discuss = null;

    private ImageButton btn_call = null;

    private ImageButton btn_direction = null;

    private static final int CAMERA_PIC_REQUEST = 2;
    private static final int GALLERY_PICK = 1;
    private static final int PICK_CROP = 3;


    private Contact contact;

    private TextView Raison_Sociale = null;
    private TextView Nom_Prenom = null;
    private TextView Numero_Contact = null;
    private TextView CodePostale = null;
    private TextView Ville = null;
    private ImageView photo = null;

    private String Telephone = "";
    private String Adresse_Map = "";
    private String CodePostal_Map = "";
    private String Ville_Map = "";

    private Context context;

    private ImageButton Delete;
    private ImageButton Edit;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_agriculteure);

        Raison_Sociale = (TextView) findViewById(R.id.Title_Menu_Agriculteur);
        Nom_Prenom = (TextView) findViewById(R.id.TextName);              //manage size
        Numero_Contact = (TextView) findViewById(R.id.TextNumberID);
        CodePostale = (TextView) findViewById(R.id.TextCodePostal);
        Ville = (TextView) findViewById(R.id.TextVille);


        Typeface Regular = Typeface.createFromAsset(getAssets(), "Exo/Exo-Regular.ttf");
        Typeface Thin = Typeface.createFromAsset(getAssets(), "Exo/Exo-Thin.ttf");
        Raison_Sociale.setTypeface(Regular);
        Nom_Prenom.setTypeface(Regular);
        Numero_Contact.setTypeface(Regular);
        CodePostale.setTypeface(Thin);
        Ville.setTypeface(Thin);

        photo = (ImageView) findViewById(R.id.ContactPhoto);
        photo.setOnClickListener(PhotoHandler);

        LoadContact(ID_Entreprise_Selected, ID_Contact_Selected);
        LoadPhotoDatabase(ID_Entreprise_Selected, ID_Contact_Selected);

        //Contact Detaillé
        BtnContactDetaille = (Button) findViewById(R.id.btn_contact_detaille);
        BtnContactDetaille.setOnClickListener(ButtontContactDetailleHandler);

        //Suivi Producteur
        BtnSuiviProducteur = (Button) findViewById(R.id.btnmenuagri2);
        BtnSuiviProducteur.setOnClickListener(ButtonSuiviHandler);

        //Audit
        BtnAudit = (Button) findViewById(R.id.btnmenuagri1);
        BtnAudit.setOnClickListener(ButtonAuditHandler);

        //Donnees parcelles / exploitation
        BtnDonneesExploitations = (Button) findViewById(R.id.btnmenuagri3);
        BtnDonneesExploitations.setOnClickListener(ButtonDonneesExploitations);

        //Vente/Commande/Sourcing
        BtnVenteCommandeSourcing = (Button) findViewById(R.id.btnmenuagri4);
        BtnVenteCommandeSourcing.setOnClickListener(ButtonVenteCommandeSourcingHandler);

        //Aide RDV
        BtnAide = (Button) findViewById(R.id.btnmenuagri5);
        BtnAide.setOnClickListener(ButtonAideRDVHandler);


        btn_call = (ImageButton) findViewById(R.id.btn_cal_agriculteur);
        btn_call.setOnClickListener(ButtontCallHandlerAgriculteur);

        btn_direction = (ImageButton) findViewById(R.id.btn_map_agriculteur);
        btn_direction.setOnClickListener(ButtonCarteHandlerAgriculteur);


        btn_discuss = (ImageButton) findViewById(R.id.btn_discuss_agriculteur);
        btn_discuss.setOnClickListener(ButtonDIsscussionHandler);


        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menu);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

        Delete = (ImageButton) findViewById(R.id.btn_delete);
        Delete.setOnClickListener(ButtonDeleteHandler);

        Edit = (ImageButton) findViewById(R.id.btn_edit);
        Edit.setOnClickListener(ButtonEditHandler);


    }

    private void LoadContact(long Id_Entreprise, long Id_Contact) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        String name,prenom;
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        Raison_Sociale.setText(e.getRaison_sociale());
                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                            if ((ct.getId() == Id_Contact)) {
                               // Nom_Prenom.setText(ct.getIdentite().getNom() + " - " + ct.getIdentite().getPrenom());
                                name=ct.getIdentite().getNom();
                                name=GlobalValues.setName(name);      //set name size to be shown on list
                                prenom=ct.getIdentite().getPrenom();
                                prenom=GlobalValues.setName(prenom);
                                Nom_Prenom.setText(name+"-"+prenom);   //set UI
                                String numero = "N° : " + ct.getId();
                                Numero_Contact.setText(numero);
                                CodePostale.setText(ct.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal());
                                Ville.setText(ct.getIdentite().getAdresse().getCode_Postal_Ville().getVille());
                                //Module Call et Map
                                Telephone = ct.getIdentite().getTelephone_principal();
                                Adresse_Map = ct.getIdentite().getAdresse().getAdresse_numero_rue();
                                CodePostal_Map = ct.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal();
                                Ville_Map = ct.getIdentite().getAdresse().getCode_Postal_Ville().getVille();
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(MenuAgriculteur.this, ListContact.class);
            finish();
            startActivity(intent);
        }
        return true;
    }


    View.OnClickListener ButtontContactDetailleHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuAgriculteur.this, ContactProfil.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener ButtonSuiviHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //  Toast.makeText(getApplicationContext(),"Prochainement disponible",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MenuAgriculteur.this, MenuSuiviProducteur.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener ButtonAuditHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuAgriculteur.this, MenuAudit.class);
            startActivity(intent);
           finish();
           // Toast.makeText(getApplicationContext(), "En cours de développement", Toast.LENGTH_SHORT).show();
        }
    };


    View.OnClickListener ButtonDonneesExploitations = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuAgriculteur.this, MenuParcelle.class);
            startActivity(intent);
            finish();
        }
    };


    View.OnClickListener ButtonVenteCommandeSourcingHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "En cours de développement", Toast.LENGTH_SHORT).show();
        }
    };

    View.OnClickListener ButtonAideRDVHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "En cours de développement", Toast.LENGTH_SHORT).show();
        }
    };


    View.OnClickListener ButtonDeleteHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style
            PopupMenu popup = new PopupMenu(wrapper, Delete); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_delete, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals(getString(R.string.TextDeleteContact))) {
                        Show_Dialog__Delete_Contact(getString(R.string.TitleDeleteContact), getString(R.string.TextDeleteContact2));
                    }
                    if (item.getTitle().equals(getString(R.string.TextDeleteOrganization))) {
                        Show_Dialog__Delete_Entreprise(getString(R.string.TitleDeleteOrganization), getString(R.string.TextDeleteOrganization2));
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    View.OnClickListener ButtonEditHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style
            PopupMenu popup = new PopupMenu(wrapper, Edit); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_edit, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    //Toast.makeText(getApplicationContext(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                    if (item.getTitle().equals(getString(R.string.TextEditContact))) {
                        Intent intent = new Intent(MenuAgriculteur.this, EditContact.class);
                        finish();
                        startActivity(intent);
                    }
                    if (item.getTitle().equals(getString(R.string.TextEditOrganization))) {
                        Intent intent = new Intent(MenuAgriculteur.this, EditOrganization.class);
                        finish();
                        startActivity(intent);
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    //Permet de "Supprimer un contact" = Masquer le contact dans la liste des contacts
    private void DeleteContact(long Id_Entreprise, long Id_Contact) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<com.srp.agronome.android.db.Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                            if ((ct.getId() == Id_Contact)) {
                                if (CheckSetArchiveContact(ct.getId(), ct.getIdentite().getId(), ct.getIdentite().getAdresse().getId())) {
                                    ArchiveContact(ct);
                                } else {
                                    Show_Dialog(getString(R.string.TitleDeleteFail), getString(R.string.TextDeleteFail));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean CheckRelationsAddress(long ID_Adresse) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        IdentiteDao identite_data = daoSession.getIdentiteDao();
        List<Identite> list_identite = identite_data.queryBuilder()
                .where(IdentiteDao.Properties.ID_Adresse.eq(ID_Adresse))
                .list();
        //Log.i("AdressRelation","Nbr Adresse Relation : " + list_identite.size() );
        return (list_identite.size() < 2);
    }

    private boolean CheckRelationsIdentite(long ID_Identite) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ContactDao contact_data = daoSession.getContactDao();
        List<com.srp.agronome.android.db.Contact> list_contact = contact_data.queryBuilder()
                .where(ContactDao.Properties.ID_Identite_Contact.eq(ID_Identite))
                .list();
        //Log.i("ContactRelation","Nbr Contact Relation : " + list_contact.size() );
        return (list_contact.size() < 2);
    }

    private boolean CheckRelationsContact(long ID_Contact) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        QueryBuilder<Entreprise> queryBuilder = entreprise_data.queryBuilder();
        queryBuilder.join(com.srp.agronome.android.db.Contact.class, ContactDao.Properties.ID_Entreprise)
                .where(ContactDao.Properties.Id.eq(ID_Contact));
        List<Entreprise> list_entreprise = queryBuilder.list();
        //Log.i("EntrepriseRelation","Nbr Entreprise Relation : " + list_entreprise.size() );
        return (list_entreprise.size() < 2);
    }

    private boolean CheckSetArchiveContact(long ID_Contact, long ID_Identite, long ID_Adresse) {
        return ((CheckRelationsIdentite(ID_Identite)) & (CheckRelationsAddress(ID_Adresse)) & (CheckRelationsContact(ID_Contact)));
    }

    private void ArchiveContact(com.srp.agronome.android.db.Contact contact_archive) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ContactDao contact_data = daoSession.getContactDao();
        IdentiteDao identite_data = daoSession.getIdentiteDao();
        AdresseDao adresse_data = daoSession.getAdresseDao();

        contact_archive.setDate_modification(Today_Date());
        contact_archive.setID_Agronome_Modification((int) ID_Compte_Selected);
        contact_archive.setArchive(1);
        DELETE_CONTACT_ID=contact_archive.getId();
        contact_archive.getIdentite().setDate_modification(Today_Date());
        contact_archive.getIdentite().setID_Agronome_Modification((int) ID_Compte_Selected);
        contact_archive.getIdentite().setArchive(1);
        contact_archive.getIdentite().getAdresse().setDate_modification(Today_Date());
        contact_archive.getIdentite().getAdresse().setID_Agronome_Modification((int) ID_Compte_Selected);
        contact_archive.getIdentite().getAdresse().setArchive(1);

        adresse_data.update(contact_archive.getIdentite().getAdresse());
        identite_data.update(contact_archive.getIdentite());
        contact_data.update(contact_archive);
    }


    //Delete Entreprise
    private void DeleteEntreprise(long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<com.srp.agronome.android.db.Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        List<com.srp.agronome.android.db.Contact> list_contact = e.getContactList();
                        boolean archive = true;
                        for (com.srp.agronome.android.db.Contact ct : list_contact) {
                            if (!(CheckSetArchiveContact(ct.getId(), ct.getIdentite().getId(), ct.getIdentite().getAdresse().getId()))) {
                                archive = false; //Check si plusieurs relation
                            }
                        }
                        if (archive) {
                            ArchiveEntreprise(e); //Archive Entreprise + Contact
                        } else {
                            Show_Dialog(getString(R.string.TitleDeleteFail), getString(R.string.TextDeleteFail));
                        }
                    }
                }
            }
        }
    }

    private void ArchiveEntreprise(Entreprise entreprise_archive) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        List<com.srp.agronome.android.db.Contact> list_contact = entreprise_archive.getContactList();
        for (com.srp.agronome.android.db.Contact ct : list_contact) {
            ArchiveContact(ct);
        }
        entreprise_archive.setDate_modification(Today_Date());
        entreprise_archive.setID_Agronome_Modification((int) ID_Compte_Selected);
        entreprise_archive.setArchive(1);
        //get delete id
        DELETE_enterprise_ID=entreprise_archive.getId();
        entreprise_data.update(entreprise_archive);
    }

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    //Action CALL
    View.OnClickListener ButtontCallHandlerAgriculteur = new View.OnClickListener() {
        public void onClick(View v) {
            String number = Telephone;
            if (!TextUtils.isEmpty(Telephone)) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + number));
                if (ActivityCompat.checkSelfPermission(MenuAgriculteur.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(intent);
            } else {
                Show_Dialog(getString(R.string.TitleMissingAdress), getString(R.string.MissingPhone));
            }
        }
    };

    //Action GPS
    View.OnClickListener ButtonCarteHandlerAgriculteur = new View.OnClickListener() {
        public void onClick(View v) {
            String Adresse = Adresse_Map;
            String CP = CodePostal_Map;
            String Ville = Ville_Map;
            if (TextUtils.isEmpty(Adresse) || (TextUtils.isEmpty(CP)) || (TextUtils.isEmpty(Ville))) {
                Toast.makeText(getApplicationContext(), getString(R.string.MissingAdress), Toast.LENGTH_SHORT).show();
            } else {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + Adresse + " " + CP + " " + Ville);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        }
    };


    View.OnClickListener ButtonDIsscussionHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "Prochainement disponible", Toast.LENGTH_SHORT).show();
        }
    };


    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuAgriculteur.this, ListContact.class);
            finish();
            startActivity(intent);
        }
    };

    //Display Dialog Delete_Contact
    private void Show_Dialog__Delete_Contact(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuAgriculteur.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteContact(ID_Entreprise_Selected, ID_Contact_Selected);
                Intent intent = new Intent(MenuAgriculteur.this, ListContact.class);
                finish();
                startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Display Dialog Delete_Entreprise
    private void Show_Dialog__Delete_Entreprise(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuAgriculteur.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteEntreprise(ID_Entreprise_Selected);
                Intent intent = new Intent(MenuAgriculteur.this, ListContact.class);
                finish();
                startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuAgriculteur.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

    //Event Clic on Photo :
    View.OnClickListener PhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Selection_Photo_Choice();
        }
    };

    //Afficher une boîte de dialogue contenant une liste de choix pour affecter une photo
    public void Selection_Photo_Choice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuAgriculteur.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionPhoto, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Prendre une photo
                        TakePicturefromCamera();
                        break;
                    case 1: //Choisir une photo depuis la galerie
                        pickImagefromGallery();
                        break;
                    case 2: //Rogner la photo affichée
                        //Verifie si il y a une photo
                        if (photo.getDrawable().getConstantState() != MenuAgriculteur.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            CropImage();
                        }
                        break;
                    case 3: //Supprimer la photo -> mettre l'image de base à la place
                        photo.setImageResource(R.mipmap.ic_add_photo);
                        SavePhotoDataBase(ID_Entreprise_Selected, ID_Contact_Selected);
                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }


    //Pick a photo from Gallery
    public void pickImagefromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICK);
    }

    //Take a photo from Camera
    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
        File file = new File(myDir, FileName);
        return file;
    }

    //Permet de retourner les 3 premiers caracteres d'un texte
    private String getSubString(String chaine) {
        String result = "";
        if (chaine.length() >= 3) {
            result = chaine.substring(0, 3);
        }
        if (chaine.length() == 2) {
            result = chaine.substring(0, 2);
        }
        if (chaine.length() == 1) {
            result = chaine.substring(0, 1);
        }
        return result;
    }

    private String getPhotoContactNameHQ(String compte, String entreprise, String nom, String prenom) {
        return getSubString(compte) + "_" + entreprise + "_C_" + getSubString(nom) + "_" + getSubString(prenom) + "_HQ.jpg";
    }

    private String getPhotoContactNameLQ(String compte, String entreprise, String nom, String prenom) {
        return getSubString(compte) + "_" + entreprise + "_C_" + getSubString(nom) + "_" + getSubString(prenom) + "_LQ.jpg";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == GALLERY_PICK) { //Depuis la galerie
            Uri uri = data.getData();
            Bitmap Picture_Load;
            try {
                Picture_Load = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                Picture_Load = rotateImageIfRequired(Picture_Load, uri, true); //Rotation si necessaire
                SaveImage(Picture_Load, getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
                Picture_Load = getResizedBitmap(Picture_Load, 256, 256);
                photo.setImageBitmap(Picture_Load);
                SaveImage(Picture_Load, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_PIC_REQUEST) { //Depuis la caméra : pas de bitmap en sortie
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
            File file = new File(myDir, getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
            Uri uri = Uri.fromFile(file);
            Bitmap newPicture;
            try {
                newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
                newPicture = rotateImageIfRequired(newPicture, uri, false); //Rotation si necessaire
                SaveImage(newPicture, getPhotoContactNameHQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
                newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
                photo.setImageBitmap(newPicture);
                SaveImage(newPicture, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_CROP) {
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
            File file = new File(myDir, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
            Uri URI = Uri.fromFile(file);
            Bitmap Picture_Crop;
            try {
                Picture_Crop = MediaStore.Images.Media.getBitmap(getContentResolver(), URI);
                photo.setImageBitmap(Picture_Crop);
                SaveImage(Picture_Crop, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        SavePhotoDataBase(ID_Entreprise_Selected, ID_Contact_Selected);
    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Permet de rogner ou recadrer une image
    private void CropImage() {
        try {
            //Acceder à la photo à rogner
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
            File photoFile = new File(myDir, getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
            Uri photoURI = FileProvider.getUriForFile(this,
                    "com.srp.android.fileprovider",
                    photoFile);
            getApplicationContext().grantUriPermission("com.android.camera", photoURI,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //Paramètres de l'Intent Crop
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //Android N need set permission to uri otherwise system camera don't has permission to access file wait crop
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // indicate image type and Uri
            cropIntent.setDataAndType(photoURI, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PICK_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
        }
    }


    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
        File file = new File(myDir, FileName);
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage, boolean PickingGallery) throws IOException {

        InputStream input = getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else if (PickingGallery) {
            ei = new ExifInterface(getPath(selectedImage));
        } else {
            ei = new ExifInterface(selectedImage.getPath());
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    //Get Path = Pick up a picture in Gallery = getRealPath
    private String getPath(Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    //Rotate an image with angle in degree
    private Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        Log.i("Info Rotation", "Rotation de l image de : " + degree);
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }

    //Get the path of the directory : Intern or SD path
    private File getDirectoryPath() {
        SharedPreferences SP = getSharedPreferences("PrefValues", 0); // 0 = Private Mode
        String location = SP.getString("LocationStorage", null);
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (location.equals("Intern")) {
            return Dirs[0];
        } else {
            return Dirs[1];
        }
    }


    //Méthode pour charger une image depuis le téléphone dans l'application.
    private void loadImageFromStorage(String FileName) {
        try {
            File f = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts/" + FileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            photo.setImageBitmap(b);
            Log.i("Load Bitmap", "L'image définie.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("Load Bitmap", "L'image est introuvable");
        }
    }

    public static boolean checkImageResource(Context ctx, ImageView imageView, int imageResource) {
        boolean result = false;
        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }
            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }
        return result;
    }

    private void SavePhotoDataBase(long Id_Entreprise, long Id_Contact) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                            Log.i("Info DB", "Parcours des contacts");
                            if ((ct.getId() == Id_Contact)) {
                                Log.i("Info DB", "Contact trouvé !");
                                if (checkImageResource(this, photo, R.mipmap.ic_add_photo)) {
                                    ct.getIdentite().setPhoto("");
                                } else {
                                    ct.getIdentite().setPhoto(getPhotoContactNameLQ(Nom_Compte, Nom_Entreprise, Nom_Contact, Prenom_Contact));
                                }
                                ct.getIdentite().update();
                            }
                        }
                    }
                }
            }
        }
    }

    private void LoadPhotoDatabase(long Id_Entreprise, long Id_Contact) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                            if ((ct.getId() == Id_Contact)) {
                                //Charger la photo si elle existe en DB
                                if (ct.getIdentite().getPhoto().equals("")) {
                                    photo.setImageResource(R.mipmap.ic_add_photo);
                                    Log.i("LoadPhoto", "Pas de photo en BD");
                                } else {
                                    loadImageFromStorage(ct.getIdentite().getPhoto());
                                    Log.i("LoadPhoto", "Chargement de la photo : " + ct.getIdentite().getPhoto());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //set name to size
    /*public String setNameSize(String nom)
    {
        if(nom.length()>15)
        {
            nom=nom.substring(0,5);  //show only first 5 letters

        }
        return nom;
    }*/


}
