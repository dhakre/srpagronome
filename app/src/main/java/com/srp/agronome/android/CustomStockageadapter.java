package com.srp.agronome.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import static com.srp.agronome.android.MenuParcelle.Id_stockage_selected;
import java.util.ArrayList;

/**
 * Created by jitendra on 04-09-2017.
 */

public class CustomStockageadapter extends ArrayAdapter<StockageClass> {
    public static String Nom_stockeur;
    public static String Identifiant_stockeur;
    public static String capacite;


    Context context;
    int layoutResourceId;
    ArrayList<StockageClass> stockagedata = new ArrayList<StockageClass>();
    private ArrayList<StockageClass> filteredList;

    public CustomStockageadapter(Context context, int layoutResourceId, ArrayList<StockageClass> data) {
        super(context, layoutResourceId, data);this.layoutResourceId = layoutResourceId;
       this.context = context;
       this.stockagedata = data;
        this.filteredList = data;
       getFilter();

       // super(context, 0, data);
    }



    //counts the total number of elements from the arrayList.
    public int getCount() {
        return stockagedata.size();
    }

    public void onItemselected(int position) {

    }
    public class Holder {
        MyTextView NOM_STOCKEUR;
        MyTextView IDENTIFIANT_STOCKEUR;
        MyTextView CAPACITE;
        ImageButton btn;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      //  View row = convertView;
        Holder holder = new Holder();

        if (convertView == null) {
          //  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
           // row = inflater.inflate(layoutResourceId, parent, false);
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list, parent, false);
        }

        CustomStockageadapter.Holder viewHolder = (CustomStockageadapter.Holder) convertView.getTag();
            if (viewHolder == null) {
            holder.NOM_STOCKEUR = (MyTextView) convertView.findViewById(R.id.NOM_STOCKEUR);
            holder.IDENTIFIANT_STOCKEUR = (MyTextView) convertView.findViewById(R.id.IDENTIFIANT_STOCKEUR);
            holder.CAPACITE = (MyTextView) convertView.findViewById(R.id.CAPACITE);
            holder.btn = (ImageButton) convertView.findViewById(R.id.btn_voir_stockage); }

        final StockageClass stockageClass = stockagedata.get(position);
            Typeface Regular = Typeface.createFromAsset(getContext().getAssets(), "Exo/Exo-Regular.ttf");

            holder.NOM_STOCKEUR.setText(stockageClass.getNOM_STOCKEUR());
            holder.NOM_STOCKEUR.setTypeface(Regular);

            holder.IDENTIFIANT_STOCKEUR.setText(stockageClass.getIDENTIFIANT_STOCKEUR());
            holder.IDENTIFIANT_STOCKEUR.setTypeface(Regular);

            holder.CAPACITE.setText(stockageClass.getCAPACITE());
            holder.CAPACITE.setTypeface(Regular);

            holder.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Nom_stockeur= stockageClass.getNOM_STOCKEUR();
                    Log.i("CheckValue", "NOM_STOCKEUR : " + Nom_stockeur ) ;
                    Identifiant_stockeur = stockageClass.getIDENTIFIANT_STOCKEUR();
                    capacite = stockageClass.getCAPACITE();
                    Id_stockage_selected = stockageClass.getid_stockage();
                    Log.i("CheckValue", "Value of ID_Stockage : " + Id_stockage_selected ) ;
                    Intent next = new Intent(context, ModificationStockage.class);
                    ((Activity) context).finish();
                    context.startActivity(next);
                }
            });

        return convertView;
    }

}
