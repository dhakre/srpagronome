package com.srp.agronome.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;


import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Visite;
import com.srp.agronome.android.db.VisiteDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.CustomVisitAdapter.RaisonSociale;
import static com.srp.agronome.android.CustomVisitAdapter.objetVisite;
import static com.srp.agronome.android.CustomVisitAdapter.sub_principal;
import static com.srp.agronome.android.CustomVisitAdapter.sub_secondary;
import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuVisite.Id_Visite_Selected;
/**
 * Created by jitendra on 07-09-2017.
 */

public class ModificationVisite  extends AppCompatActivity {
    public static int  archived = 1;
    private TextView Text_objet_visit = null;
    private TextView text_raison_sociale=null;
    private TextView text_sub_principal=null;
    private TextView text_sub_secondary=null;
    private ImageButton Edit =null;
    private ImageButton Delete =null;
    private ImageButton imgBtnHome =null;
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modification_visite);
        FindWidgetViewbyId();
        Edit = (ImageButton) findViewById(R.id.btn_edit);
        Edit.setOnClickListener(ButtonEditHandler);

        Delete = (ImageButton) findViewById(R.id.btn_delete);
        Delete.setOnClickListener(ButtonDeleteHandler);
        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_Modificationsvisite);
        imgBtnHome.setOnClickListener(ButtonbackHandler);

        //set data
        Text_objet_visit.setText(objetVisite);
        text_raison_sociale.setText(RaisonSociale);
       // text_sub_principal.setText(sub_principal);
        //text_sub_secondary.setText(sub_secondary);
    }

    View.OnClickListener ButtonDeleteHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style

            PopupMenu popup = new PopupMenu(wrapper, Delete); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_delete_visite, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {

                    if (item.getTitle().equals(getString(R.string.TextDeleteVisite))){
                        Show_Dialog__Delete_visite(getString(R.string.TitleDeleteVisite),getString(R.string.TextDeletevisitemessage));
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    //Display Dialog Delete_visite
    private void Show_Dialog__Delete_visite(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationVisite.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                VisiteDeletestrategy();
                Intent intent = new Intent(ModificationVisite.this, ListViste.class);
                finish();
                startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

private void VisiteDeletestrategy (){
    Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
    List <Visite> list_visite = getListVisite(entreprise_selected);
    Visite delete_visit =getVisite(Id_Visite_Selected,list_visite);
    //Log.i("Deletevisit","delete_visit  "+delete_visit);
    //ArchiveVisite(delete_visit);
   // Show_Dialog_Delete_Visite(getString(R.string.TitleConfirmDelete) , getString(R.string.TextConfirmDelete), delete_visit);
      DeleteVisite(delete_visit);
}
    private boolean CheckRelationsVisite(long ID_Visite){
        Log.i("Deletevisit","ID_Visite  "+ID_Visite);
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        QueryBuilder<Entreprise> queryBuilder = entreprise_data.queryBuilder();
        queryBuilder.join(Visite.class, VisiteDao.Properties.ID_Entreprise)
                .where(VisiteDao.Properties.Id.eq(ID_Visite));
        List<Entreprise> list_E = queryBuilder.list();
        Log.i("Deletevisit","list_E.size()  "+list_E.size());
        return (list_E.size() < 2 );
    }
    private void DeleteVisite(Visite visite_delete){
        if (CheckRelationsVisite(visite_delete.getId())){
            Log.i("Deletevisit","visite_delete.getId()  "+visite_delete.getId());
            Log.i("Deletevisit","visite_delete.getArchive()  "+visite_delete.getArchive());
            Log.i("Deletevisit","visite_delete  "+visite_delete);
            if (visite_delete.getArchive() == 2){
                Log.i("Deletevisit","visite_delete.getArchive()inside  "+visite_delete.getArchive());
                Show_Dialog_Delete_Visite(getString(R.string.TitleConfirmDelete) , getString(R.string.TextConfirmDelete), visite_delete);
            }

            else{

                ArchiveVisite(visite_delete);

            }
        }
        else{
            Show_Dialog(getString(R.string.TitleDeleteFail),getString(R.string.TextDeleteFail));
        }
    }


    //Display Dialog Delete_Visite
    private void Show_Dialog_Delete_Visite(String title, String message , final Visite visite_delete) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationVisite.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArchiveVisite(visite_delete);
                Log.i("Deletevisit","final visit delete  "+visite_delete);
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void ArchiveVisite(Visite visite_delete){
        Log.i("Deletevisit","ArchiveVisite1  "+visite_delete);
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        VisiteDao visite_data = daoSession.getVisiteDao();
        visite_delete.setArchive(archived);
        visite_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        visite_delete.setDate_modification(Today_Date());
        visite_data.update(visite_delete);
        Log.i("Deletevisit","visite_data  "+visite_data);
    }
    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }


    private void FindWidgetViewbyId() {
        Text_objet_visit = (TextView) findViewById(R.id.Text_objective_visite);
        text_raison_sociale= (TextView) findViewById(R.id.Text_Raison_sociale);
        text_sub_principal= (TextView) findViewById(R.id.Text_sub_principal);
        text_sub_secondary= (TextView) findViewById(R.id.Text_sub_secondary);
    }
    View.OnClickListener ButtonEditHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new android.view.ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Assign style
            android.widget.PopupMenu popup = new android.widget.PopupMenu(wrapper, Edit); //Creatting popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_edit_visite, popup.getMenu());  //menu popup
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals(getString(R.string.TextEditvisite))){
                        Intent intent = new Intent(ModificationVisite.this, EditVisite.class);
                        finish();
                        startActivity(intent);
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    //Bouton back Toolbar
    View.OnClickListener ButtonbackHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };
    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationVisite.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(ModificationVisite.this, ListViste.class);
                finish();
                startActivity(intent);
              //  Delete_cache();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
               //  clearData();
                Intent intent = new Intent(ModificationVisite.this, ListViste.class);
                finish();
                startActivity(intent);
              //  Delete_Cache();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Get the list of visit in a Organization
    private List<Visite> getListVisite(Entreprise entreprise_input){
        return entreprise_input.getVisiteList();
    }

    //Get a visit in a list of visit
    private Visite getVisite(long ID_Visite, List <Visite> list_visite){
        Visite result = null;
        if (list_visite != null){
            for (Visite V : list_visite){
                if (V.getId() == ID_Visite){
                    result = V;
                }
            }
        }
        return result;
    }
    //Return an Organization
    private Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationVisite.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

}
