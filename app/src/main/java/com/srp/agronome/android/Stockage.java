package com.srp.agronome.android;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.ETAT_STOCKEUR;
import com.srp.agronome.android.db.ETAT_STOCKEURDao;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Geolocalisation;
import com.srp.agronome.android.db.GeolocalisationDao;

import com.srp.agronome.android.db.Photo_Stockage;
import com.srp.agronome.android.db.Photo_StockageDao;
import com.srp.agronome.android.db.STATUT_STOCKEUR;
import com.srp.agronome.android.db.STATUT_STOCKEURDao;
import com.srp.agronome.android.db.STOCKAGE;
import com.srp.agronome.android.db.STOCKAGEDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;
import com.srp.agronome.android.db.TYPE_STOCKEUR;
import com.srp.agronome.android.db.TYPE_STOCKEURDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;

/**
 * Created by vincent on 07/08/17.
 */

public class Stockage extends AppCompatActivity {
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    static final int CAMERA_PIC_REQUEST = 1;

    private EditText NOM_STOCKEUR   = null;
    private final static String KeyNOM_STOCKEUR  = "NOM_STOCKEUR ";


    private ImageView Geolocalisation_stockage = null;

    private EditText IDENTIFIANT_STOCKEUR   = null;
    private final static String KeyIDENTIFIANT_STOCKEUR = "IDENTIFIANT_STOCKEUR ";

    private EditText CAPACITE    = null;
    private final static String KeyCAPACITE  = "CAPACITE";


    private Spinner SpinnerTYPE_STOCKEUR = null;
    private Spinner  SpinnerETAT_STOCKEUR = null;
    private Spinner SpinnerSTATUT_STOCKEUR = null;
    private ImageButton imgBtnHome = null;
    private Button BtnValidate = null;
    private Button PrendrePhoto = null;
    static TextView TextDate = null;
    static Date Debut_Projet = null;
    private final static String KeyDate = "KeyDate";
    private final static String datepicker = "datepicker";

    private double GPS_Latitude = 0;
    private double GPS_Longitude = 0 ;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stockage); //Associer le layout
        FindWidgetViewbyId(); //Associate Widgets
        AddListenerEvent(); //Add Widgets Event
        GetStringData(); //Read the recorded data and put it in the definition in edit text
        CreateListIntoDatabase();
        //Load spinner Type stockeur
        LoadSpinnerTYPE_STOCKEUR(SpinnerTYPE_STOCKEUR,getString(R.string.TextTYPE_STOCKEUR),
                getString(R.string.TextanotherTYPE_STOCKEUR),getString(R.string.TextValidateTYPE_STOCKEUR),
                getString(R.string.TextInputTYPE_STOCKEUR));

        LoadSpinnerETAT_STOCKEUR(SpinnerETAT_STOCKEUR,getString(R.string.TextETAT_STOCKEUR),
                getString(R.string.TextanotherETAT_STOCKEUR),getString(R.string.TextValidateETAT_STOCKEUR),
                getString(R.string.TextInputETAT_STOCKEUR));

        LoadSpinnerSTATUT_STOCKEUR(SpinnerSTATUT_STOCKEUR,getString(R.string.TextSTATUT_STOCKEUR),
                getString(R.string.TextanotherSTATUT_STOCKEUR),getString(R.string.TextValidateSTATUT_STOCKEUR),
                getString(R.string.TextInputSTATUT_STOCKEUR));
        GetDate();

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_stockage);
        imgBtnHome.setOnClickListener(ButtonbackHandler);

        BtnValidate = (Button) findViewById(R.id.btnvalidate_CQ);
        BtnValidate.setOnClickListener(ButtonValidateHandler);

        PrendrePhoto = (Button) findViewById(R.id.stockage_Photo);
        PrendrePhoto.setOnClickListener(ButtonPrendrePhotoHandler);
    }
    View.OnClickListener TextViewHandler = new View.OnClickListener() {
        public void onClick(View v) {
            showDatePickerDialog(v);
        }
    };
    public void showDatePickerDialog(View v) {

        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(),datepicker);
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Date displayed at the opening of the dialog box
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);  //
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Creates a new instance and returns it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (month <= 8) {
                Stockage.TextDate.setText(day + "/0" + (month + 1) + "/" + year);
                Stockage.SaveDateInput(KeyDate, TextDate.getText().toString());
                Debut_Projet = Create_date(year, month, day);

            } else {
                TextDate.setText(day + "/" + (month + 1) + "/" + year);
                Stockage.SaveDateInput(KeyDate, TextDate.getText().toString());
                Debut_Projet = Create_date(year, month, day);
            }
        }

    }
    public static Date Create_date(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private static void SaveDateInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }
    //Bouton back Toolbar
    View.OnClickListener ButtonbackHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };
    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Stockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Delete_Cache();
                Intent intent = new Intent(Stockage.this, MenuParcelle.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                Delete_Cache();
                Intent intent = new Intent(Stockage.this, MenuParcelle.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
        return true;
    }

    private void FindWidgetViewbyId() {
        NOM_STOCKEUR   = (EditText) findViewById(R.id.NOM_STOCKEUR);
        IDENTIFIANT_STOCKEUR   = (EditText) findViewById(R.id.IDENTIFIANT_STOCKEUR);
        CAPACITE   = (EditText) findViewById(R.id.CAPACITE);
        TextDate = (TextView) findViewById(R.id.Debut_projet);
        SpinnerTYPE_STOCKEUR = (Spinner) findViewById(R.id.SpinnerTYPE_STOCKEUR);
        SpinnerETAT_STOCKEUR = (Spinner) findViewById(R.id.SpinnerETAT_STOCKEUR);
        SpinnerSTATUT_STOCKEUR = (Spinner) findViewById(R.id.SpinnerSTATUT_STOCKEUR);
        Geolocalisation_stockage = (ImageView) findViewById(R.id.ibtnGeolocalisation_stockage);
    }

    //addTextChangedListener : Adds a TextWatcher to the list of those whose methods are called whenever this TextView's text changes.
    private void AddListenerEvent() {

        NOM_STOCKEUR.addTextChangedListener(generalTextWatcher);
        IDENTIFIANT_STOCKEUR.addTextChangedListener(generalTextWatcher);
        CAPACITE.addTextChangedListener(generalTextWatcher);
        TextDate.setOnClickListener(TextViewHandler);
        Geolocalisation_stockage.setOnClickListener(Handler_Geolocalisation_stockage);
    }

    View.OnClickListener Handler_Geolocalisation_stockage = new View.OnClickListener() {
         public void onClick(View v) {
             // create class object
             GPSTracker gps = new GPSTracker(Stockage.this);
             // check if GPS enabled
             if (gps.canGetLocation()) {
                 GPS_Latitude = gps.getLatitude();
                 GPS_Longitude = gps.getLongitude();
                 //Display Values
                 Toast.makeText(getApplicationContext(), getString(R.string.TitleLocationFound) + "\n" +
                                 getString(R.string.TextLatitude) + " " + GPS_Latitude + "\n" +
                                 getString(R.string.TextLongitude) + " " + GPS_Longitude
                         , Toast.LENGTH_LONG).show();
             } else {
                 // can't get location
                 // GPS or Network is not enabled
                 // Ask user to enable GPS/network in settings
                 gps.showSettingsAlert();
             }
         }
     };

    //Event Multiple EditText = save data
    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {


            if (NOM_STOCKEUR .getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyNOM_STOCKEUR , NOM_STOCKEUR .getText().toString());
            }else if (IDENTIFIANT_STOCKEUR  .getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyIDENTIFIANT_STOCKEUR  , IDENTIFIANT_STOCKEUR  .getText().toString());
            }else if (CAPACITE  .getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCAPACITE  , CAPACITE  .getText().toString());
            }

        }};

    private void SaveStringDataInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }


    // Read the recorded data and define it in the corresponding edit text
    private void GetStringData() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode

        if (sharedpreferences.contains(KeyNOM_STOCKEUR )) {
            NOM_STOCKEUR .setText(sharedpreferences.getString(KeyNOM_STOCKEUR  , ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyIDENTIFIANT_STOCKEUR )) {
            IDENTIFIANT_STOCKEUR  .setText(sharedpreferences.getString(KeyIDENTIFIANT_STOCKEUR  , ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCAPACITE)) {
            CAPACITE  .setText(sharedpreferences.getString(KeyCAPACITE , ""), TextView.BufferType.EDITABLE);
        }
    }
    private static void GetDate() {
        if (sharedpreferences.contains(KeyDate)) {
            TextDate.setText(sharedpreferences.getString(KeyDate, ""));
        }
    }
    //Clear data in sharedpreference
    private void clearData() {
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.remove(KeyNOM_STOCKEUR);
        editor.remove(KeyIDENTIFIANT_STOCKEUR);
        editor.remove(KeyCAPACITE);
        editor.remove(KeyDate);
        editor.apply();

    }


    //Bouton Valider Toolbar
    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();}
        } };
    // Method to check if the user has entered the data

    private Boolean CheckInput(){
        boolean b = true;

        //Vérification des texte editables

        if (TextUtils.isEmpty(NOM_STOCKEUR .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(IDENTIFIANT_STOCKEUR  .getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(CAPACITE  .getText().toString())) {
            b = false;
        }

        //Vérification des listes de choix :
        if (SpinnerTYPE_STOCKEUR.getSelectedItem().toString().equals(getString(R.string.TextTYPE_STOCKEUR))) {
            b = false;
        }
        if (SpinnerETAT_STOCKEUR.getSelectedItem().toString().equals(getString(R.string.TextETAT_STOCKEUR))) {
            b = false;
        }
        if (SpinnerSTATUT_STOCKEUR.getSelectedItem().toString().equals(getString(R.string.TextSTATUT_STOCKEUR))) {
            b = false;
        }

        return b;
    }
    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Stockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                // create stockage into db
                LoadingDialogInsertStockageDB();
                DisplaystockageInLog();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void DisplaystockageInLog(){


    }



    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewQC);
        int position = 0 ;

        if (TextUtils.isEmpty(NOM_STOCKEUR.getText().toString())) {
            position = IDENTIFIANT_STOCKEUR.getTop();
        }

        if (TextUtils.isEmpty(IDENTIFIANT_STOCKEUR.getText().toString())) {
            position = IDENTIFIANT_STOCKEUR.getTop();
        }
        if (TextUtils.isEmpty(CAPACITE.getText().toString())) {
            position = CAPACITE.getTop();
        } }
    //A method that creates a string listing the data entered.
    private String InputToText(){
        String text = getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.NOM_STOCKEUR) + " : " + NOM_STOCKEUR.getText().toString() + "\n";
        text += getString(R.string.IDENTIFIANT_STOCKEUR) + " : " +IDENTIFIANT_STOCKEUR.getText().toString() + "\n";
        text += getString(R.string.CAPACITE) + " : " + CAPACITE.getText().toString() + "\n\n";
        text += getString(R.string.datedebutprojet) + " : " + TextDate.getText().toString() + "\n\n";
        text += getString(R.string.TextTYPE_STOCKEUR) + " : " + SpinnerTYPE_STOCKEUR.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextETAT_STOCKEUR) + " : " + SpinnerETAT_STOCKEUR.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextSTATUT_STOCKEUR) + " : " + SpinnerSTATUT_STOCKEUR.getSelectedItem().toString() + "\n\n";

        return text;
    }


    View.OnClickListener ButtonPrendrePhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            TakePictureIntent();
        }
    };

    // Method for creating the selection lists in the database
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_STOCKEURDao TYPE_STOCKEUR_data = daoSession.getTYPE_STOCKEURDao();
        ETAT_STOCKEURDao ETAT_STOCKEUR_data = daoSession.getETAT_STOCKEURDao();
        STATUT_STOCKEURDao STATUT_STOCKEUR_data = daoSession.getSTATUT_STOCKEURDao();

        List<TYPE_STOCKEUR> List_TYPE_STOCKEUR = TYPE_STOCKEUR_data.loadAll();
        if (List_TYPE_STOCKEUR.size() == 0) {

            TYPE_STOCKEUR Type_Stockeur_input_1 = new TYPE_STOCKEUR();
            Type_Stockeur_input_1.setNom("Cellule");
            TYPE_STOCKEUR_data.insertOrReplace(Type_Stockeur_input_1);

            TYPE_STOCKEUR Type_Stockeur_input_2 = new TYPE_STOCKEUR();
            Type_Stockeur_input_2.setNom("Plat");
            TYPE_STOCKEUR_data.insertOrReplace(Type_Stockeur_input_2);
        }

        List <ETAT_STOCKEUR> List_ETAT_STOCKEUR = ETAT_STOCKEUR_data.loadAll();
        if (List_ETAT_STOCKEUR.size() == 0){

            ETAT_STOCKEUR Etat_Stockeur_input_1 = new ETAT_STOCKEUR();
            Etat_Stockeur_input_1.setNom("Existant");
            ETAT_STOCKEUR_data.insertOrReplace(Etat_Stockeur_input_1);

            ETAT_STOCKEUR Etat_Stockeur_input_2 = new ETAT_STOCKEUR();
            Etat_Stockeur_input_2.setNom("Projet");
            ETAT_STOCKEUR_data.insertOrReplace(Etat_Stockeur_input_2);
        }

        List <STATUT_STOCKEUR> List_STATUT_STOCKEUR = STATUT_STOCKEUR_data.loadAll();
        if (List_STATUT_STOCKEUR.size() == 0){

            STATUT_STOCKEUR Statut_Stockeur_input_1 = new STATUT_STOCKEUR();
            Statut_Stockeur_input_1.setNom("Autorisé de livraison");
            STATUT_STOCKEUR_data.insertOrReplace(Statut_Stockeur_input_1);

            STATUT_STOCKEUR Statut_Stockeur_input_2 = new STATUT_STOCKEUR();
            Statut_Stockeur_input_2.setNom("Sujet à contre-visite");
            STATUT_STOCKEUR_data.insertOrReplace(Statut_Stockeur_input_2);

            STATUT_STOCKEUR Statut_Stockeur_input_3 = new STATUT_STOCKEUR();
            Statut_Stockeur_input_3.setNom("Interdit de livraison");
            STATUT_STOCKEUR_data.insertOrReplace(Statut_Stockeur_input_3);
        }
    }

    private void LoadSpinnerTYPE_STOCKEUR (Spinner spinner , String TextHint , String TextAnother, final String TitleAddNew, final String TextAddNew){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_STOCKEURDao TYPE_STOCKEUR_data = daoSession.getTYPE_STOCKEURDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<TYPE_STOCKEUR> List_TYPE_STOCKEUR = TYPE_STOCKEUR_data.loadAll();
        if (List_TYPE_STOCKEUR != null) {
            for (TYPE_STOCKEUR TS : List_TYPE_STOCKEUR) {
                adapter.add(TS.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew,TextAddNew,1);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    private void LoadSpinnerETAT_STOCKEUR (Spinner spinner , String TextHint , String TextAnother, final String TitleAddNew, final String TextAddNew){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_STOCKEURDao ETAT_STOCKEUR_data = daoSession.getETAT_STOCKEURDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<ETAT_STOCKEUR> List_ETAT_STOCKEUR = ETAT_STOCKEUR_data.loadAll();
        if (List_ETAT_STOCKEUR != null) {
            for (ETAT_STOCKEUR ES : List_ETAT_STOCKEUR) {
                adapter.add(ES.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew,TextAddNew,2);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    private void LoadSpinnerSTATUT_STOCKEUR (Spinner spinner , String TextHint , String TextAnother, final String TitleAddNew, final String TextAddNew){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STATUT_STOCKEURDao STATUT_STOCKEUR_data = daoSession.getSTATUT_STOCKEURDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<STATUT_STOCKEUR> List_STATUT_STOCKEUR = STATUT_STOCKEUR_data.loadAll();
        if (List_STATUT_STOCKEUR != null) {
            for (STATUT_STOCKEUR SS : List_STATUT_STOCKEUR) {
                adapter.add(SS.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Type Culture
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew,TextAddNew,3);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(Stockage.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code TYPE_STOCKEUR
                        addTYPE_STOCKEUR(NewValueList);
                        break;
                    case 2 : //Code ETAT_STOCKEUR
                        addETAT_STOCKEUR(NewValueList);
                        break;
                    case 3 : //Code STATUT_STOCKEUR
                        addSTATUT_STOCKEUR(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void addTYPE_STOCKEUR(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        TYPE_STOCKEURDao TYPE_STOCKEUR_data = daoSession.getTYPE_STOCKEURDao();
        TYPE_STOCKEUR TYPE_STOCKEUR_input = new TYPE_STOCKEUR();
        TYPE_STOCKEUR_input.setNom(Nom);
        TYPE_STOCKEUR_data.insertOrReplace(TYPE_STOCKEUR_input);
        LoadSpinnerTYPE_STOCKEUR(SpinnerTYPE_STOCKEUR,getString(R.string.TextTYPE_STOCKEUR),
                getString(R.string.TextanotherTYPE_STOCKEUR),getString(R.string.TextValidateTYPE_STOCKEUR),
                getString(R.string.TextInputTYPE_STOCKEUR));
    }

    private void addETAT_STOCKEUR(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_STOCKEURDao ETAT_STOCKEUR_data = daoSession.getETAT_STOCKEURDao();
        ETAT_STOCKEUR ETAT_STOCKEUR_input = new ETAT_STOCKEUR();
        ETAT_STOCKEUR_input.setNom(Nom);
        ETAT_STOCKEUR_data.insertOrReplace(ETAT_STOCKEUR_input);
        LoadSpinnerETAT_STOCKEUR(SpinnerETAT_STOCKEUR,getString(R.string.TextETAT_STOCKEUR),
                getString(R.string.TextanotherETAT_STOCKEUR),getString(R.string.TextValidateETAT_STOCKEUR),
                getString(R.string.TextInputETAT_STOCKEUR));
    }

    private void addSTATUT_STOCKEUR(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STATUT_STOCKEURDao STATUT_STOCKEUR_data = daoSession.getSTATUT_STOCKEURDao();
        STATUT_STOCKEUR STATUT_STOCKEUR_input = new STATUT_STOCKEUR();
        STATUT_STOCKEUR_input.setNom(Nom);
        STATUT_STOCKEUR_data.insertOrReplace(STATUT_STOCKEUR_input);
        LoadSpinnerSTATUT_STOCKEUR(SpinnerSTATUT_STOCKEUR,getString(R.string.TextSTATUT_STOCKEUR),
                getString(R.string.TextanotherSTATUT_STOCKEUR),getString(R.string.TextValidateSTATUT_STOCKEUR),
                getString(R.string.TextInputSTATUT_STOCKEUR));
    }


    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    private  void insertstockagedb(){
        if ( CreateStockageIntoDatabase(NOM_STOCKEUR.getText().toString().trim(), IDENTIFIANT_STOCKEUR.getText().toString().trim(),
                Integer.valueOf(CAPACITE.getText().toString().trim()),Debut_Projet, SpinnerTYPE_STOCKEUR.getSelectedItem().toString(),
                SpinnerETAT_STOCKEUR.getSelectedItem().toString(),
                SpinnerSTATUT_STOCKEUR.getSelectedItem().toString() )!= null) {

            STOCKAGE stockageIntoDatabase = CreateStockageIntoDatabase(NOM_STOCKEUR.getText().toString().trim(), IDENTIFIANT_STOCKEUR.getText().toString().trim(),
                    Integer.valueOf(CAPACITE.getText().toString().trim()),Debut_Projet, SpinnerTYPE_STOCKEUR.getSelectedItem().toString(),
                    SpinnerETAT_STOCKEUR.getSelectedItem().toString(),
                    SpinnerSTATUT_STOCKEUR.getSelectedItem().toString());

            //Add GPS if coordinates have changed
            if ((GPS_Latitude != 0)&(GPS_Longitude != 0)){
                addGeolocalisationToStockage(stockageIntoDatabase,GPS_Latitude,GPS_Longitude);
            }

            //Link the new suivi producter to Ilot in DB
            SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
            addStockageToSuiviProducteur(sp, stockageIntoDatabase);
            SaveAndAddAllPhotoToStockage(stockageIntoDatabase);
            Delete_Cache();
        }
    }


    //Methodes base de données pour Jitendra

    private STOCKAGE CreateStockageIntoDatabase(String Nom , String Identifiant,int Capacite, Date Debut_Projet , String Type_Stockeur , String Etat_Stockeur, String Statut_Stockeur){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        TYPE_STOCKEURDao type_stockeur_data = daoSession.getTYPE_STOCKEURDao();
        ETAT_STOCKEURDao etat_stockeur_data = daoSession.getETAT_STOCKEURDao();
        STATUT_STOCKEURDao statut_stockeur_data = daoSession.getSTATUT_STOCKEURDao();

        STOCKAGE stockage_input = new STOCKAGE();
        stockage_input.setNOM_STOCKEUR(Nom);
        stockage_input.setIDENTIFIANT_STOCKEUR(Identifiant);
        stockage_input.setCAPACITE(Capacite);
        stockage_input.setDATE_DEBUT_PROJET(Debut_Projet);
        stockage_input.setArchive(1);
        stockage_input.setDate_creation(Today_Date());
        stockage_input.setDate_modification(Today_Date());
        stockage_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        stockage_input.setID_Agronome_Modification((int)ID_Compte_Selected);

        //Ajout Type Stockeur
        List<TYPE_STOCKEUR> List_Type_Stockeur = type_stockeur_data.queryBuilder()
                .where(TYPE_STOCKEURDao.Properties.Nom.eq(Type_Stockeur))
                .list();
        if (List_Type_Stockeur != null) {
            for (TYPE_STOCKEUR TS : List_Type_Stockeur) {
                stockage_input.setTYPE_STOCKEUR(TS);
                stockage_input.setID_TYPE_STOCKEUR(TS.getId());
            }
        }

        //Ajout Etat Stockeur
        List<ETAT_STOCKEUR> List_Etat_Stockeur = etat_stockeur_data.queryBuilder()
                .where(ETAT_STOCKEURDao.Properties.Nom.eq(Etat_Stockeur))
                .list();
        if (List_Etat_Stockeur != null) {
            for (ETAT_STOCKEUR ES : List_Etat_Stockeur ) {
                stockage_input.setETAT_STOCKEUR(ES);
                stockage_input.setID_ETAT_STOCKEUR(ES.getId());
            }
        }

        //Ajout Statut Stockeur
        List<STATUT_STOCKEUR> List_Statut_Stockeur = statut_stockeur_data.queryBuilder()
                .where(STATUT_STOCKEURDao.Properties.Nom.eq(Statut_Stockeur))
                .list();
        if (List_Statut_Stockeur  != null) {
            for (STATUT_STOCKEUR SS : List_Statut_Stockeur ) {
                stockage_input.setSTATUT_STOCKEUR(SS);
                stockage_input.setID_STATUT_STOCKEUR(SS.getId());
            }
        }
        stockage_data.insertOrReplace(stockage_input); //Insert in DB
        return stockage_input;
    }

    private SUIVI_PRODUCTEUR getSuiviProducteurOfOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        SUIVI_PRODUCTEUR suivi_producteur_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        suivi_producteur_result = e.getSUIVI_PRODUCTEUR();
                    }
                }
            }
        }
        return suivi_producteur_result;
    }

    private void addStockageToSuiviProducteur(SUIVI_PRODUCTEUR suivi_producteur_input , STOCKAGE stockage_input){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
        stockage_input.setID_SUIVI_PRODUCTEUR(suivi_producteur_input.getId());
        stockage_data.update(stockage_input);
        suivi_producteur_input.resetSTOCKAGEList();
        suivi_producteur_data.update(suivi_producteur_input);
    }



    private STOCKAGE getStockageV2(long ID_Stockage){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        STOCKAGE result = null;
        List<STOCKAGE> stockage_load = stockage_data.queryBuilder()
                .where(STOCKAGEDao.Properties.Id.eq(ID_Stockage))
                .list();
        if (stockage_load != null) {
            for (STOCKAGE S : stockage_load) {
                result = S;
            }
        }
        return result;
    }

    private void DeleteStockage(STOCKAGE stockage_delete){
        if (CheckRelationsStockage(stockage_delete.getId())){
            if (stockage_delete.getArchive() == 2){
                Show_Dialog_Delete_Stockage(getString(R.string.TitleConfirmDelete) , getString(R.string.TextConfirmDelete), stockage_delete);
            }
            else{
                ArchiveStockage(stockage_delete);
            }
        }
        else{
            Show_Dialog(getString(R.string.TitleDeleteFail),getString(R.string.TextDeleteFail));
        }
    }

    private boolean CheckRelationsStockage(long ID_Stockage){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
        QueryBuilder<SUIVI_PRODUCTEUR> queryBuilder = suivi_producteur_data.queryBuilder();
        queryBuilder.join(STOCKAGE.class, STOCKAGEDao.Properties.ID_SUIVI_PRODUCTEUR)
                .where(STOCKAGEDao.Properties.Id.eq(ID_Stockage));
        List<SUIVI_PRODUCTEUR> list_SP = queryBuilder.list();
        return (list_SP.size() < 2 );
    }


    private void ArchiveStockage(STOCKAGE stockage_delete){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        stockage_delete.setArchive(0);
        stockage_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        stockage_delete.setDate_modification(Today_Date());
        stockage_data.update(stockage_delete);
    }

    //Display Dialog Delete_Parcelle
    private void Show_Dialog_Delete_Stockage(String title, String message , final STOCKAGE stockage_delete) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Stockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArchiveStockage(stockage_delete);
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Stockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

    //Add Latitude and Longitude position to a Stockage
    private void addGeolocalisationToStockage (STOCKAGE stockage, double latitude, double longitude){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();
        Geolocalisation geolocalisation_input = new Geolocalisation();
        geolocalisation_input.setLatitude(latitude);
        geolocalisation_input.setLongitude(longitude);
        geolocalisation_data.insertOrReplace(geolocalisation_input);
        stockage.setGeolocalisation(geolocalisation_input);
        stockage.setID_GEOLOCALISATION_STOCKAGE(geolocalisation_input.getId());
        stockage_data.update(stockage);
    }

    //Get Latitude and Longitude position of a Stockage
    private Geolocalisation getGeolocalisationOfStockage (STOCKAGE stockage){
        return stockage.getGeolocalisation();
    }

    private void DeleteGeolocalisationStockage(STOCKAGE geolocalisation_stockage_delete){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();
        geolocalisation_data.delete(geolocalisation_stockage_delete.getGeolocalisation());
        stockage_data.update(geolocalisation_stockage_delete);
    }

    //Add a photo to a stockage
    private void AddPhotoToStockage(STOCKAGE stockage , String PhotoName){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        Photo_StockageDao photo_stockage_data = daoSession.getPhoto_StockageDao();
        Photo_Stockage photo_input = new Photo_Stockage();
        photo_input.setNom(PhotoName);
        photo_input.setID_Stockage(stockage.getId());
        photo_stockage_data.insertOrReplace(photo_input);
        stockage.resetPhoto_StockageList();
        stockage_data.update(stockage);
    }

    //Return the number of photo for one stockage
    private int CountPhotoStockage(STOCKAGE stockage){
        int result = 0;
        List <Photo_Stockage> list_photo = stockage.getPhoto_StockageList();
        if (list_photo != null){
            result = list_photo.size();
        }
        return result;
    }


    //Take a photo from Camera
    public void TakePictureIntent() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createFileImageCache(getPhotoCacheName());
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    private File createFileImageCache(String FileName) throws IOException {
        // Create a file with name
        File myDir = new File(getDirectoryPath(),"SRP_Agronome/Cache/");
        File file = new File(myDir, FileName);
        return file;
    }

    //Get the path of the directory : Intern or SD path
    private File getDirectoryPath(){
        SharedPreferences SP = getSharedPreferences("PrefValues", 0); // 0 = Private Mode
        String location = SP.getString("LocationStorage",null);
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (location.equals("Intern")){
            return Dirs[0];}
        else{
            return Dirs[1];}
    }

    //Permet de retourner les 3 premiers caracteres d'un texte
    private String getSubString(String chaine){
        String result="";
        if (chaine.length() >= 3){ result = chaine.substring(0,3);}
        if (chaine.length() == 2){ result = chaine.substring(0,2);}
        if (chaine.length() == 1){ result = chaine.substring(0,1);}
        return result ;
    }

    private String getPhotoNameHQ(String compte , String entreprise, STOCKAGE stockage){
        String nom_compte = getSubString(compte);
        String nom_stockage = getSubString(stockage.getNOM_STOCKEUR());
        return (nom_compte + "_" + entreprise + "_S_" + nom_stockage + "_HQ_" +
                CountPhotoStockage(stockage) + ".jpg");
    }

    private String getPhotoNameLQ(String compte ,String entreprise, STOCKAGE stockage){
        String nom_compte = getSubString(compte);
        String nom_stockage = getSubString(stockage.getNOM_STOCKEUR());
        return (nom_compte + "_" + entreprise + "_S_" + nom_stockage + "_LQ_" +
                CountPhotoStockage(stockage) + ".jpg");
    }

    //Return the name of photo cache (auto-incremented)
    private String getPhotoCacheName(){
        int number_file = 0;
        File CacheDir = new File(getDirectoryPath(),"SRP_Agronome/Cache/");
        File[] list_file = CacheDir.listFiles();
        if (list_file != null){
            number_file = list_file.length;}
        return "cache_" + number_file + ".jpg";
    }

    //Used to reach the photo after taking a photo
    private String getPhotoCacheNameAfterPhoto(){
        int number_file = 0;
        File CacheDir = new File(getDirectoryPath(),"SRP_Agronome/Cache/");
        File[] list_file = CacheDir.listFiles();
        if (list_file != null){
            number_file = list_file.length - 1 ;}
        return "cache_" + number_file + ".jpg";
    }

    //Delete all the file in "SRP_Agronome/Cache/"
    private void Delete_Cache(){
        File CacheDir = new File(getDirectoryPath(),"SRP_Agronome/Cache/");
        File[] list_file = CacheDir.listFiles();
        if (list_file != null) {
            for (File f : list_file) {
                f.delete();
            }
        }
    }

    //Add and Save All photos to Stockage
    private void SaveAndAddAllPhotoToStockage(STOCKAGE stockage){
        File CacheDir = new File(getDirectoryPath(),"SRP_Agronome/Cache/");
        File[] list_file = CacheDir.listFiles();
        if (list_file != null) {
            for (File file : list_file) { //For each file in "SRP_Agronome/Cache/"
                //Action for all File in Cache directory
                Uri uri = Uri.fromFile(file);
                Bitmap Photo_HQ;
                Bitmap Photo_LQ;
                try {
                    Photo_HQ = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap
                    Photo_HQ = rotateImageIfRequired(Photo_HQ,uri); //Rotate Bitmap
                    Photo_LQ = getResizedBitmap(Photo_HQ,1000,1000); //Compress Bitmap
                    String Nom_Photo_HQ = getPhotoNameHQ(Nom_Compte,Nom_Entreprise,stockage);
                    String Nom_Photo_LQ = getPhotoNameLQ(Nom_Compte,Nom_Entreprise,stockage);
                    SaveImage(Photo_HQ,Nom_Photo_HQ); //Save Photo HQ in Storage
                    SaveImage(Photo_LQ,Nom_Photo_LQ); //Save Photo LQ in Storage
                    AddPhotoToStockage(stockage,Nom_Photo_HQ); //Add Photo DB
                    Photo_HQ.recycle(); //Clear bitmap cache
                    Photo_LQ.recycle(); //Clear bitmap cache
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //Méthode pour sauvegarder une image bitmap dans le téléphone.
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Stockeurs/");
        File file = new File(myDir, FileName);
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            //Code Photo
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Cache/");
            File file = new File(myDir,getPhotoCacheNameAfterPhoto());
            Uri uri = Uri.fromFile(file);
            Bitmap Photo ;
            Bitmap Photo_LQ;
            try {
                Photo = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
                Photo = rotateImageIfRequired(Photo,uri); //Rotation si necessaire
                Photo_LQ = getResizedBitmap(Photo,1000,1000);
                Show_Dialog_Photo(Photo,Photo_LQ);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else{
            ei =  new ExifInterface(selectedImage.getPath());}

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Rotate an image with angle in degree
    private Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        //Log.i("Info Rotation","Rotation de l image de : " + degree);
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }

    private void Show_Dialog_Photo(final Bitmap bitmap , final Bitmap bitmap_LQ) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_photo, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final ImageView photo = (ImageView) dialogview.findViewById(R.id.photo_input);
        final Spinner Spinner_ThemePhoto = (Spinner) dialogview.findViewById(R.id.Spinner_Nom_Photo);
        Spinner_ThemePhoto.setVisibility(View.GONE);
        photo.setImageBitmap(bitmap_LQ);
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.TextButtonValider), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Bouton Confirmer l'enregistrement de la photo
                dialog.cancel();
                bitmap.recycle();
                bitmap_LQ.recycle();
            }
        });

        builder.setNegativeButton(getString(R.string.TextAnotherPhoto), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Bouton Autre Photo
                bitmap.recycle();
                bitmap_LQ.recycle();
                dialog.cancel();
                TakePictureIntent(); //Relance l'intent photo
            }
        });

        builder.setNeutralButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Annuler l'enregistrement de la photo
                File myDir = new File(getDirectoryPath(), "SRP_Agronome/Cache/");
                File file = new File(myDir,getPhotoCacheNameAfterPhoto());
                file.delete();
                bitmap.recycle();
                bitmap_LQ.recycle();
                dialog.cancel();

            }
        });

        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    public void LoadingDialogInsertStockageDB(){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(Stockage.this,
                getString(R.string.TitleChargement), getString(R.string.TextChargementStockeur), true);
        ringProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    insertstockagedb();
                } catch (Exception e) {
                }
                ringProgressDialog.dismiss();
                Intent intent = new Intent(Stockage.this, MenuParcelle.class);
                finish();
                startActivity(intent);
            }
        }).start();
    }



}