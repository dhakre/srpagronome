package com.srp.agronome.android;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.srp.agronome.android.MainLogin.ID_Contact_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.Nom_Contact;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;
import static com.srp.agronome.android.MainLogin.Prenom_Contact;

/**
 * Created by sumitra on 12/7/2017.
 */

public class CustomFilterContactAdapter extends ArrayAdapter<Contact> {
    ArrayList<Contact> data = new ArrayList<Contact>();


    public CustomFilterContactAdapter(Context context, ArrayList<Contact> data) {
        // super(context, layoutResourceId, data);
        super(context, 0, data);
//        this.layoutResourceId = layoutResourceId;
//        this.context = context;
        this.data = data;
        //   this.filteredList = data;
        //getFilter();
    }
    public class Holder {
        MyTextView Raison_Social;
        MyTextView Nom_Prenom;
        MyTextView Numero;
        MyTextView CodePostal;
        MyTextView Ville;
        ImageView img;
        ImageButton btn;
        ImageButton btn_call;
        ImageButton btn_direction;
        ImageButton btn_discussion;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // View row = convertView;
        CustomFilterContactAdapter.Holder holder = new CustomFilterContactAdapter.Holder();

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list, parent, false);
        }
        CustomImageAdapter.Holder viewHolder = (CustomImageAdapter.Holder) convertView.getTag();
        if (viewHolder == null) {

            holder.Raison_Social = (MyTextView) convertView.findViewById(R.id.TextListRaisonSociale);
            holder.Nom_Prenom = (MyTextView) convertView.findViewById(R.id.TextListName);
            holder.Numero = (MyTextView) convertView.findViewById(R.id.TextListNumberID);
            holder.CodePostal = (MyTextView) convertView.findViewById(R.id.TextListCodePostal);
            holder.Ville = (MyTextView) convertView.findViewById(R.id.TextListVille);

            holder.img = (ImageView) convertView.findViewById(R.id.imageView1);

            holder.btn = (ImageButton) convertView.findViewById(R.id.btn_voir_contact);
            holder.btn_call = (ImageButton) convertView.findViewById(R.id.btn_call);
            holder.btn_direction = (ImageButton) convertView.findViewById(R.id.btn_direction);
            holder.btn_discussion = (ImageButton) convertView.findViewById(R.id.btn_discussion);
            convertView.setTag(viewHolder);


        }

        final Contact contact = getItem(position);

        // LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        //row = inflater.inflate(layoutResourceId, parent, false);

        // linearMain = (LinearLayout) row.findViewById(R.id.lineraMain);
        //ImageView image = new ImageView(context);
        //final Contact contact = data.get(position);


        // Typeface Regular = Typeface.createFromAsset(getContext().getAssets(), "Exo/Exo-Regular.ttf");
        // Typeface Thin = Typeface.createFromAsset(getContext().getAssets(), "Exo/Exo-Thin.ttf");

        holder.Raison_Social.setText(contact.getStatut_soytouch());
        // holder.Raison_Social.setTypeface(Regular);
        String cnom=contact.getNom();
        String prenom=contact.getPrenom();
        if(cnom.length()>=15)
        {
            cnom=cnom.substring(0,1);
        }
        if(prenom.length()>=15)
        {
            prenom=prenom.substring(0,1);
        }
        //holder.Nom_Prenom.setText(contact.getNom() + " " + contact.getPrenom());   //chnaged this line
        holder.Nom_Prenom.setText(cnom + " " + prenom);
        //holder.Nom_Prenom.setTypeface(Regular);
        holder.Numero.setText(contact.getNumeroContact());
        //holder.Numero.setTypeface(Regular);

        holder.CodePostal.setText(ListContact.removeNull(contact.getCode_postal()));
        // holder.CodePostal.setTypeface(Thin);

        holder.Ville.setText(ListContact.removeNull(contact.getVille()));
        // holder.Ville.setTypeface(Thin);

        String FileName = contact.getImage();
        File path= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if(FileName!=null) {
            try {

                //File f = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + contact.getStatut_soytouch() + "/Contacts/" + FileName);
                File f=new File(path,"SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + contact.getStatut_soytouch() + "/Contacts/" + FileName);
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                holder.img.setImageBitmap(b);
//                Log.i("Load Bitmap","L'image définie.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
//                Log.i("Load Bitmap","L'image est introuvable");
                holder.img.setImageResource(R.mipmap.ic_photomenu);
            }
        }
        else
        {
            Log.i(TAG, "FileName image: "+" The image file dont exist");
        }


        holder.btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Nom_Entreprise = contact.getStatut_soytouch();
                Nom_Contact = contact.getNom();
                Prenom_Contact = contact.getPrenom();
                ID_Entreprise_Selected = contact.getID_Entreprise();
                ID_Contact_Selected = contact.getID_Contact();
                Intent next = new Intent(getContext(), MenuAgriculteur.class);
                ((Activity) getContext()).finish();
                getContext().startActivity(next);
            }
        });

        //Bouton Appeler
        holder.btn_call.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String number = contact.getTel();
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + number));
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                getContext().startActivity(intent);
            }
        });

        //Bouton Carte
        holder.btn_direction.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String Adresse = contact.getAdresse();
                String CP = contact.getCode_postal();
                String Ville = contact.getVille();
                if (TextUtils.isEmpty(Adresse) || (TextUtils.isEmpty(CP)) || (TextUtils.isEmpty(Ville))) {
                    Show_Dialog("Erreur", "Veuillez d'abord renseigner l'adresse du contact");
                } else { // getLatitude() ; getLongitude();
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + Adresse + " " + CP + " " + Ville);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    getContext().startActivity(mapIntent);
                }
            }
        });

        //Bouton discussion
        holder.btn_discussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Prochainement disponible", Toast.LENGTH_SHORT).show();
            }
        });


        return convertView;

    }

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }



}
