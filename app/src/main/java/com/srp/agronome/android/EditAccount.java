package com.srp.agronome.android;



import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.widget.DatePicker;


import com.srp.agronome.android.db.Adresse;
import com.srp.agronome.android.db.AdresseDao;
import com.srp.agronome.android.db.Code_Postal_Ville;
import com.srp.agronome.android.db.Code_Postal_VilleDao;
import com.srp.agronome.android.db.Collaborateur;
import com.srp.agronome.android.db.CollaborateurDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Identite;
import com.srp.agronome.android.db.IdentiteDao;
import com.srp.agronome.android.db.Pays;
import com.srp.agronome.android.db.PaysDao;
import com.srp.agronome.android.db.Region;
import com.srp.agronome.android.db.RegionDao;
import com.srp.agronome.android.db.Sexe;
import com.srp.agronome.android.db.SexeDao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;

/**
 * Created by agronome on 31/05/17.
 */

public class EditAccount extends AppCompatActivity {

    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";

    private ImageView Account_Photo = null; //Photo du compte

    private EditText First_Name = null;
    private final static String KeyFirstName = "KeyFirstName";
    private EditText Last_Name = null;
    private final static String KeyLastName = "KeyLastName";


    static TextView TextDate = null;
    static Date Birth_Date = null;

    private final static String KeyDate = "KeyDate";
    private RadioGroup Sex = null;
    private EditText StreetAddress1 = null;
    private final static String KeyAddress1 = "KeyAddress1";
    private EditText StreetAddress2 = null;
    private final static String KeyAddress2 = "KeyAddress2";
    private EditText City = null;
    private final static String KeyCity = "KeyCity";
    private EditText State = null;
    private final static String KeyState = "KeyState";
    private EditText PostalCode = null;
    private final static String KeyPostalCode = "KeyPostalCode";
    private EditText Country = null;
    private final static String KeyCountry = "KeyCountry";
    private EditText PhoneNumber1 = null;
    private final static String KeyPhoneNumber1 = "KeyPhoneNumber1";
    private EditText PhoneNumber2 = null;
    private final static String KeyPhoneNumber2 = "KeyPhoneNumber2";
    private EditText Mail1 = null;
    private final static String KeyMail1 = "KeyMail1";
    private EditText Mail2 = null;
    private final static String KeyMail2 = "KeyMail2";
    private EditText Mail3 = null;
    private final static String KeyMail3 = "KeyMail3";

    /**
     * Image bouton pour valider la saisie
     */
    private ImageButton imgBtnValidate = null;
    /**
     * Image bouton pour annuler
     */
    private ImageButton imgBtnHome = null;

    private Button BtnValidate = null;

    private Button bntPhone;
    private Button bntEmail;

    private TextView Title_ToolBar = null;

    private static final int CAMERA_PIC_REQUEST = 2;
    private static final int GALLERY_PICK = 1;
    private static final int PICK_CROP = 3;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        FindWidgetViewbyId(); //Associate Widgets
        AddListenerEvent(); //Add Widgets Event
        GetStringData(); //Load String
        GetDate(); //Load Date
        Title_ToolBar = (TextView) findViewById(R.id.toolbar_title);



        if (FillOrEditData()) {
            //Editer les informations du compte
            Title_ToolBar.setText(getString(R.string.TitleToolbar));
            LoadDataIntoDataBase();
        } else {
            //Saisir les informations du compte
            Title_ToolBar.setText(getString(R.string.TitleToolbar2));
        }

        imgBtnValidate = (ImageButton) findViewById(R.id.btn_confirm);
        imgBtnValidate.setOnClickListener(ButtonValidateHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_mainlogin);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);


        bntPhone = (Button) findViewById(R.id.btnPhoneNO);
        bntPhone.setOnClickListener(ButtonPhone);

        bntEmail = (Button) findViewById(R.id.btnemail);
        bntEmail.setOnClickListener(ButtonMail);

        BtnValidate = (Button) findViewById(R.id.btnvalidate_edit_count);
        BtnValidate.setOnClickListener(ButtonValidateHandler);

        setPrefilledData();

        //Gerer le passage d'un widget à l'autre sur la date
        First_Name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_NEXT)){
                    showDatePickerDialog(v);
                }
                return false;
            }
        });
    }

    //set prefilled data
    private void setPrefilledData()
    {
        First_Name.setText("Agronome");
        Last_Name.setText("Agricultural");
        TextDate.setText("10/01/1987");
        StreetAddress1.setText("Pouy De Touges");
        PostalCode.setText("31430");
        City.setText("Tuolouse");
        Country.setText("France");
        PhoneNumber1.setText("1234567890");
        Mail1.setText("agronome@soytouch.com");

    }

    View.OnClickListener ButtonPhone = new View.OnClickListener() {
        public void onClick(View v) {
            PhoneNumber2.setVisibility((PhoneNumber2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);

        }
    };

    View.OnClickListener ButtonMail = new View.OnClickListener() {
        public void onClick(View v) {
            Mail2.setVisibility((Mail2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Mail3.setVisibility((Mail3.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);

        }
    };

    /**
     * Fonction qui permet de définir l'insertion ou l'édition dans la base de donnée
     *
     * @return Retourne un booléen : false = insertion ; true = edition
     */
    private Boolean FillOrEditData() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        Boolean result = false;
        List<Compte> info_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (info_compte != null) {
            for (Compte c : info_compte) {
                result = c.getCollaborateur() != null;
            }
        }
        return result;
    }



    View.OnClickListener TextViewHandler = new View.OnClickListener() {
        public void onClick(View v) {
            showDatePickerDialog(v);
        }
    };

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Date affichée à l'ouverture de la boite de dialogue
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR) - 30;  //
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Crée une nouvelle instance et la retourne
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (month <= 8) {
                EditAccount.TextDate.setText(day + "/0" + (month + 1) + "/" + year);
                EditAccount.SaveDateInput(KeyDate, TextDate.getText().toString());
                Birth_Date = Create_date(year, month, day);

            } else {
                TextDate.setText(day + "/" + (month + 1) + "/" + year);
                EditAccount.SaveDateInput(KeyDate, TextDate.getText().toString());
                Birth_Date = Create_date(year, month, day);
            }
        }
    }

    /**
     * Retourne une date.
     *
     * @param year  Année
     * @param month Mois
     * @param day   Jour
     * @return Retourne la date sous le format "Date"
     */
    public static Date Create_date(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Retourne la date et l'heure actuelle
     *
     * @return Retourne la date et l'heure actuelle sous le format Date
     */

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }


    private void FindWidgetViewbyId() {
        Account_Photo = (ImageView) findViewById(R.id.account_picture);
        First_Name = (EditText) findViewById(R.id.FirstName);
        Last_Name = (EditText) findViewById(R.id.LastName);
        Sex = (RadioGroup) findViewById(R.id.MaleOrFemale);
        TextDate = (TextView) findViewById(R.id.Birthday);
        StreetAddress1 = (EditText) findViewById(R.id.Address1);
        StreetAddress2 = (EditText) findViewById(R.id.Address2);
        City = (EditText) findViewById(R.id.City);
        State = (EditText) findViewById(R.id.State);
        PostalCode = (EditText) findViewById(R.id.PostalCode);
        Country = (EditText) findViewById(R.id.Country);
        PhoneNumber1 = (EditText) findViewById(R.id.PhoneNumber1);
        PhoneNumber2 = (EditText) findViewById(R.id.PhoneNumber2);
        Mail1 = (EditText) findViewById(R.id.Mail1);
        Mail2 = (EditText) findViewById(R.id.Mail2);
        Mail3 = (EditText) findViewById(R.id.Mail3);
    }

    private void AddListenerEvent() {
        Account_Photo.setOnClickListener(PhotoHandler);
        Last_Name.addTextChangedListener(generalTextWatcher);
        TextDate.setOnClickListener(TextViewHandler);
        StreetAddress1.addTextChangedListener(generalTextWatcher);
        StreetAddress2.addTextChangedListener(generalTextWatcher);
        City.addTextChangedListener(generalTextWatcher);
        State.addTextChangedListener(generalTextWatcher);
        PostalCode.addTextChangedListener(generalTextWatcher);
        Country.addTextChangedListener(generalTextWatcher);
        PhoneNumber1.addTextChangedListener(generalTextWatcher);
        PhoneNumber2.addTextChangedListener(generalTextWatcher);
        Mail1.addTextChangedListener(generalTextWatcher);
        Mail2.addTextChangedListener(generalTextWatcher);
        Mail3.addTextChangedListener(generalTextWatcher);
        First_Name.addTextChangedListener(generalTextWatcher);
    }

    //Event Multiple EditText = save data
    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (Last_Name.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyLastName, Last_Name.getText().toString());
            } else if (First_Name.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyFirstName, First_Name.getText().toString());
            } else if (StreetAddress1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAddress1, StreetAddress1.getText().toString());
            } else if (StreetAddress2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyAddress2, StreetAddress2.getText().toString());
            } else if (City.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCity, City.getText().toString());
            } else if (State.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyState, State.getText().toString());
            } else if (PostalCode.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPostalCode, PostalCode.getText().toString());
            } else if (Country.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyCountry, Country.getText().toString());
            } else if (PhoneNumber1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPhoneNumber1, PhoneNumber1.getText().toString());
            } else if (PhoneNumber2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyPhoneNumber2, PhoneNumber2.getText().toString());
            } else if (Mail1.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyMail1, Mail1.getText().toString());
            } else if (Mail2.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyMail2, Mail2.getText().toString());
            } else if (Mail3.getText().hashCode() == s.hashCode()) {
                SaveStringDataInput(KeyMail3, Mail3.getText().toString());
            }
        }
    };


    //Event Clic on Photo :
    View.OnClickListener PhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Selection_Photo_Choice();
        }
    };


    //Afficher une boîte de dialogue contenant une liste de choix pour affecter une photo
    public void Selection_Photo_Choice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditAccount.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionPhoto, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Prendre une photo
                        TakePicturefromCamera();
                        break;
                    case 1: //Choisir une photo depuis la galerie
                        pickImagefromGallery();
                        break;
                    case 2: //Rogner la photo affichée
                        //Verifie si il y a une photo
                        if (Account_Photo.getDrawable().getConstantState() != EditAccount.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            CropImage();
                        }
                        break;
                    case 3: //Supprimer la photo -> mettre l'image de base à la place
                        Account_Photo.setImageResource(R.mipmap.ic_add_photo);
                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }


    //Pick a photo from Gallery
    public void pickImagefromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICK);
    }

    //Take a photo from Camera
    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(Nom_Compte + "_profil_picture_HD.jpg");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_Pictures");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        return file;
    }

    //Get the path of the directory : Intern or SD path
    private File getDirectoryPath(){
        SharedPreferences SP = getSharedPreferences("PrefValues", 0); // 0 = Private Mode
        String location = SP.getString("LocationStorage",null);
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (location.equals("Intern")){
            return Dirs[0];}
        else{
            return Dirs[1];}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == GALLERY_PICK) { //Depuis la galerie
            Uri uri = data.getData();
            Bitmap Picture_Load;
            try {
                Log.i("Test","URI Gallery : " + uri.toString());
                Picture_Load = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                Picture_Load = rotateImageIfRequired(Picture_Load,uri,true); //Rotation si necessaire
                SaveImage(Picture_Load, Nom_Compte + "_profil_picture_HD.jpg"); //Sauvegarde l'image HD
                Picture_Load = getResizedBitmap(Picture_Load, 256, 256);
                Account_Photo.setImageBitmap(Picture_Load);
                SaveImage(Picture_Load, Nom_Compte + "_profil_picture.jpg"); //Sauvegarde l'image compressée
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_PIC_REQUEST) { //Depuis la caméra : pas de bitmap en sortie
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_Pictures");
            File file = new File(myDir, Nom_Compte + "_profil_picture_HD.jpg");
            Uri uri = Uri.fromFile(file);
            Bitmap newPicture;
            try {
                Log.i("Test","URI Camera : " + uri.toString());
                newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
                newPicture = rotateImageIfRequired(newPicture,uri,false); //Rotation si necessaire
                SaveImage(newPicture, Nom_Compte + "_profil_picture_HD.jpg");
                newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
                Account_Photo.setImageBitmap(newPicture);
                SaveImage(newPicture, Nom_Compte + "_profil_picture.jpg"); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_CROP) {
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_Pictures");
            File file = new File(myDir, Nom_Compte + "_profil_picture.jpg");
            Uri URI = Uri.fromFile(file);
            Bitmap Picture_Crop;
            try {
                Picture_Crop = MediaStore.Images.Media.getBitmap(getContentResolver(), URI);
                Account_Photo.setImageBitmap(Picture_Crop);
                SaveImage(Picture_Crop, Nom_Compte + "_profil_picture.jpg"); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Permet de rogner ou recadrer une image
    private void CropImage() {
        try {
            //Acceder à la photo à rogner
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_Pictures");
            File photoFile = new File(myDir, Nom_Compte + "_profil_picture.jpg");
            Uri photoURI = FileProvider.getUriForFile(this,
                    "com.srp.android.fileprovider",
                    photoFile);
            getApplicationContext().grantUriPermission("com.android.camera", photoURI,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //Paramètres de l'Intent Crop
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //Android N need set permission to uri otherwise system camera don't has permission to access file wait crop
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // indicate image type and Uri
            cropIntent.setDataAndType(photoURI, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PICK_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
        }
    }


    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_Pictures");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        //Log.i("File Location",file.toString());
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Méthode pour charger une image depuis le téléphone dans l'application.
    private void loadImageFromStorage(String FileName) {
        try {
            File f = new File(getDirectoryPath(), "SRP_Agronome/Account_Pictures/" + FileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            //Log.i("Load Bitmap","L'image est remplacée à l'écran");
            Account_Photo.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            //Log.i("Load Bitmap","L'image est introuvable");
        }
    }


    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage, boolean PickingGallery) throws IOException {

        InputStream input = getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            if (PickingGallery){
                ei = new ExifInterface(getPath(selectedImage));}
            else{
                ei =  new ExifInterface(selectedImage.getPath());}

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    //Get Path = Pick up a picture in Gallery = getRealPath
    private String getPath(Uri uri) {
        String[]  data = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    //Rotate an image with angle in degree
    private Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        Log.i("Info Rotation","Rotation de l image de : " + degree);
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }


    private void SaveStringDataInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }

    /**
     * Verifie si les champs sont renseignées
     *
     * @return Retourne un booleen
     */
    private boolean CheckInput() {
        boolean b = true;

        if (TextUtils.isEmpty(Last_Name.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(First_Name.getText().toString())) {
            b = false;
        }

        if (TextDate.getText().toString().equals(getString(R.string.DefaultBirthday))){
            b = false;
        }
        if (TextUtils.isEmpty(StreetAddress1.getText().toString())) {
            b = false;
        }

        if (TextUtils.isEmpty(City.getText().toString())) {
            b = false;
        }

        if (TextUtils.isEmpty(PostalCode.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(Country.getText().toString())) {
            b = false;
        }
        if (TextUtils.isEmpty(PhoneNumber1.getText().toString())) {
            b = false;
        }

        if (TextUtils.isEmpty(Mail1.getText().toString())) {
            b = false;
        }

        return b;
    }




    private static void SaveDateInput(String key, String Data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, Data);
        editor.apply();
    }


    // Lire les données enregistrées et les mettre dans les definir dans les edit text
    private void GetStringData() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
        if (sharedpreferences.contains(KeyLastName)) {
            Last_Name.setText(sharedpreferences.getString(KeyLastName, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyFirstName)) {
            First_Name.setText(sharedpreferences.getString(KeyFirstName, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAddress1)) {
            StreetAddress1.setText(sharedpreferences.getString(KeyAddress1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyAddress2)) {
            StreetAddress2.setText(sharedpreferences.getString(KeyAddress2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCity)) {
            City.setText(sharedpreferences.getString(KeyCity, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyState)) {
            State.setText(sharedpreferences.getString(KeyState, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPostalCode)) {
            PostalCode.setText(sharedpreferences.getString(KeyPostalCode, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyCountry)) {
            Country.setText(sharedpreferences.getString(KeyCountry, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPhoneNumber1)) {
            PhoneNumber1.setText(sharedpreferences.getString(KeyPhoneNumber1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyPhoneNumber2)) {
            PhoneNumber2.setText(sharedpreferences.getString(KeyPhoneNumber2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyMail1)) {
            Mail1.setText(sharedpreferences.getString(KeyMail1, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyMail2)) {
            Mail2.setText(sharedpreferences.getString(KeyMail2, ""), TextView.BufferType.EDITABLE);
        }
        if (sharedpreferences.contains(KeyMail3)) {
            Mail3.setText(sharedpreferences.getString(KeyMail3, ""), TextView.BufferType.EDITABLE);
        }
    }

    private static void GetDate() {
        if (sharedpreferences.contains(KeyDate)) {
            //Log.i("Test date", sharedpreferences.getString(KeyDate, ""));
            TextDate.setText(sharedpreferences.getString(KeyDate, ""));
        }
    }

    //Clear data in sharepreference
    private void clearData() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(KeyLastName);
        editor.remove(KeyFirstName);
        editor.remove(KeyDate);
        editor.remove(KeyAddress1);
        editor.remove(KeyAddress2);
        editor.remove(KeyCity);
        editor.remove(KeyState);
        editor.remove(KeyPostalCode);
        editor.remove(KeyCountry);
        editor.remove(KeyPhoneNumber1);
        editor.remove(KeyPhoneNumber2);
        editor.remove(KeyMail1);
        editor.remove(KeyMail2);
        editor.remove(KeyMail3);
        editor.apply();
    }


    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {

                String email1 = Mail1.getText().toString().trim();
                String email2 = Mail2.getText().toString().trim();
                String email3 = Mail3.getText().toString().trim();
                String number = PhoneNumber1.getText().toString().trim();

                String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

                String MobilePattern="";
                String CP_Pattern = "";
                //Pattern selon Pays de selection :
                sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
                String Format_Pays = sharedpreferences.getString("FormatCode","FR");
                switch (Format_Pays){
                    case "FR" : {
                        MobilePattern = "\"(0|\\\\+33|0033)[1-9][0-9]{8}\"";
                        CP_Pattern ="^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$";
                        break;}
                    case "US" :{
                        MobilePattern = "^[0-9]{3,3}[-]{1,1}[0-9]{3,3}[-]{1,1}[0-9]{4,4}$";
                        CP_Pattern = "^[A-Z]{1,2}([0-9]{1,2}|[0-9]{1,1}[A-Z]{1,1})( |)[0-9]{1,1}[A-Z]{2,2}$";
                        break;}
                    }

                    MobilePattern = "[0-9]{10}"; //Ancien a supprimer si les tests sont bons

                if (email1.matches(emailPattern) & (email2.matches(emailPattern) || TextUtils.isEmpty(email2)) & (email3.matches(emailPattern) || TextUtils.isEmpty(email3))) {
                    if (number.matches(MobilePattern)) {

                        // Validation du Sexe
                        String Text_Sex;
                        if (Sex.getCheckedRadioButtonId() == R.id.radiobtn_male) {
                            Text_Sex = getString(R.string.Radiobtn1);
                        } else {
                            Text_Sex = getString(R.string.Radiobtn2);
                        }

                        Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm), getString(R.string.textConfirm) +
                                "\n" + getString(R.string.TextLastName) + " : " + Last_Name.getText().toString() +
                                "\n" + getString(R.string.TextFirstName) + " : " + First_Name.getText().toString() +
                                "\n" + getString(R.string.datenaissance) + " : " + TextDate.getText().toString() +
                                "\n" + getString(R.string.TextSex) + " : " + Text_Sex +
                                //"\n" + getString(R.string.TextOrganization) + " : " + Organization.getText().toString() +
                                "\n" + getString(R.string.TextAddress) + " : " + StreetAddress1.getText().toString() +
                                "\n" + getString(R.string.TextAddress2) + " : " + StreetAddress2.getText().toString() +
                                "\n" + getString(R.string.TextCity) + " : " + City.getText().toString() +
                                "\n" + getString(R.string.TextState) + " : " + State.getText().toString() +
                                "\n" + getString(R.string.TextPostalCode) + " : " + PostalCode.getText().toString() +
                                "\n" + getString(R.string.TextCountry) + " : " + Country.getText().toString() +
                                "\n" + getString(R.string.TextPhoneNumber1) + " : " + PhoneNumber1.getText().toString() +
                                "\n" + getString(R.string.TextPhoneNumber2) + " : " + PhoneNumber2.getText().toString() +
                                "\n" + getString(R.string.TextMail1) + " : " + Mail1.getText().toString() +
                                "\n" + getString(R.string.TextMail2) + " : " + Mail2.getText().toString() +
                                "\n" + getString(R.string.TextMail3) + " : " + Mail3.getText().toString());
                    } else {
                        Show_Dialog(getString(R.string.warntitle), getString(R.string.warnphone));
                    }

                } else {
                    Show_Dialog(getString(R.string.warntitle), getString(R.string.warnemail));
                }


            } else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();}
        }
    };

    //Fonction qui permet de scroller automatiquement la vue sur le champ non rempli
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewEditAccount);
        int position = 0 ;

        if (TextUtils.isEmpty(Mail1.getText().toString())) {
            position = Mail1.getTop();
        }
        if (TextUtils.isEmpty(PhoneNumber1.getText().toString())) {
            position = PhoneNumber1.getTop();
        }
        if (TextUtils.isEmpty(Country.getText().toString())) {
            position = Country.getTop();
        }
        if (TextUtils.isEmpty(PostalCode.getText().toString())) {
            position = PostalCode.getTop();
        }
        if (TextUtils.isEmpty(City.getText().toString())) {
            position = City.getTop();
        }
        if (TextUtils.isEmpty(StreetAddress1.getText().toString())) {
            position =StreetAddress1.getTop();
        }
        if (TextUtils.isEmpty(First_Name.getText().toString())) {
            position = First_Name.getTop();
        }
        if (TextUtils.isEmpty(Last_Name.getText().toString())) {
            position = Last_Name.getTop();
        }

        final int Position_Y = position;
        final int Position_X = 0;
        sc.post(new Runnable() {
            public void run() {
                sc.scrollTo(Position_X, Position_Y); // these are your x and y coordinates
            }
        });
    }


    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (FillOrEditData()) {
                clearData();
                Intent intent = new Intent(EditAccount.this, MainMenu.class);
                finish();
                startActivity(intent);
            } else {
                Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
            }
        }
    };

    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditAccount.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                if (FillOrEditData()) {
                    UpdateDataIntoDataBase();
                } else {
                    CreateDataIntoDataBase();
                }

                clearData();
                Intent intent = new Intent(EditAccount.this, MainMenu.class);
                finish();
                startActivity(intent);
            }

        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Back pressed button
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (FillOrEditData()) {
                clearData();
                Intent intent = new Intent(EditAccount.this, MainMenu.class);
                finish();
                startActivity(intent);
            } else {
                Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
            }
        }
        return true;
    }

    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditAccount.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(EditAccount.this, MainLogin.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                clearData();
                Intent intent = new Intent(EditAccount.this, MainLogin.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditAccount.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

    public static boolean checkImageResource(Context ctx, ImageView imageView, int imageResource) {
        boolean result = false;
        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }
            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }
        return result;
    }


    private void CreateDataIntoDataBase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        CollaborateurDao collaborateur_data = daoSession.getCollaborateurDao();
        IdentiteDao identite_data = daoSession.getIdentiteDao();
        SexeDao sexe_data = daoSession.getSexeDao();
        AdresseDao adresse_data = daoSession.getAdresseDao();
        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
        RegionDao region_data = daoSession.getRegionDao();
        PaysDao pays_data = daoSession.getPaysDao();
        //GeolocalisationDao geolocalisation_data = daoSession.getGeolocalisationDao();

        //Table Collaborateur
        Collaborateur collaborateur_input = new Collaborateur();
        collaborateur_input.setAdresse_mail_gmail(Mail2.getText().toString().trim()); //Mail 2
        collaborateur_input.setAdresse_mail_entreprise(Mail3.getText().toString().trim()); // Mail 3
        collaborateur_input.setArchive(1);
        collaborateur_input.setDate_creation(Today_Date());
        collaborateur_input.setDate_modification(Today_Date());
        collaborateur_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        collaborateur_input.setID_Agronome_Modification((int)ID_Compte_Selected);


        //Table identité
        Identite identite_input = new Identite();
        identite_input.setNom(Last_Name.getText().toString().trim()); //Nom
        identite_input.setPrenom(First_Name.getText().toString().trim()); //Prenom
        identite_input.setDate_naissance(Birth_Date); //Date_de_naissance
        identite_input.setTelephone_principal(PhoneNumber1.getText().toString().trim()); //Telephone principale
        identite_input.setTelephone_secondaire(PhoneNumber2.getText().toString().trim()); //Telephoe secondaire
        identite_input.setAdresse_email(Mail1.getText().toString().trim()); //Mail 1
        identite_input.setArchive(1);
        identite_input.setDate_creation(Today_Date());
        identite_input.setDate_modification(Today_Date());
        identite_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        identite_input.setID_Agronome_Modification((int)ID_Compte_Selected);

        //Gestion de la photo : soit on a une photo, soit on en a pas.
//        if (Account_Photo.getDrawable().getConstantState() == EditAccount.this.getResources().
//                getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
//        if (Account_Photo.getDrawable() == ContextCompat.getDrawable(this, R.mipmap.ic_add_photo)){
//            identite_input.setPhoto("");
//            Log.i("BDD Photo","Pas de photo à ajouter en BD");
//        } else {
//            identite_input.setPhoto(Nom_Compte + "_profil_picture.jpg");
//            Log.i("BDD Photo","La photo va être ajouté en BD");
//        }

        if (checkImageResource(this,Account_Photo,R.mipmap.ic_add_photo)){
            identite_input.setPhoto("");
            Log.i("BDD Photo","Pas de photo à ajouter en BD");
        } else {
            identite_input.setPhoto(Nom_Compte + "_profil_picture.jpg");
            Log.i("BDD Photo","La photo va être ajouté en BD");
        }

        //Table Adresse
        Adresse adresse_input = new Adresse();
        adresse_input.setAdresse_numero_rue(StreetAddress1.getText().toString().trim()); //Adresse
        adresse_input.setComplement_adresse(StreetAddress2.getText().toString().trim()); //Complement Adresse
        adresse_input.setArchive(1);
        adresse_input.setDate_creation(Today_Date());
        adresse_input.setDate_modification(Today_Date());
        adresse_input.setID_Agronome_Creation((int)ID_Compte_Selected);
        adresse_input.setID_Agronome_Modification((int)ID_Compte_Selected);

        Sexe sexe_input = new Sexe();
        if (Sex.getCheckedRadioButtonId() == R.id.radiobtn_male) {
            sexe_input.setNom(getString(R.string.Radiobtn1));
        } else {
            sexe_input.setNom(getString(R.string.Radiobtn2));
        }
        sexe_data.insertOrReplace(sexe_input);


        Code_Postal_Ville code_postal_ville_input = new Code_Postal_Ville();
        code_postal_ville_input.setCode_postal(PostalCode.getText().toString().trim());
        code_postal_ville_input.setVille(City.getText().toString().trim());
        code_postal_ville_data.insertOrReplace(code_postal_ville_input);

        Region region_input = new Region();
        region_input.setNom(State.getText().toString().trim());
        region_data.insertOrReplace(region_input);

        Pays pays_input = new Pays();
        pays_input.setNom(Country.getText().toString().trim());
        pays_data.insertOrReplace(pays_input);

        //Cardinalités entre les tables :
        adresse_input.setCode_Postal_Ville(code_postal_ville_input);
        adresse_input.setID_Code_Postal_Ville(code_postal_ville_input.getId());
        adresse_input.setRegion(region_input);
        adresse_input.setID_Region(region_input.getId());
        adresse_input.setPays(pays_input);
        adresse_input.setID_Pays(pays_input.getId());
        adresse_data.insertOrReplace(adresse_input);

        identite_input.setAdresse(adresse_input);
        identite_input.setID_Adresse(adresse_input.getId());
        identite_input.setSexe(sexe_input);
        identite_input.setID_Sexe(sexe_input.getId());
        identite_data.insertOrReplace(identite_input);

        collaborateur_input.setIdentite(identite_input);
        collaborateur_input.setID_Identite(identite_input.getId());
        collaborateur_data.insertOrReplace(collaborateur_input);

        //Associer les tables au comptes
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                Log.i("Information 1", "Associer les informations au compte suivants :");
                Log.i("Information 2", "Login : " + c.getLogin() + "\nPassword : " + c.getPassword());
                c.setCollaborateur(collaborateur_input);
                c.setID_Collaborateur(collaborateur_input.getId());
                c.update();
            }
        }
    }

    //Charger les données de la base de données et remplis les edittext correspondants
    private void LoadDataIntoDataBase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        List<Compte> informations_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected)) //Charger le compte associé au login
                .list();
        if (informations_compte != null) {
            for (Compte c : informations_compte) {
                Last_Name.setText(c.getCollaborateur().getIdentite().getNom(), TextView.BufferType.EDITABLE);
                First_Name.setText(c.getCollaborateur().getIdentite().getPrenom(), TextView.BufferType.EDITABLE);

                // Raison Sociale n'est pas présent dans la base de données actuelle
                //Organization.setText("", TextView.BufferType.EDITABLE);

                //Charger le Sexe -> Checkbox cochée
                if (c.getCollaborateur().getIdentite().getSexe().getNom().equals(getString(R.string.Radiobtn1))) {
                    Sex.check(R.id.radiobtn_male);
                } else {
                    Sex.check(R.id.radiobtn_female);
                }

                //Charger la date de naissance -> EditText formaté en date jour/mois/année
                Birth_Date = c.getCollaborateur().getIdentite().getDate_naissance();
                Calendar cal = Calendar.getInstance();
                cal.setTime(Birth_Date);
                if (cal.get(Calendar.MONTH) <= 8) {
                    TextDate.setText(cal.get(Calendar.DAY_OF_MONTH) + "/0" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR));
                } else {
                    TextDate.setText(cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR));
                }

                //Charger la photo si elle existe en DB
                if (c.getCollaborateur().getIdentite().getPhoto().equals("")){
                    Account_Photo.setImageResource(R.mipmap.ic_add_photo);
                    Log.i("InfoPhoto","Photo de base definie");}
                else{
                    Log.i("Chargement_Photo",c.getCollaborateur().getIdentite().getPhoto());
                    loadImageFromStorage(c.getCollaborateur().getIdentite().getPhoto());}


                StreetAddress1.setText(c.getCollaborateur().getIdentite().getAdresse().getAdresse_numero_rue(), TextView.BufferType.EDITABLE);
                StreetAddress2.setText(c.getCollaborateur().getIdentite().getAdresse().getComplement_adresse(), TextView.BufferType.EDITABLE);
                City.setText(c.getCollaborateur().getIdentite().getAdresse().getCode_Postal_Ville().getVille(), TextView.BufferType.EDITABLE);
                State.setText(c.getCollaborateur().getIdentite().getAdresse().getRegion().getNom(), TextView.BufferType.EDITABLE);
                PostalCode.setText(String.valueOf(c.getCollaborateur().getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal()), TextView.BufferType.EDITABLE);
                Country.setText(c.getCollaborateur().getIdentite().getAdresse().getPays().getNom(), TextView.BufferType.EDITABLE);
                PhoneNumber1.setText(c.getCollaborateur().getIdentite().getTelephone_principal(), TextView.BufferType.EDITABLE);
                PhoneNumber2.setText(c.getCollaborateur().getIdentite().getTelephone_secondaire(), TextView.BufferType.EDITABLE);
                Mail1.setText(c.getCollaborateur().getIdentite().getAdresse_email(), TextView.BufferType.EDITABLE);
                Mail2.setText(c.getCollaborateur().getAdresse_mail_gmail(), TextView.BufferType.EDITABLE);
                Mail3.setText(c.getCollaborateur().getAdresse_mail_entreprise(), TextView.BufferType.EDITABLE);

            }
        }
    }

    private void UpdateDataIntoDataBase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        CollaborateurDao collaborateur_data = daoSession.getCollaborateurDao();
        IdentiteDao identite_data = daoSession.getIdentiteDao();
        SexeDao sexe_data = daoSession.getSexeDao();
        AdresseDao adresse_data = daoSession.getAdresseDao();
        Code_Postal_VilleDao code_postal_ville_data = daoSession.getCode_Postal_VilleDao();
        RegionDao region_data = daoSession.getRegionDao();
        PaysDao pays_data = daoSession.getPaysDao();

        List<Compte> informations_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected)) //Charger le compte associé au login
                .list();
        if (informations_compte != null) {
            for (Compte c : informations_compte) {

                c.getCollaborateur().getIdentite().setNom(Last_Name.getText().toString().trim());
                c.getCollaborateur().getIdentite().setPrenom(First_Name.getText().toString().trim());

                if (Sex.getCheckedRadioButtonId() == R.id.radiobtn_male) {
                    c.getCollaborateur().getIdentite().getSexe().setNom(getString(R.string.Radiobtn1));
                } else {
                    c.getCollaborateur().getIdentite().getSexe().setNom(getString(R.string.Radiobtn2));
                }

                c.getCollaborateur().setAdresse_mail_entreprise(Mail3.getText().toString().trim());
                c.getCollaborateur().setAdresse_mail_gmail(Mail2.getText().toString().trim());
                c.getCollaborateur().setDate_modification(Today_Date());
                c.getCollaborateur().setID_Agronome_Modification((int)ID_Compte_Selected);

                c.getCollaborateur().getIdentite().setDate_naissance(Birth_Date);
                c.getCollaborateur().getIdentite().setTelephone_principal(PhoneNumber1.getText().toString().trim());
                c.getCollaborateur().getIdentite().setTelephone_secondaire(PhoneNumber2.getText().toString().trim());
                c.getCollaborateur().getIdentite().setAdresse_email(Mail1.getText().toString().trim());
                c.getCollaborateur().getIdentite().setDate_modification(Today_Date());
                c.getCollaborateur().getIdentite().setID_Agronome_Modification((int)ID_Compte_Selected);

                //Gestion de la photo : soit on a une photo, soit on en a pas.
                if (checkImageResource(this,Account_Photo,R.mipmap.ic_add_photo)){
                    c.getCollaborateur().getIdentite().setPhoto("");
                } else {
                    c.getCollaborateur().getIdentite().setPhoto(Nom_Compte + "_profil_picture.jpg");
                }

                c.getCollaborateur().getIdentite().getAdresse().setAdresse_numero_rue(StreetAddress1.getText().toString().trim());
                c.getCollaborateur().getIdentite().getAdresse().setComplement_adresse(StreetAddress2.getText().toString().trim());
                c.getCollaborateur().getIdentite().getAdresse().setDate_modification(Today_Date());
                c.getCollaborateur().getIdentite().getAdresse().setID_Agronome_Modification(1);

                c.getCollaborateur().getIdentite().getAdresse().getCode_Postal_Ville().setVille(City.getText().toString().trim());
                c.getCollaborateur().getIdentite().getAdresse().getCode_Postal_Ville().setCode_postal(PostalCode.getText().toString().trim());

                c.getCollaborateur().getIdentite().getAdresse().getRegion().setNom(State.getText().toString().trim());
                c.getCollaborateur().getIdentite().getAdresse().getPays().setNom(Country.getText().toString().trim());

                //Update les tables : DAO !! pas les classes
                code_postal_ville_data.update(c.getCollaborateur().getIdentite().getAdresse().getCode_Postal_Ville());
                region_data.update(c.getCollaborateur().getIdentite().getAdresse().getRegion());
                pays_data.update(c.getCollaborateur().getIdentite().getAdresse().getPays());
                adresse_data.update(c.getCollaborateur().getIdentite().getAdresse());
                sexe_data.update(c.getCollaborateur().getIdentite().getSexe());
                identite_data.update(c.getCollaborateur().getIdentite());
                collaborateur_data.update(c.getCollaborateur());
                compte_dao.update(c);

                Log.i("Information Update", "Update du compte de " + Nom_Compte);
            }
        }
    }

}

