package com.srp.agronome.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;

import android.util.Log;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;

import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.ILOT;
import com.srp.agronome.android.db.STOCKAGE;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;
import com.srp.agronome.android.db.TYPE_STOCKEUR;
import com.srp.agronome.android.db.TYPE_STOCKEURDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuParcelle.Id_stockage_selected;
import static com.srp.agronome.android.MenuParcelle.Id_suivi_producteur;
/**
 * Created by jitendra on 21-08-2017.
 */

public class ListStockage extends AppCompatActivity {

    private ArrayList<StockageClass> arrayListstockage = new ArrayList<StockageClass>();

    private ImageButton btnRetour =null;
    private ListView listView_stockage;
    CustomStockageadapter stockageadapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_stockage);
        Loadliststockage();
        btnRetour = (ImageButton) findViewById(R.id.arrow_back_menu);
        btnRetour.setOnClickListener(ButtonRetourHandler);

        listView_stockage = (ListView) findViewById(R.id.listviewstockage); // see layout  activity_list_stockage

        // go to  layout list_stockage.xml to display the CustomStockageadapter.java value
        stockageadapter = new CustomStockageadapter(this, R.layout.list_stockage, arrayListstockage);

        listView_stockage.setAdapter(stockageadapter);
        listView_stockage.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                stockageadapter.onItemselected(arg2);
            }
        });

    }
    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ListStockage.this, MenuParcelle.class);
            finish();
            startActivity(intent);
        }
    };


    private void Loadliststockage() {
        //Get Suivi Producteur
        SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        List <STOCKAGE> list_stockage = getStockageList(sp);
        if (list_stockage != null) {
            for (STOCKAGE stockage : list_stockage) {
                if (stockage.getArchive() != 0) {
                    arrayListstockage.add(new StockageClass(
                            "Nom_stockeur :" + stockage.getNOM_STOCKEUR(),
                            "Identifiant_stockeur :"+ stockage.getIDENTIFIANT_STOCKEUR(),
                            "capacite  :"+ stockage.getCAPACITE(),
                            stockage.getId()
                           ));

                            }}  }}

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }



    private SUIVI_PRODUCTEUR getSuiviProducteurOfOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        SUIVI_PRODUCTEUR suivi_producteur_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        suivi_producteur_result = e.getSUIVI_PRODUCTEUR();
                    }
                }
            }
        }
        return suivi_producteur_result;
    }


    //Return a list of Stockage
    private List<STOCKAGE> getStockageList(SUIVI_PRODUCTEUR Suivi_producteur){
        return Suivi_producteur.getSTOCKAGEList();
    }

    //Return a stockage
    private STOCKAGE getStockage(List <STOCKAGE> list_stockage ,long ID_Stockage){
        STOCKAGE result = null;
        for (STOCKAGE S : list_stockage){
            if (S.getId() == ID_Stockage){
                result = S;
            }
        }
        return result;
    }
}