package com.srp.agronome.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * Created by doucoure on 20/06/2017.
 */

public class AddContactChoice extends AppCompatActivity {


    private Button AjoutRapide = null;
    private Button AjoutDetaillé = null;
    private Button AjoutEntreprise = null;
    private ImageButton imgBtnHome = null;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact_choice);

        AjoutRapide = (Button) findViewById(R.id.btn_ajoutContactRapide);
        AjoutRapide.setOnClickListener(ButtonAjoutRapideHandler);

        AjoutDetaillé = (Button) findViewById(R.id.btn_ajoutContactDetaillé);
        AjoutDetaillé.setOnClickListener(ButtonAjoutDetailléHandler);

        AjoutEntreprise = (Button) findViewById(R.id.btn_ajoutEntreprise);
        AjoutEntreprise.setOnClickListener(ButtonAjoutEntrepriseHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_menu);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(AddContactChoice.this, ContactMenu.class);
            finish();
            startActivity(intent);

        }
        return true;
    }


    View.OnClickListener ButtonAjoutRapideHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(AddContactChoice.this, CreateQuickContact.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener ButtonAjoutDetailléHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(AddContactChoice.this, CreateContact.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener ButtonAjoutEntrepriseHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(AddContactChoice.this, CreateOrganization.class);
            startActivity(intent);
            finish();
        }
    };


    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(AddContactChoice.this, ContactMenu.class);
            finish();
            startActivity(intent);
        }
    };
}
