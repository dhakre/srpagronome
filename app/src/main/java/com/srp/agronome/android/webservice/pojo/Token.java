package com.srp.agronome.android.webservice.pojo;

/**
 * Created by soytouchrp on 04/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.moshi.Json;


public class Token {

    @Json(name = "status")
    private String status;
    @Json(name = "token")
    private String token;

    public String getStatus() {
        return status;
    }

    public String getToken() {
        return token;
    }

}




