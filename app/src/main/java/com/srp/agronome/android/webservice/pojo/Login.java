package com.srp.agronome.android.webservice.pojo;

/**
 * Created by soytouchrp on 04/12/2017.
 */


public class Login {


    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}




