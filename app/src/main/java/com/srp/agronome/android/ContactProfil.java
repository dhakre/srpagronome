package com.srp.agronome.android;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.srp.agronome.android.db.AdresseDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.ContactDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Identite;
import com.srp.agronome.android.db.IdentiteDao;
import com.srp.agronome.android.db.Type_Bio;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Contact_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.MainLogin.Nom_Entreprise;
import static com.srp.agronome.android.MainLogin.Prenom_Contact;
import static com.srp.agronome.android.MainLogin.Nom_Contact;

/**
 * Created by doucoure on 02/06/2017.
 */

/**
 * Modified by Vincent on 26/07/2017
 */

public class ContactProfil extends AppCompatActivity {

    //Identité
    private ImageView ContactPhoto = null;
    private MyTextView NomPrenom = null;
    private MyTextView Numero_Contact = null;
    private MyTextView Metier_Contact = null;
    private MyTextView EntrepriseName = null;
    //Contact Détaillé

    //Contact
    private MyTextView Sexe = null;
    private MyTextView DateNaissance = null;

    private MyTextView Adresse_Contact_1 = null;
    private MyTextView Adresse_Contact_2 = null;
    private MyTextView CodePostal_Contact = null;
    private MyTextView Ville_Contact = null;
    private MyTextView Region_Contact = null;
    private MyTextView Pays_Contact = null;

    private MyTextView Telephone_Contact_1 = null;
    private MyTextView Telephone_Contact_2 = null;
    private MyTextView Email_Contact = null;

    //Entreprise
    private MyTextView Structure = null;
    private MyTextView SIRET = null;

    private MyTextView Cessation_Activite = null;
    private MyTextView Cause_Cessation_Activite = null;
    private MyTextView Date_Cessation_Activite = null;

    //Adresse Siege social
    private MyTextView Adresse_Siege_1 = null;
    private MyTextView Adresse_Siege_2 = null;
    private MyTextView CodePostal_Siege = null;
    private MyTextView Ville_Siege = null;
    private MyTextView Region_Siege = null;
    private MyTextView Pays_Siege = null;

    //Adresse de facturation
    private MyTextView Adresse_Facturation_1 = null;
    private MyTextView Adresse_Facturation_2 = null;
    private MyTextView CodePostal_Facturation = null;
    private MyTextView Ville_Facturation = null;
    private MyTextView Region_Facturation = null;
    private MyTextView Pays_Facturation= null;

    //Adresse de livraison
    private MyTextView Adresse_Livraison_1 = null;
    private MyTextView Adresse_Livraison_2 = null;
    private MyTextView CodePostal_Livraison = null;
    private MyTextView Ville_Livraison = null;
    private MyTextView Region_Livraison = null;
    private MyTextView Pays_Livraison= null;

    private MyTextView Telephone_Entreprise = null;
    private MyTextView FAX_Entreprise = null;
    private MyTextView Email_Entreprise_1 = null;
    private MyTextView Email_Entreprise_2 = null;

    //Appreciation
    private MyTextView Categorie = null;
    private MyTextView Statut = null;
    private MyTextView Sociabilite = null;
    private MyTextView Remarque = null;

    //Activite
    private MyTextView ActivitePrincipale = null;
    private MyTextView ActiviteSecondaire = null;
    private MyTextView NombreSalarie = null;
    private MyTextView Situation = null;
    private MyTextView Projet = null;
    private MyTextView Biologique = null;
    private MyTextView TypeBiologique = null;
    private MyTextView SAU = null;
    private MyTextView SAU_irrigable = null;
    private MyTextView SAU_Bio = null;
    private MyTextView SAU_Bio_irrigable = null;

    //Connaissance de l'activite
    private MyTextView OS1 = null;
    private MyTextView OS2 = null;
    private MyTextView SourceInternet = null;
    private MyTextView SourceJournaux = null;

    //Bouton afficher/masquer
    private Button Contact_Detaille = null;

    private Button Contact = null;
    private Button AdressePersonnelle = null;
    private Button TelephoneEmailPersonelle = null;

    private Button Entreprise = null;
    private Button AdresseSiege = null;
    private Button AdresseFacturation = null;
    private Button AdresseLivraison = null;
    private Button TelephoneEmailFAX = null;

    private Button Appreciation = null;
    private Button Activite = null;
    private Button ConnaissanceActivite = null;

    private ImageButton Retour;
    private ImageButton Delete;
    private ImageButton Edit;

    private static final int CAMERA_PIC_REQUEST = 2;
    private static final int GALLERY_PICK = 1;
    private static final int PICK_CROP = 3;
    private GoogleApiClient mGoogleApiClient;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_profil);
        FindWidgetById();

        //Bouton Afficher/Masquer set on click listener
        Contact_Detaille.setOnClickListener(Contact_DetailleHandler);
        Contact.setOnClickListener(Contact_Handler);

        AdressePersonnelle.setOnClickListener(AdressePersonelle_Handler);
        AdressePersonnelle.setOnLongClickListener(AdressePersonnelle_Handler_LongClick); //GPS Event

        TelephoneEmailPersonelle.setOnClickListener(TelephoneEmailPersonelle_Handler);
        Entreprise.setOnClickListener(Entreprise_Handler);

        AdresseSiege.setOnClickListener(AdresseSiege_Handler);
        AdresseSiege.setOnLongClickListener(AdresseSiege_Handler_LongClick); //GPS Event

        AdresseFacturation.setOnClickListener(AdresseFacturation_Handler);
        AdresseFacturation.setOnLongClickListener(AdresseFacturation_Handler_LongClick);

        AdresseLivraison.setOnClickListener(AdresseLivraison_Handler);
        AdresseLivraison.setOnLongClickListener(AdresseLivraison_Handler_LongClick); //GPS Event

        TelephoneEmailFAX.setOnClickListener(TelephoneEmailFAX_Handler);
        Appreciation.setOnClickListener(Appreciation_Handler);
        Activite.setOnClickListener(Activite_Handler);
        ConnaissanceActivite.setOnClickListener(ConnaissanceActivite_Handler);

        Telephone_Contact_1.setOnLongClickListener(Telephone_Contact_1_Handler);
        Telephone_Contact_2.setOnLongClickListener(Telephone_Contact_2_Handler);
        Telephone_Entreprise.setOnLongClickListener(Telephone_Entreprise_Handler);

        Retour = (ImageButton) findViewById(R.id.arrow_back_menu);
        Retour.setOnClickListener(ButtonRetourHandler);

        Delete = (ImageButton) findViewById(R.id.btn_delete);
        Delete.setOnClickListener(ButtonDeleteHandler);

        Edit = (ImageButton) findViewById(R.id.btn_edit);
        Edit.setOnClickListener(ButtonEditHandler);

        ContactPhoto.setOnClickListener(PhotoHandler);
        LoadData(Nom_Entreprise,Nom_Contact,Prenom_Contact);
    }

    //set null to space
    public String correctNullValue(String str)
    {
        if(str.equals("null"))
        {
            str=" ";
        }
        return str;
    }


    private void FindWidgetById() {

        EntrepriseName = (MyTextView) findViewById(R.id.Title_Contact_Profil);

        ContactPhoto = (ImageView) findViewById(R.id.ContactPhoto);
        NomPrenom = (MyTextView) findViewById(R.id.NameandSurname);
        Numero_Contact = (MyTextView) findViewById(R.id.numero_id);
        Metier_Contact = (MyTextView) findViewById(R.id.metier_contact);

        Sexe = (MyTextView) findViewById(R.id.sexe_id);
        DateNaissance = (MyTextView) findViewById(R.id.dateNaissance_id);

        Adresse_Contact_1 = (MyTextView) findViewById(R.id.AdressePersonnelle1);
        Adresse_Contact_2 = (MyTextView) findViewById(R.id.AdressePersonnelle2);
        CodePostal_Contact = (MyTextView) findViewById(R.id.CodePostalPersonnel);
        Ville_Contact = (MyTextView) findViewById(R.id.VillePersonnelle);
        Region_Contact = (MyTextView) findViewById(R.id.RegionPersonnelle);
        Pays_Contact = (MyTextView) findViewById(R.id.PaysPersonnel);

        Telephone_Contact_1 = (MyTextView) findViewById(R.id.Telephone_Contact_1);
        Telephone_Contact_2 = (MyTextView) findViewById(R.id.Telephone_Contact_2);
        Email_Contact = (MyTextView) findViewById(R.id.Email_personnel);

        Structure = (MyTextView) findViewById(R.id.Structure);
        SIRET = (MyTextView) findViewById(R.id.SIRET);

        Cessation_Activite = (MyTextView) findViewById(R.id.CessationActivite);
        Cause_Cessation_Activite = (MyTextView) findViewById(R.id.CauseCessation);
        Date_Cessation_Activite = (MyTextView) findViewById(R.id.DateCessation);

        Adresse_Siege_1 = (MyTextView) findViewById(R.id.AdresseSiege1);
        Adresse_Siege_2 = (MyTextView) findViewById(R.id.AdresseSiege2);
        CodePostal_Siege = (MyTextView) findViewById(R.id.CodePostalSiege);
        Ville_Siege = (MyTextView) findViewById(R.id.VilleSiege);
        Region_Siege = (MyTextView) findViewById(R.id.RegionSiege);
        Pays_Siege = (MyTextView) findViewById(R.id.PaysSiege);

        Adresse_Facturation_1 = (MyTextView) findViewById(R.id.AdresseFacturation1);
        Adresse_Facturation_2 = (MyTextView) findViewById(R.id.AdresseFacturation2);
        CodePostal_Facturation = (MyTextView) findViewById(R.id.CodePostalFacturation);
        Ville_Facturation = (MyTextView) findViewById(R.id.VilleFacturation);
        Region_Facturation = (MyTextView) findViewById(R.id.RegionFacturation);
        Pays_Facturation= (MyTextView) findViewById(R.id.PaysFacturation);

        Adresse_Livraison_1 = (MyTextView) findViewById(R.id.AdresseLivraison1);
        Adresse_Livraison_2 = (MyTextView) findViewById(R.id.AdresseLivraison2);
        CodePostal_Livraison = (MyTextView) findViewById(R.id.CodePostalLivraison);
        Ville_Livraison = (MyTextView) findViewById(R.id.VilleLivraison);
        Region_Livraison = (MyTextView) findViewById(R.id.RegionLivraison);
        Pays_Livraison= (MyTextView) findViewById(R.id.PaysLivraison);

        Telephone_Entreprise = (MyTextView) findViewById(R.id.Telephone_Entreprise);
        FAX_Entreprise = (MyTextView) findViewById(R.id.FAX_Entreprise);
        Email_Entreprise_1 = (MyTextView) findViewById(R.id.Email_Entreprise_1);
        Email_Entreprise_2 = (MyTextView) findViewById(R.id.Email_Entreprise_2);

        //Appreciation
        Categorie = (MyTextView) findViewById(R.id.CategorieContact);
        Statut = (MyTextView) findViewById(R.id.StatutSoyTouch);
        Sociabilite = (MyTextView) findViewById(R.id.Sociabilite);
        Remarque = (MyTextView) findViewById(R.id.Remarque);

        //Activite
        ActivitePrincipale = (MyTextView) findViewById(R.id.ActivitePrincipale);
        ActiviteSecondaire = (MyTextView) findViewById(R.id.ActiviteSecondaire);
        NombreSalarie = (MyTextView) findViewById(R.id.NombreSalarie);
        Situation = (MyTextView) findViewById(R.id.Situation);
        Projet = (MyTextView) findViewById(R.id.Projet);
        Biologique = (MyTextView) findViewById(R.id.Biologique);
        TypeBiologique = (MyTextView) findViewById(R.id.TypeBiologique);
        SAU = (MyTextView) findViewById(R.id.SAU);
        SAU_irrigable = (MyTextView) findViewById(R.id.SAUi);
        SAU_Bio = (MyTextView) findViewById(R.id.SAUb);
        SAU_Bio_irrigable = (MyTextView) findViewById(R.id.SAUbi);

        //Connaissance Activite
        OS1 = (MyTextView) findViewById(R.id.OS1);
        OS2 = (MyTextView) findViewById(R.id.OS2);
        SourceInternet = (MyTextView) findViewById(R.id.SourceInternet);
        SourceJournaux = (MyTextView) findViewById(R.id.SourceJournaux);

        //Bouton Afficher/Masquer
        Contact_Detaille = (Button) findViewById(R.id.BoutonContactDetaille);
        Contact = (Button) findViewById(R.id.BoutonContact);
        AdressePersonnelle = (Button) findViewById(R.id.BoutonAdressePersonnelle);
        TelephoneEmailPersonelle = (Button) findViewById(R.id.BoutonTelEmailPersonnelle);
        Entreprise = (Button) findViewById(R.id.BoutonEntreprise);
        AdresseSiege = (Button) findViewById(R.id.BoutonAdresseSiege);
        AdresseFacturation = (Button) findViewById(R.id.BoutonAdresseFacturation);
        AdresseLivraison = (Button) findViewById(R.id.BoutonAdresseLivraison);
        TelephoneEmailFAX = (Button) findViewById(R.id.BoutonTelEmailFax);
        Appreciation = (Button) findViewById(R.id.BoutonAppreciation);
        Activite = (Button) findViewById(R.id.BoutonActivite);
        ConnaissanceActivite = (Button) findViewById(R.id.BoutonConnaissanceActivite);
    }

    View.OnClickListener Contact_DetailleHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Contact.setVisibility((Contact.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Entreprise.setVisibility(( Entreprise.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Sexe.setVisibility(View.GONE);
            DateNaissance.setVisibility(View.GONE);
            AdressePersonnelle.setVisibility(View.GONE);
            TelephoneEmailPersonelle.setVisibility(View.GONE);
            Adresse_Contact_1.setVisibility(View.GONE);
            Adresse_Contact_2.setVisibility(View.GONE);
            CodePostal_Contact.setVisibility(View.GONE);
            Ville_Contact.setVisibility(View.GONE);
            Region_Contact.setVisibility(View.GONE);
            Pays_Contact.setVisibility(View.GONE);
            Telephone_Contact_1.setVisibility(View.GONE);
            Telephone_Contact_2.setVisibility(View.GONE);
            Email_Contact.setVisibility(View.GONE);
            Structure.setVisibility(View.GONE);
            SIRET.setVisibility(View.GONE);
            Cessation_Activite.setVisibility(View.GONE);
            Cause_Cessation_Activite.setVisibility(View.GONE);
            Date_Cessation_Activite.setVisibility(View.GONE);
            AdresseSiege.setVisibility(View.GONE);
            AdresseLivraison.setVisibility(View.GONE);
            AdresseFacturation.setVisibility(View.GONE);
            TelephoneEmailFAX.setVisibility(View.GONE);
            Adresse_Siege_1.setVisibility(View.GONE);
            Adresse_Siege_2.setVisibility(View.GONE);
            CodePostal_Siege.setVisibility(View.GONE);
            Ville_Siege.setVisibility(View.GONE);
            Region_Siege.setVisibility(View.GONE);
            Pays_Siege.setVisibility(View.GONE);
            Adresse_Facturation_1.setVisibility(View.GONE);
            Adresse_Facturation_2.setVisibility(View.GONE);
            CodePostal_Facturation.setVisibility(View.GONE);
            Ville_Facturation.setVisibility(View.GONE);
            Region_Facturation.setVisibility(View.GONE);
            Pays_Facturation.setVisibility(View.GONE);
            Adresse_Livraison_1.setVisibility(View.GONE);
            Adresse_Livraison_2.setVisibility(View.GONE);
            CodePostal_Livraison.setVisibility(View.GONE);
            Ville_Livraison.setVisibility(View.GONE);
            Region_Livraison.setVisibility(View.GONE);
            Pays_Livraison.setVisibility(View.GONE);
            Telephone_Entreprise.setVisibility(View.GONE);
            FAX_Entreprise.setVisibility(View.GONE);
            Email_Entreprise_1.setVisibility(View.GONE);
            Email_Entreprise_2.setVisibility(View.GONE);
        }
    };

    View.OnClickListener Contact_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Sexe.setVisibility((Sexe.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            DateNaissance.setVisibility(( DateNaissance.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            AdressePersonnelle.setVisibility(( AdressePersonnelle.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            TelephoneEmailPersonelle.setVisibility((TelephoneEmailPersonelle.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Adresse_Contact_1.setVisibility(View.GONE);
            Adresse_Contact_2.setVisibility(View.GONE);
            CodePostal_Contact.setVisibility(View.GONE);
            Ville_Contact.setVisibility(View.GONE);
            Region_Contact.setVisibility(View.GONE);
            Pays_Contact.setVisibility(View.GONE);
            Telephone_Contact_1.setVisibility(View.GONE);
            Telephone_Contact_2.setVisibility(View.GONE);
            Email_Contact.setVisibility(View.GONE);

        }
    };

    View.OnClickListener AdressePersonelle_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Adresse_Contact_1.setVisibility((Adresse_Contact_1.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Adresse_Contact_2.setVisibility(( Adresse_Contact_2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            CodePostal_Contact.setVisibility(( CodePostal_Contact.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Ville_Contact.setVisibility(( Ville_Contact.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Region_Contact.setVisibility(( Region_Contact.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Pays_Contact.setVisibility(( Pays_Contact.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };

    View.OnClickListener TelephoneEmailPersonelle_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Telephone_Contact_1.setVisibility((Telephone_Contact_1.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Telephone_Contact_2.setVisibility((Telephone_Contact_2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Email_Contact.setVisibility((Email_Contact.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };

    View.OnClickListener Entreprise_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Structure.setVisibility((Structure.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            SIRET.setVisibility((SIRET.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Cessation_Activite.setVisibility((Cessation_Activite.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Cause_Cessation_Activite.setVisibility((Cause_Cessation_Activite.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Date_Cessation_Activite.setVisibility((Date_Cessation_Activite.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            AdresseSiege.setVisibility((AdresseSiege.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            AdresseLivraison.setVisibility((AdresseLivraison.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            AdresseFacturation.setVisibility((AdresseFacturation.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            TelephoneEmailFAX.setVisibility((TelephoneEmailFAX.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Adresse_Siege_1.setVisibility(View.GONE);
            Adresse_Siege_2.setVisibility(View.GONE);
            CodePostal_Siege.setVisibility(View.GONE);
            Ville_Siege.setVisibility(View.GONE);
            Region_Siege.setVisibility(View.GONE);
            Pays_Siege.setVisibility(View.GONE);
            Adresse_Facturation_1.setVisibility(View.GONE);
            Adresse_Facturation_2.setVisibility(View.GONE);
            CodePostal_Facturation.setVisibility(View.GONE);
            Ville_Facturation.setVisibility(View.GONE);
            Region_Facturation.setVisibility(View.GONE);
            Pays_Facturation.setVisibility(View.GONE);
            Adresse_Livraison_1.setVisibility(View.GONE);
            Adresse_Livraison_2.setVisibility(View.GONE);
            CodePostal_Livraison.setVisibility(View.GONE);
            Ville_Livraison.setVisibility(View.GONE);
            Region_Livraison.setVisibility(View.GONE);
            Pays_Livraison.setVisibility(View.GONE);
            Telephone_Entreprise.setVisibility(View.GONE);
            FAX_Entreprise.setVisibility(View.GONE);
            Email_Entreprise_1.setVisibility(View.GONE);
            Email_Entreprise_2.setVisibility(View.GONE);
        }
    };

    View.OnClickListener AdresseSiege_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Adresse_Siege_1.setVisibility((Adresse_Siege_1.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Adresse_Siege_2.setVisibility((Adresse_Siege_2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            CodePostal_Siege.setVisibility(( CodePostal_Siege.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Ville_Siege.setVisibility(( Ville_Siege.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Region_Siege.setVisibility(( Region_Siege.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Pays_Siege.setVisibility(( Pays_Siege.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };

    View.OnClickListener AdresseFacturation_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Adresse_Facturation_1.setVisibility((Adresse_Facturation_1.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Adresse_Facturation_2.setVisibility((Adresse_Facturation_2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            CodePostal_Facturation.setVisibility(( CodePostal_Facturation.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Ville_Facturation.setVisibility(( Ville_Facturation.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Region_Facturation.setVisibility(( Region_Facturation.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Pays_Facturation.setVisibility(( Pays_Facturation.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };

    View.OnClickListener AdresseLivraison_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Adresse_Livraison_1.setVisibility((Adresse_Livraison_1.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Adresse_Livraison_2.setVisibility((Adresse_Livraison_2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            CodePostal_Livraison.setVisibility(( CodePostal_Livraison.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Ville_Livraison.setVisibility(( Ville_Livraison.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Region_Livraison.setVisibility(( Region_Livraison.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Pays_Livraison.setVisibility(( Pays_Livraison.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };

    View.OnClickListener TelephoneEmailFAX_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Telephone_Entreprise.setVisibility((Telephone_Entreprise.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            FAX_Entreprise.setVisibility((FAX_Entreprise.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Email_Entreprise_1.setVisibility((Email_Entreprise_1.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Email_Entreprise_2.setVisibility((Email_Entreprise_2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };

    View.OnClickListener Appreciation_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            Categorie.setVisibility((Categorie.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Statut.setVisibility(( Statut.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Sociabilite.setVisibility((Sociabilite.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Remarque.setVisibility((Remarque.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };

    View.OnClickListener Activite_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            ActivitePrincipale.setVisibility((ActivitePrincipale.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            ActiviteSecondaire.setVisibility((ActiviteSecondaire.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            NombreSalarie.setVisibility((NombreSalarie.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Situation.setVisibility((Situation.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Projet.setVisibility((Projet.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            Biologique.setVisibility(( Biologique.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            TypeBiologique.setVisibility((TypeBiologique.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            SAU.setVisibility((SAU.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            SAU_irrigable.setVisibility((SAU_irrigable.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            SAU_Bio.setVisibility((SAU_Bio.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            SAU_Bio_irrigable.setVisibility((SAU_Bio_irrigable.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };

    View.OnClickListener ConnaissanceActivite_Handler = new View.OnClickListener() {
        public void onClick(View v) {
            OS1.setVisibility((OS1.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            OS2.setVisibility((OS2.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            SourceInternet.setVisibility(( SourceInternet.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
            SourceJournaux.setVisibility((SourceJournaux.getVisibility() == View.VISIBLE)
                    ? View.GONE : View.VISIBLE);
        }
    };



    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ContactProfil.this, MenuAgriculteur.class);
            finish();
            startActivity(intent);
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(ContactProfil.this, MenuAgriculteur.class);
            finish();
            startActivity(intent);
        }
        return true;
    }


    View.OnLongClickListener AdressePersonnelle_Handler_LongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> list_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (list_compte != null) {
                for (Compte c : list_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise){
                        if (e.getId() == ID_Entreprise_Selected){
                            List <com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                            for (com.srp.agronome.android.db.Contact ct : List_Contact){
                                if ((ct.getId() == ID_Contact_Selected)){
                                    if ((ct.getIdentite().getAdresse() != null)&&(ct.getIdentite().getAdresse().getCode_Postal_Ville() != null)){
                                        String Adresse = ct.getIdentite().getAdresse().getAdresse_numero_rue();
                                        String CP = ct.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal();
                                        String Ville = ct.getIdentite().getAdresse().getCode_Postal_Ville().getVille();
                                        //check for null values
                                        Adresse=correctNullValue(Adresse);
                                        CP=correctNullValue(CP);
                                        Ville=correctNullValue(Ville);
                                        if (TextUtils.isEmpty(Adresse)||(TextUtils.isEmpty(CP))||(TextUtils.isEmpty(Ville))){
                                            Toast.makeText(getApplicationContext(),getString(R.string.AddressNotFound),Toast.LENGTH_SHORT).show();}
                                        else {
                                        Dialog_Adress_GPS(Adresse,CP,Ville);}}
                                        else{
                                            Toast.makeText(getApplicationContext(),getString(R.string.AddressNotFound),Toast.LENGTH_SHORT).show();}
                                }}}}}}
            return true;
        }
    };

    View.OnLongClickListener AdresseSiege_Handler_LongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> list_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (list_compte != null) {
                for (Compte c : list_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise){
                        if (e.getId() == ID_Entreprise_Selected) {
                            if ((e.getAdresse_Siege() != null) && (e.getAdresse_Siege().getCode_Postal_Ville() != null)) {
                                String Adresse = e.getAdresse_Siege().getAdresse_numero_rue();
                                String CP = e.getAdresse_Siege().getCode_Postal_Ville().getCode_postal();
                                String Ville = e.getAdresse_Siege().getCode_Postal_Ville().getVille();
                                //check for null
                                Adresse=correctNullValue(Adresse);
                                CP=correctNullValue(CP);
                                Ville=correctNullValue(Ville);
                                if (TextUtils.isEmpty(Adresse) || (TextUtils.isEmpty(CP)) || (TextUtils.isEmpty(Ville))) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.AddressNotFound), Toast.LENGTH_SHORT).show();
                                } else {
                                    Dialog_Adress_GPS(Adresse, CP, Ville);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.AddressNotFound), Toast.LENGTH_SHORT).show();
                            }
                        }}}}
            return true;
        }
    };

    View.OnLongClickListener AdresseFacturation_Handler_LongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> list_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (list_compte != null) {
                for (Compte c : list_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise){
                        if (e.getId() == ID_Entreprise_Selected) {
                            if ((e.getAdresse_Facturation() != null) && (e.getAdresse_Facturation().getCode_Postal_Ville() != null)) {
                                String Adresse = e.getAdresse_Facturation().getAdresse_numero_rue();
                                String CP = e.getAdresse_Facturation().getCode_Postal_Ville().getCode_postal();
                                String Ville = e.getAdresse_Facturation().getCode_Postal_Ville().getVille();
                                //check for null
                                Adresse=correctNullValue(Adresse);
                                CP=correctNullValue(CP);
                                Ville=correctNullValue(Ville);
                                if (TextUtils.isEmpty(Adresse) || (TextUtils.isEmpty(CP)) || (TextUtils.isEmpty(Ville))) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.AddressNotFound), Toast.LENGTH_SHORT).show();
                                } else {
                                    Dialog_Adress_GPS(Adresse, CP, Ville);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.AddressNotFound), Toast.LENGTH_SHORT).show();
                            }
                        }}}}
            return true;
        }
    };

    View.OnLongClickListener AdresseLivraison_Handler_LongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> list_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (list_compte != null) {
                for (Compte c : list_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise){
                        if (e.getId() == ID_Entreprise_Selected) {
                            if ((e.getAdresse_Livraison() != null) && (e.getAdresse_Livraison().getCode_Postal_Ville() != null)) {
                                String Adresse = e.getAdresse_Livraison().getAdresse_numero_rue();
                                String CP = e.getAdresse_Livraison().getCode_Postal_Ville().getCode_postal();
                                String Ville = e.getAdresse_Livraison().getCode_Postal_Ville().getVille();
                                //check for null
                                Adresse=correctNullValue(Adresse);
                                CP=correctNullValue(CP);
                                Ville=correctNullValue(Ville);
                                if (TextUtils.isEmpty(Adresse) || (TextUtils.isEmpty(CP)) || (TextUtils.isEmpty(Ville))) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.AddressNotFound), Toast.LENGTH_SHORT).show();
                                } else {
                                    Dialog_Adress_GPS(Adresse, CP, Ville);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.AddressNotFound), Toast.LENGTH_SHORT).show();
                            }
                        }}}}
            return true;
        }
    };

    View.OnLongClickListener Telephone_Contact_1_Handler = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> list_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (list_compte != null) {
                for (Compte c : list_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise) {
                        if (e.getId() == ID_Entreprise_Selected) {
                            List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                            for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                if (ct.getId() == ID_Contact_Selected) {
                                    if (ct.getIdentite().getTelephone_principal() != null){
                                        String PhoneNumber = ct.getIdentite().getTelephone_principal();
                                        //
                                        PhoneNumber=correctNullValue(PhoneNumber);
                                        if (TextUtils.isEmpty(PhoneNumber)){
                                            Toast.makeText(getApplicationContext(), getString(R.string.PhoneNumberNotFound), Toast.LENGTH_SHORT).show();}
                                        else{
                                            Dialog_Call_Phone(PhoneNumber);}}
                                    else{
                                        Toast.makeText(getApplicationContext(), getString(R.string.PhoneNumberNotFound), Toast.LENGTH_SHORT).show();
                                        }
                                }}}}}}
            return true;
        }
    };

    View.OnLongClickListener Telephone_Contact_2_Handler = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> list_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (list_compte != null) {
                for (Compte c : list_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise) {
                        if (e.getId() == ID_Entreprise_Selected) {
                            List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                            for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                if (ct.getId() == ID_Contact_Selected) {
                                    if (ct.getIdentite().getTelephone_secondaire() != null){
                                        String PhoneNumber = ct.getIdentite().getTelephone_secondaire();

                                        PhoneNumber=correctNullValue(PhoneNumber);
                                        if (TextUtils.isEmpty(PhoneNumber)){
                                            Toast.makeText(getApplicationContext(), getString(R.string.PhoneNumberNotFound), Toast.LENGTH_SHORT).show();}
                                        else{
                                            Dialog_Call_Phone(PhoneNumber);}}
                                    else{
                                        Toast.makeText(getApplicationContext(), getString(R.string.PhoneNumberNotFound), Toast.LENGTH_SHORT).show();
                                    }
                                }}}}}}
            return true;
        }
    };

    View.OnLongClickListener Telephone_Entreprise_Handler = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
            CompteDao compte_dao = daoSession.getCompteDao();
            List<Compte> list_compte = compte_dao.queryBuilder()
                    .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                    .list();
            if (list_compte != null) {
                for (Compte c : list_compte) {
                    List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                    for (Entreprise e : List_Entreprise){
                        if (e.getId() == ID_Entreprise_Selected) {
                            if (e.getTelephone_entreprise() != null) {
                                String PhoneNumber = e.getTelephone_entreprise();

                                PhoneNumber=correctNullValue(PhoneNumber);
                                if (TextUtils.isEmpty(PhoneNumber)) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.PhoneNumberNotFound), Toast.LENGTH_SHORT).show();
                                } else {
                                    Dialog_Call_Phone(PhoneNumber);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.PhoneNumberNotFound), Toast.LENGTH_SHORT).show();
                            }
                        }}}}
            return true;
        }
    };



    public void Dialog_Adress_GPS(final String Adresse, final String CP ,final  String Ville) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactProfil.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionAdress, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Open GPS et Go to the Adresse
                        GPS_Goto_Address(Adresse,CP,Ville);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }

    //Open GPS and Go to the address
    private void GPS_Goto_Address(String Adresse, String CP, String Ville){
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + Adresse +  " " + CP + " " + Ville );
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }




    public void Dialog_Call_Phone(final String PhoneNumber) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactProfil.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionPhoneNumber, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // Call the phone number
                        Call_Phone_Number(PhoneNumber);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }

    //Call a phoneNumber
    private void Call_Phone_Number(String PhoneNumber){
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + PhoneNumber));
            if (ActivityCompat.checkSelfPermission(ContactProfil.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(intent);
    }

    View.OnClickListener ButtonDeleteHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style
            PopupMenu popup = new PopupMenu(wrapper, Delete); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_delete, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals(getString(R.string.TextDeleteContact))){
                        Show_Dialog__Delete_Contact(getString(R.string.TitleDeleteContact),getString(R.string.TextDeleteContact2));
                    }
                    if (item.getTitle().equals(getString(R.string.TextDeleteOrganization))){
                        Show_Dialog__Delete_Entreprise(getString(R.string.TitleDeleteOrganization),getString(R.string.TextDeleteOrganization2));
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    View.OnClickListener ButtonEditHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style
            PopupMenu popup = new PopupMenu(wrapper, Edit); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_edit, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    //Toast.makeText(getApplicationContext(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                    if (item.getTitle().equals(getString(R.string.TextEditContact))){
                        Intent intent = new Intent(ContactProfil.this, EditContact.class);
                        finish();
                        startActivity(intent);
                    }
                    if (item.getTitle().equals(getString(R.string.TextEditOrganization))){
                        Intent intent = new Intent(ContactProfil.this, EditOrganization.class);
                        finish();
                        startActivity(intent);
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    //Permet de "Supprimer un contact" = Masquer le contact dans la liste des contacts
    private void DeleteContact(long Id_Entreprise , long Id_Contact){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<com.srp.agronome.android.db.Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact){
                            if ((ct.getId() == Id_Contact)) {
                                if (CheckSetArchiveContact(ct.getId(),ct.getIdentite().getId(),ct.getIdentite().getAdresse().getId())){
                                    ArchiveContact(ct);
                                }
                                else{
                                    Show_Dialog(getString(R.string.TitleDeleteFail),getString(R.string.TextDeleteFail));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean CheckRelationsAddress(long ID_Adresse){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        IdentiteDao identite_data = daoSession.getIdentiteDao();
        List <Identite> list_identite = identite_data.queryBuilder()
                .where(IdentiteDao.Properties.ID_Adresse.eq(ID_Adresse))
                .list();
        //Log.i("AdressRelation","Nbr Adresse Relation : " + list_identite.size() );
        return (list_identite.size() < 2);
    }

    private boolean CheckRelationsIdentite(long ID_Identite){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ContactDao contact_data = daoSession.getContactDao();
        List <com.srp.agronome.android.db.Contact> list_contact = contact_data.queryBuilder()
                .where(ContactDao.Properties.ID_Identite_Contact.eq(ID_Identite))
                .list();
        //Log.i("ContactRelation","Nbr Contact Relation : " + list_contact.size() );
        return (list_contact.size() < 2);
    }

    private boolean CheckRelationsContact(long ID_Contact){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        QueryBuilder<Entreprise> queryBuilder = entreprise_data.queryBuilder();
        queryBuilder.join(com.srp.agronome.android.db.Contact.class, ContactDao.Properties.ID_Entreprise)
                .where(ContactDao.Properties.Id.eq(ID_Contact));
        List<Entreprise> list_entreprise = queryBuilder.list();
        //Log.i("EntrepriseRelation","Nbr Entreprise Relation : " + list_entreprise.size() );
        return (list_entreprise.size() < 2 );
    }

    private boolean CheckSetArchiveContact(long ID_Contact, long ID_Identite , long ID_Adresse){
        return ((CheckRelationsIdentite(ID_Identite))&(CheckRelationsAddress(ID_Adresse))&(CheckRelationsContact(ID_Contact)));
    }

    private void ArchiveContact(com.srp.agronome.android.db.Contact contact_archive){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ContactDao contact_data = daoSession.getContactDao();
        IdentiteDao identite_data = daoSession.getIdentiteDao();
        AdresseDao adresse_data = daoSession.getAdresseDao();

        contact_archive.setDate_modification(Today_Date());
        contact_archive.setID_Agronome_Modification((int)ID_Compte_Selected);
        contact_archive.setArchive(0);
        contact_archive.getIdentite().setDate_modification(Today_Date());
        contact_archive.getIdentite().setID_Agronome_Modification((int)ID_Compte_Selected);
        contact_archive.getIdentite().setArchive(0);
        contact_archive.getIdentite().getAdresse().setDate_modification(Today_Date());
        contact_archive.getIdentite().getAdresse().setID_Agronome_Modification((int)ID_Compte_Selected);
        contact_archive.getIdentite().getAdresse().setArchive(0);

        adresse_data.update(contact_archive.getIdentite().getAdresse());
        identite_data.update(contact_archive.getIdentite());
        contact_data.update(contact_archive);
    }


    //Delete Entreprise
    private void DeleteEntreprise(long Id_Entreprise){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();

        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<com.srp.agronome.android.db.Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise) {
                        List <com.srp.agronome.android.db.Contact> list_contact = e.getContactList();
                        boolean archive = true;
                        for (com.srp.agronome.android.db.Contact ct : list_contact){
                            if (!(CheckSetArchiveContact(ct.getId(),ct.getIdentite().getId(),ct.getIdentite().getAdresse().getId()))){
                                archive = false; //Check si plusieurs relation
                            }
                        }
                        if (archive){
                            ArchiveEntreprise(e); //Archive Entreprise + Contact
                        }
                        else{
                            Show_Dialog(getString(R.string.TitleDeleteFail),getString(R.string.TextDeleteFail));
                        }
                    }
                }
            }
        }
    }

    private void ArchiveEntreprise (Entreprise entreprise_archive){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        EntrepriseDao entreprise_data = daoSession.getEntrepriseDao();
        List <com.srp.agronome.android.db.Contact> list_contact = entreprise_archive.getContactList();
        for (com.srp.agronome.android.db.Contact ct : list_contact){
            ArchiveContact(ct);
        }
        entreprise_archive.setDate_modification(Today_Date());
        entreprise_archive.setID_Agronome_Modification((int)ID_Compte_Selected);
        entreprise_archive.setArchive(0);
        entreprise_data.update(entreprise_archive);
    }

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactProfil.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    //Display Dialog Delete_Contact
    private void Show_Dialog__Delete_Contact(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactProfil.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteContact(ID_Entreprise_Selected, ID_Contact_Selected);
                Intent intent = new Intent(ContactProfil.this, ListContact.class);
                finish();
                startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Display Dialog Delete_Entreprise
    private void Show_Dialog__Delete_Entreprise(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactProfil.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteEntreprise(ID_Entreprise_Selected);
                Intent intent = new Intent(ContactProfil.this, ListContact.class);
                finish();
                startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    //Charge les données de la base de données et les places sont dans les editTexts
    private void LoadData(String Entreprise, String Nom , String Prenom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<com.srp.agronome.android.db.Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise){
                    if (e.getId() == ID_Entreprise_Selected){
                        //Charger les donnes de l'entreprise
                        EntrepriseName.setText(e.getRaison_sociale());
                        if (e.getStructure_Sociale() != null){
                            String TextStructure = Structure.getText() + " : " + e.getStructure_Sociale().getNom();
                            TextStructure=correctNullValue(TextStructure);
                            Structure.setText(TextStructure);}
                        if (e.getSIRET() != null){
                            String TextSIRET = SIRET.getText() + " : " + correctNullValue(e.getSIRET());
                            //TextSIRET=correctNullValue(e.getSIRET());

                            SIRET.setText(TextSIRET);}
                        if (e.getArret_activite() != null){
                            String TextArret;
                            if (e.getArret_activite()){
                                 TextArret = Cessation_Activite.getText() + " : " + getString(R.string.TextYes);}
                            else{
                                 TextArret = Cessation_Activite.getText() + " : " + getString(R.string.TextNo);}
                            Cessation_Activite.setText(TextArret);}
                        if (e.getCause_arret_activite() != null){
                            String CauseCessationActivite = Cause_Cessation_Activite.getText() + " : " + correctNullValue(e.getCause_arret_activite());
                            Cause_Cessation_Activite.setText(CauseCessationActivite);}
                        String TextDateCessation = Date_Cessation_Activite.getText() + " : " + DatetoString(e.getDate_arret_activite());
                        Date_Cessation_Activite.setText(TextDateCessation);

                        //Adresses de l'entreprise
                        if (e.getAdresse_Siege().getAdresse_numero_rue() != null){
                            String TextAdresseSiege_1 = Adresse_Siege_1.getText() + " : " + e.getAdresse_Siege().getAdresse_numero_rue();
                            Adresse_Siege_1.setText(TextAdresseSiege_1);}
                        if (e.getAdresse_Siege().getComplement_adresse() != null){
                            String TextAdresseSiege_2 = Adresse_Siege_2.getText() + " : " + e.getAdresse_Siege().getComplement_adresse();
                            Adresse_Siege_2.setText(TextAdresseSiege_2);}
                        if (e.getAdresse_Siege().getCode_Postal_Ville() != null){
                            String TextCodePostalSiege = CodePostal_Siege.getText() + " : " + e.getAdresse_Siege().getCode_Postal_Ville().getCode_postal();
                            CodePostal_Siege.setText(TextCodePostalSiege);
                            String TextVilleSiege = Ville_Siege.getText() + " : " + e.getAdresse_Siege().getCode_Postal_Ville().getVille();
                            Ville_Siege.setText(TextVilleSiege);}
                        if (e.getAdresse_Siege().getRegion() != null){
                            String TextRegionSiege = Region_Siege.getText() + " : " + e.getAdresse_Siege().getRegion().getNom();
                            Region_Siege.setText(TextRegionSiege);}
                        if (e.getAdresse_Siege().getPays() != null){
                            String TextPaysSiege = Pays_Siege.getText() + " : " + e.getAdresse_Siege().getPays().getNom();
                            Pays_Siege.setText(TextPaysSiege);}

                        if (e.getAdresse_Livraison().getAdresse_numero_rue() != null){
                            String TextAdresseLivraison_1 = Adresse_Livraison_1.getText() + " : " + e.getAdresse_Livraison().getAdresse_numero_rue();
                            Adresse_Livraison_1.setText(TextAdresseLivraison_1);}
                        if (e.getAdresse_Livraison().getComplement_adresse() != null){
                            String TextAdresseLivraison_2 = Adresse_Livraison_2.getText() + " : " + e.getAdresse_Livraison().getComplement_adresse();
                            Adresse_Livraison_2.setText(TextAdresseLivraison_2);}
                        if (e.getAdresse_Livraison().getCode_Postal_Ville() != null){
                            String TextCodePostalLivraison = CodePostal_Livraison.getText() + " : " + e.getAdresse_Livraison().getCode_Postal_Ville().getCode_postal();
                            CodePostal_Livraison.setText(TextCodePostalLivraison);
                            String TextVilleSiege = Ville_Livraison.getText() + " : " + e.getAdresse_Livraison().getCode_Postal_Ville().getVille();
                            Ville_Livraison.setText(TextVilleSiege);}
                        if (e.getAdresse_Siege().getRegion() != null){
                            String TextRegionSiege = Region_Livraison.getText() + " : " + e.getAdresse_Livraison().getRegion().getNom();
                            Region_Livraison.setText(TextRegionSiege);}
                        if (e.getAdresse_Livraison().getPays() != null){
                            String TextPaysLivraison = Pays_Livraison.getText() + " : " + e.getAdresse_Livraison().getPays().getNom();
                            Pays_Livraison.setText(TextPaysLivraison);}

                        if (e.getAdresse_Facturation().getAdresse_numero_rue() != null){
                            String TextAdresseFacturation_1 = Adresse_Facturation_1.getText() + " : " + e.getAdresse_Facturation().getAdresse_numero_rue();
                            Adresse_Facturation_1.setText(TextAdresseFacturation_1);}
                        if (e.getAdresse_Facturation().getComplement_adresse() != null){
                            String TextAdresseFacturation_2 = Adresse_Facturation_2.getText() + " : " + e.getAdresse_Facturation().getComplement_adresse();
                            Adresse_Facturation_2.setText(TextAdresseFacturation_2);}
                        if (e.getAdresse_Facturation().getCode_Postal_Ville() != null){
                            String TextCodePostalFacturation = CodePostal_Facturation.getText() + " : " + e.getAdresse_Facturation().getCode_Postal_Ville().getCode_postal();
                            CodePostal_Facturation.setText(TextCodePostalFacturation);
                            String TextVilleFacturation = Ville_Facturation.getText() + " : " + e.getAdresse_Facturation().getCode_Postal_Ville().getVille();
                            Ville_Facturation.setText(TextVilleFacturation);}
                        if (e.getAdresse_Facturation().getRegion() != null){
                            String TextRegionFacturation = Region_Facturation.getText() + " : " + e.getAdresse_Facturation().getRegion().getNom();
                            Region_Facturation.setText(TextRegionFacturation);}
                        if (e.getAdresse_Facturation().getPays() != null){
                            String TextPaysFacturation = Pays_Facturation.getText() + " : " + e.getAdresse_Facturation().getPays().getNom();
                            Pays_Facturation.setText(TextPaysFacturation);}

                        //Telephone Email FAX
                        if (e.getTelephone_entreprise() != null){
                            String TextTelephoneEntreprise = Telephone_Entreprise.getText() + " : " + e.getTelephone_entreprise();
                            Telephone_Entreprise.setText(TextTelephoneEntreprise);}
                        if (e.getFax_entreprise() != null){
                            String TextFAXEntreprise = FAX_Entreprise.getText() + " : " + e.getFax_entreprise();
                            FAX_Entreprise.setText(TextFAXEntreprise);}
                        if (e.getAdresse_email_principale() != null){
                            String TextEmailEntreprise_1 = Email_Entreprise_1.getText() + " : " + e.getAdresse_email_principale();
                            Email_Entreprise_1.setText(TextEmailEntreprise_1);}
                        if (e.getAdresse_email_secondaire() != null){
                            String TextEmailEntreprise_2 = Email_Entreprise_2.getText() + " : " + e.getAdresse_email_secondaire();
                            Email_Entreprise_2.setText(TextEmailEntreprise_2);}

                        if (e.getStatut_Soytouch() != null){
                            String TextStatut = Statut.getText() + " : " + e.getStatut_Soytouch().getNom();
                            Statut.setText(TextStatut);}
                        if (e.getActivite_Principale() != null){
                            String TextActivitePrincipale = ActivitePrincipale.getText() + " : " + e.getActivite_Principale().getNom();
                            ActivitePrincipale.setText(TextActivitePrincipale);}
                        if (e.getActivite_Secondaire() != null){
                            String TextActiviteSecondaire = ActiviteSecondaire.getText() + " : " + e.getActivite_Secondaire().getNom();
                            ActiviteSecondaire.setText(TextActiviteSecondaire);}
                        if (e.getNombre_salarie() != null){
                            String TextNombreSalarie = NombreSalarie.getText() + " : " + e.getNombre_salarie();
                            NombreSalarie.setText(TextNombreSalarie);}
                        if (e.getSituation() != null){
                            String TextSituation = Situation.getText() + " : " + e.getSituation().getNom();
                            Situation.setText(TextSituation);}
                        if (e.getProjet_connu() != null){
                            String TextProjet = Projet.getText() + " : " +correctNullValue( e.getProjet_connu());
                            Projet.setText(TextProjet);}
                        if (e.getBiologique() != null){
                            String TextBiologique = Biologique.getText() + " : " + e.getBiologique().getNom();
                            Biologique.setText(TextBiologique);}
                        if (e.getType_BioList() != null) {
                            String TextListTypeBio = TypeBiologique.getText() + " : ";
                            List <Type_Bio> ListTypeBio = e.getType_BioList();
                            for (Type_Bio TB : ListTypeBio){  //0 à N Type Biologique
                                TextListTypeBio += TB.getNom() + " ";}
                            TypeBiologique.setText(TextListTypeBio);}

                        //Donnes Suivi Producteur à ajouter
                        if (e.getSUIVI_PRODUCTEUR() != null) {
                            if (e.getSUIVI_PRODUCTEUR().getSAU() != null) {
                                String TextSAU = SAU.getText() + " : " + e.getSUIVI_PRODUCTEUR().getSAU();
                                SAU.setText(TextSAU);
                            }
                            if (e.getSUIVI_PRODUCTEUR().getSAU_IRRIGABLE() != null) {
                                String TextSAU_irrigable = SAU_irrigable.getText() + " : " + e.getSUIVI_PRODUCTEUR().getSAU_IRRIGABLE();
                                SAU_irrigable.setText(TextSAU_irrigable);
                            }
                            if (e.getSUIVI_PRODUCTEUR().getSAU_BIO() != null) {
                                String TextSAU_bio = SAU_Bio.getText() + " : " + e.getSUIVI_PRODUCTEUR().getSAU_BIO();
                                SAU_Bio.setText(TextSAU_bio);
                            }
                            if (e.getSUIVI_PRODUCTEUR().getSAU_BIO_IRRIGABLE() != null) {
                                String TextSAU_bio_irrigable = SAU_Bio_irrigable.getText() + " : " + e.getSUIVI_PRODUCTEUR().getSAU_BIO_IRRIGABLE();
                                SAU_Bio_irrigable.setText(TextSAU_bio_irrigable);
                            }

                            //Connaissance de l'activite
                            if (e.getSUIVI_PRODUCTEUR().getOS_PRINCIPAL() != null) {
                                String TextOS1 = OS1.getText() + " : " + e.getSUIVI_PRODUCTEUR().getOS_PRINCIPAL().getNom();
                                OS1.setText(TextOS1);
                            }
                            if (e.getSUIVI_PRODUCTEUR().getOS_SECONDAIRE() != null) {
                                String TextOS2 = OS2.getText() + " : " + e.getSUIVI_PRODUCTEUR().getOS_SECONDAIRE().getNom();
                                OS2.setText(TextOS2);
                            }
                            if (e.getSUIVI_PRODUCTEUR().getSOURCE_INTERNET() != null) {
                                String TextSourceInternet = SourceInternet.getText() + " : " + e.getSUIVI_PRODUCTEUR().getSOURCE_INTERNET().getNom();
                                SourceInternet.setText(TextSourceInternet);
                            }
                            if (e.getSUIVI_PRODUCTEUR().getSOURCE_JOURNAUX() != null) {
                                String TextSourceJournaux = SourceJournaux.getText() + " : " + e.getSUIVI_PRODUCTEUR().getSOURCE_JOURNAUX().getNom();
                                SourceJournaux.setText(TextSourceJournaux);
                            }
                        }
                        List <com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact){
                            if (ct.getId() == ID_Contact_Selected){
                                //Charger les donnes du contact
                                if (ct.getIdentite().getPhoto().equals("")){
                                    ContactPhoto.setImageResource(R.mipmap.ic_add_photo);}
                                else{
                                    loadImageFromStorage(ct.getIdentite().getPhoto());}
                                NomPrenom.setText(ct.getIdentite().getNom() + " " + ct.getIdentite().getPrenom());
                                String TextNumero = "N° : " + ct.getId();
                                Numero_Contact.setText(TextNumero);
                                if (ct.getMetier() != null) {
                                    Metier_Contact.setText(ct.getMetier().getNom());
                                }

                                if (ct.getIdentite().getSexe() != null){
                                    String TextSexe = Sexe.getText().toString() + " : "  + ct.getIdentite().getSexe().getNom();
                                    Sexe.setText(TextSexe);}
                                String TextDateNaissance = DateNaissance.getText().toString() + " : "  + DatetoString(ct.getIdentite().getDate_naissance());
                                DateNaissance.setText(TextDateNaissance);
                                String TextAdresse_Contact_1 = Adresse_Contact_1.getText() + " : " + ct.getIdentite().getAdresse().getAdresse_numero_rue();
                                Adresse_Contact_1.setText(TextAdresse_Contact_1);
                                String TextAdresse_Contact_2 = Adresse_Contact_2.getText() + " : " + ct.getIdentite().getAdresse().getComplement_adresse();
                                Adresse_Contact_2.setText(TextAdresse_Contact_2);
                                if (ct.getIdentite().getAdresse().getCode_Postal_Ville() != null){
                                    String TextCodePostal_Contact = CodePostal_Contact.getText()+ " : " + ct.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal();
                                    CodePostal_Contact.setText(TextCodePostal_Contact);
                                String TextVille_Contact = Ville_Contact.getText()+ " : " + ct.getIdentite().getAdresse().getCode_Postal_Ville().getVille();
                                Ville_Contact.setText(TextVille_Contact);}
                                if (ct.getIdentite().getAdresse().getRegion() != null){
                                    String TextRegion_Contact = Region_Contact.getText() + " : " + ct.getIdentite().getAdresse().getRegion().getNom();
                                    Region_Contact.setText(TextRegion_Contact);}
                                if (ct.getIdentite().getAdresse().getPays() != null){
                                    String TextPays_Contact = Pays_Contact.getText() + " : " + ct.getIdentite().getAdresse().getPays().getNom();
                                    Pays_Contact.setText(TextPays_Contact);}
                                String TextTelephone_Contact_1 = Telephone_Contact_1.getText() + " : " + ct.getIdentite().getTelephone_principal();
                                Telephone_Contact_1.setText(TextTelephone_Contact_1 );
                                if (ct.getIdentite().getTelephone_secondaire() != null){
                                    String TextTelephone_Contact_2 = Telephone_Contact_2.getText() + " : " + ct.getIdentite().getTelephone_secondaire();
                                    Telephone_Contact_2.setText(TextTelephone_Contact_2);}
                                if(ct.getIdentite().getAdresse_email() != null){
                                    String TextEmail_Contact = Email_Contact.getText() + " : " + ct.getIdentite().getAdresse_email();
                                    Email_Contact.setText(TextEmail_Contact);}

                                if (ct.getCategorie_Contact() != null){
                                    String TextCategorie = Categorie.getText() + " : " + ct.getCategorie_Contact().getNom();
                                    Categorie.setText(TextCategorie);}
                                if (ct.getSociabilite() != null){
                                    String TextSociabilite = Sociabilite.getText() + " : " + ct.getSociabilite().getNom();
                                    Sociabilite.setText(TextSociabilite);}
                                if (ct.getRemarque_contact() != null){
                                    String TextRemarque = Remarque.getText() + " : " + ct.getRemarque_contact();
                                    Remarque.setText(TextRemarque);}

                            }
                        }
                    }
                }
            }}}


    // Method pour convertir une date en chaine de caractère
    private String DatetoString(Date date_a_convertir){
        if (date_a_convertir == null){
            return "";
        }
        else{
        Calendar cal = Calendar.getInstance();
        cal.setTime(date_a_convertir);
        if (cal.get(Calendar.MONTH) <= 8) {
            return (cal.get(Calendar.DAY_OF_MONTH) + "/0" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR));
        } else {
            return(cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR));
        }
    }
    }

    //Méthode pour charger une image depuis le téléphone dans l'application.
    private void loadImageFromStorage(String FileName) {
        try {
            File f = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise+ "/Contacts/" + FileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            ContactPhoto.setImageBitmap(b);
            Log.i("Load Bitmap","L'image définie.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("Load Bitmap","L'image est introuvable");
        }
    }

    //Event Clic on Photo :
    View.OnClickListener PhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Selection_Photo_Choice();
        }
    };
    //Afficher une boîte de dialogue contenant une liste de choix pour affecter une photo
    public void Selection_Photo_Choice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactProfil.this, R.style.AlertDialogCustom);
        builder.setItems(R.array.listSelectionPhoto, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //Prendre une photo
                        TakePicturefromCamera();
                        break;
                    case 1: //Choisir une photo depuis la galerie
                        pickImagefromGallery();
                        break;
                    case 2: //Rogner la photo affichée
                        //Verifie si il y a une photo
                        if (ContactPhoto.getDrawable().getConstantState() != ContactProfil.this.getResources().getDrawable(R.mipmap.ic_add_photo).getConstantState()) {
                            CropImage();
                        }
                        break;
                    case 3: //Supprimer la photo -> mettre l'image de base à la place
                        ContactPhoto.setImageResource(R.mipmap.ic_add_photo);
                        SavePhotoDataBase(ID_Entreprise_Selected , ID_Contact_Selected);
                }
            }
        });
        AlertDialog alert = builder.create();
        builder.create();
        alert.show();
    }


    //Pick a photo from Gallery
    public void pickImagefromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICK);
    }

    //Take a photo from Camera
    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(getPhotoContactNameHQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact));
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise + "/Contacts");
        File file = new File(myDir, FileName);
        return file;
    }

    //Get the path of the directory : Intern or SD path
    private File getDirectoryPath(){
        SharedPreferences SP = getSharedPreferences("PrefValues", 0); // 0 = Private Mode
        String location = SP.getString("LocationStorage",null);
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (location.equals("Intern")){
            return Dirs[0];}
        else{
            return Dirs[1];}
    }

    //Permet de retourner les 3 premiers caracteres d'un texte
    private String getSubString(String chaine){
        String result="";
        if (chaine.length() >= 3){ result = chaine.substring(0,3);}
        if (chaine.length() == 2){ result = chaine.substring(0,2);}
        if (chaine.length() == 1){ result = chaine.substring(0,1);}
        return result ;
    }

    private String getPhotoContactNameHQ(String compte,String entreprise,String nom, String prenom){
        return getSubString(compte) + "_" + entreprise + "_C_" + getSubString(nom) + "_" + getSubString(prenom) + "_HQ.jpg";
    }

    private String getPhotoContactNameLQ(String compte,String entreprise,String nom, String prenom){
        return getSubString(compte) + "_" + entreprise + "_C_" + getSubString(nom) + "_" + getSubString(prenom) + "_LQ.jpg";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == GALLERY_PICK) { //Depuis la galerie
            Uri uri = data.getData();
            Bitmap Picture_Load;
            try {
                Picture_Load = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                Picture_Load = rotateImageIfRequired(Picture_Load,uri,true); //Rotation si necessaire
                SaveImage(Picture_Load,getPhotoContactNameHQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact));
                Picture_Load = getResizedBitmap(Picture_Load, 256, 256);
                ContactPhoto.setImageBitmap(Picture_Load);
                SaveImage(Picture_Load, getPhotoContactNameLQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_PIC_REQUEST) { //Depuis la caméra : pas de bitmap en sortie
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise+ "/Contacts");
            File file = new File(myDir,getPhotoContactNameHQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact));
            Uri uri = Uri.fromFile(file);
            Bitmap newPicture;
            try {
                newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
                newPicture = rotateImageIfRequired(newPicture,uri,false); //Rotation si necessaire
                SaveImage(newPicture, getPhotoContactNameHQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact));
                newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
                ContactPhoto.setImageBitmap(newPicture);
                SaveImage(newPicture,getPhotoContactNameLQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_CROP) {
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise+ "/Contacts");
            File file = new File(myDir, getPhotoContactNameLQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact));
            Uri URI = Uri.fromFile(file);
            Bitmap Picture_Crop;
            try {
                Picture_Crop = MediaStore.Images.Media.getBitmap(getContentResolver(), URI);
                ContactPhoto.setImageBitmap(Picture_Crop);
                SaveImage(Picture_Crop,getPhotoContactNameLQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact)); //Sauvegarde l'image dans le téléphone
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        SavePhotoDataBase(ID_Entreprise_Selected , ID_Contact_Selected);
    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Permet de rogner ou recadrer une image
    private void CropImage() {
        try {
            //Acceder à la photo à rogner
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise+ "/Contacts");
            File photoFile = new File(myDir, getPhotoContactNameLQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact));
            Uri photoURI = FileProvider.getUriForFile(this,
                    "com.srp.android.fileprovider",
                    photoFile);
            getApplicationContext().grantUriPermission("com.android.camera", photoURI,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //Paramètres de l'Intent Crop
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //Android N need set permission to uri otherwise system camera don't has permission to access file wait crop
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // indicate image type and Uri
            cropIntent.setDataAndType(photoURI, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            cropIntent.putExtra("scale", true);
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PICK_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
        }
    }

    public static boolean checkImageResource(Context ctx, ImageView imageView, int imageResource) {
        boolean result = false;
        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }
            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage, boolean PickingGallery) throws IOException {

        InputStream input = getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
        if (PickingGallery){
            ei = new ExifInterface(getPath(selectedImage));}
        else{
            ei =  new ExifInterface(selectedImage.getPath());}

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    //Get Path = Pick up a picture in Gallery = getRealPath
    private String getPath(Uri uri) {
        String[]  data = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    //Rotate an image with angle in degree
    private Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        Log.i("Info Rotation","Rotation de l image de : " + degree);
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }

    private void SavePhotoDataBase(long Id_Entreprise , long Id_Contact){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise){
                    if (e.getId() == Id_Entreprise){
                        List <com.srp.agronome.android.db.Contact> List_Contact = e.getContactList();
                        for (com.srp.agronome.android.db.Contact ct : List_Contact){
                            Log.i("Info DB", "Parcours des contacts");
                            if ((ct.getId() == Id_Contact)){
                                Log.i("Info DB", "Contact trouvé !");
                                if (checkImageResource(this,ContactPhoto,R.mipmap.ic_add_photo)){
                                    ct.getIdentite().setPhoto("");
                                } else {
                                    ct.getIdentite().setPhoto(getPhotoContactNameLQ(Nom_Compte,Nom_Entreprise,Nom_Contact,Prenom_Contact));
                                }
                                ct.getIdentite().update();
                            }
                        }
                    }
                }
            }}}

    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/Organizations/" + Nom_Entreprise+ "/Contacts");
        File file = new File(myDir, FileName);
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
