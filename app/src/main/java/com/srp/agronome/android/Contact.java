package com.srp.agronome.android;

import java.io.Serializable;

/**
 * Created by doucoure on 21/06/2017.
 */

public class Contact implements Serializable {
    String image;
    String nom, prenom, code_postal, statut_soytouch, ville, numeroContact, adresse;
    String tel;
    long ID_Entreprise , ID_Contact;

    public Contact(String image, String nom, String prenom, String code_postal, String statut_soytouch, String ville,String adresse, String numeroContact, String tel, Long ID_Entreprise, Long ID_Contact) {
        super();
        this.image = image;
        this.nom = nom;
        this.prenom = prenom;
        this.code_postal = code_postal;
        this.statut_soytouch = statut_soytouch;
        this.ville = ville;
        this.adresse = adresse;
        this.numeroContact = numeroContact;
        this.tel = tel;
        this.ID_Entreprise = ID_Entreprise;
        this.ID_Contact = ID_Contact;
    }

    public long getID_Entreprise() {return ID_Entreprise;}

    public void setID_Entreprise(long ID_Entreprise) {this.ID_Entreprise = ID_Entreprise;}

    public long getID_Contact() {return ID_Contact;}

    public void setID_Contact(long ID_Contact) {this.ID_Contact = ID_Contact;}

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getNumeroContact() {
        return numeroContact;
    }

    public void setNumeroContact(String numeroContact) {
        this.numeroContact = numeroContact;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal = code_postal;
    }

    public String getStatut_soytouch() {
        return statut_soytouch;
    }

    public void setStatut_soytouch(String statut_soytouch) {
        this.statut_soytouch = statut_soytouch;
    }
}