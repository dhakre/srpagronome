package com.srp.agronome.android.db;

import org.greenrobot.greendao.annotation.*;

import java.util.List;
import com.srp.agronome.android.db.DaoSession;
import org.greenrobot.greendao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "COMPTE".
 */
@Entity(active = true)
public class Compte {

    @Id
    private Long id;

    @NotNull
    private String login;

    @NotNull
    private String password;
    private int archive;
    private Long ID_Type_Compte;
    private Long ID_Collaborateur;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient CompteDao myDao;

    @ToOne(joinProperty = "ID_Type_Compte")
    private Type_Compte type_Compte;

    @Generated
    private transient Long type_Compte__resolvedKey;

    @ToOne(joinProperty = "ID_Collaborateur")
    private Collaborateur collaborateur;

    @Generated
    private transient Long collaborateur__resolvedKey;

    @ToMany(joinProperties = {
        @JoinProperty(name = "id", referencedName = "ID_Compte")
    })
    private List<Photo> photoList;

    @ToMany(joinProperties = {
        @JoinProperty(name = "id", referencedName = "ID_Compte")
    })
    private List<Video> videoList;

    @ToMany(joinProperties = {
        @JoinProperty(name = "id", referencedName = "ID_Compte")
    })
    private List<Entreprise> entrepriseList;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public Compte() {
    }

    public Compte(Long id) {
        this.id = id;
    }

    @Generated
    public Compte(Long id, String login, String password, int archive, Long ID_Type_Compte, Long ID_Collaborateur) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.archive = archive;
        this.ID_Type_Compte = ID_Type_Compte;
        this.ID_Collaborateur = ID_Collaborateur;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCompteDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @NotNull
    public String getPassword() {
        return password;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setPassword(@NotNull String password) {
        this.password = password;
    }

    public int getArchive() {
        return archive;
    }

    public void setArchive(int archive) {
        this.archive = archive;
    }

    public Long getID_Type_Compte() {
        return ID_Type_Compte;
    }

    public void setID_Type_Compte(Long ID_Type_Compte) {
        this.ID_Type_Compte = ID_Type_Compte;
    }

    public Long getID_Collaborateur() {
        return ID_Collaborateur;
    }

    public void setID_Collaborateur(Long ID_Collaborateur) {
        this.ID_Collaborateur = ID_Collaborateur;
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public Type_Compte getType_Compte() {
        Long __key = this.ID_Type_Compte;
        if (type_Compte__resolvedKey == null || !type_Compte__resolvedKey.equals(__key)) {
            __throwIfDetached();
            Type_CompteDao targetDao = daoSession.getType_CompteDao();
            Type_Compte type_CompteNew = targetDao.load(__key);
            synchronized (this) {
                type_Compte = type_CompteNew;
            	type_Compte__resolvedKey = __key;
            }
        }
        return type_Compte;
    }

    @Generated
    public void setType_Compte(Type_Compte type_Compte) {
        synchronized (this) {
            this.type_Compte = type_Compte;
            ID_Type_Compte = type_Compte == null ? null : type_Compte.getId();
            type_Compte__resolvedKey = ID_Type_Compte;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public Collaborateur getCollaborateur() {
        Long __key = this.ID_Collaborateur;
        if (collaborateur__resolvedKey == null || !collaborateur__resolvedKey.equals(__key)) {
            __throwIfDetached();
            CollaborateurDao targetDao = daoSession.getCollaborateurDao();
            Collaborateur collaborateurNew = targetDao.load(__key);
            synchronized (this) {
                collaborateur = collaborateurNew;
            	collaborateur__resolvedKey = __key;
            }
        }
        return collaborateur;
    }

    @Generated
    public void setCollaborateur(Collaborateur collaborateur) {
        synchronized (this) {
            this.collaborateur = collaborateur;
            ID_Collaborateur = collaborateur == null ? null : collaborateur.getId();
            collaborateur__resolvedKey = ID_Collaborateur;
        }
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    @Generated
    public List<Photo> getPhotoList() {
        if (photoList == null) {
            __throwIfDetached();
            PhotoDao targetDao = daoSession.getPhotoDao();
            List<Photo> photoListNew = targetDao._queryCompte_PhotoList(id);
            synchronized (this) {
                if(photoList == null) {
                    photoList = photoListNew;
                }
            }
        }
        return photoList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated
    public synchronized void resetPhotoList() {
        photoList = null;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    @Generated
    public List<Video> getVideoList() {
        if (videoList == null) {
            __throwIfDetached();
            VideoDao targetDao = daoSession.getVideoDao();
            List<Video> videoListNew = targetDao._queryCompte_VideoList(id);
            synchronized (this) {
                if(videoList == null) {
                    videoList = videoListNew;
                }
            }
        }
        return videoList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated
    public synchronized void resetVideoList() {
        videoList = null;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    @Generated
    public List<Entreprise> getEntrepriseList() {
        if (entrepriseList == null) {
            __throwIfDetached();
            EntrepriseDao targetDao = daoSession.getEntrepriseDao();
            List<Entreprise> entrepriseListNew = targetDao._queryCompte_EntrepriseList(id);
            synchronized (this) {
                if(entrepriseList == null) {
                    entrepriseList = entrepriseListNew;
                }
            }
        }
        return entrepriseList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated
    public synchronized void resetEntrepriseList() {
        entrepriseList = null;
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
