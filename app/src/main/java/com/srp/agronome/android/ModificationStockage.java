package com.srp.agronome.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.STOCKAGE;
import com.srp.agronome.android.db.STOCKAGEDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuParcelle.Id_stockage_selected;

import static com.srp.agronome.android.CustomStockageadapter.Nom_stockeur;
import static com.srp.agronome.android.CustomStockageadapter.capacite;
import static com.srp.agronome.android.CustomStockageadapter.Identifiant_stockeur;
/**
 * Created by jitendra on 01-09-2017.
 */

public class ModificationStockage extends AppCompatActivity {
    public static int  archived = 1;
    private TextView Text_NOM_STOCKEUR = null;
    private TextView Text_IDENTIFIANT_STOCKEUR = null;
    private TextView Text_CAPACITE = null;

    private ImageButton Edit =null;
    private ImageButton Delete =null;
    private ImageButton imgBtnHome =null;
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modification_stockage);
        FindWidgetViewbyId();
        Edit = (ImageButton) findViewById(R.id.btn_edit);
        Edit.setOnClickListener(ButtonEditHandler);

        Delete = (ImageButton) findViewById(R.id.btn_delete);
        Delete.setOnClickListener(ButtonDeleteHandler);
        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_Modificationstockage);
        imgBtnHome.setOnClickListener(ButtonbackHandler);

         Text_NOM_STOCKEUR.setText(Nom_stockeur);
         Text_IDENTIFIANT_STOCKEUR.setText(Identifiant_stockeur);
         Text_CAPACITE.setText(capacite);


    }

    private void FindWidgetViewbyId() {
        Text_NOM_STOCKEUR = (TextView) findViewById(R.id.Textview_stockeur_nom);
        Text_IDENTIFIANT_STOCKEUR = (TextView) findViewById(R.id.Textview_identifiant_stockeur);
        Text_CAPACITE= (TextView) findViewById(R.id.Textview_capacite);
    }
    View.OnClickListener ButtonEditHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new android.view.ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Assign style
            android.widget.PopupMenu popup = new android.widget.PopupMenu(wrapper, Edit); //Creatting popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_edit_stockage, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().equals(getString(R.string.TextEditStockage))){
                        Intent intent = new Intent(ModificationStockage.this, EditStockage.class);
                        finish();
                        startActivity(intent);
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    View.OnClickListener ButtonDeleteHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Creating the instance of PopupMenu
            Context wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.MyPopupMenu); //Affecter le style

            PopupMenu popup = new PopupMenu(wrapper, Delete); //Creation du popup
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.menu_delete_stockage, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {

                    if (item.getTitle().equals(getString(R.string.TextDeleteStockage))){
                        Show_Dialog__Delete_stockage(getString(R.string.TitleDeleteStockage),getString(R.string.TextDeletestockage));
                    }
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    };

    //Display Dialog Delete_stockage
    private void Show_Dialog__Delete_stockage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                StockageDeletestrategy();
                Intent intent = new Intent(ModificationStockage.this, ListStockage.class);
                finish();
                startActivity(intent);
                dialog.cancel();

            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void StockageDeletestrategy(){
        SUIVI_PRODUCTEUR sp = getSuiviProducteurOfOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        List <STOCKAGE> list_stockage = getStockageList(sp);
        STOCKAGE Stockage_selected = getStockage(list_stockage,Id_stockage_selected);
        DeleteStockage(Stockage_selected);
    }
    private void DeleteStockage(STOCKAGE stockage_delete){
        if (CheckRelationsStockage(stockage_delete.getId())){
            if (stockage_delete.getArchive() == 2){
                Show_Dialog_Delete_Stockage(getString(R.string.TitleConfirmDelete) , getString(R.string.TextConfirmDelete), stockage_delete);
            }

        else{
            ArchiveStockage(stockage_delete);
        }  }
        else{
            Show_Dialog(getString(R.string.TitleDeleteFail),getString(R.string.TextDeleteFail));
        }
    }

    private boolean CheckRelationsStockage(long ID_Stockage){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
        QueryBuilder<SUIVI_PRODUCTEUR> queryBuilder = suivi_producteur_data.queryBuilder();
        queryBuilder.join(STOCKAGE.class, STOCKAGEDao.Properties.ID_SUIVI_PRODUCTEUR)
                .where(STOCKAGEDao.Properties.Id.eq(ID_Stockage));
        List<SUIVI_PRODUCTEUR> list_SP = queryBuilder.list();
        return (list_SP.size() < 2 );
    }

    //Display Dialog Delete_stockage
    private void Show_Dialog_Delete_Stockage(String title, String message , final STOCKAGE stockage_delete) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArchiveStockage(stockage_delete);
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }
    private void ArchiveStockage(STOCKAGE stockage_delete){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        stockage_delete.setArchive(archived);
        stockage_delete.setID_Agronome_Modification((int)ID_Compte_Selected);
        stockage_delete.setDate_modification(Today_Date());
        stockage_data.update(stockage_delete);
    }
    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }


    private SUIVI_PRODUCTEUR getSuiviProducteurOfOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        SUIVI_PRODUCTEUR suivi_producteur_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        suivi_producteur_result = e.getSUIVI_PRODUCTEUR();
                    }
                }
            }
        }
        return suivi_producteur_result;
    }

    private void addStockageToSuiviProducteur(SUIVI_PRODUCTEUR suivi_producteur_input , STOCKAGE stockage_input){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        STOCKAGEDao stockage_data = daoSession.getSTOCKAGEDao();
        SUIVI_PRODUCTEURDao suivi_producteur_data = daoSession.getSUIVI_PRODUCTEURDao();
        stockage_input.setID_SUIVI_PRODUCTEUR(suivi_producteur_input.getId());
        stockage_data.update(stockage_input);
        suivi_producteur_input.resetSTOCKAGEList();
        suivi_producteur_data.update(suivi_producteur_input);
    }
    //Return a list of Stockage
    private List<STOCKAGE> getStockageList(SUIVI_PRODUCTEUR Suivi_producteur){
        return Suivi_producteur.getSTOCKAGEList();
    }

    //Return a stockage
    private STOCKAGE getStockage(List <STOCKAGE> list_stockage ,long ID_Stockage){
        STOCKAGE result = null;
        for (STOCKAGE S : list_stockage){
            if (S.getId() == ID_Stockage){
                result = S;
            }
        }
        return result;
    }


    //Bouton back Toolbar
    View.OnClickListener ButtonbackHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };
    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ModificationStockage.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(ModificationStockage.this, ListStockage.class);
                finish();
                startActivity(intent);
                //   Delete_cache();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                // clearData();
                Intent intent = new Intent(ModificationStockage.this, ListStockage.class);
                finish();
                startActivity(intent);
                //  Delete_Cache();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }







}
