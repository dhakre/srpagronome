package com.srp.agronome.android.db;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.Type_Compte;
import com.srp.agronome.android.db.Photo;
import com.srp.agronome.android.db.Theme_Photo;
import com.srp.agronome.android.db.Video;
import com.srp.agronome.android.db.Collaborateur;
import com.srp.agronome.android.db.Identite;
import com.srp.agronome.android.db.Sexe;
import com.srp.agronome.android.db.Adresse;
import com.srp.agronome.android.db.Code_Postal_Ville;
import com.srp.agronome.android.db.Region;
import com.srp.agronome.android.db.Pays;
import com.srp.agronome.android.db.Geolocalisation;
import com.srp.agronome.android.db.Zone;
import com.srp.agronome.android.db.Groupement;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Structure_Sociale;
import com.srp.agronome.android.db.Situation;
import com.srp.agronome.android.db.Statut_Soytouch;
import com.srp.agronome.android.db.Activite;
import com.srp.agronome.android.db.Biologique;
import com.srp.agronome.android.db.Type_Bio;
import com.srp.agronome.android.db.Contact;
import com.srp.agronome.android.db.Categorie_Contact;
import com.srp.agronome.android.db.Metier;
import com.srp.agronome.android.db.Sociabilite;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.OS;
import com.srp.agronome.android.db.SOURCE_INTERNET;
import com.srp.agronome.android.db.SOURCE_JOURNAUX;
import com.srp.agronome.android.db.ILOT;
import com.srp.agronome.android.db.PARCELLE;
import com.srp.agronome.android.db.Photo_Parcelle;
import com.srp.agronome.android.db.STATUT_PARCELLE;
import com.srp.agronome.android.db.TYPE_CULTURE;
import com.srp.agronome.android.db.IRRIGATION;
import com.srp.agronome.android.db.TYPE_IRRIGATION;
import com.srp.agronome.android.db.STOCKAGE;
import com.srp.agronome.android.db.Photo_Stockage;
import com.srp.agronome.android.db.TYPE_STOCKEUR;
import com.srp.agronome.android.db.ETAT_STOCKEUR;
import com.srp.agronome.android.db.STATUT_STOCKEUR;
import com.srp.agronome.android.db.AUDIT_STOCKAGE;
import com.srp.agronome.android.db.PROPRETE;
import com.srp.agronome.android.db.CONTAMINATION_CROISE;
import com.srp.agronome.android.db.VENTILATION;
import com.srp.agronome.android.db.TEMPERATURE;
import com.srp.agronome.android.db.TRIAGE_GRAIN;
import com.srp.agronome.android.db.NUISIBLE;
import com.srp.agronome.android.db.ETAT_NUISIBLE;
import com.srp.agronome.android.db.LISTE_NUISIBLE;
import com.srp.agronome.android.db.AUDIT;
import com.srp.agronome.android.db.OBJET_AUDIT;
import com.srp.agronome.android.db.RECOLTE;
import com.srp.agronome.android.db.AUDIT_DOCUMENT;
import com.srp.agronome.android.db.DOCUMENT;
import com.srp.agronome.android.db.SCAN_DOCUMENT;
import com.srp.agronome.android.db.TYPE_DOCUMENT;
import com.srp.agronome.android.db.DOCUMENT_FOURNI;
import com.srp.agronome.android.db.AUDIT_PARCELLE;
import com.srp.agronome.android.db.ETAT_VEGETATIF;
import com.srp.agronome.android.db.PLANTE_NON_DESIRABLE;
import com.srp.agronome.android.db.ETAT_PLANTE;
import com.srp.agronome.android.db.LISTE_PLANTE;
import com.srp.agronome.android.db.CONTRE_VISITE;
import com.srp.agronome.android.db.ETAT_CONTRE_VISITE;
import com.srp.agronome.android.db.CAUSE;
import com.srp.agronome.android.db.MESSAGE;
import com.srp.agronome.android.db.SELECTION_DESTINATAIRE_DEFAUT;
import com.srp.agronome.android.db.OBJET_MESSAGE;
import com.srp.agronome.android.db.TEXTE_MESSAGE;
import com.srp.agronome.android.db.DESTINATAIRE_DEFAUT;
import com.srp.agronome.android.db.Visite;
import com.srp.agronome.android.db.Attitude_Entreprise_Visite;
import com.srp.agronome.android.db.Interet_Visite;
import com.srp.agronome.android.db.Sujet_Visite_Principal;
import com.srp.agronome.android.db.Sujet_Visite_Secondaire;
import com.srp.agronome.android.db.Objet_Visite;
import com.srp.agronome.android.db.Incident;
import com.srp.agronome.android.db.Etat_Incident;
import com.srp.agronome.android.db.Type_Incident;
import com.srp.agronome.android.db.Incident_Administratif;
import com.srp.agronome.android.db.Incident_Contrat;
import com.srp.agronome.android.db.Sujet_Message;
import com.srp.agronome.android.db.Suivi_Incident;
import com.srp.agronome.android.db.Expediteur;
import com.srp.agronome.android.db.Type_Expediteur;
import com.srp.agronome.android.db.Liste_Expediteur_Contact;
import com.srp.agronome.android.db.Liste_Expediteur_Agronome;
import com.srp.agronome.android.db.Liste_Expediteur_Administration;

import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.Type_CompteDao;
import com.srp.agronome.android.db.PhotoDao;
import com.srp.agronome.android.db.Theme_PhotoDao;
import com.srp.agronome.android.db.VideoDao;
import com.srp.agronome.android.db.CollaborateurDao;
import com.srp.agronome.android.db.IdentiteDao;
import com.srp.agronome.android.db.SexeDao;
import com.srp.agronome.android.db.AdresseDao;
import com.srp.agronome.android.db.Code_Postal_VilleDao;
import com.srp.agronome.android.db.RegionDao;
import com.srp.agronome.android.db.PaysDao;
import com.srp.agronome.android.db.GeolocalisationDao;
import com.srp.agronome.android.db.ZoneDao;
import com.srp.agronome.android.db.GroupementDao;
import com.srp.agronome.android.db.EntrepriseDao;
import com.srp.agronome.android.db.Structure_SocialeDao;
import com.srp.agronome.android.db.SituationDao;
import com.srp.agronome.android.db.Statut_SoytouchDao;
import com.srp.agronome.android.db.ActiviteDao;
import com.srp.agronome.android.db.BiologiqueDao;
import com.srp.agronome.android.db.Type_BioDao;
import com.srp.agronome.android.db.ContactDao;
import com.srp.agronome.android.db.Categorie_ContactDao;
import com.srp.agronome.android.db.MetierDao;
import com.srp.agronome.android.db.SociabiliteDao;
import com.srp.agronome.android.db.SUIVI_PRODUCTEURDao;
import com.srp.agronome.android.db.OSDao;
import com.srp.agronome.android.db.SOURCE_INTERNETDao;
import com.srp.agronome.android.db.SOURCE_JOURNAUXDao;
import com.srp.agronome.android.db.ILOTDao;
import com.srp.agronome.android.db.PARCELLEDao;
import com.srp.agronome.android.db.Photo_ParcelleDao;
import com.srp.agronome.android.db.STATUT_PARCELLEDao;
import com.srp.agronome.android.db.TYPE_CULTUREDao;
import com.srp.agronome.android.db.IRRIGATIONDao;
import com.srp.agronome.android.db.TYPE_IRRIGATIONDao;
import com.srp.agronome.android.db.STOCKAGEDao;
import com.srp.agronome.android.db.Photo_StockageDao;
import com.srp.agronome.android.db.TYPE_STOCKEURDao;
import com.srp.agronome.android.db.ETAT_STOCKEURDao;
import com.srp.agronome.android.db.STATUT_STOCKEURDao;
import com.srp.agronome.android.db.AUDIT_STOCKAGEDao;
import com.srp.agronome.android.db.PROPRETEDao;
import com.srp.agronome.android.db.CONTAMINATION_CROISEDao;
import com.srp.agronome.android.db.VENTILATIONDao;
import com.srp.agronome.android.db.TEMPERATUREDao;
import com.srp.agronome.android.db.TRIAGE_GRAINDao;
import com.srp.agronome.android.db.NUISIBLEDao;
import com.srp.agronome.android.db.ETAT_NUISIBLEDao;
import com.srp.agronome.android.db.LISTE_NUISIBLEDao;
import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.OBJET_AUDITDao;
import com.srp.agronome.android.db.RECOLTEDao;
import com.srp.agronome.android.db.AUDIT_DOCUMENTDao;
import com.srp.agronome.android.db.DOCUMENTDao;
import com.srp.agronome.android.db.SCAN_DOCUMENTDao;
import com.srp.agronome.android.db.TYPE_DOCUMENTDao;
import com.srp.agronome.android.db.DOCUMENT_FOURNIDao;
import com.srp.agronome.android.db.AUDIT_PARCELLEDao;
import com.srp.agronome.android.db.ETAT_VEGETATIFDao;
import com.srp.agronome.android.db.PLANTE_NON_DESIRABLEDao;
import com.srp.agronome.android.db.ETAT_PLANTEDao;
import com.srp.agronome.android.db.LISTE_PLANTEDao;
import com.srp.agronome.android.db.CONTRE_VISITEDao;
import com.srp.agronome.android.db.ETAT_CONTRE_VISITEDao;
import com.srp.agronome.android.db.CAUSEDao;
import com.srp.agronome.android.db.MESSAGEDao;
import com.srp.agronome.android.db.SELECTION_DESTINATAIRE_DEFAUTDao;
import com.srp.agronome.android.db.OBJET_MESSAGEDao;
import com.srp.agronome.android.db.TEXTE_MESSAGEDao;
import com.srp.agronome.android.db.DESTINATAIRE_DEFAUTDao;
import com.srp.agronome.android.db.VisiteDao;
import com.srp.agronome.android.db.Attitude_Entreprise_VisiteDao;
import com.srp.agronome.android.db.Interet_VisiteDao;
import com.srp.agronome.android.db.Sujet_Visite_PrincipalDao;
import com.srp.agronome.android.db.Sujet_Visite_SecondaireDao;
import com.srp.agronome.android.db.Objet_VisiteDao;
import com.srp.agronome.android.db.IncidentDao;
import com.srp.agronome.android.db.Etat_IncidentDao;
import com.srp.agronome.android.db.Type_IncidentDao;
import com.srp.agronome.android.db.Incident_AdministratifDao;
import com.srp.agronome.android.db.Incident_ContratDao;
import com.srp.agronome.android.db.Sujet_MessageDao;
import com.srp.agronome.android.db.Suivi_IncidentDao;
import com.srp.agronome.android.db.ExpediteurDao;
import com.srp.agronome.android.db.Type_ExpediteurDao;
import com.srp.agronome.android.db.Liste_Expediteur_ContactDao;
import com.srp.agronome.android.db.Liste_Expediteur_AgronomeDao;
import com.srp.agronome.android.db.Liste_Expediteur_AdministrationDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig compteDaoConfig;
    private final DaoConfig type_CompteDaoConfig;
    private final DaoConfig photoDaoConfig;
    private final DaoConfig theme_PhotoDaoConfig;
    private final DaoConfig videoDaoConfig;
    private final DaoConfig collaborateurDaoConfig;
    private final DaoConfig identiteDaoConfig;
    private final DaoConfig sexeDaoConfig;
    private final DaoConfig adresseDaoConfig;
    private final DaoConfig code_Postal_VilleDaoConfig;
    private final DaoConfig regionDaoConfig;
    private final DaoConfig paysDaoConfig;
    private final DaoConfig geolocalisationDaoConfig;
    private final DaoConfig zoneDaoConfig;
    private final DaoConfig groupementDaoConfig;
    private final DaoConfig entrepriseDaoConfig;
    private final DaoConfig structure_SocialeDaoConfig;
    private final DaoConfig situationDaoConfig;
    private final DaoConfig statut_SoytouchDaoConfig;
    private final DaoConfig activiteDaoConfig;
    private final DaoConfig biologiqueDaoConfig;
    private final DaoConfig type_BioDaoConfig;
    private final DaoConfig contactDaoConfig;
    private final DaoConfig categorie_ContactDaoConfig;
    private final DaoConfig metierDaoConfig;
    private final DaoConfig sociabiliteDaoConfig;
    private final DaoConfig sUIVI_PRODUCTEURDaoConfig;
    private final DaoConfig oSDaoConfig;
    private final DaoConfig sOURCE_INTERNETDaoConfig;
    private final DaoConfig sOURCE_JOURNAUXDaoConfig;
    private final DaoConfig iLOTDaoConfig;
    private final DaoConfig pARCELLEDaoConfig;
    private final DaoConfig photo_ParcelleDaoConfig;
    private final DaoConfig sTATUT_PARCELLEDaoConfig;
    private final DaoConfig tYPE_CULTUREDaoConfig;
    private final DaoConfig iRRIGATIONDaoConfig;
    private final DaoConfig tYPE_IRRIGATIONDaoConfig;
    private final DaoConfig sTOCKAGEDaoConfig;
    private final DaoConfig photo_StockageDaoConfig;
    private final DaoConfig tYPE_STOCKEURDaoConfig;
    private final DaoConfig eTAT_STOCKEURDaoConfig;
    private final DaoConfig sTATUT_STOCKEURDaoConfig;
    private final DaoConfig aUDIT_STOCKAGEDaoConfig;
    private final DaoConfig pROPRETEDaoConfig;
    private final DaoConfig cONTAMINATION_CROISEDaoConfig;
    private final DaoConfig vENTILATIONDaoConfig;
    private final DaoConfig tEMPERATUREDaoConfig;
    private final DaoConfig tRIAGE_GRAINDaoConfig;
    private final DaoConfig nUISIBLEDaoConfig;
    private final DaoConfig eTAT_NUISIBLEDaoConfig;
    private final DaoConfig lISTE_NUISIBLEDaoConfig;
    private final DaoConfig aUDITDaoConfig;
    private final DaoConfig oBJET_AUDITDaoConfig;
    private final DaoConfig rECOLTEDaoConfig;
    private final DaoConfig aUDIT_DOCUMENTDaoConfig;
    private final DaoConfig dOCUMENTDaoConfig;
    private final DaoConfig sCAN_DOCUMENTDaoConfig;
    private final DaoConfig tYPE_DOCUMENTDaoConfig;
    private final DaoConfig dOCUMENT_FOURNIDaoConfig;
    private final DaoConfig aUDIT_PARCELLEDaoConfig;
    private final DaoConfig eTAT_VEGETATIFDaoConfig;
    private final DaoConfig pLANTE_NON_DESIRABLEDaoConfig;
    private final DaoConfig eTAT_PLANTEDaoConfig;
    private final DaoConfig lISTE_PLANTEDaoConfig;
    private final DaoConfig cONTRE_VISITEDaoConfig;
    private final DaoConfig eTAT_CONTRE_VISITEDaoConfig;
    private final DaoConfig cAUSEDaoConfig;
    private final DaoConfig mESSAGEDaoConfig;
    private final DaoConfig sELECTION_DESTINATAIRE_DEFAUTDaoConfig;
    private final DaoConfig oBJET_MESSAGEDaoConfig;
    private final DaoConfig tEXTE_MESSAGEDaoConfig;
    private final DaoConfig dESTINATAIRE_DEFAUTDaoConfig;
    private final DaoConfig visiteDaoConfig;
    private final DaoConfig attitude_Entreprise_VisiteDaoConfig;
    private final DaoConfig interet_VisiteDaoConfig;
    private final DaoConfig sujet_Visite_PrincipalDaoConfig;
    private final DaoConfig sujet_Visite_SecondaireDaoConfig;
    private final DaoConfig objet_VisiteDaoConfig;
    private final DaoConfig incidentDaoConfig;
    private final DaoConfig etat_IncidentDaoConfig;
    private final DaoConfig type_IncidentDaoConfig;
    private final DaoConfig incident_AdministratifDaoConfig;
    private final DaoConfig incident_ContratDaoConfig;
    private final DaoConfig sujet_MessageDaoConfig;
    private final DaoConfig suivi_IncidentDaoConfig;
    private final DaoConfig expediteurDaoConfig;
    private final DaoConfig type_ExpediteurDaoConfig;
    private final DaoConfig liste_Expediteur_ContactDaoConfig;
    private final DaoConfig liste_Expediteur_AgronomeDaoConfig;
    private final DaoConfig liste_Expediteur_AdministrationDaoConfig;

    private final CompteDao compteDao;
    private final Type_CompteDao type_CompteDao;
    private final PhotoDao photoDao;
    private final Theme_PhotoDao theme_PhotoDao;
    private final VideoDao videoDao;
    private final CollaborateurDao collaborateurDao;
    private final IdentiteDao identiteDao;
    private final SexeDao sexeDao;
    private final AdresseDao adresseDao;
    private final Code_Postal_VilleDao code_Postal_VilleDao;
    private final RegionDao regionDao;
    private final PaysDao paysDao;
    private final GeolocalisationDao geolocalisationDao;
    private final ZoneDao zoneDao;
    private final GroupementDao groupementDao;
    private final EntrepriseDao entrepriseDao;
    private final Structure_SocialeDao structure_SocialeDao;
    private final SituationDao situationDao;
    private final Statut_SoytouchDao statut_SoytouchDao;
    private final ActiviteDao activiteDao;
    private final BiologiqueDao biologiqueDao;
    private final Type_BioDao type_BioDao;
    private final ContactDao contactDao;
    private final Categorie_ContactDao categorie_ContactDao;
    private final MetierDao metierDao;
    private final SociabiliteDao sociabiliteDao;
    private final SUIVI_PRODUCTEURDao sUIVI_PRODUCTEURDao;
    private final OSDao oSDao;
    private final SOURCE_INTERNETDao sOURCE_INTERNETDao;
    private final SOURCE_JOURNAUXDao sOURCE_JOURNAUXDao;
    private final ILOTDao iLOTDao;
    private final PARCELLEDao pARCELLEDao;
    private final Photo_ParcelleDao photo_ParcelleDao;
    private final STATUT_PARCELLEDao sTATUT_PARCELLEDao;
    private final TYPE_CULTUREDao tYPE_CULTUREDao;
    private final IRRIGATIONDao iRRIGATIONDao;
    private final TYPE_IRRIGATIONDao tYPE_IRRIGATIONDao;
    private final STOCKAGEDao sTOCKAGEDao;
    private final Photo_StockageDao photo_StockageDao;
    private final TYPE_STOCKEURDao tYPE_STOCKEURDao;
    private final ETAT_STOCKEURDao eTAT_STOCKEURDao;
    private final STATUT_STOCKEURDao sTATUT_STOCKEURDao;
    private final AUDIT_STOCKAGEDao aUDIT_STOCKAGEDao;
    private final PROPRETEDao pROPRETEDao;
    private final CONTAMINATION_CROISEDao cONTAMINATION_CROISEDao;
    private final VENTILATIONDao vENTILATIONDao;
    private final TEMPERATUREDao tEMPERATUREDao;
    private final TRIAGE_GRAINDao tRIAGE_GRAINDao;
    private final NUISIBLEDao nUISIBLEDao;
    private final ETAT_NUISIBLEDao eTAT_NUISIBLEDao;
    private final LISTE_NUISIBLEDao lISTE_NUISIBLEDao;
    private final AUDITDao aUDITDao;
    private final OBJET_AUDITDao oBJET_AUDITDao;
    private final RECOLTEDao rECOLTEDao;
    private final AUDIT_DOCUMENTDao aUDIT_DOCUMENTDao;
    private final DOCUMENTDao dOCUMENTDao;
    private final SCAN_DOCUMENTDao sCAN_DOCUMENTDao;
    private final TYPE_DOCUMENTDao tYPE_DOCUMENTDao;
    private final DOCUMENT_FOURNIDao dOCUMENT_FOURNIDao;
    private final AUDIT_PARCELLEDao aUDIT_PARCELLEDao;
    private final ETAT_VEGETATIFDao eTAT_VEGETATIFDao;
    private final PLANTE_NON_DESIRABLEDao pLANTE_NON_DESIRABLEDao;
    private final ETAT_PLANTEDao eTAT_PLANTEDao;
    private final LISTE_PLANTEDao lISTE_PLANTEDao;
    private final CONTRE_VISITEDao cONTRE_VISITEDao;
    private final ETAT_CONTRE_VISITEDao eTAT_CONTRE_VISITEDao;
    private final CAUSEDao cAUSEDao;
    private final MESSAGEDao mESSAGEDao;
    private final SELECTION_DESTINATAIRE_DEFAUTDao sELECTION_DESTINATAIRE_DEFAUTDao;
    private final OBJET_MESSAGEDao oBJET_MESSAGEDao;
    private final TEXTE_MESSAGEDao tEXTE_MESSAGEDao;
    private final DESTINATAIRE_DEFAUTDao dESTINATAIRE_DEFAUTDao;
    private final VisiteDao visiteDao;
    private final Attitude_Entreprise_VisiteDao attitude_Entreprise_VisiteDao;
    private final Interet_VisiteDao interet_VisiteDao;
    private final Sujet_Visite_PrincipalDao sujet_Visite_PrincipalDao;
    private final Sujet_Visite_SecondaireDao sujet_Visite_SecondaireDao;
    private final Objet_VisiteDao objet_VisiteDao;
    private final IncidentDao incidentDao;
    private final Etat_IncidentDao etat_IncidentDao;
    private final Type_IncidentDao type_IncidentDao;
    private final Incident_AdministratifDao incident_AdministratifDao;
    private final Incident_ContratDao incident_ContratDao;
    private final Sujet_MessageDao sujet_MessageDao;
    private final Suivi_IncidentDao suivi_IncidentDao;
    private final ExpediteurDao expediteurDao;
    private final Type_ExpediteurDao type_ExpediteurDao;
    private final Liste_Expediteur_ContactDao liste_Expediteur_ContactDao;
    private final Liste_Expediteur_AgronomeDao liste_Expediteur_AgronomeDao;
    private final Liste_Expediteur_AdministrationDao liste_Expediteur_AdministrationDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        compteDaoConfig = daoConfigMap.get(CompteDao.class).clone();
        compteDaoConfig.initIdentityScope(type);

        type_CompteDaoConfig = daoConfigMap.get(Type_CompteDao.class).clone();
        type_CompteDaoConfig.initIdentityScope(type);

        photoDaoConfig = daoConfigMap.get(PhotoDao.class).clone();
        photoDaoConfig.initIdentityScope(type);

        theme_PhotoDaoConfig = daoConfigMap.get(Theme_PhotoDao.class).clone();
        theme_PhotoDaoConfig.initIdentityScope(type);

        videoDaoConfig = daoConfigMap.get(VideoDao.class).clone();
        videoDaoConfig.initIdentityScope(type);

        collaborateurDaoConfig = daoConfigMap.get(CollaborateurDao.class).clone();
        collaborateurDaoConfig.initIdentityScope(type);

        identiteDaoConfig = daoConfigMap.get(IdentiteDao.class).clone();
        identiteDaoConfig.initIdentityScope(type);

        sexeDaoConfig = daoConfigMap.get(SexeDao.class).clone();
        sexeDaoConfig.initIdentityScope(type);

        adresseDaoConfig = daoConfigMap.get(AdresseDao.class).clone();
        adresseDaoConfig.initIdentityScope(type);

        code_Postal_VilleDaoConfig = daoConfigMap.get(Code_Postal_VilleDao.class).clone();
        code_Postal_VilleDaoConfig.initIdentityScope(type);

        regionDaoConfig = daoConfigMap.get(RegionDao.class).clone();
        regionDaoConfig.initIdentityScope(type);

        paysDaoConfig = daoConfigMap.get(PaysDao.class).clone();
        paysDaoConfig.initIdentityScope(type);

        geolocalisationDaoConfig = daoConfigMap.get(GeolocalisationDao.class).clone();
        geolocalisationDaoConfig.initIdentityScope(type);

        zoneDaoConfig = daoConfigMap.get(ZoneDao.class).clone();
        zoneDaoConfig.initIdentityScope(type);

        groupementDaoConfig = daoConfigMap.get(GroupementDao.class).clone();
        groupementDaoConfig.initIdentityScope(type);

        entrepriseDaoConfig = daoConfigMap.get(EntrepriseDao.class).clone();
        entrepriseDaoConfig.initIdentityScope(type);

        structure_SocialeDaoConfig = daoConfigMap.get(Structure_SocialeDao.class).clone();
        structure_SocialeDaoConfig.initIdentityScope(type);

        situationDaoConfig = daoConfigMap.get(SituationDao.class).clone();
        situationDaoConfig.initIdentityScope(type);

        statut_SoytouchDaoConfig = daoConfigMap.get(Statut_SoytouchDao.class).clone();
        statut_SoytouchDaoConfig.initIdentityScope(type);

        activiteDaoConfig = daoConfigMap.get(ActiviteDao.class).clone();
        activiteDaoConfig.initIdentityScope(type);

        biologiqueDaoConfig = daoConfigMap.get(BiologiqueDao.class).clone();
        biologiqueDaoConfig.initIdentityScope(type);

        type_BioDaoConfig = daoConfigMap.get(Type_BioDao.class).clone();
        type_BioDaoConfig.initIdentityScope(type);

        contactDaoConfig = daoConfigMap.get(ContactDao.class).clone();
        contactDaoConfig.initIdentityScope(type);

        categorie_ContactDaoConfig = daoConfigMap.get(Categorie_ContactDao.class).clone();
        categorie_ContactDaoConfig.initIdentityScope(type);

        metierDaoConfig = daoConfigMap.get(MetierDao.class).clone();
        metierDaoConfig.initIdentityScope(type);

        sociabiliteDaoConfig = daoConfigMap.get(SociabiliteDao.class).clone();
        sociabiliteDaoConfig.initIdentityScope(type);

        sUIVI_PRODUCTEURDaoConfig = daoConfigMap.get(SUIVI_PRODUCTEURDao.class).clone();
        sUIVI_PRODUCTEURDaoConfig.initIdentityScope(type);

        oSDaoConfig = daoConfigMap.get(OSDao.class).clone();
        oSDaoConfig.initIdentityScope(type);

        sOURCE_INTERNETDaoConfig = daoConfigMap.get(SOURCE_INTERNETDao.class).clone();
        sOURCE_INTERNETDaoConfig.initIdentityScope(type);

        sOURCE_JOURNAUXDaoConfig = daoConfigMap.get(SOURCE_JOURNAUXDao.class).clone();
        sOURCE_JOURNAUXDaoConfig.initIdentityScope(type);

        iLOTDaoConfig = daoConfigMap.get(ILOTDao.class).clone();
        iLOTDaoConfig.initIdentityScope(type);

        pARCELLEDaoConfig = daoConfigMap.get(PARCELLEDao.class).clone();
        pARCELLEDaoConfig.initIdentityScope(type);

        photo_ParcelleDaoConfig = daoConfigMap.get(Photo_ParcelleDao.class).clone();
        photo_ParcelleDaoConfig.initIdentityScope(type);

        sTATUT_PARCELLEDaoConfig = daoConfigMap.get(STATUT_PARCELLEDao.class).clone();
        sTATUT_PARCELLEDaoConfig.initIdentityScope(type);

        tYPE_CULTUREDaoConfig = daoConfigMap.get(TYPE_CULTUREDao.class).clone();
        tYPE_CULTUREDaoConfig.initIdentityScope(type);

        iRRIGATIONDaoConfig = daoConfigMap.get(IRRIGATIONDao.class).clone();
        iRRIGATIONDaoConfig.initIdentityScope(type);

        tYPE_IRRIGATIONDaoConfig = daoConfigMap.get(TYPE_IRRIGATIONDao.class).clone();
        tYPE_IRRIGATIONDaoConfig.initIdentityScope(type);

        sTOCKAGEDaoConfig = daoConfigMap.get(STOCKAGEDao.class).clone();
        sTOCKAGEDaoConfig.initIdentityScope(type);

        photo_StockageDaoConfig = daoConfigMap.get(Photo_StockageDao.class).clone();
        photo_StockageDaoConfig.initIdentityScope(type);

        tYPE_STOCKEURDaoConfig = daoConfigMap.get(TYPE_STOCKEURDao.class).clone();
        tYPE_STOCKEURDaoConfig.initIdentityScope(type);

        eTAT_STOCKEURDaoConfig = daoConfigMap.get(ETAT_STOCKEURDao.class).clone();
        eTAT_STOCKEURDaoConfig.initIdentityScope(type);

        sTATUT_STOCKEURDaoConfig = daoConfigMap.get(STATUT_STOCKEURDao.class).clone();
        sTATUT_STOCKEURDaoConfig.initIdentityScope(type);

        aUDIT_STOCKAGEDaoConfig = daoConfigMap.get(AUDIT_STOCKAGEDao.class).clone();
        aUDIT_STOCKAGEDaoConfig.initIdentityScope(type);

        pROPRETEDaoConfig = daoConfigMap.get(PROPRETEDao.class).clone();
        pROPRETEDaoConfig.initIdentityScope(type);

        cONTAMINATION_CROISEDaoConfig = daoConfigMap.get(CONTAMINATION_CROISEDao.class).clone();
        cONTAMINATION_CROISEDaoConfig.initIdentityScope(type);

        vENTILATIONDaoConfig = daoConfigMap.get(VENTILATIONDao.class).clone();
        vENTILATIONDaoConfig.initIdentityScope(type);

        tEMPERATUREDaoConfig = daoConfigMap.get(TEMPERATUREDao.class).clone();
        tEMPERATUREDaoConfig.initIdentityScope(type);

        tRIAGE_GRAINDaoConfig = daoConfigMap.get(TRIAGE_GRAINDao.class).clone();
        tRIAGE_GRAINDaoConfig.initIdentityScope(type);

        nUISIBLEDaoConfig = daoConfigMap.get(NUISIBLEDao.class).clone();
        nUISIBLEDaoConfig.initIdentityScope(type);

        eTAT_NUISIBLEDaoConfig = daoConfigMap.get(ETAT_NUISIBLEDao.class).clone();
        eTAT_NUISIBLEDaoConfig.initIdentityScope(type);

        lISTE_NUISIBLEDaoConfig = daoConfigMap.get(LISTE_NUISIBLEDao.class).clone();
        lISTE_NUISIBLEDaoConfig.initIdentityScope(type);

        aUDITDaoConfig = daoConfigMap.get(AUDITDao.class).clone();
        aUDITDaoConfig.initIdentityScope(type);

        oBJET_AUDITDaoConfig = daoConfigMap.get(OBJET_AUDITDao.class).clone();
        oBJET_AUDITDaoConfig.initIdentityScope(type);

        rECOLTEDaoConfig = daoConfigMap.get(RECOLTEDao.class).clone();
        rECOLTEDaoConfig.initIdentityScope(type);

        aUDIT_DOCUMENTDaoConfig = daoConfigMap.get(AUDIT_DOCUMENTDao.class).clone();
        aUDIT_DOCUMENTDaoConfig.initIdentityScope(type);

        dOCUMENTDaoConfig = daoConfigMap.get(DOCUMENTDao.class).clone();
        dOCUMENTDaoConfig.initIdentityScope(type);

        sCAN_DOCUMENTDaoConfig = daoConfigMap.get(SCAN_DOCUMENTDao.class).clone();
        sCAN_DOCUMENTDaoConfig.initIdentityScope(type);

        tYPE_DOCUMENTDaoConfig = daoConfigMap.get(TYPE_DOCUMENTDao.class).clone();
        tYPE_DOCUMENTDaoConfig.initIdentityScope(type);

        dOCUMENT_FOURNIDaoConfig = daoConfigMap.get(DOCUMENT_FOURNIDao.class).clone();
        dOCUMENT_FOURNIDaoConfig.initIdentityScope(type);

        aUDIT_PARCELLEDaoConfig = daoConfigMap.get(AUDIT_PARCELLEDao.class).clone();
        aUDIT_PARCELLEDaoConfig.initIdentityScope(type);

        eTAT_VEGETATIFDaoConfig = daoConfigMap.get(ETAT_VEGETATIFDao.class).clone();
        eTAT_VEGETATIFDaoConfig.initIdentityScope(type);

        pLANTE_NON_DESIRABLEDaoConfig = daoConfigMap.get(PLANTE_NON_DESIRABLEDao.class).clone();
        pLANTE_NON_DESIRABLEDaoConfig.initIdentityScope(type);

        eTAT_PLANTEDaoConfig = daoConfigMap.get(ETAT_PLANTEDao.class).clone();
        eTAT_PLANTEDaoConfig.initIdentityScope(type);

        lISTE_PLANTEDaoConfig = daoConfigMap.get(LISTE_PLANTEDao.class).clone();
        lISTE_PLANTEDaoConfig.initIdentityScope(type);

        cONTRE_VISITEDaoConfig = daoConfigMap.get(CONTRE_VISITEDao.class).clone();
        cONTRE_VISITEDaoConfig.initIdentityScope(type);

        eTAT_CONTRE_VISITEDaoConfig = daoConfigMap.get(ETAT_CONTRE_VISITEDao.class).clone();
        eTAT_CONTRE_VISITEDaoConfig.initIdentityScope(type);

        cAUSEDaoConfig = daoConfigMap.get(CAUSEDao.class).clone();
        cAUSEDaoConfig.initIdentityScope(type);

        mESSAGEDaoConfig = daoConfigMap.get(MESSAGEDao.class).clone();
        mESSAGEDaoConfig.initIdentityScope(type);

        sELECTION_DESTINATAIRE_DEFAUTDaoConfig = daoConfigMap.get(SELECTION_DESTINATAIRE_DEFAUTDao.class).clone();
        sELECTION_DESTINATAIRE_DEFAUTDaoConfig.initIdentityScope(type);

        oBJET_MESSAGEDaoConfig = daoConfigMap.get(OBJET_MESSAGEDao.class).clone();
        oBJET_MESSAGEDaoConfig.initIdentityScope(type);

        tEXTE_MESSAGEDaoConfig = daoConfigMap.get(TEXTE_MESSAGEDao.class).clone();
        tEXTE_MESSAGEDaoConfig.initIdentityScope(type);

        dESTINATAIRE_DEFAUTDaoConfig = daoConfigMap.get(DESTINATAIRE_DEFAUTDao.class).clone();
        dESTINATAIRE_DEFAUTDaoConfig.initIdentityScope(type);

        visiteDaoConfig = daoConfigMap.get(VisiteDao.class).clone();
        visiteDaoConfig.initIdentityScope(type);

        attitude_Entreprise_VisiteDaoConfig = daoConfigMap.get(Attitude_Entreprise_VisiteDao.class).clone();
        attitude_Entreprise_VisiteDaoConfig.initIdentityScope(type);

        interet_VisiteDaoConfig = daoConfigMap.get(Interet_VisiteDao.class).clone();
        interet_VisiteDaoConfig.initIdentityScope(type);

        sujet_Visite_PrincipalDaoConfig = daoConfigMap.get(Sujet_Visite_PrincipalDao.class).clone();
        sujet_Visite_PrincipalDaoConfig.initIdentityScope(type);

        sujet_Visite_SecondaireDaoConfig = daoConfigMap.get(Sujet_Visite_SecondaireDao.class).clone();
        sujet_Visite_SecondaireDaoConfig.initIdentityScope(type);

        objet_VisiteDaoConfig = daoConfigMap.get(Objet_VisiteDao.class).clone();
        objet_VisiteDaoConfig.initIdentityScope(type);

        incidentDaoConfig = daoConfigMap.get(IncidentDao.class).clone();
        incidentDaoConfig.initIdentityScope(type);

        etat_IncidentDaoConfig = daoConfigMap.get(Etat_IncidentDao.class).clone();
        etat_IncidentDaoConfig.initIdentityScope(type);

        type_IncidentDaoConfig = daoConfigMap.get(Type_IncidentDao.class).clone();
        type_IncidentDaoConfig.initIdentityScope(type);

        incident_AdministratifDaoConfig = daoConfigMap.get(Incident_AdministratifDao.class).clone();
        incident_AdministratifDaoConfig.initIdentityScope(type);

        incident_ContratDaoConfig = daoConfigMap.get(Incident_ContratDao.class).clone();
        incident_ContratDaoConfig.initIdentityScope(type);

        sujet_MessageDaoConfig = daoConfigMap.get(Sujet_MessageDao.class).clone();
        sujet_MessageDaoConfig.initIdentityScope(type);

        suivi_IncidentDaoConfig = daoConfigMap.get(Suivi_IncidentDao.class).clone();
        suivi_IncidentDaoConfig.initIdentityScope(type);

        expediteurDaoConfig = daoConfigMap.get(ExpediteurDao.class).clone();
        expediteurDaoConfig.initIdentityScope(type);

        type_ExpediteurDaoConfig = daoConfigMap.get(Type_ExpediteurDao.class).clone();
        type_ExpediteurDaoConfig.initIdentityScope(type);

        liste_Expediteur_ContactDaoConfig = daoConfigMap.get(Liste_Expediteur_ContactDao.class).clone();
        liste_Expediteur_ContactDaoConfig.initIdentityScope(type);

        liste_Expediteur_AgronomeDaoConfig = daoConfigMap.get(Liste_Expediteur_AgronomeDao.class).clone();
        liste_Expediteur_AgronomeDaoConfig.initIdentityScope(type);

        liste_Expediteur_AdministrationDaoConfig = daoConfigMap.get(Liste_Expediteur_AdministrationDao.class).clone();
        liste_Expediteur_AdministrationDaoConfig.initIdentityScope(type);

        compteDao = new CompteDao(compteDaoConfig, this);
        type_CompteDao = new Type_CompteDao(type_CompteDaoConfig, this);
        photoDao = new PhotoDao(photoDaoConfig, this);
        theme_PhotoDao = new Theme_PhotoDao(theme_PhotoDaoConfig, this);
        videoDao = new VideoDao(videoDaoConfig, this);
        collaborateurDao = new CollaborateurDao(collaborateurDaoConfig, this);
        identiteDao = new IdentiteDao(identiteDaoConfig, this);
        sexeDao = new SexeDao(sexeDaoConfig, this);
        adresseDao = new AdresseDao(adresseDaoConfig, this);
        code_Postal_VilleDao = new Code_Postal_VilleDao(code_Postal_VilleDaoConfig, this);
        regionDao = new RegionDao(regionDaoConfig, this);
        paysDao = new PaysDao(paysDaoConfig, this);
        geolocalisationDao = new GeolocalisationDao(geolocalisationDaoConfig, this);
        zoneDao = new ZoneDao(zoneDaoConfig, this);
        groupementDao = new GroupementDao(groupementDaoConfig, this);
        entrepriseDao = new EntrepriseDao(entrepriseDaoConfig, this);
        structure_SocialeDao = new Structure_SocialeDao(structure_SocialeDaoConfig, this);
        situationDao = new SituationDao(situationDaoConfig, this);
        statut_SoytouchDao = new Statut_SoytouchDao(statut_SoytouchDaoConfig, this);
        activiteDao = new ActiviteDao(activiteDaoConfig, this);
        biologiqueDao = new BiologiqueDao(biologiqueDaoConfig, this);
        type_BioDao = new Type_BioDao(type_BioDaoConfig, this);
        contactDao = new ContactDao(contactDaoConfig, this);
        categorie_ContactDao = new Categorie_ContactDao(categorie_ContactDaoConfig, this);
        metierDao = new MetierDao(metierDaoConfig, this);
        sociabiliteDao = new SociabiliteDao(sociabiliteDaoConfig, this);
        sUIVI_PRODUCTEURDao = new SUIVI_PRODUCTEURDao(sUIVI_PRODUCTEURDaoConfig, this);
        oSDao = new OSDao(oSDaoConfig, this);
        sOURCE_INTERNETDao = new SOURCE_INTERNETDao(sOURCE_INTERNETDaoConfig, this);
        sOURCE_JOURNAUXDao = new SOURCE_JOURNAUXDao(sOURCE_JOURNAUXDaoConfig, this);
        iLOTDao = new ILOTDao(iLOTDaoConfig, this);
        pARCELLEDao = new PARCELLEDao(pARCELLEDaoConfig, this);
        photo_ParcelleDao = new Photo_ParcelleDao(photo_ParcelleDaoConfig, this);
        sTATUT_PARCELLEDao = new STATUT_PARCELLEDao(sTATUT_PARCELLEDaoConfig, this);
        tYPE_CULTUREDao = new TYPE_CULTUREDao(tYPE_CULTUREDaoConfig, this);
        iRRIGATIONDao = new IRRIGATIONDao(iRRIGATIONDaoConfig, this);
        tYPE_IRRIGATIONDao = new TYPE_IRRIGATIONDao(tYPE_IRRIGATIONDaoConfig, this);
        sTOCKAGEDao = new STOCKAGEDao(sTOCKAGEDaoConfig, this);
        photo_StockageDao = new Photo_StockageDao(photo_StockageDaoConfig, this);
        tYPE_STOCKEURDao = new TYPE_STOCKEURDao(tYPE_STOCKEURDaoConfig, this);
        eTAT_STOCKEURDao = new ETAT_STOCKEURDao(eTAT_STOCKEURDaoConfig, this);
        sTATUT_STOCKEURDao = new STATUT_STOCKEURDao(sTATUT_STOCKEURDaoConfig, this);
        aUDIT_STOCKAGEDao = new AUDIT_STOCKAGEDao(aUDIT_STOCKAGEDaoConfig, this);
        pROPRETEDao = new PROPRETEDao(pROPRETEDaoConfig, this);
        cONTAMINATION_CROISEDao = new CONTAMINATION_CROISEDao(cONTAMINATION_CROISEDaoConfig, this);
        vENTILATIONDao = new VENTILATIONDao(vENTILATIONDaoConfig, this);
        tEMPERATUREDao = new TEMPERATUREDao(tEMPERATUREDaoConfig, this);
        tRIAGE_GRAINDao = new TRIAGE_GRAINDao(tRIAGE_GRAINDaoConfig, this);
        nUISIBLEDao = new NUISIBLEDao(nUISIBLEDaoConfig, this);
        eTAT_NUISIBLEDao = new ETAT_NUISIBLEDao(eTAT_NUISIBLEDaoConfig, this);
        lISTE_NUISIBLEDao = new LISTE_NUISIBLEDao(lISTE_NUISIBLEDaoConfig, this);
        aUDITDao = new AUDITDao(aUDITDaoConfig, this);
        oBJET_AUDITDao = new OBJET_AUDITDao(oBJET_AUDITDaoConfig, this);
        rECOLTEDao = new RECOLTEDao(rECOLTEDaoConfig, this);
        aUDIT_DOCUMENTDao = new AUDIT_DOCUMENTDao(aUDIT_DOCUMENTDaoConfig, this);
        dOCUMENTDao = new DOCUMENTDao(dOCUMENTDaoConfig, this);
        sCAN_DOCUMENTDao = new SCAN_DOCUMENTDao(sCAN_DOCUMENTDaoConfig, this);
        tYPE_DOCUMENTDao = new TYPE_DOCUMENTDao(tYPE_DOCUMENTDaoConfig, this);
        dOCUMENT_FOURNIDao = new DOCUMENT_FOURNIDao(dOCUMENT_FOURNIDaoConfig, this);
        aUDIT_PARCELLEDao = new AUDIT_PARCELLEDao(aUDIT_PARCELLEDaoConfig, this);
        eTAT_VEGETATIFDao = new ETAT_VEGETATIFDao(eTAT_VEGETATIFDaoConfig, this);
        pLANTE_NON_DESIRABLEDao = new PLANTE_NON_DESIRABLEDao(pLANTE_NON_DESIRABLEDaoConfig, this);
        eTAT_PLANTEDao = new ETAT_PLANTEDao(eTAT_PLANTEDaoConfig, this);
        lISTE_PLANTEDao = new LISTE_PLANTEDao(lISTE_PLANTEDaoConfig, this);
        cONTRE_VISITEDao = new CONTRE_VISITEDao(cONTRE_VISITEDaoConfig, this);
        eTAT_CONTRE_VISITEDao = new ETAT_CONTRE_VISITEDao(eTAT_CONTRE_VISITEDaoConfig, this);
        cAUSEDao = new CAUSEDao(cAUSEDaoConfig, this);
        mESSAGEDao = new MESSAGEDao(mESSAGEDaoConfig, this);
        sELECTION_DESTINATAIRE_DEFAUTDao = new SELECTION_DESTINATAIRE_DEFAUTDao(sELECTION_DESTINATAIRE_DEFAUTDaoConfig, this);
        oBJET_MESSAGEDao = new OBJET_MESSAGEDao(oBJET_MESSAGEDaoConfig, this);
        tEXTE_MESSAGEDao = new TEXTE_MESSAGEDao(tEXTE_MESSAGEDaoConfig, this);
        dESTINATAIRE_DEFAUTDao = new DESTINATAIRE_DEFAUTDao(dESTINATAIRE_DEFAUTDaoConfig, this);
        visiteDao = new VisiteDao(visiteDaoConfig, this);
        attitude_Entreprise_VisiteDao = new Attitude_Entreprise_VisiteDao(attitude_Entreprise_VisiteDaoConfig, this);
        interet_VisiteDao = new Interet_VisiteDao(interet_VisiteDaoConfig, this);
        sujet_Visite_PrincipalDao = new Sujet_Visite_PrincipalDao(sujet_Visite_PrincipalDaoConfig, this);
        sujet_Visite_SecondaireDao = new Sujet_Visite_SecondaireDao(sujet_Visite_SecondaireDaoConfig, this);
        objet_VisiteDao = new Objet_VisiteDao(objet_VisiteDaoConfig, this);
        incidentDao = new IncidentDao(incidentDaoConfig, this);
        etat_IncidentDao = new Etat_IncidentDao(etat_IncidentDaoConfig, this);
        type_IncidentDao = new Type_IncidentDao(type_IncidentDaoConfig, this);
        incident_AdministratifDao = new Incident_AdministratifDao(incident_AdministratifDaoConfig, this);
        incident_ContratDao = new Incident_ContratDao(incident_ContratDaoConfig, this);
        sujet_MessageDao = new Sujet_MessageDao(sujet_MessageDaoConfig, this);
        suivi_IncidentDao = new Suivi_IncidentDao(suivi_IncidentDaoConfig, this);
        expediteurDao = new ExpediteurDao(expediteurDaoConfig, this);
        type_ExpediteurDao = new Type_ExpediteurDao(type_ExpediteurDaoConfig, this);
        liste_Expediteur_ContactDao = new Liste_Expediteur_ContactDao(liste_Expediteur_ContactDaoConfig, this);
        liste_Expediteur_AgronomeDao = new Liste_Expediteur_AgronomeDao(liste_Expediteur_AgronomeDaoConfig, this);
        liste_Expediteur_AdministrationDao = new Liste_Expediteur_AdministrationDao(liste_Expediteur_AdministrationDaoConfig, this);

        registerDao(Compte.class, compteDao);
        registerDao(Type_Compte.class, type_CompteDao);
        registerDao(Photo.class, photoDao);
        registerDao(Theme_Photo.class, theme_PhotoDao);
        registerDao(Video.class, videoDao);
        registerDao(Collaborateur.class, collaborateurDao);
        registerDao(Identite.class, identiteDao);
        registerDao(Sexe.class, sexeDao);
        registerDao(Adresse.class, adresseDao);
        registerDao(Code_Postal_Ville.class, code_Postal_VilleDao);
        registerDao(Region.class, regionDao);
        registerDao(Pays.class, paysDao);
        registerDao(Geolocalisation.class, geolocalisationDao);
        registerDao(Zone.class, zoneDao);
        registerDao(Groupement.class, groupementDao);
        registerDao(Entreprise.class, entrepriseDao);
        registerDao(Structure_Sociale.class, structure_SocialeDao);
        registerDao(Situation.class, situationDao);
        registerDao(Statut_Soytouch.class, statut_SoytouchDao);
        registerDao(Activite.class, activiteDao);
        registerDao(Biologique.class, biologiqueDao);
        registerDao(Type_Bio.class, type_BioDao);
        registerDao(Contact.class, contactDao);
        registerDao(Categorie_Contact.class, categorie_ContactDao);
        registerDao(Metier.class, metierDao);
        registerDao(Sociabilite.class, sociabiliteDao);
        registerDao(SUIVI_PRODUCTEUR.class, sUIVI_PRODUCTEURDao);
        registerDao(OS.class, oSDao);
        registerDao(SOURCE_INTERNET.class, sOURCE_INTERNETDao);
        registerDao(SOURCE_JOURNAUX.class, sOURCE_JOURNAUXDao);
        registerDao(ILOT.class, iLOTDao);
        registerDao(PARCELLE.class, pARCELLEDao);
        registerDao(Photo_Parcelle.class, photo_ParcelleDao);
        registerDao(STATUT_PARCELLE.class, sTATUT_PARCELLEDao);
        registerDao(TYPE_CULTURE.class, tYPE_CULTUREDao);
        registerDao(IRRIGATION.class, iRRIGATIONDao);
        registerDao(TYPE_IRRIGATION.class, tYPE_IRRIGATIONDao);
        registerDao(STOCKAGE.class, sTOCKAGEDao);
        registerDao(Photo_Stockage.class, photo_StockageDao);
        registerDao(TYPE_STOCKEUR.class, tYPE_STOCKEURDao);
        registerDao(ETAT_STOCKEUR.class, eTAT_STOCKEURDao);
        registerDao(STATUT_STOCKEUR.class, sTATUT_STOCKEURDao);
        registerDao(AUDIT_STOCKAGE.class, aUDIT_STOCKAGEDao);
        registerDao(PROPRETE.class, pROPRETEDao);
        registerDao(CONTAMINATION_CROISE.class, cONTAMINATION_CROISEDao);
        registerDao(VENTILATION.class, vENTILATIONDao);
        registerDao(TEMPERATURE.class, tEMPERATUREDao);
        registerDao(TRIAGE_GRAIN.class, tRIAGE_GRAINDao);
        registerDao(NUISIBLE.class, nUISIBLEDao);
        registerDao(ETAT_NUISIBLE.class, eTAT_NUISIBLEDao);
        registerDao(LISTE_NUISIBLE.class, lISTE_NUISIBLEDao);
        registerDao(AUDIT.class, aUDITDao);
        registerDao(OBJET_AUDIT.class, oBJET_AUDITDao);
        registerDao(RECOLTE.class, rECOLTEDao);
        registerDao(AUDIT_DOCUMENT.class, aUDIT_DOCUMENTDao);
        registerDao(DOCUMENT.class, dOCUMENTDao);
        registerDao(SCAN_DOCUMENT.class, sCAN_DOCUMENTDao);
        registerDao(TYPE_DOCUMENT.class, tYPE_DOCUMENTDao);
        registerDao(DOCUMENT_FOURNI.class, dOCUMENT_FOURNIDao);
        registerDao(AUDIT_PARCELLE.class, aUDIT_PARCELLEDao);
        registerDao(ETAT_VEGETATIF.class, eTAT_VEGETATIFDao);
        registerDao(PLANTE_NON_DESIRABLE.class, pLANTE_NON_DESIRABLEDao);
        registerDao(ETAT_PLANTE.class, eTAT_PLANTEDao);
        registerDao(LISTE_PLANTE.class, lISTE_PLANTEDao);
        registerDao(CONTRE_VISITE.class, cONTRE_VISITEDao);
        registerDao(ETAT_CONTRE_VISITE.class, eTAT_CONTRE_VISITEDao);
        registerDao(CAUSE.class, cAUSEDao);
        registerDao(MESSAGE.class, mESSAGEDao);
        registerDao(SELECTION_DESTINATAIRE_DEFAUT.class, sELECTION_DESTINATAIRE_DEFAUTDao);
        registerDao(OBJET_MESSAGE.class, oBJET_MESSAGEDao);
        registerDao(TEXTE_MESSAGE.class, tEXTE_MESSAGEDao);
        registerDao(DESTINATAIRE_DEFAUT.class, dESTINATAIRE_DEFAUTDao);
        registerDao(Visite.class, visiteDao);
        registerDao(Attitude_Entreprise_Visite.class, attitude_Entreprise_VisiteDao);
        registerDao(Interet_Visite.class, interet_VisiteDao);
        registerDao(Sujet_Visite_Principal.class, sujet_Visite_PrincipalDao);
        registerDao(Sujet_Visite_Secondaire.class, sujet_Visite_SecondaireDao);
        registerDao(Objet_Visite.class, objet_VisiteDao);
        registerDao(Incident.class, incidentDao);
        registerDao(Etat_Incident.class, etat_IncidentDao);
        registerDao(Type_Incident.class, type_IncidentDao);
        registerDao(Incident_Administratif.class, incident_AdministratifDao);
        registerDao(Incident_Contrat.class, incident_ContratDao);
        registerDao(Sujet_Message.class, sujet_MessageDao);
        registerDao(Suivi_Incident.class, suivi_IncidentDao);
        registerDao(Expediteur.class, expediteurDao);
        registerDao(Type_Expediteur.class, type_ExpediteurDao);
        registerDao(Liste_Expediteur_Contact.class, liste_Expediteur_ContactDao);
        registerDao(Liste_Expediteur_Agronome.class, liste_Expediteur_AgronomeDao);
        registerDao(Liste_Expediteur_Administration.class, liste_Expediteur_AdministrationDao);
    }
    
    public void clear() {
        compteDaoConfig.clearIdentityScope();
        type_CompteDaoConfig.clearIdentityScope();
        photoDaoConfig.clearIdentityScope();
        theme_PhotoDaoConfig.clearIdentityScope();
        videoDaoConfig.clearIdentityScope();
        collaborateurDaoConfig.clearIdentityScope();
        identiteDaoConfig.clearIdentityScope();
        sexeDaoConfig.clearIdentityScope();
        adresseDaoConfig.clearIdentityScope();
        code_Postal_VilleDaoConfig.clearIdentityScope();
        regionDaoConfig.clearIdentityScope();
        paysDaoConfig.clearIdentityScope();
        geolocalisationDaoConfig.clearIdentityScope();
        zoneDaoConfig.clearIdentityScope();
        groupementDaoConfig.clearIdentityScope();
        entrepriseDaoConfig.clearIdentityScope();
        structure_SocialeDaoConfig.clearIdentityScope();
        situationDaoConfig.clearIdentityScope();
        statut_SoytouchDaoConfig.clearIdentityScope();
        activiteDaoConfig.clearIdentityScope();
        biologiqueDaoConfig.clearIdentityScope();
        type_BioDaoConfig.clearIdentityScope();
        contactDaoConfig.clearIdentityScope();
        categorie_ContactDaoConfig.clearIdentityScope();
        metierDaoConfig.clearIdentityScope();
        sociabiliteDaoConfig.clearIdentityScope();
        sUIVI_PRODUCTEURDaoConfig.clearIdentityScope();
        oSDaoConfig.clearIdentityScope();
        sOURCE_INTERNETDaoConfig.clearIdentityScope();
        sOURCE_JOURNAUXDaoConfig.clearIdentityScope();
        iLOTDaoConfig.clearIdentityScope();
        pARCELLEDaoConfig.clearIdentityScope();
        photo_ParcelleDaoConfig.clearIdentityScope();
        sTATUT_PARCELLEDaoConfig.clearIdentityScope();
        tYPE_CULTUREDaoConfig.clearIdentityScope();
        iRRIGATIONDaoConfig.clearIdentityScope();
        tYPE_IRRIGATIONDaoConfig.clearIdentityScope();
        sTOCKAGEDaoConfig.clearIdentityScope();
        photo_StockageDaoConfig.clearIdentityScope();
        tYPE_STOCKEURDaoConfig.clearIdentityScope();
        eTAT_STOCKEURDaoConfig.clearIdentityScope();
        sTATUT_STOCKEURDaoConfig.clearIdentityScope();
        aUDIT_STOCKAGEDaoConfig.clearIdentityScope();
        pROPRETEDaoConfig.clearIdentityScope();
        cONTAMINATION_CROISEDaoConfig.clearIdentityScope();
        vENTILATIONDaoConfig.clearIdentityScope();
        tEMPERATUREDaoConfig.clearIdentityScope();
        tRIAGE_GRAINDaoConfig.clearIdentityScope();
        nUISIBLEDaoConfig.clearIdentityScope();
        eTAT_NUISIBLEDaoConfig.clearIdentityScope();
        lISTE_NUISIBLEDaoConfig.clearIdentityScope();
        aUDITDaoConfig.clearIdentityScope();
        oBJET_AUDITDaoConfig.clearIdentityScope();
        rECOLTEDaoConfig.clearIdentityScope();
        aUDIT_DOCUMENTDaoConfig.clearIdentityScope();
        dOCUMENTDaoConfig.clearIdentityScope();
        sCAN_DOCUMENTDaoConfig.clearIdentityScope();
        tYPE_DOCUMENTDaoConfig.clearIdentityScope();
        dOCUMENT_FOURNIDaoConfig.clearIdentityScope();
        aUDIT_PARCELLEDaoConfig.clearIdentityScope();
        eTAT_VEGETATIFDaoConfig.clearIdentityScope();
        pLANTE_NON_DESIRABLEDaoConfig.clearIdentityScope();
        eTAT_PLANTEDaoConfig.clearIdentityScope();
        lISTE_PLANTEDaoConfig.clearIdentityScope();
        cONTRE_VISITEDaoConfig.clearIdentityScope();
        eTAT_CONTRE_VISITEDaoConfig.clearIdentityScope();
        cAUSEDaoConfig.clearIdentityScope();
        mESSAGEDaoConfig.clearIdentityScope();
        sELECTION_DESTINATAIRE_DEFAUTDaoConfig.clearIdentityScope();
        oBJET_MESSAGEDaoConfig.clearIdentityScope();
        tEXTE_MESSAGEDaoConfig.clearIdentityScope();
        dESTINATAIRE_DEFAUTDaoConfig.clearIdentityScope();
        visiteDaoConfig.clearIdentityScope();
        attitude_Entreprise_VisiteDaoConfig.clearIdentityScope();
        interet_VisiteDaoConfig.clearIdentityScope();
        sujet_Visite_PrincipalDaoConfig.clearIdentityScope();
        sujet_Visite_SecondaireDaoConfig.clearIdentityScope();
        objet_VisiteDaoConfig.clearIdentityScope();
        incidentDaoConfig.clearIdentityScope();
        etat_IncidentDaoConfig.clearIdentityScope();
        type_IncidentDaoConfig.clearIdentityScope();
        incident_AdministratifDaoConfig.clearIdentityScope();
        incident_ContratDaoConfig.clearIdentityScope();
        sujet_MessageDaoConfig.clearIdentityScope();
        suivi_IncidentDaoConfig.clearIdentityScope();
        expediteurDaoConfig.clearIdentityScope();
        type_ExpediteurDaoConfig.clearIdentityScope();
        liste_Expediteur_ContactDaoConfig.clearIdentityScope();
        liste_Expediteur_AgronomeDaoConfig.clearIdentityScope();
        liste_Expediteur_AdministrationDaoConfig.clearIdentityScope();
    }

    public CompteDao getCompteDao() {
        return compteDao;
    }

    public Type_CompteDao getType_CompteDao() {
        return type_CompteDao;
    }

    public PhotoDao getPhotoDao() {
        return photoDao;
    }

    public Theme_PhotoDao getTheme_PhotoDao() {
        return theme_PhotoDao;
    }

    public VideoDao getVideoDao() {
        return videoDao;
    }

    public CollaborateurDao getCollaborateurDao() {
        return collaborateurDao;
    }

    public IdentiteDao getIdentiteDao() {
        return identiteDao;
    }

    public SexeDao getSexeDao() {
        return sexeDao;
    }

    public AdresseDao getAdresseDao() {
        return adresseDao;
    }

    public Code_Postal_VilleDao getCode_Postal_VilleDao() {
        return code_Postal_VilleDao;
    }

    public RegionDao getRegionDao() {
        return regionDao;
    }

    public PaysDao getPaysDao() {
        return paysDao;
    }

    public GeolocalisationDao getGeolocalisationDao() {
        return geolocalisationDao;
    }

    public ZoneDao getZoneDao() {
        return zoneDao;
    }

    public GroupementDao getGroupementDao() {
        return groupementDao;
    }

    public EntrepriseDao getEntrepriseDao() {
        return entrepriseDao;
    }

    public Structure_SocialeDao getStructure_SocialeDao() {
        return structure_SocialeDao;
    }

    public SituationDao getSituationDao() {
        return situationDao;
    }

    public Statut_SoytouchDao getStatut_SoytouchDao() {
        return statut_SoytouchDao;
    }

    public ActiviteDao getActiviteDao() {
        return activiteDao;
    }

    public BiologiqueDao getBiologiqueDao() {
        return biologiqueDao;
    }

    public Type_BioDao getType_BioDao() {
        return type_BioDao;
    }

    public ContactDao getContactDao() {
        return contactDao;
    }

    public Categorie_ContactDao getCategorie_ContactDao() {
        return categorie_ContactDao;
    }

    public MetierDao getMetierDao() {
        return metierDao;
    }

    public SociabiliteDao getSociabiliteDao() {
        return sociabiliteDao;
    }

    public SUIVI_PRODUCTEURDao getSUIVI_PRODUCTEURDao() {
        return sUIVI_PRODUCTEURDao;
    }

    public OSDao getOSDao() {
        return oSDao;
    }

    public SOURCE_INTERNETDao getSOURCE_INTERNETDao() {
        return sOURCE_INTERNETDao;
    }

    public SOURCE_JOURNAUXDao getSOURCE_JOURNAUXDao() {
        return sOURCE_JOURNAUXDao;
    }

    public ILOTDao getILOTDao() {
        return iLOTDao;
    }

    public PARCELLEDao getPARCELLEDao() {
        return pARCELLEDao;
    }

    public Photo_ParcelleDao getPhoto_ParcelleDao() {
        return photo_ParcelleDao;
    }

    public STATUT_PARCELLEDao getSTATUT_PARCELLEDao() {
        return sTATUT_PARCELLEDao;
    }

    public TYPE_CULTUREDao getTYPE_CULTUREDao() {
        return tYPE_CULTUREDao;
    }

    public IRRIGATIONDao getIRRIGATIONDao() {
        return iRRIGATIONDao;
    }

    public TYPE_IRRIGATIONDao getTYPE_IRRIGATIONDao() {
        return tYPE_IRRIGATIONDao;
    }

    public STOCKAGEDao getSTOCKAGEDao() {
        return sTOCKAGEDao;
    }

    public Photo_StockageDao getPhoto_StockageDao() {
        return photo_StockageDao;
    }

    public TYPE_STOCKEURDao getTYPE_STOCKEURDao() {
        return tYPE_STOCKEURDao;
    }

    public ETAT_STOCKEURDao getETAT_STOCKEURDao() {
        return eTAT_STOCKEURDao;
    }

    public STATUT_STOCKEURDao getSTATUT_STOCKEURDao() {
        return sTATUT_STOCKEURDao;
    }

    public AUDIT_STOCKAGEDao getAUDIT_STOCKAGEDao() {
        return aUDIT_STOCKAGEDao;
    }

    public PROPRETEDao getPROPRETEDao() {
        return pROPRETEDao;
    }

    public CONTAMINATION_CROISEDao getCONTAMINATION_CROISEDao() {
        return cONTAMINATION_CROISEDao;
    }

    public VENTILATIONDao getVENTILATIONDao() {
        return vENTILATIONDao;
    }

    public TEMPERATUREDao getTEMPERATUREDao() {
        return tEMPERATUREDao;
    }

    public TRIAGE_GRAINDao getTRIAGE_GRAINDao() {
        return tRIAGE_GRAINDao;
    }

    public NUISIBLEDao getNUISIBLEDao() {
        return nUISIBLEDao;
    }

    public ETAT_NUISIBLEDao getETAT_NUISIBLEDao() {
        return eTAT_NUISIBLEDao;
    }

    public LISTE_NUISIBLEDao getLISTE_NUISIBLEDao() {
        return lISTE_NUISIBLEDao;
    }

    public AUDITDao getAUDITDao() {
        return aUDITDao;
    }

    public OBJET_AUDITDao getOBJET_AUDITDao() {
        return oBJET_AUDITDao;
    }

    public RECOLTEDao getRECOLTEDao() {
        return rECOLTEDao;
    }

    public AUDIT_DOCUMENTDao getAUDIT_DOCUMENTDao() {
        return aUDIT_DOCUMENTDao;
    }

    public DOCUMENTDao getDOCUMENTDao() {
        return dOCUMENTDao;
    }

    public SCAN_DOCUMENTDao getSCAN_DOCUMENTDao() {
        return sCAN_DOCUMENTDao;
    }

    public TYPE_DOCUMENTDao getTYPE_DOCUMENTDao() {
        return tYPE_DOCUMENTDao;
    }

    public DOCUMENT_FOURNIDao getDOCUMENT_FOURNIDao() {
        return dOCUMENT_FOURNIDao;
    }

    public AUDIT_PARCELLEDao getAUDIT_PARCELLEDao() {
        return aUDIT_PARCELLEDao;
    }

    public ETAT_VEGETATIFDao getETAT_VEGETATIFDao() {
        return eTAT_VEGETATIFDao;
    }

    public PLANTE_NON_DESIRABLEDao getPLANTE_NON_DESIRABLEDao() {
        return pLANTE_NON_DESIRABLEDao;
    }

    public ETAT_PLANTEDao getETAT_PLANTEDao() {
        return eTAT_PLANTEDao;
    }

    public LISTE_PLANTEDao getLISTE_PLANTEDao() {
        return lISTE_PLANTEDao;
    }

    public CONTRE_VISITEDao getCONTRE_VISITEDao() {
        return cONTRE_VISITEDao;
    }

    public ETAT_CONTRE_VISITEDao getETAT_CONTRE_VISITEDao() {
        return eTAT_CONTRE_VISITEDao;
    }

    public CAUSEDao getCAUSEDao() {
        return cAUSEDao;
    }

    public MESSAGEDao getMESSAGEDao() {
        return mESSAGEDao;
    }

    public SELECTION_DESTINATAIRE_DEFAUTDao getSELECTION_DESTINATAIRE_DEFAUTDao() {
        return sELECTION_DESTINATAIRE_DEFAUTDao;
    }

    public OBJET_MESSAGEDao getOBJET_MESSAGEDao() {
        return oBJET_MESSAGEDao;
    }

    public TEXTE_MESSAGEDao getTEXTE_MESSAGEDao() {
        return tEXTE_MESSAGEDao;
    }

    public DESTINATAIRE_DEFAUTDao getDESTINATAIRE_DEFAUTDao() {
        return dESTINATAIRE_DEFAUTDao;
    }

    public VisiteDao getVisiteDao() {
        return visiteDao;
    }

    public Attitude_Entreprise_VisiteDao getAttitude_Entreprise_VisiteDao() {
        return attitude_Entreprise_VisiteDao;
    }

    public Interet_VisiteDao getInteret_VisiteDao() {
        return interet_VisiteDao;
    }

    public Sujet_Visite_PrincipalDao getSujet_Visite_PrincipalDao() {
        return sujet_Visite_PrincipalDao;
    }

    public Sujet_Visite_SecondaireDao getSujet_Visite_SecondaireDao() {
        return sujet_Visite_SecondaireDao;
    }

    public Objet_VisiteDao getObjet_VisiteDao() {
        return objet_VisiteDao;
    }

    public IncidentDao getIncidentDao() {
        return incidentDao;
    }

    public Etat_IncidentDao getEtat_IncidentDao() {
        return etat_IncidentDao;
    }

    public Type_IncidentDao getType_IncidentDao() {
        return type_IncidentDao;
    }

    public Incident_AdministratifDao getIncident_AdministratifDao() {
        return incident_AdministratifDao;
    }

    public Incident_ContratDao getIncident_ContratDao() {
        return incident_ContratDao;
    }

    public Sujet_MessageDao getSujet_MessageDao() {
        return sujet_MessageDao;
    }

    public Suivi_IncidentDao getSuivi_IncidentDao() {
        return suivi_IncidentDao;
    }

    public ExpediteurDao getExpediteurDao() {
        return expediteurDao;
    }

    public Type_ExpediteurDao getType_ExpediteurDao() {
        return type_ExpediteurDao;
    }

    public Liste_Expediteur_ContactDao getListe_Expediteur_ContactDao() {
        return liste_Expediteur_ContactDao;
    }

    public Liste_Expediteur_AgronomeDao getListe_Expediteur_AgronomeDao() {
        return liste_Expediteur_AgronomeDao;
    }

    public Liste_Expediteur_AdministrationDao getListe_Expediteur_AdministrationDao() {
        return liste_Expediteur_AdministrationDao;
    }

}
