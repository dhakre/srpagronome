package com.srp.agronome.android.webservice;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by doucoure on 22/08/2017.
 */


public class ServerResponse implements Serializable {
    @SerializedName("returned_remarque")
    private String remarque;
    @SerializedName("returned_archive")
    private int archive;
    @SerializedName("response_code")
    private int responseCode;

    public ServerResponse(String remarque, int archive, int responseCode) {
        this.remarque = remarque;
        this.archive = archive;

        this.responseCode = responseCode;
    }


    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public int getArchive() {
        return archive;
    }

    public void setArchive(int archive) {
        this.archive = archive;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}

