package com.srp.agronome.android.webservice.pojo;

/**
 * Created by soytouchrp on 05/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataAdresse {

    @SerializedName("ID_ADRESSE")
    @Expose
    private Integer idAdresse;
    @SerializedName("RUE")
    @Expose
    private String rue;
    @SerializedName("VILLE")
    @Expose
    private String ville;
    @SerializedName("PAYS")
    @Expose
    private String pays;
    @SerializedName("CREATION_DATE")
    @Expose
    private String cREATIONDATE;
    @SerializedName("MODIFICATION_DATE")
    @Expose
    private String mODIFICATIONDATE;

    public Integer getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(Integer idAdresse) {
        this.idAdresse = idAdresse;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getCREATIONDATE() {
        return cREATIONDATE;
    }

    public void setCREATIONDATE(String cREATIONDATE) {
        this.cREATIONDATE = cREATIONDATE;
    }

    public String getMODIFICATIONDATE() {
        return mODIFICATIONDATE;
    }

    public void setMODIFICATIONDATE(String mODIFICATIONDATE) {
        this.mODIFICATIONDATE = mODIFICATIONDATE;
    }

}



//
//-----------------------------------com.example.AdresseSiege.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class AdresseSiege {
//
//    @SerializedName("id_adresse")
//    @Expose
//    private Integer idAdresse;
//    @SerializedName("rue")
//    @Expose
//    private String rue;
//    @SerializedName("ville")
//    @Expose
//    private String ville;
//    @SerializedName("pays")
//    @Expose
//    private String pays;
//    @SerializedName("CREATION_DATE")
//    @Expose
//    private CREATIONDATE cREATIONDATE;
//    @SerializedName("MODIFICATION_DATE")
//    @Expose
//    private MODIFICATIONDATE mODIFICATIONDATE;
//
//    public Integer getIdAdresse() {
//        return idAdresse;
//    }
//
//    public void setIdAdresse(Integer idAdresse) {
//        this.idAdresse = idAdresse;
//    }
//
//    public String getRue() {
//        return rue;
//    }
//
//    public void setRue(String rue) {
//        this.rue = rue;
//    }
//
//    public String getVille() {
//        return ville;
//    }
//
//    public void setVille(String ville) {
//        this.ville = ville;
//    }
//
//    public String getPays() {
//        return pays;
//    }
//
//    public void setPays(String pays) {
//        this.pays = pays;
//    }
//
//    public CREATIONDATE getCREATIONDATE() {
//        return cREATIONDATE;
//    }
//
//    public void setCREATIONDATE(CREATIONDATE cREATIONDATE) {
//        this.cREATIONDATE = cREATIONDATE;
//    }
//
//    public MODIFICATIONDATE getMODIFICATIONDATE() {
//        return mODIFICATIONDATE;
//    }
//
//    public void setMODIFICATIONDATE(MODIFICATIONDATE mODIFICATIONDATE) {
//        this.mODIFICATIONDATE = mODIFICATIONDATE;
//    }
//
//}
//-----------------------------------com.example.CREATIONDATE.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class CREATIONDATE {
//
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("timezone_type")
//    @Expose
//    private Integer timezoneType;
//    @SerializedName("timezone")
//    @Expose
//    private String timezone;
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public Integer getTimezoneType() {
//        return timezoneType;
//    }
//
//    public void setTimezoneType(Integer timezoneType) {
//        this.timezoneType = timezoneType;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//}
//-----------------------------------com.example.CreationDate.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class CreationDate {
//
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("timezone_type")
//    @Expose
//    private Integer timezoneType;
//    @SerializedName("timezone")
//    @Expose
//    private String timezone;
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public Integer getTimezoneType() {
//        return timezoneType;
//    }
//
//    public void setTimezoneType(Integer timezoneType) {
//        this.timezoneType = timezoneType;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//}
//-----------------------------------com.example.Data.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class Data {
//
//    @SerializedName("id_fournisseur")
//    @Expose
//    private String idFournisseur;
//    @SerializedName("nom_fournisseur")
//    @Expose
//    private String nomFournisseur;
//    @SerializedName("mail_fournisseur")
//    @Expose
//    private String mailFournisseur;
//    @SerializedName("tel_fournisseur")
//    @Expose
//    private String telFournisseur;
//    @SerializedName("portable_fournisseur")
//    @Expose
//    private String portableFournisseur;
//    @SerializedName("creation_date")
//    @Expose
//    private CreationDate creationDate;
//    @SerializedName("modification_date")
//    @Expose
//    private ModificationDate modificationDate;
//    @SerializedName("adresse_siege")
//    @Expose
//    private AdresseSiege adresseSiege;
//
//    public String getIdFournisseur() {
//        return idFournisseur;
//    }
//
//    public void setIdFournisseur(String idFournisseur) {
//        this.idFournisseur = idFournisseur;
//    }
//
//    public String getNomFournisseur() {
//        return nomFournisseur;
//    }
//
//    public void setNomFournisseur(String nomFournisseur) {
//        this.nomFournisseur = nomFournisseur;
//    }
//
//    public String getMailFournisseur() {
//        return mailFournisseur;
//    }
//
//    public void setMailFournisseur(String mailFournisseur) {
//        this.mailFournisseur = mailFournisseur;
//    }
//
//    public String getTelFournisseur() {
//        return telFournisseur;
//    }
//
//    public void setTelFournisseur(String telFournisseur) {
//        this.telFournisseur = telFournisseur;
//    }
//
//    public String getPortableFournisseur() {
//        return portableFournisseur;
//    }
//
//    public void setPortableFournisseur(String portableFournisseur) {
//        this.portableFournisseur = portableFournisseur;
//    }
//
//    public CreationDate getCreationDate() {
//        return creationDate;
//    }
//
//    public void setCreationDate(CreationDate creationDate) {
//        this.creationDate = creationDate;
//    }
//
//    public ModificationDate getModificationDate() {
//        return modificationDate;
//    }
//
//    public void setModificationDate(ModificationDate modificationDate) {
//        this.modificationDate = modificationDate;
//    }
//
//    public AdresseSiege getAdresseSiege() {
//        return adresseSiege;
//    }
//
//    public void setAdresseSiege(AdresseSiege adresseSiege) {
//        this.adresseSiege = adresseSiege;
//    }
//
//}
//-----------------------------------com.example.Example.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class Example {
//
//    @SerializedName("data")
//    @Expose
//    private Data data;
//
//    public Data getData() {
//        return data;
//    }
//
//    public void setData(Data data) {
//        this.data = data;
//    }
//
//}
//-----------------------------------com.example.MODIFICATIONDATE.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class MODIFICATIONDATE {
//
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("timezone_type")
//    @Expose
//    private Integer timezoneType;
//    @SerializedName("timezone")
//    @Expose
//    private String timezone;
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public Integer getTimezoneType() {
//        return timezoneType;
//    }
//
//    public void setTimezoneType(Integer timezoneType) {
//        this.timezoneType = timezoneType;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//}
//-----------------------------------com.example.ModificationDate.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class ModificationDate {
//
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("timezone_type")
//    @Expose
//    private Integer timezoneType;
//    @SerializedName("timezone")
//    @Expose
//    private String timezone;
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public Integer getTimezoneType() {
//        return timezoneType;
//    }
//
//    public void setTimezoneType(Integer timezoneType) {
//        this.timezoneType = timezoneType;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//}