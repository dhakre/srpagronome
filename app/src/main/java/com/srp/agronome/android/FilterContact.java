package com.srp.agronome.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.srp.agronome.android.db.*;

import java.util.ArrayList;
import java.util.Comparator;

public class FilterContact extends AppCompatActivity {

    ArrayList <Contact>filterContact= new ArrayList<Contact>();
    Intent intent;
    private static final String TAG ="LISTCONTACT Activiy" ;
    private ArrayList<Contact> listContact = new ArrayList<Contact>();
    CustomFilterContactAdapter filterAdapter;
    String newSearchvalue=null;

    //ui
    EditText editSearchText=null;
    private ListView listView;
    private ImageButton btnRetour = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_contact);


        //get the list of data to be shown
        intent=getIntent();
       filterContact= (ArrayList<Contact>) getIntent().getExtras().getSerializable("FilterContactData");
        Log.i("Filter class", "onCreate: size of filtered array="+filterContact.size());

        //serach
       /* editSearchText= (EditText) findViewById(R.id.searchText);
        //watch text change
        editSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                /*newSearchvalue=editSearchText.getText().toString();
                Intent intent = new Intent(FilterContact.this, ListContact.class);
                intent.putExtra("newSearchData", newSearchvalue);
                startActivity(intent);

            }
        });*/

        //list
        listView = (ListView) findViewById(R.id.listViewIndexable);

        filterAdapter = new CustomFilterContactAdapter(FilterContact.this, filterContact);
        filterAdapter.sort(new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                String nom1 = c1.getNom();
                String nom2 = c2.getNom();
                return nom1.compareTo(nom2);
            }
        });

        listView.setAdapter(filterAdapter);
        filterAdapter.notifyDataSetChanged();
        listView.setFastScrollEnabled(true);

        btnRetour = (ImageButton) findViewById(R.id.arrow_back_menu);
        btnRetour.setOnClickListener(ButtonRetourHandler);



    }//end oncreate

    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(FilterContact.this, ListContact.class);
            finish();
            startActivity(intent);
        }
    };


}


