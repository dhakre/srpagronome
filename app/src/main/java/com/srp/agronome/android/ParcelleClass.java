package com.srp.agronome.android;

/**
 * Created by doucoure on 23/08/2017.
 */

public class ParcelleClass {

    private String Nom_Ilot;
    private String Numero_ilot;
    private String Nom_Parcelle;
    private String NUMERO_PARCELLE;
    private Long ID_parcelle;
    private Long ID_ilot;


    public ParcelleClass(String nom_Ilot, String numero_ilot, String nom_Parcelle, String numero_parcelle,Long id_ilot,Long id_parcelle) {
        super();
        this.Nom_Ilot = nom_Ilot;
        this.Numero_ilot = numero_ilot;
        this.Nom_Parcelle = nom_Parcelle;
        this.NUMERO_PARCELLE = numero_parcelle;
        this.ID_parcelle = id_parcelle;
        this.ID_ilot = id_ilot;
    }



    public String getNom_Ilot() {
        return Nom_Ilot;
    }

    public void setNom_Ilot(String nom_Ilot) {
        this.Nom_Ilot = nom_Ilot;
    }

    public String getNumero_ilot() {
        return Numero_ilot;
    }

    public void setNumero_ilot(String numero_ilot) {
        this.Numero_ilot = numero_ilot;
    }

    public String getNom_Parcelle() {
        return Nom_Parcelle;
    }

    public void setNom_Parcelle(String nom_Parcelle) {
        this.Nom_Parcelle = nom_Parcelle;
    }

    public String getNUMERO_PARCELLE() {
        return NUMERO_PARCELLE;
    }

    public void setNUMERO_PARCELLE(String NUMERO_PARCELLE) {
        this.NUMERO_PARCELLE = NUMERO_PARCELLE;
    }

    public void setID_parcelle(long ID_parcelle) {
        this.ID_parcelle = ID_parcelle;
    }
    public long getID_parcelle() {
        return ID_parcelle;
    }

    public void setID_ilot(long ID_ilot) {
        this.ID_ilot = ID_ilot;
    }
    public long getID_ilot() {
        return ID_ilot;
    }
}
