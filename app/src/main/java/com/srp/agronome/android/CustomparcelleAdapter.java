package com.srp.agronome.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;


import java.util.ArrayList;

import static com.srp.agronome.android.MenuParcelle.Id_Parcelle_selected;
import static com.srp.agronome.android.MenuParcelle.Id_ilot_selected;


public class CustomparcelleAdapter extends ArrayAdapter<ParcelleClass> {


    public static String Nom_ilot;
    public static String Numero_ilot;
    public static String Nom_Parcelle;
    public static String Numero_Parcelle;

    Context context;
    int layoutResourceId;
    ArrayList<ParcelleClass> parcelledata = new ArrayList<ParcelleClass>();
    private ArrayList<ParcelleClass> filteredList;


    public CustomparcelleAdapter(Context context, int layoutResourceId, ArrayList<ParcelleClass> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.parcelledata = data;
        this.filteredList = data;
        getFilter();
    }
    //counts the total number of elements from the arrayList.
    public int getCount() {
        return parcelledata.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onItemselected(int position) {

    }




    public class Holder {
        MyTextView Nom_Ilot;
        MyTextView Numero_ilot;
        MyTextView Nom_Parcelle;
        MyTextView NUMERO_PARCELLE;
        ImageButton btn;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder holder = new Holder();

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);


           final ParcelleClass parcelle = parcelledata.get(position);

            holder.Nom_Ilot = (MyTextView) row.findViewById(R.id.Nom_Ilot);
            holder.Numero_ilot = (MyTextView) row.findViewById(R.id.Numero_ilot);
            holder.Nom_Parcelle = (MyTextView) row.findViewById(R.id.Nom_Parcelle);
            holder.NUMERO_PARCELLE = (MyTextView) row.findViewById(R.id.NUMERO_PARCELLE);
            holder.btn = (ImageButton) row.findViewById(R.id.btn_voir_parcelle);

            Typeface Regular = Typeface.createFromAsset(getContext().getAssets(), "Exo/Exo-Regular.ttf");
            Typeface Thin = Typeface.createFromAsset(getContext().getAssets(), "Exo/Exo-Thin.ttf");

            holder.Nom_Ilot.setText(parcelle.getNom_Ilot());
            holder.Nom_Ilot.setTypeface(Regular);

            holder.Numero_ilot.setText(parcelle.getNumero_ilot());
            holder.Numero_ilot.setTypeface(Regular);

            holder.Nom_Parcelle.setText(parcelle.getNom_Parcelle());
            holder.Nom_Parcelle.setTypeface(Regular);

            holder.NUMERO_PARCELLE.setText(parcelle.getNUMERO_PARCELLE());
            holder.NUMERO_PARCELLE.setTypeface(Regular);



            holder.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Nom_ilot= parcelle.getNom_Ilot();
                  //  Log.i("CheckValue", "Nom_ILOT : " + Nom_ilot ) ;
                    Numero_ilot = parcelle.getNumero_ilot();
                    Nom_Parcelle = parcelle.getNom_Parcelle();
                  //  Log.i("CheckValue", "Nom_Parcelle : " + Nom_Parcelle ) ;
                    Numero_Parcelle = parcelle.getNUMERO_PARCELLE();
                    Id_ilot_selected = parcelle.getID_ilot();
                   // Log.i("CheckValue", "Value of ID_ILOT : " + Id_ilot_selected ) ;
                    Id_Parcelle_selected = parcelle.getID_parcelle();
                   // Log.i("CheckValue", "Value of ID_Parcelle : " + Id_Parcelle_selected ) ;
                    Intent next = new Intent(context, ModificationParcelle.class);
                    ((Activity) context).finish();
                    context.startActivity(next);
                }
            });
}
        return row;
    }
}