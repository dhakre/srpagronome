package com.srp.agronome.android;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.srp.agronome.android.db.Attitude_Entreprise_Visite;
import com.srp.agronome.android.db.Attitude_Entreprise_VisiteDao;
import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Interet_Visite;
import com.srp.agronome.android.db.Interet_VisiteDao;
import com.srp.agronome.android.db.Objet_Visite;
import com.srp.agronome.android.db.Objet_VisiteDao;
import com.srp.agronome.android.db.Sujet_Visite_Principal;
import com.srp.agronome.android.db.Sujet_Visite_PrincipalDao;
import com.srp.agronome.android.db.Sujet_Visite_Secondaire;
import com.srp.agronome.android.db.Sujet_Visite_SecondaireDao;
import com.srp.agronome.android.db.Visite;
import com.srp.agronome.android.db.VisiteDao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuVisite.Id_Visite_Selected;

/**
 * Created by jitendra on 07-09-2017.
 */

public class EditVisite  extends AppCompatActivity {
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";
    private Spinner SpinnerAttitudeEntreprise = null;
    private Spinner  SpinnerInteretVisite = null;
    private Spinner SpinnerObjetVisite = null;
    private Button BtnValidate = null;
    private ImageButton imgBtnHome = null;
    private EditText Prinicplesujet_Discussion   = null;
    private EditText commentVisite   = null;
    private final static String KeyPrinicplesujet_Discussion  = "Prinicplesujet_Discussion ";
    private final static String KeyComment  = "Comment_Visite ";

    private EditText Secondairesujet_Discussion   = null;
    private final static String KeySecondairesujet_Discussion  = "Secondairesujet_Discussion ";


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_visite); //Add layout here
        FindWidgetViewbyId(); //Associate Widgets

        CreateListIntoDatabase();
        LoadSpinnerAttitudeEntreprise (SpinnerAttitudeEntreprise , getString(R.string.TextAttitudeEntreprise));

        LoadSpinnerInteretVisite (SpinnerInteretVisite , getString(R.string.TextInteretVisite));

        LoadSpinnerObjetVisite (SpinnerObjetVisite ,getString(R.string.TextObjetVisite), getString(R.string.TextanotherObjetVisite),
                getString(R.string.TextValidateObjetVisite), getString(R.string.TextInputObjetVisite));

        BtnValidate = (Button) findViewById(R.id.btnvalidate_CQ);
        BtnValidate.setOnClickListener(ButtonValidateHandler);

        imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_createvisite);
        imgBtnHome.setOnClickListener(ButtonbackHandler);
        Visite visite_loaded = getVisiteV2(Id_Visite_Selected);
        setDataVisiteToWidget(visite_loaded);

    }
    //Bouton back Toolbar
    View.OnClickListener ButtonbackHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };
    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditVisite.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(EditVisite.this, ListViste.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(EditVisite.this, ListViste.class);
                finish();
                startActivity(intent);
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }
    private void FindWidgetViewbyId() {
        Prinicplesujet_Discussion =(EditText) findViewById(R.id.Prinicple_sujet_Discussion);
        Secondairesujet_Discussion =(EditText) findViewById(R.id.Secondaire_sujet_Discussion);
        commentVisite=(EditText) findViewById(R.id.commentVisite);

        SpinnerAttitudeEntreprise = (Spinner) findViewById(R.id.Spinner_AttitudeEntreprise);
        SpinnerInteretVisite = (Spinner) findViewById(R.id.Spinner_InteretVisite);
        SpinnerObjetVisite = (Spinner) findViewById(R.id.Spinner_ObjetVisite);

    }
    // Input : PARCELLE objects
    private void setDataVisiteToWidget(Visite visite_selected){
        //Example EditText
        Prinicplesujet_Discussion.setText(visite_selected.getSujet_Visite_Principal().getDiscussion_Visite().toString().trim());
       // Secondairesujet_Discussion.setText(visite_selected.getSujet_Visite_SecondaireList().getClass().getSimpleName());
        commentVisite.setText(visite_selected.getComment());


        //Example Spinner
        SpinnerAdapter TextAdapterAttitudeEntreprise = SpinnerAttitudeEntreprise.getAdapter();
        for (int i = 0; i < TextAdapterAttitudeEntreprise.getCount(); i++) {
            if (TextAdapterAttitudeEntreprise.getItem(i).toString().equals(visite_selected.getAttitude_Entreprise_Visite().getNom())) {
                SpinnerAttitudeEntreprise.setSelection(i);
            }
        }

        //Spinner ETAT_stockeur
        SpinnerAdapter TextAdapterSpinnerInteretVisite = SpinnerInteretVisite.getAdapter();
        for (int i = 0; i < TextAdapterSpinnerInteretVisite.getCount(); i++) {
            if (TextAdapterSpinnerInteretVisite.getItem(i).toString().equals(visite_selected.getInteret_Visite().getNom())) {
                SpinnerInteretVisite.setSelection(i);
            }
        }

        //Spinner IRRIGATION
        SpinnerAdapter TextAdapterSpinnerObjetVisite = SpinnerObjetVisite.getAdapter();
        for (int i = 0; i < TextAdapterSpinnerObjetVisite.getCount(); i++) {
            if (TextAdapterSpinnerObjetVisite.getItem(i).toString().equals(visite_selected.getSujet_Visite_Principal().getObjet_Visite().getNom())) {
                SpinnerObjetVisite.setSelection(i);
            }
        }

    }
    // Method for creating the selection lists in the database
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Attitude_Entreprise_VisiteDao attitude_entreprise_data = daoSession.getAttitude_Entreprise_VisiteDao();
        Interet_VisiteDao interet_visite_data = daoSession.getInteret_VisiteDao();
        Objet_VisiteDao objet_visite_data = daoSession.getObjet_VisiteDao();

        List<Attitude_Entreprise_Visite> List_Attitude_Entreprise = attitude_entreprise_data.loadAll();
        if (List_Attitude_Entreprise.size() == 0) {
            Attitude_Entreprise_Visite Attitude_Entreprise_input_1 = new Attitude_Entreprise_Visite();
            Attitude_Entreprise_input_1.setNom("1");
            attitude_entreprise_data.insertOrReplace(Attitude_Entreprise_input_1);

            Attitude_Entreprise_Visite Attitude_Entreprise_input_2 = new Attitude_Entreprise_Visite();
            Attitude_Entreprise_input_2.setNom("2");
            attitude_entreprise_data.insertOrReplace(Attitude_Entreprise_input_2);

            Attitude_Entreprise_Visite Attitude_Entreprise_input_3 = new Attitude_Entreprise_Visite();
            Attitude_Entreprise_input_3.setNom("3");
            attitude_entreprise_data.insertOrReplace(Attitude_Entreprise_input_3);}

        List<Interet_Visite> List_Interet_Visite = interet_visite_data.loadAll();
        if (List_Interet_Visite.size() == 0) {

            Interet_Visite Interet_Visite_input_1 = new Interet_Visite();
            Interet_Visite_input_1.setNom("Intéressé");
            interet_visite_data.insertOrReplace(Interet_Visite_input_1);

            Interet_Visite Interet_Visite_input_2 = new Interet_Visite();
            Interet_Visite_input_2.setNom("Pas intéressé");
            interet_visite_data.insertOrReplace(Interet_Visite_input_2);}

        List<Objet_Visite> List_Objet_Visite = objet_visite_data.loadAll();
        if (List_Objet_Visite.size() == 0) {
            Objet_Visite Objet_Visite_input_1 = new Objet_Visite();
            Objet_Visite_input_1.setNom("Prospection");
            objet_visite_data.insertOrReplace(Objet_Visite_input_1);
        }
    }

    private void LoadSpinnerAttitudeEntreprise (Spinner spinner , String TextHint){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Attitude_Entreprise_VisiteDao Attitude_Entreprise_data = daoSession.getAttitude_Entreprise_VisiteDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Load items from the list
        List<Attitude_Entreprise_Visite> List_Attitude_Entreprise = Attitude_Entreprise_data.loadAll();
        if (List_Attitude_Entreprise != null) {
            for (Attitude_Entreprise_Visite AES : List_Attitude_Entreprise) {
                adapter.add(AES.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
    }

    private void LoadSpinnerInteretVisite (Spinner spinner , String TextHint){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Interet_VisiteDao Interet_Visite_data = daoSession.getInteret_VisiteDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Load items from the list
        List<Interet_Visite> List_Interet_Visite = Interet_Visite_data.loadAll();
        if (List_Interet_Visite != null) {
            for (Interet_Visite IV : List_Interet_Visite) {
                adapter.add(IV.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
    }

    private void LoadSpinnerObjetVisite (Spinner spinner , String TextHint , String TextAnother, final String TitleAddNew, final String TextAddNew){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Objet_VisiteDao Objet_Visite_data = daoSession.getObjet_VisiteDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(TextAnother);
        //Load items from the list
        List<Objet_Visite> List_Objet_Visite = Objet_Visite_data.loadAll();
        if (List_Objet_Visite != null) {
            for (Objet_Visite OV : List_Objet_Visite) {
                adapter.add(OV.getNom()); //Ajouter les éléments
            }
        }

        adapter.add(TextHint); //This is the text that will be displayed as hint.
        spinner.setAdapter(adapter);  //pay attention
        spinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        // Add new Objet Visite
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                if (position == 0 ){ //Cas où l'on ajoute une nouveau
                    Show_Dialog_EditText(TitleAddNew,TextAddNew,1);
                }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Créer une boite de dialogue avec un editText et retourne une chaine de caractere
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditVisite.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code TYPE_STOCKEUR
                        addObjetVisite(NewValueList);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    private void addObjetVisite(String Nom) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Objet_VisiteDao Objet_Visite_data = daoSession.getObjet_VisiteDao();
        Objet_Visite objet_visite_input = new Objet_Visite();
        objet_visite_input.setNom(Nom);
        Objet_Visite_data.insertOrReplace(objet_visite_input);
        LoadSpinnerObjetVisite (SpinnerObjetVisite ,getString(R.string.TextObjetVisite), getString(R.string.TextanotherObjetVisite),
                getString(R.string.TextValidateObjetVisite), getString(R.string.TextInputObjetVisite));
    }

    // Method to check if the user has entered the data

    private Boolean CheckInput(){
        boolean b = true;

        //Vérification des listes de choix :
        if (SpinnerAttitudeEntreprise.getSelectedItem().toString().equals(getString(R.string.TextAttitudeEntreprise))) {
            b = false;
        }
        if (SpinnerInteretVisite.getSelectedItem().toString().equals(getString(R.string.TextInteretVisite))) {
            b = false;
        }
        if (SpinnerObjetVisite.getSelectedItem().toString().equals(getString(R.string.TextObjetVisite))) {
            b = false;
        }

        return b;
    }

    //A method that creates a string listing the data entered.
    private String InputToText(){
        String text = getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.Prinicple_sujet_Discussion) + " : " + Prinicplesujet_Discussion.getText().toString() + "\n";
        text += getString(R.string.Prinicple_sujet_Discussion) + " : " + commentVisite.getText().toString() + "\n";
        text += getString(R.string.Secondaire_sujet_Discussion) + " : " +Secondairesujet_Discussion.getText().toString() + "\n";
        text += getString(R.string.TextAttitudeEntreprise) + " : " + SpinnerAttitudeEntreprise.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextInteretVisite) + " : " + SpinnerInteretVisite.getSelectedItem().toString() + "\n\n";
        text += getString(R.string.TextObjetVisite) + " : " + SpinnerObjetVisite.getSelectedItem().toString() + "\n\n";

        return text;
    }
    //Bouton Valider Toolbar
    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {

            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
            }
        } };


    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditVisite.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                // clearData();
                // create stockage into db
                LoadingDialogeditvisiteDB();
                DisplayCreatevisiteInLog();
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void DisplayCreatevisiteInLog(){
        //Get the Organization
        Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);

        Log.i("CheckBD","entreprise_selected"+    entreprise_selected.getRaison_sociale());
        List <Visite> list_visite = getListVisite(entreprise_selected);
        if (list_visite != null){
            for (Visite visite : list_visite){
            }}}

    public void LoadingDialogeditvisiteDB(){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(EditVisite.this,
                getString(R.string.TitleChargement), getString(R.string.TextChargementStockeur), true);
        ringProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                  //  Load data from databse
                    Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
                    List <Visite> list_visite = getListVisite(entreprise_selected);
                                   Visite    visite_input =  getVisite(Id_Visite_Selected,list_visite);
                    //update data into databse
                    Visite update_visit= UpdateVisiteIntoDatabase(visite_input,SpinnerAttitudeEntreprise.getSelectedItem().toString() ,SpinnerInteretVisite.getSelectedItem().toString());
                    UpdateSujetVisitePrincipalToVisite(visite_input,Prinicplesujet_Discussion.getText().toString().trim(), SpinnerObjetVisite.getSelectedItem().toString());
                    addSujetVisiteSecondaireToVisite(update_visit,Secondairesujet_Discussion.getText().toString().trim(), SpinnerObjetVisite.getSelectedItem().toString());
                } catch (Exception e) {
                }
                ringProgressDialog.dismiss();
                Intent intent = new Intent(EditVisite.this, MenuVisite.class);
                finish();
                startActivity(intent);
            }
        }).start();
    }
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditVisite.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
    }
// BD for Editvisite

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }
//Get the list of visit in a Organization
private List<Visite> getListVisite(Entreprise entreprise_input){
    return entreprise_input.getVisiteList();
}

    //Get a visit in a list of visit
    private Visite getVisite(long ID_Visite, List <Visite> list_visite){
        Visite result = null;
        if (list_visite != null){
            for (Visite V : list_visite){
                if (V.getId() == ID_Visite){
                    result = V;
                }
            }
        }
        return result;
    }

    //Get the visite only with the ID
    private Visite getVisiteV2(long ID_Visite){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        VisiteDao visite_data = daoSession.getVisiteDao();
        Visite result = null;
        List<Visite> visite_load = visite_data.queryBuilder()
                .where(VisiteDao.Properties.Id.eq(ID_Visite))
                .list();
        if (visite_load != null) {
            for (Visite V: visite_load) {
                result = V;
            }
        }
        return result;
    }


    //Add Sujet Visite Secondaire to Visite
    private void addSujetVisiteSecondaireToVisite(Visite visite_input,String Discussion , String Objet_Visite){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        VisiteDao visite_data = daoSession.getVisiteDao();
        Objet_VisiteDao objet_visite_data = daoSession.getObjet_VisiteDao();
        Sujet_Visite_SecondaireDao sujet_visite_s_data = daoSession.getSujet_Visite_SecondaireDao();

        Sujet_Visite_Secondaire sujet_visite_s_input = new Sujet_Visite_Secondaire();
        sujet_visite_s_input.setDiscussion_Visite(Discussion);

        //Ajout Objet Visite
        List<Objet_Visite> List_Objet_Visite = objet_visite_data.queryBuilder()
                .where(Objet_VisiteDao.Properties.Nom.eq(Objet_Visite))
                .list();
        if (List_Objet_Visite != null) {
            for (Objet_Visite OB: List_Objet_Visite) {
                sujet_visite_s_input.setObjet_Visite(OB);
                sujet_visite_s_input.setID_Objet_Visite(OB.getId());
            }
        }
        sujet_visite_s_input.setID_Visite(visite_input.getId());
        sujet_visite_s_data.insertOrReplace(sujet_visite_s_input);
        visite_input.resetSujet_Visite_SecondaireList();
        visite_data.update(visite_input);
    }

    private Visite UpdateVisiteIntoDatabase(Visite visite_input,String Attitude_Entreprise ,String Interet_Entreprise){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        VisiteDao visite_data = daoSession.getVisiteDao();
        Attitude_Entreprise_VisiteDao attitude_entreprise_data = daoSession.getAttitude_Entreprise_VisiteDao();
        Interet_VisiteDao interet_visite_data = daoSession.getInteret_VisiteDao();
        Sujet_Visite_SecondaireDao sujet_visite_s_data = daoSession.getSujet_Visite_SecondaireDao();

        visite_input.setDate_modification(Today_Date());
        visite_input.setID_Agronome_Modification((int)ID_Compte_Selected);
        visite_input.setComment(commentVisite.getText().toString());  //update comment

        //Ajout Attitude Entreprise
        List<Attitude_Entreprise_Visite> List_Attitude_Visite = attitude_entreprise_data.queryBuilder()
                .where(Attitude_Entreprise_VisiteDao.Properties.Nom.eq(Attitude_Entreprise))
                .list();
        if (List_Attitude_Visite != null) {
            for (Attitude_Entreprise_Visite AES : List_Attitude_Visite) {
                visite_input.setAttitude_Entreprise_Visite(AES);
                visite_input.setID_Attitude_Entreprise(AES.getId());
            }
        }
        //Ajout Interet Entreprise
        List<Interet_Visite> List_Interet_Visite = interet_visite_data.queryBuilder()
                .where(Interet_VisiteDao.Properties.Nom.eq(Interet_Entreprise))
                .list();
        if (List_Interet_Visite != null) {
            for (Interet_Visite IV: List_Interet_Visite) {
                visite_input.setInteret_Visite(IV);
                visite_input.setID_Interet_Visite(IV.getId());
            }
        }

        //Clear List_Sujet_Secondaire
        List <Sujet_Visite_Secondaire> List_Sujet_Visite_Secondaire = visite_input.getSujet_Visite_SecondaireList();
        for (Sujet_Visite_Secondaire SVS : List_Sujet_Visite_Secondaire) {
            SVS.setID_Visite(null);
            sujet_visite_s_data.update(SVS);
        }
        visite_input.resetSujet_Visite_SecondaireList();
        visite_data.update(visite_input);
        return visite_input;
    }

    private void UpdateSujetVisitePrincipalToVisite(Visite visite_input,String Discussion , String Objet_Visite){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        VisiteDao visite_data = daoSession.getVisiteDao();
        Objet_VisiteDao objet_visite_data = daoSession.getObjet_VisiteDao();
        Sujet_Visite_PrincipalDao sujet_visite_p_data = daoSession.getSujet_Visite_PrincipalDao();
        Sujet_Visite_Principal sujet_visite_p_input = visite_input.getSujet_Visite_Principal();

        sujet_visite_p_input.setDiscussion_Visite(Discussion);
        sujet_visite_p_input.getObjet_Visite().setNom(Objet_Visite);
        objet_visite_data.update(sujet_visite_p_input.getObjet_Visite());
        sujet_visite_p_data.update(sujet_visite_p_input);
        visite_data.update(visite_input);
    }

    //Return an Organization
    private Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }
}
