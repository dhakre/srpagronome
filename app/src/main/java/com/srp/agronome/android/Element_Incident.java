package com.srp.agronome.android;

public class Element_Incident {
    private String objet_incident = "";
    private String texte_incident = "";
    private String date_incident = "";


    public void setObjetIncident(String objet) {
        this.objet_incident = objet;
    }
    public String getObjetIncident() {
        return objet_incident;
    }


    public void setTexteIncident(String texte) {
        this.texte_incident = texte;
    }
    public String getTexteIncident() {
        return texte_incident;
    }


    public void setDateIncident(String date) {
        this.date_incident = date;
    }
    public String getDateIncident() {
        return date_incident;
    }
}