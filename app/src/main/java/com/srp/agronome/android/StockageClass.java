package com.srp.agronome.android;

/**
 * Created by jitendra on 01-09-2017.
 */

public class StockageClass {

    private String NOM_STOCKEUR;
    private String IDENTIFIANT_STOCKEUR;
    private String CAPACITE;
    private Long ID_stockage;


       public StockageClass(String nom_stockeur, String identifiant_stockeur, String capacite,Long id_stockage) {
        super();
        this.NOM_STOCKEUR = nom_stockeur;
        this.IDENTIFIANT_STOCKEUR = identifiant_stockeur;
        this.CAPACITE = capacite;
        this.ID_stockage = id_stockage;
    }


    public String getNOM_STOCKEUR() {
        return NOM_STOCKEUR;
    }

    public void setNOM_STOCKEUR(String nom_stockeur) {
        this.NOM_STOCKEUR = nom_stockeur;
    }

    public String getIDENTIFIANT_STOCKEUR() {
        return IDENTIFIANT_STOCKEUR;
    }

    public void setNumero_ilot(String identifiant_stockeur) {
        this.IDENTIFIANT_STOCKEUR = identifiant_stockeur;
    }

    public String getCAPACITE() {
        return CAPACITE;
    }

    public void setCAPACITE(String capacite) {
        this.CAPACITE = capacite;
    }


    public void setId_stockage(long id_stockage) {
        this.ID_stockage = id_stockage;
    }
    public long getid_stockage() {return ID_stockage;}


}
