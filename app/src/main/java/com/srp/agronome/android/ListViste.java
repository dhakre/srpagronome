package com.srp.agronome.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Entreprise;
import com.srp.agronome.android.db.Objet_Visite;
import com.srp.agronome.android.db.Objet_VisiteDao;
import com.srp.agronome.android.db.STOCKAGE;
import com.srp.agronome.android.db.SUIVI_PRODUCTEUR;
import com.srp.agronome.android.db.Sujet_Visite_Principal;
import com.srp.agronome.android.db.Visite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MenuVisite.Id_Visite_Selected;
/**
 * Created by jitendra on 07-09-2017.
 */

public class ListViste extends AppCompatActivity {
    private ImageButton btnRetour =null;
    private ListView listView_visite;
    public static DaoSession DAOSESSION;
    private ArrayList<VisiteClass> arrayListvisite = new ArrayList<VisiteClass>();

    CustomVisitAdapter visiteAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_visit);
        Loadlistvisit();
        btnRetour = (ImageButton) findViewById(R.id.arrow_back);
        btnRetour.setOnClickListener(ButtonRetourHandler);
        listView_visite = (ListView) findViewById(R.id.listview_visite); // see layout  activity_list_visit

        // go to  layout liste_visite.xml to display the CustomVisitAdapter.java value
        visiteAdapter = new CustomVisitAdapter(this, R.layout.liste_visite, arrayListvisite);

        listView_visite.setAdapter(visiteAdapter);
        listView_visite.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                visiteAdapter.onItemselected(arg2);
            }
        });

        DAOSESSION=((AppController) getApplication()).getDaoSession();

    }

    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(ListViste.this, MenuVisite.class);
            finish();
            startActivity(intent);
        }
    };


    public void Loadlistvisit() {
        //Get the Organization
        Entreprise entreprise_selected = getOrganization(ID_Compte_Selected,ID_Entreprise_Selected);
        String entrepriseselected =entreprise_selected.getRaison_sociale();
        List <Visite> list_visite = getListVisite(entreprise_selected);
       // List<Sujet_Visite_Principal> sujetPrincipalList=getSujetPrincipal(list_visite);
        Log.i("CheckValue", "list_visite  : " + list_visite ) ;
            for (Visite visite : list_visite) {
                // int archive=GlobalValues.getInstance().getGlobalArchive();
                try {
                    if (visite.getArchive() == GlobalValues.getInstance().getGlobalArchive())        //chnaging the value 1 to the global value
                        arrayListvisite.add(new VisiteClass(
                                "Raison sociale : " + entrepriseselected,
                                visite.getId(),
                                "Objet visite :" + visite.getSujet_Visite_Principal().getObjet_Visite().getNom().toString(),
                                "Subject Principal :"+visite.getSujet_Visite_Principal().getDiscussion_Visite().toString(),
                                "Comment:"  + visite.getComment()

                        ));
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
    }

    /*public  String getPrincipal(Sujet_Visite_Principal principal)
    {
       // return (principal.getDiscussion_Visite().get)
    }*/



//DB to access
    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    //Return an Organization
    public Entreprise getOrganization(long Id_Compte, long Id_Entreprise) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        Entreprise entreprise_result = null;
        List<Compte> List_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(Id_Compte))
                .list();
        if (List_compte != null) {
            for (Compte c : List_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList();
                for (Entreprise e : List_Entreprise) {
                    if (e.getId() == Id_Entreprise){
                        entreprise_result = e;
                    }
                }
            }
        }
        return entreprise_result;
    }

    //Get the list of visit in a Organization
    public List<Visite> getListVisite(Entreprise entreprise_input){
        return entreprise_input.getVisiteList();
    }
    //get principal
    public List<Sujet_Visite_Principal> getSujetPrincipal(List<Visite> visite_input)
    {
        List<Sujet_Visite_Principal> principalList = null;
      for(Visite v:visite_input)
      {
          principalList.add(v.getSujet_Visite_Principal());
      }
      return principalList;
    }
    //Get a visit in a list of visit
    public Visite getVisite(long ID_Visite, List <Visite> list_visite){
        Visite result = null;
        if (list_visite != null){
            for (Visite V : list_visite){
                if (V.getId() == ID_Visite){
                    result = V;
                }
            }
        }
        return result;
    }

}
