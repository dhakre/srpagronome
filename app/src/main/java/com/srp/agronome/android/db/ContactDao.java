package com.srp.agronome.android.db;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.SqlUtils;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "CONTACT".
*/
public class ContactDao extends AbstractDao<Contact, Long> {

    public static final String TABLENAME = "CONTACT";

    /**
     * Properties of entity Contact.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Remarque_contact = new Property(1, String.class, "remarque_contact", false, "REMARQUE_CONTACT");
        public final static Property Contact_identifiant = new Property(2, String.class, "contact_identifiant", false, "CONTACT_IDENTIFIANT");
        public final static Property Archive = new Property(3, int.class, "archive", false, "ARCHIVE");
        public final static Property ID_Agronome_Creation = new Property(4, int.class, "ID_Agronome_Creation", false, "ID__AGRONOME__CREATION");
        public final static Property Date_creation = new Property(5, java.util.Date.class, "date_creation", false, "DATE_CREATION");
        public final static Property ID_Agronome_Modification = new Property(6, int.class, "ID_Agronome_Modification", false, "ID__AGRONOME__MODIFICATION");
        public final static Property Date_modification = new Property(7, java.util.Date.class, "date_modification", false, "DATE_MODIFICATION");
        public final static Property ID_Identite_Contact = new Property(8, Long.class, "ID_Identite_Contact", false, "ID__IDENTITE__CONTACT");
        public final static Property ID_Categorie_Contact = new Property(9, Long.class, "ID_Categorie_Contact", false, "ID__CATEGORIE__CONTACT");
        public final static Property ID_Metier = new Property(10, Long.class, "ID_Metier", false, "ID__METIER");
        public final static Property ID_Sociabilite = new Property(11, Long.class, "ID_Sociabilite", false, "ID__SOCIABILITE");
        public final static Property ID_Entreprise = new Property(12, long.class, "ID_Entreprise", false, "ID__ENTREPRISE");
    }

    private DaoSession daoSession;

    private Query<Contact> entreprise_ContactListQuery;

    public ContactDao(DaoConfig config) {
        super(config);
    }
    
    public ContactDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"CONTACT\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"REMARQUE_CONTACT\" TEXT," + // 1: remarque_contact
                "\"CONTACT_IDENTIFIANT\" TEXT," + // 2: contact_identifiant
                "\"ARCHIVE\" INTEGER NOT NULL ," + // 3: archive
                "\"ID__AGRONOME__CREATION\" INTEGER NOT NULL ," + // 4: ID_Agronome_Creation
                "\"DATE_CREATION\" INTEGER NOT NULL ," + // 5: date_creation
                "\"ID__AGRONOME__MODIFICATION\" INTEGER NOT NULL ," + // 6: ID_Agronome_Modification
                "\"DATE_MODIFICATION\" INTEGER NOT NULL ," + // 7: date_modification
                "\"ID__IDENTITE__CONTACT\" INTEGER," + // 8: ID_Identite_Contact
                "\"ID__CATEGORIE__CONTACT\" INTEGER," + // 9: ID_Categorie_Contact
                "\"ID__METIER\" INTEGER," + // 10: ID_Metier
                "\"ID__SOCIABILITE\" INTEGER," + // 11: ID_Sociabilite
                "\"ID__ENTREPRISE\" INTEGER NOT NULL );"); // 12: ID_Entreprise
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"CONTACT\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Contact entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String remarque_contact = entity.getRemarque_contact();
        if (remarque_contact != null) {
            stmt.bindString(2, remarque_contact);
        }
 
        String contact_identifiant = entity.getContact_identifiant();
        if (contact_identifiant != null) {
            stmt.bindString(3, contact_identifiant);
        }
        stmt.bindLong(4, entity.getArchive());
        stmt.bindLong(5, entity.getID_Agronome_Creation());
        stmt.bindLong(6, entity.getDate_creation().getTime());
        stmt.bindLong(7, entity.getID_Agronome_Modification());
        stmt.bindLong(8, entity.getDate_modification().getTime());
 
        Long ID_Identite_Contact = entity.getID_Identite_Contact();
        if (ID_Identite_Contact != null) {
            stmt.bindLong(9, ID_Identite_Contact);
        }
 
        Long ID_Categorie_Contact = entity.getID_Categorie_Contact();
        if (ID_Categorie_Contact != null) {
            stmt.bindLong(10, ID_Categorie_Contact);
        }
 
        Long ID_Metier = entity.getID_Metier();
        if (ID_Metier != null) {
            stmt.bindLong(11, ID_Metier);
        }
 
        Long ID_Sociabilite = entity.getID_Sociabilite();
        if (ID_Sociabilite != null) {
            stmt.bindLong(12, ID_Sociabilite);
        }
        stmt.bindLong(13, entity.getID_Entreprise());
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Contact entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String remarque_contact = entity.getRemarque_contact();
        if (remarque_contact != null) {
            stmt.bindString(2, remarque_contact);
        }
 
        String contact_identifiant = entity.getContact_identifiant();
        if (contact_identifiant != null) {
            stmt.bindString(3, contact_identifiant);
        }
        stmt.bindLong(4, entity.getArchive());
        stmt.bindLong(5, entity.getID_Agronome_Creation());
        stmt.bindLong(6, entity.getDate_creation().getTime());
        stmt.bindLong(7, entity.getID_Agronome_Modification());
        stmt.bindLong(8, entity.getDate_modification().getTime());
 
        Long ID_Identite_Contact = entity.getID_Identite_Contact();
        if (ID_Identite_Contact != null) {
            stmt.bindLong(9, ID_Identite_Contact);
        }
 
        Long ID_Categorie_Contact = entity.getID_Categorie_Contact();
        if (ID_Categorie_Contact != null) {
            stmt.bindLong(10, ID_Categorie_Contact);
        }
 
        Long ID_Metier = entity.getID_Metier();
        if (ID_Metier != null) {
            stmt.bindLong(11, ID_Metier);
        }
 
        Long ID_Sociabilite = entity.getID_Sociabilite();
        if (ID_Sociabilite != null) {
            stmt.bindLong(12, ID_Sociabilite);
        }
        stmt.bindLong(13, entity.getID_Entreprise());
    }

    @Override
    protected final void attachEntity(Contact entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Contact readEntity(Cursor cursor, int offset) {
        Contact entity = new Contact( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // remarque_contact
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // contact_identifiant
            cursor.getInt(offset + 3), // archive
            cursor.getInt(offset + 4), // ID_Agronome_Creation
            new java.util.Date(cursor.getLong(offset + 5)), // date_creation
            cursor.getInt(offset + 6), // ID_Agronome_Modification
            new java.util.Date(cursor.getLong(offset + 7)), // date_modification
            cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8), // ID_Identite_Contact
            cursor.isNull(offset + 9) ? null : cursor.getLong(offset + 9), // ID_Categorie_Contact
            cursor.isNull(offset + 10) ? null : cursor.getLong(offset + 10), // ID_Metier
            cursor.isNull(offset + 11) ? null : cursor.getLong(offset + 11), // ID_Sociabilite
            cursor.getLong(offset + 12) // ID_Entreprise
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Contact entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setRemarque_contact(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setContact_identifiant(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setArchive(cursor.getInt(offset + 3));
        entity.setID_Agronome_Creation(cursor.getInt(offset + 4));
        entity.setDate_creation(new java.util.Date(cursor.getLong(offset + 5)));
        entity.setID_Agronome_Modification(cursor.getInt(offset + 6));
        entity.setDate_modification(new java.util.Date(cursor.getLong(offset + 7)));
        entity.setID_Identite_Contact(cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8));
        entity.setID_Categorie_Contact(cursor.isNull(offset + 9) ? null : cursor.getLong(offset + 9));
        entity.setID_Metier(cursor.isNull(offset + 10) ? null : cursor.getLong(offset + 10));
        entity.setID_Sociabilite(cursor.isNull(offset + 11) ? null : cursor.getLong(offset + 11));
        entity.setID_Entreprise(cursor.getLong(offset + 12));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Contact entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Contact entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Contact entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "contactList" to-many relationship of Entreprise. */
    public List<Contact> _queryEntreprise_ContactList(long ID_Entreprise) {
        synchronized (this) {
            if (entreprise_ContactListQuery == null) {
                QueryBuilder<Contact> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.ID_Entreprise.eq(null));
                entreprise_ContactListQuery = queryBuilder.build();
            }
        }
        Query<Contact> query = entreprise_ContactListQuery.forCurrentThread();
        query.setParameter(0, ID_Entreprise);
        return query.list();
    }

    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getIdentiteDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T1", daoSession.getCategorie_ContactDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T2", daoSession.getMetierDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T3", daoSession.getSociabiliteDao().getAllColumns());
            builder.append(" FROM CONTACT T");
            builder.append(" LEFT JOIN IDENTITE T0 ON T.\"ID__IDENTITE__CONTACT\"=T0.\"_id\"");
            builder.append(" LEFT JOIN CATEGORIE__CONTACT T1 ON T.\"ID__CATEGORIE__CONTACT\"=T1.\"_id\"");
            builder.append(" LEFT JOIN METIER T2 ON T.\"ID__METIER\"=T2.\"_id\"");
            builder.append(" LEFT JOIN SOCIABILITE T3 ON T.\"ID__SOCIABILITE\"=T3.\"_id\"");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected Contact loadCurrentDeep(Cursor cursor, boolean lock) {
        Contact entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        Identite identite = loadCurrentOther(daoSession.getIdentiteDao(), cursor, offset);
        entity.setIdentite(identite);
        offset += daoSession.getIdentiteDao().getAllColumns().length;

        Categorie_Contact categorie_Contact = loadCurrentOther(daoSession.getCategorie_ContactDao(), cursor, offset);
        entity.setCategorie_Contact(categorie_Contact);
        offset += daoSession.getCategorie_ContactDao().getAllColumns().length;

        Metier metier = loadCurrentOther(daoSession.getMetierDao(), cursor, offset);
        entity.setMetier(metier);
        offset += daoSession.getMetierDao().getAllColumns().length;

        Sociabilite sociabilite = loadCurrentOther(daoSession.getSociabiliteDao(), cursor, offset);
        entity.setSociabilite(sociabilite);

        return entity;    
    }

    public Contact loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<Contact> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<Contact> list = new ArrayList<Contact>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<Contact> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<Contact> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
