package com.srp.agronome.android.db;

import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "ILOT".
*/
public class ILOTDao extends AbstractDao<ILOT, Long> {

    public static final String TABLENAME = "ILOT";

    /**
     * Properties of entity ILOT.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property NOM_ILOT  = new Property(1, String.class, "NOM_ILOT ", false, "NOM__ILOT ");
        public final static Property NUMERO_ILOT  = new Property(2, String.class, "NUMERO_ILOT ", false, "NUMERO__ILOT ");
        public final static Property Archive = new Property(3, int.class, "archive", false, "ARCHIVE");
        public final static Property ID_Agronome_Creation = new Property(4, int.class, "ID_Agronome_Creation", false, "ID__AGRONOME__CREATION");
        public final static Property Date_creation = new Property(5, java.util.Date.class, "date_creation", false, "DATE_CREATION");
        public final static Property ID_Agronome_Modification = new Property(6, int.class, "ID_Agronome_Modification", false, "ID__AGRONOME__MODIFICATION");
        public final static Property Date_modification = new Property(7, java.util.Date.class, "date_modification", false, "DATE_MODIFICATION");
        public final static Property ID_SUIVI_PRODUCTEUR = new Property(8, Long.class, "ID_SUIVI_PRODUCTEUR", false, "ID__SUIVI__PRODUCTEUR");
    }

    private DaoSession daoSession;

    private Query<ILOT> sUIVI_PRODUCTEUR_ILOTListQuery;

    public ILOTDao(DaoConfig config) {
        super(config);
    }
    
    public ILOTDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"ILOT\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"NOM__ILOT \" TEXT," + // 1: NOM_ILOT 
                "\"NUMERO__ILOT \" TEXT," + // 2: NUMERO_ILOT 
                "\"ARCHIVE\" INTEGER NOT NULL ," + // 3: archive
                "\"ID__AGRONOME__CREATION\" INTEGER NOT NULL ," + // 4: ID_Agronome_Creation
                "\"DATE_CREATION\" INTEGER NOT NULL ," + // 5: date_creation
                "\"ID__AGRONOME__MODIFICATION\" INTEGER NOT NULL ," + // 6: ID_Agronome_Modification
                "\"DATE_MODIFICATION\" INTEGER NOT NULL ," + // 7: date_modification
                "\"ID__SUIVI__PRODUCTEUR\" INTEGER);"); // 8: ID_SUIVI_PRODUCTEUR
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"ILOT\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, ILOT entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String NOM_ILOT  = entity.getNOM_ILOT ();
        if (NOM_ILOT  != null) {
            stmt.bindString(2, NOM_ILOT );
        }
 
        String NUMERO_ILOT  = entity.getNUMERO_ILOT ();
        if (NUMERO_ILOT  != null) {
            stmt.bindString(3, NUMERO_ILOT );
        }
        stmt.bindLong(4, entity.getArchive());
        stmt.bindLong(5, entity.getID_Agronome_Creation());
        stmt.bindLong(6, entity.getDate_creation().getTime());
        stmt.bindLong(7, entity.getID_Agronome_Modification());
        stmt.bindLong(8, entity.getDate_modification().getTime());
 
        Long ID_SUIVI_PRODUCTEUR = entity.getID_SUIVI_PRODUCTEUR();
        if (ID_SUIVI_PRODUCTEUR != null) {
            stmt.bindLong(9, ID_SUIVI_PRODUCTEUR);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, ILOT entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String NOM_ILOT  = entity.getNOM_ILOT ();
        if (NOM_ILOT  != null) {
            stmt.bindString(2, NOM_ILOT );
        }
 
        String NUMERO_ILOT  = entity.getNUMERO_ILOT ();
        if (NUMERO_ILOT  != null) {
            stmt.bindString(3, NUMERO_ILOT );
        }
        stmt.bindLong(4, entity.getArchive());
        stmt.bindLong(5, entity.getID_Agronome_Creation());
        stmt.bindLong(6, entity.getDate_creation().getTime());
        stmt.bindLong(7, entity.getID_Agronome_Modification());
        stmt.bindLong(8, entity.getDate_modification().getTime());
 
        Long ID_SUIVI_PRODUCTEUR = entity.getID_SUIVI_PRODUCTEUR();
        if (ID_SUIVI_PRODUCTEUR != null) {
            stmt.bindLong(9, ID_SUIVI_PRODUCTEUR);
        }
    }

    @Override
    protected final void attachEntity(ILOT entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public ILOT readEntity(Cursor cursor, int offset) {
        ILOT entity = new ILOT( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // NOM_ILOT 
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // NUMERO_ILOT 
            cursor.getInt(offset + 3), // archive
            cursor.getInt(offset + 4), // ID_Agronome_Creation
            new java.util.Date(cursor.getLong(offset + 5)), // date_creation
            cursor.getInt(offset + 6), // ID_Agronome_Modification
            new java.util.Date(cursor.getLong(offset + 7)), // date_modification
            cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8) // ID_SUIVI_PRODUCTEUR
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, ILOT entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setNOM_ILOT (cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setNUMERO_ILOT (cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setArchive(cursor.getInt(offset + 3));
        entity.setID_Agronome_Creation(cursor.getInt(offset + 4));
        entity.setDate_creation(new java.util.Date(cursor.getLong(offset + 5)));
        entity.setID_Agronome_Modification(cursor.getInt(offset + 6));
        entity.setDate_modification(new java.util.Date(cursor.getLong(offset + 7)));
        entity.setID_SUIVI_PRODUCTEUR(cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(ILOT entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(ILOT entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(ILOT entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "iLOTList" to-many relationship of SUIVI_PRODUCTEUR. */
    public List<ILOT> _querySUIVI_PRODUCTEUR_ILOTList(Long ID_SUIVI_PRODUCTEUR) {
        synchronized (this) {
            if (sUIVI_PRODUCTEUR_ILOTListQuery == null) {
                QueryBuilder<ILOT> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.ID_SUIVI_PRODUCTEUR.eq(null));
                sUIVI_PRODUCTEUR_ILOTListQuery = queryBuilder.build();
            }
        }
        Query<ILOT> query = sUIVI_PRODUCTEUR_ILOTListQuery.forCurrentThread();
        query.setParameter(0, ID_SUIVI_PRODUCTEUR);
        return query.list();
    }

}
