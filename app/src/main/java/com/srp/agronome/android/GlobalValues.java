package com.srp.agronome.android;

import android.app.Application;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by sumitra on 10/4/2017.
 */

public class GlobalValues {

    private static GlobalValues globalinstance=null;

    //global archieve values as maximum archives values in table contact are 0
    private static int ARCHIVABLE=0;  //can be archived
    private static int ARCHIVE=1;
    private static int ARCHIVABLE_AVEC_VALIDATION_UTILISATEUR=2;

    protected GlobalValues(){}

    //

    public int getGlobalArchive()
    {
        return GlobalValues.ARCHIVE;
    }

    public int getGlobalArchivable()
    {
        return GlobalValues.ARCHIVABLE;
    }

    public int getGlobalArchiveAvecUtilisateur()
    {
        return GlobalValues.ARCHIVABLE_AVEC_VALIDATION_UTILISATEUR;
    }
     //
    public  void setGlobalArchives(int archive)
    {
          GlobalValues.ARCHIVE=archive;
    }

    public  void setGlobalArchivable(int archivable)
    {
        GlobalValues.ARCHIVABLE=archivable;
    }

    public  void setGlobalArchiveAvecutilisateur(int archiveUtilizateur)
    {
        GlobalValues.ARCHIVABLE_AVEC_VALIDATION_UTILISATEUR=archiveUtilizateur;
    }

    public static synchronized GlobalValues getInstance()
    {
        if(globalinstance==null)
        {
            globalinstance=new GlobalValues();
        }
        return globalinstance;
    }

    //fun to set name
    public static String setName(String name)
    {
        if(name.length()>15)
        {
            name=name.substring(0,16);
        }
        return name;
    }

    //function to set date
    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

}
