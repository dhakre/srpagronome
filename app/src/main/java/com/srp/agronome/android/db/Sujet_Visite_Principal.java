package com.srp.agronome.android.db;

import org.greenrobot.greendao.annotation.*;

import com.srp.agronome.android.db.DaoSession;
import org.greenrobot.greendao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "SUJET__VISITE__PRINCIPAL".
 */
@Entity(active = true)
public class Sujet_Visite_Principal {

    @Id
    private Long id;
    private String Discussion_Visite;
    private Long ID_Objet_Visite;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient Sujet_Visite_PrincipalDao myDao;

    @ToOne(joinProperty = "ID_Objet_Visite")
    private Objet_Visite objet_Visite;

    @Generated
    private transient Long objet_Visite__resolvedKey;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public Sujet_Visite_Principal() {
    }

    public Sujet_Visite_Principal(Long id) {
        this.id = id;
    }

    @Generated
    public Sujet_Visite_Principal(Long id, String Discussion_Visite, Long ID_Objet_Visite) {
        this.id = id;
        this.Discussion_Visite = Discussion_Visite;
        this.ID_Objet_Visite = ID_Objet_Visite;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getSujet_Visite_PrincipalDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiscussion_Visite() {
        return Discussion_Visite;
    }

    public void setDiscussion_Visite(String Discussion_Visite) {
        this.Discussion_Visite = Discussion_Visite;
    }

    public Long getID_Objet_Visite() {
        return ID_Objet_Visite;
    }

    public void setID_Objet_Visite(Long ID_Objet_Visite) {
        this.ID_Objet_Visite = ID_Objet_Visite;
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public Objet_Visite getObjet_Visite() {
        Long __key = this.ID_Objet_Visite;
        if (objet_Visite__resolvedKey == null || !objet_Visite__resolvedKey.equals(__key)) {
            __throwIfDetached();
            Objet_VisiteDao targetDao = daoSession.getObjet_VisiteDao();
            Objet_Visite objet_VisiteNew = targetDao.load(__key);
            synchronized (this) {
                objet_Visite = objet_VisiteNew;
            	objet_Visite__resolvedKey = __key;
            }
        }
        return objet_Visite;
    }

    @Generated
    public void setObjet_Visite(Objet_Visite objet_Visite) {
        synchronized (this) {
            this.objet_Visite = objet_Visite;
            ID_Objet_Visite = objet_Visite == null ? null : objet_Visite.getId();
            objet_Visite__resolvedKey = ID_Objet_Visite;
        }
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
