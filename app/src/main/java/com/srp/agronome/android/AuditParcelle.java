package com.srp.agronome.android;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.EditText;
import android.widget.ImageButton;

import android.widget.ScrollView;
import android.widget.Spinner;

import android.widget.TextView;


import com.srp.agronome.android.db.AUDITDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.ETAT_PLANTE;
import com.srp.agronome.android.db.ETAT_PLANTEDao;
import com.srp.agronome.android.db.ETAT_VEGETATIF;
import com.srp.agronome.android.db.ETAT_VEGETATIFDao;
import com.srp.agronome.android.db.LISTE_PLANTE;
import com.srp.agronome.android.db.LISTE_PLANTEDao;

import com.srp.agronome.android.db.PLANTE_NON_DESIRABLE;
import com.srp.agronome.android.db.PROPRETE;
import com.srp.agronome.android.db.PROPRETEDao;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static com.srp.agronome.android.MenuAudit.Audit_Id_Selected;
import static com.srp.agronome.android.Audit.AUDIT_TYPE;

/**
 * Created by Jitendra on 19/07/2017.
 */

public class AuditParcelle extends AppCompatActivity {

    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    private static final String preferenceValues = "PrefValues";


    private EditText Etat_vegetatif = null;
    private EditText Etat_plante = null;
    private EditText Liste_plante = null;
    private EditText proprete = null;
    private TextView Remarques = null;  //take from Audit table


    private static final int CAMERA_PIC_REQUEST = 2;
    private static final int GALLERY_PICK = 1;
    private static final int PICK_CROP = 3;



    private Spinner SpinnerETAT_VEGETATIF = null;
    //Rename the spinner name to avoid conflict with inseting list of data into databse

    private Spinner SpinnerETAT_PLANTE = null;
    private Spinner SpinnerLISTE_PLANTE = null;



    private Spinner Spinner_Proprete = null;

    private Button PrendrePhoto = null;
    private ImageButton imgBtnHome = null;
    private Button BtnValidate = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_parcelle);
        //set audit type
        AUDIT_TYPE="Parcelle";

        FindWidgetViewbyId(); //Associate Widgets


        //Liste de choix dans la base de données
        CreateListIntoDatabase();
        LoadSpinnerETAT_VEGETATIF();
        LoadSpinnerETAT_PLANTE();
        LoadSpinnerLISTE_PLANTE();

        Remarques= (TextView) findViewById(R.id.auditRemarque);  //get data from audit


        Spinner_Proprete = (Spinner) findViewById(R.id.spinnerPROPRETE);
        LoadSpinnerProprete();

       imgBtnHome = (ImageButton) findViewById(R.id.arrow_back_AuditParcelle);
        imgBtnHome.setOnClickListener(ButtonHomeHandler);

        PrendrePhoto = (Button) findViewById(R.id.btnprendre_photo);
        PrendrePhoto.setOnClickListener(ButtonPrendrePhotoHandler);

        BtnValidate = (Button) findViewById(R.id.btnvalidate_CQ);
        BtnValidate.setOnClickListener(ButtonValidateHandler);


      //  Remarques.setText("Good audit");


        File AppDirAudit = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit/2017/Soytouch");
        Log.i("Path Directory location", AppDirAudit.toString());
        if (!AppDirAudit.exists()) {
            if (!AppDirAudit.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
    }

    View.OnClickListener ButtonPrendrePhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
            TakePicturefromCamera();
        }
    };


    public void TakePicturefromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile("Audit parcelle" + "_HD.jpg");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivity(intent);
            }
        }
    }

    private File createImageFile(String FileName) throws IOException {
        // Create an image file name
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit/2017/Soytouch");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        return file;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        //Depuis la caméra : pas de bitmap en sortie
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit/2017/Soytouch");
        File file = new File(myDir, "_auditparcelle_picture_HD.jpg");
        Uri uri = Uri.fromFile(file);
        Bitmap newPicture;
        try {
            newPicture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
            SaveImage(newPicture, "_auditparcelle_picture_HD.jpg");
            newPicture = getResizedBitmap(newPicture, 256, 256); //Resize the BitmapPicture
            SaveImage(newPicture, "_auditparcelle_picture.jpg"); //Sauvegarde l'image dans le téléphone
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }


    //Méthode pour sauvegarder une image compressée dans le téléphone dans le répertoire "SRP_Agronome/Account_Pictures".
    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Audit_Parcelle");
        //Log.i("Path Location",myDir.toString());
        File file = new File(myDir, FileName);
        //Log.i("File Location",file.toString());
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
            //Log.i("Save Bitmap","L'image est sauvegardé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void FindWidgetViewbyId() {

        SpinnerETAT_VEGETATIF = (Spinner) findViewById(R.id.SpinnerETAT_VEGETATIF);
        SpinnerETAT_PLANTE = (Spinner) findViewById(R.id.SpinnerETAT_PLANTE);
        SpinnerLISTE_PLANTE = (Spinner) findViewById(R.id.SpinnerLISTE_PLANTE);


    }





    //Bouton Home Toolbar
    View.OnClickListener ButtonHomeHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Show_Dialog_Home(getString(R.string.TitleDialogHome), getString(R.string.textHome));
        }
    };

    //Bouton Valider Toolbar
    View.OnClickListener ButtonValidateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            if (CheckInput()) {
                Show_Dialog_Confirm(getString(R.string.TitleDialogConfirm),InputToText());
            }
            else{
                Show_Dialog(getString(R.string.warntitle), getString(R.string.TextWarning));
                GoToMissingField();
            }
        } };


    //Function that automatically scrolls the view to the unfilled field
    private void GoToMissingField(){
        final ScrollView sc = (ScrollView) findViewById(R.id.ScrollViewContact);
        int position = 0 ;

        if (SpinnerETAT_VEGETATIF.getSelectedItem().toString().equals(getString(R.string.TextETAT_VEGETATIF))) {
            position = SpinnerETAT_VEGETATIF.getTop();
        }
        if (SpinnerETAT_PLANTE.getSelectedItem().toString().equals(getString(R.string.TextETAT_PLANTE))) {
            position =SpinnerETAT_PLANTE.getTop();
        }

        if (SpinnerLISTE_PLANTE.getSelectedItem().toString().equals(getString(R.string.TextLISTE_PLANTE))) {
            position = SpinnerLISTE_PLANTE.getTop();
        }

        if (Spinner_Proprete.getSelectedItem().toString().equals(getString(R.string.TextPROPRETE))) {
            position = Spinner_Proprete.getTop();
        }

        final int Position_Y = position;
        final int Position_X = 0;
        sc.post(new Runnable() {
            public void run() {
                sc.scrollTo(Position_X, Position_Y); // these are your x and y coordinates
            }
        });
    }

    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditParcelle.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }


    //A method that creates a string listing the data entered.
    private String InputToText(){
        String text = getString(R.string.textConfirm) + "\n\n";
        text += getString(R.string.TextValidateETAT_VEGETATIF) + " : "+SpinnerETAT_VEGETATIF.getSelectedItem().toString() + "\n";
        text += getString(R.string.TextValidateEtate_Plante) + " : "+SpinnerETAT_PLANTE.getSelectedItem().toString() + "\n";
        text += getString(R.string.TextValidateLISTE_PLANTE) + " : "+SpinnerLISTE_PLANTE.getSelectedItem().toString() + "\n";
        text += getString(R.string.TextValidatePROPRETE) + " : "+Spinner_Proprete.getSelectedItem().toString() + "\n";
        return text;
    }



    //Display Dialog Confirm Data
    private void Show_Dialog_Confirm(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                //  clearData();
               CreateAuditParcelleIntoDataBase();

                Intent intent = new Intent(AuditParcelle.this, MenuAudit.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void CreateAuditParcelleIntoDataBase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_VEGETATIFDao Etat_vegetatif_data = daoSession.getETAT_VEGETATIFDao();

        ETAT_PLANTEDao Etat_plante_data = daoSession.getETAT_PLANTEDao();
        LISTE_PLANTEDao Liste_plante_data = daoSession.getLISTE_PLANTEDao();
        PROPRETEDao proprete_data = daoSession.getPROPRETEDao();
        AUDITDao audit_data=daoSession.getAUDITDao();

       String remarqueTest="auditRemarque";
        //Table  Audit parcelle
        com.srp.agronome.android.db.AUDIT_PARCELLE AuditParcelle_input = new  com.srp.agronome.android.db.AUDIT_PARCELLE();
        //AuditParcelle_input.setREMARQUES_AUDIT_PARCELLE(Remarques.getText().toString().trim());
        AuditParcelle_input.setREMARQUES_AUDIT_PARCELLE(remarqueTest);
        AuditParcelle_input.setPHOTO_PARCELLE("");
        AuditParcelle_input.setArchive(1);
        AuditParcelle_input.setDate_creation(Today_Date());
        AuditParcelle_input.setDate_modification(Today_Date());
        AuditParcelle_input.setID_Agronome_Creation(1);
        AuditParcelle_input.setID_Agronome_Modification(1);

        //Table Etat_vegetatif   error
        ETAT_VEGETATIF Etat_vegetatif_input = new ETAT_VEGETATIF();
       // Etat_vegetatif_input.setNom(Etat_vegetatif.getText().toString().trim());        //null exception
        Etat_vegetatif_input.setNom("Audit vegi");
        Etat_vegetatif_data.insertOrReplace(Etat_vegetatif_input);

//Add list of table Etat_vegetatif
        List<ETAT_VEGETATIF> List_Etat_vegetatif = Etat_vegetatif_data.queryBuilder()
                .where(ETAT_VEGETATIFDao.Properties.Nom.eq(SpinnerETAT_VEGETATIF.getSelectedItem().toString()))
                .list();
        if (List_Etat_vegetatif != null) {
            for (ETAT_VEGETATIF EV : List_Etat_vegetatif) {
                AuditParcelle_input.setETAT_VEGETATIF(EV);
                AuditParcelle_input.setID_ETAT_VEGETATIF(EV.getId());
            }
        }
//Add  table PLANTE_NON_DESIRABLE
        PLANTE_NON_DESIRABLE plante_non_desirable_input = new PLANTE_NON_DESIRABLE();


//Add  table Etat_plant
        ETAT_PLANTE Etat_plante_input = new ETAT_PLANTE();
        //Etat_plante_input.setNom(Etat_plante.getText().toString().trim());
        Etat_plante_input.setNom("plant");
        Etat_plante_data.insertOrReplace(Etat_plante_input);

//Add list of table Etat_plant
        List<ETAT_PLANTE> List_Etat_plante = Etat_plante_data.queryBuilder()
                .where(ETAT_PLANTEDao.Properties.Nom.eq(SpinnerETAT_PLANTE.getSelectedItem().toString()))
                .list();
        if (List_Etat_plante != null) {
            for (ETAT_PLANTE Ep : List_Etat_plante) {
                plante_non_desirable_input.setETAT_PLANTE(Ep);
                plante_non_desirable_input.setID_ETAT_PLANTE(Ep.getId());
            }
        }

//Add  table Liste_Plante
        LISTE_PLANTE Liste_plante_input = new LISTE_PLANTE();
       // Liste_plante_input.setNom(Liste_plante.getText().toString().trim());
        Liste_plante_input.setNom("list plant");
        Liste_plante_data.insertOrReplace(Liste_plante_input);

//Add list of table Etat_plant
        List<LISTE_PLANTE> List_plante = Liste_plante_data.queryBuilder()
                .where(LISTE_PLANTEDao.Properties.Nom.eq(SpinnerLISTE_PLANTE.getSelectedItem().toString()))
                .list();
        if (List_plante != null) {
            for (LISTE_PLANTE lp : List_plante) {
                plante_non_desirable_input.setLISTE_PLANTE(lp);
                plante_non_desirable_input.setID_LISTE_PLANTE(lp.getId());
            }
        }


// Add table proprete

        PROPRETE Proprete_input = new PROPRETE();
       // Proprete_input.setNom(proprete.getText().toString().trim());
        Proprete_input.setNom("proper");
        proprete_data.insertOrReplace(Proprete_input);

//Add list of table proprete
      /*  List<PROPRETE> proprete = proprete_data.queryBuilder()
                .where(LISTE_PLANTEDao.Properties.Nom.eq(SpinnerLISTE_PLANTE.getSelectedItem().toString()))
                .list();
        if (proprete != null) {
            for (PROPRETE pp : proprete) {
                AuditParcelle_input.setPROPRETE(pp);
                AuditParcelle_input.setID_PROPRETE_PARCELLE(pp.getId());
            }
        } */


        //Cardinalités entre les tables :
        AuditParcelle_input.setETAT_VEGETATIF(Etat_vegetatif_input);
        AuditParcelle_input.setID_ETAT_VEGETATIF(Etat_vegetatif_input.getId());


        plante_non_desirable_input.setETAT_PLANTE(Etat_plante_input);
        plante_non_desirable_input.setID_ETAT_PLANTE(Etat_plante_input.getId());

        plante_non_desirable_input.setLISTE_PLANTE(Liste_plante_input);
        plante_non_desirable_input.setID_LISTE_PLANTE(Liste_plante_input.getId());



        AuditParcelle_input.setPROPRETE(Proprete_input);
        AuditParcelle_input.setID_PROPRETE_PARCELLE(Proprete_input.getId());



        //Un contact possède une identité


        //Ajout du contact à l'entreprise concernée
    }

    public static Date Today_Date() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }


    //Display Dialog return HomePage
    private void Show_Dialog_Home(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditParcelle.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(AuditParcelle.this, Audit.class);
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent intent = new Intent(AuditParcelle.this, Audit.class);
                finish();
                startActivity(intent);
            }
        });

        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }



    // Method for creating the selection lists in the database
    private void CreateListIntoDatabase() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_VEGETATIFDao ETAT_VEGETATIF_data = daoSession.getETAT_VEGETATIFDao();
        ETAT_PLANTEDao ETAT_PLANTE_data = daoSession.getETAT_PLANTEDao();
        LISTE_PLANTEDao LISTE_PLANTE_data = daoSession.getLISTE_PLANTEDao();
        PROPRETEDao PROPRETE_data = daoSession.getPROPRETEDao();


        List<ETAT_VEGETATIF> List_ETAT_VEGETATIF = ETAT_VEGETATIF_data.loadAll();
        if (List_ETAT_VEGETATIF.size() == 0) {

            ETAT_VEGETATIF  input_1 = new ETAT_VEGETATIF();
            input_1.setNom(" Mauvais");
            ETAT_VEGETATIF_data.insertOrReplace(input_1);

            ETAT_VEGETATIF  input_2 = new ETAT_VEGETATIF();
            input_2.setNom(" Correct");
            ETAT_VEGETATIF_data.insertOrReplace(input_2);

            ETAT_VEGETATIF  input_3 = new ETAT_VEGETATIF();
            input_3.setNom(" Excellent");
            ETAT_VEGETATIF_data.insertOrReplace(input_3);
        }

        List<ETAT_PLANTE> List_ETAT_PLANTE = ETAT_PLANTE_data.loadAll();
        if (List_ETAT_PLANTE.size() == 0) {

            ETAT_PLANTE   input_1 = new ETAT_PLANTE ();
            input_1.setNom("Présence");
            ETAT_PLANTE_data.insertOrReplace(input_1);

            ETAT_PLANTE  input_2 = new ETAT_PLANTE();
            input_2.setNom("Présence maîtrisée");
            ETAT_PLANTE_data.insertOrReplace(input_2);

            ETAT_PLANTE  input_3 = new ETAT_PLANTE();
            input_3.setNom("Absence");
            ETAT_PLANTE_data.insertOrReplace(input_3);

        }
        List<LISTE_PLANTE> LIST_PLANTE = LISTE_PLANTE_data.loadAll();
        if (LIST_PLANTE.size() == 0) {

            LISTE_PLANTE  input_1 = new LISTE_PLANTE();
            input_1.setNom("Datura");
            LISTE_PLANTE_data.insertOrReplace(input_1);

            LISTE_PLANTE  input_2 = new LISTE_PLANTE();
            input_2.setNom("Morelle");
            LISTE_PLANTE_data.insertOrReplace(input_2);

            LISTE_PLANTE  input_3 = new LISTE_PLANTE();
            input_3.setNom("Autre");
            LISTE_PLANTE_data.insertOrReplace(input_3);
        }

        List<PROPRETE> LISTE_PROPRETE = PROPRETE_data.loadAll();
        if (LISTE_PROPRETE.size() == 0) {

            PROPRETE  proprete_input_1 = new PROPRETE();
            proprete_input_1.setNom("Très propre");
            PROPRETE_data.insertOrReplace(proprete_input_1);

            PROPRETE  proprete_input_2 = new PROPRETE();
            proprete_input_2.setNom("Propre");
            PROPRETE_data.insertOrReplace(proprete_input_2);

            PROPRETE  proprete_input_3 = new PROPRETE();
            proprete_input_3.setNom("Sale");
            PROPRETE_data.insertOrReplace(proprete_input_3);

            PROPRETE  proprete_input_4 = new PROPRETE();
            proprete_input_4.setNom("Très sale");
            PROPRETE_data.insertOrReplace(proprete_input_4);
        }

    }






    // Load SpinnerETAT_VEGETATIF
    private void LoadSpinnerETAT_VEGETATIF() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_VEGETATIFDao ETAT_VEGETATIF_data = daoSession.getETAT_VEGETATIFDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));

                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherETAT_VEGETATIF));

        //Load items from the list
        List<ETAT_VEGETATIF> List_ETAT_VEGETATIF = ETAT_VEGETATIF_data.loadAll();
        if (List_ETAT_VEGETATIF != null) {
            for (ETAT_VEGETATIF EV : List_ETAT_VEGETATIF) {
                adapter.add(EV.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextETAT_VEGETATIF)); //This is the text that will be displayed as hint.
        SpinnerETAT_VEGETATIF.setAdapter(adapter);  //pay attention
        SpinnerETAT_VEGETATIF.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

        SpinnerETAT_VEGETATIF.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidateETAT_VEGETATIF),getString(R.string.TextInputETAT_VEGETATIF),1);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});

    }


    //  LoadSpinnerETAT_PLANTE();
    private void  LoadSpinnerETAT_PLANTE() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_PLANTEDao ETAT_PLANTE_data = daoSession.getETAT_PLANTEDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextanotherEtat_plante));
        //Load items from the list
        List<ETAT_PLANTE> List_ETAT_PLANTE = ETAT_PLANTE_data.loadAll();
        if (List_ETAT_PLANTE != null) {
            for (ETAT_PLANTE Ep : List_ETAT_PLANTE) {
                adapter.add(Ep.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextETAT_PLANTE)); //This is the text that will be displayed as hint.
        SpinnerETAT_PLANTE.setAdapter(adapter);  //pay attention
        SpinnerETAT_PLANTE.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

        SpinnerETAT_PLANTE.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidateEtate_Plante),getString(R.string.TextInputEtat_Plante),2);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }


    //  Load SpinnerLISTE_PLANTE()();
    private void  LoadSpinnerLISTE_PLANTE() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        LISTE_PLANTEDao LISTE_PLANTE_data = daoSession.getLISTE_PLANTEDao();
        //ArrayAdapter return view for each object such as ListView or Spinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherLISTE_PLANTE));
        //Load items from the list
        List<LISTE_PLANTE> List_ETAT_PLANTE = LISTE_PLANTE_data.loadAll();
        if (List_ETAT_PLANTE != null) {
            for (LISTE_PLANTE Lp : List_ETAT_PLANTE) {
                adapter.add(Lp.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(getString(R.string.TextLISTE_PLANTE)); //This is the text that will be displayed as hint.
        SpinnerLISTE_PLANTE.setAdapter(adapter);  //pay attention
        SpinnerLISTE_PLANTE.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.


        SpinnerLISTE_PLANTE.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidateLISTE_PLANTE),getString(R.string.TextInputLISTE_PLANTE),3);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});


    }


    //  Load SpinnerPROPRETE();
    //Vincent Debug Here
    private void LoadSpinnerProprete(){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PROPRETEDao Proprete_data = daoSession.getPROPRETEDao();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView)v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(),R.color.colorHint));
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherPROPRETE)); //Text Add a proprete

        //Charger les éléments de la liste
        List<PROPRETE> List_Proprete = Proprete_data.loadAll();
        if (List_Proprete != null) {
            for (PROPRETE P : List_Proprete) {
                adapter.add(P.getNom()); //Ajouter les éléments
                Log.i("Liste de choix:" , P.getNom());
            }
        }

        adapter.add(getString(R.string.TextPROPRETE)); //This is the text that will be displayed as hint.
        Spinner_Proprete.setAdapter(adapter);
        Spinner_Proprete.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.

        Spinner_Proprete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute une nouveau
                Show_Dialog_EditText(getString(R.string.TextValidatePROPRETE),getString(R.string.TextInputPROPRETE),4);
            }}
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }

    //Create a dialog with an editText and return a string
    private void Show_Dialog_EditText(String title, String message, Integer code) {
        final Integer CodeList = code;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_edittext, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(AuditParcelle.this);
        final EditText edittext = (EditText) dialogview.findViewById(R.id.InputDialogText);
        builder.setTitle(title);
        edittext.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String NewValueList = edittext.getText().toString().trim(); //Globale Variable String
                switch (CodeList) { //Permet de sélectionner la liste de choix en db a incrémenter
                    case 1 : //Code ETAT_VEGETATIF
                        AddETAT_VEGETATIFDB(NewValueList);
                        break;
                    case 2 : //Code ETAT_PLANTE
                        AddETAT_PLANTEDB(NewValueList);
                        break;
                    case 3 : //Code LISTE_PLANTE
                        AddLISTE_PLANTEDB(NewValueList);
                        break;
                    case 4 : //Code proprete
                        AddPROPRETEDB(NewValueList);
                        break;

                }
            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void AddETAT_VEGETATIFDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_VEGETATIFDao Etat_Vegetatif_data = daoSession.getETAT_VEGETATIFDao();
        ETAT_VEGETATIF Etat_Vegetatif_input = new ETAT_VEGETATIF();
        Etat_Vegetatif_input.setNom(Nom);
        Etat_Vegetatif_data.insertOrReplace(Etat_Vegetatif_input);
        LoadSpinnerETAT_VEGETATIF();
    }


    private void AddETAT_PLANTEDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ETAT_PLANTEDao ETAT_PLANTE_data = daoSession.getETAT_PLANTEDao();
        ETAT_PLANTE ETAT_PLANTE_input = new ETAT_PLANTE();
        ETAT_PLANTE_input.setNom(Nom);
        ETAT_PLANTE_data.insertOrReplace(ETAT_PLANTE_input);
        LoadSpinnerETAT_PLANTE();
    }

    private void AddLISTE_PLANTEDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        LISTE_PLANTEDao LISTE_PLANTE_data = daoSession.getLISTE_PLANTEDao();
        LISTE_PLANTE LISTE_PLANTE_input = new LISTE_PLANTE();
        LISTE_PLANTE_input.setNom(Nom);
        LISTE_PLANTE_data.insertOrReplace(LISTE_PLANTE_input);
        LoadSpinnerLISTE_PLANTE();
    }

    private void AddPROPRETEDB(String Nom){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        PROPRETEDao Proprete_data = daoSession.getPROPRETEDao();
        PROPRETE proprete_input = new PROPRETE();
        proprete_input.setNom(Nom);
        Proprete_data.insertOrReplace(proprete_input);
        LoadSpinnerProprete();
    }
    // Method to check if the user has entered the data
    private Boolean CheckInput(){
        boolean b = true;



        if (SpinnerETAT_VEGETATIF.getSelectedItem().toString().equals(getString(R.string.TextETAT_VEGETATIF))) {
            b = false;
        }
        if (SpinnerETAT_PLANTE .getSelectedItem().toString().equals(getString(R.string.TextETAT_PLANTE))) {
            b = false;
        }
        if ( SpinnerLISTE_PLANTE.getSelectedItem().toString().equals(getString(R.string.TextLISTE_PLANTE))) {
            b = false;
        }
        if (Spinner_Proprete.getSelectedItem().toString().equals(getString(R.string.TextPROPRETE))) {
            b = false;
        }

        return b;
    }


}