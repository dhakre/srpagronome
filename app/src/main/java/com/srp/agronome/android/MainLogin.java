package com.srp.agronome.android;

// Widgets et Events

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


// Preferences
import android.content.SharedPreferences;
import android.widget.Toast;


import com.srp.agronome.android.db.*;
import com.srp.agronome.android.db.Contact;
import com.srp.agronome.android.webservice.Service;

//import org.apache.http.HttpResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.srp.agronome.android.CreateContact.Today_Date;
import static com.srp.agronome.android.CreateOrganization.Date_Arret;


public class MainLogin extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    //Déclaration Android Widgets

    /**
     * Input String du login
     */
    private EditText ID_login = null;
    /**
     * Input Int du password
     */
    private EditText password = null;
    /**
     * Image bouton de connexion
     */
    private ImageButton imgBtnConnection = null;
    /**
     * Bouton Aide
     */
    private Button BtnHelp = null;
    /**
     * Bouton Appel Support
     */
    private Button BtnCallSupport = null;

    private Button BtnForfotPwd = null;
    /**
     * CheckBox Memoriser ID
     */
    private CheckBox Checkbox_ID = null;
    /**
     * Image bouton pour acceder aux options
     */
    private ImageButton imgBtnSettings = null;


    //Preference = set of variable saved
    public static SharedPreferences sharedpreferences;
    /**
     * Clé d'accès pour préférence
     */
    private static final String preferenceValues = "PrefValues";
    /**
     * Clé d'accès pour la variable de verrouillage
     */
    private static final String Lock = "LockKey";
    /**
     * Clé d'accès pour la variable de comptage
     */
    private static final String Count = "CountKey";
    /**
     * Clé d'acces pour la memorisation du mot de passe
     */

    private static final String PASSWORD = "PswKey";
    /**
     * Clé d'accès pour la mémorisation du login
     */
    private static final String ID = "IdKey";

    /**
     * Clé d'accès pour la mémorisation de la langue
     */
    public static final String Language = "LanguageKey";
    /**
     * Clé d'accès pour la memorisation de la premiere connexion
     */
    public static final String FirstConnexion = "FirstConnexionKey";
    /**
     * Nombre d'essai de connexion maximum
     */
    private static final int Number_attemps = 10;
    /**
     * Nombre d'essai de connexion actuel
     */
    private int count_attemps;
    /**
     * Application verrouillée ou non
     */

    //private ArrayList<String> data;
    private boolean locked;
    /**
     * Login mémorisé
     */
    private String ID_saved;
    private String PASSWORD_saved;

    // Acceder à un compte dans la base de données = Mauvaise idee : utiliser les ID plutot
    public static String Nom_Compte;
    public static String Nom_Entreprise;
    public static String Nom_Contact;
    public static String Prenom_Contact;


    //Acces BD a partir des ID
    public static long ID_Compte_Selected;
    public static long ID_Entreprise_Selected;
    public static long ID_Contact_Selected;

    public static long ID_compte_server;

    //execute ones
    public static int EXECUTE_ONES=1;


    /**
     * Langue utilisateur choisie
     */
    public static String Language_saved;

    //Premiere connexion a l'application
    private boolean first_connection = true;
    private static boolean first_loading = false;

    private CompteDao compte_dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);


        ID_login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);
        Checkbox_ID = (CheckBox) findViewById(R.id.checkBox);

        //get compte
        //String loginText=ID_login.getText().toString();
        //
        // ID_compte_server=getSelectedCompte(loginText);


        imgBtnConnection = (ImageButton) findViewById(R.id.imageButtonGo);
        imgBtnConnection.setOnTouchListener(imgButtonConnectionHandler);

        BtnHelp = (Button) findViewById(R.id.ButtonHelp);
        BtnHelp.setOnTouchListener(ButtonHelpHandler);

        BtnCallSupport = (Button) findViewById(R.id.ButtonCallSupport);
        BtnCallSupport.setOnTouchListener(ButtonCallSupportHandler);

        BtnForfotPwd = (Button) findViewById(R.id.buttonforgotmdp_id);
        BtnForfotPwd.setOnClickListener(BtnForgotPwdHandler);

        imgBtnSettings = (ImageButton) findViewById(R.id.btn_settings);
        imgBtnSettings.setOnClickListener(ButtonSettingsHandler);

        //Test if the application is locked or not when the user opens the application
        GetPreference();
        if (locked) {
            Locked_application();
        }

        //Load the ID and password
        // ID_login.setText(ID_saved, TextView.BufferType.EDITABLE);
        // password.setText(PASSWORD_saved, TextView.BufferType.EDITABLE);
        ID_login.setText(ID_saved, TextView.BufferType.EDITABLE);
        password.setText(PASSWORD_saved, TextView.BufferType.EDITABLE);

        //Ask a list of permission needed (Android version > 6.0)
        AskPermissionUser();

        if (first_loading) {
            if (!(TextUtils.isEmpty(Language_saved))) {
                EditLanguage(Language_saved);
                first_loading = false;
            }
        }

        //Création des comptes à la 1ere ouverture de l'application = A changer plus tard avec la syncronisation
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        compte_dao = daoSession.getCompteDao();
        Type_CompteDao type_compte_dao = daoSession.getType_CompteDao();

        if (first_connection) {
            Save_First_Connection(false);
            //Premiere connexion : champs identifiant et mot de passe vide !
            Save_ID("");
            Save_Password("");
            ID_login.setText("admin", TextView.BufferType.EDITABLE);
            password.setText("1234", TextView.BufferType.EDITABLE);

            ID_login.setText("mathieu", TextView.BufferType.EDITABLE);
            password.setText("4578", TextView.BufferType.EDITABLE);

            ID_login.setText("justine", TextView.BufferType.EDITABLE);
            password.setText("4500", TextView.BufferType.EDITABLE);

            ID_login.setText("philippe", TextView.BufferType.EDITABLE);
            password.setText("4455", TextView.BufferType.EDITABLE);

            ID_login.setText("paul", TextView.BufferType.EDITABLE);
            password.setText("7400", TextView.BufferType.EDITABLE);


            Create_Account("admin", "1234", "Administrateur");  //android id=1
            Create_Account("jean", "0000", "Administrateur");    //android id=2
            Create_Account("vanseng", "0042", "Administrateur");  //android id=3
            //new logins
            Create_Account("justine", "4500", "Administrateur");  //android id=4
            Create_Account("mathieu", "4578", "Administrateur");  //android id=5
            //admin account is philippe
            Create_Account("philippe", "4455", "Administrateur");  //android id=6

            Create_Account("paul", "7400", "Administrateur");    //android id=7

            ID_compte_server = getSelectedCompte(ID_login.getText().toString());
            Log.i(TAG, "onCreate: ID_compte_server=" + ID_compte_server);


            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("FormatCode", "FR");
            editor.putString("LocationStorage", "Intern");
            editor.apply();

            if (hasExternalSDCard()) {
                Show_Dialog(getString(R.string.TitleSDCardDetected), getString(R.string.TextSDCardDetected));
            }

        }

        //Creation du dossier contenant les ressources de l'application :
        //Localisation : /storage/emulated/0/SRP_Agronome
        //Localisation : /storage/extSdCard/SRP_Agronome
        File AppDir = new File(getDirectoryPath(), "SRP_Agronome/Account_Pictures");
        if (!AppDir.exists()) {
            if (!AppDir.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
        //loadJSON();

        //addDataForAccounts add=new addDataForAccounts();
       // add.addData();


    } //End_Oncreate


    //get selected compte
    public long getSelectedCompte(String compteName) {
        long id_server_compte;
        if (compteName.equals("justine")) {
            id_server_compte = 1;
        } else if (compteName.equals("mathieu")) {
            id_server_compte = 2;
        } else if (compteName.equals("philippe")) {
            id_server_compte = 3;  //admin access
        } else {
            id_server_compte = 4;//admin show all accounts
        }
        return id_server_compte;
    }

    private void Create_Account(String login, String password, String type_compte) {

        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        compte_dao = daoSession.getCompteDao();
        Type_CompteDao type_compte_dao = daoSession.getType_CompteDao();

        Compte admin = new Compte();
        admin.setLogin(login); //Login
        admin.setPassword(password); //Password
        admin.setArchive(1);

        Type_Compte Types_compte = new Type_Compte();
        Types_compte.setNom(type_compte);
        type_compte_dao.insert(Types_compte);

        admin.setType_Compte(Types_compte);
        admin.setID_Type_Compte(Types_compte.getId());
        admin.setCollaborateur(null);
        admin.setID_Collaborateur(null);
        compte_dao.insert(admin);
        //set this

    }

    //Créer un repertoire contenant les documents pour un compte donné.
    private void CreateDirectoryAccount(String UserName) {

        File AccountDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + UserName + "_data");
        if (!AccountDir.exists()) {
            if (!AccountDir.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }

        File MediaDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + UserName + "_data/SRP_Camera");
        notifyMediaStoreScanner(MediaDir);
        if (!MediaDir.exists()) {
            if (!MediaDir.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }


        File CompanyDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + UserName + "_data/Organizations");
        if (!CompanyDir.exists()) {
            if (!CompanyDir.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }

        File CacheDir = new File(getDirectoryPath(), "SRP_Agronome/Cache");
        if (!CacheDir.exists()) {
            if (!CacheDir.mkdirs()) {
                Log.i("App", "failed to create directory : already exist");
            }
        }
    }


    //Get the path of the directory : Intern or SD path
    private File getDirectoryPath() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
        String location = sharedpreferences.getString("LocationStorage", null);
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (location.equals("Intern")) {
            return Dirs[0];
        } else {
            return Dirs[1];
        }
    }


    public final void notifyMediaStoreScanner(final File file) {
        try {
            MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(),
                    file.getAbsolutePath(), file.getName(), null);
            getApplicationContext().sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * Permet de se connecter lorsque l'utilisateur clique sur "Go".
     */
    View.OnTouchListener imgButtonConnectionHandler = new View.OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            boolean connected = false;
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    imgBtnConnection.setImageResource(R.drawable.btn_ok_touch);

                    String id = ID_login.getText().toString();
                    String psw = password.getText().toString();

                    if (TextUtils.isEmpty(id) && TextUtils.isEmpty(psw)) {
                        Show_Dialog(getString(R.string.DialogTitle1), getString(R.string.LoginPwdMissing));
                    }

                    if (TextUtils.isEmpty(id) && !(TextUtils.isEmpty(psw))) {
                        Show_Dialog(getString(R.string.DialogTitle1), getString(R.string.LoginMissing));
                    }

                    if (!(TextUtils.isEmpty(id)) && TextUtils.isEmpty(psw)) {
                        Show_Dialog(getString(R.string.DialogTitle1), getString(R.string.PwdMissing));
                    }

                    if (!(TextUtils.isEmpty(id)) && !(TextUtils.isEmpty(psw))) {
                        connected = Compare_DB_ID_PASSWORD(id, psw);
                        if (connected) {
                            if (Checkbox_ID.isChecked()) {
                                Save_ID(id);
                                Save_Password(psw);

                            } // Sauvegarde l'ID si checkbox cochée
                            count_attemps = 0; //Reset Locking parameters
                            SavePreference(false, count_attemps);
                            Nom_Compte = id;
                            CreateDirectoryAccount(Nom_Compte);
                            ID_Compte_Selected = getIDAccount(id);
                            ID_compte_server=getSelectedCompte(id);


                            Edit_Account_Opener();


                        } else {
                            SavePreference(false, count_attemps++);
                            if (Compare_DB_ID(id)) {
                                Show_Error_Connetion(Number_attemps, count_attemps, getString(R.string.ErrorPassword));
                            } else {
                                Show_Error_Connetion(Number_attemps, count_attemps, getString(R.string.ErrorLogin));
                            }
                            Check_Locked(Number_attemps, count_attemps);
                        }
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    imgBtnConnection.setImageResource(R.drawable.btn_ok);
                    break;
            }
            return true;
        }
    };

    //kill
    @Override
    public void onBackPressed() {

        android.os.Process.killProcess(android.os.Process.myPid());
        // This above line close correctly
    }

    private long getIDAccount(String username) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        compte_dao = daoSession.getCompteDao();
        long result = 0;
        List<Compte> list_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Login.eq(username))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                result = c.getId();
            }
        }
        return result;
    }


    //Permet d'acceder à edit_Account si la bd est vide sur le compte
    private void Edit_Account_Opener() {
        List<Compte> info_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte_Selected))
                .list();
        if (info_compte != null) {
            for (Compte c : info_compte) {
                if (c.getCollaborateur() == null) {
                    Intent intent = new Intent(MainLogin.this, EditAccount.class);
                    finish();
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainLogin.this, MainMenu.class);
                    finish();
                    startActivity(intent);
                }
            }
        }
    }


    /**
     * Affiche une boîte de dialogue lorsque l'utilisateur clique sur "Aide".
     */
    View.OnTouchListener ButtonHelpHandler = new View.OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    BtnHelp.setBackgroundColor(getResources().getColor(R.color.colorBackgroundButton));
                    Show_Dialog(getString(R.string.DialogTitleHelp), getString(R.string.ContentHelp));
                    break;
                case MotionEvent.ACTION_UP:
                    BtnHelp.setBackgroundColor(getResources().getColor(R.color.colorBackground));
                    break;
            }
            return true;
        }
    };


    /**
     * Affiche une boîte de dialogue lorsque l'utilisateur clique sur "Appel Support".
     */
    View.OnTouchListener ButtonCallSupportHandler = new View.OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    BtnCallSupport.setBackgroundColor(getResources().getColor(R.color.colorBackgroundButton));
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{"ibrsoytouch@gmail.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT, "nous contacter");
                    i.putExtra(Intent.EXTRA_TEXT, "votre question:");
                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(MainLogin.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    BtnCallSupport.setBackgroundColor(getResources().getColor(R.color.colorBackground));
                    break;
            }
            return true;
        }
    };


    View.OnClickListener BtnForgotPwdHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MainLogin.this, ResetPassWord.class);
            startActivity(intent);
            finish();

        }
    };

    View.OnClickListener ButtonSettingsHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MainLogin.this, Settings.class);
            finish();
            startActivity(intent);
        }
    };

    /**
     * Recherche si le login et le mot de passe fournis correspond dans la base de donnée.
     *
     * @param login Identifiant de l'utilisateur.
     * @param mdp   Mot de passe de l'utilisateur.
     * @return Retourne le résultat, sous la forme d'un booléen.
     */
    private boolean Compare_DB_ID_PASSWORD(String login, String mdp) {

        List<Compte> compte_connexion = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Login.eq(login), CompteDao.Properties.Password.eq(mdp)) //Conditon AND
                .list();
        return (compte_connexion.size() != 0);// Le login existe -> mot de passe correct ou incorrect
    }

    /**
     * Recherche uniquement si le login existe dans la base de donnée
     *
     * @param login Identifiant de l'utilisateur.
     * @return Retourne le résultat, sous la forme d'un booléen.
     */
    private boolean Compare_DB_ID(String login) {
        List<Compte> compte_connexion_login = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Login.eq(login))
                .list();
        return ((compte_connexion_login.size() != 0));
    }


    /**
     * Vérifie si l'application doit être verrouillée.
     *
     * @param Number_Max_Attemps Nombre d'essai maximal de connexion.
     * @param Current_Number     Nombre d'essai actuel de connexion.
     */
    private void Check_Locked(int Number_Max_Attemps, int Current_Number) {
        if (Current_Number >= Number_Max_Attemps) { //Condition for locking the application
            SavePreference(true, count_attemps);
            Locked_application();
        }
    }

    /**
     * Affiche le nombre d'essai de connexion restant et l'erreur de connexion.
     *
     * @param Current_Number     Nombre d'essai actuel de connexion.
     * @param Number_Max_Attemps Nombre d'essai maximal de connexion.
     */
    private void Show_Error_Connetion(int Number_Max_Attemps, int Current_Number, String text) {
        String Message = text + "\n" + getString(R.string.Remaining1) + (Number_Max_Attemps - Current_Number) + getString(R.string.Remaining2);
        Show_Dialog(getString(R.string.DialogTitleFailConnection), Message);
    }

    /**
     * Verrouille l'application en désactivant les formulaires et le bouton de connexion.
     */
    private void Locked_application() {
        Show_Dialog(getString(R.string.DialogTitleLocked), getString(R.string.TextLocked));
        ID_login.setEnabled(false);
        password.setEnabled(false);
        Checkbox_ID.setEnabled(false);
        imgBtnConnection.setEnabled(false);
    }


    /**
     * Enregistrer les préférences de l'utilisateur.
     *
     * @param locked L'application est verrouillé ou non.
     * @param count  Nombre d'essai de connexions non réussis.
     */
    private void SavePreference(Boolean locked, int count) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(Lock, locked);
        editor.putInt(Count, count);
        editor.apply();
    }

    /**
     * Lire les préférences de l'utilisateur enregistrées.
     */
    private void GetPreference() {
        sharedpreferences = getSharedPreferences(preferenceValues, 0); // 0 = Private Mode
        if (sharedpreferences.contains(Lock)) {
            locked = sharedpreferences.getBoolean(Lock, false); // Default value -> false
        }
        if (sharedpreferences.contains(Count)) {
            count_attemps = sharedpreferences.getInt(Count, 0); // Default value -> zero
        }
        if (sharedpreferences.contains(ID)) {
            ID_saved = sharedpreferences.getString(ID, ""); // Default value -> vide
        }
        if (sharedpreferences.contains(PASSWORD)) {
            PASSWORD_saved = sharedpreferences.getString(PASSWORD, ""); // Default value -> vide
        }
        if (sharedpreferences.contains(Language)) {
            Language_saved = sharedpreferences.getString(Language, ""); // Default value -> vide
        }
        if (sharedpreferences.contains(FirstConnexion)) {
            first_connection = sharedpreferences.getBoolean(FirstConnexion, true); // Default value -> faux
        }
    }

    /**
     * Enregistre l'ID de l'utilisateur
     *
     * @param ID_Input ID à enregistrer
     */
    private void Save_ID(String ID_Input) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(ID, ID_Input);
        editor.apply();
    }

    /**
     * Enregistre l'ID de l'utilisateur
     *
     * @param Password_Input PASSWORD à enregistrer
     */
    private void Save_Password(String Password_Input) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PASSWORD, Password_Input);
        editor.apply();
    }

    /**
     * @param first_connection Premiere connexion à l'application
     */
    private void Save_First_Connection(Boolean first_connection) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(FirstConnexion, first_connection);
        editor.apply();
    }


    /**
     * Créer et afficher une boîte de dialogue.
     *
     * @param message Le message affiché.
     * @param title   Le titre de la boîte de dialogue.
     */
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainLogin.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

    // Permet de changer la langue de l'application
    public void EditLanguage(String lang) {
        Locale locale;
        Configuration config = new Configuration();
        locale = new Locale(lang);
        Locale.setDefault(locale);
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, null);
        onConfigurationChanged(config);
        this.recreate(); // Rafraichir Activité courante
    }

    //Return true if the device has a extern SD Card
    public boolean hasExternalSDCard() {
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (Dirs.length == 1) {
            return false;
        } else {
            return (!(Dirs[1] == null));
        }
    }

    // Methods for checking and asking user's permission : Android version >= 6.0 (API Level 23)
    private void AskPermissionUser() {
        int PERMISSION_ALL = 1;

        //Add permission here
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        //Request permissions All
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

    }

    // Methods for checking and asking user's permission : Android version >= 6.0 (API Level 23)
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Creation du dossier contenant les ressources de l'application :
                    //Localisation : /storage/emulated/0/SRP_Agronome
                    File AppDir = new File(Environment.getExternalStorageDirectory(), "SRP_Agronome/Account_Pictures");
                    //Log.i("Path Directory location", AppDir.toString());
                    if (!AppDir.exists()) {
                        if (!AppDir.mkdirs()) {
                            Log.i("App", "failed to create directory : already exist");
                        }
                    }
                }
            }
        }
    }

    //create data for justine account

}


