package com.srp.agronome.android.webservice;

/**
 * Created by soytouchrp on 05/12/2017.
 */

import android.util.Log;

import com.srp.agronome.android.webservice.pojo.TokenInterface;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@Singleton
public class MyLoginInterceptor implements Interceptor {
    private String sessionToken;

    private static MyLoginInterceptor loginInterceptor;

    public static MyLoginInterceptor getInstance(){
        if (loginInterceptor == null){
            loginInterceptor = new MyLoginInterceptor();
        }
        return loginInterceptor;
    }
    @Inject
    private MyLoginInterceptor() {

        System.out.println("TVS : login interceptor : 1  ");
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        Request requestBuilder = request.newBuilder()
                .header("email", "test43@free.fr")
                .header("password", "mlkjhhg")
                .method(request.method(), request.body())
                .build();
//        System.out.println("TVS : interceptor : 2 : requestBuilder : "+requestBuilder.headers());

//        if (request.header(Service.KEY_AUTHENTIFICATION) == null) {
//            // needs credentials
//            if (sessionToken == null) {
//                throw new RuntimeException("Session token should be defined for auth apis");
//            } else {
////                requestBuilder.addHeader("Cookie", sessionToken);
//            }
//        }

        Response response =  chain.proceed(requestBuilder);
//        System.out.println("TVS : interceptor : response : "+response.toString());

//        Log.d("MyApp", "Code : "+response.code());
        if (response.code() == 401){
            // Magic is here ( Handle the error as your way )
            // launch update of the token

            return response;
        }
        return response;
    }
}


