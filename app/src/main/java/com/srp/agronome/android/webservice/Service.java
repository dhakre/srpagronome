package com.srp.agronome.android.webservice;


import com.squareup.okhttp.Response;
import com.srp.agronome.android.db.Contact;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by doucoure on 22/08/2017.
 */

public interface Service {

    String ENDPOINT = "http://138.201.126.203/testmysrp/webservice/index.php/api/Srp/contacts/";
    String VISITENDPOINT="http://138.201.126.203/testmysrp/webservice/index.php/api/Visit/visit/";
    String VISITEENDPOINT="http://138.201.126.203/testmysrp/webservice/index.php/api/SrpVisite/visite/";
    String COMPTEPOINT="http://138.201.126.203/testmysrp/webservice/index.php/api/Compte/compte";
    String KEY_AUTHENTIFICATION = "Bearer"; // use bearer as token
    String REST2_ENDPOINT = "http://138.201.126.203/restv2/public/api/";
    String REST3_ENDPOINT = "http://138.201.126.203/restv2/public/api/";
}
