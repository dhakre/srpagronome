package com.srp.agronome.android.db;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.SqlUtils;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "COMPTE".
*/
public class CompteDao extends AbstractDao<Compte, Long> {

    public static final String TABLENAME = "COMPTE";

    /**
     * Properties of entity Compte.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Login = new Property(1, String.class, "login", false, "LOGIN");
        public final static Property Password = new Property(2, String.class, "password", false, "PASSWORD");
        public final static Property Archive = new Property(3, int.class, "archive", false, "ARCHIVE");
        public final static Property ID_Type_Compte = new Property(4, Long.class, "ID_Type_Compte", false, "ID__TYPE__COMPTE");
        public final static Property ID_Collaborateur = new Property(5, Long.class, "ID_Collaborateur", false, "ID__COLLABORATEUR");
    }

    private DaoSession daoSession;


    public CompteDao(DaoConfig config) {
        super(config);
    }
    
    public CompteDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"COMPTE\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"LOGIN\" TEXT NOT NULL ," + // 1: login
                "\"PASSWORD\" TEXT NOT NULL ," + // 2: password
                "\"ARCHIVE\" INTEGER NOT NULL ," + // 3: archive
                "\"ID__TYPE__COMPTE\" INTEGER," + // 4: ID_Type_Compte
                "\"ID__COLLABORATEUR\" INTEGER);"); // 5: ID_Collaborateur
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"COMPTE\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Compte entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindString(2, entity.getLogin());
        stmt.bindString(3, entity.getPassword());
        stmt.bindLong(4, entity.getArchive());
 
        Long ID_Type_Compte = entity.getID_Type_Compte();
        if (ID_Type_Compte != null) {
            stmt.bindLong(5, ID_Type_Compte);
        }
 
        Long ID_Collaborateur = entity.getID_Collaborateur();
        if (ID_Collaborateur != null) {
            stmt.bindLong(6, ID_Collaborateur);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Compte entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindString(2, entity.getLogin());
        stmt.bindString(3, entity.getPassword());
        stmt.bindLong(4, entity.getArchive());
 
        Long ID_Type_Compte = entity.getID_Type_Compte();
        if (ID_Type_Compte != null) {
            stmt.bindLong(5, ID_Type_Compte);
        }
 
        Long ID_Collaborateur = entity.getID_Collaborateur();
        if (ID_Collaborateur != null) {
            stmt.bindLong(6, ID_Collaborateur);
        }
    }

    @Override
    protected final void attachEntity(Compte entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Compte readEntity(Cursor cursor, int offset) {
        Compte entity = new Compte( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getString(offset + 1), // login
            cursor.getString(offset + 2), // password
            cursor.getInt(offset + 3), // archive
            cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4), // ID_Type_Compte
            cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5) // ID_Collaborateur
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Compte entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setLogin(cursor.getString(offset + 1));
        entity.setPassword(cursor.getString(offset + 2));
        entity.setArchive(cursor.getInt(offset + 3));
        entity.setID_Type_Compte(cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4));
        entity.setID_Collaborateur(cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Compte entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Compte entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Compte entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getType_CompteDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T1", daoSession.getCollaborateurDao().getAllColumns());
            builder.append(" FROM COMPTE T");
            builder.append(" LEFT JOIN TYPE__COMPTE T0 ON T.\"ID__TYPE__COMPTE\"=T0.\"_id\"");
            builder.append(" LEFT JOIN COLLABORATEUR T1 ON T.\"ID__COLLABORATEUR\"=T1.\"_id\"");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected Compte loadCurrentDeep(Cursor cursor, boolean lock) {
        Compte entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        Type_Compte type_Compte = loadCurrentOther(daoSession.getType_CompteDao(), cursor, offset);
        entity.setType_Compte(type_Compte);
        offset += daoSession.getType_CompteDao().getAllColumns().length;

        Collaborateur collaborateur = loadCurrentOther(daoSession.getCollaborateurDao(), cursor, offset);
        entity.setCollaborateur(collaborateur);

        return entity;    
    }

    public Compte loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<Compte> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<Compte> list = new ArrayList<Compte>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<Compte> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<Compte> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
