package com.srp.agronome.android;


import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.StatFs;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.VideoView;

import com.srp.agronome.android.db.Compte;
import com.srp.agronome.android.db.CompteDao;
import com.srp.agronome.android.db.DaoSession;
import com.srp.agronome.android.db.Photo;
import com.srp.agronome.android.db.PhotoDao;
import com.srp.agronome.android.db.Theme_Photo;
import com.srp.agronome.android.db.Theme_PhotoDao;
import com.srp.agronome.android.db.Video;
import com.srp.agronome.android.db.VideoDao;


import org.apache.commons.io.FilenameUtils;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Compte_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static java.util.jar.Pack200.Packer.ERROR;


/**
 * Created by vincent on 14/08/17.
 */

public class MenuPhoto extends AppCompatActivity {

    private Button Button_Photo = null;
    private Button Button_Video = null;
    private Button Button_Gallery = null;
    private ImageButton Button_Back = null;

    static final int REQUEST_VIDEO_CAPTURE = 1;
    static final int CAMERA_PIC_REQUEST = 2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_photo);

        Button_Photo = (Button) findViewById(R.id.button_take_picture);
        Button_Photo.setOnClickListener(ButtonPhotoHandler);

        Button_Video = (Button) findViewById(R.id.button_take_video);
        Button_Video.setOnClickListener(ButtonVideoHandler);

        Button_Gallery = (Button) findViewById(R.id.button_view_gallery);
        Button_Gallery.setOnClickListener(ButtonGalleryHandler);

        Button_Back = (ImageButton) findViewById(R.id.arrow_back_menu_photo);
        Button_Back.setOnClickListener(ButtonBackHandler);
        CheckStorageSize();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(MenuPhoto.this, MainMenu.class);
            startActivity(intent);
            finish();
        }
        return true;
    }

    //Bouton Retour
    View.OnClickListener ButtonBackHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuPhoto.this, MainMenu.class);
            startActivity(intent);
            finish();}
    };

    //Bouton Photo
    View.OnClickListener ButtonPhotoHandler = new View.OnClickListener() {
        public void onClick(View v) {
           //Code bouton Photo
            TakePictureIntent();
//            Toast.makeText(getApplicationContext(),"En cours de développement",Toast.LENGTH_SHORT).show();
        }
    };

    //Bouton Video
    View.OnClickListener ButtonVideoHandler = new View.OnClickListener() {
        public void onClick(View v) {
           //Code bouton Video
            TakeVideoIntent();
        }
    };

    //Bouton Gallery
    View.OnClickListener ButtonGalleryHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //Code bouton Gallery
            ScanAllMediaInDirectory();
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setType("*/*");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    };

    //Take a video from camera
    private void TakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File videoFile = null;
            try {
                videoFile = createFileVideo("video_tmp.mp4");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (videoFile != null) {
                Uri videoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        videoFile);
                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI);
                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
            }
        }
    }

    //Take a photo from Camera
    public void TakePictureIntent() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createFileImage("photo_tmp.jpg");
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.srp.android.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    //Permet de scanner les fichiers pour les afficher dans la galerie du téléphone.
    private void ScanAllMediaInDirectory(){
        File Dir = new File (getDirectoryPath(),"SRP_Agronome/Account_" + Nom_Compte + "_data/SRP_Camera/");
        File[] fList = Dir.listFiles();
        for (File file : fList){
            if (FilenameUtils.getExtension(file.getPath()).equals("jpg")){ //Scan Image
                addImageToGallery(file.getPath(),MenuPhoto.this);
            }
            if (FilenameUtils.getExtension(file.getPath()).equals("mp4")){  //Scan Vidéo
                addVideoToGallery(file.getPath(),MenuPhoto.this);
            }
        }
    }

    //Permet de rendre visible une image depuis la galerie du téléphone
    public static void addImageToGallery(final String filePath, final Context context) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);
        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    //Permet de rendre visible une video depuis la galerie du téléphone
    public static void addVideoToGallery(final String filePath, final Context context) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Video.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
        values.put(MediaStore.MediaColumns.DATA, filePath);
        context.getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
    }

    private File createFileImage(String FileName) throws IOException {
        // Create a file with name
        File myDir = new File(getDirectoryPath(),"SRP_Agronome/Account_" + Nom_Compte + "_data/SRP_Camera/");
        File file = new File(myDir, FileName);
        return file;
    }

    private File createFileVideo(String FileName) throws IOException {
        // Create a file with name
        File myDir = new File(getDirectoryPath(),"SRP_Agronome/Account_" + Nom_Compte + "_data/SRP_Camera/");
        File file = new File(myDir, FileName);
        return file;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            //Code Video
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/SRP_Camera");
            File video_output = new File(myDir,"video_tmp.mp4");
            Uri video_uri = Uri.fromFile(video_output);
            Show_Dialog_Video(video_output,video_uri);
        }

        if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            //Code Photo
            File myDir = new File(getDirectoryPath(), "SRP_Agronome/Account_" + Nom_Compte + "_data/SRP_Camera");
            File file = new File(myDir,"photo_tmp.jpg");
            Uri uri = Uri.fromFile(file);
            Bitmap Photo ;
            Bitmap Photo_LQ ;
            try {
                Photo = MediaStore.Images.Media.getBitmap(getContentResolver(), uri); //Get Bitmap from URI
                Photo = rotateImageIfRequired(Photo,uri); //Rotation si necessaire
                Photo_LQ = getResizedBitmap(Photo,1000,1000);
                DeleteFile("photo_tmp.jpg");
                Show_Dialog_Photo(Photo,Photo_LQ);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean rename(File from, File to) {
        return from.getParentFile().exists() && from.exists() && from.renameTo(to);
    }

    private void SaveImage(Bitmap finalBitmap, String FileName) {
        File myDir = new File(getDirectoryPath(),"SRP_Agronome/Account_" + Nom_Compte + "_data/SRP_Camera");
        File file = new File(myDir, FileName);
        if (file.exists()) file.delete(); //Already exist -> delete it
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //Format JPEG , Quality :(min 0 max 100)
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //notifyMediaStoreScanner(file);
        addImageToGallery(file.getAbsolutePath(),MenuPhoto.this);
    }

    private void SaveVideoStorage(File input,String VideoName){
            File videofile = null;
            try {
                videofile = createFileVideo(VideoName);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            rename(input,videofile);
            addVideoToGallery(videofile.getPath(),MenuPhoto.this);
    }


    //Get the path of the directory : Intern or SD path
    private File getDirectoryPath(){
        SharedPreferences SP = getSharedPreferences("PrefValues", 0); // 0 = Private Mode
        String location = SP.getString("LocationStorage",null);
        File[] Dirs = ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
        if (location.equals("Intern")){
            return Dirs[0];}
        else{
            return Dirs[1];}
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else{
            ei =  new ExifInterface(selectedImage.getPath());}

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    //Méthode qui permet de redimensionner une image Bitmap
    private static Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    //Rotate an image with angle in degree
    private Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        //Log.i("Info Rotation","Rotation de l image de : " + degree);
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }

    private void DeleteFile(String NameDelete){
        File file = new File(getDirectoryPath(),"SRP_Agronome/Account_" + Nom_Compte + "_data/SRP_Camera/" + NameDelete);
        boolean deleted = file.delete();
    }

    private String getPhotoName(long ID_Compte){
        int number_photo_db = CountPhotoAccount(ID_Compte);
        return getSubString(Nom_Compte) + "_photo_" + number_photo_db + ".jpg";
    }

    private String getPhotoThemeName(long ID_Compte, String nom){
        int number_photo_db_theme = CountPhotoThemeAccount(ID_Compte,nom);
        return getSubString(Nom_Compte) + " _" + nom + "_" + number_photo_db_theme + ".jpg";
    }

    private String getVideoName(long ID_Compte){
        int number_video_db = CountVideoAccount(ID_Compte);
        return getSubString(Nom_Compte) + "_video_" + number_video_db + ".mp4";
    }

    private String getVideoThemeName(long ID_Compte, String nom){
        int number_video_db_theme = CountVideoThemeAccount(ID_Compte,nom);
        return getSubString(Nom_Compte) + " _" + nom + "_" + number_video_db_theme + ".mp4";
    }

    //Permet de retourner les 3 premiers caracteres d'un texte
    private String getSubString(String chaine){
        String result="";
        if (chaine.length() >= 3){ result = chaine.substring(0,3);}
        if (chaine.length() == 2){ result = chaine.substring(0,2);}
        if (chaine.length() == 1){ result = chaine.substring(0,1);}
        return result ;
    }


    private void addPhotoToAccount(long ID_Compte , String Photo_Name) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        PhotoDao photo_data = daoSession.getPhotoDao();

        Photo photo_input = new Photo();
        photo_input.setNom(Photo_Name);

        //Associer les tables au comptes
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                photo_input.setID_Compte(c.getId());
                photo_data.insertOrReplace(photo_input);
                c.resetPhotoList();
                compte_data.update(c);
            }
        }
    }

    private void addVideoToAccount(long ID_Compte,String Video_Name){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        VideoDao video_data = daoSession.getVideoDao();

        Video video_input = new Video();
        video_input.setNom(Video_Name);

        //Associer les tables au comptes
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                video_input.setID_Compte(c.getId());
                video_data.insertOrReplace(video_input);
                c.resetVideoList();
                compte_data.update(c);
            }
        }
    }

    private void addPhotoToAccountWithTheme(long ID_Compte, String Photo_Name,String Theme){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        PhotoDao photo_data = daoSession.getPhotoDao();
        Theme_PhotoDao theme_photo_data = daoSession.getTheme_PhotoDao();

        Photo photo_input = new Photo();
        photo_input.setNom(Photo_Name);
        //Edit Theme Photo
        List<Theme_Photo> List_Theme_Photo = theme_photo_data.queryBuilder()
                .where(Theme_PhotoDao.Properties.Nom.eq(Theme))
                .list();
        if (List_Theme_Photo != null) {
            for (Theme_Photo TP : List_Theme_Photo) {
                photo_input.setTheme_Photo(TP);
                photo_input.setID_Theme_Photo(TP.getId());
            }
        }
        //Associer les tables au comptes
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                photo_input.setID_Compte(c.getId());
                photo_data.insertOrReplace(photo_input);
                c.resetPhotoList();
                compte_data.update(c);
            }
        }
    }

    private void addVideoToAccountWithTheme(long ID_Compte, String Video_Name, String Theme){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        VideoDao video_data = daoSession.getVideoDao();
        Theme_PhotoDao theme_photo_data = daoSession.getTheme_PhotoDao();

        Video video_input = new Video();
        video_input.setNom(Video_Name);
        //Edit Theme Photo
        List<Theme_Photo> List_Theme_Photo = theme_photo_data.queryBuilder()
                .where(Theme_PhotoDao.Properties.Nom.eq(Theme))
                .list();
        if (List_Theme_Photo != null) {
            for (Theme_Photo TP : List_Theme_Photo) {
                video_input.setTheme_Photo(TP);
                video_input.setID_Theme_Video(TP.getId());
            }
        }
        //Associer les tables au comptes
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte != null) {
            for (Compte c : list_compte) {
                video_input.setID_Compte(c.getId());
                video_data.insertOrReplace(video_input);
                c.resetVideoList();
                compte_data.update(c);
            }
        }
    }

    //Permet de retourner le nombre de photos associées à un compte
    private int CountPhotoAccount(long ID_Compte){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        int result = 0;
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte == null){
            result = 0;
        }
        else{
            for (Compte c : list_compte){
                result = c.getPhotoList().size();
            }
        }
        return result ;
    }

    //Permet de retourner le nombre de photos associées à un compte

    private int CountVideoAccount(long ID_Compte){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        int result = 0;
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte == null){
            result = 0;
        }
        else{
            for (Compte c : list_compte){
                result = c.getVideoList().size();
            }
        }
        return result ;
    }

    //Permet de retourner le nombre de photos selon le thème associées à un compte
    private int CountPhotoThemeAccount(long ID_Compte ,String NomTheme) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        int result = 0;
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte == null) {
            result = 0;
        } else {
            for (Compte c : list_compte) {
                List <Photo> list_photo = c.getPhotoList();
                if (list_photo != null){
                    for (Photo P : list_photo){
                        if (P.getTheme_Photo() != null) {
                            if (P.getTheme_Photo().getNom().equals(NomTheme)) {
                                result++;
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    //Permet de retourner le nombre de video selon le thème associée à un compte
    private int CountVideoThemeAccount(long ID_Compte ,String NomTheme) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        int result = 0;
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte == null) {
            result = 0;
        } else {
            for (Compte c : list_compte) {
                List <Video> list_video = c.getVideoList();
                if (list_video != null){
                    for (Video V : list_video){
                        if (V.getTheme_Photo() != null) {
                            if (V.getTheme_Photo().getNom().equals(NomTheme)) {
                                result++;
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private void DisplayPhotoAccountInLog(long ID_Compte){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte != null){
            for (Compte c : list_compte) {
                List<Photo> list_photo = c.getPhotoList();
                for (Photo p : list_photo) {
                    Log.i("Photo de la liste", p.getNom());
                }}}}


    private void DisplayVideoAccountInLog(long ID_Compte){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_data = daoSession.getCompteDao();
        List<Compte> list_compte = compte_data.queryBuilder()
                .where(CompteDao.Properties.Id.eq(ID_Compte))
                .list();
        if (list_compte != null){
            for (Compte c : list_compte) {
                List<Video> list_video = c.getVideoList();
                for (Video V : list_video) {
                    Log.i("Video de la liste", V.getNom());
                }}}}



    private void Show_Dialog_Photo(final Bitmap bitmap , final Bitmap bitmap_LQ) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_photo, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final ImageView photo = (ImageView) dialogview.findViewById(R.id.photo_input);
        final Spinner Spinner_ThemePhoto = (Spinner) dialogview.findViewById(R.id.Spinner_Nom_Photo);
        final EditText EditText_Nom = (EditText) dialogview.findViewById(R.id.EditTextAutre_Nom);
        LoadSpinnerThemePhoto(Spinner_ThemePhoto,EditText_Nom,getString(R.string.TextHintSpinnerPhoto));
        EditText_Nom.setHint(getString(R.string.TextNomPhoto));
        photo.setImageBitmap(bitmap_LQ);
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.TextButtonValider), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Bouton Confirmer l'enregistrement de la photo
                Save_Photo(Spinner_ThemePhoto,EditText_Nom,bitmap);
                dialog.cancel();
                bitmap.recycle();
                bitmap_LQ.recycle();
            }
        });

        builder.setNegativeButton(getString(R.string.TextAnotherPhoto), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Bouton Autre Photo
                Save_Photo(Spinner_ThemePhoto,EditText_Nom,bitmap);
                bitmap.recycle();
                bitmap_LQ.recycle();
                dialog.cancel();
                TakePictureIntent(); //Relance l'intent photo
            }
        });

        builder.setNeutralButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Annuler l'enregistrement de la photo
                bitmap.recycle();
                bitmap_LQ.recycle();
                dialog.cancel();

            }
        });

        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    private void Show_Dialog_Video(final File VideoFile , final Uri VideoURI) {
        LayoutInflater inflater = LayoutInflater.from(this);
        final View dialogview = inflater.inflate(R.layout.dialog_video, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final VideoView Lecteur_Video = (VideoView) dialogview.findViewById(R.id.video_input);
        final Spinner Spinner_ThemePhoto = (Spinner) dialogview.findViewById(R.id.Spinner_Nom_Photo);
        final EditText EditText_Nom = (EditText) dialogview.findViewById(R.id.EditTextAutre_Nom);
        EditText_Nom.setHint(getString(R.string.TextNomVideo));
        LoadSpinnerThemePhoto(Spinner_ThemePhoto,EditText_Nom,getString(R.string.TextHintSpinnerVideo));

        //Code du lecteur video avec MediaControlleur
        Lecteur_Video.setZOrderOnTop(true);
        Lecteur_Video.setVideoURI(VideoURI);
        Lecteur_Video.requestFocus();
        Lecteur_Video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp,
                                                   int width, int height) {
                        MediaController mc = new MediaController(MenuPhoto.this);
                        Lecteur_Video.setMediaController(mc);
                        mc.setAnchorView(Lecteur_Video);
                        ((ViewGroup) mc.getParent()).removeView(mc);
                        ((FrameLayout) dialogview.findViewById(R.id.videoViewWrapper))
                                .addView(mc);
                        mc.setVisibility(View.VISIBLE);}});
                Lecteur_Video.start();
            }
        });

        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.TextButtonValider), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Bouton Confirmer l'enregistrement de la video
                Save_Video(Spinner_ThemePhoto,EditText_Nom,VideoFile);
                dialog.cancel();
            }
        });

        builder.setNegativeButton(getString(R.string.TextAnotherVideo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Bouton Autre Video
                Save_Video(Spinner_ThemePhoto,EditText_Nom,VideoFile);
                dialog.cancel();
                TakeVideoIntent();
            }
        });

        builder.setNeutralButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Annuler l'enregistrement de la video
                dialog.cancel();

            }
        });

        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }

    private void Save_Photo(Spinner spinner_input, EditText edittext_input, Bitmap bitmap_input){

        String TextSpinner = spinner_input.getSelectedItem().toString();
        String TextEditText = edittext_input.getText().toString().trim();

        //Rien n'est specifié : enregistrement auto
        if (TextSpinner.equals(getString(R.string.TextHintSpinnerPhoto))){
            String Name_Photo = getPhotoName(ID_Compte_Selected);
            SaveImage(bitmap_input,Name_Photo);
            addPhotoToAccount(ID_Compte_Selected,Name_Photo);
        }
        //Nom personalisé sélectionné mais non renseigné
        if (TextSpinner.equals(getString(R.string.TextAnotherNamePhoto))&(TextUtils.isEmpty(TextEditText))){
            String Name_Photo = getPhotoName(ID_Compte_Selected);
            SaveImage(bitmap_input,Name_Photo);
            addPhotoToAccount(ID_Compte_Selected,Name_Photo);
        }

        //Theme de la photo est specifié : enregistrement auto incremente
        if (!(TextSpinner.equals(getString(R.string.TextHintSpinnerPhoto))) & (!(TextSpinner.equals(getString(R.string.TextAnotherNamePhoto))))){
            String Name_Photo = getPhotoThemeName(ID_Compte_Selected,TextSpinner);
            SaveImage(bitmap_input,Name_Photo);
            addPhotoToAccountWithTheme(ID_Compte_Selected,Name_Photo,TextSpinner);
        }

        //Nom personalisé sélectionné et renseigné
        if (TextSpinner.equals(getString(R.string.TextAnotherNamePhoto))&(!(TextUtils.isEmpty(TextEditText)))){
            String Name_Photo = getSubString(Nom_Compte) + "_" + TextEditText.replaceAll("\\s+","_");
            Name_Photo += ".jpg";
            SaveImage(bitmap_input,Name_Photo);
            addPhotoToAccount(ID_Compte_Selected,Name_Photo);
        }
    }

    private void Save_Video(Spinner spinner_input, EditText edittext_input, File video_file){

        String TextSpinner = spinner_input.getSelectedItem().toString();
        String TextEditText = edittext_input.getText().toString().trim();

        //Rien n'est specifié : enregistrement auto
        if (TextSpinner.equals(getString(R.string.TextHintSpinnerVideo))){
            String Name_Video = getVideoName(ID_Compte_Selected);
            SaveVideoStorage(video_file,Name_Video);
            addVideoToAccount(ID_Compte_Selected,Name_Video);
        }
        //Nom personalisé sélectionné mais non renseigné
        if (TextSpinner.equals(getString(R.string.TextAnotherNamePhoto))&(TextUtils.isEmpty(TextEditText))){
            String Name_Video = getVideoName(ID_Compte_Selected);
            SaveVideoStorage(video_file,Name_Video);
            addVideoToAccount(ID_Compte_Selected,Name_Video);
        }

        //Theme de la photo est specifié : enregistrement auto incremente
        if (!(TextSpinner.equals(getString(R.string.TextHintSpinnerVideo))) & (!(TextSpinner.equals(getString(R.string.TextAnotherNamePhoto))))){
            String Name_Video = getVideoThemeName(ID_Compte_Selected,TextSpinner);
            SaveVideoStorage(video_file,Name_Video);
            addVideoToAccountWithTheme(ID_Compte_Selected,Name_Video,TextSpinner);
        }

        //Nom personalisé sélectionné et renseigné
        if (TextSpinner.equals(getString(R.string.TextAnotherNamePhoto))&(!(TextUtils.isEmpty(TextEditText)))){
            String Name_Video =  getSubString(Nom_Compte) + "_" + TextEditText.replaceAll("\\s+","_");
            Name_Video += ".mp4";
            SaveVideoStorage(video_file,Name_Video);
            addVideoToAccount(ID_Compte_Selected,Name_Video);
        }
        DisplayVideoAccountInLog(ID_Compte_Selected);
    }


    //Charger le spinner Sociabilité
    private void LoadSpinnerThemePhoto(Spinner Spinner_input, final EditText Name_input,String Hint) {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Theme_PhotoDao theme_photo_data = daoSession.getTheme_PhotoDao();
        CreateListThemePhoto();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }return v;}
            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }};
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.TextAnotherNamePhoto));
        //Charger les éléments de la liste
        List<Theme_Photo> List_Theme_Photo = theme_photo_data.loadAll();
        if (List_Theme_Photo != null) {
            for (Theme_Photo TP : List_Theme_Photo) {
                adapter.add(TP.getNom()); //Ajouter les éléments
            }
        }
        adapter.add(Hint); //This is the text that will be displayed as hint.
        Spinner_input.setAdapter(adapter);
        Spinner_input.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        Spinner_input.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {public void onItemSelected(AdapterView<?> parent, View view,
                                    int position, long arg3)
        {
            if (position == 0 ){ //Cas où l'on ajoute un nom personnalisé
                Name_input.setVisibility(View.VISIBLE);
            }
            else{
                Name_input.setVisibility(View.GONE);
                Name_input.setText("", TextView.BufferType.EDITABLE);
            }
        }
            public void onNothingSelected(AdapterView<?> arg0)
            {}});
    }


    private void CreateListThemePhoto(){
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        Theme_PhotoDao theme_photo_data = daoSession.getTheme_PhotoDao();
        List<Theme_Photo> List_Theme_Photo = theme_photo_data.loadAll();
        if (List_Theme_Photo.size() == 0) {

            Theme_Photo theme_photo_input_1 = new Theme_Photo();
            theme_photo_input_1.setNom("Matériel");
            theme_photo_data.insertOrReplace(theme_photo_input_1);

            Theme_Photo theme_photo_input_2 = new Theme_Photo();
            theme_photo_input_2.setNom("Bâtiment");
            theme_photo_data.insertOrReplace(theme_photo_input_2);

            Theme_Photo theme_photo_input_3 = new Theme_Photo();
            theme_photo_input_3.setNom("Exploitation");
            theme_photo_data.insertOrReplace(theme_photo_input_3);

            Theme_Photo theme_photo_input_4 = new Theme_Photo();
            theme_photo_input_4.setNom("Ressource");
            theme_photo_data.insertOrReplace(theme_photo_input_4);
        }
    }

    //Method to check the size of the storage
    private void CheckStorageSize(){
        double Available_Memory = getAvailableExternalMemorySize();
        double Total_Memory = getTotalExternalMemorySize();
        double Pourcentage = (Available_Memory / Total_Memory)*100;
        Log.i("Info Memoire","Taux d'espace libre : " + Pourcentage);
        if (Pourcentage <= 10){
            Show_Dialog(getString(R.string.TitleCheckStorage),getString(R.string.TextCheckStorage));
        }
    }


    //Retourne la mémoire disponible pour un stockage externe
    public double getAvailableExternalMemorySize() {
            File path = getDirectoryPath();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSizeLong();
            long availableBlocks = stat.getAvailableBlocksLong();
            return (availableBlocks * blockSize);
    }

    //Retourne la mémoire totale pour un stockage externe
    public double getTotalExternalMemorySize() {
            File path = getDirectoryPath();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSizeLong();
            long totalBlocks = stat.getBlockCountLong();
            return (totalBlocks * blockSize);
    }

    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuPhoto.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }


}
