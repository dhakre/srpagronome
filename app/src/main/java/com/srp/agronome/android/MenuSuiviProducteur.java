package com.srp.agronome.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.srp.agronome.android.db.Incident_Administratif;

/**
 * Created by vincent on 28/08/17.
 */


public class MenuSuiviProducteur extends AppCompatActivity {

    private Button Btn_Data_SAU_Source = null;
    private Button Btn_Visite = null;
    private Button Btn_Prix_Marche = null;
    private Button Btn_Conseil = null;
    private Button Btn_Incident = null;

    private ImageView Btn_Retour = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_suivi_producteur);

        Btn_Data_SAU_Source = (Button) findViewById(R.id.button_data_sau_information_source);
        Btn_Data_SAU_Source.setOnClickListener(ButtonDataSAUSourceHandler);

        Btn_Visite = (Button) findViewById(R.id.button_visite);
        Btn_Visite.setOnClickListener(ButtonVisiteHandler);

        Btn_Prix_Marche = (Button) findViewById(R.id.button_market_price);
        Btn_Prix_Marche.setOnClickListener(ButtonPrixMarcheHandler);

        Btn_Conseil = (Button) findViewById(R.id.button_technical_advice);
        Btn_Conseil.setOnClickListener(ButtonConseilHandler);

        Btn_Incident = (Button) findViewById(R.id.button_administrative_incident);
        Btn_Incident.setOnClickListener(ButtonIncidentHandler);

        Btn_Retour = (ImageView) findViewById(R.id.arrow_back_menu_suivi_producteur);
        Btn_Retour.setOnClickListener(ButtonRetourHandler);

    }//End Oncreate


    //Bouton Information SAU Source
    View.OnClickListener ButtonDataSAUSourceHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuSuiviProducteur.this, SuiviProducteur.class);
            finish();
            startActivity(intent);
        }
    };

    //Bouton Visite
    View.OnClickListener ButtonVisiteHandler = new View.OnClickListener() {
        public void onClick(View v) {
          //  Toast.makeText(getApplicationContext(),"En cours de développement",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MenuSuiviProducteur.this, MenuVisite.class);
            finish();
            startActivity(intent);
        }
    };

    //Bouton Prix marché
    View.OnClickListener ButtonPrixMarcheHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(),"En cours de développement",Toast.LENGTH_SHORT).show();
        }
    };

    //Bouton Conseil Technique
    View.OnClickListener ButtonConseilHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(),"En cours de développement",Toast.LENGTH_SHORT).show();
        }
    };

    //Bouton Incident Administratif
    View.OnClickListener ButtonIncidentHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuSuiviProducteur.this, IncidentAdministratif.class);
            finish();
            startActivity(intent);
        }
    };

    //Bouton Retour
    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MenuSuiviProducteur.this, MenuAgriculteur.class);
            finish();
            startActivity(intent);
        }
    };

    //Event BackPressed
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(MenuSuiviProducteur.this, MenuAgriculteur.class);
            finish();
            startActivity(intent);
        }
        return true;
    }

}