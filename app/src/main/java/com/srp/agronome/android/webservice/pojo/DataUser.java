package com.srp.agronome.android.webservice.pojo;

/**
 * Created by soytouchrp on 05/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataUser {

    @SerializedName("ID_FOURNISSEUR")
    @Expose
    private int idFournisseur;
    @SerializedName("NOM")
    @Expose
    private String NOM;
    @SerializedName("MAIL")
    @Expose
    private String mailFournisseur;
    @SerializedName("TEL")
    @Expose
    private String telFournisseur;
    @SerializedName("TEL_P")
    @Expose
    private String portableFournisseur;
    @SerializedName("CREATION_DATE")
    @Expose
    private String creationDate;
    @SerializedName("MODIFICATION_DATE")
    @Expose
    private String modificationDate;
    @SerializedName("adresseSiege")
    @Expose
    private DataAdresse adresseSiege;

    public int getIdFournisseur() {
        return idFournisseur;
    }

    public void setIdFournisseur(int idFournisseur) {
        this.idFournisseur = idFournisseur;
    }

    public String getNomFournisseur() {
        return NOM;
    }

    public void setNomFournisseur(String nomFournisseur) {
        this.NOM = nomFournisseur;
    }

    public String getMailFournisseur() {
        return mailFournisseur;
    }

    public void setMailFournisseur(String mailFournisseur) {
        this.mailFournisseur = mailFournisseur;
    }

    public String getTelFournisseur() {
        return telFournisseur;
    }

    public void setTelFournisseur(String telFournisseur) {
        this.telFournisseur = telFournisseur;
    }

    public String getPortableFournisseur() {
        return portableFournisseur;
    }

    public void setPortableFournisseur(String portableFournisseur) {
        this.portableFournisseur = portableFournisseur;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public DataAdresse getAdresseSiege() {
        return adresseSiege;
    }

    public void setAdresseSiege(DataAdresse adresseSiege) {
        this.adresseSiege = adresseSiege;
    }
}



//
//-----------------------------------com.example.AdresseSiege.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//
//}
//-----------------------------------com.example.CREATIONDATE.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class CREATIONDATE {
//
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("timezone_type")
//    @Expose
//    private Integer timezoneType;
//    @SerializedName("timezone")
//    @Expose
//    private String timezone;
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public Integer getTimezoneType() {
//        return timezoneType;
//    }
//
//    public void setTimezoneType(Integer timezoneType) {
//        this.timezoneType = timezoneType;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//}
//-----------------------------------com.example.CreationDate.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class CreationDate {
//
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("timezone_type")
//    @Expose
//    private Integer timezoneType;
//    @SerializedName("timezone")
//    @Expose
//    private String timezone;
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public Integer getTimezoneType() {
//        return timezoneType;
//    }
//
//    public void setTimezoneType(Integer timezoneType) {
//        this.timezoneType = timezoneType;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//}
//-----------------------------------com.example.Data.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class Data {
//
//    @SerializedName("id_fournisseur")
//    @Expose
//    private String idFournisseur;
//    @SerializedName("nom_fournisseur")
//    @Expose
//    private String nomFournisseur;
//    @SerializedName("mail_fournisseur")
//    @Expose
//    private String mailFournisseur;
//    @SerializedName("tel_fournisseur")
//    @Expose
//    private String telFournisseur;
//    @SerializedName("portable_fournisseur")
//    @Expose
//    private String portableFournisseur;
//    @SerializedName("creation_date")
//    @Expose
//    private CreationDate creationDate;
//    @SerializedName("modification_date")
//    @Expose
//    private ModificationDate modificationDate;
//    @SerializedName("adresse_siege")
//    @Expose
//    private AdresseSiege adresseSiege;
//
//    public String getIdFournisseur() {
//        return idFournisseur;
//    }
//
//    public void setIdFournisseur(String idFournisseur) {
//        this.idFournisseur = idFournisseur;
//    }
//
//    public String getNomFournisseur() {
//        return nomFournisseur;
//    }
//
//    public void setNomFournisseur(String nomFournisseur) {
//        this.nomFournisseur = nomFournisseur;
//    }
//
//    public String getMailFournisseur() {
//        return mailFournisseur;
//    }
//
//    public void setMailFournisseur(String mailFournisseur) {
//        this.mailFournisseur = mailFournisseur;
//    }
//
//    public String getTelFournisseur() {
//        return telFournisseur;
//    }
//
//    public void setTelFournisseur(String telFournisseur) {
//        this.telFournisseur = telFournisseur;
//    }
//
//    public String getPortableFournisseur() {
//        return portableFournisseur;
//    }
//
//    public void setPortableFournisseur(String portableFournisseur) {
//        this.portableFournisseur = portableFournisseur;
//    }
//
//
//}
//-----------------------------------com.example.Example.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class Example {
//
//    @SerializedName("data")
//    @Expose
//    private Data data;
//
//    public Data getData() {
//        return data;
//    }
//
//    public void setData(Data data) {
//        this.data = data;
//    }
//
//}
//-----------------------------------com.example.MODIFICATIONDATE.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class MODIFICATIONDATE {
//
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("timezone_type")
//    @Expose
//    private Integer timezoneType;
//    @SerializedName("timezone")
//    @Expose
//    private String timezone;
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public Integer getTimezoneType() {
//        return timezoneType;
//    }
//
//    public void setTimezoneType(Integer timezoneType) {
//        this.timezoneType = timezoneType;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//}
//-----------------------------------com.example.ModificationDate.java-----------------------------------
//
//        package com.example;
//
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//
//public class ModificationDate {
//
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("timezone_type")
//    @Expose
//    private Integer timezoneType;
//    @SerializedName("timezone")
//    @Expose
//    private String timezone;
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public Integer getTimezoneType() {
//        return timezoneType;
//    }
//
//    public void setTimezoneType(Integer timezoneType) {
//        this.timezoneType = timezoneType;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//}