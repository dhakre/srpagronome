package com.srp.agronome.android.db;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.SqlUtils;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "CONTRE__VISITE".
*/
public class CONTRE_VISITEDao extends AbstractDao<CONTRE_VISITE, Long> {

    public static final String TABLENAME = "CONTRE__VISITE";

    /**
     * Properties of entity CONTRE_VISITE.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property DATE_CONTRE_VISITE     = new Property(1, java.util.Date.class, "DATE_CONTRE_VISITE    ", false, "DATE__CONTRE__VISITE    ");
        public final static Property REMARQUE_CONTRE_VISITE      = new Property(2, String.class, "REMARQUE_CONTRE_VISITE     ", false, "REMARQUE__CONTRE__VISITE     ");
        public final static Property Archive = new Property(3, int.class, "archive", false, "ARCHIVE");
        public final static Property ID_Agronome_Creation = new Property(4, int.class, "ID_Agronome_Creation", false, "ID__AGRONOME__CREATION");
        public final static Property Date_creation = new Property(5, java.util.Date.class, "date_creation", false, "DATE_CREATION");
        public final static Property ID_Agronome_Modification = new Property(6, int.class, "ID_Agronome_Modification", false, "ID__AGRONOME__MODIFICATION");
        public final static Property Date_modification = new Property(7, java.util.Date.class, "date_modification", false, "DATE_MODIFICATION");
        public final static Property ID_ETAT_CONTRE_VISITE  = new Property(8, Long.class, "ID_ETAT_CONTRE_VISITE ", false, "ID__ETAT__CONTRE__VISITE ");
        public final static Property ID_MESSAGE_CONTRE_VISITE     = new Property(9, Long.class, "ID_MESSAGE_CONTRE_VISITE    ", false, "ID__MESSAGE__CONTRE__VISITE    ");
        public final static Property ID_OBJET_MESSAGE      = new Property(10, Long.class, "ID_OBJET_MESSAGE     ", false, "ID__OBJET__MESSAGE     ");
    }

    private DaoSession daoSession;


    public CONTRE_VISITEDao(DaoConfig config) {
        super(config);
    }
    
    public CONTRE_VISITEDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"CONTRE__VISITE\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"DATE__CONTRE__VISITE    \" INTEGER," + // 1: DATE_CONTRE_VISITE    
                "\"REMARQUE__CONTRE__VISITE     \" TEXT," + // 2: REMARQUE_CONTRE_VISITE     
                "\"ARCHIVE\" INTEGER NOT NULL ," + // 3: archive
                "\"ID__AGRONOME__CREATION\" INTEGER NOT NULL ," + // 4: ID_Agronome_Creation
                "\"DATE_CREATION\" INTEGER NOT NULL ," + // 5: date_creation
                "\"ID__AGRONOME__MODIFICATION\" INTEGER NOT NULL ," + // 6: ID_Agronome_Modification
                "\"DATE_MODIFICATION\" INTEGER NOT NULL ," + // 7: date_modification
                "\"ID__ETAT__CONTRE__VISITE \" INTEGER," + // 8: ID_ETAT_CONTRE_VISITE 
                "\"ID__MESSAGE__CONTRE__VISITE    \" INTEGER," + // 9: ID_MESSAGE_CONTRE_VISITE    
                "\"ID__OBJET__MESSAGE     \" INTEGER);"); // 10: ID_OBJET_MESSAGE     
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"CONTRE__VISITE\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, CONTRE_VISITE entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        java.util.Date DATE_CONTRE_VISITE     = entity.getDATE_CONTRE_VISITE    ();
        if (DATE_CONTRE_VISITE     != null) {
            stmt.bindLong(2, DATE_CONTRE_VISITE    .getTime());
        }
 
        String REMARQUE_CONTRE_VISITE      = entity.getREMARQUE_CONTRE_VISITE     ();
        if (REMARQUE_CONTRE_VISITE      != null) {
            stmt.bindString(3, REMARQUE_CONTRE_VISITE     );
        }
        stmt.bindLong(4, entity.getArchive());
        stmt.bindLong(5, entity.getID_Agronome_Creation());
        stmt.bindLong(6, entity.getDate_creation().getTime());
        stmt.bindLong(7, entity.getID_Agronome_Modification());
        stmt.bindLong(8, entity.getDate_modification().getTime());
 
        Long ID_ETAT_CONTRE_VISITE  = entity.getID_ETAT_CONTRE_VISITE ();
        if (ID_ETAT_CONTRE_VISITE  != null) {
            stmt.bindLong(9, ID_ETAT_CONTRE_VISITE );
        }
 
        Long ID_MESSAGE_CONTRE_VISITE     = entity.getID_MESSAGE_CONTRE_VISITE    ();
        if (ID_MESSAGE_CONTRE_VISITE     != null) {
            stmt.bindLong(10, ID_MESSAGE_CONTRE_VISITE    );
        }
 
        Long ID_OBJET_MESSAGE      = entity.getID_OBJET_MESSAGE     ();
        if (ID_OBJET_MESSAGE      != null) {
            stmt.bindLong(11, ID_OBJET_MESSAGE     );
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, CONTRE_VISITE entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        java.util.Date DATE_CONTRE_VISITE     = entity.getDATE_CONTRE_VISITE    ();
        if (DATE_CONTRE_VISITE     != null) {
            stmt.bindLong(2, DATE_CONTRE_VISITE    .getTime());
        }
 
        String REMARQUE_CONTRE_VISITE      = entity.getREMARQUE_CONTRE_VISITE     ();
        if (REMARQUE_CONTRE_VISITE      != null) {
            stmt.bindString(3, REMARQUE_CONTRE_VISITE     );
        }
        stmt.bindLong(4, entity.getArchive());
        stmt.bindLong(5, entity.getID_Agronome_Creation());
        stmt.bindLong(6, entity.getDate_creation().getTime());
        stmt.bindLong(7, entity.getID_Agronome_Modification());
        stmt.bindLong(8, entity.getDate_modification().getTime());
 
        Long ID_ETAT_CONTRE_VISITE  = entity.getID_ETAT_CONTRE_VISITE ();
        if (ID_ETAT_CONTRE_VISITE  != null) {
            stmt.bindLong(9, ID_ETAT_CONTRE_VISITE );
        }
 
        Long ID_MESSAGE_CONTRE_VISITE     = entity.getID_MESSAGE_CONTRE_VISITE    ();
        if (ID_MESSAGE_CONTRE_VISITE     != null) {
            stmt.bindLong(10, ID_MESSAGE_CONTRE_VISITE    );
        }
 
        Long ID_OBJET_MESSAGE      = entity.getID_OBJET_MESSAGE     ();
        if (ID_OBJET_MESSAGE      != null) {
            stmt.bindLong(11, ID_OBJET_MESSAGE     );
        }
    }

    @Override
    protected final void attachEntity(CONTRE_VISITE entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public CONTRE_VISITE readEntity(Cursor cursor, int offset) {
        CONTRE_VISITE entity = new CONTRE_VISITE( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : new java.util.Date(cursor.getLong(offset + 1)), // DATE_CONTRE_VISITE    
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // REMARQUE_CONTRE_VISITE     
            cursor.getInt(offset + 3), // archive
            cursor.getInt(offset + 4), // ID_Agronome_Creation
            new java.util.Date(cursor.getLong(offset + 5)), // date_creation
            cursor.getInt(offset + 6), // ID_Agronome_Modification
            new java.util.Date(cursor.getLong(offset + 7)), // date_modification
            cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8), // ID_ETAT_CONTRE_VISITE 
            cursor.isNull(offset + 9) ? null : cursor.getLong(offset + 9), // ID_MESSAGE_CONTRE_VISITE    
            cursor.isNull(offset + 10) ? null : cursor.getLong(offset + 10) // ID_OBJET_MESSAGE     
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, CONTRE_VISITE entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setDATE_CONTRE_VISITE    (cursor.isNull(offset + 1) ? null : new java.util.Date(cursor.getLong(offset + 1)));
        entity.setREMARQUE_CONTRE_VISITE     (cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setArchive(cursor.getInt(offset + 3));
        entity.setID_Agronome_Creation(cursor.getInt(offset + 4));
        entity.setDate_creation(new java.util.Date(cursor.getLong(offset + 5)));
        entity.setID_Agronome_Modification(cursor.getInt(offset + 6));
        entity.setDate_modification(new java.util.Date(cursor.getLong(offset + 7)));
        entity.setID_ETAT_CONTRE_VISITE (cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8));
        entity.setID_MESSAGE_CONTRE_VISITE    (cursor.isNull(offset + 9) ? null : cursor.getLong(offset + 9));
        entity.setID_OBJET_MESSAGE     (cursor.isNull(offset + 10) ? null : cursor.getLong(offset + 10));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(CONTRE_VISITE entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(CONTRE_VISITE entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(CONTRE_VISITE entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getETAT_CONTRE_VISITEDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T1", daoSession.getMESSAGEDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T2", daoSession.getOBJET_MESSAGEDao().getAllColumns());
            builder.append(" FROM CONTRE__VISITE T");
            builder.append(" LEFT JOIN ETAT__CONTRE__VISITE T0 ON T.\"ID__ETAT__CONTRE__VISITE \"=T0.\"_id\"");
            builder.append(" LEFT JOIN MESSAGE T1 ON T.\"ID__MESSAGE__CONTRE__VISITE    \"=T1.\"_id\"");
            builder.append(" LEFT JOIN OBJET__MESSAGE T2 ON T.\"ID__OBJET__MESSAGE     \"=T2.\"_id\"");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected CONTRE_VISITE loadCurrentDeep(Cursor cursor, boolean lock) {
        CONTRE_VISITE entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        ETAT_CONTRE_VISITE eTAT_CONTRE_VISITE = loadCurrentOther(daoSession.getETAT_CONTRE_VISITEDao(), cursor, offset);
        entity.setETAT_CONTRE_VISITE(eTAT_CONTRE_VISITE);
        offset += daoSession.getETAT_CONTRE_VISITEDao().getAllColumns().length;

        MESSAGE mESSAGE = loadCurrentOther(daoSession.getMESSAGEDao(), cursor, offset);
        entity.setMESSAGE(mESSAGE);
        offset += daoSession.getMESSAGEDao().getAllColumns().length;

        OBJET_MESSAGE oBJET_MESSAGE = loadCurrentOther(daoSession.getOBJET_MESSAGEDao(), cursor, offset);
        entity.setOBJET_MESSAGE(oBJET_MESSAGE);

        return entity;    
    }

    public CONTRE_VISITE loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<CONTRE_VISITE> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<CONTRE_VISITE> list = new ArrayList<CONTRE_VISITE>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<CONTRE_VISITE> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<CONTRE_VISITE> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
