package com.srp.agronome.android;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.srp.agronome.android.db.*;
import com.srp.agronome.android.db.Contact;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.srp.agronome.android.MainLogin.ID_Contact_Selected;
import static com.srp.agronome.android.MainLogin.ID_Entreprise_Selected;
import static com.srp.agronome.android.MainLogin.Nom_Compte;
import static com.srp.agronome.android.R.id.map;


public class MapsActivity extends FragmentActivity implements
        GoogleApiClient.ConnectionCallbacks,
        OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        LocationListener {

    public static final String TAG = MapsActivity.class.getSimpleName();


    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;


    private GoogleMap mMap = null; // Might be null if Google Play services APK is not available.

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private  LocationManager locationManager;
    private SupportMapFragment dummtMap;

    private Spinner BtnOption = null;

    private ImageButton BtnRetour = null;

    private MyTextView Raison_Sociale = null;
    private MyTextView Nom_Prenom = null;
    private MyTextView Numero_Contact = null;
    private MyTextView CodePostale = null;
    private MyTextView Ville = null;



    private ImageButton Direction = null;
    private ImageButton ContactProfil = null;

    List<LatLng> l=null;

    Location userlocation=null;
    ArrayList<String> contactNames=new ArrayList<>();
    List<String> contactIdentity=null;



    private HashMap<Marker, Contact> eventMarkerMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //set the map variable
        //mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync();

       /* SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/



        Log.i(TAG, "onCreate: starting of the maps Activity");

        Raison_Sociale = (MyTextView) findViewById(R.id.TxtRS);
        Nom_Prenom = (MyTextView) findViewById(R.id.TxtName);
        Numero_Contact = (MyTextView) findViewById(R.id.TxtNumberID);
        CodePostale = (MyTextView) findViewById(R.id.TxtCodePostal);
        Ville = (MyTextView) findViewById(R.id.TxtVille);


        BtnOption = (Spinner) findViewById(R.id.option_id);

        BtnRetour = (ImageButton) findViewById(R.id.arrow_back_contact);
        BtnRetour.setOnClickListener(ButtonRetourHandler);



        Log.i(TAG, "onCreate MapsActivity: running setUpMapIfNeeded()");
        setUpMapIfNeeded();
        Log.i(TAG, "onCreate MapsActivity: running LoadZoneInSpinner()");
       //  LoadZoneInSpinner();


        //displayContactInLog();

//        //Selection_zone_Choice();
//        for (int i = 0; i < getListgps().size(); i++) {
//            Log.i("gps", getListgps().get(i).toString());
//        }


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds


        initListeners();

        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ZoneDao zoneDao = daoSession.getZoneDao();

       // List<LatLng> ll =getListgps();
        l = getListgps();

        int lsize=l.size();
        //Log.i(TAG, "size of Latlang in MapsActivity: "+lsize);
        //setMarkerWithData(l);

        // Show_Dialog("Information ", "En developpement");
        //call aysnc task
        MapMarkerFunctions mAsync=new MapMarkerFunctions();
        mAsync.execute();


        // Fixing Later Map loading Delay
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MapView mv = new MapView(getApplicationContext());
                    mv.onCreate(null);
                    mv.onPause();
                    mv.onDestroy();
                }catch (Exception ignored){

                }
            }
        }).start();


    }//end onCreate

    View.OnClickListener ButtonRetourHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(MapsActivity.this, ContactMenu.class);
            finish();
            startActivity(intent);
        }
    };

    //load dummy map




    private void initListeners() {
       //crash
      //  mMap.setOnMarkerClickListener(this);
        //mMap.setOnMapLongClickListener(this);
        //mMap.setOnInfoWindowClickListener(this);
        //mMap.setOnMapClickListener(this);
    }

    //set marker
    private void setMarkerWithData(List<LatLng> lt)
    {
        for(LatLng l: lt)
        {
            double lat=l.latitude;
            double lon=l.longitude;
             this.mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lon))
                    .title("new point"));

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.

            SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(map);
           //
              mapFrag.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    //user location
                    if(userlocation!=null)
                    {
                        double userlat = userlocation.getLatitude();
                        double userlon = userlocation.getLatitude();

                        //add marker to the map
                        for (LatLng ll : l) {
                            double lat = ll.latitude;
                            double lon = ll.longitude;
                            double distance = distance(userlat, userlon, lat, lon);
                            Log.i(TAG, "onMapReady: distance calculates="+distance);
                            //check distance radius 30 Km Radius
                            if (distance <= 40) {
                                Log.i(TAG, "onMapReady: setting marker inside radius");
                                mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).title(getLocationName(ll)));
                            } else {
                                Log.i(TAG, "onMapReady: out of the radius");
                            }
                           // mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).title("new point"));
                        }
                    }

                  if(userlocation!=null)
                  {
                      //setMarkerFor30(userlocation);
                  }

                }
            });


            //MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

            mapFrag.getMapAsync(this);




           // mMap.setMyLocationEnabled(true);
            // mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

        }

        // Check if we were successful in obtaining the map.
        if (mMap != null) {
            setUpMap();
            setMarkerFor30(userlocation);


        }
    }



    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {


        LocationManagerCheck locationManagerCheck = new LocationManagerCheck(
                this);
        Location location;

        if (locationManagerCheck.isLocationServiceAvailable()) {

            if (locationManagerCheck.getProviderType() == 1)
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
           // location=getLastKnownLocation();
            if (locationManagerCheck.getProviderType() == 2)
                location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

           // setMarkerFor30(location);

            //get user location
            userlocation=new Location(location);

            handleNewLocation(location);
            //set Marker
            initCamera(location);

            //set marker for radius



        } else {
            locationManagerCheck.createLocationServiceError(MapsActivity.this);
        }


//
    }

    //set marker for the radius of 30 Km
    private void setMarkerFor30(Location userloc)
    {
        Log.i(TAG, "setMarkerFor30: inside function");
        if(userloc!=null)
        {
            for(LatLng lal:l)
            {
                if(distance(userloc.getLatitude(),userloc.getLongitude(),lal.latitude,lal.longitude)<=30)
                {
                    //set marker
                    Log.i(TAG, "setMarkerFor30: contact Name="+getLocationName(lal));
                    mMap.addMarker(new MarkerOptions().position(new LatLng(lal.latitude,lal.longitude)).title(getLocationName(lal)));
                }
            }
        }
        else
        {
            Log.i(TAG, "setMarkerFor30: user location is null");
        }
    }
    //get name of the location

    String getLocationName(LatLng location)
    {
        Log.i(TAG, "getLocationName: inside function");
        String Cname = null;
        for(int i=0;i<l.size();i++)
        {
            if(location.equals(l.get(i)))
            {
                Cname=contactNames.get(i);
            }
        }
        return Cname;

    }

    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            //Log.d("last known location, provider: %s, location: %s", provider,l);
            Log.i(TAG, "getLastKnownLocation: "+provider+ l);

            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                Log.i(TAG, "getLastKnownLocation: Last best known location"+ l);
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

//    private void InsertGps(Double latitude, Double longitude) {
//
//        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
//        GeolocalisationDao geolocalisation_dao = daoSession.getGeolocalisationDao();
//
//        Geolocalisation geolocalisation = new Geolocalisation();
//        geolocalisation.setLatitude(latitude);
//        geolocalisation.setLongitude(longitude);
//        geolocalisation_dao.insert(geolocalisation);
//
//
//    }

    private void InsertZone(String nom, int rayon, LatLng centre) {

        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        GeolocalisationDao geolocalisation_dao = daoSession.getGeolocalisationDao();

        ZoneDao zoneDao = daoSession.getZoneDao();

        Zone zone_input = new Zone();
        zone_input.setNom(nom);
        zone_input.setRayon(rayon);
        Geolocalisation geolocalisation = new Geolocalisation();
        geolocalisation.setLatitude(centre.latitude);
        geolocalisation.setLongitude(centre.longitude);
        geolocalisation_dao.insertOrReplace(geolocalisation);
        zone_input.setGeolocalisation(geolocalisation);
        zone_input.setID_Geolocalisation(geolocalisation.getId());
        zoneDao.insertOrReplace(zone_input);

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(MapsActivity.this, ContactMenu.class);
            finish();
            startActivity(intent);

        }
        return true;
    }

    private Address getLocation(String adresse) {
        Geocoder coder = new Geocoder(this);
        List<Address> address = null;
        try {
            address = coder.getFromLocationName(adresse, 5);

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        if (address == null) {
            return null;
        }
        if (address.size() != 0) {
            Address location = address.get(0);
            return location;

        } else return null;


    }


    private List<LatLng> getListgps() {

        String adresseTxt = "";


        List<LatLng> latLngs = new ArrayList<>();
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        CompteDao compte_dao = daoSession.getCompteDao();
        List<Compte> update_compte = compte_dao.queryBuilder()
                .where(CompteDao.Properties.Login.eq(Nom_Compte))
                .list();
        if (update_compte != null) {
            for (Compte c : update_compte) {
                List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                for (Entreprise e : List_Entreprise) {
                    List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                    for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                        //get contact names
                       // contactIdentity.add(ct.getContact_identifiant());
                        String cname=ct.getIdentite().getNom()+" "+ct.getIdentite().getPrenom();
                       // Log.i(TAG, "getListgps: cname="+cname);
                        contactNames.add(ct.getIdentite().getNom()); //get contact name
                        //Log.i(TAG, "name contact"+ct.getIdentite().getNom());
                        //Log.i(TAG, "getListgps: contact list size="+contactNames.size());
                       // Log.i(TAG, "getListgps: contact identifier"+ct.getContact_identifiant());
                        if (ct.getIdentite().getAdresse() == null) {
                            continue;
                        }
                        adresseTxt = ct.getIdentite().getAdresse().getAdresse_numero_rue() + " " + ct.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal() + " " + ct.getIdentite().getAdresse().getCode_Postal_Ville().getVille();
                        if (getLocation(adresseTxt) != null) {
                            if(latLngs.size()>=400) {
                                return latLngs;
                            }
                                else {
                                latLngs.add(new LatLng(getLocation(adresseTxt).getLatitude(), getLocation(adresseTxt).getLongitude()));
                                // Log.i(TAG, "latlngs: "+latLngs.size());
                            }

                        }

                    }

                }
            }
        }

        Log.i(TAG, "latlang: totalsize"+latLngs.size());
        return (latLngs);
    }


    @Override
    public void onMapReady(GoogleMap map) {
        setUpMapIfNeeded();
       // mMap=map;

    }

    private void handleNewLocation(Location location) {
        //Log.d("MapsActivity", location.toString());

        double currentLatitude = location.getLatitude(); //null
        double currentLongitude = location.getLongitude();



        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

        mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("I am here!");
        mMap.addMarker(options);
        //get user location
        //userlocation=new Location(location);


        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //handleNewLocation(location);

        }

        //initCamera(location);
    }

    //get contact names according to the location
    void getContactNames(List<String> contactIdentity)
    {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ContactDao contact_data=daoSession.getContactDao();
        EntrepriseDao enterprise_data=daoSession.getEntrepriseDao();

        //list contact
        List<Contact> contactList=contact_data.loadAll();
        for(Contact c: contactList)
        {
            for(String ct: contactIdentity)
            {
                //if(ct==c.)
            }
        }

        //enterprise
        List<Entreprise> enterpriseList=enterprise_data.loadAll();
        for (Entreprise e:enterpriseList)
        {
            //contactIdentity.add(e.get)
        }

    }

    private void initCamera(Location location) {
        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(location.getLatitude(),
                        location.getLongitude()))
                .zoom(16f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);
        mMap.setTrafficEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Toast.makeText(MapsActivity.this,
                "onInfoWindowClick():\n" +
                        marker.getPosition().latitude + "\n" +
                        marker.getTitle(),
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onMapClick(LatLng latLng) {


    }

    @Override
    public void onMapLongClick(LatLng latLng) {


        MarkerOptions markerOptions = new MarkerOptions();

        // Setting the position for the marker
        markerOptions.position(latLng);

        // Setting the title for the marker.
        // This will be displayed on taping the marker
        markerOptions.title(latLng.latitude + " : " + latLng.longitude);

        // Clears the previously touched position
        mMap.clear();

        // Animating to the touched position
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

        // Placing a marker on the touched position
        mMap.addMarker(markerOptions);

        Show_Dialog_EditText("Ajout Zone", "Ajouter une zone", latLng);
    }


    @Override
    public boolean onMarkerClick(final Marker arg0) {
//
//        Toast.makeText(MapsActivity.this,
//                "onInfoWindowClick():\n" +
//                        arg0.getPosition().latitude + "\n" +
//                        arg0.getTitle(),
//                Toast.LENGTH_LONG).show();


//

        final Contact c = eventMarkerMap.get(arg0);


        if (arg0.getSnippet() != null) {
            mMap.moveCamera(CameraUpdateFactory.zoomIn());
            return true;
        }
        //arg0.showInfoWindow();
        //final DataClass data = myMapData.get(arg0);
        final Dialog d = new Dialog(MapsActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setTitle("Select");
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        d.setContentView(R.layout.info_content);

        Raison_Sociale = (MyTextView) d.findViewById(R.id.TxtRS);
        Nom_Prenom = (MyTextView) d.findViewById(R.id.TxtName);
        Numero_Contact = (MyTextView) d.findViewById(R.id.TxtNumberID);
        CodePostale = (MyTextView) d.findViewById(R.id.TxtCodePostal);
        Ville = (MyTextView) d.findViewById(R.id.TxtVille);
        ImageView ivPhoto = (ImageView) d.findViewById(R.id.CPhoto);
        try {
            Nom_Prenom.setText(eventMarkerMap.get(arg0).getIdentite().getNom());
            Numero_Contact.setText("N°:" + eventMarkerMap.get(arg0).getId().toString());
            CodePostale.setText(eventMarkerMap.get(arg0).getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal());
            Ville.setText(eventMarkerMap.get(arg0).getIdentite().getAdresse().getCode_Postal_Ville().getVille());
        } catch (Exception e) {
            System.out.println("Null Pointer ");
        }

        Direction = (ImageButton) d.findViewById(R.id.btn_aller);
        Direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Adresse = c.getIdentite().getAdresse().getAdresse_numero_rue();
                String CP = c.getIdentite().getAdresse().getCode_Postal_Ville().getCode_postal();
                String Ville = c.getIdentite().getAdresse().getCode_Postal_Ville().getVille();
                if (TextUtils.isEmpty(Adresse) || (TextUtils.isEmpty(CP)) || (TextUtils.isEmpty(Ville))) {
                    Show_Dialog("Erreur", "Veuillez d'abord renseigner l'adresse du contact");
                } else { // getLatitude() ; getLongitude();
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + Adresse + " " + CP + " " + Ville);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(mapIntent);
                }
            }
        });
//        Direction.setOnClickListener(ButtonCarteHandlerAgriculteur);
        ContactProfil = (ImageButton) d.findViewById(R.id.btn_profils);


        ivPhoto.setImageResource(R.drawable.face);

        final LatLng l = arg0.getPosition();

        //   TextView tvName = (TextView) d.findViewById(R.id.infocontent_tv_name);
        // tvName.setText(arg0.getTitle());

        // TextView tvType = (TextView) d.findViewById(R.id.infocontent_tv_type);
        //tvType.setText("(" + arg0.getTitle() + ")");

        //TextView tvDesc = (TextView) d.findViewById(R.id.infocontent_tv_desc);


        //extView tvAddr = (TextView) d.findViewById(R.id.infocontent_tv_addr);
        //tvAddr.setText(Html.fromHtml(arg0.getTitle()));

        ContactProfil.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ID_Entreprise_Selected = Long.parseLong(arg0.getTitle());
                ID_Contact_Selected = (c.getId());
                Intent next = new Intent(getApplicationContext(), MenuAgriculteur.class);
                next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(next);
            }
        });

        d.show();
        return true;


        //Intent intent = new Intent(MapsActivity.this, ContactProfil.class);
        //startActivity(intent);
        //finish();


    }


    private void drawCircle(LatLng location, int radius) {
        CircleOptions options = new CircleOptions();
        options.center(location);
        //Radius in meters
        options.radius(radius);

        options.fillColor(getResources()
                .getColor(R.color.zoneA));
        options.strokeColor(getResources()
                .getColor(R.color.zoneA));


        options.strokeWidth(10);
        mMap.addCircle(options);
    }

    private static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1.609344;
        return (dist);// return distance en km
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::	This function converts decimal degrees to radians						 :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::	This function converts radians to decimal degrees						 :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }


    //Charger le spinner
    private void LoadZoneInSpinner() {
        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        ZoneDao zoneDao = daoSession.getZoneDao();
        List<Zone> update_zone = zoneDao.queryBuilder()
                .list();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MapsActivity.this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {   //error cannot find convertView

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorBackgroundDialog));
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.AjoutZone));
        //Charger la liste des zones
        if (update_zone != null) {
            for (Zone z : update_zone) {

                adapter.add(z.getNom());

            }
        }
        adapter.add(getString(R.string.Optionaffichage)); //This is the text that will be displayed as hint.


        BtnOption.setAdapter(adapter);
        BtnOption.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        BtnOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() //Event
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3) {

                if (position == 0) { //Cas où l'on ajoute une zone
                    Show_Dialog("Information", getString(R.string.AjoutZonepop));

                } else {

                    DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
                    ZoneDao zoneDao = daoSession.getZoneDao();

                    List<Zone> update_zone = zoneDao.queryBuilder()
                            .list();
                    //get list


                    //contact
                    CompteDao compte_dao = daoSession.getCompteDao();
                    List<Compte> update_compte = compte_dao.queryBuilder()
                            .where(CompteDao.Properties.Login.eq(Nom_Compte))
                            .list();


                    if (update_zone != null) {
                        for (Zone z : update_zone) {
                            if (z.getNom().equals(BtnOption.getSelectedItem().toString())) {

                                drawCircle(new LatLng(z.getGeolocalisation().getLatitude(), z.getGeolocalisation().getLongitude()), z.getRayon() * 1000);
                                //setUpMapIfNeeded();
                                //initListeners();


                                for (int i = 0; i < l.size(); i++) {
                                    if (update_compte != null) {
                                        for (Compte c : update_compte) {
                                            List<Entreprise> List_Entreprise = c.getEntrepriseList(); //Load All Entreprise
                                            for (Entreprise e : List_Entreprise) {
                                                List<com.srp.agronome.android.db.Contact> List_Contact = e.getContactList(); //Load All Contact
                                                for (com.srp.agronome.android.db.Contact ct : List_Contact) {
                                                    if (ct.getIdentite().getAdresse().getGeolocalisation() == null) {
                                                        continue;
                                                    }
                                                    if (distance(l.get(i).latitude, l.get(i).longitude, z.getGeolocalisation().getLatitude(), z.getGeolocalisation().getLongitude()) <= (z.getRayon())) {

                                                        if (ct.getIdentite().getAdresse().getGeolocalisation().getLatitude() == l.get(i).latitude && ct.getIdentite().getAdresse().getGeolocalisation().getLongitude() == l.get(i).longitude) {
                                                            mMap.addMarker(new MarkerOptions().position(new LatLng(l.get(i).latitude, l.get(i).longitude)).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_face)).title(ct.getIdentite().getNom()));
                                                            mMap.setMapType(0);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //if (distance(l.get(i).latitude, l.get(i).longitude, z.getGeolocalisation().getLatitude(), z.getGeolocalisation().getLongitude()) <= (z.getRayon())) {

                                }//


                            }


                        }

                    }
                }


            }


            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


//    View.OnClickListener ButtonOptionHandler = new View.OnClickListener() {
//        public void onClick(View v) {
//            Selection_zone_Choice();
//
//
//        }
//    };

    private void Show_Dialog_EditText(String title, String message, LatLng centre) {
        final LatLng l = centre;
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.dialog_add_zone, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
        final EditText NomZone = (EditText) dialogview.findViewById(R.id.InputDialogText);
        final EditText Rayon = (EditText) dialogview.findViewById(R.id.InputDialodyayon);

        builder.setTitle(title);
        NomZone.setHint(message); //Message = Hint de l'editText
        builder.setCancelable(false);
        builder.setView(dialogview);

        builder.setPositiveButton(getString(R.string.textYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
                ZoneDao zoneDao = daoSession.getZoneDao();
                List<Zone> update_zone = zoneDao.queryBuilder()
                        .list();
                String NewValueList = NomZone.getText().toString().trim(); //Globale Variable String
                int Rayonnew = Integer.parseInt(Rayon.getText().toString().trim());
                Boolean b = true;

                //verifie si la zone ajouter n'est pas inclus dans une zone existante

                if (update_zone != null) {
                    for (Zone z : update_zone) {
                        if (distance(l.latitude, l.longitude, z.getGeolocalisation().getLatitude(), z.getGeolocalisation().getLongitude()) < (z.getRayon() + Rayonnew)) {
                            Log.i(" compare distance ", "distance entre les 2 centres: " + distance(l.latitude, l.longitude, z.getGeolocalisation().getLatitude(), z.getGeolocalisation().getLongitude()) + "la somme des 2 rayons" + z.getRayon() + Rayonnew);
                            Show_Dialog("Information", "votre zone est incluse dans la zone:" + z.getNom());
                            //Log.i();
                            b = false;
                        }

                        break;
                    }
                }

                if (b) {
                    InsertZone(NewValueList, Rayonnew, l);
                    drawCircle(new LatLng(l.latitude, l.longitude), Rayonnew * 1000);
                    LoadZoneInSpinner();
                }


            }
        });
        builder.setNegativeButton(getString(R.string.textNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alert = builder.create();
        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });
        alert.show();
    }


    //dialog champs vide
    private void Show_Dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        //Change Background Color
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Window view = ((AlertDialog) dialog).getWindow();
                view.setBackgroundDrawableResource(R.color.colorBackgroundDialog);
            }
        });

        alert.show();
    }

// le probleme avec le phone de jean c'est qu'il n'a pas de carte SD et sa memoire est faible

//https://issuetracker.google.com/issues/35827769

//    private void displayContactInLog() {
//        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
//        ZoneDao zoneDao = daoSession.getZoneDao();
//        List<Zone> update_compte = zoneDao.queryBuilder()
//                .list();
//        if (update_compte != null) {
//            for (Zone c : update_compte) {
//                Log.i(" zone ",c.getNom());
//
//            }
//        }
//    }


    //async class to handle the heavy calc events
    public  class MapMarkerFunctions extends AsyncTask<Object, Object, List<LatLng>>
    {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected List<LatLng> doInBackground(Object... params) {
            Log.i(TAG, "doInBackground: in Background thread getting list of Latlng points");
             l = getListgps();
            return l;
        }
    }

}