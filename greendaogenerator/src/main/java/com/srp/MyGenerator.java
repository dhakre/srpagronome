package com.srp;


import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;


public class MyGenerator {
    public static void main(String[] args) {
        //Always modify the number of schema before run Ex: change 16 to 17
        Schema schema = new Schema(40, "com.srp.agronome.android.db"); // Your app package name and the (.db) is the folder where the DAO files will be generated into.
        schema.enableKeepSectionsByDefault();
        addTables(schema);

        // Version of Schema a incrementer avant chaque execution

        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java"); //Générer la base de données
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Creation des tables
    private static void addTables(final Schema schema) {

        Entity Compte = addCompte(schema);
        Entity Type_Compte = addType_Compte(schema);

        Entity Photo = addPhoto(schema);
        Entity ThemePhoto = addThemePhoto(schema);
        Property ID_Theme_Photo = Photo.addLongProperty("ID_Theme_Photo").getProperty();
        Photo.addToOne(ThemePhoto,ID_Theme_Photo);
        Property ID_Compte_Photo = Photo.addLongProperty("ID_Compte").getProperty();
        Compte.addToMany(Photo, ID_Compte_Photo);

        Entity Video = addVideo(schema);
        Property ID_Theme_Video = Video.addLongProperty("ID_Theme_Video").getProperty();
        Video.addToOne(ThemePhoto,ID_Theme_Video);
        Property ID_Compte_Video = Video.addLongProperty("ID_Compte").getProperty();
        Compte.addToMany(Video,ID_Compte_Video);

        Property ID_Type_Compte = Compte.addLongProperty("ID_Type_Compte").getProperty(); // Foreign Key
        Compte.addToOne(Type_Compte, ID_Type_Compte); // Foreign Table

        Entity Collaborateur = addCollaborateur(schema);
        Property ID_Collaborateur = Compte.addLongProperty("ID_Collaborateur").getProperty();
        Compte.addToOne(Collaborateur, ID_Collaborateur);

        Entity Identite = addIdentite(schema);
        Property ID_Identite = Collaborateur.addLongProperty("ID_Identite").getProperty();
        Collaborateur.addToOne(Identite, ID_Identite);

        Entity Sexe = addSexe(schema);
        Property ID_Sexe = Identite.addLongProperty("ID_Sexe").getProperty();
        Identite.addToOne(Sexe, ID_Sexe);

        Entity Adresse = addAdresse(schema);
        Property ID_Adresse = Identite.addLongProperty("ID_Adresse").getProperty();
        Identite.addToOne(Adresse, ID_Adresse);

        Entity CodePostalVille = addCodePostalVille(schema);
        Property ID_Code_Postal_Ville = Adresse.addLongProperty("ID_Code_Postal_Ville").getProperty();
        Adresse.addToOne(CodePostalVille, ID_Code_Postal_Ville);

        Entity Region = addRegion(schema);
        Property ID_Region = Adresse.addLongProperty("ID_Region").getProperty();
        Adresse.addToOne(Region, ID_Region);

        Entity Pays = addPays(schema);
        Property ID_Pays = Adresse.addLongProperty("ID_Pays").getProperty();
        Adresse.addToOne(Pays, ID_Pays);

        Entity Geolocalisation = addGeolocalisation(schema);
        Property ID_Geolocalisation = Adresse.addLongProperty("ID_Geolocalisation").getProperty();

        Adresse.addToOne(Geolocalisation, ID_Geolocalisation);

        Entity Zone = addZone(schema);
        Property ID_GeolocalisationZone = Zone.addLongProperty("ID_Geolocalisation").getProperty();
        Zone.addToOne(Geolocalisation,ID_GeolocalisationZone);
        Property ID_Zone = Adresse.addLongProperty("ID_Zone").getProperty();
        Adresse.addToOne(Zone,ID_Zone);


        Entity Groupement = addGroupement(schema);
        Property ID_Groupement = Adresse.addLongProperty("ID_Groupement").getProperty();
        Adresse.addToOne(Groupement,ID_Groupement);

        Entity Entreprise = addEntreprise(schema);

        Entity Structure_Sociale = addStructureSociale(schema);
        Property ID_Structure_Sociale = Entreprise.addLongProperty("ID_Structure_Sociale").getProperty();
        Entreprise.addToOne(Structure_Sociale, ID_Structure_Sociale);

        Entity Situation = addSituation(schema);
        Property ID_Situation = Entreprise.addLongProperty("ID_Situation").getProperty();
        Entreprise.addToOne(Situation, ID_Situation);

        Entity Statut_Soytouch = addStatut_Soytouch(schema);
        Property ID_Statut_Soytouch = Entreprise.addLongProperty("ID_Statut_Soytouch").getProperty();
        Entreprise.addToOne(Statut_Soytouch, ID_Statut_Soytouch);

        Property ID_Adresse_Facturation = Entreprise.addLongProperty("ID_Adresse_Facturation").getProperty();
        Entreprise.addToOne(Adresse, ID_Adresse_Facturation, "Adresse_Facturation");

        Property ID_Adresse_Livraison = Entreprise.addLongProperty("ID_Adresse_Livraison").getProperty();
        Entreprise.addToOne(Adresse, ID_Adresse_Livraison, "Adresse_Livraison");

        Property ID_Adresse_Siege = Entreprise.addLongProperty("ID_Adresse_Siege").getProperty();
        Entreprise.addToOne(Adresse, ID_Adresse_Siege, "Adresse_Siege");

        Entity Activite = addActivite(schema);
        Property ID_Activite_Principale = Entreprise.addLongProperty("ID_Activite_Principale").getProperty();
        Entreprise.addToOne(Activite, ID_Activite_Principale, "Activite_Principale");

        Property ID_Activite_Secondaire = Entreprise.addLongProperty("ID_Activite_Secondaire").getProperty();
        Entreprise.addToOne(Activite, ID_Activite_Secondaire, "Activite_Secondaire");

        Entity Biologique = addBiologique(schema);
        Property ID_Biologique = Entreprise.addLongProperty("ID_Biologique").getProperty();
        Entreprise.addToOne(Biologique, ID_Biologique);

        // Une entreprise possède 0-N Type Bio.
        Entity Type_Bio = addType_Bio(schema);
        Property ID_Entreprise_Bio = Type_Bio.addLongProperty("ID_Entreprise").getProperty();
        Entreprise.addToMany(Type_Bio, ID_Entreprise_Bio);


        Entity Contact = addContact(schema);

        Property ID_Identite_Contact = Contact.addLongProperty("ID_Identite_Contact").getProperty();
        Contact.addToOne(Identite, ID_Identite_Contact);

        Entity Categorie_Contact = addCategorie_Contact(schema);
        Property ID_Categorie_Contact = Contact.addLongProperty("ID_Categorie_Contact").getProperty();
        Contact.addToOne(Categorie_Contact, ID_Categorie_Contact);

        Entity Metier = addMetier(schema);
        Property ID_Metier = Contact.addLongProperty("ID_Metier").getProperty();
        Contact.addToOne(Metier,ID_Metier);

        Entity Sociabilite = addSociabilite(schema);
        Property ID_Sociabilite = Contact.addLongProperty("ID_Sociabilite").getProperty();
        Contact.addToOne(Sociabilite, ID_Sociabilite);

        //Un compte a 0-N entreprise
        Property ID_Compte = Entreprise.addLongProperty("ID_Compte").notNull().getProperty();
        Compte.addToMany(Entreprise, ID_Compte);

        //Une entreprise a 0-N Contacts
        Property ID_Entreprise = Contact.addLongProperty("ID_Entreprise").notNull().getProperty();
        Entreprise.addToMany(Contact, ID_Entreprise);

//Audit part by jitendra

        Entity SUIVI_PRODUCTEUR = addSUIVI_PRODUCTEUR(schema);

        Entity OS = addOS(schema);

        Property ID_OS_PRINCIPAL   = SUIVI_PRODUCTEUR.addLongProperty("ID_OS_PRINCIPAL ").getProperty();
        Property ID_OS_SECONDAIRE    = SUIVI_PRODUCTEUR.addLongProperty("ID_OS_SECONDAIRE  ").getProperty();

        SUIVI_PRODUCTEUR.addToOne(OS,ID_OS_PRINCIPAL,"OS_PRINCIPAL" ); // To differentiate between os principle and os secondary add 3rd option  ex :"OS_PRINCIPAL" as string to differentiate
        SUIVI_PRODUCTEUR.addToOne(OS,ID_OS_SECONDAIRE,"OS_SECONDAIRE");

        Entity SOURCE_INTERNET = addSOURCE_INTERNET(schema);
        Property ID_SOURCE_INTERNET   = SUIVI_PRODUCTEUR.addLongProperty("ID_SOURCE_INTERNET").getProperty();
        SUIVI_PRODUCTEUR.addToOne(SOURCE_INTERNET,ID_SOURCE_INTERNET );

        Entity SOURCE_JOURNAUX  = addSOURCE_JOURNAUX(schema);
        Property ID_SOURCE_JOURNAUX   = SUIVI_PRODUCTEUR.addLongProperty("ID_SOURCE_JOURNAUX").getProperty();
        SUIVI_PRODUCTEUR.addToOne(SOURCE_JOURNAUX,ID_SOURCE_JOURNAUX );


        // 1 Entreprise possède 0-1 Suivi Producteurs
        Property ID_SUIVI_PRODUCTEUR = Entreprise.addLongProperty("ID_SUIVI_PRODUCTEUR").getProperty();
        Entreprise.addToOne(SUIVI_PRODUCTEUR,ID_SUIVI_PRODUCTEUR);


        // 1 Suivi producteur a plusieurs Ilots
        Entity ILOT = addILOT(schema);
        Property ID_SUIVI_PRODUCTEUR_ILOT = ILOT.addLongProperty("ID_SUIVI_PRODUCTEUR").getProperty();
        SUIVI_PRODUCTEUR.addToMany(ILOT,ID_SUIVI_PRODUCTEUR_ILOT); // 1 SUIVI_PRODUCTEUR can connect to many ILOT


        //Parcelle

        Entity PARCELLE = addPARCELLE(schema);
        Property ID_ILOT = PARCELLE.addLongProperty("ID_ILOT").notNull().getProperty();
        ILOT.addToMany(PARCELLE,ID_ILOT);  // Un îlot est composé de 1 à N parcelles.

        //Parcelle has to many photos
        Entity PhotoParcelle = addPhotoParcelle(schema);
        Property ID_Photo_Parcelle = PhotoParcelle.addLongProperty("ID_Parcelle").getProperty();
        PARCELLE.addToMany(PhotoParcelle, ID_Photo_Parcelle);


        Property ID_GEOLOCALISATION_PARCELLE  = PARCELLE.addLongProperty("ID_GEOLOCALISATION_PARCELLE ").getProperty(); //changed  id name from ID_GEOLOCALISATION to ID_GEOLOCALISATION_PARCELLE
        PARCELLE.addToOne(Geolocalisation,ID_GEOLOCALISATION_PARCELLE);

        Entity STATUT_PARCELLE = addSTATUT_PARCELLE(schema);
        Property ID_STATUT_PARCELLE   = PARCELLE.addLongProperty("ID_STATUT_PARCELLE  ").getProperty();
        PARCELLE.addToOne(STATUT_PARCELLE,ID_STATUT_PARCELLE);  // Ex :Autorisé de livraison ou Interdit de livraison

        Entity TYPE_CULTURE = addTYPE_CULTURE(schema);
        Property ID_TYPE_CULTURE    = PARCELLE.addLongProperty("ID_TYPE_CULTURE ").getProperty();
        PARCELLE.addToOne(TYPE_CULTURE,ID_TYPE_CULTURE);

        Entity IRRIGATION = addIRRIGATION(schema);
        Property ID_IRRIGATION  = PARCELLE.addLongProperty("ID_IRRIGATION ").getProperty();
        PARCELLE.addToOne(IRRIGATION,ID_IRRIGATION);

        Entity TYPE_IRRIGATION = addTYPE_IRRIGATION(schema);
        Property ID_TYPE_IRRIGATION   = PARCELLE.addLongProperty("ID_TYPE_IRRIGATION ").getProperty();
        PARCELLE.addToOne(TYPE_IRRIGATION,ID_TYPE_IRRIGATION); //pivot , Enrouleur, Mix

        //Stockage
        Entity STOCKAGE = addSTOCKAGE(schema);
        Property ID_SUIVI_PRODUCTEUR_STOCKAGE = STOCKAGE.addLongProperty("ID_SUIVI_PRODUCTEUR").getProperty();
        SUIVI_PRODUCTEUR.addToMany(STOCKAGE,ID_SUIVI_PRODUCTEUR_STOCKAGE); // 1 SuiviProducteur possède 0-N Stockage

        //Stockage has to many photos.
        Entity PhotoStockage = addPhotoStockage(schema);
        Property ID_Photo_Stockage = PhotoStockage.addLongProperty("ID_Stockage").getProperty();
        STOCKAGE.addToMany(PhotoStockage, ID_Photo_Stockage);

        Property ID_GEOLOCALISATION_STOCKAGE  = STOCKAGE.addLongProperty("ID_GEOLOCALISATION_STOCKAGE ").getProperty(); //There are many reltion of geolocalisation ..
        STOCKAGE.addToOne(Geolocalisation,ID_GEOLOCALISATION_STOCKAGE);

        Entity TYPE_STOCKEUR = addTYPE_STOCKEUR(schema);
        Property ID_TYPE_STOCKEUR   = STOCKAGE.addLongProperty("ID_TYPE_STOCKEUR  ").getProperty();
        STOCKAGE.addToOne(TYPE_STOCKEUR,ID_TYPE_STOCKEUR);

        Entity ETAT_STOCKEUR = addETAT_STOCKEUR(schema);
        Property ID_ETAT_STOCKEUR   = STOCKAGE.addLongProperty("ID_ETAT_STOCKEUR ").getProperty();
        STOCKAGE.addToOne(ETAT_STOCKEUR,ID_ETAT_STOCKEUR );

        Entity STATUT_STOCKEUR = addSTATUT_STOCKEUR(schema);
        Property ID_STATUT_STOCKEUR   = STOCKAGE.addLongProperty("ID_STATUT_STOCKEUR  ").getProperty();
        STOCKAGE.addToOne(STATUT_STOCKEUR,ID_STATUT_STOCKEUR );



        Entity AUDIT_STOCKAGE = addAUDIT_STOCKAGE(schema);
        Property ID_STOCKAGE   = AUDIT_STOCKAGE.addLongProperty("ID_STOCKAGE  ").getProperty();
        AUDIT_STOCKAGE.addToOne(STOCKAGE,ID_STOCKAGE);


        Entity PROPRETE = addPROPRETE(schema);
        Property ID_PROPRETE_AUDIT_STOCKAGE    = AUDIT_STOCKAGE.addLongProperty("ID_PROPRETE_AUDIT_STOCKAGE ").getProperty();
        AUDIT_STOCKAGE.addToOne(PROPRETE,ID_PROPRETE_AUDIT_STOCKAGE ); // keep the same id name in postgresql also

        Entity CONTAMINATION_CROISE = addCONTAMINATION_CROISE(schema);
        Property ID_CONTAMINATION_CROISE    = AUDIT_STOCKAGE.addLongProperty("ID_CONTAMINATION_CROISE   ").getProperty();
        AUDIT_STOCKAGE.addToOne(CONTAMINATION_CROISE,ID_CONTAMINATION_CROISE );

        Entity VENTILATION = addVENTILATION(schema);
        Property ID_VENTILATION    = AUDIT_STOCKAGE.addLongProperty("ID_VENTILATION ").getProperty();
        AUDIT_STOCKAGE.addToOne(VENTILATION,ID_VENTILATION  );

        Entity TEMPERATURE = addTEMPERATURE(schema);
        Property ID_TEMPERATURE    = AUDIT_STOCKAGE.addLongProperty("ID_TEMPERATURE ").getProperty();
        AUDIT_STOCKAGE.addToOne(TEMPERATURE,ID_TEMPERATURE  );

        Entity TRIAGE_GRAIN = addTRIAGE_GRAIN(schema);
        Property ID_TRIAGE_GRAIN   = AUDIT_STOCKAGE.addLongProperty("ID_TRIAGE_GRAIN  ").getProperty();
        AUDIT_STOCKAGE.addToOne(TRIAGE_GRAIN,ID_TRIAGE_GRAIN  );

        Entity NUISIBLE = addNUISIBLE(schema);
        Property ID_AUDIT_STOCKAGE    = NUISIBLE.addLongProperty("ID_AUDIT_STOCKAGE ").getProperty();
        AUDIT_STOCKAGE.addToMany(NUISIBLE,ID_AUDIT_STOCKAGE  ); //Audit Stockage peut avoir 0 à N Nuisibles.

        Entity ETAT_NUISIBLE = addETAT_NUISIBLE(schema);
        Property ID_ETAT_NUISIBLE    = NUISIBLE.addLongProperty("ID_ETAT_NUISIBLE ").getProperty();
        NUISIBLE.addToOne(ETAT_NUISIBLE,ID_ETAT_NUISIBLE);

        Entity LISTE_NUISIBLE = addLISTE_NUISIBLE(schema);
        Property ID_LISTE_NUISIBLE    = NUISIBLE.addLongProperty("ID_LISTE_NUISIBLE  ").getProperty();
        NUISIBLE.addToOne(LISTE_NUISIBLE,ID_LISTE_NUISIBLE  );


        Entity AUDIT = addAUDIT(schema);
        Property ID_SUIVI_PRODCTEUR  = AUDIT.addLongProperty("ID_SUIVI_PRODCTEUR  ").getProperty();
        AUDIT.addToOne(SUIVI_PRODUCTEUR,ID_SUIVI_PRODCTEUR);


        Property ID_AUDIT  = AUDIT_STOCKAGE.addLongProperty("ID_AUDIT ").notNull().getProperty();
        AUDIT.addToMany(AUDIT_STOCKAGE,ID_AUDIT); // Un Audit est composé de 1 à N audits stockage (pour chaque stockeur).


        Entity OBJET_AUDIT = addOBJET_AUDIT(schema);
        Property ID_OBJET_AUDIT     = AUDIT.addLongProperty("ID_OBJET_AUDIT   ").getProperty();
        AUDIT.addToOne(OBJET_AUDIT,ID_OBJET_AUDIT  );

        Entity RECOLTE = addRECOLTE(schema);
        Property ID_RECOLTE = AUDIT.addLongProperty("ID_RECOLTE ").getProperty();
        AUDIT.addToOne(RECOLTE,ID_RECOLTE);

        Entity AUDIT_DOCUMENT = addAUDIT_DOCUMENT(schema);
        Property ID_AUDIT_DOCUMENT  = AUDIT.addLongProperty("ID_AUDIT_DOCUMENT   ").notNull().getProperty();
        AUDIT.addToOne(AUDIT_DOCUMENT,ID_AUDIT_DOCUMENT   );


        Entity DOCUMENT = addDOCUMENT(schema);
        Property ID_AUDIT_DOCUMENT_DOCUMENT   = DOCUMENT.addLongProperty("ID_AUDIT_DOCUMENT_DOCUMENT  ").getProperty(); //foreign ID name changesd because Audit document connected to both Audit and document
        AUDIT_DOCUMENT.addToMany(DOCUMENT,ID_AUDIT_DOCUMENT_DOCUMENT   ); //Un Audit document peut avoir 0 à N Documents

        Entity SCAN_DOCUMENT = addSCAN_DOCUMENT(schema);
        Property ID_DOCUMENT   = SCAN_DOCUMENT.addLongProperty("ID_DOCUMENT ").notNull().getProperty();
        SCAN_DOCUMENT.addToOne(DOCUMENT,ID_DOCUMENT   );


        Entity TYPE_DOCUMENT = addTYPE_DOCUMENT(schema);
        Property ID_TYPE_DOCUMENT    = DOCUMENT.addLongProperty("ID_TYPE_DOCUMENT  ").notNull().getProperty();
        DOCUMENT.addToOne(TYPE_DOCUMENT,ID_TYPE_DOCUMENT    );

        Entity DOCUMENT_FOURNI = addDOCUMENT_FOURNI(schema);
        Property ID_DOCUMENT_FOURNI    = DOCUMENT.addLongProperty("ID_DOCUMENT_FOURNI  ").notNull().getProperty();
        DOCUMENT.addToOne(DOCUMENT_FOURNI,ID_DOCUMENT_FOURNI    );


        Entity AUDIT_PARCELLE = addAUDIT_PARCELLE(schema);
        Property ID_AUDIT_PARCELLE  = AUDIT_PARCELLE.addLongProperty("ID_AUDIT_PARCELLE   ").notNull().getProperty(); // ID name changed because it repeats
        AUDIT.addToMany(AUDIT_PARCELLE,ID_AUDIT_PARCELLE );  // Un Audit est composé de 1 à N audits parcelles (pour chaque parcelles).


        Property ID_PROPRETE_PARCELLE      = AUDIT_PARCELLE.addLongProperty("ID_PROPRETE_PARCELLE   ").getProperty();
        AUDIT_PARCELLE.addToOne(PROPRETE,ID_PROPRETE_PARCELLE );

        Property ID_PARCELLE     = AUDIT_PARCELLE.addLongProperty("ID_PARCELLE   ").getProperty();
        AUDIT_PARCELLE.addToOne(PARCELLE,ID_PARCELLE   );

        Entity ETAT_VEGETATIF = addETAT_VEGETATIF(schema);
        Property ID_ETAT_VEGETATIF     = AUDIT_PARCELLE.addLongProperty("ID_ETAT_VEGETATIF   ").getProperty();
        AUDIT_PARCELLE.addToOne(ETAT_VEGETATIF,ID_ETAT_VEGETATIF );


        Entity PLANTE_NON_DESIRABLE = addPLANTE_NON_DESIRABLE(schema);
        Property ID_AUDIT_PARCELLE_PLANTE_NON_DESIRABLE    = PLANTE_NON_DESIRABLE.addLongProperty("ID_AUDIT_PARCELLE_PLANTE_NON_DESIRABLE  ").getProperty();
        AUDIT_PARCELLE.addToMany(PLANTE_NON_DESIRABLE,ID_AUDIT_PARCELLE_PLANTE_NON_DESIRABLE  ); //Audit Parcelle peut avoir 0 à N Plantes indésirables.

        Entity ETAT_PLANTE = addETAT_PLANTE(schema);
        Property ID_ETAT_PLANTE = PLANTE_NON_DESIRABLE.addLongProperty("ID_ETAT_PLANTE    ").notNull().getProperty();
        PLANTE_NON_DESIRABLE.addToOne(ETAT_PLANTE,ID_ETAT_PLANTE );

        Entity LISTE_PLANTE = addLISTE_PLANTE(schema);
        Property ID_LISTE_PLANTE = PLANTE_NON_DESIRABLE.addLongProperty("ID_LISTE_PLANTE    ").getProperty();
        PLANTE_NON_DESIRABLE.addToOne(LISTE_PLANTE,ID_LISTE_PLANTE  );

        Entity CONTRE_VISITE = addCONTRE_VISITE(schema);


        Entity ETAT_CONTRE_VISITE = addETAT_CONTRE_VISITE(schema);
        Property ID_ETAT_CONTRE_VISITE  = CONTRE_VISITE.addLongProperty("ID_ETAT_CONTRE_VISITE ").getProperty();
        CONTRE_VISITE.addToOne(ETAT_CONTRE_VISITE,ID_ETAT_CONTRE_VISITE   );

        Entity CAUSE = addCAUSE(schema);
        Property ID_CONTRE_VISITE  = CAUSE.addLongProperty("ID_CONTRE_VISITE    ").getProperty();
        CAUSE.addToOne(CONTRE_VISITE,ID_CONTRE_VISITE  );

        Entity MESSAGE = addMESSAGE(schema);
        Property ID_MESSAGE_CONTRE_VISITE = CONTRE_VISITE.addLongProperty("ID_MESSAGE_CONTRE_VISITE    ").getProperty(); //chenage the foregin key id as per table because it is used at many place
        CONTRE_VISITE.addToOne(MESSAGE,ID_MESSAGE_CONTRE_VISITE  );

        Entity SELECTION_DESTINATAIRE_DEFAUT = addSELECTION_DESTINATAIRE_DEFAUT(schema);
        Property ID_MESSAGE  = SELECTION_DESTINATAIRE_DEFAUT.addLongProperty("ID_MESSAGE  ").getProperty();
        SELECTION_DESTINATAIRE_DEFAUT.addToOne(MESSAGE,ID_MESSAGE  );

        Entity OBJET_MESSAGE = addOBJET_MESSAGE(schema);
        Property ID_OBJET_MESSAGE  = CONTRE_VISITE.addLongProperty("ID_OBJET_MESSAGE     ").getProperty();
        CONTRE_VISITE.addToOne(OBJET_MESSAGE,ID_OBJET_MESSAGE   );

        Entity TEXTE_MESSAGE = addTEXTE_MESSAGE(schema);
        Property ID_TEXTE_MESSAGE  = MESSAGE.addLongProperty("ID_TEXTE_MESSAGE     ").getProperty();
        MESSAGE.addToOne(TEXTE_MESSAGE,ID_TEXTE_MESSAGE   );

        Entity DESTINATAIRE_DEFAUT = addDESTINATAIRE_DEFAUT(schema);
        Property ID_DESTINATAIRE_DEFAUT  = SELECTION_DESTINATAIRE_DEFAUT.addLongProperty("ID_DESTINATAIRE_DEFAUT ").getProperty();
        SELECTION_DESTINATAIRE_DEFAUT.addToOne(DESTINATAIRE_DEFAUT,ID_DESTINATAIRE_DEFAUT  );

        //Visite d'une entreprise
        Entity Visite = addVisite(schema);
        Entity Attitude_Entreprise = addAttitudeEntrepriseVisite(schema);
        Entity Interet_Visite = addInteretVisite(schema);
        Entity Sujet_Visite_Principal = addSujetVisitePrincipal(schema);
        Entity Sujet_Visite_Secondaire = addSujetVisiteSecondaire(schema);
        Entity Objet_Visite = addObjetVisite(schema);

        Property ID_Attitude_Entreprise = Visite.addLongProperty("ID_Attitude_Entreprise").getProperty();
        Property ID_Interet_Visite = Visite.addLongProperty("ID_Interet_Visite").getProperty();
        Visite.addToOne(Attitude_Entreprise,ID_Attitude_Entreprise);
        Visite.addToOne(Interet_Visite,ID_Interet_Visite);

        Property ID_Objet_Visite_P = Sujet_Visite_Principal.addLongProperty("ID_Objet_Visite").getProperty();
        Sujet_Visite_Principal.addToOne(Objet_Visite,ID_Objet_Visite_P);
        Property ID_Objet_Visite_S = Sujet_Visite_Secondaire.addLongProperty("ID_Objet_Visite").getProperty();
        Sujet_Visite_Secondaire.addToOne(Objet_Visite,ID_Objet_Visite_S);

        Property ID_Sujet_Visite_Principal = Visite.addLongProperty("ID_Sujet_Visite_Principal").getProperty();
        Visite.addToOne(Sujet_Visite_Principal,ID_Sujet_Visite_Principal);

        Property ID_Visite = Sujet_Visite_Secondaire.addLongProperty("ID_Visite").getProperty();
        Visite.addToMany(Sujet_Visite_Secondaire,ID_Visite);

        //Une entreprise peut recevoir 0 - N Visites
        Property ID_Entreprise_Visite = Visite.addLongProperty("ID_Entreprise").getProperty();
        Entreprise.addToMany(Visite,ID_Entreprise_Visite);



        //Tables : Incident Administratif
        Entity Incident = addIncident(schema);
        Entity EtatIncident = addEtatIncident(schema);
        Entity TypeIncident = addTypeIncident(schema);
        Entity IncidentAdministratif = addIncidentAdministratif(schema);
        Entity IncidentContrat = addIncidentContrat(schema);
        Entity SujetMessage = addSujetMessage(schema);

        Property ID_EtatIncident = Incident.addLongProperty("ID_Etat_Incident").getProperty();
        Incident.addToOne(EtatIncident,ID_EtatIncident);

        Property ID_SujetMessage = Incident.addLongProperty("ID_Sujet_Message").getProperty();
        Incident.addToOne(SujetMessage,ID_SujetMessage);

        Property ID_TypeIncident = SujetMessage.addLongProperty("ID_Type_Incident").getProperty();
        SujetMessage.addToOne(TypeIncident,ID_TypeIncident);

        Property ID_Nom = SujetMessage.addLongProperty("ID_Nom").getProperty();
        SujetMessage.addToOne(IncidentAdministratif,ID_Nom);
        SujetMessage.addToOne(IncidentContrat,ID_Nom);

        //Une entreprise peut avoir 0 - N incidents administratifs
        Property ID_Entreprise_Incident = Incident.addLongProperty("ID_Entreprise").getProperty();
        Entreprise.addToMany(Incident,ID_Entreprise_Incident);

        //Tables : Suivi Incidents
        Entity SuiviIncident = addSuiviIncident(schema);
        Entity Expediteur = addExpediteur(schema);
        Entity TypeExpediteur = addTypeExpediteur(schema);
        Entity ListeExpediteurContact = addListeExpediteurContact(schema);
        Entity ListeExpediteurAgronome = addListeExpediteurAgronome(schema);
        Entity ListeExpediteurAdministration = addListeExpediteurAdministration(schema);

        Property ID_Type_Expediteur = Expediteur.addLongProperty("ID_Type_Expediteur").getProperty();
        Expediteur.addToOne(TypeExpediteur,ID_Type_Expediteur);

        Property ID_Nom_Expediteur = Expediteur.addLongProperty("ID_Nom").getProperty();
        Expediteur.addToOne(ListeExpediteurContact,ID_Nom_Expediteur);
        Expediteur.addToOne(ListeExpediteurAgronome,ID_Nom_Expediteur);
        Expediteur.addToOne(ListeExpediteurAdministration,ID_Nom_Expediteur);

        Property ID_Expediteur = SuiviIncident.addLongProperty("ID_Expediteur").getProperty();
        SuiviIncident.addToOne(Expediteur,ID_Expediteur);

        Property ID_Incident = SuiviIncident.addLongProperty("ID_Incident").getProperty();
        Incident.addToMany(SuiviIncident,ID_Incident);

    }


    //Table Compte
    private static Entity addCompte(final Schema schema) {
        Entity Compte = schema.addEntity("Compte");
        Compte.addIdProperty().primaryKey();
        Compte.addStringProperty("login").notNull();
        Compte.addStringProperty("password").notNull();
        Compte.addIntProperty("archive").notNull();
        return Compte;
    }

    //Table Photo
    private static Entity addPhoto(final Schema schema){
        Entity Photo = schema.addEntity("Photo");
        Photo.addIdProperty().primaryKey();
        Photo.addStringProperty("nom");
        return Photo;
    }

    //Table Photo Parcelle
    private static Entity addPhotoParcelle(final Schema schema){
        Entity PhotoParcelle = schema.addEntity("Photo_Parcelle");
        PhotoParcelle.addIdProperty().primaryKey();
        PhotoParcelle.addStringProperty("nom");
        return PhotoParcelle;
    }

    //Table Photo Stockage
    private static Entity addPhotoStockage(final Schema schema){
        Entity PhotoStockage = schema.addEntity("Photo_Stockage");
        PhotoStockage.addIdProperty().primaryKey();
        PhotoStockage.addStringProperty("nom");
        return PhotoStockage;
    }



    //Table Video
    private static Entity addVideo(final Schema schema){
        Entity Video = schema.addEntity("Video");
        Video.addIdProperty().primaryKey();
        Video.addStringProperty("nom");
        return Video;
    }

    private static Entity addThemePhoto(final Schema schema){
        Entity ThemePhoto = schema.addEntity("Theme_Photo");
        ThemePhoto.addIdProperty().primaryKey();
        ThemePhoto.addStringProperty("nom");
        return ThemePhoto;
    }

    //Liste de choix : Type_Compte
    private static Entity addType_Compte(final Schema schema) {
        Entity Type_Compte = schema.addEntity("Type_Compte");
        Type_Compte.addIdProperty().primaryKey();
        Type_Compte.addStringProperty("nom");
        return Type_Compte;
    }

    //Table Collaborateur
    private static Entity addCollaborateur(final Schema schema) {
        Entity Collaborateur = schema.addEntity("Collaborateur");
        Collaborateur.addIdProperty().primaryKey();
        Collaborateur.addStringProperty("adresse_mail_gmail");
        Collaborateur.addStringProperty("adresse_mail_entreprise");
        Collaborateur.addIntProperty("archive").notNull();
        Collaborateur.addIntProperty("ID_Agronome_Creation").notNull();
        Collaborateur.addDateProperty("date_creation").notNull();
        Collaborateur.addIntProperty("ID_Agronome_Modification").notNull();
        Collaborateur.addDateProperty("date_modification").notNull();
        return Collaborateur;
    }

    //Table Identite
    private static Entity addIdentite(final Schema schema) {
        Entity Identite = schema.addEntity("Identite");
        Identite.addIdProperty().primaryKey();
        Identite.addStringProperty("nom");
        Identite.addStringProperty("prenom");
        Identite.addDateProperty("date_naissance");
        Identite.addStringProperty("photo");
        Identite.addStringProperty("telephone_principal");
        Identite.addStringProperty("telephone_secondaire");
        Identite.addStringProperty("adresse_email");
        Identite.addIntProperty("archive").notNull();
        Identite.addIntProperty("ID_Agronome_Creation").notNull();
        Identite.addDateProperty("date_creation").notNull();
        Identite.addIntProperty("ID_Agronome_Modification").notNull();
        Identite.addDateProperty("date_modification").notNull();
        return Identite;
    }


    //Liste de choix : Sexe
    private static Entity addSexe(final Schema schema) {
        Entity Sexe = schema.addEntity("Sexe");
        Sexe.addIdProperty().primaryKey();
        Sexe.addStringProperty("nom");
        return Sexe;
    }


    //Table Adresse
    private static Entity addAdresse(final Schema schema) {
        Entity Adresse = schema.addEntity("Adresse");
        Adresse.addIdProperty().primaryKey();
        Adresse.addStringProperty("adresse_numero_rue");
        Adresse.addStringProperty("complement_adresse");
        Adresse.addIntProperty("archive").notNull();
        Adresse.addIntProperty("ID_Agronome_Creation").notNull();
        Adresse.addDateProperty("date_creation").notNull();
        Adresse.addIntProperty("ID_Agronome_Modification").notNull();
        Adresse.addDateProperty("date_modification").notNull();
        return Adresse;
    }

    private static Entity addZone(final Schema schema){
        Entity Zone = schema.addEntity("Zone");
        Zone.addIdProperty().primaryKey();
        Zone.addStringProperty("nom");
        Zone.addIntProperty("rayon");
        return Zone;
    }

    private static Entity addGroupement(final Schema schema){
        Entity Groupement = schema.addEntity("Groupement");
        Groupement.addIdProperty().primaryKey();
        Groupement.addStringProperty("nom");
        return Groupement;
    }

    //Liste de choix : Code_Postal_Ville
    private static Entity addCodePostalVille(final Schema schema) {
        Entity Code_Postal_Ville = schema.addEntity("Code_Postal_Ville");
        Code_Postal_Ville.addIdProperty().primaryKey();
        Code_Postal_Ville.addStringProperty("code_postal");
        Code_Postal_Ville.addStringProperty("ville");
        return Code_Postal_Ville;
    }

    //Liste de choix : Region
    private static Entity addRegion(final Schema schema) {
        Entity Region = schema.addEntity("Region");
        Region.addIdProperty().primaryKey();
        Region.addStringProperty("nom");
        return Region;
    }

    //Liste de choix : Pays
    private static Entity addPays(final Schema schema) {
        Entity Pays = schema.addEntity("Pays");
        Pays.addIdProperty().primaryKey();
        Pays.addStringProperty("nom");
        return Pays;
    }

    //Table : Geolocalisation
    private static Entity addGeolocalisation(final Schema schema) {
        Entity Geolocalisation = schema.addEntity("Geolocalisation");
        Geolocalisation.addIdProperty().primaryKey();
        Geolocalisation.addDoubleProperty("longitude");
        Geolocalisation.addDoubleProperty("latitude");
        return Geolocalisation;
    }

    //Tables pour les contacts d'une entreprise

    //Table : Entreprise
    private static Entity addEntreprise(final Schema schema) {
        Entity Entreprise = schema.addEntity("Entreprise");
        Entreprise.addIdProperty().primaryKey();
        Entreprise.addStringProperty("raison_sociale");
        Entreprise.addIntProperty("nombre_salarie");
        Entreprise.addStringProperty("SIRET");
        Entreprise.addStringProperty("projet_connu");
        Entreprise.addBooleanProperty("arret_activite");
        Entreprise.addStringProperty("cause_arret_activite");
        Entreprise.addDateProperty("date_arret_activite");
        Entreprise.addStringProperty("telephone_entreprise");
        Entreprise.addStringProperty("fax_entreprise");
        Entreprise.addStringProperty("adresse_email_principale");
        Entreprise.addStringProperty("adresse_email_secondaire");
        Entreprise.addIntProperty("archive").notNull();
        Entreprise.addIntProperty("ID_Agronome_Creation").notNull();
        Entreprise.addDateProperty("date_creation").notNull();
        Entreprise.addIntProperty("ID_Agronome_Modification").notNull();
        ;
        Entreprise.addDateProperty("date_modification").notNull();
        ;
        return Entreprise;
    }

    //Liste de choix Structure Sociale
    private static Entity addStructureSociale(final Schema schema) {
        Entity Structure_Sociale = schema.addEntity("Structure_Sociale");
        Structure_Sociale.addIdProperty().primaryKey();
        Structure_Sociale.addStringProperty("nom");
        return Structure_Sociale;
    }

    //Liste de choix Situation
    private static Entity addSituation(final Schema schema) {
        Entity Situation = schema.addEntity("Situation");
        Situation.addIdProperty().primaryKey();
        Situation.addStringProperty("nom");
        return Situation;
    }

    //Liste de choix Statut_Soytouch
    private static Entity addStatut_Soytouch(final Schema schema) {
        Entity Statut_Soytouch = schema.addEntity("Statut_Soytouch");
        Statut_Soytouch.addIdProperty().primaryKey();
        Statut_Soytouch.addStringProperty("nom");
        return Statut_Soytouch;
    }

    //Liste de choix Activite
    private static Entity addActivite(final Schema schema) {
        Entity Activite = schema.addEntity("Activite");
        Activite.addIdProperty().primaryKey();
        Activite.addStringProperty("nom");
        return Activite;
    }

    //Liste de choix Biologique
    private static Entity addBiologique(final Schema schema) {
        Entity Biologique = schema.addEntity("Biologique");
        Biologique.addIdProperty().primaryKey();
        Biologique.addStringProperty("nom");
        return Biologique;
    }

    //Liste de choix Type_Bio
    private static Entity addType_Bio(final Schema schema) {
        Entity Type_Bio = schema.addEntity("Type_Bio");
        Type_Bio.addIdProperty().primaryKey();
        Type_Bio.addStringProperty("nom");
        return Type_Bio;
    }

    //Table : Contact
    private static Entity addContact(final Schema schema) {
        Entity Contact = schema.addEntity("Contact");
        Contact.addIdProperty().primaryKey();
        Contact.addStringProperty("remarque_contact");
        Contact.addStringProperty("contact_identifiant");
        Contact.addIntProperty("archive").notNull(); //Boolean 0 = hidden 1 = visible
        Contact.addIntProperty("ID_Agronome_Creation").notNull();
        Contact.addDateProperty("date_creation").notNull();
        Contact.addIntProperty("ID_Agronome_Modification").notNull();
        Contact.addDateProperty("date_modification").notNull();
        return Contact;
    }

    //Liste de choix Categorie_Contact
    private static Entity addCategorie_Contact(final Schema schema) {
        Entity Categorie_Contact = schema.addEntity("Categorie_Contact");
        Categorie_Contact.addIdProperty().primaryKey();
        Categorie_Contact.addStringProperty("nom");
        return Categorie_Contact;
    }

    //Liste de choix Metier
    private static Entity addMetier(final Schema schema){
        Entity Metier = schema.addEntity("Metier");
        Metier.addIdProperty().primaryKey();
        Metier.addStringProperty("Nom");
        return Metier;
    }

    //Liste de choix Sociabilite
    private static Entity addSociabilite(final Schema schema) {
        Entity Sociabilite = schema.addEntity("Sociabilite");
        Sociabilite.addIdProperty().primaryKey();
        Sociabilite.addStringProperty("nom");
        return Sociabilite;
    }
// Audit Table ny Jitendra


    //Table SUIVI_PRODUCTEUR
    private static Entity addSUIVI_PRODUCTEUR(final Schema schema) {
        Entity SUIVI_PRODUCTEUR = schema.addEntity("SUIVI_PRODUCTEUR");
        SUIVI_PRODUCTEUR.addIdProperty().primaryKey();
        SUIVI_PRODUCTEUR.addIntProperty("SAU  ");
        SUIVI_PRODUCTEUR.addIntProperty("SAU_IRRIGABLE  ");
        SUIVI_PRODUCTEUR.addIntProperty("SAU_BIO   ");
        SUIVI_PRODUCTEUR.addIntProperty("SAU_BIO_IRRIGABLE   ");
        SUIVI_PRODUCTEUR.addIntProperty("archive").notNull();
        SUIVI_PRODUCTEUR.addIntProperty("ID_Agronome_Creation").notNull();
        SUIVI_PRODUCTEUR.addDateProperty("date_creation").notNull();
        SUIVI_PRODUCTEUR.addIntProperty("ID_Agronome_Modification").notNull();
        SUIVI_PRODUCTEUR.addDateProperty("date_modification").notNull();
        return SUIVI_PRODUCTEUR;
    }

    //Liste de choix OS
    private static Entity addOS(final Schema schema){
        Entity OS = schema.addEntity("OS");
        OS.addIdProperty().primaryKey();
        OS.addStringProperty("nom");
        return OS;
    }

    //Liste de choix SOURCE_INTERNET
    private static Entity addSOURCE_INTERNET(final Schema schema){
        Entity SOURCE_INTERNET = schema.addEntity("SOURCE_INTERNET");
        SOURCE_INTERNET.addIdProperty().primaryKey();
        SOURCE_INTERNET.addStringProperty("nom");
        return SOURCE_INTERNET;
    }

    //Liste de choix SOURCE_JOURNAUX
    private static Entity addSOURCE_JOURNAUX(final Schema schema){
        Entity SOURCE_JOURNAUX = schema.addEntity("SOURCE_JOURNAUX");
        SOURCE_JOURNAUX.addIdProperty().primaryKey();
        SOURCE_JOURNAUX.addStringProperty("nom");
        return SOURCE_JOURNAUX;
    }

    //Table ILOT
    private static Entity addILOT(final Schema schema) {
        Entity ILOT = schema.addEntity("ILOT");
        ILOT.addIdProperty().primaryKey();
        ILOT.addStringProperty("NOM_ILOT ");
        ILOT.addStringProperty("NUMERO_ILOT ");
        ILOT.addIntProperty("archive").notNull();
        ILOT.addIntProperty("ID_Agronome_Creation").notNull();
        ILOT.addDateProperty("date_creation").notNull();
        ILOT.addIntProperty("ID_Agronome_Modification").notNull();
        ILOT.addDateProperty("date_modification").notNull();
        return ILOT;
    }

    //Table PARCELLE
    private static Entity addPARCELLE(final Schema schema) {
        Entity PARCELLE = schema.addEntity("PARCELLE");
        PARCELLE.addIdProperty().primaryKey();
        //PARCELLE.addStringProperty("NOM_ILOT ");
        PARCELLE.addStringProperty("NOM_PARCELLE ");
        PARCELLE.addStringProperty("NUMERO_PARCELLE  ");
        PARCELLE.addFloatProperty("SURFACE  ");
        PARCELLE.addIntProperty("archive").notNull();
        PARCELLE.addIntProperty("ID_Agronome_Creation").notNull();
        PARCELLE.addDateProperty("date_creation").notNull();
        PARCELLE.addIntProperty("ID_Agronome_Modification").notNull();
        PARCELLE.addDateProperty("date_modification").notNull();
        return PARCELLE;
    }

    //Liste de choix STATUT_PARCELLE
    private static Entity addSTATUT_PARCELLE(final Schema schema){
        Entity STATUT_PARCELLE = schema.addEntity("STATUT_PARCELLE");
        STATUT_PARCELLE.addIdProperty().primaryKey();
        STATUT_PARCELLE.addStringProperty("nom");
        return STATUT_PARCELLE;

    }
    //Liste de choix TYPE_CULTURE
    private static Entity addTYPE_CULTURE(final Schema schema){
        Entity TYPE_CULTURE = schema.addEntity("TYPE_CULTURE");
        TYPE_CULTURE.addIdProperty().primaryKey();
        TYPE_CULTURE.addStringProperty("nom");
        return TYPE_CULTURE;

    }
    //Liste de choix IRRIGATION
    private static Entity addIRRIGATION(final Schema schema){
        Entity IRRIGATION = schema.addEntity("IRRIGATION");
        IRRIGATION.addIdProperty().primaryKey();
        IRRIGATION.addStringProperty("nom");
        return IRRIGATION;
    }
    //Liste de choix TYPE_IRRIGATION
    private static Entity addTYPE_IRRIGATION(final Schema schema){
        Entity TYPE_IRRIGATION = schema.addEntity("TYPE_IRRIGATION");
        TYPE_IRRIGATION.addIdProperty().primaryKey();
        TYPE_IRRIGATION.addStringProperty("nom");
        return TYPE_IRRIGATION;
    }
    //Table STOCKAGE
    private static Entity addSTOCKAGE(final Schema schema) {
        Entity STOCKAGE = schema.addEntity("STOCKAGE");
        STOCKAGE.addIdProperty().primaryKey();
        STOCKAGE.addStringProperty("NOM_STOCKEUR  ");
        STOCKAGE.addStringProperty("IDENTIFIANT_STOCKEUR ");
        STOCKAGE.addDateProperty("DATE_DEBUT_PROJET ");
        STOCKAGE.addIntProperty("CAPACITE ");
        STOCKAGE.addIntProperty("archive").notNull();
        STOCKAGE.addIntProperty("ID_Agronome_Creation").notNull();
        STOCKAGE.addDateProperty("date_creation").notNull();
        STOCKAGE.addIntProperty("ID_Agronome_Modification").notNull();
        STOCKAGE.addDateProperty("date_modification").notNull();
        return STOCKAGE;
    }
    //Liste de choix TYPE_STOCKEUR
    private static Entity addTYPE_STOCKEUR(final Schema schema){
        Entity TYPE_STOCKEUR = schema.addEntity("TYPE_STOCKEUR");
        TYPE_STOCKEUR.addIdProperty().primaryKey();
        TYPE_STOCKEUR.addStringProperty("nom");
        return TYPE_STOCKEUR;
    }

    //Liste de choix ETAT_STOCKEUR
    private static Entity addETAT_STOCKEUR(final Schema schema){
        Entity ETAT_STOCKEUR = schema.addEntity("ETAT_STOCKEUR");
        ETAT_STOCKEUR.addIdProperty().primaryKey();
        ETAT_STOCKEUR.addStringProperty("nom");
        return ETAT_STOCKEUR;
    }
    //Liste de choix STATUT_STOCKEUR
    private static Entity addSTATUT_STOCKEUR(final Schema schema){
        Entity STATUT_STOCKEUR = schema.addEntity("STATUT_STOCKEUR");
        STATUT_STOCKEUR.addIdProperty().primaryKey();
        STATUT_STOCKEUR.addStringProperty("nom");
        return STATUT_STOCKEUR;
    }

    //Table AUDIT_STOCKAGE
    private static Entity addAUDIT_STOCKAGE(final Schema schema) {
        Entity AUDIT_STOCKAGE = schema.addEntity("AUDIT_STOCKAGE");
        AUDIT_STOCKAGE.addIdProperty().primaryKey();
        AUDIT_STOCKAGE.addStringProperty("REMARQUES_AUDIT_STOCKAGE ");

        AUDIT_STOCKAGE.addIntProperty("archive").notNull();
        AUDIT_STOCKAGE.addIntProperty("ID_Agronome_Creation").notNull();
        AUDIT_STOCKAGE.addDateProperty("date_creation").notNull();
        AUDIT_STOCKAGE.addIntProperty("ID_Agronome_Modification").notNull();
        AUDIT_STOCKAGE.addDateProperty("date_modification").notNull();
        return AUDIT_STOCKAGE;
    }
    //Liste de choix CONTAMINATION_CROISE
    private static Entity addCONTAMINATION_CROISE(final Schema schema){
        Entity CONTAMINATION_CROISE = schema.addEntity("CONTAMINATION_CROISE");
        CONTAMINATION_CROISE.addIdProperty().primaryKey();
        CONTAMINATION_CROISE.addStringProperty("nom");
        return CONTAMINATION_CROISE;
    }
    //Liste de choix VENTILATION
    private static Entity addVENTILATION(final Schema schema){
        Entity VENTILATION = schema.addEntity("VENTILATION");
        VENTILATION.addIdProperty().primaryKey();
        VENTILATION.addStringProperty("nom");
        return VENTILATION;
    }
    //Liste de choix TEMPERATURE
    private static Entity addTEMPERATURE(final Schema schema){
        Entity TEMPERATURE = schema.addEntity("TEMPERATURE");
        TEMPERATURE.addIdProperty().primaryKey();
        TEMPERATURE.addStringProperty("nom");
        return TEMPERATURE;
    }
    //Liste de choix TRIAGE_GRAIN
    private static Entity addTRIAGE_GRAIN(final Schema schema){
        Entity TRIAGE_GRAIN = schema.addEntity("TRIAGE_GRAIN");
        TRIAGE_GRAIN.addIdProperty().primaryKey();
        TRIAGE_GRAIN.addStringProperty("nom");
        return TRIAGE_GRAIN;
    }

    //Table  NUISIBLE
    private static Entity addNUISIBLE(final Schema schema){
        Entity NUISIBLE = schema.addEntity("NUISIBLE");
        NUISIBLE.addIdProperty().primaryKey();
        return NUISIBLE;
    }

    //Liste de choix ETAT_NUISIBLE
    private static Entity addETAT_NUISIBLE(final Schema schema){
        Entity ETAT_NUISIBLE = schema.addEntity("ETAT_NUISIBLE");
        ETAT_NUISIBLE.addIdProperty().primaryKey();
        ETAT_NUISIBLE.addStringProperty("nom");
        return ETAT_NUISIBLE;
    }
    //Liste de choix LISTE_NUISIBLE
    private static Entity addLISTE_NUISIBLE(final Schema schema){
        Entity LISTE_NUISIBLE = schema.addEntity("LISTE_NUISIBLE");
        LISTE_NUISIBLE.addIdProperty().primaryKey();
        LISTE_NUISIBLE.addStringProperty("nom");
        return LISTE_NUISIBLE;
    }
    //Liste de choix PROPRETE
    private static Entity addPROPRETE(final Schema schema){
        Entity PROPRETE = schema.addEntity("PROPRETE");
        PROPRETE.addIdProperty().primaryKey();
        PROPRETE.addStringProperty("nom");
        return PROPRETE;
    }
    //Table AUDIT
    private static Entity addAUDIT(final Schema schema) {
        Entity AUDIT = schema.addEntity("AUDIT");
        AUDIT.addIdProperty().primaryKey();
        AUDIT.addDateProperty("DATE ");
        AUDIT.addStringProperty("REMARQUES_AUDITS  ");
        AUDIT.addIntProperty("archive").notNull();
        AUDIT.addIntProperty("ID_Agronome_Creation").notNull();
        AUDIT.addDateProperty("date_creation").notNull();
        AUDIT.addIntProperty("ID_Agronome_Modification").notNull();
        AUDIT.addDateProperty("date_modification").notNull();
        return AUDIT;
    }

    //Liste de choix OBJET_AUDIT
    private static Entity addOBJET_AUDIT(final Schema schema){
        Entity OBJET_AUDIT = schema.addEntity("OBJET_AUDIT");
        OBJET_AUDIT.addIdProperty().primaryKey();
        OBJET_AUDIT.addStringProperty("nom");
        return OBJET_AUDIT;
    }

    //Liste de choix RECOLTE
    private static Entity addRECOLTE(final Schema schema){
        Entity RECOLTE = schema.addEntity("RECOLTE");
        RECOLTE.addIdProperty().primaryKey();
        RECOLTE.addStringProperty("nom");
        return RECOLTE;
    }
    //Table AUDIT_PARCELLE
    private static Entity addAUDIT_PARCELLE(final Schema schema) {
        Entity AUDIT_PARCELLE = schema.addEntity("AUDIT_PARCELLE");
        AUDIT_PARCELLE.addIdProperty().primaryKey();
        AUDIT_PARCELLE.addStringProperty("PHOTO_PARCELLE  ");
        AUDIT_PARCELLE.addStringProperty("REMARQUES_AUDIT_PARCELLE   ");
        AUDIT_PARCELLE.addIntProperty("archive").notNull();
        AUDIT_PARCELLE.addIntProperty("ID_Agronome_Creation").notNull();
        AUDIT_PARCELLE.addDateProperty("date_creation").notNull();
        AUDIT_PARCELLE.addIntProperty("ID_Agronome_Modification").notNull();
        AUDIT_PARCELLE.addDateProperty("date_modification").notNull();
        return AUDIT_PARCELLE;
    }

    //Liste de ETAT_VEGETATIF
    private static Entity addETAT_VEGETATIF(final Schema schema){
        Entity ETAT_VEGETATIF = schema.addEntity("ETAT_VEGETATIF");
        ETAT_VEGETATIF.addIdProperty().primaryKey();
        ETAT_VEGETATIF.addStringProperty("nom");
        return ETAT_VEGETATIF;
    }
    //Liste de choix PLANTE_NON_DESIRABLE
    private static Entity addPLANTE_NON_DESIRABLE(final Schema schema){
        Entity PLANTE_NON_DESIRABLE = schema.addEntity("PLANTE_NON_DESIRABLE");
        PLANTE_NON_DESIRABLE.addIdProperty().primaryKey();
        PLANTE_NON_DESIRABLE.addStringProperty("nom");
        return PLANTE_NON_DESIRABLE;
    }

    //Liste de choix ETAT_PLANTE
    private static Entity addETAT_PLANTE(final Schema schema){
        Entity ETAT_PLANTE = schema.addEntity("ETAT_PLANTE");
        ETAT_PLANTE.addIdProperty().primaryKey();
        ETAT_PLANTE.addStringProperty("nom");
        return ETAT_PLANTE;
    }
    //Liste de choix LISTE_PLANTE
    private static Entity addLISTE_PLANTE(final Schema schema){
        Entity LISTE_PLANTE = schema.addEntity("LISTE_PLANTE");
        LISTE_PLANTE.addIdProperty().primaryKey();
        LISTE_PLANTE.addStringProperty("nom");
        return LISTE_PLANTE;
    }

    //Table AUDIT_DOCUMENT
    private static Entity addAUDIT_DOCUMENT(final Schema schema) {
        Entity AUDIT_DOCUMENT = schema.addEntity("AUDIT_DOCUMENT");
        AUDIT_DOCUMENT.addIdProperty().primaryKey();
        AUDIT_DOCUMENT.addStringProperty("REMARQUES_AUDIT_DOCUMENT   ");

        AUDIT_DOCUMENT.addIntProperty("archive").notNull();
        AUDIT_DOCUMENT.addIntProperty("ID_Agronome_Creation").notNull();
        AUDIT_DOCUMENT.addDateProperty("date_creation").notNull();
        AUDIT_DOCUMENT.addIntProperty("ID_Agronome_Modification").notNull();
        AUDIT_DOCUMENT.addDateProperty("date_modification").notNull();
        return AUDIT_DOCUMENT;
    }

    //Table DOCUMENT
    private static Entity addDOCUMENT(final Schema schema) {
        Entity DOCUMENT = schema.addEntity("DOCUMENT");
        DOCUMENT.addIdProperty().primaryKey();
        return DOCUMENT;
    }
    //Liste de choix SCAN_DOCUMENT
    private static Entity addSCAN_DOCUMENT(final Schema schema){
        Entity SCAN_DOCUMENT = schema.addEntity("SCAN_DOCUMENT");
        SCAN_DOCUMENT.addIdProperty().primaryKey();
        SCAN_DOCUMENT.addStringProperty("nom");
        return SCAN_DOCUMENT;
    }
    //Liste de choix TYPE_DOCUMENT
    private static Entity addTYPE_DOCUMENT(final Schema schema){
        Entity TYPE_DOCUMENT = schema.addEntity("TYPE_DOCUMENT");
        TYPE_DOCUMENT.addIdProperty().primaryKey();
        TYPE_DOCUMENT.addStringProperty("nom");
        return TYPE_DOCUMENT;
    }
    //Liste de choix DOCUMENT_FOURNI
    private static Entity addDOCUMENT_FOURNI(final Schema schema){
        Entity DOCUMENT_FOURNI = schema.addEntity("DOCUMENT_FOURNI");
        DOCUMENT_FOURNI.addIdProperty().primaryKey();
        DOCUMENT_FOURNI.addStringProperty("nom");
        return DOCUMENT_FOURNI;
    }
    //Table CONTRE_VISITE
    private static Entity addCONTRE_VISITE(final Schema schema) {
        Entity CONTRE_VISITE = schema.addEntity("CONTRE_VISITE");
        CONTRE_VISITE.addIdProperty().primaryKey();
        CONTRE_VISITE.addDateProperty("DATE_CONTRE_VISITE    ");
        CONTRE_VISITE.addStringProperty("REMARQUE_CONTRE_VISITE     ");
        CONTRE_VISITE.addIntProperty("archive").notNull();
        CONTRE_VISITE.addIntProperty("ID_Agronome_Creation").notNull();
        CONTRE_VISITE.addDateProperty("date_creation").notNull();
        CONTRE_VISITE.addIntProperty("ID_Agronome_Modification").notNull();
        CONTRE_VISITE.addDateProperty("date_modification").notNull();
        return CONTRE_VISITE;
    }
    //Liste de choix ETAT_CONTRE_VISITE
    private static Entity addETAT_CONTRE_VISITE(final Schema schema){
        Entity ETAT_CONTRE_VISITE = schema.addEntity("ETAT_CONTRE_VISITE");
        ETAT_CONTRE_VISITE.addIdProperty().primaryKey();
        ETAT_CONTRE_VISITE.addStringProperty("nom");
        return ETAT_CONTRE_VISITE;
    }
    //Liste de choix CAUSE
    private static Entity addCAUSE(final Schema schema){
        Entity CAUSE = schema.addEntity("CAUSE");
        CAUSE.addIdProperty().primaryKey();
        CAUSE.addStringProperty("nom");
        return CAUSE;
    }
    //Table MESSAGE
    private static Entity addMESSAGE(final Schema schema) {
        Entity MESSAGE = schema.addEntity("MESSAGE");
        MESSAGE.addIdProperty().primaryKey();

        return MESSAGE;
    }
    //Table SELECTION_DESTINATAIRE_DEFAUT
    private static Entity addSELECTION_DESTINATAIRE_DEFAUT(final Schema schema) {
        Entity SELECTION_DESTINATAIRE_DEFAUT = schema.addEntity("SELECTION_DESTINATAIRE_DEFAUT");
        SELECTION_DESTINATAIRE_DEFAUT.addIdProperty().primaryKey();

        return SELECTION_DESTINATAIRE_DEFAUT;
    }

    //Table DESTINATAIRE_DEFAUT
    private static Entity addDESTINATAIRE_DEFAUT(final Schema schema) {
        Entity DESTINATAIRE_DEFAUT = schema.addEntity("DESTINATAIRE_DEFAUT");
        DESTINATAIRE_DEFAUT.addIdProperty().primaryKey();
        DESTINATAIRE_DEFAUT.addStringProperty("nom");
        return DESTINATAIRE_DEFAUT;
    }
    //Table OBJET_MESSAGE
    private static Entity addOBJET_MESSAGE(final Schema schema) {
        Entity OBJET_MESSAGE = schema.addEntity("OBJET_MESSAGE");
        OBJET_MESSAGE.addIdProperty().primaryKey();
        OBJET_MESSAGE.addStringProperty("nom");
        return OBJET_MESSAGE;
    }

    //Table TEXTE_MESSAGE
    private static Entity addTEXTE_MESSAGE(final Schema schema) {
        Entity TEXTE_MESSAGE = schema.addEntity("TEXTE_MESSAGE");
        TEXTE_MESSAGE.addIdProperty().primaryKey();
        TEXTE_MESSAGE.addStringProperty("nom");
        return TEXTE_MESSAGE;
    }

    //Base de données : Visite

    //Table Visite
    private static Entity addVisite(final Schema schema){
        Entity Visite = schema.addEntity("Visite");
        Visite.addIdProperty().primaryKey();
        Visite.addIntProperty("archive").notNull();
        Visite.addIntProperty("ID_Agronome_Creation").notNull();
        Visite.addDateProperty("date_creation").notNull();
        Visite.addIntProperty("ID_Agronome_Modification").notNull();
        Visite.addDateProperty("date_modification").notNull();
        return Visite;
    }

    //Liste de choix : Attitude Entreprise
    private static Entity addAttitudeEntrepriseVisite (final Schema schema){
        Entity AttitudeEntrepriseVisite = schema.addEntity("Attitude_Entreprise_Visite");
        AttitudeEntrepriseVisite.addIdProperty().primaryKey();
        AttitudeEntrepriseVisite.addStringProperty("nom");
        return AttitudeEntrepriseVisite;
    }

    //Liste de choix : Interet Entreprise
    private static Entity addInteretVisite (final Schema schema){
        Entity InteretVisite= schema.addEntity("Interet_Visite");
        InteretVisite.addIdProperty().primaryKey();
        InteretVisite.addStringProperty("nom");
        return InteretVisite;
    }

    //Liste de choix : Objet Visite
    private static Entity addObjetVisite (final Schema schema){
        Entity ObjetVisite= schema.addEntity("Objet_Visite");
        ObjetVisite.addIdProperty().primaryKey();
        ObjetVisite.addStringProperty("nom");
        return ObjetVisite;
    }

    //Table Sujet Visite Principal
    private static Entity addSujetVisitePrincipal(final Schema schema){
        Entity SujetVisiteP = schema.addEntity("Sujet_Visite_Principal");
        SujetVisiteP.addIdProperty().primaryKey();
        SujetVisiteP.addStringProperty("Discussion_Visite");
        return SujetVisiteP;
    }

    //Table Sujet Visite Secondaire
    private static Entity addSujetVisiteSecondaire(final Schema schema){
        Entity SujetVisites = schema.addEntity("Sujet_Visite_Secondaire");
        SujetVisites.addIdProperty().primaryKey();
        SujetVisites.addStringProperty("Discussion_Visite");
        return SujetVisites;
    }

    //Base de données : Incident Administratif

    //Table Incident
    private static Entity addIncident(final Schema schema){
        Entity Incident = schema.addEntity("Incident");
        Incident.addIdProperty().primaryKey();
        Incident.addStringProperty("Message_Incident");
        Incident.addIntProperty("archive").notNull();
        Incident.addIntProperty("ID_Agronome_Creation").notNull();
        Incident.addDateProperty("date_creation").notNull();
        Incident.addIntProperty("ID_Agronome_Modification").notNull();
        Incident.addDateProperty("date_modification").notNull();
        return Incident;
    }

    //Table Sujet Message
    private static Entity addSujetMessage(final Schema schema){
        Entity Sujet_Message = schema.addEntity("Sujet_Message");
        Sujet_Message.addIdProperty().primaryKey();
        return Sujet_Message;
    }

    //Table Type_Incident
    private static Entity addTypeIncident(final Schema schema){
        Entity Type_Incident = schema.addEntity("Type_Incident");
        Type_Incident.addIdProperty().primaryKey();
        Type_Incident.addStringProperty("nom");
        return Type_Incident;
    }

    //Table Incident Administratif
    private static Entity addIncidentAdministratif(final Schema schema){
        Entity Incident_Administratif = schema.addEntity("Incident_Administratif");
        Incident_Administratif.addIdProperty().primaryKey();
        Incident_Administratif.addStringProperty("nom");
        return Incident_Administratif;
    }

    //Table Incident Contrat
    private static Entity addIncidentContrat(final Schema schema){
        Entity Incident_Contrat = schema.addEntity("Incident_Contrat");
        Incident_Contrat.addIdProperty().primaryKey();
        Incident_Contrat.addStringProperty("nom");
        return Incident_Contrat;
    }

    //Table Etat Incident
    private static Entity addEtatIncident(final Schema schema){
        Entity Etat_Incident = schema.addEntity("Etat_Incident");
        Etat_Incident.addIdProperty().primaryKey();
        Etat_Incident.addStringProperty("nom");
        return Etat_Incident;
    }

    //Base de données Suivi Incident Administratif

    //Table Suivi Incident
    private static Entity addSuiviIncident(final Schema schema){
        Entity Suivi_Incident = schema.addEntity("Suivi_Incident");
        Suivi_Incident.addIdProperty().primaryKey();
        Suivi_Incident.addStringProperty("Message_Suivi_Incident");
        Suivi_Incident.addIntProperty("archive").notNull();
        Suivi_Incident.addIntProperty("ID_Agronome_Creation").notNull();
        Suivi_Incident.addDateProperty("date_creation").notNull();
        Suivi_Incident.addIntProperty("ID_Agronome_Modification").notNull();
        Suivi_Incident.addDateProperty("date_modification").notNull();
        return Suivi_Incident;
    }

    //Table Expediteurs
    private static Entity addExpediteur(final Schema schema){
        Entity Expediteur = schema.addEntity("Expediteur");
        Expediteur.addIdProperty().primaryKey();
        return Expediteur;
    }

    //Table Type Expediteur
    private static Entity addTypeExpediteur(final Schema schema){
        Entity Type_Expediteur = schema.addEntity("Type_Expediteur");
        Type_Expediteur.addIdProperty().primaryKey();
        Type_Expediteur.addStringProperty("nom");
        return Type_Expediteur;
    }

    //Table Liste Expediteur Contact
    private static Entity addListeExpediteurContact(final Schema schema){
        Entity ListeExpediteurContact = schema.addEntity("Liste_Expediteur_Contact");
        ListeExpediteurContact.addIdProperty().primaryKey();
        ListeExpediteurContact.addStringProperty("nom");
        return ListeExpediteurContact;
    }

    //Table Liste Expediteur Agronome
    private static Entity addListeExpediteurAgronome(final Schema schema){
        Entity ListeExpediteurAgronome = schema.addEntity("Liste_Expediteur_Agronome");
        ListeExpediteurAgronome.addIdProperty().primaryKey();
        ListeExpediteurAgronome.addStringProperty("nom");
        return ListeExpediteurAgronome;
    }

    //Table Liste Expediteur Administration
    private static Entity addListeExpediteurAdministration(final Schema schema){
        Entity ListeExpediteurAdministration = schema.addEntity("Liste_Expediteur_Administration");
        ListeExpediteurAdministration.addIdProperty().primaryKey();
        ListeExpediteurAdministration.addStringProperty("nom");
        return ListeExpediteurAdministration;
    }


}

